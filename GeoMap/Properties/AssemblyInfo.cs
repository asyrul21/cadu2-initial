﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CADU 2.0")]
[assembly: AssemblyDescription("Centralize Administrative Data Update Platform")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TM")]
[assembly: AssemblyProduct("Geomatics")]
[assembly: AssemblyCopyright("Telekom Malaysia Berhad")]
[assembly: AssemblyTrademark("2020@TM")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8017c2f6-a31b-4040-9d36-111b05e09aba")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.42")]
[assembly: AssemblyFileVersion("2.0.0.42")]

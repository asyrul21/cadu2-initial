﻿using System;
using System.Collections.Generic;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Controls;
using Geomatic.UI.Forms.Documents;
using Geomatic.UI.Commands;
using Geomatic.MapTool;
using Geomatic.Core;
using ICommand = Geomatic.UI.Commands.ICommand;
using Geomatic.MapTool.EditorTools;
using ESRI.ArcGIS.Carto;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;

namespace Geomatic.Map.Commands
{
    /// <summary>
    /// To handle control by grouping them together
    /// </summary>
    class CommandPool : Geomatic.UI.Commands.CommandPool
    {
        public EventHandler<ToolReportedEventArgs> ToolReported;
        public EventHandler<ToolStepReportedEventArgs> ToolStepReported;

        private IHookHelper _hookHelper;
        private Dictionary<string, ITool> _tools;

        private IMapControl4 MapControl
        {
            get
            {
                return _hookHelper.Hook as IMapControl4;
            }
        }

        public CommandPool()
        {
            _hookHelper = new HookHelperClass();
            _tools = new Dictionary<string, ITool>();
        }

        public void RegisterMapTool(string commandName, ITool tool)
        {
            if (_tools.ContainsKey(commandName))
            {
                throw new Exception(string.Format("Map tool already registered. {0}", commandName));
            }

            IToolEvent toolEvent = tool as IToolEvent;
            if (toolEvent != null)
            {
                toolEvent.Reported += new EventHandler<ToolReportedEventArgs>(Tool_Reported);
                toolEvent.StepReported += new EventHandler<ToolStepReportedEventArgs>(Tool_StepReported);
            }
            _tools.Add(commandName, tool);
        }

        public void Limit(string groupCommandName, List<string> commands)
        {
            GroupCommand groupCommand = GetCommand(groupCommandName) as GroupCommand;

            if (groupCommand == null)
            {
                return;
            }

            GroupCommand limitedToCommand = new GroupCommand();

            foreach (string commandName in commands)
            {
                ICommand command = GetCommand(commandName);
                if (command == null)
                {
                    continue;
                }
                limitedToCommand.Add(command);
            }

            foreach (ICommand command in groupCommand.Commands)
            {
                if (!limitedToCommand.Contains(command))
                {
                    command.Enabled = false;
                }
            }
        }

        public void SetMapTool(string commandName)
        {
            ITool tool = GetMapTool(commandName);

            if (MapControl != null)
            {
                MapControl.CurrentTool = tool;

                if (MapControl.CurrentTool == null)
                {
                    foreach (string commandName2 in _tools.Keys)
                    {
                        UnCheck(commandName2);
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, ITool> pair in _tools)
                    {
                        if (object.Equals(MapControl.CurrentTool, pair.Value))
                        {
                            Check(pair.Key);
                        }
                    }
                }
            }
        }

        private ITool GetMapTool(string commandName)
        {
            if (!_tools.ContainsKey(commandName))
            {
                throw new ArgumentException(string.Format("Invalid command. {0}", commandName));
            }
            return _tools[commandName];
        }

        public void SetInvisible(string commandName)
        {
            ICommand command = GetCommand(commandName);
            command.Visible = false;
        }

        public void SetVisible(string commandName)
        {
            ICommand command = GetCommand(commandName);
            command.Visible = true;
        }

        public void OnSelectedLayerChanged(ILayer selectLayer)
        {
            foreach (ITool tool in _tools.Values)
            {
                if (tool is IEditorTool)
                {
                    ((IEditorTool)tool).OnSelectedLayerChanged(selectLayer);
                }
            }
        }

        public void OnDocumentChanged(GeoMapDocument document)
        {
            _hookHelper.Hook = (document == null) ? null : document.MapControl;

            foreach (IToolHook toolHook in _tools.Values)
            {
                toolHook.SetHook((document == null) ? null : document.MapControl);
            }

            foreach (IToolEvent toolEvent in _tools.Values)
            {
                toolEvent.OnDocumentChanged(document);
            }
        }

        public void OnSegmentChanged(SegmentName version)
        {
            foreach (IToolEvent toolEvent in _tools.Values)
            {
                toolEvent.OnSegmentChanged(version);
            }
        }

        public void OnUndo()
        {
            foreach (IToolEvent toolEvent in _tools.Values)
            {
                toolEvent.OnUndo();
            }
        }

        public void OnRedo()
        {
            foreach (IToolEvent toolEvent in _tools.Values)
            {
                toolEvent.OnRedo();
            }
        }

        public void OnSave()
        {
            foreach (IToolEvent toolEvent in _tools.Values)
            {
                toolEvent.OnSave();
            }
        }

        private void Tool_Reported(object sender, ToolReportedEventArgs e)
        {
            OnToolReported(e);
        }

        private void OnToolReported(ToolReportedEventArgs e)
        {
            if (ToolReported != null)
                ToolReported(this, e);
        }

        private void Tool_StepReported(object sender, ToolStepReportedEventArgs e)
        {
            OnToolStepReported(e);
        }

        private void OnToolStepReported(ToolStepReportedEventArgs e)
        {
            if (ToolStepReported != null)
                ToolStepReported(this, e);
        }
    }
}

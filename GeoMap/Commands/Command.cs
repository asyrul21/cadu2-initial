﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Map.Commands
{
    class Command
    {
        public const string OpenShape = "OpenShape";
        public const string OpenGdb = "OpenGdb";
        public const string OpenRaster = "OpenRaster";
        public const string OpenWork = "OpenWork";
        public const string OpenGPS = "OpenGPS";  // noraini ali - Jun 2020
        public const string Save = "Save";
        public const string Undo = "Undo";
        public const string Redo = "Redo";
        public const string NoTool = "NoTool";
        public const string Select = "Select";
        public const string Pan = "Pan";
        public const string ZoomIn = "ZoomIn";
        public const string ZoomOut = "ZoomOut";
        public const string ZoomToSelection = "ZoomToSelection";
        public const string FullExtent = "FullExtent";
        public const string PreviousExtent = "PreviousExtent";
        public const string NextExtent = "NextExtent";
        public const string Refresh = "Refresh";
        public const string ClearSelection = "ClearSelection";
        public const string Ruler = "Ruler";
        //added by asyrul
        public const string OnLayerToggle = "OnLayerToggle";
        //added end
        public const string SegmentedCommand = "SegmentedCommand";
        public const string DocumentCommand = "DocumentCommand";
        public const string MapTool = "MapTool";
        // editor
        public const string DrawLine = "DrawLine";
        public const string DrawCircle = "DrawCircle";
        public const string DrawRectangle = "DrawRectangle";
        public const string DrawPolygon = "DrawPolygon";
        public const string ClearDrawings = "ClearDrawings";
        public const string ViewEdits = "ViewEdits";
        // street
        public const string SearchStreet = "SearchStreet";
        public const string AddStreet = "AddStreet";
        public const string EditStreet = "EditStreet";
        public const string DeleteStreet = "DeleteStreet";
        public const string SplitStreet = "SplitStreet";
        public const string UnionStreet = "UnionStreet";
        public const string CopyStreet = "CopyStreet";
        public const string FlipStreet = "FlipStreet";
        public const string ShowStreetProperty = "ShowStreetProperty";
        public const string ReshapeStreet = "ReshapeStreet";

        //added by asyrul
        public const string CopyStreetAttribute = "CopyStreetAttribute";
        public const string SearchStreetById = "SearchStreetById";
        public const string SearchStreetByName = "SearchStreetByName";
        //added end

        public const string AddStreetVertex = "AddStreetVertex";
        public const string MoveStreetVertex = "MoveStreetVertex";
        public const string DeleteStreetVertex = "DeleteStreetVertex";
        public const string SearchStreetRestriction = "SearchStreetRestriction";
        public const string AddStreetRestriction = "AddStreetRestriction";
        public const string DeleteStreetRestriction = "DeleteStreetRestriction";
        public const string MoveStreetRestriction = "MoveStreetRestriction";
        public const string ShowStreetRestrictionStreet = "ShowStreetRestrictionStreet";
        // junction
        public const string SearchJunction = "SearchJunction";
        public const string EditJunction = "EditJunction";
        public const string MoveJunction = "MoveJunction";
        public const string DeleteJunction = "DeleteJunction";
        public const string SplitJunction = "SplitJunction";
        public const string SearchJunctionById = "SearchJunctionById";
        
        //added by asyrul
        public const string ReassociateJunction = "ReassociateJunction";
        //added end

        // property
        public const string SearchProperty = "SearchProperty";
        public const string AddProperty = "AddProperty";
        public const string AddMultipleProperty = "AddMultipleProperty";
        public const string EditProperty = "EditProperty";
        public const string EditMultipleProperty = "EditMultipleProperty";
        public const string MoveProperty = "MoveProperty";
        public const string DeleteProperty = "DeleteProperty";
        public const string ReassociateProperty = "ReassociateProperty";
        public const string SearchFloor = "SearchFloor";

        // added by asyrul
        public const string SearchPropertyById = "SearchPropertyById";
        public const string SearchPropertyByName = "SearchPropertyByName";
        public const string SearchPropertyFloorById = "SearchPropertyFloorById";
        // added end
        // building
        public const string SearchBuilding = "SearchBuilding";
        public const string AddBuilding = "AddBuilding";
        public const string EditBuilding = "EditBuilding";
        public const string DeleteBuilding = "DeleteBuilding";
        public const string MoveBuilding = "MoveBuilding";
        
        // added by asyrul
        public const string ReassociateBuilding = "ReassociateBuilding";
        public const string SearchBuildingById = "SearchBuildingById";
        public const string SearchBuildingByName = "SearchBuildingByName";
        public const string SearchBuildingGroupById = "SearchBuildingGroupById";
        public const string SearchBuildingGroupByName = "SearchBuildingGroupByName";
        public const string SearchMultiStoreyById = "SearchMultiStoreyById";
        // added end
        public const string SearchBuildingGroup = "SearchBuildingGroup";
        public const string AddBuildingGroup = "AddBuildingGroup";
        public const string EditBuildingGroup = "EditBuildingGroup";
        public const string DeleteBuildingGroup = "DeleteBuildingGroup";
        public const string ReassociateBuildingGroup = "ReassociateBuildingGroup";
        public const string MoveBuildingGroup = "MoveBuildingGroup";
        // added by asyrul
        public const string AssociateBuildingToBuildingGroup = "AssociateBuildingToBuildingGroup";
        public const string AssociateBuildingByDistance = "AssociateBuildingByDistance";
        // added end
        public const string DissociateBuildingToBuildingGroup = "DissociateBuildingToBuildingGroup";
        public const string SearchMultiStorey = "SearchMultiStorey";
        // landmark
        public const string SearchLandmark = "SearchLandmark";
        public const string AddLandmark = "AddLandmark";
        public const string EditLandmark = "EditLandmark";
        public const string DeleteLandmark = "DeleteLandmark";
        public const string ReassociateLandmark = "ReassociateLandmark";
        public const string MoveLandmark = "MoveLandmark";
        public const string SearchLandmarkBoundary = "SearchLandmarkBoundary";
        public const string AddLandmarkBoundary = "AddLandmarkBoundary";
        public const string DeleteLandmarkBoundary = "DeleteLandmarkBoundary";
        public const string MoveLandmarkBoundary = "MoveLandmarkBoundary";
        public const string AddLandmarkBoundaryVertex = "AddLandmarkBoundaryVertex";
        public const string MoveLandmarkBoundaryVertex = "MoveLandmarkBoundaryVertex";
        public const string DeleteLandmarkBoundaryVertex = "DeleteLandmarkBoundaryVertex";
        // added by asyrul
        public const string SearchLandmarkById = "SearchLandmarkById";
        public const string SearchLandmarkByName = "SearchLandmarkByName";
        // added end
        // poi
        public const string SearchPoi = "SearchPoi";
        public const string EditPoi = "EditPoi";
        public const string MergePoi = "MergePoi";
        public const string ShowPoiStreet = "ShowPoiStreet";
        // added by asyrul
        public const string SearchPoiById = "SearchPoiById";
        public const string SearchPoiByName = "SearchPoiByName";
        // added end
        // phone
        public const string SearchPhone = "SearchPhone";
        public const string SearchPhoneByNo = "SearchPhoneByNo";
        // location
        public const string SearchSectionBoundary = "SearchSectionBoundary";
        public const string AddSectionBoundary = "AddSectionBoundary";
        public const string EditSectionBoundary = "EditSectionBoundary";
        public const string DeleteSectionBoundary = "DeleteSectionBoundary";
        public const string MoveSectionBoundary = "MoveSectionBoundary";
        public const string AddSectionBoundaryVertex = "AddSectionBoundaryVertex";
        public const string MoveSectionBoundaryVertex = "MoveSectionBoundaryVertex";
        public const string DeleteSectionBoundaryVertex = "DeleteSectionBoundaryVertex";
        public const string ManageLocation = "ManageLocation";
        // user
        public const string ManageAccessibility = "ManageAccessibility";
        public const string ManageAccessibilityPssword = "ManageAccessibilityPssword";
        public const string UserCommand = "UserCommand";
        public const string VerifyWEB = "VerifyWEB";

        // region / index
        public const string ShowIndex = "ShowIndex";
        public const string SearchIndex = "SearchIndex";

        // Verification AND Feature
        public const string SearchWorkArea = "SearchWorkArea";
        public const string VerifyWorkArea = "VerifyWorkArea";
        public const string VerifyWorkArea2 = "VerifyWorkArea2";

        // work area
        public const string CreateWorkArea = "CreateWorkArea";
        public const string DeleteWorkArea = "DeleteWorkArea";
        public const string CompleteWorkArea = "CompleteWorkArea";
        public const string CloseWorkArea = "CloseWorkArea";

        // reshapevertex work area
        public const string AddWorkAreaVertex = "AddWorkAreaVertex";
        public const string MoveWorkAreaVertex = "MoveWorkAreaVertex";
        public const string DeleteWorkAreaVertex = "DeleteWorkAreaVertex";

        // jupem Lot
        public const string SearchJupemLot = "SearchJupemLot";

		// raster
		public const string OpenRasterWarping = "OpenRasterWarping";
		public const string CloseRasterWarping = "CloseRasterWarping";
		public const string AddPoint = "AddPoint";
		public const string DeletePoint = "DeletePoint";
		public const string DeleteAllPoints = "DeleteAllPoints";
		public const string UpdateAlignment = "UpdateAlignment";
		public const string FitToDisplay = "FitToDisplay";
		public const string RegisterRaster = "RegisterRaster";
		public const string BuildPyramid = "BuildPyramid";
		public const string MoveRaster = "MoveRaster";
		public const string RotateClockwise = "RotateClockwise";
		public const string RotateAntiClockwise = "RotateAntiClockwise";
		public const string ScaleUp = "ScaleUp";
		public const string ScaleDown = "ScaleDown";
		public const string OpenRasterInPaint = "OpenRasterInPaint";
		public const string ExportRaster = "ExportRaster";
	}
}

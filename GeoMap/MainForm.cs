﻿using ESRI.ArcGIS.Carto;
using Geomatic.Core;
using Geomatic.Core.CRS;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using Geomatic.Map.Commands;
using Geomatic.MapTool;
using Geomatic.MapTool.BuildingGroupTools;
using Geomatic.MapTool.BuildingTools;
using Geomatic.MapTool.DrawingTools;
using Geomatic.MapTool.JunctionTools;
using Geomatic.MapTool.LandmarkBoundaryTools;
using Geomatic.MapTool.LandmarkTools;
using Geomatic.MapTool.PoiTools;
using Geomatic.MapTool.PropertyTools;
using Geomatic.MapTool.RegionTools;
using Geomatic.MapTool.JupemTools;
using Geomatic.MapTool.WorkAreaTools;
using Geomatic.MapTool.SectionBoundaryTools;
using Geomatic.MapTool.StreetTools;
using Geomatic.UI;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Documents;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.NewItems;
using Geomatic.UI.Forms.OpenMap;
using Geomatic.UI.Forms.Options;
using Geomatic.UI.Forms.Search;
using Geomatic.UI.Forms.Toolbox;
using Geomatic.UI.Forms.Views;
using Geomatic.UI.Maps;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using GeoCommandPool = Geomatic.Map.Commands.CommandPool;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;
using Geomatic.MapTool.RasterTools;

namespace Geomatic.Map
{
    public partial class MainForm : RibbonForm
    {
        private ViewManager _viewManager;
        private ToolboxManager _toolboxManager;
        private GeoCommandPool _commandPool;

        private ILayer SelectedLayer { set; get; }

        private SegmentName? SelectedSegment
        {
            get
            {
                SegmentButton segmentButton = cbHomeGeneralSegment.SelectedItem as SegmentButton;
                return (segmentButton == null) ? null : segmentButton.SegmentName;
            }
        }

        private string SegmentFullName
        {
            get
            {
                SegmentButton segmentButton = cbHomeGeneralSegment.SelectedItem as SegmentButton;
                return (segmentButton == null) ? null : StringUtils.Trim(segmentButton.Text);
            }
        }

        private Area SelectedArea
        {
            get
            {
                if (!SelectedSegment.HasValue)
                {
                    return Area.UNDECIDED;
                }
                return (SelectedSegment.Value == SegmentName.AS
                    || SelectedSegment.Value == SegmentName.JH
                    || SelectedSegment.Value == SegmentName.JP
                    || SelectedSegment.Value == SegmentName.KN
                    || SelectedSegment.Value == SegmentName.KV
                    || SelectedSegment.Value == SegmentName.MK
                    || SelectedSegment.Value == SegmentName.PG
                    || SelectedSegment.Value == SegmentName.TG) ? Area.WEST :
                    (SelectedSegment.Value == SegmentName.KG
                    || SelectedSegment.Value == SegmentName.KK) ? Area.EAST :
                    Area.UNDECIDED;
            }
        }

        private GeoMapDocument ActiveMap
        {
            get
            {
                return dockPanel.ActiveDocument as GeoMapDocument;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            RefreshTitle("{0} - v{1}", AppInfo.Title, AppInfo.Version);

            //Disable POI,Location,User,Editor tab on AND Form
            if (Session.User.GetGroup().Name == "AND")
            {
                poiTab.Visible = false;
                locationTab.Visible = false;
                userTab.Visible = false;
                editorTab.Visible = false;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (DesignMode) { return; }

            Program.Splash.Show(this);
            using (new WaitCursor())
            {
                InitSessions();
                InitScales();
                InitViews();
                InitToolboxes();
                InitCommands();
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.Pan);
            UpdateMenu();

            Program.Splash.Close();

            //WindowState = FormWindowState.Maximized;
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            this.SuspendLayout();
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            this.ResumeLayout();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Session.Current.HasEdits())
            {
                using (QuestionMessageBox box = new QuestionMessageBox())
                {
                    box.SetCaption("Exit");
                    box.SetText("Do you want to exit without saving the changes you made?");
                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);
                    DialogResult result = box.Show(this);

                    switch (result)
                    {
                        case DialogResult.Yes:
                            {
                                break;
                            }
                        case DialogResult.No:
                        case DialogResult.Cancel:
                            {
                                e.Cancel = true;
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseDocuments();
            _viewManager.Dispose();
            _toolboxManager.Dispose();
            Session.Current.Close();
        }

        private void InitSessions()
        {
            Session.Current.WorkspaceChanged += new EventHandler<WorkspaceChangedEventArgs>(Session_WorkspaceChanged);
            Session.Current.StopEditing += new EventHandler<EditEventArgs>(Session_StopEditing);
            Session.Current.UndoOperation += new EventHandler<WorkspaceEventArgs>(Session_UndoOperation);
            Session.Current.RedoOperation += new EventHandler<WorkspaceEventArgs>(Session_RedoOperation);
            Session.Current.StartOperation += new EventHandler<WorkspaceEventArgs>(Session_StartOperation);
            Session.Current.StopOperation += new EventHandler<WorkspaceEventArgs>(Session_StopOperation);
            Session.Current.AbortOperation += new EventHandler<WorkspaceEventArgs>(Session_AbortOperation);
            Session.Current.ConflictsDetected += new EventHandler<WorkspaceEventArgs>(Session_ConflictsDetected);
        }

        private void InitViews()
        {
            _viewManager = new ViewManager(this, dockPanel);
            _viewManager.GetView<LayerView>().Show(dockPanel);
            ((LayerView)_viewManager.GetView<LayerView>()).SelectedLayerChanged += new EventHandler<SelectedLayerChangedEventArgs>(LayerView_SelectedLayerChanged);
            _viewManager.GetView<OutputView>().Show(dockPanel);
            _viewManager.GetView<SelectionsView>().Show(dockPanel);
        }

        private void InitToolboxes()
        {
            _toolboxManager = new ToolboxManager();
            GoToToolbox gotoToolbox = (GoToToolbox)_toolboxManager.GetView<GoToToolbox>();
            gotoToolbox.ZoomTo += new EventHandler<GoToEventArgs>(goToToolbox_ZoomTo);
            gotoToolbox.PanTo += new EventHandler<GoToEventArgs>(goToToolbox_PanTo);
            gotoToolbox.ClearMarker += new EventHandler(goToToolbox_ClearMarker);
        }

        private void InitCommands()
        {
            _commandPool = new GeoCommandPool();
            _commandPool.ToolReported += new EventHandler<ToolReportedEventArgs>(CommandPool_ToolReported);
            _commandPool.ToolStepReported += new EventHandler<ToolStepReportedEventArgs>(CommandPool_ToolStepReported);

            _commandPool.Register(Command.OpenShape, menuFileOpenShape);
            _commandPool.Register(Command.OpenGdb, menuFileOpenGdb);
            _commandPool.Register(Command.OpenRaster, menuFileOpenRaster);
            _commandPool.Register(Command.OpenWork, menuFileOpenWorkOrder);
            _commandPool.Register(Command.OpenGPS, menuFileOpenGPS);  // noraini ali - Jun 2020

            _commandPool.Register(Command.Save, menuFileSave, btnSave);
            _commandPool.Register(Command.Undo, btnUndo);
            _commandPool.Register(Command.Redo, btnRedo);

            _commandPool.Register(Command.NoTool, btnHomeMapNoTool, menuNoTool);
            _commandPool.Register(Command.Select, btnHomeMapSelect, menuSelect);
            _commandPool.Register(Command.Pan, btnHomeMapPan, menuPan);
            _commandPool.Register(Command.ZoomIn, btnHomeMapZoomIn, menuZoomIn);
            _commandPool.Register(Command.ZoomOut, btnHomeMapZoomOut, menuZoomOut);
            _commandPool.Register(Command.ZoomToSelection, btnHomeMapZoomToSelection, menuZoomToSelection);
            _commandPool.Register(Command.FullExtent, btnHomeMapFullExtent, menuFullExtent);
            _commandPool.Register(Command.PreviousExtent, btnHomeMapPreviousExtent, menuPreviousExtent);
            _commandPool.Register(Command.NextExtent, btnHomeMapNextExtent, menuNextExtent);
            _commandPool.Register(Command.Refresh, btnHomeMapRefresh, menuRefresh);
            _commandPool.Register(Command.ClearSelection, btnHomeMapClearSelection, menuClearSelection);
            _commandPool.Register(Command.Ruler, btnHomeMapRuler, menuRuler);

            _commandPool.Register(Command.DrawLine, btnEditorDrawLine);
            _commandPool.Register(Command.DrawCircle, btnEditorDrawCircle);
            _commandPool.Register(Command.DrawRectangle, btnEditorDrawRectangle);
            _commandPool.Register(Command.DrawPolygon, btnEditorDrawPolygon);
            _commandPool.Register(Command.ClearDrawings, btnEditorDrawClearDrawings);

            _commandPool.Register(Command.SearchStreet, btnStreetGeneralSearch);
            _commandPool.Register(Command.AddStreet, btnStreetGeneralAdd);
            _commandPool.Register(Command.EditStreet, btnStreetGeneralEdit);
            _commandPool.Register(Command.DeleteStreet, btnStreetGeneralDelete);
            _commandPool.Register(Command.SplitStreet, btnStreetGeneralSplit);
            _commandPool.Register(Command.UnionStreet, btnStreetGeneralUnion);
            _commandPool.Register(Command.CopyStreet, btnStreetGeneralCopy);
            _commandPool.Register(Command.FlipStreet, btnStreetGeneralFlip);
            _commandPool.Register(Command.ShowStreetProperty, btnStreetGeneralShowProperty);
            _commandPool.Register(Command.ReshapeStreet, btnStreetGeneralReshape);
            // added by asyrul
            _commandPool.Register(Command.CopyStreetAttribute, btnStreetGeneralCopyAttribute);
            _commandPool.Register(Command.SearchStreetById, btnStreetGeneralSearchById);
            _commandPool.Register(Command.SearchStreetByName, btnStreetGeneralSearchByName);
            // added end
            _commandPool.Register(Command.AddStreetVertex, btnStreetVertexAdd);
            _commandPool.Register(Command.MoveStreetVertex, btnStreetVertexMove);
            _commandPool.Register(Command.DeleteStreetVertex, btnStreetVertexDelete);
            _commandPool.Register(Command.SearchStreetRestriction, btnStreetRestrictionSearch);
            _commandPool.Register(Command.AddStreetRestriction, btnStreetRestrictionAdd);
            _commandPool.Register(Command.DeleteStreetRestriction, btnStreetRestrictionDelete);
            _commandPool.Register(Command.MoveStreetRestriction, btnStreetRestrictionMove);
            _commandPool.Register(Command.ShowStreetRestrictionStreet, btnStreetRestrictionShowStreet);

            _commandPool.Register(Command.SearchJunction, btnJunctionGeneralSearch);
            _commandPool.Register(Command.EditJunction, btnJunctionGeneralEdit);
            _commandPool.Register(Command.DeleteJunction, btnJunctionGeneralDelete);
            _commandPool.Register(Command.MoveJunction, btnJunctionGeneralMove);
            _commandPool.Register(Command.SplitJunction, btnJunctionGeneralSplit);
            // added by asyrul
            _commandPool.Register(Command.ReassociateJunction, btnJunctionGeneralReassociate);
            _commandPool.Register(Command.SearchJunctionById, btnJunctionSearchById);
            // added end

            _commandPool.Register(Command.SearchProperty, btnPropertyGeneralSearch);
            _commandPool.Register(Command.AddProperty, btnPropertyGeneralAdd);
            _commandPool.Register(Command.AddMultipleProperty, btnPropertyGeneralAddMultiple);
            _commandPool.Register(Command.EditProperty, btnPropertyGeneralEdit);
            _commandPool.Register(Command.EditMultipleProperty, btnPropertyGeneralEditMultiple);
            _commandPool.Register(Command.DeleteProperty, btnPropertyGeneralDelete);
            _commandPool.Register(Command.ReassociateProperty, btnPropertyGeneralReassociate);
            _commandPool.Register(Command.MoveProperty, btnPropertyGeneralMove);
            _commandPool.Register(Command.SearchFloor, btnPropertyFloorSearch);
            // added by asyrul
            _commandPool.Register(Command.SearchPropertyById, btnPropertyGeneralSearchById);
            _commandPool.Register(Command.SearchPropertyByName, btnPropertyGeneralSearchByName);
            _commandPool.Register(Command.SearchPropertyFloorById, btnPropertyFloorSearchById);
            // added end

            _commandPool.Register(Command.SearchBuilding, btnBuildingGeneralSearch);
            _commandPool.Register(Command.AddBuilding, btnBuildingGeneralAdd);
            _commandPool.Register(Command.EditBuilding, btnBuildingGeneralEdit);
            _commandPool.Register(Command.DeleteBuilding, btnBuildingGeneralDelete);
            _commandPool.Register(Command.MoveBuilding, btnBuildingGeneralMove);
            // added by asyrul
            _commandPool.Register(Command.ReassociateBuilding, btnBuildingGeneralReassociate);
            _commandPool.Register(Command.SearchBuildingById, btnBuildingGeneralSearchById);
            _commandPool.Register(Command.SearchBuildingByName, btnBuildingGeneralSearchByName);
            _commandPool.Register(Command.SearchBuildingGroupById, btnBuildingGroupSearchById);
            _commandPool.Register(Command.SearchBuildingGroupByName, btnBuildingGroupSearchByName);
            // added end
            _commandPool.Register(Command.SearchBuildingGroup, btnBuildingGroupSearch);
            _commandPool.Register(Command.AddBuildingGroup, btnBuildingGroupAdd);
            _commandPool.Register(Command.EditBuildingGroup, btnBuildingGroupEdit);
            _commandPool.Register(Command.DeleteBuildingGroup, btnBuildingGroupDelete);
            _commandPool.Register(Command.ReassociateBuildingGroup, btnBuildingGroupReassociate);
            _commandPool.Register(Command.MoveBuildingGroup, btnBuildingGroupMove);
            _commandPool.Register(Command.SearchMultiStorey, btnBuildingMultiStoreySearch);
            // added by asyrul
            _commandPool.Register(Command.AssociateBuildingToBuildingGroup, btnAssociateBuildingToBuildingGroup);
            _commandPool.Register(Command.AssociateBuildingByDistance, btnAssociateBuildingByDistance);
            _commandPool.Register(Command.SearchMultiStoreyById, btnBuildingMultiStoreySearchById);
            // added by asyrul
            _commandPool.Register(Command.DissociateBuildingToBuildingGroup, btnDissociateBuildingToBuildingGroup); // noraini - NOV 2020

            _commandPool.Register(Command.SearchLandmark, btnLandmarkGeneralSearch);
            _commandPool.Register(Command.AddLandmark, btnLandmarkGeneralAdd);
            _commandPool.Register(Command.EditLandmark, btnLandmarkGeneralEdit);
            _commandPool.Register(Command.DeleteLandmark, btnLandmarkGeneralDelete);
            _commandPool.Register(Command.ReassociateLandmark, btnLandmarkGeneralReassociate);
            _commandPool.Register(Command.MoveLandmark, btnLandmarkGeneralMove);
            _commandPool.Register(Command.SearchLandmarkBoundary, btnLandmarkBoundarySearch);
            _commandPool.Register(Command.AddLandmarkBoundary, btnLandmarkBoundaryAdd);
            _commandPool.Register(Command.DeleteLandmarkBoundary, btnLandmarkBoundaryDelete);
            _commandPool.Register(Command.MoveLandmarkBoundary, btnLandmarkBoundaryMove);
            _commandPool.Register(Command.AddLandmarkBoundaryVertex, btnLandmarkBoundaryVertexAdd);
            _commandPool.Register(Command.MoveLandmarkBoundaryVertex, btnLandmarkBoundaryVertexMove);
            _commandPool.Register(Command.DeleteLandmarkBoundaryVertex, btnLandmarkBoundaryVertexDelete);
            // added by asyrul
            _commandPool.Register(Command.SearchLandmarkById, btnLandmarkGeneralSearchById);
            _commandPool.Register(Command.SearchLandmarkByName, btnLandmarkGeneralSearchByName);
            // added end

            _commandPool.Register(Command.SearchPoi, btnPoiGeneralSearch);
            _commandPool.Register(Command.EditPoi, btnPoiGeneralEdit);
            _commandPool.Register(Command.MergePoi, btnPoiGeneralMerge);
            _commandPool.Register(Command.ShowPoiStreet, btnPoiGeneralShowStreet);
            // added by asyrul
            _commandPool.Register(Command.SearchPoiById, btnPoiGeneralSearchById);
            _commandPool.Register(Command.SearchPoiByName, btnPoiGeneralSearchByName);
            // added end

            _commandPool.Register(Command.SearchPhone, btnPhoneGeneralSearch);
            _commandPool.Register(Command.SearchPhoneByNo, btnPhoneSearchByNo);

            _commandPool.Register(Command.SearchSectionBoundary, btnLocationSectionBoundarySearch);
            _commandPool.Register(Command.AddSectionBoundary, btnLocationSectionBoundaryAdd);
            _commandPool.Register(Command.EditSectionBoundary, btnLocationSectionBoundaryEdit);
            _commandPool.Register(Command.DeleteSectionBoundary, btnLocationSectionBoundaryDelete);
            _commandPool.Register(Command.MoveSectionBoundary, btnLocationSectionBoundaryMove);
            _commandPool.Register(Command.AddSectionBoundaryVertex, btnLocationSectionBoundaryVertexAdd);
            _commandPool.Register(Command.MoveSectionBoundaryVertex, btnLocationSectionBoundaryVertexMove);
            _commandPool.Register(Command.DeleteSectionBoundaryVertex, btnLocationSectionBoundaryVertexDelete);
            _commandPool.Register(Command.ManageLocation, btnLocationManage);

            _commandPool.Register(Command.ManageAccessibility, btnUserAccessibilityManage); // noraini
            _commandPool.Register(Command.ManageAccessibilityPssword, btnUserAccessibilityChangePassword);
            _commandPool.Register(Command.VerifyWEB, btnVerifyWeb); // noraini

            //added by asyrul
            _commandPool.Register(Command.ShowIndex, btnShowIndex);
            _commandPool.Register(Command.SearchIndex, btnSearchIndex);

            // noraini - Aug 2021
            _commandPool.Register(Command.SearchJupemLot, btnSearchJupemLot);

            _commandPool.Register(Command.OnLayerToggle, btnOnLayerToggle);

            _commandPool.Register(Command.CreateWorkArea, btnCreateWA);
            //added by asyrul ends

            //added by noraini - Jan 2020 - CADU 2 AND
            _commandPool.Register(Command.SearchWorkArea, btnSearchWorkArea);
            _commandPool.Register(Command.VerifyWorkArea, btnVerifyWorkArea);
            _commandPool.Register(Command.DeleteWorkArea, btnDeleteWA);
            _commandPool.Register(Command.CompleteWorkArea, btnCompleteWA);
            _commandPool.Register(Command.CloseWorkArea, btnCloseWA);
            _commandPool.Register(Command.VerifyWorkArea2, btnVerifyWorkArea2);
            //added end

            //work area vertex
            _commandPool.Register(Command.AddWorkAreaVertex, btnWorkAreaVertexAdd);
            _commandPool.Register(Command.MoveWorkAreaVertex, btnWorkAreaVertexMove);
            _commandPool.Register(Command.DeleteWorkAreaVertex, btnWorkAreaVertexDelete);

			//Raster
			_commandPool.Register(Command.OpenRasterWarping, btnOpenRasterWarping);
			_commandPool.Register(Command.CloseRasterWarping, btnCloseRasterWarping);
			_commandPool.Register(Command.AddPoint, btnAddPoint);
			_commandPool.Register(Command.DeletePoint, btnDeletePoint);
			_commandPool.Register(Command.DeleteAllPoints, btnDeleteAllPoints);
			_commandPool.Register(Command.UpdateAlignment, btnUpdateAlignment);
			_commandPool.Register(Command.FitToDisplay, btnFitToDisplay);
			_commandPool.Register(Command.RegisterRaster, btnRegisterRaster);
			_commandPool.Register(Command.BuildPyramid, btnBuildPyramid);
			_commandPool.Register(Command.MoveRaster, btnMoveRaster);
			_commandPool.Register(Command.RotateClockwise, btnRotateClockwise);
			_commandPool.Register(Command.RotateAntiClockwise, btnRotateAntiClockwise);
			_commandPool.Register(Command.ScaleUp, btnScaleUp);
			_commandPool.Register(Command.ScaleDown, btnScaleDown);
			_commandPool.Register(Command.OpenRasterInPaint, btnOpenRasterInPaint);
			_commandPool.Register(Command.ExportRaster, btnExportRaster);

			// Map Tools
			_commandPool.RegisterMapTool(Command.NoTool, new NoTool());
            _commandPool.RegisterMapTool(Command.Select, new SelectTool());
            _commandPool.RegisterMapTool(Command.Pan, new PanTool());
            _commandPool.RegisterMapTool(Command.ZoomIn, new ZoomInTool());
            _commandPool.RegisterMapTool(Command.ZoomOut, new ZoomOutTool());
            _commandPool.RegisterMapTool(Command.Ruler, new RulerTool());

            _commandPool.RegisterMapTool(Command.DrawLine, new DrawLineTool());
            _commandPool.RegisterMapTool(Command.DrawCircle, new DrawCircleTool());
            _commandPool.RegisterMapTool(Command.DrawRectangle, new DrawRectangleTool());
            _commandPool.RegisterMapTool(Command.DrawPolygon, new DrawPolygonTool());

            _commandPool.RegisterMapTool(Command.AddStreet, new AddStreetTool());
            _commandPool.RegisterMapTool(Command.EditStreet, new EditStreetTool());
            _commandPool.RegisterMapTool(Command.DeleteStreet, new DeleteStreetTool());
            _commandPool.RegisterMapTool(Command.SplitStreet, new SplitStreetTool());
            _commandPool.RegisterMapTool(Command.UnionStreet, new UnionStreetTool());
            _commandPool.RegisterMapTool(Command.CopyStreet, new CopyStreetTool());
            _commandPool.RegisterMapTool(Command.FlipStreet, new FlipStreetTool());
            _commandPool.RegisterMapTool(Command.ShowStreetProperty, new ShowStreetPropertyTool());
            _commandPool.RegisterMapTool(Command.ReshapeStreet, new ReshapeStreetTool());
            // added by asyrul
            _commandPool.RegisterMapTool(Command.CopyStreetAttribute, new CopyStreetAttributeTool());
            // added end
            _commandPool.RegisterMapTool(Command.AddStreetVertex, new AddStreetVertexTool());
            _commandPool.RegisterMapTool(Command.MoveStreetVertex, new MoveStreetVertexTool());
            _commandPool.RegisterMapTool(Command.DeleteStreetVertex, new DeleteStreetVertexTool());
            _commandPool.RegisterMapTool(Command.AddStreetRestriction, new AddStreetRestrictionTool());
            _commandPool.RegisterMapTool(Command.DeleteStreetRestriction, new DeleteStreetRestrictionTool());
            _commandPool.RegisterMapTool(Command.MoveStreetRestriction, new MoveStreetRestrictionTool());
            _commandPool.RegisterMapTool(Command.ShowStreetRestrictionStreet, new ShowStreetRestrictionStreetTool());

            _commandPool.RegisterMapTool(Command.EditJunction, new EditJuntionTool());
            _commandPool.RegisterMapTool(Command.DeleteJunction, new DeleteJunctionTool());
            _commandPool.RegisterMapTool(Command.MoveJunction, new MoveJunctionTool());
            _commandPool.RegisterMapTool(Command.SplitJunction, new SplitJunctionTool());
            // added by asyrul
            _commandPool.RegisterMapTool(Command.ReassociateJunction, new ReassociateJunctionTool());
            // added end

            _commandPool.RegisterMapTool(Command.AddProperty, new AddPropertyTool());
            _commandPool.RegisterMapTool(Command.AddMultipleProperty, new AddPropertyMultipleTool());
            _commandPool.RegisterMapTool(Command.EditProperty, new EditPropertyTool());
            _commandPool.RegisterMapTool(Command.EditMultipleProperty, new EditPropertyMultipleTool());
            _commandPool.RegisterMapTool(Command.DeleteProperty, new DeletePropertyTool());
            _commandPool.RegisterMapTool(Command.ReassociateProperty, new ReassociatePropertyTool());
            _commandPool.RegisterMapTool(Command.MoveProperty, new MovePropertyTool());

            _commandPool.RegisterMapTool(Command.AddBuilding, new AddBuildingTool());
            _commandPool.RegisterMapTool(Command.EditBuilding, new EditBuildingTool());
            _commandPool.RegisterMapTool(Command.DeleteBuilding, new DeleteBuildingTool());
            _commandPool.RegisterMapTool(Command.MoveBuilding, new MoveBuildingTool());
            //added by asyrul
            _commandPool.RegisterMapTool(Command.ReassociateBuilding, new ReassociateBuildingTool());
            //added end

            _commandPool.RegisterMapTool(Command.AddBuildingGroup, new AddBuildingGroupTool());
            _commandPool.RegisterMapTool(Command.EditBuildingGroup, new EditBuildingGroupTool());
            _commandPool.RegisterMapTool(Command.DeleteBuildingGroup, new DeleteBuildingGroupTool());
            _commandPool.RegisterMapTool(Command.ReassociateBuildingGroup, new ReassociateBuildingGroupTool());
            _commandPool.RegisterMapTool(Command.MoveBuildingGroup, new MoveBuildingGroupTool());
            // added by asyrul
            _commandPool.RegisterMapTool(Command.AssociateBuildingToBuildingGroup, new AssociateBuildingToBuildingGroupTool());
            _commandPool.RegisterMapTool(Command.AssociateBuildingByDistance, new AssociateBuildingByDistanceTool());
            // added end

            _commandPool.RegisterMapTool(Command.DissociateBuildingToBuildingGroup, new DissociateBuildingToBuildingGroupTool());

            _commandPool.RegisterMapTool(Command.AddLandmark, new AddLandmarkTool());
            _commandPool.RegisterMapTool(Command.EditLandmark, new EditLandmarkTool());
            _commandPool.RegisterMapTool(Command.DeleteLandmark, new DeleteLandmarkTool());
            _commandPool.RegisterMapTool(Command.ReassociateLandmark, new ReassociateLandmarkTool());
            _commandPool.RegisterMapTool(Command.MoveLandmark, new MoveLandmarkTool());
            _commandPool.RegisterMapTool(Command.AddLandmarkBoundary, new AddLandmarkBoundaryTool());
            _commandPool.RegisterMapTool(Command.MoveLandmarkBoundary, new MoveLandmarkBoundaryTool());
            _commandPool.RegisterMapTool(Command.AddLandmarkBoundaryVertex, new AddLandmarkBoundaryVertexTool());
            _commandPool.RegisterMapTool(Command.MoveLandmarkBoundaryVertex, new MoveLandmarkBoundaryVertexTool());
            _commandPool.RegisterMapTool(Command.DeleteLandmarkBoundaryVertex, new DeleteLandmarkBoundaryVertexTool());

            _commandPool.RegisterMapTool(Command.EditPoi, new EditPoiTool());
            _commandPool.RegisterMapTool(Command.MergePoi, new MergePoiTool());
            _commandPool.RegisterMapTool(Command.ShowPoiStreet, new ShowPoiStreetTool());

            _commandPool.RegisterMapTool(Command.AddSectionBoundary, new AddSectionBoundaryTool());
            _commandPool.RegisterMapTool(Command.EditSectionBoundary, new EditSectionBoundaryTool());
            _commandPool.RegisterMapTool(Command.DeleteSectionBoundary, new DeleteSectionBoundaryTool());
            _commandPool.RegisterMapTool(Command.MoveSectionBoundary, new MoveSectionBoundaryTool());
            _commandPool.RegisterMapTool(Command.AddSectionBoundaryVertex, new AddSectionBoundaryVertexTool());
            _commandPool.RegisterMapTool(Command.MoveSectionBoundaryVertex, new MoveSectionBoundaryVertexTool());
            _commandPool.RegisterMapTool(Command.DeleteSectionBoundaryVertex, new DeleteSectionBoundaryVertexTool());

            //added by asyrul
            _commandPool.RegisterMapTool(Command.ShowIndex, new ShowIndexTool());
            _commandPool.RegisterMapTool(Command.SearchIndex, new SearchIndexTool());
            //added by asyrul ends

            //added by noraini
            //_commandPool.RegisterMapTool(Command.SearchWorkArea, new SearchWorkAreaTool());
            _commandPool.RegisterMapTool(Command.VerifyWorkArea, new VerifyWorkAreaTool());
            _commandPool.RegisterMapTool(Command.DeleteWorkArea, new DeleteWorkAreaTool());
            _commandPool.RegisterMapTool(Command.CompleteWorkArea, new CompleteWorkAreaTool());
            _commandPool.RegisterMapTool(Command.CloseWorkArea, new CloseWorkAreaTool());
            //_commandPool.RegisterMapTool(Command.VerifyWorkArea2, new VerifyWorkAreaTool2());
            //added end

            //work area vertex
            _commandPool.RegisterMapTool(Command.AddWorkAreaVertex, new AddWorkAreaVertexTool());
            _commandPool.RegisterMapTool(Command.MoveWorkAreaVertex, new MoveWorkAreaVertexTool());
            _commandPool.RegisterMapTool(Command.DeleteWorkAreaVertex, new DeleteWorkAreaVertexTool());

            //added by syafiq
            _commandPool.RegisterMapTool(Command.CreateWorkArea, new CreateWorkAreaTool());
            //added end

            //added by noraini
            _commandPool.RegisterMapTool(Command.SearchJupemLot, new SearchJupemLotTool());

			//Raster
			_commandPool.RegisterMapTool(Command.AddPoint, new AddPoint());
			_commandPool.RegisterMapTool(Command.DeletePoint, new DeletePoint());
			_commandPool.RegisterMapTool(Command.DeleteAllPoints, new DeleteAllPoints());
			_commandPool.RegisterMapTool(Command.UpdateAlignment, new UpdateAlignment());
			_commandPool.RegisterMapTool(Command.FitToDisplay, new FitToDisplay());
			_commandPool.RegisterMapTool(Command.RegisterRaster, new RegisterRaster());
			_commandPool.RegisterMapTool(Command.BuildPyramid, new BuildPyramid());
			_commandPool.RegisterMapTool(Command.MoveRaster, new MoveRaster());
			_commandPool.RegisterMapTool(Command.RotateClockwise, new RotateClockwise());
			_commandPool.RegisterMapTool(Command.RotateAntiClockwise, new RotateAntiClockwise());
			_commandPool.RegisterMapTool(Command.ScaleUp, new ScaleUp());
			_commandPool.RegisterMapTool(Command.ScaleDown, new ScaleDown());
			_commandPool.RegisterMapTool(Command.ExportRaster, new ExportRaster());

			// all map tools must be registered as maptool radio group, 
			// only one tool can be active at a time
			_commandPool.RegisterRadio(Command.MapTool,
                                            Command.NoTool,
                                            Command.Select,
                                            Command.Pan,
                                            Command.ZoomIn,
                                            Command.ZoomOut,
                                            Command.Ruler,
                                            Command.DrawLine,
                                            Command.DrawCircle,
                                            Command.DrawRectangle,
                                            Command.DrawPolygon,
                                            Command.AddStreet,
                                            Command.EditStreet,
                                            Command.DeleteStreet,
                                            Command.SplitStreet,
                                            Command.UnionStreet,
                                            Command.CopyStreet,
                                            Command.FlipStreet,
                                            Command.ShowStreetProperty,
                                            Command.ReshapeStreet,
                                            //added by asyrul
                                            Command.CopyStreetAttribute,
                                            //added end
                                            Command.AddStreetVertex,
                                            Command.MoveStreetVertex,
                                            Command.DeleteStreetVertex,
                                            Command.AddStreetRestriction,
                                            Command.DeleteStreetRestriction,
                                            Command.MoveStreetRestriction,
                                            Command.ShowStreetRestrictionStreet,
                                            Command.EditJunction,
                                            Command.DeleteJunction,
                                            Command.MoveJunction,
                                            Command.SplitJunction,
                                            //added by asyrul
                                            Command.ReassociateJunction,
                                            //added end
                                            Command.AddProperty,
                                            Command.AddMultipleProperty,
                                            Command.EditProperty,
                                            Command.EditMultipleProperty,
                                            Command.DeleteProperty,
                                            Command.ReassociateProperty,
                                            Command.MoveProperty,
                                            Command.AddBuilding,
                                            Command.EditBuilding,
                                            Command.DeleteBuilding,
                                            Command.MoveBuilding,
                                            //added by asyrul
                                            Command.ReassociateBuilding,
                                            //added end
                                            Command.AddBuildingGroup,
                                            Command.EditBuildingGroup,
                                            Command.DeleteBuildingGroup,
                                            Command.ReassociateBuildingGroup,
                                            //added by asyrul
                                            Command.AssociateBuildingToBuildingGroup,
                                            Command.AssociateBuildingByDistance,
                                            //added end
                                            Command.DissociateBuildingToBuildingGroup,
                                            Command.MoveBuildingGroup,
                                            Command.AddLandmark,
                                            Command.EditLandmark,
                                            Command.DeleteLandmark,
                                            Command.ReassociateLandmark,
                                            Command.MoveLandmark,
                                            Command.AddLandmarkBoundary,
                                            Command.DeleteLandmarkBoundary,
                                            Command.MoveLandmarkBoundary,
                                            Command.AddLandmarkBoundaryVertex,
                                            Command.MoveLandmarkBoundaryVertex,
                                            Command.DeleteLandmarkBoundaryVertex,
                                            Command.EditPoi,
                                            Command.MergePoi,
                                            Command.ShowPoiStreet,
                                            Command.AddSectionBoundary,
                                            Command.EditSectionBoundary,
                                            Command.DeleteSectionBoundary,
                                            Command.MoveSectionBoundary,
                                            Command.AddSectionBoundaryVertex,
                                            Command.MoveSectionBoundaryVertex,
                                            Command.DeleteSectionBoundaryVertex,
                                            Command.ShowIndex,
                                            Command.SearchIndex,
                                            Command.SearchJupemLot,
                                            Command.SearchWorkArea,
                                            Command.CreateWorkArea,
                                            Command.DeleteWorkArea,
                                            Command.CompleteWorkArea,
                                            Command.CloseWorkArea,
                                            Command.AddWorkAreaVertex,
                                            Command.MoveWorkAreaVertex,
                                            Command.DeleteWorkAreaVertex,
											Command.AddPoint,
											Command.DeletePoint,
											Command.DeleteAllPoints,
											Command.UpdateAlignment,
											Command.FitToDisplay,
											Command.RegisterRaster,
											Command.BuildPyramid,
											Command.MoveRaster,
											Command.RotateClockwise,
											Command.RotateAntiClockwise,
											Command.ScaleUp,
											Command.ScaleDown,
											Command.ExportRaster);

            _commandPool.RegisterGroup(Command.DocumentCommand,
                                            Command.NoTool,
                                            Command.Select,
                                            Command.Pan,
                                            Command.ZoomIn,
                                            Command.ZoomOut,
                                            Command.ZoomToSelection,
                                            Command.FullExtent,
                                            Command.PreviousExtent,
                                            Command.NextExtent,
                                            Command.Refresh,
                                            Command.ClearSelection,
                                            Command.Ruler,
                                            Command.ClearDrawings,
                                            Command.DrawLine,
                                            Command.DrawCircle,
                                            Command.DrawRectangle,
                                            Command.DrawPolygon,
                                            Command.AddStreet,
                                            Command.EditStreet,
                                            Command.DeleteStreet,
                                            Command.SplitStreet,
                                            Command.UnionStreet,
                                            Command.CopyStreet,
                                            Command.FlipStreet,
                                            Command.ShowStreetProperty,
                                            Command.ReshapeStreet,
                                            //added by asyrul
                                            Command.CopyStreetAttribute,
                                            //added end
                                            Command.AddStreetVertex,
                                            Command.MoveStreetVertex,
                                            Command.DeleteStreetVertex,
                                            Command.AddStreetRestriction,
                                            Command.DeleteStreetRestriction,
                                            Command.MoveStreetRestriction,
                                            Command.ShowStreetRestrictionStreet,
                                            Command.EditJunction,
                                            Command.DeleteJunction,
                                            Command.MoveJunction,
                                            Command.SplitJunction,
                                            //added by asyrul
                                            Command.ReassociateJunction,
                                            //added end
                                            Command.AddProperty,
                                            Command.AddMultipleProperty,
                                            Command.EditProperty,
                                            Command.EditMultipleProperty,
                                            Command.DeleteProperty,
                                            Command.ReassociateProperty,
                                            Command.MoveProperty,
                                            Command.AddBuilding,
                                            Command.EditBuilding,
                                            Command.DeleteBuilding,
                                            Command.MoveBuilding,
                                            //added by asyrul
                                            Command.ReassociateBuilding,
                                            //added end
                                            Command.AddBuildingGroup,
                                            Command.EditBuildingGroup,
                                            Command.DeleteBuildingGroup,
                                            Command.ReassociateBuildingGroup,
                                            //added by asyrul
                                            Command.AssociateBuildingToBuildingGroup,
                                            Command.AssociateBuildingByDistance,
                                            //added end
                                            Command.DissociateBuildingToBuildingGroup,
                                            Command.MoveBuildingGroup,
                                            Command.AddLandmark,
                                            Command.EditLandmark,
                                            Command.DeleteLandmark,
                                            Command.ReassociateLandmark,
                                            Command.MoveLandmark,
                                            Command.AddLandmarkBoundary,
                                            Command.DeleteLandmarkBoundary,
                                            Command.MoveLandmarkBoundary,
                                            Command.AddLandmarkBoundaryVertex,
                                            Command.MoveLandmarkBoundaryVertex,
                                            Command.DeleteLandmarkBoundaryVertex,
                                            Command.EditPoi,
                                            Command.MergePoi,
                                            Command.ShowPoiStreet,
                                            Command.SearchPhone,
                                            Command.SearchPhoneByNo,
                                            Command.AddSectionBoundary,
                                            Command.EditSectionBoundary,
                                            Command.DeleteSectionBoundary,
                                            Command.MoveSectionBoundary,
                                            Command.AddSectionBoundaryVertex,
                                            Command.MoveSectionBoundaryVertex,
                                            Command.DeleteSectionBoundaryVertex,
                                            Command.ShowIndex,
                                            Command.SearchIndex,
                                            Command.SearchJupemLot,
                                            Command.SearchWorkArea,
                                            Command.CreateWorkArea,
                                            Command.DeleteWorkArea,
                                            Command.CompleteWorkArea,
                                            Command.CloseWorkArea,
                                            Command.AddWorkAreaVertex,
                                            Command.MoveWorkAreaVertex,
                                            Command.DeleteWorkAreaVertex,
											Command.AddPoint,
											Command.DeletePoint,
											Command.DeleteAllPoints,
											Command.UpdateAlignment,
											Command.FitToDisplay,
											Command.RegisterRaster,
											Command.BuildPyramid,
											Command.MoveRaster,
											Command.RotateClockwise,
											Command.RotateAntiClockwise,
											Command.ScaleUp,
											Command.ScaleDown,
											Command.ExportRaster);

			_commandPool.RegisterGroup(Command.SegmentedCommand,
                                                Command.Select,
                                                Command.SearchStreet,
                                                Command.AddStreet,
                                                Command.EditStreet,
                                                Command.DeleteStreet,
                                                Command.SplitStreet,
                                                Command.UnionStreet,
                                                Command.CopyStreet,
                                                Command.FlipStreet,
                                                Command.ShowStreetProperty,
                                                Command.ReshapeStreet,
                                                //added by asyrul
                                                Command.CopyStreetAttribute,
                                                //added end
                                                Command.AddStreetVertex,
                                                Command.MoveStreetVertex,
                                                Command.DeleteStreetVertex,
                                                Command.SearchStreetRestriction,
                                                Command.AddStreetRestriction,
                                                Command.DeleteStreetRestriction,
                                                Command.MoveStreetRestriction,
                                                Command.ShowStreetRestrictionStreet,
                                                Command.SearchJunction,
                                                Command.EditJunction,
                                                Command.DeleteJunction,
                                                Command.MoveJunction,
                                                Command.SplitJunction,
                                                //added by asyrul
                                                Command.ReassociateJunction,
                                                //added end
                                                Command.SearchProperty,
                                                Command.AddProperty,
                                                Command.AddMultipleProperty,
                                                Command.EditProperty,
                                                Command.EditMultipleProperty,
                                                Command.DeleteProperty,
                                                Command.ReassociateProperty,
                                                Command.MoveProperty,
                                                Command.SearchFloor,
                                                Command.SearchBuilding,
                                                Command.AddBuilding,
                                                Command.EditBuilding,
                                                Command.DeleteBuilding,
                                                Command.MoveBuilding,
                                                //added by asyrul
                                                Command.ReassociateBuilding,
                                                //added end
                                                Command.SearchBuildingGroup,
                                                Command.AddBuildingGroup,
                                                Command.EditBuildingGroup,
                                                Command.DeleteBuildingGroup,
                                                Command.ReassociateBuildingGroup,
                                                //added by asyrul
                                                Command.AssociateBuildingToBuildingGroup,
                                                Command.AssociateBuildingByDistance,
                                                //added end
                                                Command.DissociateBuildingToBuildingGroup,
                                                Command.MoveBuildingGroup,
                                                Command.SearchMultiStorey,
                                                Command.SearchLandmark,
                                                Command.AddLandmark,
                                                Command.EditLandmark,
                                                Command.DeleteLandmark,
                                                Command.ReassociateLandmark,
                                                Command.MoveLandmark,
                                                Command.SearchLandmarkBoundary,
                                                Command.AddLandmarkBoundary,
                                                Command.DeleteLandmarkBoundary,
                                                Command.MoveLandmarkBoundary,
                                                Command.AddLandmarkBoundaryVertex,
                                                Command.MoveLandmarkBoundaryVertex,
                                                Command.DeleteLandmarkBoundaryVertex,
                                                Command.SearchPoi,
                                                Command.EditPoi,
                                                Command.MergePoi,
                                                Command.ShowPoiStreet,
                                                Command.SearchPhone,
                                                Command.SearchPhoneByNo,
                                                Command.SearchSectionBoundary,
                                                Command.AddSectionBoundary,
                                                Command.EditSectionBoundary,
                                                Command.DeleteSectionBoundary,
                                                Command.MoveSectionBoundary,
                                                Command.AddSectionBoundaryVertex,
                                                Command.MoveSectionBoundaryVertex,
                                                Command.DeleteSectionBoundaryVertex,
                                                Command.OpenWork,
                                                Command.ShowIndex,
                                                Command.SearchWorkArea,
                                                Command.VerifyWorkArea,
                                                Command.VerifyWorkArea2,
                                                Command.CreateWorkArea,
                                                Command.DeleteWorkArea,
                                                Command.CompleteWorkArea,
                                                Command.CloseWorkArea,
                                                Command.AddWorkAreaVertex,
                                                Command.MoveWorkAreaVertex,
                                                Command.DeleteWorkAreaVertex,
												Command.AddPoint,
												Command.DeletePoint,
												Command.DeleteAllPoints,
												Command.UpdateAlignment,
												Command.FitToDisplay,
												Command.RegisterRaster,
												Command.BuildPyramid,
												Command.MoveRaster,
												Command.RotateClockwise,
												Command.RotateAntiClockwise,
												Command.ScaleUp,
												Command.ScaleDown,
												Command.ExportRaster,
                                                Command.VerifyWEB);

			_commandPool.RegisterGroup(Command.UserCommand,
                                         Command.SearchStreet,
                                         Command.AddStreet,
                                         Command.EditStreet,
                                         Command.DeleteStreet,
                                         Command.SplitStreet,
                                         Command.UnionStreet,
                                         Command.CopyStreet,
                                         Command.FlipStreet,
                                         Command.ShowStreetProperty,
                                         Command.ReshapeStreet,
                                         //added by asyrul
                                         //Command.CopyStreetAttribute, //need to add this in usergroup functions database
                                         //added end
                                         Command.AddStreetVertex,
                                         Command.MoveStreetVertex,
                                         Command.DeleteStreetVertex,
                                         Command.SearchStreetRestriction,
                                         Command.AddStreetRestriction,
                                         Command.DeleteStreetRestriction,
                                         Command.MoveStreetRestriction,
                                         Command.ShowStreetRestrictionStreet,
                                         Command.SearchJunction,
                                         Command.EditJunction,
                                         Command.DeleteJunction,
                                         Command.MoveJunction,
                                         Command.SplitJunction,
                                         Command.SearchProperty,
                                         Command.AddProperty,
                                         Command.AddMultipleProperty,
                                         Command.EditProperty,
                                         Command.EditMultipleProperty,
                                         Command.DeleteProperty,
                                         Command.MoveProperty,
                                         Command.SearchFloor,
                                         Command.SearchBuilding,
                                         Command.AddBuilding,
                                         Command.EditBuilding,
                                         Command.DeleteBuilding,
                                         Command.MoveBuilding,
                                         Command.SearchBuildingGroup,
                                         Command.AddBuildingGroup,
                                         Command.EditBuildingGroup,
                                         Command.DeleteBuildingGroup,
                                         Command.ReassociateBuildingGroup,
                                         Command.MoveBuildingGroup,
                                         Command.SearchMultiStorey,
                                         Command.SearchLandmark,
                                         Command.AddLandmark,
                                         Command.EditLandmark,
                                         Command.DeleteLandmark,
                                         Command.ReassociateLandmark,
                                         Command.MoveLandmark,
                                         Command.SearchLandmarkBoundary,
                                         Command.AddLandmarkBoundary,
                                         Command.DeleteLandmarkBoundary,
                                         Command.MoveLandmarkBoundary,
                                         Command.AddLandmarkBoundaryVertex,
                                         Command.MoveLandmarkBoundaryVertex,
                                         Command.DeleteLandmarkBoundaryVertex,
                                         Command.SearchPoi,
                                         Command.EditPoi,
                                         Command.MergePoi,
                                         Command.ShowPoiStreet,
                                         Command.SearchPhone,
                                         //Command.SearchPhoneByNo,
                                         Command.SearchSectionBoundary,
                                         Command.AddSectionBoundary,
                                         Command.EditSectionBoundary,
                                         Command.DeleteSectionBoundary,
                                         Command.MoveSectionBoundary,
                                         Command.AddSectionBoundaryVertex,
                                         Command.MoveSectionBoundaryVertex,
                                         Command.DeleteSectionBoundaryVertex,
                                         Command.ManageLocation,
                                         Command.ManageAccessibility, // noraini - enable this buttton
                                         Command.VerifyWEB, // noraini - enable this buttton
                                         Command.OpenWork,
                                         // noraini ali - Jun 2020 - enable button use by ADM_FUNCTION
                                         Command.SearchWorkArea,
                                         Command.VerifyWorkArea,
                                         Command.VerifyWorkArea2,
                                         Command.CreateWorkArea,
                                         Command.DeleteWorkArea,
                                         Command.CompleteWorkArea,
                                         Command.CloseWorkArea,
                                         Command.AddWorkAreaVertex,
                                         Command.MoveWorkAreaVertex,
                                         Command.DeleteWorkAreaVertex);
                                         // end added by noraini
                                         //Command.ShowIndex,
                                         //Command.SearchIndex);
        }

        private void InitSegments(IEnumerable<SegmentName> segments)
        {
            cbHomeGeneralSegment.DropDownItems.Clear();
            if (segments.Contains(SegmentName.AS))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("AS - Kedah & Perlis   ") { SegmentName = SegmentName.AS });
            }
            if (segments.Contains(SegmentName.JH))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("JH - Johor   ") { SegmentName = SegmentName.JH });
            }
            if (segments.Contains(SegmentName.JP))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("JP - Perak   ") { SegmentName = SegmentName.JP });
            }
            if (segments.Contains(SegmentName.KG))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("KG - Sarawak   ") { SegmentName = SegmentName.KG });
            }
            if (segments.Contains(SegmentName.KK))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("KK - Sabah & Labuan   ") { SegmentName = SegmentName.KK });
            }
            if (segments.Contains(SegmentName.KN))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("KN - Pahang   ") { SegmentName = SegmentName.KN });
            }
            if (segments.Contains(SegmentName.KV))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("KV - Kuala Lumpur & Selangor   ") { SegmentName = SegmentName.KV });
            }
            if (segments.Contains(SegmentName.MK))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("MK - Melaka & Negeri Sembilan  ") { SegmentName = SegmentName.MK });
            }
            if (segments.Contains(SegmentName.PG))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("PG - Penang   ") { SegmentName = SegmentName.PG });
            }
            if (segments.Contains(SegmentName.TG))
            {
                cbHomeGeneralSegment.DropDownItems.Add(new SegmentButton("TG - Terengganu & Kelantan   ") { SegmentName = SegmentName.TG });
            }
            if (cbHomeGeneralSegment.DropDownItems.Count > 0)
            {
                cbHomeGeneralSegment.SelectedItem = cbHomeGeneralSegment.DropDownItems[0];
            }
        }

        private void InitScales()
        {
            cbScale.DropDownItems.Add(new RibbonButton() { Text = "1:500" });
            cbScale.DropDownItems.Add(new RibbonButton() { Text = "1:1000" });
            cbScale.DropDownItems.Add(new RibbonButton() { Text = "1:3000" });
            cbScale.DropDownItems.Add(new RibbonButton() { Text = "1:10000" });
        }

        /// <summary>
        /// Close all documents
        /// </summary>
        private void CloseDocuments()
        {
            foreach (IDockContent dockContent in dockPanel.DocumentsToArray())
            {
                if (dockContent is Document)
                {
                    dockContent.DockHandler.Close();
                }
            }
        }

        private void cbHomeGeneralSegment_DropDownItemClicked(object sender, RibbonItemEventArgs e)
        {
            _commandPool.OnSegmentChanged(SelectedSegment.Value);
            _toolboxManager.OnSegmentChanged(SelectedSegment.Value);
            if (ActiveMap != null)
            {
                if (SelectedSegment.HasValue)
                {
                    VersionName versionName;
                    switch (SelectedSegment.Value)
                    {
                        case SegmentName.AS:
                            versionName = VersionName.AS;
                            break;
                        case SegmentName.JH:
                            versionName = VersionName.JH;
                            break;
                        case SegmentName.JP:
                            versionName = VersionName.JP;
                            break;
                        case SegmentName.KG:
                            versionName = VersionName.KG;
                            break;
                        case SegmentName.KK:
                            versionName = VersionName.KK;
                            break;
                        case SegmentName.KN:
                            versionName = VersionName.KN;
                            break;
                        case SegmentName.KV:
                            versionName = VersionName.KV;
                            break;
                        case SegmentName.MK:
                            versionName = VersionName.MK;
                            break;
                        case SegmentName.PG:
                            versionName = VersionName.PG;
                            break;
                        case SegmentName.TG:
                            versionName = VersionName.TG;
                            break;
                        default:
                            throw new Exception("Invalid segment.");
                    }
                    Session.Current.OpenCadu(versionName, true);
                }
            }
            UpdateMenu();
        }

        private void OnFileOpenMap(object sender, EventArgs e)
        {
            using (OpenMapForm openMapForm = new OpenMapForm())
            {
                if (openMapForm.ShowDialog(this) == DialogResult.OK) //goes inside once user selects a segment
                {
                    if (ActiveMap != null) //fails this test
                    {
                        if (Session.Current.HasEdits())
                        {
                            using (QuestionMessageBox box = new QuestionMessageBox())
                            {
                                box.SetCaption("Editing")
                                   .SetText("Do you want to discard current session editing and start a new map?");
                                DialogResult result = box.Show(this);

                                using (new WaitCursor())
                                {
                                    switch (result)
                                    {
                                        case DialogResult.Yes:
                                            {
                                                Session.Current.DiscardEdits();
                                                break;
                                            }
                                        case DialogResult.No:
                                        case DialogResult.Cancel:
                                        default:
                                            return;
                                    }
                                }
                            }
                        }

                        ActiveMap.DockHandler.Close();
                    }

                    using (new WaitCursor())
                    {
                        List<ILayer> layers = new List<ILayer>();

                        layers.Add(MapLayerFactory.CreateCountry(Session.Current.OpenCadu(VersionName.DEFAULT, false)));

                        // added by noraini ali Dec 2019 - CADU2 AND
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            if (openMapForm.Segments.Contains(SegmentName.AS))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.AS, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.AS));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.TG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.TG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.TG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.PG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.PG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.PG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.MK))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.MK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.MK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KV))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.KV, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KV));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KN))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.KN, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KN));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KK))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.KK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.KG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JP))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.JP, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JP));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JH))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupAND(Session.Current.OpenCadu(VersionName.JH, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JH));
                            }
                        }
                        else if (Session.User.GetGroup().Name == "SUPERVISOR")
                        {
                            if (openMapForm.Segments.Contains(SegmentName.AS))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.AS, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.AS));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.TG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.TG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.TG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.PG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.PG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.PG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.MK))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.MK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.MK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KV))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.KV, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KV));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KN))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.KN, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KN));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KK))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.KK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KG))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.KG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JP))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.JP, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JP));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JH))
                            {
                                layers.Add(MapLayerFactory.CreateStateUserGroupSupervisor(Session.Current.OpenCadu(VersionName.JH, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JH));
                            }
                        } // end added CADU2 AND - Group SUPERVISOR
                        else
                        {
                            if (openMapForm.Segments.Contains(SegmentName.TG))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.TG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.TG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.PG))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.PG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.PG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.MK))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.MK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.MK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KV))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.KV, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KV));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KN))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.KN, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KN));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KK))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.KK, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KK));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.KG))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.KG, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.KG));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JP))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.JP, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JP));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.JH))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.JH, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.JH));
                            }
                            if (openMapForm.Segments.Contains(SegmentName.AS))
                            {
                                layers.Add(MapLayerFactory.CreateState(Session.Current.OpenCadu(VersionName.AS, false), Session.Current.OpenClb(StringVersionName.DEFAULT), SegmentName.AS));
                            }
                        }


                        GeoMapDocument document = new GeoMapDocument();
                        document.SetMapName("Malaysia");
                        document.ContextMenuStrip = menuDocument;
                        document.SelectionChanged += new EventHandler<SelectionChangedEventArgs>(MapDocument_SelectionChanged);
                        document.MouseMoved += new EventHandler<MouseMovedEventArgs>(MapDocument_MouseMoved);
                        document.ScreenDrawn += new EventHandler<ScreenDrawnEventArgs>(MapDocument_ScreenDrawn);
                        document.ExtendUpdated += new EventHandler<ExtentUpdatedEventArgs>(MapDocument_ExtendUpdated);
                        document.AddLayers(layers);

                        document.Show(dockPanel);

                        InitSegments(openMapForm.Segments);

                        // added by noraini ali - Jun 2020 Cadu2 AND - Display User Name & Group
                        txtUserName.TextBoxText = Session.User.Name;
                        txtUserGroup.TextBoxText = Session.User.GetGroup().Name;
                        // end

                        _commandPool.SetMapTool(Command.NoTool);
                    }
                }
            }
        }

        private void OnFileOpenWorkOrder(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            using (NewItemForm form = new NewItemForm(SelectedSegment.Value))
            {
                form.ShowDialog(this);
            }
        }

        private void OnFileOpenShape(object sender, EventArgs e)
        {
            DialogResult result = openShapeDialog.ShowDialog(this);
            if (result != DialogResult.OK)
            {
                return;
            }
            using (new WaitCursor())
            {
                string path = Path.GetDirectoryName(openShapeDialog.FileName);
                string file = Path.GetFileName(openShapeDialog.FileName);
                ActiveMap.AddShape(path, file);
            }
        }

        private void OnFileOpenGdb(object sender, EventArgs e)
        {
            DialogResult result = openGdbDialog.ShowDialog(this);
            if (result != DialogResult.OK)
            {
                return;
            }
            using (new WaitCursor())
            {
                ActiveMap.AddGdb(openGdbDialog.SelectedPath);
            }
        }

        private void OnFileOpenRaster(object sender, EventArgs e)
        {
            DialogResult result = openRasterDialog.ShowDialog(this);
            if (result != DialogResult.OK)
            {
                return;
            }
            using (new WaitCursor())
            {
                string path = Path.GetDirectoryName(openRasterDialog.FileName);
                string file = Path.GetFileName(openRasterDialog.FileName);
                ActiveMap.AddRaster(path, file);
            }
        }

        private void OnFileOpenGPSCadToShp(object sender, EventArgs e)
        {
            DialogResult result = openGPSDialog.ShowDialog(this);
            if (result != DialogResult.OK)
            {
                return;
            }
            using (new WaitCursor())
            {
                string path = Path.GetDirectoryName(openGPSDialog.FileName);
                string file = Path.GetFileName(openGPSDialog.FileName);
                ActiveMap.ConvertGPSCadToShp(path, file);
            }
        }

        private void OnFileSave(object sender, EventArgs e)
        {
            if (!Session.Current.Cadu.HasEdits())
            {
                throw new Exception("Version has no edits.");
            }

            using (QuestionMessageBox box = new QuestionMessageBox())
            {
                box.SetCaption("Save ( {0} )", SegmentFullName)
                   .SetText("Do you want to save the changes you made to {0}?", SegmentFullName);
                DialogResult result = box.Show(this);

                using (new WaitCursor())
                {
                    switch (result)
                    {
                        case DialogResult.Yes:
                            {
                                Session.Current.Cadu.Save(true);
                                Session.Current.Cadu.RefreshVersion();
                                ActiveMap.RefreshMap();
                                break;
                            }
                        case DialogResult.No:
                            {
                                Session.Current.Cadu.Save(false);
                                Session.Current.Cadu.RefreshVersion();
                                ActiveMap.RefreshMap();
                                break;
                            }
                        case DialogResult.Cancel:
                        default:
                            break;
                    }
                }
            }
        }

        private void OnFileOptions(object sender, EventArgs e)
        {
            using (OptionForm optionForm = new OptionForm())
            {
                optionForm.ShowDialog(this);
            }
        }

        private void OnFileExit(object sender, EventArgs e)
        {
            Close();
        }

        private void OnEditUndo(object sender, EventArgs e)
        {
            if (!Session.Current.Cadu.HasUndos())
            {
                throw new Exception("Version has no undos.");
            }
            using (new WaitCursor())
            {
                Session.Current.Cadu.Undo();
            }
        }

        private void OnEditRedo(object sender, EventArgs e)
        {
            if (!Session.Current.Cadu.HasRedos())
            {
                throw new Exception("Version has no redos.");
            }
            using (new WaitCursor())
            {
                Session.Current.Cadu.Redo();
            }
        }

        private void OnMapNoTool(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.NoTool);
        }

        private void OnMapSelect(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.Select);
        }

        private void OnMapPan(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.Pan);
        }

        private void OnMapZoomIn(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ZoomIn);
        }

        private void OnMapZoomOut(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ZoomOut);
        }

        private void OnMapZoomToSelection(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.ZoomToSelection();
        }

        private void OnMapFullExtent(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.FullExtent();
        }

        private void OnMapPreviousExtent(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.UndoExtent();
        }

        private void OnMapNextExtent(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.RedoExtent();
        }

        private void OnMapRefresh(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.RefreshMap();
        }

        private void OnMapClearSelection(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            using (new WaitCursor())
            {
                ActiveMap.ClearSelection();
                ActiveMap.RefreshSelection();
            }

            // noraini ali - OKT 2020
            //ActiveMap.HighlightClearNew();
            ActiveMap.RefreshMap();
        }

        private void OnMapRuler(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.Ruler);
        }

        // added by asyrul
        private void  LayerToggle(object sender, EventArgs e)
        {
            MessageBox.Show("Activating All Layer Toggle!");
            //List<ILayer> layers = new List<ILayer>();

            if (!SelectedSegment.HasValue)
            {
                return;
            }
            using (new WaitCursor())
            {
                //foreach (ILayer item in layers)
                //{
                //    item.Visible = true;
                //}
                
                _viewManager.GetView<LayerView>().Activate();
                ((LayerView)_viewManager.GetView<LayerView>()).SelectedLayerChanged += new EventHandler<SelectedLayerChangedEventArgs>(LayerView_SelectedLayerChanged);
                //form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                //form.ShowDialog(this);
                //form.ShowMap -= (SearchForm_ShowMap);
            }
        }

        // added end

        private void goToToolbox_ZoomTo(object sender, GoToEventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.AddMarker(e.X, e.Y);
            ActiveMap.ZoomToPoint(e.X, e.Y);
        }

        private void goToToolbox_PanTo(object sender, GoToEventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.AddMarker(e.X, e.Y);
            ActiveMap.PanToPoint(e.X, e.Y);
        }

        private void goToToolbox_ClearMarker(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.RemoveMarker();
            ActiveMap.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
        }

        private void cbScale_DropDownItemClicked(object sender, RibbonItemEventArgs e)
        {
            string scaleStr = cbScale.TextBoxText.Trim();
            if (string.IsNullOrEmpty(scaleStr))
            {
                return;
            }
            ScaleChanged(scaleStr);
        }

        private void cbScale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
            {
                return;
            }
            string scaleStr = cbScale.TextBoxText.Trim();
            if (string.IsNullOrEmpty(scaleStr))
            {
                return;
            }
            ScaleChanged(scaleStr);
        }

        private void cbMenuScale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
            {
                return;
            }
            string scaleStr = cbMenuScale.Text.Trim();
            if (string.IsNullOrEmpty(scaleStr))
            {
                return;
            }
            ScaleChanged(scaleStr);
        }

        private void ScaleChanged(string scaleStr)
        {
            if (Regex.IsMatch(scaleStr, @"^1\s*:\s*\d+[.]?\d*$", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase))
            {
                scaleStr = Regex.Replace(scaleStr, @"^(1\s*:\s*)", "", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            }

            double scaleDouble;
            if (!double.TryParse(scaleStr, out scaleDouble))
            {
                return;
            }

            ActiveMap.SetScale(scaleDouble);
        }

        private void OnToolboxGoTo(object sender, EventArgs e)
        {
            _toolboxManager.GetView<GoToToolbox>().Show(this);
        }

        private void OnWindowLayer(object sender, EventArgs e)
        {
            _viewManager.GetView<LayerView>().Activate();
        }

        private void OnWindowOutput(object sender, EventArgs e)
        {
            _viewManager.GetView<OutputView>().Activate();
        }

        private void OnWindowSelections_Click(object sender, EventArgs e)
        {
            _viewManager.GetView<SelectionsView>().Activate();
        }

        private void dockPanel_ActiveDocumentChanged(object sender, EventArgs e)
        {
            UpdateMenu();
            _commandPool.OnDocumentChanged(ActiveMap);
            _viewManager.OnActiveDocumentChanged(ActiveMap);
        }

        private void CommandPool_ToolReported(object sender, ToolReportedEventArgs e)
        {
            if (e == null)
            {
                return;
            }
            AppendMessage(e.Message);
        }

        private void CommandPool_ToolStepReported(object sender, ToolStepReportedEventArgs e)
        {
            if (e == null)
            {
                return;
            }
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.Step(e.Message);
        }

        private void AppendMessage(string format, params object[] args)
        {
            AppendMessage(string.Format(format, args));
        }

        private void AppendMessage(string message)
        {
            OutputView outputView = (OutputView)_viewManager.GetView<OutputView>();
            outputView.AppendMessage(message);
        }

        private void MapDocument_MouseMoved(object sender, MouseMovedEventArgs e)
        {
            lblCoordinateText.Text = (e == null) ? "N/A" : string.Format("{0:0.0000}, {1:0.0000}", e.X, e.Y);
        }

        private void MapDocument_ScreenDrawn(object sender, ScreenDrawnEventArgs e)
        {
            lblStatus.Text = (e.IsDrawing) ? "Drawing Map..." : "Ready";
        }

        private void MapDocument_ExtendUpdated(object sender, ExtentUpdatedEventArgs e)
        {
            UpdateExtentMenu();
            lblScaleText.Text = (e == null) ? "N/A" : string.Format("1:{0:0.0000}", e.MapScale);
            cbScale.TextBoxText = (e == null) ? string.Empty : string.Format("1:{0:0.0000}", e.MapScale);
            cbMenuScale.Text = (e == null) ? string.Empty : string.Format("1:{0:0.0000}", e.MapScale);
            lblUnitText.Text = (e == null) ? "Unknown" : string.Format("{0}", e.MapUnit);
        }

        private void MapDocument_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateSelectionMenu();
            SelectionsView view = (SelectionsView)_viewManager.GetView<SelectionsView>();
            view.OnSelectionChanged(e.Selections);
            lblSelectionText.Text = (e == null) ? "0" : e.Count.ToString();
        }

        private void LayerView_SelectedLayerChanged(object sender, SelectedLayerChangedEventArgs e)
        {
            SelectedLayer = e.SelectedLayer;
            UpdateEditorMenu();
            _commandPool.OnSelectedLayerChanged(e.SelectedLayer);
        }

        #region Editor Menu

        private void btnEditorDrawLine_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DrawLine);
        }

        private void btnEditorDrawCircle_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DrawCircle);
        }

        private void btnEditorDrawRectangle_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DrawRectangle);
        }

        private void btnEditorDrawPolygon_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DrawPolygon);
        }

        private void btnEditorDrawClearDrawings_Click(object sender, EventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }
            ActiveMap.ClearGraphics();
            ActiveMap.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
        }

        #endregion

        #region Street Menu

        private void btnStreetGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            //using (StreetSearchForm form = new StreetSearchForm(SelectedSegment.Value))
            //{
            //    form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
            //    form.ShowDialog(this);
            //    form.ShowMap -= (SearchForm_ShowMap);
            //}
            _commandPool.SetMapTool(Command.NoTool);
            // using new form
            using (StreetSearchForm2 form = new StreetSearchForm2(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            } 
        }

        // added by asyrul
        private void btnStreetGeneralSearchById_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (StreetSearchByIdForm form = new StreetSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        // added by asyrul
        private void btnStreetGeneralSearchByName_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (StreetSearchByNameForm form = new StreetSearchByNameForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnStreetGeneralAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddStreet);
        }

        private void btnStreetGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditStreet);
        }

        private void btnStreetGeneralDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteStreet);
        }

        private void btnStreetGeneralSplit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.SplitStreet);
        }

        private void btnStreetGeneralUnion_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.UnionStreet);
        }

        private void btnStreetGeneralCopy_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.CopyStreet);
        }

        private void btnStreetGeneralFlip_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.FlipStreet);
        }

        private void btnStreetGeneralShowProperty_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ShowStreetProperty);
        }

        private void btnStreetGeneralReshape_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReshapeStreet);
        }

        //added by asyrul
        private void btnStreetGeneralCopyAttribute_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.CopyStreetAttribute);
        }

        //private void btnStreetGeneralSearchById_Click(object sender, EventArgs e)
        //{
        //    //_commandPool.SetMapTool(Command.CopyStreetAttribute);
        //    MessageBox.Show("Search street by Id!");
        //}

        // added end

        private void btnStreetVertexAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddStreetVertex);
        }

        private void btnStreetVertexMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveStreetVertex);
        }

        private void btnStreetVertexDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteStreetVertex);
        }

        private void btnStreetRestrictionSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            using (StreetRestrictionSearchForm form = new StreetRestrictionSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnStreetRestrictionAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddStreetRestriction);
        }

        private void btnStreetRestrictionDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteStreetRestriction);
        }

        private void btnStreetRestrictionMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveStreetRestriction);
        }

        private void btnStreetRestrictionShowStreet_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ShowStreetRestrictionStreet);
        }

        #endregion

        #region Junction Menu

        private void btnJunctionGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (JunctionSearchForm form = new JunctionSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnJunctionSearchById_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (JunctionSearchByIdForm form = new JunctionSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnJunctionGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditJunction);
        }

        private void btnJunctionGeneralMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveJunction);
        }

        private void btnJunctionGeneralDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteJunction);
        }

        private void btnJunctionGeneralSplit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.SplitJunction);
        }

        //added by asyrul
        private void btnJunctionGeneralReassociate_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReassociateJunction);
        }
        //added end

        #endregion

        #region Property Menu

        private void btnPropertyGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PropertySearchForm form = new PropertySearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnPropertyGeneralAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddProperty);
        }

        private void btnPropertyGeneralAddMultiple_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddMultipleProperty);
        }

        private void btnPropertyGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditProperty);
        }

        private void btnPropertyGeneralEditMultiple_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditMultipleProperty);
        }

        private void btnPropertyGeneralDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteProperty);
        }

        private void btnPropertyGeneralReassociate_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReassociateProperty);
        }

        private void btnPropertyGeneralMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveProperty);
        }

        //added by asyrul
        private void btnPropertyGeneralSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search property by Id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PropertySearchByIdForm form = new PropertySearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnPropertyGeneralSearchByName_Click(object sender, EventArgs e)
        {
            //_commandPool.SetMapTool(Command.CopyStreetAttribute);
            //MessageBox.Show("Search property by Name!");
        }
        // added end

        private void btnPropertyFloorSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (FloorSearchForm form = new FloorSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnPropertyFloorSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search Property Floor by Id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (FloorPropertySearchByIdForm form = new FloorPropertySearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added end

        #endregion

        #region Building Menu

        private void btnBuildingGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingSearchForm form = new BuildingSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnBuildingGeneralSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search building by Id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingSearchByIdForm form = new BuildingSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnBuildingGeneralSearchByName_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search building by Name!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingSearchByNameForm form = new BuildingSearchByNameForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnBuildingGeneralAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddBuilding);
        }

        private void btnBuildingGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditBuilding);
        }

        private void btnBuildingGeneralDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteBuilding);
        }

        private void btnBuildingGeneralMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveBuilding);
        }

        // added by asyrul
        private void btnBuildingGeneralReassociate_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReassociateBuilding);
            //MessageBox.Show("Enter Building Reassociation!");
        }
        // added end

        private void btnBuildingGroupSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingGroupSearchForm form = new BuildingGroupSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnBuildingGroupSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search building group by Id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingGroupSearchByIdForm form = new BuildingGroupSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnBuildingGroupSearchByName_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search building group by Name!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (BuildingGroupSearchByNameForm form = new BuildingGroupSearchByNameForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnBuildingGroupAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddBuildingGroup);
        }

        private void btnBuildingGroupEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditBuildingGroup);
        }

        private void btnBuildingGroupDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteBuildingGroup);
        }

        private void btnBuildingGroupReassociate_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReassociateBuildingGroup);
        }

        // added by asyrul
        private void btnAssociateBuildingToBuildingGroup_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AssociateBuildingToBuildingGroup);
        }

       

        private void btnAssociateBuildingByDistance_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Associate by Distance!");
            _commandPool.SetMapTool(Command.AssociateBuildingByDistance);
        }
        // added end

        // noraini ali - NOV 2020
        private void btnDissociateBuildingToBuildingGroup_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DissociateBuildingToBuildingGroup);
        }
        // added end

        private void btnBuildingGroupMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveBuildingGroup);
        }

        private void btnBuildingMultiStoreySearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (MultiStoreySearchForm form = new MultiStoreySearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnBuildingMultiStoreySearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search Building multi storey by id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (MultistoreySearchByIdForm form = new MultistoreySearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        #endregion

        #region Landmark Menu

        private void btnLandmarkGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (LandmarkSearchForm form = new LandmarkSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnLandmarkGeneralSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search landmark by id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (LandmarkSearchByIdForm form = new LandmarkSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnLandmarkGeneralSearchByName_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search landmark by Name!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (LandmarkSearchByNameForm form = new LandmarkSearchByNameForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnLandmarkGeneralAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddLandmark);
        }

        private void btnLandmarkGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditLandmark);
        }

        private void btnLandmarkGeneralDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteLandmark);
        }

        private void btnLandmarkGeneralReassociate_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ReassociateLandmark);
        }

        private void btnLandmarkGeneralMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveLandmark);
        }

        private void btnLandmarkBoundarySearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (LandmarkBoundarySearchForm form = new LandmarkBoundarySearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
            }
        }

        private void btnLandmarkBoundaryAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddLandmarkBoundary);
        }

        private void btnLandmarkBoundaryMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveLandmarkBoundary);
        }

        private void btnLandmarkBoundaryVertexAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddLandmarkBoundaryVertex);
        }

        private void btnLandmarkBoundaryVertexMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveLandmarkBoundaryVertex);
        }

        private void btnLandmarkBoundaryVertexDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteLandmarkBoundaryVertex);
        }

        #endregion

        #region Poi Menu

        private void btnPoiGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PoiSearchForm form = new PoiSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        // added by asyrul
        private void btnPoiGeneralSearchById_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search POI by id!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PoiSearchByIdForm form = new PoiSearchByIdForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnPoiGeneralSearchByName_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Search POI by name!");
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PoiSearchByNameForm form = new PoiSearchByNameForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // added end

        private void btnPoiGeneralEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditPoi);
        }

        private void btnPoiGeneralMerge_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MergePoi);
        }

        private void btnPoiGeneralShowStreet_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ShowPoiStreet);
        }

        #endregion

        #region Phone Menu

        private void btnPhoneGeneralSearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PhoneSearchForm form = new PhoneSearchForm(SelectedSegment.Value))
            {
                form.ShowDialog(this);
            }
        }

        // added by asyrul
        private void btnPhoneSearchByNo_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (PhoneSearchByNoForm form = new PhoneSearchByNoForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
            }

            //private void btnStreetGeneralSearchById_Click(object sender, EventArgs e)
            //{
            //    if (!SelectedSegment.HasValue)
            //    {
            //        return;
            //    }
            //    using (StreetSearchByIdForm form = new StreetSearchByIdForm(SelectedSegment.Value))
            //    {
            //        form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
            //        form.ShowDialog(this);
            //        form.ShowMap -= (SearchForm_ShowMap);
            //    }
            //}
        }

        #endregion

        #region Location Menu

        private void btnLocationSectionBoundarySearch_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (SectionBoundarySearchForm form = new SectionBoundarySearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
            }
        }

        private void btnLocationSectionBoundaryAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddSectionBoundary);
        }

        private void btnLocationSectionBoundaryEdit_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.EditSectionBoundary);
        }

        private void btnLocationSectionBoundaryDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteSectionBoundary);
        }

        private void btnLocationSectionBoundaryMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveSectionBoundary);
        }

        private void btnLocationSectionBoundaryVertexAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddSectionBoundaryVertex);
        }

        private void btnLocationSectionBoundaryVertexMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveSectionBoundaryVertex);
        }

        private void btnLocationSectionBoundaryVertexDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteSectionBoundaryVertex);
        }

        private void btnLocationManage_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                using (ManageLocationForm form = new ManageLocationForm())
                {
                    form.ShowDialog(this);
                }
            }
            else
            {
                using (ManageLocationForm form = new ManageLocationForm(SelectedSegment.Value))
                {
                    form.ShowDialog(this);
                }
            }
        }

        #endregion

        #region User Menu

        private void btnUserAccessibilityManage_Click(object sender, EventArgs e)
        {
            using (AccessibilityForm form = new AccessibilityForm())
            {
                form.ShowDialog(this);
            }
        }

        private void btnUserAccessibilityChangePassword_Click(object sender, EventArgs e)
        {
            using (UserPasswordForm form = new UserPasswordForm())
            {
                form.ShowDialog(this);
            }
        }

        #endregion

        #region Region/Index Menu
        //added by asyrul
        private void btnShowIndex_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.ShowIndex);
        }

        private void btnSearchIndex_Click(object sender, EventArgs e)
        {
            //_commandPool.SetMapTool(Command.SearchIndex);

            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (IndexSearchForm form = new IndexSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
            }
        }

        #endregion

        //added by asyrul end

        #region Jupem
        // noraini - Aug 2021
        private void btnSearchJupemLot_Click(object sender, EventArgs e)
        {
            if (!SelectedSegment.HasValue)
            {
                return;
            }
            _commandPool.SetMapTool(Command.NoTool);
            using (JupemLotSearchForm form = new JupemLotSearchForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }
        // end
        #endregion

        #region Work Area
        //added by asyrul
        private void btnCreateWorkArea_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Create Work Area!");
            _commandPool.SetMapTool(Command.CreateWorkArea);
        }
        private void btnWorkAreaVertexAdd_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.AddWorkAreaVertex);
        }

        private void btnWorkAreaVertexMove_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.MoveWorkAreaVertex);
        }

        private void btnWorkAreaVertexDelete_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.DeleteWorkAreaVertex);
        }

        #endregion

        private void SearchForm_ShowMap(object sender, ShowMapEventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }

            using (new WaitCursor())
            {
                ActiveMap.SelectFeatures(e.Collection, true);
                ActiveMap.ZoomToSelection();
            }
        }

        private void SearchForm_ShowMap2(object sender, ShowMapEventArgs e)
        {
            if (ActiveMap == null)
            {
                return;
            }

            using (new WaitCursor())
            {
                ActiveMap.SelectFeatures(e.Collection, true);
                ActiveMap.ZoomToSelection();
            }
            // noraini ali - NOV 2020 highlight use in verification module
            ActiveMap.HighlightSelection();
        }

        private void Session_WorkspaceChanged(object sender, WorkspaceChangedEventArgs e)
        {
            lblWorkspaceText.Text = (e == null) ? "N/A" : string.Format("{0}", e.Name);
            if (ActiveMap != null)
            {
                ActiveMap.OnWorkspaceChanged(e.PreviousWorkspace, e.Workspace);
            }
        }

        private void Session_StopEditing(object sender, EditEventArgs e)
        {
            _commandPool.OnSave();
            UpdateOperationMenu();
        }

        private void Session_UndoOperation(object sender, WorkspaceEventArgs e)
        {
            UpdateOperationMenu();
            ActiveMap.OnUndo();
            _commandPool.OnUndo();
        }

        private void Session_RedoOperation(object sender, WorkspaceEventArgs e)
        {
            UpdateOperationMenu();
            ActiveMap.OnRedo();
            _commandPool.OnRedo();
        }

        private void Session_StartOperation(object sender, WorkspaceEventArgs e)
        {
            OperationEventArgs e1 = (OperationEventArgs)e;
            ActiveMap.OnStartOperation(e1.Names);
        }

        private void Session_StopOperation(object sender, WorkspaceEventArgs e)
        {
            UpdateOperationMenu();
            OperationEventArgs e1 = (OperationEventArgs)e;
            ActiveMap.OnStopOperation(e1.Names);
        }

        private void Session_AbortOperation(object sender, WorkspaceEventArgs e)
        {
            OperationEventArgs e1 = (OperationEventArgs)e;
            ActiveMap.OnAbortOperation(e1.Names);
        }

        private void Session_ConflictsDetected(object sender, WorkspaceEventArgs e)
        {
        }

        private void UpdateOperationMenu()
        {
            bool enableSave = Session.Current.HasEdits();
            bool enableUndo = Session.Current.HasUndos();
            bool enableRedo = Session.Current.HasRedos();

            if (enableSave)
            {
                _commandPool.Enable(Command.Save);
            }
            else
            {
                _commandPool.Disable(Command.Save);
            }

            if (enableUndo)
            {
                _commandPool.Enable(Command.Undo);
            }
            else
            {
                _commandPool.Disable(Command.Undo);
            }

            if (enableRedo)
            {
                _commandPool.Enable(Command.Redo);
            }
            else
            {
                _commandPool.Disable(Command.Redo);
            }
        }

        private void UpdateExtentMenu()
        {
            bool enablePreviousExtent = (ActiveMap == null) ? false : ActiveMap.CanUndoExtent;
            bool enableNextExtent = (ActiveMap == null) ? false : ActiveMap.CanRedoExtent;

            if (enablePreviousExtent)
            {
                _commandPool.Enable(Command.PreviousExtent);
            }
            else
            {
                _commandPool.Disable(Command.PreviousExtent);
            }

            if (enableNextExtent)
            {
                _commandPool.Enable(Command.NextExtent);
            }
            else
            {
                _commandPool.Disable(Command.NextExtent);
            }
        }

        private void UpdateSelectionMenu()
        {
            bool enableSelection = (ActiveMap == null) ? false : ActiveMap.SelectionCount > 0;

            if (enableSelection)
            {
                _commandPool.Enable(Command.ZoomToSelection);
                _commandPool.Enable(Command.ClearSelection);
            }
            else
            {
                _commandPool.Disable(Command.ZoomToSelection);
                _commandPool.Disable(Command.ClearSelection);
            }
        }

        private void UpdateEditorMenu()
        {
            if (ActiveMap == null)
            {
                _commandPool.Disable(Command.DrawLine);
                _commandPool.Disable(Command.DrawCircle);
                _commandPool.Disable(Command.DrawRectangle);
                _commandPool.Disable(Command.DrawPolygon);
                _commandPool.Disable(Command.ClearDrawings);
            }
            else
            {
                _commandPool.Enable(Command.DrawLine);
                _commandPool.Enable(Command.DrawCircle);
                _commandPool.Enable(Command.DrawRectangle);
                _commandPool.Enable(Command.DrawPolygon);
                _commandPool.Enable(Command.ClearDrawings);
            }
        }

        private void UpdateOpenFileMenu()
        {
            if (ActiveMap == null)
            {
                _commandPool.Disable(Command.OpenShape);
                _commandPool.Disable(Command.OpenGdb);
                _commandPool.Disable(Command.OpenRaster);
                _commandPool.Disable(Command.OpenGPS);  // noraini ali - Jun 2020
            }
            else
            {
                _commandPool.Enable(Command.OpenShape);
                _commandPool.Enable(Command.OpenGdb);
                _commandPool.Enable(Command.OpenRaster);
                _commandPool.Enable(Command.OpenGPS);   // noraini ali - Jun 2020
            }
        }

		private void UpdateRasterMenu()
		{
			if (ActiveMap == null)
			{

				_commandPool.Disable(Command.OpenRasterWarping);
				_commandPool.Disable(Command.CloseRasterWarping);
				_commandPool.Disable(Command.OpenRasterInPaint);
			}
			else
			{
				_commandPool.Enable(Command.OpenRasterWarping);
				_commandPool.Enable(Command.CloseRasterWarping);
				_commandPool.Enable(Command.OpenRasterInPaint);
			}
		}

        private void UpdateMenu()
        {
            UpdateOperationMenu();
            UpdateExtentMenu();
            UpdateSelectionMenu();
            UpdateEditorMenu();
            UpdateOpenFileMenu();
			UpdateRasterMenu();

			bool enableSegmentedCommands = (ActiveMap == null) ? false : SelectedSegment.HasValue;

            if (enableSegmentedCommands)
            {
                _commandPool.Enable(Command.SegmentedCommand);
            }
            else
            {
                _commandPool.Disable(Command.SegmentedCommand);
            }

            _commandPool.Disable(Command.AddLandmarkBoundary);
            _commandPool.Disable(Command.DeleteLandmarkBoundary);

            _commandPool.Limit(Command.UserCommand, Session.User.GetCommands());

        }

        private void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }

        private void RefreshTitle(string title)
        {
            Text = title; ;
        }

        private void springLabel_Click(object sender, EventArgs e)
        {
           
        }

        private void dockPanel_ActiveContentChanged(object sender, EventArgs e)
        {

        }

        // added by noraini - Jan 2020 - CADU 2 AND
        #region Supervisor AND
        private void btnSearchWorkArea_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.NoTool);
            using (SearchWorkAreaForm form = new SearchWorkAreaForm(SelectedSegment.Value))
            {
                form.ShowMap += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap);
                form.ShowDialog(this);
                form.ShowMap -= (SearchForm_ShowMap);
                ActiveMap.HighlightSelectionFeature();
            }
        }

        private void btnVerifyWorkArea_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.VerifyWorkArea);
        }

        private void btnDeleteWorkArea_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Create Work Area!");
            _commandPool.SetMapTool(Command.DeleteWorkArea);
        }

        private void btnCloseWorkArea_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.CloseWorkArea);
        }

        private void btnCompletedWorkArea_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Complete Work Area!");
            _commandPool.SetMapTool(Command.CompleteWorkArea);
        }

        private void btnVerifyWorkArea2_Click(object sender, EventArgs e)
        {
            _commandPool.SetMapTool(Command.NoTool);

            using (VerifyWorkArea2Form form = new VerifyWorkArea2Form(SelectedSegment.Value))
            {
                form.ShowMap2 += new EventHandler<ShowMapEventArgs>(SearchForm_ShowMap2);
                form.ShowDialog(this);
                form.ShowMap2 -= (SearchForm_ShowMap2);
                
            }
            ActiveMap.HighlightClear();
            ActiveMap.RefreshMap();
        }

		#endregion
		// end added

		#region [Raster] syafiq - July 2021

		private void OnOpenRasterWarping(object sender, EventArgs e)
		{
			DialogResult result = openRasterDialog.ShowDialog(this);
			if (result != DialogResult.OK)
			{
				return;
			}
			using (new WaitCursor())
			{
				string path = Path.GetDirectoryName(openRasterDialog.FileName);
				string file = Path.GetFileName(openRasterDialog.FileName);
				ActiveMap.OpenRaster(path, file);
			}
		}

		private void OnCloseRasterWarping(object sender, EventArgs e)
		{
			ActiveMap.CloseRaster();
		}

		private void btnAddPoint_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.AddPoint);
		}

		private void btnDeletePoint_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.DeletePoint);
		}

		private void btnDeleteAllPoints_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.DeleteAllPoints);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnUpdateAlignment_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.UpdateAlignment);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnFitToDisplay_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.FitToDisplay);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnRegisterRaster_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.RegisterRaster);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnBuildPyramid_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.BuildPyramid);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnMoveRaster_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.MoveRaster);
		}

		private void btnScaleUp_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.ScaleUp);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnScaleDown_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.ScaleDown);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnRotateClockwise_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.RotateClockwise);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void btnRotateAntiClockwise_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.RotateAntiClockwise);
			_commandPool.SetMapTool(Command.NoTool);
		}

		private void OnOpenRasterInPaint(object sender, EventArgs e)
		{
			DialogResult result = openRasterDialog.ShowDialog(this);
			if (result != DialogResult.OK)
			{
				return;
			}
			using (new WaitCursor())
			{
				string path = Path.GetDirectoryName(openRasterDialog.FileName);
				string file = Path.GetFileName(openRasterDialog.FileName);
				ActiveMap.OpenRasterInPaint(path, file);
			}
		}

		private void btnExportRaster_Click(object sender, EventArgs e)
		{
			_commandPool.SetMapTool(Command.ExportRaster);
			_commandPool.SetMapTool(Command.NoTool);
		}
        #endregion

        #region [Verification CADU3 - Supervisor Geomatics ] noraini - Aug 22
        private void btnVerifyWEB_Click(object sender, EventArgs e)
        {
            using (VerifyCadu3WebForm form = new VerifyCadu3WebForm(SelectedSegment.Value))
            {
                form.ShowDialog(this);
            }
        }
        #endregion
    }
}

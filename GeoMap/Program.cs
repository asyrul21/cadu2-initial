﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ESRI.ArcGIS.esriSystem;
using Geomatic.Core;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Search;
using Geomatic.UI.Forms.Edit;

namespace Geomatic.Map
{
    static class Program
    {
        public static SplashForm Splash;

        private static LicenseInitializer m_AOLicenseInitializer = new LicenseInitializer();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //ESRI License Initializer generated code.
            if (!m_AOLicenseInitializer.InitializeApplication(new esriLicenseProductCode[] { esriLicenseProductCode.esriLicenseProductCodeEngineGeoDB },
            new esriLicenseExtensionCode[] { }))
            {
                MessageBox.Show(m_AOLicenseInitializer.LicenseMessage() +
                "\n\nThis application could not initialize with the correct ArcGIS license and will shutdown.",
                "ArcGIS License Failture");
                m_AOLicenseInitializer.ShutdownApplication();
                Application.Exit();
                return;
            }

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Splash = new SplashForm();

            DialogResult result;
            using (LoginForm loginForm = new LoginForm())
            {
                result = loginForm.ShowDialog();
            }
            if (result == DialogResult.OK)
            {
                Application.Run(new MainForm());
            }

            //ESRI License Initializer generated code.
            //Do not make any call to ArcObjects after ShutDownApplication()
            m_AOLicenseInitializer.ShutdownApplication();
            Application.Exit();
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            using (MessageBoxBuilder box = MessageBoxFactory.Create(e.Exception))
            {
                box.Show();
            }
        }
    }
}

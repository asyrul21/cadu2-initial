﻿namespace Geomatic.Map
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripStatusLabel lblCoordinate;
            System.Windows.Forms.ToolStripStatusLabel lblScale;
            System.Windows.Forms.ToolStripStatusLabel lblUnit;
            System.Windows.Forms.ToolStripStatusLabel lblSelection;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.ribbon = new System.Windows.Forms.Ribbon();
            this.menuFileOpen = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonSeparator11 = new System.Windows.Forms.RibbonSeparator();
            this.menuFileOpenMap = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.ribbonSeparator12 = new System.Windows.Forms.RibbonSeparator();
            this.menuFileOpenWorkOrder = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.ribbonSeparator13 = new System.Windows.Forms.RibbonSeparator();
            this.menuFileOpenShape = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.menuFileOpenGdb = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.menuFileOpenRaster = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.menuFileOpenGPS = new System.Windows.Forms.RibbonDescriptionMenuItem();
            this.menuFileSave = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnFileExit = new System.Windows.Forms.RibbonOrbOptionButton();
            this.btnFileOption = new System.Windows.Forms.RibbonOrbOptionButton();
            this.btnSave = new System.Windows.Forms.RibbonButton();
            this.btnUndo = new System.Windows.Forms.RibbonButton();
            this.btnRedo = new System.Windows.Forms.RibbonButton();
            this.homeTab = new System.Windows.Forms.RibbonTab();
            this.homeGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.txtUserName = new System.Windows.Forms.RibbonTextBox();
            this.txtUserGroup = new System.Windows.Forms.RibbonTextBox();
            this.cbHomeGeneralSegment = new System.Windows.Forms.RibbonComboBox();
            this.cbScale = new System.Windows.Forms.RibbonComboBox();
            this.homeMapPanel = new System.Windows.Forms.RibbonPanel();
            this.btnHomeMapNoTool = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapSelect = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapPan = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapZoomIn = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapZoomOut = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapZoomToSelection = new System.Windows.Forms.RibbonButton();
            this.separator03 = new System.Windows.Forms.RibbonSeparator();
            this.btnHomeMapFullExtent = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapPreviousExtent = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapNextExtent = new System.Windows.Forms.RibbonButton();
            this.separator04 = new System.Windows.Forms.RibbonSeparator();
            this.btnHomeMapRefresh = new System.Windows.Forms.RibbonButton();
            this.btnHomeMapClearSelection = new System.Windows.Forms.RibbonButton();
            this.separator05 = new System.Windows.Forms.RibbonSeparator();
            this.btnHomeMapRuler = new System.Windows.Forms.RibbonButton();
            this.homeToolbox = new System.Windows.Forms.RibbonPanel();
            this.btnHomeToolboxGoTo = new System.Windows.Forms.RibbonButton();
            this.homeWindowPanel = new System.Windows.Forms.RibbonPanel();
            this.btnHomeWindowLayer = new System.Windows.Forms.RibbonButton();
            this.btnHomeWindowOutput = new System.Windows.Forms.RibbonButton();
            this.btnHomeWindowSelections = new System.Windows.Forms.RibbonButton();
            this.editorTab = new System.Windows.Forms.RibbonTab();
            this.editorGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnEditorDrawLine = new System.Windows.Forms.RibbonButton();
            this.btnEditorDrawCircle = new System.Windows.Forms.RibbonButton();
            this.btnEditorDrawRectangle = new System.Windows.Forms.RibbonButton();
            this.btnEditorDrawPolygon = new System.Windows.Forms.RibbonButton();
            this.btnEditorDrawClearDrawings = new System.Windows.Forms.RibbonButton();
            this.streetTab = new System.Windows.Forms.RibbonTab();
            this.streetGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnStreetGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.btnStreetGeneralAdd = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralDelete = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralSplit = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralUnion = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralCopy = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralFlip = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralShowProperty = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralReshape = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralCopyAttribute = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralSearchById = new System.Windows.Forms.RibbonButton();
            this.btnStreetGeneralSearchByName = new System.Windows.Forms.RibbonButton();
            this.streetVertexPanel = new System.Windows.Forms.RibbonPanel();
            this.btnStreetVertexAdd = new System.Windows.Forms.RibbonButton();
            this.btnStreetVertexMove = new System.Windows.Forms.RibbonButton();
            this.btnStreetVertexDelete = new System.Windows.Forms.RibbonButton();
            this.streetRestrictionPanel = new System.Windows.Forms.RibbonPanel();
            this.btnStreetRestrictionSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator9 = new System.Windows.Forms.RibbonSeparator();
            this.btnStreetRestrictionAdd = new System.Windows.Forms.RibbonButton();
            this.btnStreetRestrictionDelete = new System.Windows.Forms.RibbonButton();
            this.btnStreetRestrictionMove = new System.Windows.Forms.RibbonButton();
            this.btnStreetRestrictionShowStreet = new System.Windows.Forms.RibbonButton();
            this.junctionTab = new System.Windows.Forms.RibbonTab();
            this.junctionGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnJunctionGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator3 = new System.Windows.Forms.RibbonSeparator();
            this.btnJunctionGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnJunctionGeneralDelete = new System.Windows.Forms.RibbonButton();
            this.btnJunctionGeneralMove = new System.Windows.Forms.RibbonButton();
            this.btnJunctionGeneralSplit = new System.Windows.Forms.RibbonButton();
            this.btnJunctionSearchById = new System.Windows.Forms.RibbonButton();
            this.propertyTab = new System.Windows.Forms.RibbonTab();
            this.propertyGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnPropertyGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.btnPropertyGeneralAdd = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralAddMultiple = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralEditMultiple = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralDelete = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralReassociate = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralMove = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralSearchById = new System.Windows.Forms.RibbonButton();
            this.propertyFloorPanel = new System.Windows.Forms.RibbonPanel();
            this.btnPropertyFloorSearch = new System.Windows.Forms.RibbonButton();
            this.btnPropertyFloorSearchById = new System.Windows.Forms.RibbonButton();
            this.buildingTab = new System.Windows.Forms.RibbonTab();
            this.buildingGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnBuildingGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator2 = new System.Windows.Forms.RibbonSeparator();
            this.btnBuildingGeneralAdd = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralDelete = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralMove = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralReassociate = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralSearchById = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGeneralSearchByName = new System.Windows.Forms.RibbonButton();
            this.buildingGroupPanel = new System.Windows.Forms.RibbonPanel();
            this.ribbonSeparator5 = new System.Windows.Forms.RibbonSeparator();
            this.btnBuildingGroupSearch = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupAdd = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupEdit = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupDelete = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupReassociate = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupMove = new System.Windows.Forms.RibbonButton();
            this.btnAssociateBuildingToBuildingGroup = new System.Windows.Forms.RibbonButton();
            this.btnDissociateBuildingToBuildingGroup = new System.Windows.Forms.RibbonButton();
            this.btnAssociateBuildingByDistance = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupSearchById = new System.Windows.Forms.RibbonButton();
            this.btnBuildingGroupSearchByName = new System.Windows.Forms.RibbonButton();
            this.buildingMultiStoreyPanel = new System.Windows.Forms.RibbonPanel();
            this.btnBuildingMultiStoreySearch = new System.Windows.Forms.RibbonButton();
            this.btnBuildingMultiStoreySearchById = new System.Windows.Forms.RibbonButton();
            this.landmarkTab = new System.Windows.Forms.RibbonTab();
            this.landmarkGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLandmarkGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator6 = new System.Windows.Forms.RibbonSeparator();
            this.btnLandmarkGeneralAdd = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralDelete = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralReassociate = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralMove = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralSearchById = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkGeneralSearchByName = new System.Windows.Forms.RibbonButton();
            this.landmarkBoundaryPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLandmarkBoundarySearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator7 = new System.Windows.Forms.RibbonSeparator();
            this.btnLandmarkBoundaryAdd = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkBoundaryDelete = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkBoundaryMove = new System.Windows.Forms.RibbonButton();
            this.landmarkBoundaryVertexPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLandmarkBoundaryVertexAdd = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkBoundaryVertexMove = new System.Windows.Forms.RibbonButton();
            this.btnLandmarkBoundaryVertexDelete = new System.Windows.Forms.RibbonButton();
            this.poiTab = new System.Windows.Forms.RibbonTab();
            this.poiGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnPoiGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator8 = new System.Windows.Forms.RibbonSeparator();
            this.btnPoiGeneralEdit = new System.Windows.Forms.RibbonButton();
            this.btnPoiGeneralMerge = new System.Windows.Forms.RibbonButton();
            this.btnPoiGeneralShowStreet = new System.Windows.Forms.RibbonButton();
            this.btnPoiGeneralSearchById = new System.Windows.Forms.RibbonButton();
            this.btnPoiGeneralSearchByName = new System.Windows.Forms.RibbonButton();
            this.phoneTab = new System.Windows.Forms.RibbonTab();
            this.phoneGeneralPanel = new System.Windows.Forms.RibbonPanel();
            this.btnPhoneGeneralSearch = new System.Windows.Forms.RibbonButton();
            this.btnPhoneSearchByNo = new System.Windows.Forms.RibbonButton();
            this.locationTab = new System.Windows.Forms.RibbonTab();
            this.sectionBoundaryPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLocationSectionBoundarySearch = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator10 = new System.Windows.Forms.RibbonSeparator();
            this.btnLocationSectionBoundaryAdd = new System.Windows.Forms.RibbonButton();
            this.btnLocationSectionBoundaryEdit = new System.Windows.Forms.RibbonButton();
            this.btnLocationSectionBoundaryDelete = new System.Windows.Forms.RibbonButton();
            this.btnLocationSectionBoundaryMove = new System.Windows.Forms.RibbonButton();
            this.locationSectionBoundaryVertexPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLocationSectionBoundaryVertexAdd = new System.Windows.Forms.RibbonButton();
            this.btnLocationSectionBoundaryVertexMove = new System.Windows.Forms.RibbonButton();
            this.btnLocationSectionBoundaryVertexDelete = new System.Windows.Forms.RibbonButton();
            this.locationPanel = new System.Windows.Forms.RibbonPanel();
            this.btnLocationManage = new System.Windows.Forms.RibbonButton();
            this.userTab = new System.Windows.Forms.RibbonTab();
            this.userAccessibilityPanel = new System.Windows.Forms.RibbonPanel();
            this.btnUserAccessibilityManage = new System.Windows.Forms.RibbonButton();
            this.btnUserAccessibilityChangePassword = new System.Windows.Forms.RibbonButton();
            this.Cadu3Panel = new System.Windows.Forms.RibbonPanel();
            this.btnVerifyWeb = new System.Windows.Forms.RibbonButton();
            this.indexTab = new System.Windows.Forms.RibbonTab();
            this.indexPanel = new System.Windows.Forms.RibbonPanel();
            this.btnShowIndex = new System.Windows.Forms.RibbonButton();
            this.btnSearchIndex = new System.Windows.Forms.RibbonButton();
            this.jupemPanel = new System.Windows.Forms.RibbonPanel();
            this.btnSearchJupemLot = new System.Windows.Forms.RibbonButton();
            this.WorkAreaANDTab = new System.Windows.Forms.RibbonTab();
            this.ANDWorkAreaPanel = new System.Windows.Forms.RibbonPanel();
            this.btnCreateWA = new System.Windows.Forms.RibbonButton();
            this.btnDeleteWA = new System.Windows.Forms.RibbonButton();
            this.btnCloseWA = new System.Windows.Forms.RibbonButton();
            this.btnSearchWorkArea = new System.Windows.Forms.RibbonButton();
            this.btnVerifyWorkArea2 = new System.Windows.Forms.RibbonButton();
            this.btnCompleteWA = new System.Windows.Forms.RibbonButton();
            this.btnVerifyWorkArea = new System.Windows.Forms.RibbonButton();
            this.workAreaVertexPanel = new System.Windows.Forms.RibbonPanel();
            this.btnWorkAreaVertexAdd = new System.Windows.Forms.RibbonButton();
            this.btnWorkAreaVertexMove = new System.Windows.Forms.RibbonButton();
            this.btnWorkAreaVertexDelete = new System.Windows.Forms.RibbonButton();
            this.rasterTab = new System.Windows.Forms.RibbonTab();
            this.rasterPanel = new System.Windows.Forms.RibbonPanel();
            this.btnOpenRasterWarping = new System.Windows.Forms.RibbonButton();
            this.btnCloseRasterWarping = new System.Windows.Forms.RibbonButton();
            this.warpingPanel = new System.Windows.Forms.RibbonPanel();
            this.btnAddPoint = new System.Windows.Forms.RibbonButton();
            this.btnDeletePoint = new System.Windows.Forms.RibbonButton();
            this.btnDeleteAllPoints = new System.Windows.Forms.RibbonButton();
            this.btnUpdateAlignment = new System.Windows.Forms.RibbonButton();
            this.btnFitToDisplay = new System.Windows.Forms.RibbonButton();
            this.btnRegisterRaster = new System.Windows.Forms.RibbonButton();
            this.btnBuildPyramid = new System.Windows.Forms.RibbonButton();
            this.btnMoveRaster = new System.Windows.Forms.RibbonButton();
            this.btnScaleUp = new System.Windows.Forms.RibbonButton();
            this.btnScaleDown = new System.Windows.Forms.RibbonButton();
            this.btnRotateClockwise = new System.Windows.Forms.RibbonButton();
            this.btnRotateAntiClockwise = new System.Windows.Forms.RibbonButton();
            this.editRasterPanel = new System.Windows.Forms.RibbonPanel();
            this.btnOpenRasterInPaint = new System.Windows.Forms.RibbonButton();
            this.btnExportRaster = new System.Windows.Forms.RibbonButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.springLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblWorkspace = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblWorkspaceText = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblSelectionText = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCoordinateText = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblScaleText = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUnitText = new System.Windows.Forms.ToolStripStatusLabel();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuDocument = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuNoTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSelect = new System.Windows.Forms.ToolStripMenuItem();
            this.separator11 = new System.Windows.Forms.ToolStripSeparator();
            this.menuPan = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoomIn = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoomOut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoomToSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.separator12 = new System.Windows.Forms.ToolStripSeparator();
            this.cbMenuScale = new System.Windows.Forms.ToolStripComboBox();
            this.menuFullExtent = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreviousExtent = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNextExtent = new System.Windows.Forms.ToolStripMenuItem();
            this.separator13 = new System.Windows.Forms.ToolStripSeparator();
            this.menuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClearSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.separator14 = new System.Windows.Forms.ToolStripSeparator();
            this.menuRuler = new System.Windows.Forms.ToolStripMenuItem();
            this.openShapeDialog = new System.Windows.Forms.OpenFileDialog();
            this.openRasterDialog = new System.Windows.Forms.OpenFileDialog();
            this.openGPSDialog = new System.Windows.Forms.OpenFileDialog();
            this.openGdbDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.btnOnLayerToggle = new System.Windows.Forms.RibbonButton();
            this.btnJunctionGeneralReassociate = new System.Windows.Forms.RibbonButton();
            this.btnPropertyGeneralSearchByName = new System.Windows.Forms.RibbonButton();
            lblCoordinate = new System.Windows.Forms.ToolStripStatusLabel();
            lblScale = new System.Windows.Forms.ToolStripStatusLabel();
            lblUnit = new System.Windows.Forms.ToolStripStatusLabel();
            lblSelection = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip.SuspendLayout();
            this.menuDocument.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCoordinate
            // 
            lblCoordinate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            lblCoordinate.Name = "lblCoordinate";
            lblCoordinate.Size = new System.Drawing.Size(66, 19);
            lblCoordinate.Text = "Coordinate";
            // 
            // lblScale
            // 
            lblScale.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            lblScale.Name = "lblScale";
            lblScale.Size = new System.Drawing.Size(34, 19);
            lblScale.Text = "Scale";
            // 
            // lblUnit
            // 
            lblUnit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            lblUnit.Name = "lblUnit";
            lblUnit.Size = new System.Drawing.Size(29, 19);
            lblUnit.Text = "Unit";
            // 
            // lblSelection
            // 
            lblSelection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            lblSelection.Name = "lblSelection";
            lblSelection.Size = new System.Drawing.Size(55, 19);
            lblSelection.Text = "Selection";
            // 
            // ribbon
            // 
            this.ribbon.BackColor = System.Drawing.Color.White;
            this.ribbon.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Minimized = false;
            this.ribbon.Name = "ribbon";
            // 
            // 
            // 
            this.ribbon.OrbDropDown.BorderRoundness = 8;
            this.ribbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon.OrbDropDown.MenuItems.Add(this.menuFileOpen);
            this.ribbon.OrbDropDown.MenuItems.Add(this.menuFileSave);
            this.ribbon.OrbDropDown.Name = "";
            this.ribbon.OrbDropDown.OptionItems.Add(this.btnFileExit);
            this.ribbon.OrbDropDown.OptionItems.Add(this.btnFileOption);
            this.ribbon.OrbDropDown.Size = new System.Drawing.Size(527, 160);
            this.ribbon.OrbDropDown.TabIndex = 0;
            this.ribbon.OrbImage = null;
            this.ribbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.ribbon.OrbText = "File";
            // 
            // 
            // 
            this.ribbon.QuickAcessToolbar.Items.Add(this.btnSave);
            this.ribbon.QuickAcessToolbar.Items.Add(this.btnUndo);
            this.ribbon.QuickAcessToolbar.Items.Add(this.btnRedo);
            this.ribbon.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon.Size = new System.Drawing.Size(1125, 160);
            this.ribbon.TabIndex = 0;
            this.ribbon.Tabs.Add(this.homeTab);
            this.ribbon.Tabs.Add(this.editorTab);
            this.ribbon.Tabs.Add(this.streetTab);
            this.ribbon.Tabs.Add(this.junctionTab);
            this.ribbon.Tabs.Add(this.propertyTab);
            this.ribbon.Tabs.Add(this.buildingTab);
            this.ribbon.Tabs.Add(this.landmarkTab);
            this.ribbon.Tabs.Add(this.poiTab);
            this.ribbon.Tabs.Add(this.phoneTab);
            this.ribbon.Tabs.Add(this.locationTab);
            this.ribbon.Tabs.Add(this.userTab);
            this.ribbon.Tabs.Add(this.indexTab);
            this.ribbon.Tabs.Add(this.WorkAreaANDTab);
            this.ribbon.Tabs.Add(this.rasterTab);
            this.ribbon.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.ribbon.Text = "ribbon";
            this.ribbon.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpen.DropDownItems.Add(this.ribbonSeparator11);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenMap);
            this.menuFileOpen.DropDownItems.Add(this.ribbonSeparator12);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenWorkOrder);
            this.menuFileOpen.DropDownItems.Add(this.ribbonSeparator13);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenShape);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenGdb);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenRaster);
            this.menuFileOpen.DropDownItems.Add(this.menuFileOpenGPS);
            this.menuFileOpen.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpen.Image")));
            this.menuFileOpen.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpen.SmallImage")));
            this.menuFileOpen.Style = System.Windows.Forms.RibbonButtonStyle.SplitDropDown;
            this.menuFileOpen.Text = "Open";
            // 
            // ribbonSeparator11
            // 
            this.ribbonSeparator11.Text = "Open Map";
            // 
            // menuFileOpenMap
            // 
            this.menuFileOpenMap.DescriptionBounds = new System.Drawing.Rectangle(30, 47, 331, 28);
            this.menuFileOpenMap.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenMap.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenMap.Image")));
            this.menuFileOpenMap.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenMap.SmallImage")));
            this.menuFileOpenMap.Text = "Map";
            this.menuFileOpenMap.Click += new System.EventHandler(this.OnFileOpenMap);
            // 
            // ribbonSeparator12
            // 
            this.ribbonSeparator12.Text = "";
            this.ribbonSeparator12.Visible = false;
            // 
            // menuFileOpenWorkOrder
            // 
            this.menuFileOpenWorkOrder.DescriptionBounds = new System.Drawing.Rectangle(30, 122, 331, 28);
            this.menuFileOpenWorkOrder.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenWorkOrder.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenWorkOrder.Image")));
            this.menuFileOpenWorkOrder.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenWorkOrder.SmallImage")));
            this.menuFileOpenWorkOrder.Text = "Work Order";
            this.menuFileOpenWorkOrder.Visible = false;
            this.menuFileOpenWorkOrder.Click += new System.EventHandler(this.OnFileOpenWorkOrder);
            // 
            // ribbonSeparator13
            // 
            this.ribbonSeparator13.Text = "Open File";
            // 
            // menuFileOpenShape
            // 
            this.menuFileOpenShape.DescriptionBounds = new System.Drawing.Rectangle(30, 197, 331, 28);
            this.menuFileOpenShape.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenShape.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenShape.Image")));
            this.menuFileOpenShape.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenShape.SmallImage")));
            this.menuFileOpenShape.Text = "Shape File";
            this.menuFileOpenShape.Click += new System.EventHandler(this.OnFileOpenShape);
            // 
            // menuFileOpenGdb
            // 
            this.menuFileOpenGdb.DescriptionBounds = new System.Drawing.Rectangle(30, 250, 331, 28);
            this.menuFileOpenGdb.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenGdb.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenGdb.Image")));
            this.menuFileOpenGdb.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenGdb.SmallImage")));
            this.menuFileOpenGdb.Text = "File Gdb";
            this.menuFileOpenGdb.Click += new System.EventHandler(this.OnFileOpenGdb);
            // 
            // menuFileOpenRaster
            // 
            this.menuFileOpenRaster.DescriptionBounds = new System.Drawing.Rectangle(30, 285, 331, 28);
            this.menuFileOpenRaster.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenRaster.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenRaster.Image")));
            this.menuFileOpenRaster.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenRaster.SmallImage")));
            this.menuFileOpenRaster.Text = "";
            this.menuFileOpenRaster.Visible = false;
            this.menuFileOpenRaster.Click += new System.EventHandler(this.OnFileOpenRaster);
            // 
            // menuFileOpenGPS
            // 
            this.menuFileOpenGPS.DescriptionBounds = new System.Drawing.Rectangle(30, 356, 331, 28);
            this.menuFileOpenGPS.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileOpenGPS.Image = ((System.Drawing.Image)(resources.GetObject("menuFileOpenGPS.Image")));
            this.menuFileOpenGPS.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileOpenGPS.SmallImage")));
            this.menuFileOpenGPS.Text = "Convert GPS(CAD) To Shp";
            this.menuFileOpenGPS.Click += new System.EventHandler(this.OnFileOpenGPSCadToShp);
            // 
            // menuFileSave
            // 
            this.menuFileSave.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.menuFileSave.Image = ((System.Drawing.Image)(resources.GetObject("menuFileSave.Image")));
            this.menuFileSave.SmallImage = ((System.Drawing.Image)(resources.GetObject("menuFileSave.SmallImage")));
            this.menuFileSave.Text = "Save";
            this.menuFileSave.Click += new System.EventHandler(this.OnFileSave);
            // 
            // btnFileExit
            // 
            this.btnFileExit.Image = ((System.Drawing.Image)(resources.GetObject("btnFileExit.Image")));
            this.btnFileExit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnFileExit.SmallImage")));
            this.btnFileExit.Text = "Exit";
            this.btnFileExit.Click += new System.EventHandler(this.OnFileExit);
            // 
            // btnFileOption
            // 
            this.btnFileOption.Image = ((System.Drawing.Image)(resources.GetObject("btnFileOption.Image")));
            this.btnFileOption.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnFileOption.SmallImage")));
            this.btnFileOption.Text = "Option";
            this.btnFileOption.Click += new System.EventHandler(this.OnFileOptions);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnSave.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSave.SmallImage")));
            this.btnSave.Text = "Save";
            this.btnSave.ToolTip = "Save";
            this.btnSave.Click += new System.EventHandler(this.OnFileSave);
            // 
            // btnUndo
            // 
            this.btnUndo.Image = ((System.Drawing.Image)(resources.GetObject("btnUndo.Image")));
            this.btnUndo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnUndo.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUndo.SmallImage")));
            this.btnUndo.Text = "Undo";
            this.btnUndo.ToolTip = "Undo";
            this.btnUndo.Click += new System.EventHandler(this.OnEditUndo);
            // 
            // btnRedo
            // 
            this.btnRedo.Image = ((System.Drawing.Image)(resources.GetObject("btnRedo.Image")));
            this.btnRedo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnRedo.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRedo.SmallImage")));
            this.btnRedo.Text = "Redo";
            this.btnRedo.ToolTip = "Redo";
            this.btnRedo.Click += new System.EventHandler(this.OnEditRedo);
            // 
            // homeTab
            // 
            this.homeTab.Panels.Add(this.homeGeneralPanel);
            this.homeTab.Panels.Add(this.homeMapPanel);
            this.homeTab.Panels.Add(this.homeToolbox);
            this.homeTab.Panels.Add(this.homeWindowPanel);
            this.homeTab.Text = "Home";
            // 
            // homeGeneralPanel
            // 
            this.homeGeneralPanel.Items.Add(this.txtUserName);
            this.homeGeneralPanel.Items.Add(this.txtUserGroup);
            this.homeGeneralPanel.Items.Add(this.cbHomeGeneralSegment);
            this.homeGeneralPanel.Items.Add(this.cbScale);
            this.homeGeneralPanel.Text = "General";
            // 
            // txtUserName
            // 
            this.txtUserName.AllowTextEdit = false;
            this.txtUserName.Text = "User       :";
            this.txtUserName.TextBoxText = "";
            this.txtUserName.ToolTip = "User Name";
            this.txtUserName.Value = "";
            // 
            // txtUserGroup
            // 
            this.txtUserGroup.AllowTextEdit = false;
            this.txtUserGroup.Text = "Group    :";
            this.txtUserGroup.TextBoxText = "";
            // 
            // cbHomeGeneralSegment
            // 
            this.cbHomeGeneralSegment.AllowTextEdit = false;
            this.cbHomeGeneralSegment.Text = "Segment:";
            this.cbHomeGeneralSegment.TextBoxText = "";
            this.cbHomeGeneralSegment.ToolTip = "Segment";
            this.cbHomeGeneralSegment.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cbHomeGeneralSegment_DropDownItemClicked);
            // 
            // cbScale
            // 
            this.cbScale.Text = "Scale      :";
            this.cbScale.TextBoxText = "";
            this.cbScale.DropDownItemClicked += new System.Windows.Forms.RibbonComboBox.RibbonItemEventHandler(this.cbScale_DropDownItemClicked);
            this.cbScale.TextBoxKeyDown += new System.Windows.Forms.KeyEventHandler(this.cbScale_KeyDown);
            // 
            // homeMapPanel
            // 
            this.homeMapPanel.Items.Add(this.btnHomeMapNoTool);
            this.homeMapPanel.Items.Add(this.btnHomeMapSelect);
            this.homeMapPanel.Items.Add(this.btnHomeMapPan);
            this.homeMapPanel.Items.Add(this.btnHomeMapZoomIn);
            this.homeMapPanel.Items.Add(this.btnHomeMapZoomOut);
            this.homeMapPanel.Items.Add(this.btnHomeMapZoomToSelection);
            this.homeMapPanel.Items.Add(this.separator03);
            this.homeMapPanel.Items.Add(this.btnHomeMapFullExtent);
            this.homeMapPanel.Items.Add(this.btnHomeMapPreviousExtent);
            this.homeMapPanel.Items.Add(this.btnHomeMapNextExtent);
            this.homeMapPanel.Items.Add(this.separator04);
            this.homeMapPanel.Items.Add(this.btnHomeMapRefresh);
            this.homeMapPanel.Items.Add(this.btnHomeMapClearSelection);
            this.homeMapPanel.Items.Add(this.separator05);
            this.homeMapPanel.Items.Add(this.btnHomeMapRuler);
            this.homeMapPanel.Text = "Map";
            // 
            // btnHomeMapNoTool
            // 
            this.btnHomeMapNoTool.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapNoTool.Image")));
            this.btnHomeMapNoTool.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapNoTool.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapNoTool.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapNoTool.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapNoTool.SmallImage")));
            this.btnHomeMapNoTool.Text = "No Tool";
            this.btnHomeMapNoTool.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapNoTool.ToolTip = "No Tool";
            this.btnHomeMapNoTool.Click += new System.EventHandler(this.OnMapNoTool);
            // 
            // btnHomeMapSelect
            // 
            this.btnHomeMapSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapSelect.Image")));
            this.btnHomeMapSelect.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapSelect.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapSelect.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapSelect.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapSelect.SmallImage")));
            this.btnHomeMapSelect.Text = "Select Feature";
            this.btnHomeMapSelect.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapSelect.ToolTip = "Select Feature";
            this.btnHomeMapSelect.Click += new System.EventHandler(this.OnMapSelect);
            // 
            // btnHomeMapPan
            // 
            this.btnHomeMapPan.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapPan.Image")));
            this.btnHomeMapPan.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapPan.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapPan.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapPan.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapPan.SmallImage")));
            this.btnHomeMapPan.Text = "Map Pan";
            this.btnHomeMapPan.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapPan.ToolTip = "Map Pan";
            this.btnHomeMapPan.Click += new System.EventHandler(this.OnMapPan);
            // 
            // btnHomeMapZoomIn
            // 
            this.btnHomeMapZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomIn.Image")));
            this.btnHomeMapZoomIn.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomIn.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapZoomIn.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomIn.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomIn.SmallImage")));
            this.btnHomeMapZoomIn.Text = "Zoom In";
            this.btnHomeMapZoomIn.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapZoomIn.ToolTip = "Zoom In";
            this.btnHomeMapZoomIn.Click += new System.EventHandler(this.OnMapZoomIn);
            // 
            // btnHomeMapZoomOut
            // 
            this.btnHomeMapZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomOut.Image")));
            this.btnHomeMapZoomOut.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomOut.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapZoomOut.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomOut.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomOut.SmallImage")));
            this.btnHomeMapZoomOut.Text = "Zoom Out";
            this.btnHomeMapZoomOut.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapZoomOut.ToolTip = "Zoom Out";
            this.btnHomeMapZoomOut.Click += new System.EventHandler(this.OnMapZoomOut);
            // 
            // btnHomeMapZoomToSelection
            // 
            this.btnHomeMapZoomToSelection.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomToSelection.Image")));
            this.btnHomeMapZoomToSelection.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomToSelection.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapZoomToSelection.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapZoomToSelection.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapZoomToSelection.SmallImage")));
            this.btnHomeMapZoomToSelection.Text = "Zoom To Selection";
            this.btnHomeMapZoomToSelection.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapZoomToSelection.ToolTip = "Zoom To Selection";
            this.btnHomeMapZoomToSelection.Click += new System.EventHandler(this.OnMapZoomToSelection);
            // 
            // btnHomeMapFullExtent
            // 
            this.btnHomeMapFullExtent.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapFullExtent.Image")));
            this.btnHomeMapFullExtent.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapFullExtent.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapFullExtent.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapFullExtent.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapFullExtent.SmallImage")));
            this.btnHomeMapFullExtent.Text = "Full Extent";
            this.btnHomeMapFullExtent.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapFullExtent.ToolTip = "Full Extent";
            this.btnHomeMapFullExtent.Click += new System.EventHandler(this.OnMapFullExtent);
            // 
            // btnHomeMapPreviousExtent
            // 
            this.btnHomeMapPreviousExtent.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapPreviousExtent.Image")));
            this.btnHomeMapPreviousExtent.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapPreviousExtent.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapPreviousExtent.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapPreviousExtent.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapPreviousExtent.SmallImage")));
            this.btnHomeMapPreviousExtent.Text = "Previous Extent";
            this.btnHomeMapPreviousExtent.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapPreviousExtent.ToolTip = "Previous Extent";
            this.btnHomeMapPreviousExtent.Click += new System.EventHandler(this.OnMapPreviousExtent);
            // 
            // btnHomeMapNextExtent
            // 
            this.btnHomeMapNextExtent.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapNextExtent.Image")));
            this.btnHomeMapNextExtent.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapNextExtent.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapNextExtent.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapNextExtent.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapNextExtent.SmallImage")));
            this.btnHomeMapNextExtent.Text = "Next Extent";
            this.btnHomeMapNextExtent.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapNextExtent.ToolTip = "Next Extent";
            this.btnHomeMapNextExtent.Click += new System.EventHandler(this.OnMapNextExtent);
            // 
            // btnHomeMapRefresh
            // 
            this.btnHomeMapRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapRefresh.Image")));
            this.btnHomeMapRefresh.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapRefresh.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapRefresh.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapRefresh.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapRefresh.SmallImage")));
            this.btnHomeMapRefresh.Text = "Map Refresh";
            this.btnHomeMapRefresh.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapRefresh.ToolTip = "Map Refresh";
            this.btnHomeMapRefresh.Click += new System.EventHandler(this.OnMapRefresh);
            // 
            // btnHomeMapClearSelection
            // 
            this.btnHomeMapClearSelection.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapClearSelection.Image")));
            this.btnHomeMapClearSelection.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapClearSelection.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapClearSelection.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapClearSelection.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapClearSelection.SmallImage")));
            this.btnHomeMapClearSelection.Text = "Clear Selection";
            this.btnHomeMapClearSelection.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapClearSelection.ToolTip = "Clear Selection";
            this.btnHomeMapClearSelection.Click += new System.EventHandler(this.OnMapClearSelection);
            // 
            // btnHomeMapRuler
            // 
            this.btnHomeMapRuler.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeMapRuler.Image")));
            this.btnHomeMapRuler.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapRuler.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeMapRuler.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeMapRuler.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeMapRuler.SmallImage")));
            this.btnHomeMapRuler.Text = "Ruler Tool";
            this.btnHomeMapRuler.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeMapRuler.ToolTip = "Ruler Tool";
            this.btnHomeMapRuler.Click += new System.EventHandler(this.OnMapRuler);
            // 
            // homeToolbox
            // 
            this.homeToolbox.Items.Add(this.btnHomeToolboxGoTo);
            this.homeToolbox.Text = "Toolbox";
            // 
            // btnHomeToolboxGoTo
            // 
            this.btnHomeToolboxGoTo.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeToolboxGoTo.Image")));
            this.btnHomeToolboxGoTo.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeToolboxGoTo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeToolboxGoTo.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeToolboxGoTo.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeToolboxGoTo.SmallImage")));
            this.btnHomeToolboxGoTo.Text = "Go To";
            this.btnHomeToolboxGoTo.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnHomeToolboxGoTo.ToolTip = "Go To";
            this.btnHomeToolboxGoTo.Click += new System.EventHandler(this.OnToolboxGoTo);
            // 
            // homeWindowPanel
            // 
            this.homeWindowPanel.Items.Add(this.btnHomeWindowLayer);
            this.homeWindowPanel.Items.Add(this.btnHomeWindowOutput);
            this.homeWindowPanel.Items.Add(this.btnHomeWindowSelections);
            this.homeWindowPanel.Text = "Window";
            // 
            // btnHomeWindowLayer
            // 
            this.btnHomeWindowLayer.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowLayer.Image")));
            this.btnHomeWindowLayer.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowLayer.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeWindowLayer.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowLayer.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowLayer.SmallImage")));
            this.btnHomeWindowLayer.Text = "Layer View";
            this.btnHomeWindowLayer.ToolTip = "Layer View";
            this.btnHomeWindowLayer.Click += new System.EventHandler(this.OnWindowLayer);
            // 
            // btnHomeWindowOutput
            // 
            this.btnHomeWindowOutput.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowOutput.Image")));
            this.btnHomeWindowOutput.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowOutput.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeWindowOutput.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowOutput.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowOutput.SmallImage")));
            this.btnHomeWindowOutput.Text = "Output View";
            this.btnHomeWindowOutput.ToolTip = "Output View";
            this.btnHomeWindowOutput.Click += new System.EventHandler(this.OnWindowOutput);
            // 
            // btnHomeWindowSelections
            // 
            this.btnHomeWindowSelections.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowSelections.Image")));
            this.btnHomeWindowSelections.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowSelections.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnHomeWindowSelections.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnHomeWindowSelections.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeWindowSelections.SmallImage")));
            this.btnHomeWindowSelections.Text = "Selection View";
            this.btnHomeWindowSelections.ToolTip = "Selection View";
            this.btnHomeWindowSelections.Click += new System.EventHandler(this.OnWindowSelections_Click);
            // 
            // editorTab
            // 
            this.editorTab.Panels.Add(this.editorGeneralPanel);
            this.editorTab.Text = "Editor";
            // 
            // editorGeneralPanel
            // 
            this.editorGeneralPanel.Items.Add(this.btnEditorDrawLine);
            this.editorGeneralPanel.Items.Add(this.btnEditorDrawCircle);
            this.editorGeneralPanel.Items.Add(this.btnEditorDrawRectangle);
            this.editorGeneralPanel.Items.Add(this.btnEditorDrawPolygon);
            this.editorGeneralPanel.Items.Add(this.btnEditorDrawClearDrawings);
            this.editorGeneralPanel.Text = "General";
            // 
            // btnEditorDrawLine
            // 
            this.btnEditorDrawLine.Image = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawLine.Image")));
            this.btnEditorDrawLine.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnEditorDrawLine.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawLine.SmallImage")));
            this.btnEditorDrawLine.Text = "Line";
            this.btnEditorDrawLine.ToolTip = "Line";
            this.btnEditorDrawLine.Click += new System.EventHandler(this.btnEditorDrawLine_Click);
            // 
            // btnEditorDrawCircle
            // 
            this.btnEditorDrawCircle.Image = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawCircle.Image")));
            this.btnEditorDrawCircle.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnEditorDrawCircle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawCircle.SmallImage")));
            this.btnEditorDrawCircle.Text = "Circle";
            this.btnEditorDrawCircle.ToolTip = "Circle";
            this.btnEditorDrawCircle.Click += new System.EventHandler(this.btnEditorDrawCircle_Click);
            // 
            // btnEditorDrawRectangle
            // 
            this.btnEditorDrawRectangle.Image = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawRectangle.Image")));
            this.btnEditorDrawRectangle.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnEditorDrawRectangle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawRectangle.SmallImage")));
            this.btnEditorDrawRectangle.Text = "Rectangle";
            this.btnEditorDrawRectangle.ToolTip = "Rectangle";
            this.btnEditorDrawRectangle.Click += new System.EventHandler(this.btnEditorDrawRectangle_Click);
            // 
            // btnEditorDrawPolygon
            // 
            this.btnEditorDrawPolygon.Image = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawPolygon.Image")));
            this.btnEditorDrawPolygon.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnEditorDrawPolygon.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawPolygon.SmallImage")));
            this.btnEditorDrawPolygon.Text = "Poylgon";
            this.btnEditorDrawPolygon.ToolTip = "Polygon";
            this.btnEditorDrawPolygon.Click += new System.EventHandler(this.btnEditorDrawPolygon_Click);
            // 
            // btnEditorDrawClearDrawings
            // 
            this.btnEditorDrawClearDrawings.Image = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawClearDrawings.Image")));
            this.btnEditorDrawClearDrawings.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnEditorDrawClearDrawings.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnEditorDrawClearDrawings.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnEditorDrawClearDrawings.SmallImage")));
            this.btnEditorDrawClearDrawings.Text = "Clear Drawings";
            this.btnEditorDrawClearDrawings.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnEditorDrawClearDrawings.Click += new System.EventHandler(this.btnEditorDrawClearDrawings_Click);
            // 
            // streetTab
            // 
            this.streetTab.Panels.Add(this.streetGeneralPanel);
            this.streetTab.Panels.Add(this.streetVertexPanel);
            this.streetTab.Panels.Add(this.streetRestrictionPanel);
            this.streetTab.Text = "Street";
            // 
            // streetGeneralPanel
            // 
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralSearch);
            this.streetGeneralPanel.Items.Add(this.ribbonSeparator1);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralAdd);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralEdit);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralDelete);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralSplit);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralUnion);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralCopy);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralFlip);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralShowProperty);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralReshape);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralCopyAttribute);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralSearchById);
            this.streetGeneralPanel.Items.Add(this.btnStreetGeneralSearchByName);
            this.streetGeneralPanel.Items.Add(this.ribbonSeparator1);
            this.streetGeneralPanel.Text = "General";
            // 
            // btnStreetGeneralSearch
            // 
            this.btnStreetGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearch.Image")));
            this.btnStreetGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearch.SmallImage")));
            this.btnStreetGeneralSearch.Text = "Search";
            this.btnStreetGeneralSearch.ToolTip = "Search Street";
            this.btnStreetGeneralSearch.Click += new System.EventHandler(this.btnStreetGeneralSearch_Click);
            // 
            // btnStreetGeneralAdd
            // 
            this.btnStreetGeneralAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralAdd.Image")));
            this.btnStreetGeneralAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralAdd.SmallImage")));
            this.btnStreetGeneralAdd.Text = "Add";
            this.btnStreetGeneralAdd.ToolTip = "Add Street";
            this.btnStreetGeneralAdd.Click += new System.EventHandler(this.btnStreetGeneralAdd_Click);
            // 
            // btnStreetGeneralEdit
            // 
            this.btnStreetGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralEdit.Image")));
            this.btnStreetGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralEdit.SmallImage")));
            this.btnStreetGeneralEdit.Text = "Edit";
            this.btnStreetGeneralEdit.ToolTip = "Edit Street";
            this.btnStreetGeneralEdit.Click += new System.EventHandler(this.btnStreetGeneralEdit_Click);
            // 
            // btnStreetGeneralDelete
            // 
            this.btnStreetGeneralDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralDelete.Image")));
            this.btnStreetGeneralDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralDelete.SmallImage")));
            this.btnStreetGeneralDelete.Text = "Delete";
            this.btnStreetGeneralDelete.ToolTip = "Delete Street";
            this.btnStreetGeneralDelete.Click += new System.EventHandler(this.btnStreetGeneralDelete_Click);
            // 
            // btnStreetGeneralSplit
            // 
            this.btnStreetGeneralSplit.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSplit.Image")));
            this.btnStreetGeneralSplit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralSplit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSplit.SmallImage")));
            this.btnStreetGeneralSplit.Text = "Split";
            this.btnStreetGeneralSplit.ToolTip = "Split Street";
            this.btnStreetGeneralSplit.Click += new System.EventHandler(this.btnStreetGeneralSplit_Click);
            // 
            // btnStreetGeneralUnion
            // 
            this.btnStreetGeneralUnion.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralUnion.Image")));
            this.btnStreetGeneralUnion.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralUnion.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralUnion.SmallImage")));
            this.btnStreetGeneralUnion.Text = "Union";
            this.btnStreetGeneralUnion.ToolTip = "Union Street";
            this.btnStreetGeneralUnion.Click += new System.EventHandler(this.btnStreetGeneralUnion_Click);
            // 
            // btnStreetGeneralCopy
            // 
            this.btnStreetGeneralCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralCopy.Image")));
            this.btnStreetGeneralCopy.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralCopy.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralCopy.SmallImage")));
            this.btnStreetGeneralCopy.Text = "Copy Drag";
            this.btnStreetGeneralCopy.ToolTip = "Copy Street";
            this.btnStreetGeneralCopy.Click += new System.EventHandler(this.btnStreetGeneralCopy_Click);
            // 
            // btnStreetGeneralFlip
            // 
            this.btnStreetGeneralFlip.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralFlip.Image")));
            this.btnStreetGeneralFlip.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralFlip.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralFlip.SmallImage")));
            this.btnStreetGeneralFlip.Text = "Flip";
            this.btnStreetGeneralFlip.ToolTip = "Flip Street";
            this.btnStreetGeneralFlip.Click += new System.EventHandler(this.btnStreetGeneralFlip_Click);
            // 
            // btnStreetGeneralShowProperty
            // 
            this.btnStreetGeneralShowProperty.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralShowProperty.Image")));
            this.btnStreetGeneralShowProperty.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralShowProperty.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralShowProperty.SmallImage")));
            this.btnStreetGeneralShowProperty.Text = "Show Property";
            this.btnStreetGeneralShowProperty.ToolTip = "Show Street Property";
            this.btnStreetGeneralShowProperty.Click += new System.EventHandler(this.btnStreetGeneralShowProperty_Click);
            // 
            // btnStreetGeneralReshape
            // 
            this.btnStreetGeneralReshape.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralReshape.Image")));
            this.btnStreetGeneralReshape.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralReshape.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralReshape.SmallImage")));
            this.btnStreetGeneralReshape.Text = "Reshape";
            this.btnStreetGeneralReshape.ToolTip = "Reshape";
            this.btnStreetGeneralReshape.Click += new System.EventHandler(this.btnStreetGeneralReshape_Click);
            // 
            // btnStreetGeneralCopyAttribute
            // 
            this.btnStreetGeneralCopyAttribute.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralCopyAttribute.Image")));
            this.btnStreetGeneralCopyAttribute.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralCopyAttribute.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralCopyAttribute.SmallImage")));
            this.btnStreetGeneralCopyAttribute.Text = "Copy Attribute";
            this.btnStreetGeneralCopyAttribute.ToolTip = "Copy Attribute";
            this.btnStreetGeneralCopyAttribute.Click += new System.EventHandler(this.btnStreetGeneralCopyAttribute_Click);
            // 
            // btnStreetGeneralSearchById
            // 
            this.btnStreetGeneralSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearchById.Image")));
            this.btnStreetGeneralSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearchById.SmallImage")));
            this.btnStreetGeneralSearchById.Text = "Search by Id";
            this.btnStreetGeneralSearchById.ToolTip = "Search By Id";
            this.btnStreetGeneralSearchById.Click += new System.EventHandler(this.btnStreetGeneralSearchById_Click);
            // 
            // btnStreetGeneralSearchByName
            // 
            this.btnStreetGeneralSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearchByName.Image")));
            this.btnStreetGeneralSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetGeneralSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetGeneralSearchByName.SmallImage")));
            this.btnStreetGeneralSearchByName.Text = "Search by Name";
            this.btnStreetGeneralSearchByName.ToolTip = "Search By Name";
            this.btnStreetGeneralSearchByName.Click += new System.EventHandler(this.btnStreetGeneralSearchByName_Click);
            // 
            // streetVertexPanel
            // 
            this.streetVertexPanel.Items.Add(this.btnStreetVertexAdd);
            this.streetVertexPanel.Items.Add(this.btnStreetVertexMove);
            this.streetVertexPanel.Items.Add(this.btnStreetVertexDelete);
            this.streetVertexPanel.Text = "Vertex";
            // 
            // btnStreetVertexAdd
            // 
            this.btnStreetVertexAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexAdd.Image")));
            this.btnStreetVertexAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetVertexAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexAdd.SmallImage")));
            this.btnStreetVertexAdd.Text = "Add";
            this.btnStreetVertexAdd.ToolTip = "Add Street Vertex";
            this.btnStreetVertexAdd.Click += new System.EventHandler(this.btnStreetVertexAdd_Click);
            // 
            // btnStreetVertexMove
            // 
            this.btnStreetVertexMove.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexMove.Image")));
            this.btnStreetVertexMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetVertexMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexMove.SmallImage")));
            this.btnStreetVertexMove.Text = "Move";
            this.btnStreetVertexMove.ToolTip = "Move Street Vertex";
            this.btnStreetVertexMove.Click += new System.EventHandler(this.btnStreetVertexMove_Click);
            // 
            // btnStreetVertexDelete
            // 
            this.btnStreetVertexDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexDelete.Image")));
            this.btnStreetVertexDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetVertexDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetVertexDelete.SmallImage")));
            this.btnStreetVertexDelete.Text = "Delete";
            this.btnStreetVertexDelete.ToolTip = "Delete Street Vertex";
            this.btnStreetVertexDelete.Click += new System.EventHandler(this.btnStreetVertexDelete_Click);
            // 
            // streetRestrictionPanel
            // 
            this.streetRestrictionPanel.Items.Add(this.btnStreetRestrictionSearch);
            this.streetRestrictionPanel.Items.Add(this.ribbonSeparator9);
            this.streetRestrictionPanel.Items.Add(this.btnStreetRestrictionAdd);
            this.streetRestrictionPanel.Items.Add(this.btnStreetRestrictionDelete);
            this.streetRestrictionPanel.Items.Add(this.btnStreetRestrictionMove);
            this.streetRestrictionPanel.Items.Add(this.btnStreetRestrictionShowStreet);
            this.streetRestrictionPanel.Text = "Restriction";
            // 
            // btnStreetRestrictionSearch
            // 
            this.btnStreetRestrictionSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionSearch.Image")));
            this.btnStreetRestrictionSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionSearch.SmallImage")));
            this.btnStreetRestrictionSearch.Text = "Search";
            this.btnStreetRestrictionSearch.Click += new System.EventHandler(this.btnStreetRestrictionSearch_Click);
            // 
            // btnStreetRestrictionAdd
            // 
            this.btnStreetRestrictionAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionAdd.Image")));
            this.btnStreetRestrictionAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetRestrictionAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionAdd.SmallImage")));
            this.btnStreetRestrictionAdd.Text = "Add";
            this.btnStreetRestrictionAdd.ToolTip = "Add";
            this.btnStreetRestrictionAdd.Click += new System.EventHandler(this.btnStreetRestrictionAdd_Click);
            // 
            // btnStreetRestrictionDelete
            // 
            this.btnStreetRestrictionDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionDelete.Image")));
            this.btnStreetRestrictionDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetRestrictionDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionDelete.SmallImage")));
            this.btnStreetRestrictionDelete.Text = "Delete";
            this.btnStreetRestrictionDelete.ToolTip = "Delete";
            this.btnStreetRestrictionDelete.Click += new System.EventHandler(this.btnStreetRestrictionDelete_Click);
            // 
            // btnStreetRestrictionMove
            // 
            this.btnStreetRestrictionMove.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionMove.Image")));
            this.btnStreetRestrictionMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetRestrictionMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionMove.SmallImage")));
            this.btnStreetRestrictionMove.Text = "Move";
            this.btnStreetRestrictionMove.ToolTip = "Move";
            this.btnStreetRestrictionMove.Click += new System.EventHandler(this.btnStreetRestrictionMove_Click);
            // 
            // btnStreetRestrictionShowStreet
            // 
            this.btnStreetRestrictionShowStreet.Image = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionShowStreet.Image")));
            this.btnStreetRestrictionShowStreet.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnStreetRestrictionShowStreet.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnStreetRestrictionShowStreet.SmallImage")));
            this.btnStreetRestrictionShowStreet.Text = "Show Street";
            this.btnStreetRestrictionShowStreet.ToolTip = "Show Street";
            this.btnStreetRestrictionShowStreet.Click += new System.EventHandler(this.btnStreetRestrictionShowStreet_Click);
            // 
            // junctionTab
            // 
            this.junctionTab.Panels.Add(this.junctionGeneralPanel);
            this.junctionTab.Text = "Junction";
            // 
            // junctionGeneralPanel
            // 
            this.junctionGeneralPanel.Items.Add(this.btnJunctionGeneralSearch);
            this.junctionGeneralPanel.Items.Add(this.ribbonSeparator3);
            this.junctionGeneralPanel.Items.Add(this.btnJunctionGeneralEdit);
            this.junctionGeneralPanel.Items.Add(this.btnJunctionGeneralDelete);
            this.junctionGeneralPanel.Items.Add(this.btnJunctionGeneralMove);
            this.junctionGeneralPanel.Items.Add(this.btnJunctionGeneralSplit);
            this.junctionGeneralPanel.Items.Add(this.btnJunctionSearchById);
            this.junctionGeneralPanel.Text = "General";
            // 
            // btnJunctionGeneralSearch
            // 
            this.btnJunctionGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralSearch.Image")));
            this.btnJunctionGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralSearch.SmallImage")));
            this.btnJunctionGeneralSearch.Text = "Search";
            this.btnJunctionGeneralSearch.Click += new System.EventHandler(this.btnJunctionGeneralSearch_Click);
            // 
            // btnJunctionGeneralEdit
            // 
            this.btnJunctionGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralEdit.Image")));
            this.btnJunctionGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralEdit.SmallImage")));
            this.btnJunctionGeneralEdit.Text = "Edit";
            this.btnJunctionGeneralEdit.Click += new System.EventHandler(this.btnJunctionGeneralEdit_Click);
            // 
            // btnJunctionGeneralDelete
            // 
            this.btnJunctionGeneralDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralDelete.Image")));
            this.btnJunctionGeneralDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionGeneralDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralDelete.SmallImage")));
            this.btnJunctionGeneralDelete.Text = "Delete";
            this.btnJunctionGeneralDelete.Click += new System.EventHandler(this.btnJunctionGeneralDelete_Click);
            // 
            // btnJunctionGeneralMove
            // 
            this.btnJunctionGeneralMove.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralMove.Image")));
            this.btnJunctionGeneralMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionGeneralMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralMove.SmallImage")));
            this.btnJunctionGeneralMove.Text = "Move";
            this.btnJunctionGeneralMove.ToolTip = "Move Junction";
            this.btnJunctionGeneralMove.Click += new System.EventHandler(this.btnJunctionGeneralMove_Click);
            // 
            // btnJunctionGeneralSplit
            // 
            this.btnJunctionGeneralSplit.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralSplit.Image")));
            this.btnJunctionGeneralSplit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionGeneralSplit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralSplit.SmallImage")));
            this.btnJunctionGeneralSplit.Text = "Split";
            this.btnJunctionGeneralSplit.ToolTip = "Split";
            this.btnJunctionGeneralSplit.Click += new System.EventHandler(this.btnJunctionGeneralSplit_Click);
            // 
            // btnJunctionSearchById
            // 
            this.btnJunctionSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionSearchById.Image")));
            this.btnJunctionSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionSearchById.SmallImage")));
            this.btnJunctionSearchById.Text = "Search by Id";
            this.btnJunctionSearchById.ToolTip = "Search by Id";
            this.btnJunctionSearchById.Click += new System.EventHandler(this.btnJunctionSearchById_Click);
            // 
            // propertyTab
            // 
            this.propertyTab.Panels.Add(this.propertyGeneralPanel);
            this.propertyTab.Panels.Add(this.propertyFloorPanel);
            this.propertyTab.Text = "Property";
            // 
            // propertyGeneralPanel
            // 
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralSearch);
            this.propertyGeneralPanel.Items.Add(this.ribbonSeparator4);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralAdd);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralAddMultiple);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralEdit);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralEditMultiple);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralDelete);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralReassociate);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralMove);
            this.propertyGeneralPanel.Items.Add(this.btnPropertyGeneralSearchById);
            this.propertyGeneralPanel.Text = "General";
            // 
            // btnPropertyGeneralSearch
            // 
            this.btnPropertyGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearch.Image")));
            this.btnPropertyGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearch.SmallImage")));
            this.btnPropertyGeneralSearch.Text = "Search";
            this.btnPropertyGeneralSearch.Click += new System.EventHandler(this.btnPropertyGeneralSearch_Click);
            // 
            // btnPropertyGeneralAdd
            // 
            this.btnPropertyGeneralAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralAdd.Image")));
            this.btnPropertyGeneralAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralAdd.SmallImage")));
            this.btnPropertyGeneralAdd.Text = "Add";
            this.btnPropertyGeneralAdd.Click += new System.EventHandler(this.btnPropertyGeneralAdd_Click);
            // 
            // btnPropertyGeneralAddMultiple
            // 
            this.btnPropertyGeneralAddMultiple.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralAddMultiple.Image")));
            this.btnPropertyGeneralAddMultiple.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralAddMultiple.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralAddMultiple.SmallImage")));
            this.btnPropertyGeneralAddMultiple.Text = "Add Multiple";
            this.btnPropertyGeneralAddMultiple.ToolTip = "Add Multiple";
            this.btnPropertyGeneralAddMultiple.Click += new System.EventHandler(this.btnPropertyGeneralAddMultiple_Click);
            // 
            // btnPropertyGeneralEdit
            // 
            this.btnPropertyGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralEdit.Image")));
            this.btnPropertyGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralEdit.SmallImage")));
            this.btnPropertyGeneralEdit.Text = "Edit";
            this.btnPropertyGeneralEdit.Click += new System.EventHandler(this.btnPropertyGeneralEdit_Click);
            // 
            // btnPropertyGeneralEditMultiple
            // 
            this.btnPropertyGeneralEditMultiple.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralEditMultiple.Image")));
            this.btnPropertyGeneralEditMultiple.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralEditMultiple.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralEditMultiple.SmallImage")));
            this.btnPropertyGeneralEditMultiple.Text = "Edit Multiple";
            this.btnPropertyGeneralEditMultiple.ToolTip = "Edit Multiple";
            this.btnPropertyGeneralEditMultiple.Click += new System.EventHandler(this.btnPropertyGeneralEditMultiple_Click);
            // 
            // btnPropertyGeneralDelete
            // 
            this.btnPropertyGeneralDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralDelete.Image")));
            this.btnPropertyGeneralDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralDelete.SmallImage")));
            this.btnPropertyGeneralDelete.Text = "Delete";
            this.btnPropertyGeneralDelete.Click += new System.EventHandler(this.btnPropertyGeneralDelete_Click);
            // 
            // btnPropertyGeneralReassociate
            // 
            this.btnPropertyGeneralReassociate.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralReassociate.Image")));
            this.btnPropertyGeneralReassociate.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralReassociate.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralReassociate.SmallImage")));
            this.btnPropertyGeneralReassociate.Text = "Reassociate";
            this.btnPropertyGeneralReassociate.Click += new System.EventHandler(this.btnPropertyGeneralReassociate_Click);
            // 
            // btnPropertyGeneralMove
            // 
            this.btnPropertyGeneralMove.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralMove.Image")));
            this.btnPropertyGeneralMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralMove.SmallImage")));
            this.btnPropertyGeneralMove.Text = "Move";
            this.btnPropertyGeneralMove.ToolTip = "Move Property";
            this.btnPropertyGeneralMove.Click += new System.EventHandler(this.btnPropertyGeneralMove_Click);
            // 
            // btnPropertyGeneralSearchById
            // 
            this.btnPropertyGeneralSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearchById.Image")));
            this.btnPropertyGeneralSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearchById.SmallImage")));
            this.btnPropertyGeneralSearchById.Text = "Search by Id";
            this.btnPropertyGeneralSearchById.ToolTip = "Search By Id";
            this.btnPropertyGeneralSearchById.Click += new System.EventHandler(this.btnPropertyGeneralSearchById_Click);
            // 
            // propertyFloorPanel
            // 
            this.propertyFloorPanel.Items.Add(this.btnPropertyFloorSearch);
            this.propertyFloorPanel.Items.Add(this.btnPropertyFloorSearchById);
            this.propertyFloorPanel.Text = "Floor";
            // 
            // btnPropertyFloorSearch
            // 
            this.btnPropertyFloorSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyFloorSearch.Image")));
            this.btnPropertyFloorSearch.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyFloorSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyFloorSearch.SmallImage")));
            this.btnPropertyFloorSearch.Text = "Advanced Search";
            this.btnPropertyFloorSearch.Click += new System.EventHandler(this.btnPropertyFloorSearch_Click);
            // 
            // btnPropertyFloorSearchById
            // 
            this.btnPropertyFloorSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyFloorSearchById.Image")));
            this.btnPropertyFloorSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyFloorSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyFloorSearchById.SmallImage")));
            this.btnPropertyFloorSearchById.Text = "Search By Id";
            this.btnPropertyFloorSearchById.Click += new System.EventHandler(this.btnPropertyFloorSearchById_Click);
            // 
            // buildingTab
            // 
            this.buildingTab.Panels.Add(this.buildingGeneralPanel);
            this.buildingTab.Panels.Add(this.buildingGroupPanel);
            this.buildingTab.Panels.Add(this.buildingMultiStoreyPanel);
            this.buildingTab.Text = "Building";
            // 
            // buildingGeneralPanel
            // 
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralSearch);
            this.buildingGeneralPanel.Items.Add(this.ribbonSeparator2);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralAdd);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralEdit);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralDelete);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralMove);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralReassociate);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralSearchById);
            this.buildingGeneralPanel.Items.Add(this.btnBuildingGeneralSearchByName);
            this.buildingGeneralPanel.Text = "Building";
            // 
            // btnBuildingGeneralSearch
            // 
            this.btnBuildingGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearch.Image")));
            this.btnBuildingGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearch.SmallImage")));
            this.btnBuildingGeneralSearch.Text = "Search";
            this.btnBuildingGeneralSearch.Click += new System.EventHandler(this.btnBuildingGeneralSearch_Click);
            // 
            // btnBuildingGeneralAdd
            // 
            this.btnBuildingGeneralAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralAdd.Image")));
            this.btnBuildingGeneralAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralAdd.SmallImage")));
            this.btnBuildingGeneralAdd.Text = "Add";
            this.btnBuildingGeneralAdd.Click += new System.EventHandler(this.btnBuildingGeneralAdd_Click);
            // 
            // btnBuildingGeneralEdit
            // 
            this.btnBuildingGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralEdit.Image")));
            this.btnBuildingGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralEdit.SmallImage")));
            this.btnBuildingGeneralEdit.Text = "Edit";
            this.btnBuildingGeneralEdit.Click += new System.EventHandler(this.btnBuildingGeneralEdit_Click);
            // 
            // btnBuildingGeneralDelete
            // 
            this.btnBuildingGeneralDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralDelete.Image")));
            this.btnBuildingGeneralDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralDelete.SmallImage")));
            this.btnBuildingGeneralDelete.Text = "Delete";
            this.btnBuildingGeneralDelete.Click += new System.EventHandler(this.btnBuildingGeneralDelete_Click);
            // 
            // btnBuildingGeneralMove
            // 
            this.btnBuildingGeneralMove.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralMove.Image")));
            this.btnBuildingGeneralMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralMove.SmallImage")));
            this.btnBuildingGeneralMove.Text = "Move";
            this.btnBuildingGeneralMove.ToolTip = "Move Building";
            this.btnBuildingGeneralMove.Click += new System.EventHandler(this.btnBuildingGeneralMove_Click);
            // 
            // btnBuildingGeneralReassociate
            // 
            this.btnBuildingGeneralReassociate.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralReassociate.Image")));
            this.btnBuildingGeneralReassociate.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralReassociate.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralReassociate.SmallImage")));
            this.btnBuildingGeneralReassociate.Text = "Reassociate";
            this.btnBuildingGeneralReassociate.ToolTip = "Reassoicate Property";
            this.btnBuildingGeneralReassociate.Click += new System.EventHandler(this.btnBuildingGeneralReassociate_Click);
            // 
            // btnBuildingGeneralSearchById
            // 
            this.btnBuildingGeneralSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearchById.Image")));
            this.btnBuildingGeneralSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearchById.SmallImage")));
            this.btnBuildingGeneralSearchById.Text = "Search By Id";
            this.btnBuildingGeneralSearchById.ToolTip = "Search By Id";
            this.btnBuildingGeneralSearchById.Click += new System.EventHandler(this.btnBuildingGeneralSearchById_Click);
            // 
            // btnBuildingGeneralSearchByName
            // 
            this.btnBuildingGeneralSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearchByName.Image")));
            this.btnBuildingGeneralSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGeneralSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGeneralSearchByName.SmallImage")));
            this.btnBuildingGeneralSearchByName.Text = "Search By Name";
            this.btnBuildingGeneralSearchByName.ToolTip = "Search By Name";
            this.btnBuildingGeneralSearchByName.Click += new System.EventHandler(this.btnBuildingGeneralSearchByName_Click);
            // 
            // buildingGroupPanel
            // 
            this.buildingGroupPanel.Items.Add(this.ribbonSeparator5);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupSearch);
            this.buildingGroupPanel.Items.Add(this.ribbonSeparator5);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupAdd);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupEdit);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupDelete);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupReassociate);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupMove);
            this.buildingGroupPanel.Items.Add(this.btnAssociateBuildingToBuildingGroup);
            this.buildingGroupPanel.Items.Add(this.btnDissociateBuildingToBuildingGroup);
            this.buildingGroupPanel.Items.Add(this.btnAssociateBuildingByDistance);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupSearchById);
            this.buildingGroupPanel.Items.Add(this.btnBuildingGroupSearchByName);
            this.buildingGroupPanel.Text = "Building Group";
            // 
            // btnBuildingGroupSearch
            // 
            this.btnBuildingGroupSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearch.Image")));
            this.btnBuildingGroupSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearch.SmallImage")));
            this.btnBuildingGroupSearch.Text = "Search";
            this.btnBuildingGroupSearch.Click += new System.EventHandler(this.btnBuildingGroupSearch_Click);
            // 
            // btnBuildingGroupAdd
            // 
            this.btnBuildingGroupAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupAdd.Image")));
            this.btnBuildingGroupAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupAdd.SmallImage")));
            this.btnBuildingGroupAdd.Text = "Add";
            this.btnBuildingGroupAdd.Click += new System.EventHandler(this.btnBuildingGroupAdd_Click);
            // 
            // btnBuildingGroupEdit
            // 
            this.btnBuildingGroupEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupEdit.Image")));
            this.btnBuildingGroupEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupEdit.SmallImage")));
            this.btnBuildingGroupEdit.Text = "Edit";
            this.btnBuildingGroupEdit.Click += new System.EventHandler(this.btnBuildingGroupEdit_Click);
            // 
            // btnBuildingGroupDelete
            // 
            this.btnBuildingGroupDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupDelete.Image")));
            this.btnBuildingGroupDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupDelete.SmallImage")));
            this.btnBuildingGroupDelete.Text = "Delete";
            this.btnBuildingGroupDelete.Click += new System.EventHandler(this.btnBuildingGroupDelete_Click);
            // 
            // btnBuildingGroupReassociate
            // 
            this.btnBuildingGroupReassociate.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupReassociate.Image")));
            this.btnBuildingGroupReassociate.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupReassociate.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupReassociate.SmallImage")));
            this.btnBuildingGroupReassociate.Text = "Reassociate";
            this.btnBuildingGroupReassociate.Click += new System.EventHandler(this.btnBuildingGroupReassociate_Click);
            // 
            // btnBuildingGroupMove
            // 
            this.btnBuildingGroupMove.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupMove.Image")));
            this.btnBuildingGroupMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupMove.SmallImage")));
            this.btnBuildingGroupMove.Text = "Move";
            this.btnBuildingGroupMove.ToolTip = "Move Building Group";
            this.btnBuildingGroupMove.Click += new System.EventHandler(this.btnBuildingGroupMove_Click);
            // 
            // btnAssociateBuildingToBuildingGroup
            // 
            this.btnAssociateBuildingToBuildingGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnAssociateBuildingToBuildingGroup.Image")));
            this.btnAssociateBuildingToBuildingGroup.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnAssociateBuildingToBuildingGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnAssociateBuildingToBuildingGroup.SmallImage")));
            this.btnAssociateBuildingToBuildingGroup.Text = "Associate Building Group";
            this.btnAssociateBuildingToBuildingGroup.ToolTip = "Associate Building to Building Group";
            this.btnAssociateBuildingToBuildingGroup.Click += new System.EventHandler(this.btnAssociateBuildingToBuildingGroup_Click);
            // 
            // btnDissociateBuildingToBuildingGroup
            // 
            this.btnDissociateBuildingToBuildingGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnDissociateBuildingToBuildingGroup.Image")));
            this.btnDissociateBuildingToBuildingGroup.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnDissociateBuildingToBuildingGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDissociateBuildingToBuildingGroup.SmallImage")));
            this.btnDissociateBuildingToBuildingGroup.Text = "Dissociate Building Group";
            this.btnDissociateBuildingToBuildingGroup.Click += new System.EventHandler(this.btnDissociateBuildingToBuildingGroup_Click);
            // 
            // btnAssociateBuildingByDistance
            // 
            this.btnAssociateBuildingByDistance.Image = ((System.Drawing.Image)(resources.GetObject("btnAssociateBuildingByDistance.Image")));
            this.btnAssociateBuildingByDistance.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnAssociateBuildingByDistance.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnAssociateBuildingByDistance.SmallImage")));
            this.btnAssociateBuildingByDistance.Text = "Associate Building By Distance";
            this.btnAssociateBuildingByDistance.ToolTip = "Associate Building By Distance";
            this.btnAssociateBuildingByDistance.Click += new System.EventHandler(this.btnAssociateBuildingByDistance_Click);
            // 
            // btnBuildingGroupSearchById
            // 
            this.btnBuildingGroupSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearchById.Image")));
            this.btnBuildingGroupSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearchById.SmallImage")));
            this.btnBuildingGroupSearchById.Text = "Search By Id";
            this.btnBuildingGroupSearchById.ToolTip = "Search By Id";
            this.btnBuildingGroupSearchById.Click += new System.EventHandler(this.btnBuildingGroupSearchById_Click);
            // 
            // btnBuildingGroupSearchByName
            // 
            this.btnBuildingGroupSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearchByName.Image")));
            this.btnBuildingGroupSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingGroupSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingGroupSearchByName.SmallImage")));
            this.btnBuildingGroupSearchByName.Text = "Search By Name";
            this.btnBuildingGroupSearchByName.ToolTip = "Search By Name";
            this.btnBuildingGroupSearchByName.Click += new System.EventHandler(this.btnBuildingGroupSearchByName_Click);
            // 
            // buildingMultiStoreyPanel
            // 
            this.buildingMultiStoreyPanel.Items.Add(this.btnBuildingMultiStoreySearch);
            this.buildingMultiStoreyPanel.Items.Add(this.btnBuildingMultiStoreySearchById);
            this.buildingMultiStoreyPanel.Text = "MultiStorey";
            // 
            // btnBuildingMultiStoreySearch
            // 
            this.btnBuildingMultiStoreySearch.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingMultiStoreySearch.Image")));
            this.btnBuildingMultiStoreySearch.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingMultiStoreySearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingMultiStoreySearch.SmallImage")));
            this.btnBuildingMultiStoreySearch.Text = "Search";
            this.btnBuildingMultiStoreySearch.Click += new System.EventHandler(this.btnBuildingMultiStoreySearch_Click);
            // 
            // btnBuildingMultiStoreySearchById
            // 
            this.btnBuildingMultiStoreySearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildingMultiStoreySearchById.Image")));
            this.btnBuildingMultiStoreySearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildingMultiStoreySearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildingMultiStoreySearchById.SmallImage")));
            this.btnBuildingMultiStoreySearchById.Text = "Search By Id";
            this.btnBuildingMultiStoreySearchById.Click += new System.EventHandler(this.btnBuildingMultiStoreySearchById_Click);
            // 
            // landmarkTab
            // 
            this.landmarkTab.Panels.Add(this.landmarkGeneralPanel);
            this.landmarkTab.Panels.Add(this.landmarkBoundaryPanel);
            this.landmarkTab.Panels.Add(this.landmarkBoundaryVertexPanel);
            this.landmarkTab.Text = "Landmark";
            // 
            // landmarkGeneralPanel
            // 
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralSearch);
            this.landmarkGeneralPanel.Items.Add(this.ribbonSeparator6);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralAdd);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralEdit);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralDelete);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralReassociate);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralMove);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralSearchById);
            this.landmarkGeneralPanel.Items.Add(this.btnLandmarkGeneralSearchByName);
            this.landmarkGeneralPanel.Text = "General";
            // 
            // btnLandmarkGeneralSearch
            // 
            this.btnLandmarkGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearch.Image")));
            this.btnLandmarkGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearch.SmallImage")));
            this.btnLandmarkGeneralSearch.Text = "Search";
            this.btnLandmarkGeneralSearch.Click += new System.EventHandler(this.btnLandmarkGeneralSearch_Click);
            // 
            // btnLandmarkGeneralAdd
            // 
            this.btnLandmarkGeneralAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralAdd.Image")));
            this.btnLandmarkGeneralAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralAdd.SmallImage")));
            this.btnLandmarkGeneralAdd.Text = "Add";
            this.btnLandmarkGeneralAdd.Click += new System.EventHandler(this.btnLandmarkGeneralAdd_Click);
            // 
            // btnLandmarkGeneralEdit
            // 
            this.btnLandmarkGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralEdit.Image")));
            this.btnLandmarkGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralEdit.SmallImage")));
            this.btnLandmarkGeneralEdit.Text = "Edit";
            this.btnLandmarkGeneralEdit.Click += new System.EventHandler(this.btnLandmarkGeneralEdit_Click);
            // 
            // btnLandmarkGeneralDelete
            // 
            this.btnLandmarkGeneralDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralDelete.Image")));
            this.btnLandmarkGeneralDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralDelete.SmallImage")));
            this.btnLandmarkGeneralDelete.Text = "Delete";
            this.btnLandmarkGeneralDelete.Click += new System.EventHandler(this.btnLandmarkGeneralDelete_Click);
            // 
            // btnLandmarkGeneralReassociate
            // 
            this.btnLandmarkGeneralReassociate.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralReassociate.Image")));
            this.btnLandmarkGeneralReassociate.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralReassociate.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralReassociate.SmallImage")));
            this.btnLandmarkGeneralReassociate.Text = "Reassociate";
            this.btnLandmarkGeneralReassociate.Click += new System.EventHandler(this.btnLandmarkGeneralReassociate_Click);
            // 
            // btnLandmarkGeneralMove
            // 
            this.btnLandmarkGeneralMove.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralMove.Image")));
            this.btnLandmarkGeneralMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralMove.SmallImage")));
            this.btnLandmarkGeneralMove.Text = "Move";
            this.btnLandmarkGeneralMove.ToolTip = "Move Landmark";
            this.btnLandmarkGeneralMove.Click += new System.EventHandler(this.btnLandmarkGeneralMove_Click);
            // 
            // btnLandmarkGeneralSearchById
            // 
            this.btnLandmarkGeneralSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearchById.Image")));
            this.btnLandmarkGeneralSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearchById.SmallImage")));
            this.btnLandmarkGeneralSearchById.Text = "Search By Id";
            this.btnLandmarkGeneralSearchById.ToolTip = "Search By Id";
            this.btnLandmarkGeneralSearchById.Click += new System.EventHandler(this.btnLandmarkGeneralSearchById_Click);
            // 
            // btnLandmarkGeneralSearchByName
            // 
            this.btnLandmarkGeneralSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearchByName.Image")));
            this.btnLandmarkGeneralSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkGeneralSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkGeneralSearchByName.SmallImage")));
            this.btnLandmarkGeneralSearchByName.Text = "Search By Name";
            this.btnLandmarkGeneralSearchByName.ToolTip = "Search By Name";
            this.btnLandmarkGeneralSearchByName.Click += new System.EventHandler(this.btnLandmarkGeneralSearchByName_Click);
            // 
            // landmarkBoundaryPanel
            // 
            this.landmarkBoundaryPanel.Items.Add(this.btnLandmarkBoundarySearch);
            this.landmarkBoundaryPanel.Items.Add(this.ribbonSeparator7);
            this.landmarkBoundaryPanel.Items.Add(this.btnLandmarkBoundaryAdd);
            this.landmarkBoundaryPanel.Items.Add(this.btnLandmarkBoundaryDelete);
            this.landmarkBoundaryPanel.Items.Add(this.btnLandmarkBoundaryMove);
            this.landmarkBoundaryPanel.Text = "Landmark Boundary";
            // 
            // btnLandmarkBoundarySearch
            // 
            this.btnLandmarkBoundarySearch.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundarySearch.Image")));
            this.btnLandmarkBoundarySearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundarySearch.SmallImage")));
            this.btnLandmarkBoundarySearch.Text = "Search";
            this.btnLandmarkBoundarySearch.Click += new System.EventHandler(this.btnLandmarkBoundarySearch_Click);
            // 
            // btnLandmarkBoundaryAdd
            // 
            this.btnLandmarkBoundaryAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryAdd.Image")));
            this.btnLandmarkBoundaryAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryAdd.SmallImage")));
            this.btnLandmarkBoundaryAdd.Text = "Add";
            this.btnLandmarkBoundaryAdd.ToolTip = "Add";
            this.btnLandmarkBoundaryAdd.Click += new System.EventHandler(this.btnLandmarkBoundaryAdd_Click);
            // 
            // btnLandmarkBoundaryDelete
            // 
            this.btnLandmarkBoundaryDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryDelete.Image")));
            this.btnLandmarkBoundaryDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryDelete.SmallImage")));
            this.btnLandmarkBoundaryDelete.Text = "Delete";
            this.btnLandmarkBoundaryDelete.ToolTip = "Delete";
            // 
            // btnLandmarkBoundaryMove
            // 
            this.btnLandmarkBoundaryMove.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryMove.Image")));
            this.btnLandmarkBoundaryMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryMove.SmallImage")));
            this.btnLandmarkBoundaryMove.Text = "Move";
            this.btnLandmarkBoundaryMove.ToolTip = "Move";
            this.btnLandmarkBoundaryMove.Click += new System.EventHandler(this.btnLandmarkBoundaryMove_Click);
            // 
            // landmarkBoundaryVertexPanel
            // 
            this.landmarkBoundaryVertexPanel.Items.Add(this.btnLandmarkBoundaryVertexAdd);
            this.landmarkBoundaryVertexPanel.Items.Add(this.btnLandmarkBoundaryVertexMove);
            this.landmarkBoundaryVertexPanel.Items.Add(this.btnLandmarkBoundaryVertexDelete);
            this.landmarkBoundaryVertexPanel.Text = "Vertex";
            // 
            // btnLandmarkBoundaryVertexAdd
            // 
            this.btnLandmarkBoundaryVertexAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexAdd.Image")));
            this.btnLandmarkBoundaryVertexAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryVertexAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexAdd.SmallImage")));
            this.btnLandmarkBoundaryVertexAdd.Text = "Add";
            this.btnLandmarkBoundaryVertexAdd.ToolTip = "Add";
            this.btnLandmarkBoundaryVertexAdd.Click += new System.EventHandler(this.btnLandmarkBoundaryVertexAdd_Click);
            // 
            // btnLandmarkBoundaryVertexMove
            // 
            this.btnLandmarkBoundaryVertexMove.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexMove.Image")));
            this.btnLandmarkBoundaryVertexMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryVertexMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexMove.SmallImage")));
            this.btnLandmarkBoundaryVertexMove.Text = "Move";
            this.btnLandmarkBoundaryVertexMove.ToolTip = "Move";
            this.btnLandmarkBoundaryVertexMove.Click += new System.EventHandler(this.btnLandmarkBoundaryVertexMove_Click);
            // 
            // btnLandmarkBoundaryVertexDelete
            // 
            this.btnLandmarkBoundaryVertexDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexDelete.Image")));
            this.btnLandmarkBoundaryVertexDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLandmarkBoundaryVertexDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLandmarkBoundaryVertexDelete.SmallImage")));
            this.btnLandmarkBoundaryVertexDelete.Text = "Delete";
            this.btnLandmarkBoundaryVertexDelete.ToolTip = "Delete";
            this.btnLandmarkBoundaryVertexDelete.Click += new System.EventHandler(this.btnLandmarkBoundaryVertexDelete_Click);
            // 
            // poiTab
            // 
            this.poiTab.Panels.Add(this.poiGeneralPanel);
            this.poiTab.Text = "POI";
            // 
            // poiGeneralPanel
            // 
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralSearch);
            this.poiGeneralPanel.Items.Add(this.ribbonSeparator8);
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralEdit);
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralMerge);
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralShowStreet);
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralSearchById);
            this.poiGeneralPanel.Items.Add(this.btnPoiGeneralSearchByName);
            this.poiGeneralPanel.Text = "General";
            // 
            // btnPoiGeneralSearch
            // 
            this.btnPoiGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearch.Image")));
            this.btnPoiGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearch.SmallImage")));
            this.btnPoiGeneralSearch.Text = "Search";
            this.btnPoiGeneralSearch.Click += new System.EventHandler(this.btnPoiGeneralSearch_Click);
            // 
            // btnPoiGeneralEdit
            // 
            this.btnPoiGeneralEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralEdit.Image")));
            this.btnPoiGeneralEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPoiGeneralEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralEdit.SmallImage")));
            this.btnPoiGeneralEdit.Text = "Edit";
            this.btnPoiGeneralEdit.Click += new System.EventHandler(this.btnPoiGeneralEdit_Click);
            // 
            // btnPoiGeneralMerge
            // 
            this.btnPoiGeneralMerge.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralMerge.Image")));
            this.btnPoiGeneralMerge.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPoiGeneralMerge.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralMerge.SmallImage")));
            this.btnPoiGeneralMerge.Text = "Merge";
            this.btnPoiGeneralMerge.ToolTip = "Merge Poi";
            this.btnPoiGeneralMerge.Click += new System.EventHandler(this.btnPoiGeneralMerge_Click);
            // 
            // btnPoiGeneralShowStreet
            // 
            this.btnPoiGeneralShowStreet.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralShowStreet.Image")));
            this.btnPoiGeneralShowStreet.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPoiGeneralShowStreet.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralShowStreet.SmallImage")));
            this.btnPoiGeneralShowStreet.Text = "Show Street";
            this.btnPoiGeneralShowStreet.ToolTip = "Show Street";
            this.btnPoiGeneralShowStreet.Click += new System.EventHandler(this.btnPoiGeneralShowStreet_Click);
            // 
            // btnPoiGeneralSearchById
            // 
            this.btnPoiGeneralSearchById.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearchById.Image")));
            this.btnPoiGeneralSearchById.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPoiGeneralSearchById.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearchById.SmallImage")));
            this.btnPoiGeneralSearchById.Text = "Search by id";
            this.btnPoiGeneralSearchById.ToolTip = "Search by id";
            this.btnPoiGeneralSearchById.Click += new System.EventHandler(this.btnPoiGeneralSearchById_Click);
            // 
            // btnPoiGeneralSearchByName
            // 
            this.btnPoiGeneralSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearchByName.Image")));
            this.btnPoiGeneralSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPoiGeneralSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPoiGeneralSearchByName.SmallImage")));
            this.btnPoiGeneralSearchByName.Text = "Search by Name";
            this.btnPoiGeneralSearchByName.ToolTip = "Search by Name";
            this.btnPoiGeneralSearchByName.Click += new System.EventHandler(this.btnPoiGeneralSearchByName_Click);
            // 
            // phoneTab
            // 
            this.phoneTab.Panels.Add(this.phoneGeneralPanel);
            this.phoneTab.Text = "Phone";
            // 
            // phoneGeneralPanel
            // 
            this.phoneGeneralPanel.Items.Add(this.btnPhoneGeneralSearch);
            this.phoneGeneralPanel.Items.Add(this.btnPhoneSearchByNo);
            this.phoneGeneralPanel.Text = "General";
            // 
            // btnPhoneGeneralSearch
            // 
            this.btnPhoneGeneralSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnPhoneGeneralSearch.Image")));
            this.btnPhoneGeneralSearch.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPhoneGeneralSearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPhoneGeneralSearch.SmallImage")));
            this.btnPhoneGeneralSearch.Text = "Search";
            this.btnPhoneGeneralSearch.ToolTip = "Search";
            this.btnPhoneGeneralSearch.Click += new System.EventHandler(this.btnPhoneGeneralSearch_Click);
            // 
            // btnPhoneSearchByNo
            // 
            this.btnPhoneSearchByNo.Image = ((System.Drawing.Image)(resources.GetObject("btnPhoneSearchByNo.Image")));
            this.btnPhoneSearchByNo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPhoneSearchByNo.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPhoneSearchByNo.SmallImage")));
            this.btnPhoneSearchByNo.Text = "Search By No";
            this.btnPhoneSearchByNo.ToolTip = "Search By No";
            this.btnPhoneSearchByNo.Click += new System.EventHandler(this.btnPhoneSearchByNo_Click);
            // 
            // locationTab
            // 
            this.locationTab.Panels.Add(this.sectionBoundaryPanel);
            this.locationTab.Panels.Add(this.locationSectionBoundaryVertexPanel);
            this.locationTab.Panels.Add(this.locationPanel);
            this.locationTab.Text = "Location";
            // 
            // sectionBoundaryPanel
            // 
            this.sectionBoundaryPanel.Items.Add(this.btnLocationSectionBoundarySearch);
            this.sectionBoundaryPanel.Items.Add(this.ribbonSeparator10);
            this.sectionBoundaryPanel.Items.Add(this.btnLocationSectionBoundaryAdd);
            this.sectionBoundaryPanel.Items.Add(this.btnLocationSectionBoundaryEdit);
            this.sectionBoundaryPanel.Items.Add(this.btnLocationSectionBoundaryDelete);
            this.sectionBoundaryPanel.Items.Add(this.btnLocationSectionBoundaryMove);
            this.sectionBoundaryPanel.Text = "Section Boundary";
            // 
            // btnLocationSectionBoundarySearch
            // 
            this.btnLocationSectionBoundarySearch.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundarySearch.Image")));
            this.btnLocationSectionBoundarySearch.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundarySearch.SmallImage")));
            this.btnLocationSectionBoundarySearch.Text = "Search";
            this.btnLocationSectionBoundarySearch.Click += new System.EventHandler(this.btnLocationSectionBoundarySearch_Click);
            // 
            // btnLocationSectionBoundaryAdd
            // 
            this.btnLocationSectionBoundaryAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryAdd.Image")));
            this.btnLocationSectionBoundaryAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryAdd.SmallImage")));
            this.btnLocationSectionBoundaryAdd.Text = "Add";
            this.btnLocationSectionBoundaryAdd.ToolTip = "Add";
            this.btnLocationSectionBoundaryAdd.Click += new System.EventHandler(this.btnLocationSectionBoundaryAdd_Click);
            // 
            // btnLocationSectionBoundaryEdit
            // 
            this.btnLocationSectionBoundaryEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryEdit.Image")));
            this.btnLocationSectionBoundaryEdit.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryEdit.SmallImage")));
            this.btnLocationSectionBoundaryEdit.Text = "Edit";
            this.btnLocationSectionBoundaryEdit.ToolTip = "Edit";
            this.btnLocationSectionBoundaryEdit.Click += new System.EventHandler(this.btnLocationSectionBoundaryEdit_Click);
            // 
            // btnLocationSectionBoundaryDelete
            // 
            this.btnLocationSectionBoundaryDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryDelete.Image")));
            this.btnLocationSectionBoundaryDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryDelete.SmallImage")));
            this.btnLocationSectionBoundaryDelete.Text = "Delete";
            this.btnLocationSectionBoundaryDelete.ToolTip = "Delete";
            this.btnLocationSectionBoundaryDelete.Click += new System.EventHandler(this.btnLocationSectionBoundaryDelete_Click);
            // 
            // btnLocationSectionBoundaryMove
            // 
            this.btnLocationSectionBoundaryMove.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryMove.Image")));
            this.btnLocationSectionBoundaryMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryMove.SmallImage")));
            this.btnLocationSectionBoundaryMove.Text = "Move";
            this.btnLocationSectionBoundaryMove.ToolTip = "Move";
            this.btnLocationSectionBoundaryMove.Click += new System.EventHandler(this.btnLocationSectionBoundaryMove_Click);
            // 
            // locationSectionBoundaryVertexPanel
            // 
            this.locationSectionBoundaryVertexPanel.Items.Add(this.btnLocationSectionBoundaryVertexAdd);
            this.locationSectionBoundaryVertexPanel.Items.Add(this.btnLocationSectionBoundaryVertexMove);
            this.locationSectionBoundaryVertexPanel.Items.Add(this.btnLocationSectionBoundaryVertexDelete);
            this.locationSectionBoundaryVertexPanel.Text = "Vertex";
            // 
            // btnLocationSectionBoundaryVertexAdd
            // 
            this.btnLocationSectionBoundaryVertexAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexAdd.Image")));
            this.btnLocationSectionBoundaryVertexAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryVertexAdd.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexAdd.SmallImage")));
            this.btnLocationSectionBoundaryVertexAdd.Text = "Add";
            this.btnLocationSectionBoundaryVertexAdd.ToolTip = "Add";
            this.btnLocationSectionBoundaryVertexAdd.Click += new System.EventHandler(this.btnLocationSectionBoundaryVertexAdd_Click);
            // 
            // btnLocationSectionBoundaryVertexMove
            // 
            this.btnLocationSectionBoundaryVertexMove.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexMove.Image")));
            this.btnLocationSectionBoundaryVertexMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryVertexMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexMove.SmallImage")));
            this.btnLocationSectionBoundaryVertexMove.Text = "Move";
            this.btnLocationSectionBoundaryVertexMove.ToolTip = "Move";
            this.btnLocationSectionBoundaryVertexMove.Click += new System.EventHandler(this.btnLocationSectionBoundaryVertexMove_Click);
            // 
            // btnLocationSectionBoundaryVertexDelete
            // 
            this.btnLocationSectionBoundaryVertexDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexDelete.Image")));
            this.btnLocationSectionBoundaryVertexDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnLocationSectionBoundaryVertexDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationSectionBoundaryVertexDelete.SmallImage")));
            this.btnLocationSectionBoundaryVertexDelete.Text = "Delete";
            this.btnLocationSectionBoundaryVertexDelete.ToolTip = "Delete";
            this.btnLocationSectionBoundaryVertexDelete.Click += new System.EventHandler(this.btnLocationSectionBoundaryVertexDelete_Click);
            // 
            // locationPanel
            // 
            this.locationPanel.Items.Add(this.btnLocationManage);
            this.locationPanel.Text = "Location";
            // 
            // btnLocationManage
            // 
            this.btnLocationManage.Image = ((System.Drawing.Image)(resources.GetObject("btnLocationManage.Image")));
            this.btnLocationManage.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnLocationManage.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnLocationManage.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnLocationManage.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnLocationManage.SmallImage")));
            this.btnLocationManage.Text = "Manage";
            this.btnLocationManage.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnLocationManage.ToolTip = "Manage Location";
            this.btnLocationManage.Click += new System.EventHandler(this.btnLocationManage_Click);
            // 
            // userTab
            // 
            this.userTab.Panels.Add(this.userAccessibilityPanel);
            this.userTab.Panels.Add(this.Cadu3Panel);
            this.userTab.Text = "User";
            // 
            // userAccessibilityPanel
            // 
            this.userAccessibilityPanel.Items.Add(this.btnUserAccessibilityManage);
            this.userAccessibilityPanel.Items.Add(this.btnUserAccessibilityChangePassword);
            this.userAccessibilityPanel.Text = "Accessibility";
            // 
            // btnUserAccessibilityManage
            // 
            this.btnUserAccessibilityManage.Image = ((System.Drawing.Image)(resources.GetObject("btnUserAccessibilityManage.Image")));
            this.btnUserAccessibilityManage.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnUserAccessibilityManage.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnUserAccessibilityManage.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUserAccessibilityManage.SmallImage")));
            this.btnUserAccessibilityManage.Text = "Manage";
            this.btnUserAccessibilityManage.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnUserAccessibilityManage.ToolTip = "Manage";
            this.btnUserAccessibilityManage.Click += new System.EventHandler(this.btnUserAccessibilityManage_Click);
            // 
            // btnUserAccessibilityChangePassword
            // 
            this.btnUserAccessibilityChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("btnUserAccessibilityChangePassword.Image")));
            this.btnUserAccessibilityChangePassword.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnUserAccessibilityChangePassword.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnUserAccessibilityChangePassword.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUserAccessibilityChangePassword.SmallImage")));
            this.btnUserAccessibilityChangePassword.Text = "Change Password";
            this.btnUserAccessibilityChangePassword.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnUserAccessibilityChangePassword.ToolTip = "Change Password";
            this.btnUserAccessibilityChangePassword.Click += new System.EventHandler(this.btnUserAccessibilityChangePassword_Click);
            // 
            // Cadu3Panel
            // 
            this.Cadu3Panel.Items.Add(this.btnVerifyWeb);
            this.Cadu3Panel.Text = "Cadu3";
            // 
            // btnVerifyWeb
            // 
            this.btnVerifyWeb.Image = global::Geomatic.Map.Properties.Resources.apply;
            this.btnVerifyWeb.SmallImage = global::Geomatic.Map.Properties.Resources.apply;
            this.btnVerifyWeb.Text = "Verify WEB";
            this.btnVerifyWeb.Click += new System.EventHandler(this.btnVerifyWEB_Click);
            // 
            // indexTab
            // 
            this.indexTab.Panels.Add(this.indexPanel);
            this.indexTab.Panels.Add(this.jupemPanel);
            this.indexTab.Text = "Region/Jupem";
            // 
            // indexPanel
            // 
            this.indexPanel.Items.Add(this.btnShowIndex);
            this.indexPanel.Items.Add(this.btnSearchIndex);
            this.indexPanel.Text = "Region";
            // 
            // btnShowIndex
            // 
            this.btnShowIndex.Image = ((System.Drawing.Image)(resources.GetObject("btnShowIndex.Image")));
            this.btnShowIndex.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnShowIndex.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnShowIndex.SmallImage")));
            this.btnShowIndex.Text = "Show Index";
            this.btnShowIndex.ToolTip = "Show Region Index";
            this.btnShowIndex.Click += new System.EventHandler(this.btnShowIndex_Click);
            // 
            // btnSearchIndex
            // 
            this.btnSearchIndex.Image = global::Geomatic.Map.Properties.Resources.search;
            this.btnSearchIndex.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnSearchIndex.SmallImage = global::Geomatic.Map.Properties.Resources.search;
            this.btnSearchIndex.Text = "Search Index";
            this.btnSearchIndex.ToolTip = "Search Region Index";
            this.btnSearchIndex.Click += new System.EventHandler(this.btnSearchIndex_Click);
            // 
            // jupemPanel
            // 
            this.jupemPanel.FlowsTo = System.Windows.Forms.RibbonPanelFlowDirection.Right;
            this.jupemPanel.Items.Add(this.btnSearchJupemLot);
            this.jupemPanel.Text = "Jupem";
            // 
            // btnSearchJupemLot
            // 
            this.btnSearchJupemLot.Image = global::Geomatic.Map.Properties.Resources.search;
            this.btnSearchJupemLot.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnSearchJupemLot.SmallImage = global::Geomatic.Map.Properties.Resources.search;
            this.btnSearchJupemLot.Tag = "Search Jupem Lot";
            this.btnSearchJupemLot.Text = "Search Lot";
            this.btnSearchJupemLot.Click += new System.EventHandler(this.btnSearchJupemLot_Click);
            // 
            // WorkAreaANDTab
            // 
            this.WorkAreaANDTab.Panels.Add(this.ANDWorkAreaPanel);
            this.WorkAreaANDTab.Panels.Add(this.workAreaVertexPanel);
            this.WorkAreaANDTab.Text = "Work Area";
            // 
            // ANDWorkAreaPanel
            // 
            this.ANDWorkAreaPanel.Items.Add(this.btnCreateWA);
            this.ANDWorkAreaPanel.Items.Add(this.btnDeleteWA);
            this.ANDWorkAreaPanel.Items.Add(this.btnCloseWA);
            this.ANDWorkAreaPanel.Items.Add(this.btnSearchWorkArea);
            this.ANDWorkAreaPanel.Items.Add(this.btnVerifyWorkArea2);
            this.ANDWorkAreaPanel.Items.Add(this.btnCompleteWA);
            this.ANDWorkAreaPanel.Items.Add(this.btnVerifyWorkArea);
            this.ANDWorkAreaPanel.Text = "Work Area";
            // 
            // btnCreateWA
            // 
            this.btnCreateWA.Image = global::Geomatic.Map.Properties.Resources.add;
            this.btnCreateWA.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnCreateWA.SmallImage = global::Geomatic.Map.Properties.Resources.add;
            this.btnCreateWA.Text = "Create";
            this.btnCreateWA.ToolTip = "Create Work Area";
            this.btnCreateWA.Click += new System.EventHandler(this.btnCreateWorkArea_Click);
            // 
            // btnDeleteWA
            // 
            this.btnDeleteWA.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteWA.Image")));
            this.btnDeleteWA.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnDeleteWA.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteWA.SmallImage")));
            this.btnDeleteWA.Text = "Delete";
            this.btnDeleteWA.ToolTipTitle = "Delete Work Area";
            this.btnDeleteWA.Click += new System.EventHandler(this.btnDeleteWorkArea_Click);
            // 
            // btnCloseWA
            // 
            this.btnCloseWA.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseWA.Image")));
            this.btnCloseWA.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnCloseWA.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnCloseWA.SmallImage")));
            this.btnCloseWA.Tag = "Close";
            this.btnCloseWA.Text = "Close";
            this.btnCloseWA.ToolTipTitle = "Close Work Area";
            this.btnCloseWA.Click += new System.EventHandler(this.btnCloseWorkArea_Click);
            // 
            // btnSearchWorkArea
            // 
            this.btnSearchWorkArea.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchWorkArea.Image")));
            this.btnSearchWorkArea.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnSearchWorkArea.SmallImage = global::Geomatic.Map.Properties.Resources.search;
            this.btnSearchWorkArea.Text = "Search";
            this.btnSearchWorkArea.ToolTipTitle = "Search Work Area";
            this.btnSearchWorkArea.Click += new System.EventHandler(this.btnSearchWorkArea_Click);
            // 
            // btnVerifyWorkArea2
            // 
            this.btnVerifyWorkArea2.Image = ((System.Drawing.Image)(resources.GetObject("btnVerifyWorkArea2.Image")));
            this.btnVerifyWorkArea2.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnVerifyWorkArea2.SmallImage = global::Geomatic.Map.Properties.Resources.apply;
            this.btnVerifyWorkArea2.Tag = "New Verify";
            this.btnVerifyWorkArea2.Text = "Verify";
            this.btnVerifyWorkArea2.Click += new System.EventHandler(this.btnVerifyWorkArea2_Click);
            // 
            // btnCompleteWA
            // 
            this.btnCompleteWA.Image = ((System.Drawing.Image)(resources.GetObject("btnCompleteWA.Image")));
            this.btnCompleteWA.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnCompleteWA.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnCompleteWA.SmallImage")));
            this.btnCompleteWA.Text = "Completed";
            this.btnCompleteWA.ToolTip = "Completed Work Area";
            this.btnCompleteWA.Click += new System.EventHandler(this.btnCompletedWorkArea_Click);
            // 
            // btnVerifyWorkArea
            // 
            this.btnVerifyWorkArea.Image = ((System.Drawing.Image)(resources.GetObject("btnVerifyWorkArea.Image")));
            this.btnVerifyWorkArea.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnVerifyWorkArea.SmallImage = global::Geomatic.Map.Properties.Resources.apply;
            this.btnVerifyWorkArea.Text = "VerifyOld";
            this.btnVerifyWorkArea.ToolTip = "Verify Work Area";
            this.btnVerifyWorkArea.Visible = false;
            this.btnVerifyWorkArea.Click += new System.EventHandler(this.btnVerifyWorkArea_Click);
            // 
            // workAreaVertexPanel
            // 
            this.workAreaVertexPanel.Items.Add(this.btnWorkAreaVertexAdd);
            this.workAreaVertexPanel.Items.Add(this.btnWorkAreaVertexMove);
            this.workAreaVertexPanel.Items.Add(this.btnWorkAreaVertexDelete);
            this.workAreaVertexPanel.Text = "Reshape Vertex";
            // 
            // btnWorkAreaVertexAdd
            // 
            this.btnWorkAreaVertexAdd.Image = global::Geomatic.Map.Properties.Resources.add;
            this.btnWorkAreaVertexAdd.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnWorkAreaVertexAdd.SmallImage = global::Geomatic.Map.Properties.Resources.add;
            this.btnWorkAreaVertexAdd.Text = "Add";
            this.btnWorkAreaVertexAdd.ToolTip = "Add";
            this.btnWorkAreaVertexAdd.Click += new System.EventHandler(this.btnWorkAreaVertexAdd_Click);
            // 
            // btnWorkAreaVertexMove
            // 
            this.btnWorkAreaVertexMove.Image = ((System.Drawing.Image)(resources.GetObject("btnWorkAreaVertexMove.Image")));
            this.btnWorkAreaVertexMove.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnWorkAreaVertexMove.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnWorkAreaVertexMove.SmallImage")));
            this.btnWorkAreaVertexMove.Text = "Move";
            this.btnWorkAreaVertexMove.ToolTip = "Move";
            this.btnWorkAreaVertexMove.Click += new System.EventHandler(this.btnWorkAreaVertexMove_Click);
            // 
            // btnWorkAreaVertexDelete
            // 
            this.btnWorkAreaVertexDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnWorkAreaVertexDelete.Image")));
            this.btnWorkAreaVertexDelete.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnWorkAreaVertexDelete.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnWorkAreaVertexDelete.SmallImage")));
            this.btnWorkAreaVertexDelete.Text = "Delete";
            this.btnWorkAreaVertexDelete.ToolTip = "Delete";
            this.btnWorkAreaVertexDelete.Click += new System.EventHandler(this.btnWorkAreaVertexDelete_Click);
            // 
            // rasterTab
            // 
            this.rasterTab.Panels.Add(this.rasterPanel);
            this.rasterTab.Panels.Add(this.warpingPanel);
            this.rasterTab.Panels.Add(this.editRasterPanel);
            this.rasterTab.Text = "Raster";
            // 
            // rasterPanel
            // 
            this.rasterPanel.Items.Add(this.btnOpenRasterWarping);
            this.rasterPanel.Items.Add(this.btnCloseRasterWarping);
            this.rasterPanel.Text = "Raster";
            // 
            // btnOpenRasterWarping
            // 
            this.btnOpenRasterWarping.Image = global::Geomatic.Map.Properties.Resources.add;
            this.btnOpenRasterWarping.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnOpenRasterWarping.SmallImage = global::Geomatic.Map.Properties.Resources.add;
            this.btnOpenRasterWarping.Text = "Open";
            this.btnOpenRasterWarping.Click += new System.EventHandler(this.OnOpenRasterWarping);
            // 
            // btnCloseRasterWarping
            // 
            this.btnCloseRasterWarping.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseRasterWarping.Image")));
            this.btnCloseRasterWarping.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnCloseRasterWarping.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnCloseRasterWarping.SmallImage")));
            this.btnCloseRasterWarping.Text = "Close";
            this.btnCloseRasterWarping.Click += new System.EventHandler(this.OnCloseRasterWarping);
            // 
            // warpingPanel
            // 
            this.warpingPanel.Items.Add(this.btnAddPoint);
            this.warpingPanel.Items.Add(this.btnDeletePoint);
            this.warpingPanel.Items.Add(this.btnDeleteAllPoints);
            this.warpingPanel.Items.Add(this.btnUpdateAlignment);
            this.warpingPanel.Items.Add(this.btnFitToDisplay);
            this.warpingPanel.Items.Add(this.btnRegisterRaster);
            this.warpingPanel.Items.Add(this.btnBuildPyramid);
            this.warpingPanel.Items.Add(this.btnMoveRaster);
            this.warpingPanel.Items.Add(this.btnScaleUp);
            this.warpingPanel.Items.Add(this.btnScaleDown);
            this.warpingPanel.Items.Add(this.btnRotateClockwise);
            this.warpingPanel.Items.Add(this.btnRotateAntiClockwise);
            this.warpingPanel.Text = "Warping";
            // 
            // btnAddPoint
            // 
            this.btnAddPoint.Image = ((System.Drawing.Image)(resources.GetObject("btnAddPoint.Image")));
            this.btnAddPoint.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnAddPoint.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnAddPoint.SmallImage")));
            this.btnAddPoint.Text = "Add Point";
            this.btnAddPoint.Click += new System.EventHandler(this.btnAddPoint_Click);
            // 
            // btnDeletePoint
            // 
            this.btnDeletePoint.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePoint.Image")));
            this.btnDeletePoint.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnDeletePoint.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDeletePoint.SmallImage")));
            this.btnDeletePoint.Text = "Delete Point";
            this.btnDeletePoint.Click += new System.EventHandler(this.btnDeletePoint_Click);
            // 
            // btnDeleteAllPoints
            // 
            this.btnDeleteAllPoints.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAllPoints.Image")));
            this.btnDeleteAllPoints.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnDeleteAllPoints.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteAllPoints.SmallImage")));
            this.btnDeleteAllPoints.Text = "Delete All Points";
            this.btnDeleteAllPoints.Click += new System.EventHandler(this.btnDeleteAllPoints_Click);
            // 
            // btnUpdateAlignment
            // 
            this.btnUpdateAlignment.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateAlignment.Image")));
            this.btnUpdateAlignment.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnUpdateAlignment.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnUpdateAlignment.SmallImage")));
            this.btnUpdateAlignment.Text = "Update Alignment";
            this.btnUpdateAlignment.Click += new System.EventHandler(this.btnUpdateAlignment_Click);
            // 
            // btnFitToDisplay
            // 
            this.btnFitToDisplay.Image = ((System.Drawing.Image)(resources.GetObject("btnFitToDisplay.Image")));
            this.btnFitToDisplay.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnFitToDisplay.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnFitToDisplay.SmallImage")));
            this.btnFitToDisplay.Text = "Fit To Display";
            this.btnFitToDisplay.Click += new System.EventHandler(this.btnFitToDisplay_Click);
            // 
            // btnRegisterRaster
            // 
            this.btnRegisterRaster.Image = ((System.Drawing.Image)(resources.GetObject("btnRegisterRaster.Image")));
            this.btnRegisterRaster.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnRegisterRaster.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRegisterRaster.SmallImage")));
            this.btnRegisterRaster.Text = "Register Raster";
            this.btnRegisterRaster.Click += new System.EventHandler(this.btnRegisterRaster_Click);
            // 
            // btnBuildPyramid
            // 
            this.btnBuildPyramid.Image = ((System.Drawing.Image)(resources.GetObject("btnBuildPyramid.Image")));
            this.btnBuildPyramid.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnBuildPyramid.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnBuildPyramid.SmallImage")));
            this.btnBuildPyramid.Text = "Build Pyramid";
            this.btnBuildPyramid.Click += new System.EventHandler(this.btnBuildPyramid_Click);
            // 
            // btnMoveRaster
            // 
            this.btnMoveRaster.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveRaster.Image")));
            this.btnMoveRaster.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnMoveRaster.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnMoveRaster.SmallImage")));
            this.btnMoveRaster.Text = "Move Raster";
            this.btnMoveRaster.Click += new System.EventHandler(this.btnMoveRaster_Click);
            // 
            // btnScaleUp
            // 
            this.btnScaleUp.Image = global::Geomatic.Map.Properties.Resources.scale_up;
            this.btnScaleUp.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnScaleUp.SmallImage = global::Geomatic.Map.Properties.Resources.scale_up;
            this.btnScaleUp.Text = "Scale Up";
            this.btnScaleUp.Click += new System.EventHandler(this.btnScaleUp_Click);
            // 
            // btnScaleDown
            // 
            this.btnScaleDown.Image = global::Geomatic.Map.Properties.Resources.scale_down;
            this.btnScaleDown.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnScaleDown.SmallImage = global::Geomatic.Map.Properties.Resources.scale_down;
            this.btnScaleDown.Text = "Scale Down";
            this.btnScaleDown.Click += new System.EventHandler(this.btnScaleDown_Click);
            // 
            // btnRotateClockwise
            // 
            this.btnRotateClockwise.Image = global::Geomatic.Map.Properties.Resources.rotate_clockwise;
            this.btnRotateClockwise.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnRotateClockwise.SmallImage = global::Geomatic.Map.Properties.Resources.rotate_clockwise;
            this.btnRotateClockwise.Text = "Rotate Clockwise";
            this.btnRotateClockwise.Click += new System.EventHandler(this.btnRotateClockwise_Click);
            // 
            // btnRotateAntiClockwise
            // 
            this.btnRotateAntiClockwise.Image = global::Geomatic.Map.Properties.Resources.rotate_anti_clockwise;
            this.btnRotateAntiClockwise.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnRotateAntiClockwise.SmallImage = global::Geomatic.Map.Properties.Resources.rotate_anti_clockwise;
            this.btnRotateAntiClockwise.Text = "Rotate Anti Clockwise";
            this.btnRotateAntiClockwise.Click += new System.EventHandler(this.btnRotateAntiClockwise_Click);
            // 
            // editRasterPanel
            // 
            this.editRasterPanel.Items.Add(this.btnOpenRasterInPaint);
            this.editRasterPanel.Items.Add(this.btnExportRaster);
            this.editRasterPanel.Text = "Raster Tools";
            // 
            // btnOpenRasterInPaint
            // 
            this.btnOpenRasterInPaint.Image = global::Geomatic.Map.Properties.Resources.paint;
            this.btnOpenRasterInPaint.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnOpenRasterInPaint.SmallImage = global::Geomatic.Map.Properties.Resources.paint;
            this.btnOpenRasterInPaint.Text = "Open in Paint";
            this.btnOpenRasterInPaint.Click += new System.EventHandler(this.OnOpenRasterInPaint);
            // 
            // btnExportRaster
            // 
            this.btnExportRaster.Image = global::Geomatic.Map.Properties.Resources.up;
            this.btnExportRaster.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnExportRaster.SmallImage = global::Geomatic.Map.Properties.Resources.up;
            this.btnExportRaster.Text = "Export Raster";
            this.btnExportRaster.Click += new System.EventHandler(this.btnExportRaster_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.Color.White;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.springLabel,
            this.lblWorkspace,
            this.lblWorkspaceText,
            lblSelection,
            this.lblSelectionText,
            lblCoordinate,
            this.lblCoordinateText,
            lblScale,
            this.lblScaleText,
            lblUnit,
            this.lblUnitText});
            this.statusStrip.Location = new System.Drawing.Point(0, 568);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1125, 24);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip";
            // 
            // lblStatus
            // 
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 19);
            this.lblStatus.Text = "Ready";
            // 
            // springLabel
            // 
            this.springLabel.Name = "springLabel";
            this.springLabel.Size = new System.Drawing.Size(673, 19);
            this.springLabel.Spring = true;
            this.springLabel.Click += new System.EventHandler(this.springLabel_Click);
            // 
            // lblWorkspace
            // 
            this.lblWorkspace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblWorkspace.Name = "lblWorkspace";
            this.lblWorkspace.Size = new System.Drawing.Size(65, 19);
            this.lblWorkspace.Text = "Workspace";
            // 
            // lblWorkspaceText
            // 
            this.lblWorkspaceText.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblWorkspaceText.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblWorkspaceText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblWorkspaceText.Name = "lblWorkspaceText";
            this.lblWorkspaceText.Size = new System.Drawing.Size(33, 19);
            this.lblWorkspaceText.Text = "N/A";
            // 
            // lblSelectionText
            // 
            this.lblSelectionText.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblSelectionText.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblSelectionText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblSelectionText.Name = "lblSelectionText";
            this.lblSelectionText.Size = new System.Drawing.Size(17, 19);
            this.lblSelectionText.Text = "0";
            // 
            // lblCoordinateText
            // 
            this.lblCoordinateText.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblCoordinateText.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblCoordinateText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblCoordinateText.Name = "lblCoordinateText";
            this.lblCoordinateText.Size = new System.Drawing.Size(33, 19);
            this.lblCoordinateText.Text = "N/A";
            // 
            // lblScaleText
            // 
            this.lblScaleText.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblScaleText.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblScaleText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblScaleText.Name = "lblScaleText";
            this.lblScaleText.Size = new System.Drawing.Size(33, 19);
            this.lblScaleText.Text = "N/A";
            // 
            // lblUnitText
            // 
            this.lblUnitText.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblUnitText.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblUnitText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.lblUnitText.Name = "lblUnitText";
            this.lblUnitText.Size = new System.Drawing.Size(33, 19);
            this.lblUnitText.Text = "N/A";
            // 
            // dockPanel
            // 
            this.dockPanel.ActiveAutoHideContent = null;
            this.dockPanel.BackColor = System.Drawing.Color.White;
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingSdi;
            this.dockPanel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dockPanel.Location = new System.Drawing.Point(0, 160);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Size = new System.Drawing.Size(1125, 408);
            dockPanelGradient1.EndColor = System.Drawing.Color.White;
            dockPanelGradient1.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            dockPanelGradient1.StartColor = System.Drawing.Color.White;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.Color.White;
            tabGradient1.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient1.StartColor = System.Drawing.Color.White;
            tabGradient1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(234)))), ((int)(((byte)(172)))));
            tabGradient2.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient2.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(70)))));
            tabGradient2.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.Color.White;
            dockPanelGradient2.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            dockPanelGradient2.StartColor = System.Drawing.Color.White;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient3.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient3.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient3.TextColor = System.Drawing.Color.White;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient4.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(234)))), ((int)(((byte)(172)))));
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(70)))));
            tabGradient4.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(234)))), ((int)(((byte)(172)))));
            tabGradient5.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient5.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(70)))));
            tabGradient5.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.Color.White;
            dockPanelGradient3.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            dockPanelGradient3.StartColor = System.Drawing.Color.White;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient6.TextColor = System.Drawing.Color.White;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient7.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient7.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            tabGradient7.TextColor = System.Drawing.Color.White;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel.Skin = dockPanelSkin1;
            this.dockPanel.TabIndex = 1;
            this.dockPanel.ActiveDocumentChanged += new System.EventHandler(this.dockPanel_ActiveDocumentChanged);
            this.dockPanel.ActiveContentChanged += new System.EventHandler(this.dockPanel_ActiveContentChanged);
            // 
            // menuDocument
            // 
            this.menuDocument.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNoTool,
            this.menuSelect,
            this.separator11,
            this.menuPan,
            this.menuZoomIn,
            this.menuZoomOut,
            this.menuZoomToSelection,
            this.separator12,
            this.cbMenuScale,
            this.menuFullExtent,
            this.menuPreviousExtent,
            this.menuNextExtent,
            this.separator13,
            this.menuRefresh,
            this.menuClearSelection,
            this.separator14,
            this.menuRuler});
            this.menuDocument.Name = "menuDocument";
            this.menuDocument.Size = new System.Drawing.Size(182, 319);
            // 
            // menuNoTool
            // 
            this.menuNoTool.Image = ((System.Drawing.Image)(resources.GetObject("menuNoTool.Image")));
            this.menuNoTool.Name = "menuNoTool";
            this.menuNoTool.Size = new System.Drawing.Size(181, 22);
            this.menuNoTool.Text = "No Tool";
            this.menuNoTool.Click += new System.EventHandler(this.OnMapNoTool);
            // 
            // menuSelect
            // 
            this.menuSelect.Image = ((System.Drawing.Image)(resources.GetObject("menuSelect.Image")));
            this.menuSelect.Name = "menuSelect";
            this.menuSelect.Size = new System.Drawing.Size(181, 22);
            this.menuSelect.Text = "Select";
            this.menuSelect.Click += new System.EventHandler(this.OnMapSelect);
            // 
            // separator11
            // 
            this.separator11.Name = "separator11";
            this.separator11.Size = new System.Drawing.Size(178, 6);
            // 
            // menuPan
            // 
            this.menuPan.Image = ((System.Drawing.Image)(resources.GetObject("menuPan.Image")));
            this.menuPan.Name = "menuPan";
            this.menuPan.Size = new System.Drawing.Size(181, 22);
            this.menuPan.Text = "Pan";
            this.menuPan.Click += new System.EventHandler(this.OnMapPan);
            // 
            // menuZoomIn
            // 
            this.menuZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("menuZoomIn.Image")));
            this.menuZoomIn.Name = "menuZoomIn";
            this.menuZoomIn.Size = new System.Drawing.Size(181, 22);
            this.menuZoomIn.Text = "Zoom In";
            this.menuZoomIn.Click += new System.EventHandler(this.OnMapZoomIn);
            // 
            // menuZoomOut
            // 
            this.menuZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("menuZoomOut.Image")));
            this.menuZoomOut.Name = "menuZoomOut";
            this.menuZoomOut.Size = new System.Drawing.Size(181, 22);
            this.menuZoomOut.Text = "Zoom Out";
            this.menuZoomOut.Click += new System.EventHandler(this.OnMapZoomOut);
            // 
            // menuZoomToSelection
            // 
            this.menuZoomToSelection.Image = ((System.Drawing.Image)(resources.GetObject("menuZoomToSelection.Image")));
            this.menuZoomToSelection.Name = "menuZoomToSelection";
            this.menuZoomToSelection.Size = new System.Drawing.Size(181, 22);
            this.menuZoomToSelection.Text = "Zoom To Selection";
            this.menuZoomToSelection.Click += new System.EventHandler(this.OnMapZoomToSelection);
            // 
            // separator12
            // 
            this.separator12.Name = "separator12";
            this.separator12.Size = new System.Drawing.Size(178, 6);
            // 
            // cbMenuScale
            // 
            this.cbMenuScale.Name = "cbMenuScale";
            this.cbMenuScale.Size = new System.Drawing.Size(121, 23);
            this.cbMenuScale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbMenuScale_KeyDown);
            // 
            // menuFullExtent
            // 
            this.menuFullExtent.Image = ((System.Drawing.Image)(resources.GetObject("menuFullExtent.Image")));
            this.menuFullExtent.Name = "menuFullExtent";
            this.menuFullExtent.Size = new System.Drawing.Size(181, 22);
            this.menuFullExtent.Text = "Full Extent";
            this.menuFullExtent.Click += new System.EventHandler(this.OnMapFullExtent);
            // 
            // menuPreviousExtent
            // 
            this.menuPreviousExtent.Image = ((System.Drawing.Image)(resources.GetObject("menuPreviousExtent.Image")));
            this.menuPreviousExtent.Name = "menuPreviousExtent";
            this.menuPreviousExtent.Size = new System.Drawing.Size(181, 22);
            this.menuPreviousExtent.Text = "Previous Extent";
            this.menuPreviousExtent.Click += new System.EventHandler(this.OnMapPreviousExtent);
            // 
            // menuNextExtent
            // 
            this.menuNextExtent.Image = ((System.Drawing.Image)(resources.GetObject("menuNextExtent.Image")));
            this.menuNextExtent.Name = "menuNextExtent";
            this.menuNextExtent.Size = new System.Drawing.Size(181, 22);
            this.menuNextExtent.Text = "Next Extent";
            this.menuNextExtent.Click += new System.EventHandler(this.OnMapNextExtent);
            // 
            // separator13
            // 
            this.separator13.Name = "separator13";
            this.separator13.Size = new System.Drawing.Size(178, 6);
            // 
            // menuRefresh
            // 
            this.menuRefresh.Image = ((System.Drawing.Image)(resources.GetObject("menuRefresh.Image")));
            this.menuRefresh.Name = "menuRefresh";
            this.menuRefresh.Size = new System.Drawing.Size(181, 22);
            this.menuRefresh.Text = "Refresh";
            this.menuRefresh.Click += new System.EventHandler(this.OnMapRefresh);
            // 
            // menuClearSelection
            // 
            this.menuClearSelection.Image = ((System.Drawing.Image)(resources.GetObject("menuClearSelection.Image")));
            this.menuClearSelection.Name = "menuClearSelection";
            this.menuClearSelection.Size = new System.Drawing.Size(181, 22);
            this.menuClearSelection.Text = "Clear Selection";
            this.menuClearSelection.Click += new System.EventHandler(this.OnMapClearSelection);
            // 
            // separator14
            // 
            this.separator14.Name = "separator14";
            this.separator14.Size = new System.Drawing.Size(178, 6);
            // 
            // menuRuler
            // 
            this.menuRuler.Image = ((System.Drawing.Image)(resources.GetObject("menuRuler.Image")));
            this.menuRuler.Name = "menuRuler";
            this.menuRuler.Size = new System.Drawing.Size(181, 22);
            this.menuRuler.Text = "Ruler";
            this.menuRuler.Click += new System.EventHandler(this.OnMapRuler);
            // 
            // openShapeDialog
            // 
            this.openShapeDialog.Filter = "Shape|*.shp";
            this.openShapeDialog.Title = "Open Shape File";
            // 
            // openRasterDialog
            // 
            this.openRasterDialog.Filter = "All files|*.*|TIF|*.tif|JPG|*.jpg|ECW|*.ecw";
            this.openRasterDialog.Title = "Open Raster";
            // 
            // openGPSDialog
            // 
            this.openGPSDialog.Filter = "DGN|*.dgn";
            this.openGPSDialog.Title = "Open GPS File";
            // 
            // openGdbDialog
            // 
            this.openGdbDialog.Description = ".gdb";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Text = "ribbonTab1";
            // 
            // btnOnLayerToggle
            // 
            this.btnOnLayerToggle.Image = ((System.Drawing.Image)(resources.GetObject("btnOnLayerToggle.Image")));
            this.btnOnLayerToggle.MaximumSize = new System.Drawing.Size(64, 64);
            this.btnOnLayerToggle.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btnOnLayerToggle.MinimumSize = new System.Drawing.Size(64, 64);
            this.btnOnLayerToggle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOnLayerToggle.SmallImage")));
            this.btnOnLayerToggle.Text = "Toggle All Layer";
            this.btnOnLayerToggle.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnOnLayerToggle.ToolTip = "Toggle All Layer";
            this.btnOnLayerToggle.Click += new System.EventHandler(this.LayerToggle);
            // 
            // btnJunctionGeneralReassociate
            // 
            this.btnJunctionGeneralReassociate.Image = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralReassociate.Image")));
            this.btnJunctionGeneralReassociate.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnJunctionGeneralReassociate.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnJunctionGeneralReassociate.SmallImage")));
            this.btnJunctionGeneralReassociate.Text = "Reassociate";
            this.btnJunctionGeneralReassociate.ToolTip = "Reassociate";
            this.btnJunctionGeneralReassociate.Click += new System.EventHandler(this.btnJunctionGeneralReassociate_Click);
            // 
            // btnPropertyGeneralSearchByName
            // 
            this.btnPropertyGeneralSearchByName.Image = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearchByName.Image")));
            this.btnPropertyGeneralSearchByName.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btnPropertyGeneralSearchByName.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPropertyGeneralSearchByName.SmallImage")));
            this.btnPropertyGeneralSearchByName.Text = "Search by Name";
            this.btnPropertyGeneralSearchByName.ToolTip = "Search By Name";
            this.btnPropertyGeneralSearchByName.Click += new System.EventHandler(this.btnPropertyGeneralSearchByName_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1125, 592);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuDocument.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon;
        private System.Windows.Forms.RibbonTab homeTab;
        private System.Windows.Forms.RibbonButton btnUndo;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel springLabel;
        private System.Windows.Forms.ToolStripStatusLabel lblCoordinateText;
        private System.Windows.Forms.RibbonOrbMenuItem menuFileSave;
        private System.Windows.Forms.RibbonOrbMenuItem menuFileOpen;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.RibbonButton btnRedo;
        private System.Windows.Forms.RibbonOrbOptionButton btnFileExit;
        private System.Windows.Forms.RibbonOrbOptionButton btnFileOption;
        private System.Windows.Forms.RibbonTab streetTab;
        private System.Windows.Forms.RibbonTab junctionTab;
        private System.Windows.Forms.RibbonTab propertyTab;
        private System.Windows.Forms.RibbonTab buildingTab;
        private System.Windows.Forms.RibbonTab landmarkTab;
        private System.Windows.Forms.RibbonTab locationTab;

        //added by asyrul on 23rd October 2018
        private System.Windows.Forms.RibbonTab indexTab;
        private System.Windows.Forms.RibbonPanel indexPanel;
        private System.Windows.Forms.RibbonButton btnShowIndex;
        private System.Windows.Forms.RibbonButton btnSearchIndex;

        //private System.Windows.Forms.RibbonTab workAreaTab;
        //private System.Windows.Forms.RibbonPanel workAreaPanel;
        //private System.Windows.Forms.RibbonButton btnCreateWorkArea;

        //added by asyrul end

        private System.Windows.Forms.RibbonPanel streetGeneralPanel;
        private System.Windows.Forms.RibbonButton btnStreetGeneralSearch;
        private System.Windows.Forms.RibbonButton btnStreetGeneralAdd;
        private System.Windows.Forms.RibbonButton btnStreetGeneralEdit;
        private System.Windows.Forms.RibbonButton btnStreetGeneralDelete;
        private System.Windows.Forms.RibbonPanel streetVertexPanel;
        private System.Windows.Forms.RibbonButton btnStreetGeneralSplit;
        private System.Windows.Forms.RibbonButton btnStreetGeneralUnion;
        private System.Windows.Forms.RibbonPanel propertyGeneralPanel;
        private System.Windows.Forms.RibbonPanel homeMapPanel;
        private System.Windows.Forms.RibbonPanel homeGeneralPanel;
        private System.Windows.Forms.RibbonPanel homeWindowPanel;
        private System.Windows.Forms.RibbonButton btnHomeWindowLayer;
        private System.Windows.Forms.RibbonButton btnHomeWindowOutput;
        private System.Windows.Forms.RibbonButton btnHomeMapFullExtent;
        private System.Windows.Forms.RibbonButton btnHomeMapPan;
        private System.Windows.Forms.RibbonButton btnHomeMapZoomIn;
        private System.Windows.Forms.RibbonButton btnHomeMapZoomOut;
        private System.Windows.Forms.RibbonButton btnSave;
        private System.Windows.Forms.RibbonButton btnStreetVertexAdd;
        private System.Windows.Forms.RibbonButton btnStreetVertexMove;
        private System.Windows.Forms.RibbonButton btnStreetVertexDelete;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralSearch;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralAdd;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralEdit;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralDelete;
        private System.Windows.Forms.RibbonComboBox cbHomeGeneralSegment;
        private System.Windows.Forms.RibbonPanel junctionGeneralPanel;
        private System.Windows.Forms.RibbonButton btnJunctionGeneralSearch;
        private System.Windows.Forms.RibbonButton btnJunctionGeneralEdit;
        private System.Windows.Forms.RibbonButton btnJunctionGeneralMove;
        private System.Windows.Forms.ToolStripStatusLabel lblScaleText;
        private System.Windows.Forms.ToolStripStatusLabel lblUnitText;
        private System.Windows.Forms.RibbonSeparator separator03;
        private System.Windows.Forms.RibbonButton btnHomeMapRuler;
        //added by asyrul
        private System.Windows.Forms.RibbonButton btnOnLayerToggle;
        //added end

        private System.Windows.Forms.RibbonPanel buildingGeneralPanel;
        private System.Windows.Forms.RibbonPanel landmarkGeneralPanel;
        private System.Windows.Forms.RibbonPanel sectionBoundaryPanel;
        private System.Windows.Forms.RibbonTab poiTab;
        private System.Windows.Forms.RibbonButton btnHomeMapZoomToSelection;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralSearch;

        //added by asyrul
        private System.Windows.Forms.RibbonButton btnBuildingGeneralSearchById;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralSearchByName;

        private System.Windows.Forms.RibbonButton btnBuildingGroupSearchById;
        private System.Windows.Forms.RibbonButton btnBuildingGroupSearchByName;

        private System.Windows.Forms.RibbonButton btnLandmarkGeneralSearchById;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralSearchByName;

        private System.Windows.Forms.RibbonButton btnPoiGeneralSearchById;
        private System.Windows.Forms.RibbonButton btnPoiGeneralSearchByName;
        //added end
        private System.Windows.Forms.RibbonButton btnHomeMapRefresh;
        private System.Windows.Forms.RibbonButton btnHomeMapClearSelection;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralSearch;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundarySearch;
        private System.Windows.Forms.RibbonPanel poiGeneralPanel;
        private System.Windows.Forms.RibbonButton btnPoiGeneralSearch;
        private System.Windows.Forms.RibbonPanel buildingGroupPanel;
        private System.Windows.Forms.RibbonButton btnBuildingGroupSearch;
        private System.Windows.Forms.RibbonPanel landmarkBoundaryPanel;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundarySearch;
        private System.Windows.Forms.RibbonButton btnHomeMapPreviousExtent;
        private System.Windows.Forms.RibbonSeparator separator04;
        private System.Windows.Forms.RibbonSeparator separator05;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralAdd;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralEdit;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralDelete;
        private System.Windows.Forms.ToolStripStatusLabel lblSelectionText;
        private System.Windows.Forms.RibbonPanel streetRestrictionPanel;
        private System.Windows.Forms.RibbonTab userTab;
        private System.Windows.Forms.RibbonButton btnHomeWindowSelections;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralMove;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralAdd;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralEdit;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralDelete;
        private System.Windows.Forms.RibbonButton btnBuildingGeneralMove;
        //added by asyrul
        private System.Windows.Forms.RibbonButton btnBuildingGeneralReassociate;
        //added by asyrul
        private System.Windows.Forms.RibbonButton btnBuildingGroupAdd;
        private System.Windows.Forms.RibbonButton btnBuildingGroupEdit;
        private System.Windows.Forms.RibbonButton btnBuildingGroupDelete;
        private System.Windows.Forms.RibbonButton btnBuildingGroupMove;
        private System.Windows.Forms.RibbonButton btnPoiGeneralEdit;
        private System.Windows.Forms.RibbonButton btnStreetRestrictionSearch;
        private System.Windows.Forms.RibbonPanel locationPanel;
        private System.Windows.Forms.RibbonButton btnLocationManage;
        private System.Windows.Forms.RibbonPanel userAccessibilityPanel;
        private System.Windows.Forms.RibbonButton btnUserAccessibilityManage;
        private System.Windows.Forms.RibbonButton btnHomeMapNextExtent;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator1;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator3;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
        private System.Windows.Forms.RibbonButton btnStreetGeneralCopy;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator6;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralMove;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryAdd;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator7;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryDelete;
        private System.Windows.Forms.ContextMenuStrip menuDocument;
        private System.Windows.Forms.ToolStripMenuItem menuPan;
        private System.Windows.Forms.ToolStripMenuItem menuZoomIn;
        private System.Windows.Forms.ToolStripMenuItem menuZoomOut;
        private System.Windows.Forms.ToolStripMenuItem menuZoomToSelection;
        private System.Windows.Forms.ToolStripSeparator separator12;
        private System.Windows.Forms.ToolStripMenuItem menuFullExtent;
        private System.Windows.Forms.ToolStripMenuItem menuPreviousExtent;
        private System.Windows.Forms.ToolStripMenuItem menuNextExtent;
        private System.Windows.Forms.ToolStripSeparator separator13;
        private System.Windows.Forms.ToolStripMenuItem menuRefresh;
        private System.Windows.Forms.ToolStripMenuItem menuClearSelection;
        private System.Windows.Forms.ToolStripSeparator separator14;
        private System.Windows.Forms.ToolStripMenuItem menuRuler;
        private System.Windows.Forms.RibbonButton btnStreetGeneralFlip;
        private System.Windows.Forms.OpenFileDialog openShapeDialog;
        private System.Windows.Forms.RibbonButton btnHomeMapSelect;
        private System.Windows.Forms.ToolStripMenuItem menuSelect;
        private System.Windows.Forms.ToolStripSeparator separator11;
        private System.Windows.Forms.RibbonComboBox cbScale;
        private System.Windows.Forms.RibbonButton btnStreetGeneralShowProperty;
        private System.Windows.Forms.RibbonButton btnJunctionGeneralDelete;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator2;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator5;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator8;
        private System.Windows.Forms.RibbonButton btnJunctionGeneralSplit;
        // added by asyrul
        private System.Windows.Forms.RibbonButton btnJunctionGeneralReassociate;
        // added end
        private System.Windows.Forms.RibbonSeparator ribbonSeparator9;
        private System.Windows.Forms.RibbonButton btnStreetRestrictionAdd;
        private System.Windows.Forms.RibbonButton btnStreetRestrictionDelete;
        private System.Windows.Forms.RibbonPanel propertyFloorPanel;
        private System.Windows.Forms.RibbonButton btnPropertyFloorSearch;
        private System.Windows.Forms.RibbonPanel homeToolbox;
        private System.Windows.Forms.RibbonButton btnHomeToolboxGoTo;
        private System.Windows.Forms.ToolStripComboBox cbMenuScale;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralReassociate;
        private System.Windows.Forms.RibbonButton btnLandmarkGeneralReassociate;
        private System.Windows.Forms.RibbonButton btnBuildingGroupReassociate;
        private System.Windows.Forms.RibbonPanel buildingMultiStoreyPanel;
        private System.Windows.Forms.RibbonButton btnBuildingMultiStoreySearch;
        //added by asyrul
        private System.Windows.Forms.RibbonButton btnAssociateBuildingToBuildingGroup;
        private System.Windows.Forms.RibbonButton btnAssociateBuildingByDistance;
        private System.Windows.Forms.RibbonButton btnBuildingMultiStoreySearchById;
        //added end
        private System.Windows.Forms.OpenFileDialog openRasterDialog;
        private System.Windows.Forms.OpenFileDialog openGPSDialog;
        private System.Windows.Forms.FolderBrowserDialog openGdbDialog;
        private System.Windows.Forms.RibbonTab editorTab;
        private System.Windows.Forms.RibbonPanel editorGeneralPanel;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenShape;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenGdb;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenRaster;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenGPS;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator13;
        private System.Windows.Forms.RibbonButton btnHomeMapNoTool;
        private System.Windows.Forms.ToolStripStatusLabel lblWorkspace;
        private System.Windows.Forms.ToolStripStatusLabel lblWorkspaceText;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenWorkOrder;
        private System.Windows.Forms.RibbonButton btnPoiGeneralMerge;
        private System.Windows.Forms.RibbonButton btnStreetGeneralReshape;
        //added by asyrul
        private System.Windows.Forms.RibbonButton btnStreetGeneralCopyAttribute;
        private System.Windows.Forms.RibbonButton btnStreetGeneralSearchById;
        private System.Windows.Forms.RibbonButton btnStreetGeneralSearchByName;

        private System.Windows.Forms.RibbonButton btnJunctionSearchById;

        private System.Windows.Forms.RibbonButton btnPropertyGeneralSearchById;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralSearchByName;

        private System.Windows.Forms.RibbonButton btnPropertyFloorSearchById;
        // added end
        private System.Windows.Forms.RibbonSeparator ribbonSeparator12;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator11;
        private System.Windows.Forms.RibbonDescriptionMenuItem menuFileOpenMap;
        private System.Windows.Forms.RibbonButton btnEditorDrawLine;
        private System.Windows.Forms.RibbonButton btnEditorDrawCircle;
        private System.Windows.Forms.RibbonButton btnEditorDrawRectangle;
        private System.Windows.Forms.RibbonButton btnEditorDrawPolygon;
        private System.Windows.Forms.RibbonButton btnEditorDrawClearDrawings;
        private System.Windows.Forms.ToolStripMenuItem menuNoTool;
        private System.Windows.Forms.RibbonButton btnUserAccessibilityChangePassword;
        private System.Windows.Forms.RibbonButton btnPoiGeneralShowStreet;
        private System.Windows.Forms.RibbonTab phoneTab;
        private System.Windows.Forms.RibbonPanel phoneGeneralPanel;
        private System.Windows.Forms.RibbonButton btnPhoneGeneralSearch;
        // added by asyrul
        private System.Windows.Forms.RibbonButton btnPhoneSearchByNo;
        // added end
        private System.Windows.Forms.RibbonButton btnStreetRestrictionShowStreet;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralAddMultiple;
        private System.Windows.Forms.RibbonButton btnStreetRestrictionMove;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator10;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryAdd;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryEdit;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryDelete;
        private System.Windows.Forms.RibbonPanel locationSectionBoundaryVertexPanel;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryVertexAdd;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryVertexMove;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryVertexDelete;
        private System.Windows.Forms.RibbonButton btnLocationSectionBoundaryMove;
        private System.Windows.Forms.RibbonButton btnPropertyGeneralEditMultiple;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryMove;
        private System.Windows.Forms.RibbonPanel landmarkBoundaryVertexPanel;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryVertexAdd;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryVertexMove;
        private System.Windows.Forms.RibbonButton btnLandmarkBoundaryVertexDelete;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonTab WorkAreaANDTab;
        private System.Windows.Forms.RibbonPanel ANDWorkAreaPanel;
        private System.Windows.Forms.RibbonButton btnSearchWorkArea;
        private System.Windows.Forms.RibbonButton btnVerifyWorkArea;
        private System.Windows.Forms.RibbonButton btnCreateWA;
        private System.Windows.Forms.RibbonButton btnDeleteWA;
        private System.Windows.Forms.RibbonButton btnCompleteWA;
        private System.Windows.Forms.RibbonButton btnCloseWA;
        private System.Windows.Forms.RibbonPanel workAreaVertexPanel;
        private System.Windows.Forms.RibbonButton btnWorkAreaVertexAdd;
        private System.Windows.Forms.RibbonButton btnWorkAreaVertexMove;
        private System.Windows.Forms.RibbonButton btnWorkAreaVertexDelete;
        private System.Windows.Forms.RibbonTextBox txtUserName;
        private System.Windows.Forms.RibbonTextBox txtUserGroup;
        private System.Windows.Forms.RibbonButton btnVerifyWorkArea2;
        private System.Windows.Forms.RibbonButton btnDissociateBuildingToBuildingGroup;

        // noraini - Aug 2021
        private System.Windows.Forms.RibbonPanel jupemPanel;
        private System.Windows.Forms.RibbonButton btnSearchJupemLot;
		private System.Windows.Forms.RibbonTab rasterTab;
		private System.Windows.Forms.RibbonPanel rasterPanel;
		private System.Windows.Forms.RibbonButton btnOpenRasterWarping;
		private System.Windows.Forms.RibbonButton btnCloseRasterWarping;
		private System.Windows.Forms.RibbonPanel warpingPanel;
		private System.Windows.Forms.RibbonButton btnAddPoint;
		private System.Windows.Forms.RibbonButton btnDeletePoint;
		private System.Windows.Forms.RibbonButton btnDeleteAllPoints;
		private System.Windows.Forms.RibbonButton btnUpdateAlignment;
		private System.Windows.Forms.RibbonButton btnFitToDisplay;
		private System.Windows.Forms.RibbonButton btnRegisterRaster;
		private System.Windows.Forms.RibbonButton btnBuildPyramid;
		private System.Windows.Forms.RibbonButton btnMoveRaster;
		private System.Windows.Forms.RibbonButton btnScaleUp;
		private System.Windows.Forms.RibbonButton btnScaleDown;
		private System.Windows.Forms.RibbonButton btnRotateClockwise;
		private System.Windows.Forms.RibbonButton btnRotateAntiClockwise;
		private System.Windows.Forms.RibbonPanel editRasterPanel;
		private System.Windows.Forms.RibbonButton btnOpenRasterInPaint;
		private System.Windows.Forms.RibbonButton btnExportRaster;
        private System.Windows.Forms.RibbonPanel Cadu3Panel;
        private System.Windows.Forms.RibbonButton btnVerifyWeb;

        //Additional
        //private System.Windows.Forms.Button btnAerialView;
    }
}
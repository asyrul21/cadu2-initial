﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geomatic.Core;
using Geomatic.Core.Oracle;
using Geomatic.Core.Rows;
using NUnit.Framework;

namespace GeoCoreOracle.Tests
{
    [TestFixture]
    public class OracleDBConnectionTests
    {
        private string _id = "00360352989";

        [Test]
        public void canConnectToDB()
        {
            //Arrange / act
            System.Diagnostics.Debug.WriteLine("Hello!");

            OracleDBConnection cnn = new OracleDBConnection();
            string res = cnn.testConnection();

            //assert
            Assert.That(res, Is.EqualTo("Connection Success and closed successfully"));
        }

        //all is ok
        //[Test]
        //public void produceUpdateStatement()
        //{
        //    // Arrange
        //    string expected = "UPDATE KV_TEL_NO SET STREET_NAME='DAMAI 2/3A',CUSTOMER_NAME='MAH SUN YEEN',EXCHANGE_ABB='BAGX' WHERE PHONE_NUMBER='00360352989'";
        //    TelNoQueryBuilder query = new TelNoQueryBuilder(_id, SegmentName.KV);

        //    // act
        //    string testresult = query.ungeocodeTelNoStatement;

        //    //assert
        //    Assert.That(testresult, Is.EqualTo(expected));
        //}

        //[Test]
        //public void produceSelectStatement()
        //{
        //    // Arrange
        //    string expected = "SELECT * from KV_TEL_NO WHERE PHONE_NUMBER = '00360352989'";
        //    TelNoQueryBuilder query = new TelNoQueryBuilder(_id, SegmentName.KV);

        //    // act
        //    string testresult = query.selectDataStatement;

        //    //assert
        //    Assert.That(testresult, Is.EqualTo(expected));
        //}

        //[Test]
        //public void selectData()
        //{
        //    // Arrange

        //    string expected = "MAH SUN YEE";
        //    TelNoUngeocoder ung = new TelNoUngeocoder(SegmentName.KV);

        //    // act
        //    string testresult = ung.findTelNoById(_id);

        //    //assert
        //    Assert.That(testresult, Is.EqualTo(expected));
        //}

        //[Test]
        //public void realUngeocodeStatement()
        //{
        //    // Arrange
        //    string expected = "Updating success";
        //    TelNoUngeocoder ung = new TelNoUngeocoder(SegmentName.KV);

        //    // act
        //    string testresult = ung.ungeocodeTelNo(_id);

        //    //assert
        //    Assert.That(testresult, Is.EqualTo(expected));
        //}
    }
}

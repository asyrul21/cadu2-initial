﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using System.Runtime.InteropServices;

namespace TextMapper
{
    public static class GeodatabaseExt
    {
        /// <summary>
        /// Readonly
        /// </summary>
        /// <param name="table"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        internal static IEnumerable<IRow> ReadRows(this ITable table, IQueryFilter filter)
        {
            ICursor cursor = table.Search(filter, true);

            try
            {
                IRow row = null;

                while ((row = cursor.NextRow()) != null)
                {
                    yield return row;
                }
            }
            finally
            {
                while (Marshal.ReleaseComObject(cursor) != 0) { }
            }
        }

        /// <summary>
        /// Finds a row by the OID and returns the data as an object of the specified type.
        /// If no match is found, returns null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public static T FindItemByOID<T>(this ITable table, int oid) where T : TextFeature, new()
        {
            return new TextMapper<T>(table).SelectItem(oid);
        }

        /// <summary>
        /// Finds a feature by the OID and returns the data as an object of the specified type.
        /// If no match is found, returns null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="featureClass"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        public static T FindItemByOID<T>(this IFeatureClass featureClass, int oid) where T : TextFeature, new()
        {
            return ((ITable)featureClass).FindItemByOID<T>(oid);
        }

        /// <summary>
        /// Reads all rows whose OIDs are within the specified collection and returns them as a (lazily-evaluated) sequence of objects of the specified type.  The returned collection is not sorted.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="oids"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this ITable table, IEnumerable<int> oids) where T : TextFeature, new()
        {
            foreach (IEnumerable<int> batch in oids.Distinct().Partition(100))
            {
                string[] array = batch.Select(n => n.ToString()).ToArray();

                string whereClause = string.Format("{0} in ({1})", table.OIDFieldName, array.Length == 0 ? "-1" : string.Join(",", array));

                foreach (T item in table.MapText<T>(whereClause))
                    yield return item;
            }
        }

        /// <summary>
        /// Reads all features whose OIDs are within the specified collection and returns them as a (lazily-evaluated) sequence of objects of the specified type.  The returned collection is not sorted.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="featureClass"></param>
        /// <param name="oids"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this IFeatureClass featureClass, IEnumerable<int> oids) where T : TextFeature, new()
        {
            return ((ITable)featureClass).MapText<T>(oids);
        }

        /// <summary>
        /// Reads all rows meeting the filter criteria as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="whereClause">SQL where clause.</param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this ITable table, string whereClause) where T : TextFeature, new()
        {
            return table.MapText<T>(whereClause, null);
        }

        /// <summary>
        /// Reads all features meeting the filter criteria as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="featureClass"></param>
        /// <param name="whereClause">SQL where clause.</param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this IFeatureClass featureClass, string whereClause) where T : TextFeature, new()
        {
            return ((ITable)featureClass).MapText<T>(whereClause);
        }

        /// <summary>
        /// Reads all rows meeting the filter criteria as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="whereClause">SQL where clause.</param>
        /// <param name="postfixClause">A clause that will be appended to the end of the where clause (i.e. ORDER BY).</param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this ITable table, string whereClause, string postfixClause) where T : TextFeature, new()
        {
            IQueryFilter filter = new QueryFilter { WhereClause = whereClause };

            if (!string.IsNullOrEmpty(postfixClause))
                ((IQueryFilterDefinition)filter).PostfixClause = postfixClause;

            return new TextMapper<T>(table).SelectItems(filter);
        }

        /// <summary>
        /// Reads all features meeting the filter criteria as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="featureClass"></param>
        /// <param name="whereClause">SQL where clause.</param>
        /// <param name="postfixClause">A clause that will be appended to the end of the where clause (i.e. ORDER BY).</param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this IFeatureClass featureClass, string whereClause, string postfixClause) where T : TextFeature, new()
        {
            return ((ITable)featureClass).MapText<T>(whereClause, postfixClause);
        }

        /// <summary>
        /// Reads all rows meeting the filter criteria as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="filter">The query filter.</param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this ITable table, IQueryFilter filter) where T : TextFeature, new()
        {
            return new TextMapper<T>(table).SelectItems(filter);
        }

        /// <summary>
        /// Reads all features as a (lazily-evaluated) sequence of objects of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="featureClass"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapText<T>(this IFeatureClass featureClass, IQueryFilter filter) where T : TextFeature, new()
        {
            return ((ITable)featureClass).MapText<T>(filter);
        }
    }
}

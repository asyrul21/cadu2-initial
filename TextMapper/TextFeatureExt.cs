﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace TextMapper
{
    public static class TextFeatureExt
    {
        /// <summary>
        /// Inserts this item into a table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to insert.</param>
        /// <param name="table">The target table.</param>
        /// <returns>The inserted item (with a newly assigned OID).</returns>
        public static T InsertTextInto<T>(this T item, ITable table) where T : TextFeature, new()
        {
            return new TextMapper<T>(table).Insert(item);
        }

        /// <summary>
        /// Inserts this item into a feature class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to insert.</param>
        /// <param name="featureClass">The target feature class.</param>
        /// <returns>The inserted item (with a newly assigned OID).</returns>
        public static T InsertTextInto<T>(this T item, IFeatureClass featureClass) where T : TextFeature, new()
        {
            return item.InsertTextInto((ITable)featureClass);
        }

        /// <summary>
        /// Updates this item in the underlying table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to update.  The item must be bound to a table and must have a valid OID.</param>
        public static void UpdateText<T>(this T item) where T : TextFeature, new()
        {
            new TextMapper<T>(item.Table).Update(item);
        }

        /// <summary>
        /// Deletes this item from the underlying table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item to delete.  The item must be bound to a table and must have a valid OID.</param>
        public static void DeleteText<T>(this T item) where T : TextFeature, new()
        {
            new TextMapper<T>(item.Table).Delete(item);
        }
    }
}

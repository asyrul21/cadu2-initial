﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Display;

namespace TextMapper
{
    public abstract class TextFeature
    {
        internal ITable Table { get; set; }

        private readonly Type _type;
        private double _angle = 0;
        private double _size = 3;
        private esriTextHorizontalAlignment _horizontalAlignment = esriTextHorizontalAlignment.esriTHACenter;
        private esriTextVerticalAlignment _verticalAlignment = esriTextVerticalAlignment.esriTVABaseline;
        private IGeometry _shape;

        /// <summary>
        /// The OID of the item.
        /// If an item is not bound to a feature class (if it is created from scratch), the OID is set to -1.
        /// </summary>
        public int OID { get; internal set; }

        public double Angle
        {
            get
            {
                return _angle;
            }
            set
            {
                _angle = value;
            }
        }

        public string Text { set; get; }

        public double Size
        {
            set
            {
                _size = value;
            }
            get
            {
                return _size;
            }
        }

        public esriTextHorizontalAlignment HorizontalAlignment
        {
            set
            {
                _horizontalAlignment = value;
            }
            get
            {
                return _horizontalAlignment;
            }
        }

        public esriTextVerticalAlignment VerticalAlignment
        {
            set
            {
                _verticalAlignment = value;
            }
            get
            {
                return _verticalAlignment;
            }
        }

        public int? LinkedFeatureID { set; get; }

        public IGeometry Shape
        {
            get { return _shape; }
            set
            {
                _shape = value.Clone();
            }
        }

        public bool IsDataBound
        {
            get { return Table != null && OID > 0; }
        }

        protected TextFeature()
        {
            _type = GetType();
            OID = -1;
        }
    }
}

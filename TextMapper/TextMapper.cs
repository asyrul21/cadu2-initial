﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;

namespace TextMapper
{
    /// <summary>
    /// The element is locked. 
    /// It is good that you have a better way to implement everything here.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class TextMapper<T> where T : TextFeature, new()
    {
        private readonly ITable _table;

        public TextMapper(ITable table)
        {
            _table = table;
        }

        public T SelectItem(int oid)
        {
            IRow row;

            try { row = _table.GetRow(oid); }
            catch { return null; }

            // handle row == null
            if (row == null)
                return null;

            return Read(row);
        }

        public IEnumerable<T> SelectItems(IQueryFilter filter)
        {
            return _table.ReadRows(filter).Select(Read);
        }

        private T Read(IRow row)
        {
            if (!(row is IAnnotationFeature))
            {
                throw new Exception("Not annotation feature");
            }
            IAnnotationFeature annoFeature = (IAnnotationFeature)row;
            ITextElement textElement = (ITextElement)annoFeature.Annotation;
            IElement element = (IElement)annoFeature.Annotation;
            ITextSymbol textSymbol = textElement.Symbol;

            T item = Activator.CreateInstance<T>();
            item.Table = _table;
            item.OID = _table.HasOID ? row.OID : -1;
            item.Angle = textSymbol.Angle;
            item.Text = textElement.Text;
            item.Size = textSymbol.Size;
            item.HorizontalAlignment = textSymbol.HorizontalAlignment;
            item.VerticalAlignment = textSymbol.VerticalAlignment;
            item.LinkedFeatureID = annoFeature.LinkedFeatureID;
            item.Shape = element.Geometry;

            return item;
        }

        private T Write(T item, IRow row, bool isUpdate)
        {
            if (!(row is IAnnotationFeature))
            {
                throw new Exception("Not annotation feature");
            }
            IAnnotationFeature annoFeature = (IAnnotationFeature)row;
            ITextElement textElement = new TextElementClass();
            textElement.Text = item.Text;

            ITextSymbol textSymbol = textElement.Symbol;
            textSymbol.Angle = item.Angle;
            textSymbol.Size = item.Size;
            textSymbol.HorizontalAlignment = item.HorizontalAlignment;
            textSymbol.VerticalAlignment = item.VerticalAlignment;

            textElement.Symbol = textSymbol;

            IElement element = (IElement)textElement;
            element.Geometry = item.Shape;

            annoFeature.LinkedFeatureID = item.LinkedFeatureID.Value;
            annoFeature.Annotation = element;

            row.Store();

            return Read(_table.GetRow(row.OID));
        }

        public T Insert(T item)
        {
            return Write(item, _table.CreateRow(), false);
        }

        public void Update(T item)
        {
            if (!item.IsDataBound)
                throw new Exception("This item cannot be updated because it is not bound to a table.");

            Write(item, _table.GetRow(item.OID), true);
        }

        public void Delete(T item)
        {
            if (!item.IsDataBound)
                throw new Exception("This item cannot be deleted because it is not bound to a table.");

            _table.GetRow(item.OID).Delete();

            item.Table = null;
            item.OID = -1;
        }
    }
}

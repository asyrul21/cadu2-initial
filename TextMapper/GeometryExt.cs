﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geometry;

namespace TextMapper
{
    internal static class GeometryExt
    {
        /// <summary>
        /// Clones the current geometry without altering the state of the original object.
        /// </summary>
        /// <typeparam name="T">The type of geometry.</typeparam>
        /// <param name="shape">The current geometry.</param>
        /// <returns>A new copy of the geometry.</returns>
        public static T Clone<T>(this T shape) where T : class, IGeometry
        {
            if (shape == null)
                return default(T);

            IClone clone = (IClone)shape;
            return (T)clone.Clone();
        }
    }
}

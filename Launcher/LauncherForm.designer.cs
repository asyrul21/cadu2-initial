﻿namespace Launcher
{
    partial class LauncherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxIcon = new System.Windows.Forms.PictureBox();
            this.lblReleaseNotes = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblUpdate = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSkip = new System.Windows.Forms.Button();
            this.btnRemindLater = new System.Windows.Forms.Button();
            this.webKit = new WebKit.WebKitBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Image = global::Launcher.Properties.Resources.update;
            this.pictureBoxIcon.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBoxIcon.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(70, 66);
            this.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxIcon.TabIndex = 24;
            this.pictureBoxIcon.TabStop = false;
            // 
            // lblReleaseNotes
            // 
            this.lblReleaseNotes.AutoSize = true;
            this.lblReleaseNotes.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblReleaseNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblReleaseNotes.Location = new System.Drawing.Point(9, 88);
            this.lblReleaseNotes.Name = "lblReleaseNotes";
            this.lblReleaseNotes.Size = new System.Drawing.Size(102, 17);
            this.lblReleaseNotes.TabIndex = 2;
            this.lblReleaseNotes.Text = "Release Notes :";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblDescription.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDescription.Location = new System.Drawing.Point(88, 43);
            this.lblDescription.MaximumSize = new System.Drawing.Size(550, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(480, 15);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "{0} {1} is now available. You have version {2} installed. Would you like to downl" +
                "oad it now?";
            // 
            // lblUpdate
            // 
            this.lblUpdate.AutoSize = true;
            this.lblUpdate.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblUpdate.Location = new System.Drawing.Point(88, 12);
            this.lblUpdate.MaximumSize = new System.Drawing.Size(560, 0);
            this.lblUpdate.Name = "lblUpdate";
            this.lblUpdate.Size = new System.Drawing.Size(227, 19);
            this.lblUpdate.TabIndex = 0;
            this.lblUpdate.Text = "A new version of {0} is available!";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnUpdate.Image = global::Launcher.Properties.Resources.download;
            this.btnUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnUpdate.Location = new System.Drawing.Point(518, 373);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(135, 28);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSkip
            // 
            this.btnSkip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSkip.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btnSkip.Enabled = false;
            this.btnSkip.Image = global::Launcher.Properties.Resources.hand_point;
            this.btnSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSkip.Location = new System.Drawing.Point(379, 373);
            this.btnSkip.Margin = new System.Windows.Forms.Padding(2);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(135, 28);
            this.btnSkip.TabIndex = 5;
            this.btnSkip.Text = "Skip this version";
            this.btnSkip.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSkip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSkip.UseVisualStyleBackColor = true;
            this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // btnRemindLater
            // 
            this.btnRemindLater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemindLater.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnRemindLater.Enabled = false;
            this.btnRemindLater.Image = global::Launcher.Properties.Resources.clock_play;
            this.btnRemindLater.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRemindLater.Location = new System.Drawing.Point(240, 373);
            this.btnRemindLater.Margin = new System.Windows.Forms.Padding(2);
            this.btnRemindLater.Name = "btnRemindLater";
            this.btnRemindLater.Size = new System.Drawing.Size(135, 28);
            this.btnRemindLater.TabIndex = 4;
            this.btnRemindLater.Text = "Remind me later";
            this.btnRemindLater.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemindLater.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRemindLater.UseVisualStyleBackColor = true;
            this.btnRemindLater.Click += new System.EventHandler(this.btnRemindLater_Click);
            // 
            // webKit
            // 
            this.webKit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.webKit.BackColor = System.Drawing.Color.White;
            this.webKit.Location = new System.Drawing.Point(12, 108);
            this.webKit.Name = "webKit";
            this.webKit.Size = new System.Drawing.Size(641, 260);
            this.webKit.TabIndex = 3;
            this.webKit.Url = null;
            // 
            // LauncherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 412);
            this.Controls.Add(this.webKit);
            this.Controls.Add(this.pictureBoxIcon);
            this.Controls.Add(this.lblReleaseNotes);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblUpdate);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSkip);
            this.Controls.Add(this.btnRemindLater);
            this.Name = "LauncherForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LauncherForm";
            this.Shown += new System.EventHandler(this.LauncherForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIcon;
        private System.Windows.Forms.Label lblReleaseNotes;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSkip;
        private System.Windows.Forms.Button btnRemindLater;
        private WebKit.WebKitBrowser webKit;
    }
}
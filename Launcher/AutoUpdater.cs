﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.ComponentModel;
using System.Reflection;
using Microsoft.Win32;
using System.Net;
using System.IO;
using System.Xml;
using System.Threading;
using System.Net.Cache;
using System.Windows.Forms;
using System.Diagnostics;

namespace Launcher
{
    public static class AutoUpdater
    {
        public const string TargetFile = "Zerg.exe";

        internal static string DialogTitle;
        internal static string ChangeLogURL;
        internal static List<string> DownloadURLs;
        internal static string RegistryLocation;
        internal static string AppTitle;
        internal static Version CurrentVersion;
        internal static Version InstalledVersion;

        /// <summary>
        /// URL of the xml file that contains information about latest version of the application.
        /// </summary>
        /// 
        public static String AppCastURL;

        /// <summary>
        /// Opens the download url in default browser if true. Very usefull if you have portable application.
        /// </summary>
        public static bool OpenDownloadPage;

        /// <summary>
        /// Sets the current culture of the auto update notification window. Set this value if your application supports functionalty to change the languge of the application.
        /// </summary>
        public static CultureInfo CurrentCulture;

        /// <summary>
        /// If this is true users see dialog where they can set remind later interval otherwise it will take the interval from RemindLaterAt and RemindLaterTimeSpan fields.
        /// </summary>
        public static Boolean LetUserSelectRemindLater = true;

        /// <summary>
        /// Remind Later interval after user should be reminded of update.
        /// </summary>
        public static int RemindLaterAt = 2;

        /// <summary>
        /// Set if RemindLaterAt interval should be in Minutes, Hours or Days.
        /// </summary>
        public static RemindLaterFormat RemindLaterTimeSpan = RemindLaterFormat.Days;

        public static void Start()
        {
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += BackgroundWorker_DoWork;
            backgroundWorker.RunWorkerAsync();
        }

        public static DialogResult CheckVersion(string appCast, bool developmentFlag)
        {
            AppCastURL = appCast;
            string file = Directory.GetCurrentDirectory() + @"\" + TargetFile;

            if (!File.Exists(file))
            {
            }

            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(file);

            string productName = fileVersionInfo.ProductName;
            string productVersion = fileVersionInfo.ProductVersion;
            string companyName = fileVersionInfo.CompanyName;
            string description = fileVersionInfo.FileDescription;

            InstalledVersion = new Version(productVersion);

            AppTitle = !string.IsNullOrEmpty(productName) ? productName : description;

            RegistryLocation = !string.IsNullOrEmpty(companyName) ? string.Format(@"Software\{0}\{1}\AutoUpdater", companyName, AppTitle) : string.Format(@"Software\{0}\AutoUpdater", AppTitle);
            //RegistryKey updateKey = Registry.CurrentUser.OpenSubKey(RegistryLocation);

            //if (updateKey != null)
            //{
            //    object remindLaterTime = updateKey.GetValue("remindlater");

            //    if (remindLaterTime != null)
            //    {
            //        DateTime remindLater = Convert.ToDateTime(remindLaterTime.ToString(), CultureInfo.CreateSpecificCulture("en-US"));
            //        int compareResult = DateTime.Compare(DateTime.Now, remindLater);

            //        if (compareResult < 0)
            //        {
            //            var updateForm = new LauncherForm();
            //            updateForm.SetTimer(remindLater);
            //            return DialogResult.No;
            //        }
            //    }
            //}


            WebRequest webRequest = WebRequest.Create(AppCastURL);
            webRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            WebResponse webResponse;

            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch (Exception)
            {
                return DialogResult.Abort;
            }

            Stream appCastStream = webResponse.GetResponseStream();

            var receivedAppCastDocument = new XmlDocument();

            if (appCastStream != null)
            {
                receivedAppCastDocument.Load(appCastStream);
            }
            else
            {
                return DialogResult.Abort;
            }

            XmlNodeList appCastItems = receivedAppCastDocument.SelectNodes("item");

            if (appCastItems != null)
            {
                foreach (XmlNode item in appCastItems)
                {
                    XmlNode appCastVersion = item.SelectSingleNode("version");
                    if (appCastVersion != null)
                    {
                        string appVersion = appCastVersion.InnerText;
                        var version = new Version(appVersion);
                        if (version <= InstalledVersion)
                        {
                            continue;
                        }
                        CurrentVersion = version;
                    }
                    else
                    {
                        continue;
                    }

                    XmlNode appCastTitle = item.SelectSingleNode("title");
                    DialogTitle = appCastTitle != null ? appCastTitle.InnerText : "";

                    if (developmentFlag)
                    {
                        DialogTitle = string.Format("{0} (Development)", DialogTitle);
                    }

                    XmlNode appCastChangeLog = item.SelectSingleNode("changelog");
                    ChangeLogURL = appCastChangeLog != null ? appCastChangeLog.InnerText : "";

                    XmlNodeList appCastUrls = item.SelectSingleNode("urls").ChildNodes;
                    DownloadURLs = new List<string>();
                    foreach (XmlNode appCastUrl in appCastUrls)
                    {
                        string downloadURL = appCastUrl != null ? appCastUrl.InnerText : "";
                        if (!string.IsNullOrEmpty(downloadURL))
                        {
                            DownloadURLs.Add(downloadURL);
                        }
                    }
                }
            }

            //if (updateKey != null)
            //{
            //    object skip = updateKey.GetValue("skip");
            //    object applicationVersion = updateKey.GetValue("version");
            //    if (skip != null && applicationVersion != null)
            //    {
            //        string skipValue = skip.ToString();
            //        var skipVersion = new Version(applicationVersion.ToString());
            //        if (skipValue.Equals("1") && CurrentVersion <= skipVersion)
            //        {
            //            return DialogResult.Cancel;
            //        }
            //        if (CurrentVersion > skipVersion)
            //        {
            //            RegistryKey updateKeyWrite = Registry.CurrentUser.CreateSubKey(RegistryLocation);
            //            if (updateKeyWrite != null)
            //            {
            //                updateKeyWrite.SetValue("version", CurrentVersion.ToString());
            //                updateKeyWrite.SetValue("skip", 0);
            //            }
            //        }
            //    }
            //    updateKey.Close();
            //}

            if (CurrentVersion > InstalledVersion)
            {
                var form = new LauncherForm();
                return form.ShowDialog();
            }

            return DialogResult.Cancel;
        }

        private static void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //var mainAssembly = Assembly.GetEntryAssembly();
            //var companyAttribute = (AssemblyCompanyAttribute)GetAttribute(mainAssembly, typeof(AssemblyCompanyAttribute));
            //var titleAttribute = (AssemblyTitleAttribute)GetAttribute(mainAssembly, typeof(AssemblyTitleAttribute));
            //AppTitle = titleAttribute != null ? titleAttribute.Title : mainAssembly.GetName().Name;
            //var appCompany = companyAttribute != null ? companyAttribute.Company : "";

            //RegistryLocation = !string.IsNullOrEmpty(appCompany) ? string.Format(@"Software\{0}\{1}\AutoUpdater", appCompany, AppTitle) : string.Format(@"Software\{0}\AutoUpdater", AppTitle);

            //RegistryKey updateKey = Registry.CurrentUser.OpenSubKey(RegistryLocation);

            //if (updateKey != null)
            //{
            //    object remindLaterTime = updateKey.GetValue("remindlater");

            //    if (remindLaterTime != null)
            //    {
            //        DateTime remindLater = Convert.ToDateTime(remindLaterTime.ToString(), CultureInfo.CreateSpecificCulture("en-US"));

            //        int compareResult = DateTime.Compare(DateTime.Now, remindLater);

            //        if (compareResult < 0)
            //        {
            //            var updateForm = new LauncherForm();
            //            updateForm.SetTimer(remindLater);
            //            return;
            //        }
            //    }
            //}

            //InstalledVersion = mainAssembly.GetName().Version;
            //WebRequest webRequest = WebRequest.Create(AppCastURL);
            //webRequest.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            //WebResponse webResponse;

            //try
            //{
            //    webResponse = webRequest.GetResponse();
            //}
            //catch (Exception)
            //{
            //    return;
            //}

            //Stream appCastStream = webResponse.GetResponseStream();

            //var receivedAppCastDocument = new XmlDocument();

            //if (appCastStream != null) receivedAppCastDocument.Load(appCastStream);
            //else return;

            //XmlNodeList appCastItems = receivedAppCastDocument.SelectNodes("item");

            //if (appCastItems != null)
            //{
            //    foreach (XmlNode item in appCastItems)
            //    {
            //        XmlNode appCastVersion = item.SelectSingleNode("version");
            //        if (appCastVersion != null)
            //        {
            //            String appVersion = appCastVersion.InnerText;
            //            var version = new Version(appVersion);
            //            if (version <= InstalledVersion)
            //                continue;
            //            CurrentVersion = version;
            //        }
            //        else
            //            continue;

            //        XmlNode appCastTitle = item.SelectSingleNode("title");
            //        DialogTitle = appCastTitle != null ? appCastTitle.InnerText : "";
            //        XmlNode appCastChangeLog = item.SelectSingleNode("changelog");
            //        ChangeLogURL = appCastChangeLog != null ? appCastChangeLog.InnerText : "";

            //        XmlNodeList appCastUrls = item.SelectSingleNode("urls").ChildNodes;
            //        DownloadURLs = new List<string>();
            //        foreach (XmlNode appCastUrl in appCastUrls)
            //        {
            //            string downloadURL = appCastUrl != null ? appCastUrl.InnerText : "";
            //            if (downloadURL != "")
            //            {
            //                DownloadURLs.Add(downloadURL);
            //            }
            //        }
            //    }
            //}

            //if (updateKey != null)
            //{
            //    object skip = updateKey.GetValue("skip");
            //    object applicationVersion = updateKey.GetValue("version");
            //    if (skip != null && applicationVersion != null)
            //    {
            //        string skipValue = skip.ToString();
            //        var skipVersion = new Version(applicationVersion.ToString());
            //        if (skipValue.Equals("1") && CurrentVersion <= skipVersion)
            //            return;
            //        if (CurrentVersion > skipVersion)
            //        {
            //            RegistryKey updateKeyWrite = Registry.CurrentUser.CreateSubKey(RegistryLocation);
            //            if (updateKeyWrite != null)
            //            {
            //                updateKeyWrite.SetValue("version", CurrentVersion.ToString());
            //                updateKeyWrite.SetValue("skip", 0);
            //            }
            //        }
            //    }
            //    updateKey.Close();
            //}

            //if (CurrentVersion == null)
            //    return;

            //if (CurrentVersion > InstalledVersion)
            //{
            //    var thread = new Thread(ShowUI);
            //    thread.CurrentCulture = thread.CurrentUICulture = CurrentCulture ?? System.Windows.Forms.Application.CurrentCulture;
            //    thread.SetApartmentState(ApartmentState.STA);
            //    thread.Start();
            //}
        }

        private static void ShowUI()
        {
            var updateForm = new LauncherForm();
            updateForm.ShowDialog();
        }
    }
}

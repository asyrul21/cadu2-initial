﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Nini.Config;

namespace Launcher
{
    class Program
    {
        private static bool _developmentFlag;

        [STAThread]
        static void Main()
        {
            InitiateConfig();

            string appCastUrl =
                _developmentFlag ?
                "http://200.15.16.29/CADU2/development/appcast.xml" :
                "http://200.15.16.29/CADU2/release/appcast.xml";

            DialogResult result = AutoUpdater.CheckVersion(appCastUrl, _developmentFlag);

            //if ((result == DialogResult.OK) || (result == DialogResult.No))
            //{
            //    RunProgram();
            //}

            //if (result == DialogResult.Abort)
            //{
            //    MessageBox.Show("Update error!", AutoUpdater.AppTitle + " Launcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

            //if (result == DialogResult.Cancel) // already up to date
            //{
            //    RunProgram();
            //}

            RunProgram();
        }

        private static void RunProgram()
        {
            // Prepare the process to run
            Process process = new Process();
            process.StartInfo.FileName = AutoUpdater.TargetFile;
            process.Start();
        }

        private static void InitiateConfig()
        {
            try
            {
                // read from config.ini file
                IConfigSource source = new IniConfigSource("config.ini");
                
                _developmentFlag = source.Configs["General"].GetBoolean("DevelopmentFlag", true);
            }
            catch
            {
                // if no config.ini file, create it
                IniConfigSource newSource = new IniConfigSource();

                IConfig config = newSource.AddConfig("General");
                config.Set("DevelopmentFlag", true);

                newSource.Save("config.ini");

                // set default values
                _developmentFlag = true;
            }
        }
    }
}

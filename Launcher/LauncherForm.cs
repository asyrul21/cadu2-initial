﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;
using Microsoft.Win32;

namespace Launcher
{
    public partial class LauncherForm : Form
    {
        private System.Timers.Timer _timer;

        public LauncherForm()
        {
            InitializeComponent();

            Text = AutoUpdater.DialogTitle + " Updater";
            lblUpdate.Text = string.Format("A new version of {0} is available!", AutoUpdater.AppTitle);
            lblDescription.Text = string.Format("{0} {1} is now available. You have version {2} installed. Would you like to download it now?", AutoUpdater.AppTitle, AutoUpdater.CurrentVersion, AutoUpdater.InstalledVersion);
        }

        private void LauncherForm_Shown(object sender, EventArgs e)
        {
            string url = AutoUpdater.ChangeLogURL;
            string html = string.Format(@"<html>
                            <head>
                            <title>A web page that points a browser to a different page after 2 seconds</title>
                            <meta http-equiv='refresh' content='0.5; URL={0}'>
                            <meta name='keywords' content='automatic redirection'>
                            </head>
                            <body>
                            
                            </body>
                            </html>", url);
            webKit.DocumentText = html;
        }

        private void btnRemindLater_Click(object sender, EventArgs e)
        {
            if (AutoUpdater.LetUserSelectRemindLater)
            {
                var remindLaterForm = new RemindLaterForm();

                var dialogResult = remindLaterForm.ShowDialog();

                if (dialogResult.Equals(DialogResult.OK))
                {
                    AutoUpdater.RemindLaterTimeSpan = remindLaterForm.RemindLaterFormat;
                    AutoUpdater.RemindLaterAt = remindLaterForm.RemindLaterAt;
                }
                else if (dialogResult.Equals(DialogResult.Abort))
                {
                    try
                    {
                        using (DownloadUpdateDialog downloadDialog = new DownloadUpdateDialog(AutoUpdater.DownloadURLs))
                        {
                            downloadDialog.ShowDialog();
                        }
                    }
                    catch (System.Reflection.TargetInvocationException)
                    {
                        return;
                    }
                    return;
                }
                else
                {
                    DialogResult = DialogResult.None;
                    return;
                }
            }

            RegistryKey updateKey = Registry.CurrentUser.CreateSubKey(AutoUpdater.RegistryLocation);
            if (updateKey != null)
            {
                updateKey.SetValue("version", AutoUpdater.CurrentVersion);
                updateKey.SetValue("skip", 0);
                DateTime remindLaterDateTime = DateTime.Now;
                switch (AutoUpdater.RemindLaterTimeSpan)
                {
                    case RemindLaterFormat.Days:
                        remindLaterDateTime = DateTime.Now + TimeSpan.FromDays(AutoUpdater.RemindLaterAt);
                        break;
                    case RemindLaterFormat.Hours:
                        remindLaterDateTime = DateTime.Now + TimeSpan.FromHours(AutoUpdater.RemindLaterAt);
                        break;
                    case RemindLaterFormat.Minutes:
                        remindLaterDateTime = DateTime.Now + TimeSpan.FromMinutes(AutoUpdater.RemindLaterAt);
                        break;
                    default:
                        throw new Exception("Unknown format.");
                }
                updateKey.SetValue("remindlater", remindLaterDateTime.ToString(CultureInfo.CreateSpecificCulture("en-US")));
                SetTimer(remindLaterDateTime);
                updateKey.Close();
            }
        }

        public void SetTimer(DateTime remindLater)
        {
            TimeSpan timeSpan = remindLater - DateTime.Now;
            _timer = new System.Timers.Timer
            {
                Interval = (int)timeSpan.TotalMilliseconds
            };
            _timer.Elapsed += TimerElapsed;
            _timer.Start();
        }

        private void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Stop();
            AutoUpdater.Start();
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            RegistryKey updateKey = Registry.CurrentUser.CreateSubKey(AutoUpdater.RegistryLocation);
            if (updateKey != null)
            {
                updateKey.SetValue("version", AutoUpdater.CurrentVersion.ToString());
                updateKey.SetValue("skip", 1);
                updateKey.Close();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (AutoUpdater.OpenDownloadPage)
            {

            }
            else
            {
                try
                {
                    using (DownloadUpdateDialog downloadDialog = new DownloadUpdateDialog(AutoUpdater.DownloadURLs))
                    {
                        downloadDialog.ShowDialog();
                    }
                }
                catch (System.Reflection.TargetInvocationException)
                {

                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Diagnostics;
using System.Net.Cache;
using System.IO;

namespace Launcher
{
    public partial class DownloadUpdateDialog : Form
    {
        private readonly List<string> _downloadURLs;

        public DownloadUpdateDialog(List<string> downloadURLs)
        {
            InitializeComponent();
            _downloadURLs = downloadURLs;
        }

        private Queue<string> _downloadUrls = new Queue<string>();

        private void downloadFile(IEnumerable<string> urls)
        {
            foreach (var url in urls)
            {
                _downloadUrls.Enqueue(url);
            }

            // Starts the download
            lblStatus.Text = "Downloading...";
            //btnGetDownload.Enabled = false;
            //progressBar1.Visible = true;
            //lblFileName.Visible = true;

            DownloadFile();
        }

        private void DownloadFile()
        {
            if (_downloadUrls.Any())
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += client_DownloadProgressChanged;
                client.DownloadFileCompleted += client_DownloadFileCompleted;

                var url = _downloadUrls.Dequeue();
                string FileName = Path.GetFileName(url);

                client.DownloadFileAsync(new Uri(url), "C:\\Test\\" + FileName);
                lblStatus.Text = string.Format("Downloading {0}..,", url);
                return;
            }

            // End of the download
            lblStatus.Text = "Download Complete";
        }

        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            progressBar.Value = int.Parse(Math.Truncate(percentage).ToString());
        }

        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // handle error scenario
                throw e.Error;
            }
            if (e.Cancelled)
            {
                // handle cancelled scenario
            }
            DownloadFile();
        }

        private void DownloadUpdateDialog_Load(object sender, EventArgs e)
        {
            downloadFile(_downloadURLs);
        }
    }
}

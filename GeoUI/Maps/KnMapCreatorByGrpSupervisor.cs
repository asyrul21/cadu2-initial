﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.Regions;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Jupem;
using Geomatic.Core.Features.LotBoundaryTexts;
using Geomatic.Core.Features.BuildingGeoints;

namespace Geomatic.UI.Maps
{
    public class KnMapCreatorByGrpSupervisor : MapCreatorByGrpSupervisor
    {
        public KnMapCreatorByGrpSupervisor(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Pahang"; }
        }

        protected override string PoiTableName
        {
            get { return GKnPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GKnBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GKnBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GKnProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GKnLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GKnJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GKnGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GKnStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GKnStreet.TABLE_NAME; }
        }

        protected override string StreetTextTableName
        {
            get { return GKnStreetText.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryTableName
        {
            get { return GKnLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GKnSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GKnHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GKnMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GKnMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GKnRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GKnHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GKnMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GKnMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GKnBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GKnBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GKnBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GKnBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GKnResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GKnResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GKnResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GKnShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GKnShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GKnIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GKnIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GKnCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GKnCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GKnOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GKnOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GKnParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GKnParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GKnLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GKnLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GKnHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GKnHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GKnGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GKnGreenaryPolygon.TABLE_NAME; }
        }

        protected override string RegionTableName
        {
            get { return GKnRegion.TABLE_NAME; }
        }

        protected override string WorkAreaTableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        protected override string BuildingGroupANDTableName
        {
            get { return Core.Features.BuildingGroupsAND.GKnBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingANDTableName
        {
            get { return Core.Features.BuildingsAND.GKnBuilding.TABLE_NAME; }
        }

        protected override string PropertyANDTableName
        {
            get { return Core.Features.PropertiesAND.GKnProperty.TABLE_NAME; }
        }

        protected override string StreetANDTableName
        {
            get { return Core.Features.StreetsAND.GKnStreet.TABLE_NAME; }
        }

        protected override string JunctionANDTableName
        {
            get { return Core.Features.JunctionsAND.GKnJunction.TABLE_NAME; }
        }

        protected override string LandmarkANDTableName
        {
            get { return Core.Features.LandmarksAND.GKnLandmark.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryANDTableName
        {
            get { return Core.Features.LandmarkBoundariesAND.GKnLandmarkBoundary.TABLE_NAME; }
        }

        // noraini - Aug 2021
        protected override string JupemTableName
        {
            get { return GKnJupem.TABLE_NAME; }
        }

        protected override string LotBoundaryTextTableName
        {
            get { return GKnLotBoundaryText.TABLE_NAME; }
        }

        protected override string BuildingGeointTableName
        {
            get { return GKnBuildingGeoint.TABLE_NAME; }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.Regions;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features;

namespace Geomatic.UI.Maps
{
    public class KvMapCreatorByGroup : MapCreatorByGroup
    {
        public KvMapCreatorByGroup(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Kuala Lumpur & Selangor"; }
        }

        protected override string PoiTableName
        {
            get { return GKvPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GKvBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GKvBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GKvProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GKvLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GKvJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GKvGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GKvStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GKvStreet.TABLE_NAME; }
        }

        protected override string StreetTextTableName
        {
            get { return GKvStreetText.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryTableName
        {
            get { return GKvLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GKvSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GKvHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GKvMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GKvMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GKvRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GKvHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GKvMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GKvMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GKvBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GKvBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GKvBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GKvBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GKvResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GKvResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GKvResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GKvShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GKvShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GKvIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GKvIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GKvCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GKvCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GKvOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GKvOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GKvParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GKvParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GKvLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GKvLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GKvHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GKvHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GKvGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GKvGreenaryPolygon.TABLE_NAME; }
        }

        protected override string RegionTableName
        {
            get { return GKvRegion.TABLE_NAME; }
        }

        protected override string WorkAreaTableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        protected override string BuildingGroupANDTableName
        {
            get { return Core.Features.BuildingGroupsAND.GKvBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingANDTableName
        {
            get { return Core.Features.BuildingsAND.GKvBuilding.TABLE_NAME; }
        }

        protected override string PropertyANDTableName
        {
            get { return Core.Features.PropertiesAND.GKvProperty.TABLE_NAME; }
        }

        protected override string StreetANDTableName
        {
            get { return Core.Features.StreetsAND.GKvStreet.TABLE_NAME; }
        }

        protected override string JunctionANDTableName
        {
            get { return Core.Features.JunctionsAND.GKvJunction.TABLE_NAME; }
        }

        protected override string LandmarkANDTableName
        {
            get { return Core.Features.LandmarksAND.GKvLandmark.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryANDTableName
        {
            get { return Core.Features.LandmarkBoundariesAND.GKvLandmarkBoundary.TABLE_NAME; }
        }
    }
}

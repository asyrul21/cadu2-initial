﻿using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Maps
{
    public class MapLayerFactory
    {
        public static ILayer CreateState(IWorkspace caduWorkspace, IWorkspace clbWorkspace, SegmentName segmentName)
        {
            switch (segmentName)
            {
                case SegmentName.AS:
                    return new AsMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.JH:
                    return new JhMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.JP:
                    return new JpMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.KG:
                    return new KgMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.KK:
                    return new KkMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.KN:
                    return new KnMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.KV:
                    return new KvMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.MK:
                    return new MkMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.PG:
                    return new PgMapCreator(caduWorkspace, clbWorkspace).Create();
                case SegmentName.TG:
                    return new TgMapCreator(caduWorkspace, clbWorkspace).Create();
                default:
                    throw new Exception("Unknown Segment.");
            }
        }

        public static ILayer CreateCountry(IWorkspace workspace)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = workspace.OpenFeatureClass(GState.TABLE_NAME);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 0);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(214, 205, 104);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "State";
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            return layer;
        }
        // added by noraini ali - CADU2 AND 
        // display layer feature use for User Group 'AND', need to filter AND feature where AND_STATUS <> 2 
        // and filter feature ADM feature where AND_STATUS is null or 0
        // but no filter for User Group 'Supervisor'
        public static ILayer CreateStateUserGroupAND(IWorkspace caduWorkspace, IWorkspace clbWorkspace, SegmentName segmentName)
        {
            switch (segmentName)
            {
                case SegmentName.AS:
                    return new AsMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.JH:
                    return new JhMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.JP:
                    return new JpMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.KG:
                    return new KgMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.KK:
                    return new KkMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.KN:
                    return new KnMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.KV:
                    return new KvMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.MK:
                    return new MkMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.PG:
                    return new PgMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                case SegmentName.TG:
                    return new TgMapCreatorByGrpAND(caduWorkspace, clbWorkspace).CreateLayerGroupAND();
                default:
                    throw new Exception("Unknown Segment.");
            }
        }
        public static ILayer CreateStateUserGroupSupervisor(IWorkspace caduWorkspace, IWorkspace clbWorkspace, SegmentName segmentName)
        {
            switch (segmentName)
            {
                case SegmentName.AS:
                    return new AsMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.JH:
                    return new JhMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.JP:
                    return new JpMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.KG:
                    return new KgMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.KK:
                    return new KkMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.KN:
                    return new KnMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.KV:
                    return new KvMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.MK:
                    return new MkMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.PG:
                    return new PgMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                case SegmentName.TG:
                    return new TgMapCreatorByGrpSupervisor(caduWorkspace, clbWorkspace).CreateLayerGroupSupervisor();
                default:
                    throw new Exception("Unknown Segment.");
            }
        }
        // end add CADU2 AND
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.Regions;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Jupem;
using Geomatic.Core.Features.LotBoundaryTexts;
using Geomatic.Core.Features.BuildingGeoints;

namespace Geomatic.UI.Maps
{
    public class PgMapCreatorByGrpAND : MapCreatorByGrpAND
    {
        public PgMapCreatorByGrpAND(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Pulau Pinang"; }
        }

        protected override string PoiTableName
        {
            get { return GPgPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GPgBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GPgBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GPgProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GPgLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GPgJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GPgGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GPgStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GPgStreet.TABLE_NAME; }
        }

        protected override string StreetTextTableName
        {
            get { return GPgStreetText.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryTableName
        {
            get { return GPgLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GPgSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GPgHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GPgMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GPgMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GPgRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GPgHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GPgMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GPgMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GPgBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GPgBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GPgBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GPgBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GPgResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GPgResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GPgResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GPgShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GPgShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GPgIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GPgIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GPgCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GPgCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GPgOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GPgOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GPgParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GPgParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GPgLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GPgLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GPgHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GPgHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GPgGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GPgGreenaryPolygon.TABLE_NAME; }
        }

        protected override string RegionTableName
        {
            get { return GPgRegion.TABLE_NAME; }
        }

        protected override string WorkAreaTableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        protected override string BuildingGroupANDTableName
        {
            get { return Core.Features.BuildingGroupsAND.GPgBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingANDTableName
        {
            get { return Core.Features.BuildingsAND.GPgBuilding.TABLE_NAME; }
        }

        protected override string PropertyANDTableName
        {
            get { return Core.Features.PropertiesAND.GPgProperty.TABLE_NAME; }
        }

        protected override string StreetANDTableName
        {
            get { return Core.Features.StreetsAND.GPgStreet.TABLE_NAME; }
        }

        protected override string JunctionANDTableName
        {
            get { return Core.Features.JunctionsAND.GPgJunction.TABLE_NAME; }
        }

        protected override string LandmarkANDTableName
        {
            get { return Core.Features.LandmarksAND.GPgLandmark.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryANDTableName
        {
            get { return Core.Features.LandmarkBoundariesAND.GPgLandmarkBoundary.TABLE_NAME; }
        }

        // noraini - Aug 2021
        protected override string JupemTableName
        {
            get { return GPgJupem.TABLE_NAME; }
        }

        protected override string LotBoundaryTextTableName
        {
            get { return GPgLotBoundaryText.TABLE_NAME; }
        }

        protected override string BuildingGeointTableName
        {
            get { return GPgBuildingGeoint.TABLE_NAME; }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features.Jupem;
using Geomatic.Core.Features.LotBoundaryTexts;
using Geomatic.Core.Features.BuildingGeoints;

namespace Geomatic.UI.Maps
{
    public class JhMapCreator : MapCreator
    {
        public JhMapCreator(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Johor"; }
        }

        protected override string PoiTableName
        {
            get { return GJhPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GJhBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GJhBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GJhProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GJhLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GJhJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GJhGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GJhStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GJhStreet.TABLE_NAME; }
        }

        // added by asyrul
        protected override string StreetTextTableName
        {
            get { return GJhStreetText.TABLE_NAME; }
        }
        // added end

        protected override string LandmarkBoundaryTableName
        {
            get { return GJhLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GJhSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GJhHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GJhMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GJhMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GJhRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GJhHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GJhMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GJhMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GJhBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GJhBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GJhBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GJhBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GJhResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GJhResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GJhResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GJhShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GJhShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GJhIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GJhIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GJhCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GJhCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GJhOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GJhOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GJhParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GJhParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GJhLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GJhLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GJhHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GJhHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GJhGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GJhGreenaryPolygon.TABLE_NAME; }
        }

        //added by Noraini Ali on 14 Jan 2019
        protected override string RegionTableName
        {
            //get { return "TEST"; }
            get { return GJhRegion.TABLE_NAME; }
        }
        //Noraini Ali - OKT 2020
        protected override string WorkAreaTableName
        {
            get { return Geomatic.Core.Features.GWorkArea.TABLE_NAME; }
        }

        //Noraini Ali - Aug 2021
        protected override string JupemTableName
        {
            get { return GJhJupem.TABLE_NAME; }
        }

        protected override string LotBoundaryTextTableName
        {
            get { return GJhLotBoundaryText.TABLE_NAME; }
        }

        protected override string BuildingGeointTableName
        {
            get { return GJhBuildingGeoint.TABLE_NAME; }
        }
    }
}

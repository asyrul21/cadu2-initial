﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features.Jupem;
using Geomatic.Core.Features.LotBoundaryTexts;
using Geomatic.Core.Features.BuildingGeoints;

namespace Geomatic.UI.Maps
{
    public class JpMapCreator : MapCreator
    {
        public JpMapCreator(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Perak"; }
        }

        protected override string PoiTableName
        {
            get { return GJpPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GJpBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GJpBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GJpProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GJpLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GJpJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GJpGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GJpStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GJpStreet.TABLE_NAME; }
        }

        // added by asyrul
        protected override string StreetTextTableName
        {
            get { return GJpStreetText.TABLE_NAME; }
        }
        // added end

        protected override string LandmarkBoundaryTableName
        {
            get { return GJpLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GJpSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GJpHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GJpMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GJpMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GJpRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GJpHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GJpMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GJpMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GJpBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GJpBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GJpBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GJpBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GJpResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GJpResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GJpResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GJpShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GJpShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GJpIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GJpIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GJpCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GJpCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GJpOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GJpOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GJpParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GJpParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GJpLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GJpLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GJpHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GJpHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GJpGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GJpGreenaryPolygon.TABLE_NAME; }
        }

        //added by Noraini Ali on 14 Jan 2019
        protected override string RegionTableName
        {
            //get { return "TEST"; }
            get { return GJpRegion.TABLE_NAME; }
        }

        //Noraini Ali - OKT 2020
        protected override string WorkAreaTableName
        {
            get { return Geomatic.Core.Features.GWorkArea.TABLE_NAME; }
        }

        // noraini - Aug 2021
        protected override string JupemTableName
        {
            get { return GJpJupem.TABLE_NAME; }
        }

        protected override string LotBoundaryTextTableName
        {
            get { return GJpLotBoundaryText.TABLE_NAME; }
        }

        protected override string BuildingGeointTableName
        {
            get { return GJpBuildingGeoint.TABLE_NAME; }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features.Jupem;
using Geomatic.Core.Features.LotBoundaryTexts;
using Geomatic.Core.Features.BuildingGeoints;

namespace Geomatic.UI.Maps
{
    public class KgMapCreator : MapCreator
    {
        public KgMapCreator(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Sarawak"; }
        }

        protected override string PoiTableName
        {
            get { return GKgPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GKgBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GKgBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GKgProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GKgLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GKgJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GKgGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GKgStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GKgStreet.TABLE_NAME; }
        }

        // added by asyrul
        protected override string StreetTextTableName
        {
            get { return GKgStreetText.TABLE_NAME; }
        }
        // added end

        protected override string LandmarkBoundaryTableName
        {
            get { return GKgLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GKgSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GKgHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GKgMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GKgMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GKgRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GKgHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GKgMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GKgMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GKgBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GKgBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GKgBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GKgBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GKgResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GKgResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GKgResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GKgShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GKgShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GKgIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GKgIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GKgCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GKgCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GKgOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GKgOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GKgParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GKgParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GKgLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GKgLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GKgHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GKgHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GKgGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GKgGreenaryPolygon.TABLE_NAME; }
        }

        //added by Noraini Ali on 14 Jan 2019
        protected override string RegionTableName
        {
            //get { return "TEST"; }
            get { return GKgRegion.TABLE_NAME; }
        }

        //Noraini Ali - OKT 2020
        protected override string WorkAreaTableName
        {
            get { return Geomatic.Core.Features.GWorkArea.TABLE_NAME; }
        }

        // noraini - Aug 2021
        protected override string JupemTableName
        {
            get { return GKgJupem.TABLE_NAME; }
        }

        protected override string LotBoundaryTextTableName
        {
            get { return GKgLotBoundaryText.TABLE_NAME; }
        }

        protected override string BuildingGeointTableName
        {
            get { return GKgBuildingGeoint.TABLE_NAME; }
        }
    }
}

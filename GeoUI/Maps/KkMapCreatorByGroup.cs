﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.Regions;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features;

namespace Geomatic.UI.Maps
{
    public class KkMapCreatorByGroup : MapCreatorByGroup
    {
        public KkMapCreatorByGroup(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Sabah"; }
        }

        protected override string PoiTableName
        {
            get { return GKkPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GKkBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GKkBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GKkProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GKkLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GKkJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GKkGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GKkStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GKkStreet.TABLE_NAME; }
        }

        protected override string StreetTextTableName
        {
            get { return GKkStreetText.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryTableName
        {
            get { return GKkLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GKkSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GKkHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GKkMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GKkMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GKkRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GKkHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GKkMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GKkMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GKkBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GKkBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GKkBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GKkBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GKkResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GKkResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GKkResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GKkShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GKkShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GKkIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GKkIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GKkCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GKkCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GKkOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GKkOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GKkParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GKkParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GKkLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GKkLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GKkHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GKkHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GKkGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GKkGreenaryPolygon.TABLE_NAME; }
        }

        protected override string RegionTableName
        {
            get { return GKkRegion.TABLE_NAME; }
        }

        protected override string WorkAreaTableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        protected override string BuildingGroupANDTableName
        {
            get { return Core.Features.BuildingGroupsAND.GKkBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingANDTableName
        {
            get { return Core.Features.BuildingsAND.GKkBuilding.TABLE_NAME; }
        }

        protected override string PropertyANDTableName
        {
            get { return Core.Features.PropertiesAND.GKkProperty.TABLE_NAME; }
        }

        protected override string StreetANDTableName
        {
            get { return Core.Features.StreetsAND.GKkStreet.TABLE_NAME; }
        }

        protected override string JunctionANDTableName
        {
            get { return Core.Features.JunctionsAND.GKkJunction.TABLE_NAME; }
        }

        protected override string LandmarkANDTableName
        {
            get { return Core.Features.LandmarksAND.GKkLandmark.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryANDTableName
        {
            get { return Core.Features.LandmarkBoundariesAND.GKkLandmarkBoundary.TABLE_NAME; }
        }
    }
}

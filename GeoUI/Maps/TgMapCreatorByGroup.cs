﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.BuildingLines;
using Geomatic.Core.Features.BuildingPolygons;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BungalowLines;
using Geomatic.Core.Features.BungalowPolygons;
using Geomatic.Core.Features.CommercialLines;
using Geomatic.Core.Features.CommercialPolygons;
using Geomatic.Core.Features.GpsLines;
using Geomatic.Core.Features.GreenaryLines;
using Geomatic.Core.Features.GreenaryPolygons;
using Geomatic.Core.Features.HighwayLines;
using Geomatic.Core.Features.HighwayOutlines;
using Geomatic.Core.Features.HydrographicLines;
using Geomatic.Core.Features.HydrographicPolygons;
using Geomatic.Core.Features.IndustrialLines;
using Geomatic.Core.Features.IndustrialPolygons;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.LandmarkBoundaries;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.LotLines;
using Geomatic.Core.Features.LotPolygons;
using Geomatic.Core.Features.MajorRoadLines;
using Geomatic.Core.Features.MajorRoadOutlines;
using Geomatic.Core.Features.MinorRoadLines;
using Geomatic.Core.Features.MinorRoadOutlines;
using Geomatic.Core.Features.OtherLines;
using Geomatic.Core.Features.OtherPolygons;
using Geomatic.Core.Features.ParkingLines;
using Geomatic.Core.Features.ParkingPolygons;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Railways;
using Geomatic.Core.Features.Regions;
using Geomatic.Core.Features.ResidentialLines;
using Geomatic.Core.Features.ResidentialPolygon2s;
using Geomatic.Core.Features.ResidentialPolygons;
using Geomatic.Core.Features.SectionBoundaries;
using Geomatic.Core.Features.ShopLines;
using Geomatic.Core.Features.ShopPolygons;
using Geomatic.Core.Features.StreetRestrictions;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.StreetTexts;
using Geomatic.Core.Features;

namespace Geomatic.UI.Maps
{
    public class TgMapCreatorByGroup : MapCreatorByGroup
    {
        public TgMapCreatorByGroup(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
            : base(caduWorkspace, clbWorkspace)
        {
        }

        protected override string Name
        {
            get { return "Terengganu & Kelantan"; }
        }

        protected override string PoiTableName
        {
            get { return GTgPoi.TABLE_NAME; }
        }

        protected override string BuildingGroupTableName
        {
            get { return GTgBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingTableName
        {
            get { return GTgBuilding.TABLE_NAME; }
        }

        protected override string PropertyTableName
        {
            get { return GTgProperty.TABLE_NAME; }
        }

        protected override string LandmarkTableName
        {
            get { return GTgLandmark.TABLE_NAME; }
        }

        protected override string JunctionTableName
        {
            get { return GTgJunction.TABLE_NAME; }
        }

        protected override string GpsLineTableName
        {
            get { return GTgGpsLine.TABLE_NAME; }
        }

        protected override string RestrictionTableName
        {
            get { return GTgStreetRestriction.TABLE_NAME; }
        }

        protected override string StreetTableName
        {
            get { return GTgStreet.TABLE_NAME; }
        }

        protected override string StreetTextTableName
        {
            get { return GTgStreetText.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryTableName
        {
            get { return GTgLandmarkBoundary.TABLE_NAME; }
        }

        protected override string SectionBoundaryTableName
        {
            get { return GTgSectionBoundary.TABLE_NAME; }
        }

        protected override string HighwayLineTableName
        {
            get { return GTgHighwayLine.TABLE_NAME; }
        }

        protected override string MajorRoadLineTableName
        {
            get { return GTgMajorRoadLine.TABLE_NAME; }
        }

        protected override string MinorRoadLineTableName
        {
            get { return GTgMinorRoadLine.TABLE_NAME; }
        }

        protected override string RailwayTableName
        {
            get { return GTgRailway.TABLE_NAME; }
        }

        protected override string HighwayOutlineTableName
        {
            get { return GTgHighwayOutline.TABLE_NAME; }
        }

        protected override string MajorRoadOutlineTableName
        {
            get { return GTgMajorRoadOutline.TABLE_NAME; }
        }

        protected override string MinorRoadOutlineTableName
        {
            get { return GTgMinorRoadOutline.TABLE_NAME; }
        }

        protected override string BuildingLineTableName
        {
            get { return GTgBuildingLine.TABLE_NAME; }
        }

        protected override string BuildingPolygonTableName
        {
            get { return GTgBuildingPolygon.TABLE_NAME; }
        }

        protected override string BungalowLineTableName
        {
            get { return GTgBungalowLine.TABLE_NAME; }
        }

        protected override string BungalowPolygonTableName
        {
            get { return GTgBungalowPolygon.TABLE_NAME; }
        }

        protected override string ResidentialLineTableName
        {
            get { return GTgResidentialLine.TABLE_NAME; }
        }

        protected override string ResidentialPolygonTableName
        {
            get { return GTgResidentialPolygon.TABLE_NAME; }
        }

        protected override string ResidentialPolygon2TableName
        {
            get { return GTgResidentialPolygon2.TABLE_NAME; }
        }

        protected override string ShopLineTableName
        {
            get { return GTgShopLine.TABLE_NAME; }
        }

        protected override string ShopPolygonTableName
        {
            get { return GTgShopPolygon.TABLE_NAME; }
        }

        protected override string IndustrialLineTableName
        {
            get { return GTgIndustrialLine.TABLE_NAME; }
        }

        protected override string IndustrialPolygonTableName
        {
            get { return GTgIndustrialPolygon.TABLE_NAME; }
        }

        protected override string CommercialLineTableName
        {
            get { return GTgCommercialLine.TABLE_NAME; }
        }

        protected override string CommercialPolygonTableName
        {
            get { return GTgCommercialPolygon.TABLE_NAME; }
        }

        protected override string OtherLineTableName
        {
            get { return GTgOtherLine.TABLE_NAME; }
        }

        protected override string OtherPolygonTableName
        {
            get { return GTgOtherPolygon.TABLE_NAME; }
        }

        protected override string ParkingLineTableName
        {
            get { return GTgParkingLine.TABLE_NAME; }
        }

        protected override string ParkingPolygonTableName
        {
            get { return GTgParkingPolygon.TABLE_NAME; }
        }

        protected override string LotLineTableName
        {
            get { return GTgLotLine.TABLE_NAME; }
        }

        protected override string LotPolygonTableName
        {
            get { return GTgLotPolygon.TABLE_NAME; }
        }

        protected override string HydrographicLineTableName
        {
            get { return GTgHydrographicLine.TABLE_NAME; }
        }

        protected override string HydrographicPolygonTableName
        {
            get { return GTgHydrographicPolygon.TABLE_NAME; }
        }

        protected override string GreenaryLineTableName
        {
            get { return GTgGreenaryLine.TABLE_NAME; }
        }

        protected override string GreenaryPolygonTableName
        {
            get { return GTgGreenaryPolygon.TABLE_NAME; }
        }

        protected override string RegionTableName
        {
            get { return GTgRegion.TABLE_NAME; }
        }

        protected override string WorkAreaTableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        protected override string BuildingGroupANDTableName
        {
            get { return Core.Features.BuildingGroupsAND.GTgBuildingGroup.TABLE_NAME; }
        }

        protected override string BuildingANDTableName
        {
            get { return Core.Features.BuildingsAND.GTgBuilding.TABLE_NAME; }
        }

        protected override string PropertyANDTableName
        {
            get { return Core.Features.PropertiesAND.GTgProperty.TABLE_NAME; }
        }

        protected override string StreetANDTableName
        {
            get { return Core.Features.StreetsAND.GTgStreet.TABLE_NAME; }
        }

        protected override string JunctionANDTableName
        {
            get { return Core.Features.JunctionsAND.GTgJunction.TABLE_NAME; }
        }

        protected override string LandmarkANDTableName
        {
            get { return Core.Features.LandmarksAND.GTgLandmark.TABLE_NAME; }
        }

        protected override string LandmarkBoundaryANDTableName
        {
            get { return Core.Features.LandmarkBoundariesAND.GTgLandmarkBoundary.TABLE_NAME; }
        }
    }
}

﻿using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Maps
{
    public abstract class MapCreatorByGroup
    {
        protected IGroupLayer _layer;
        protected IWorkspace _caduWorkspace;
        protected IWorkspace _clbWorkspace;

        protected abstract string Name { get; }

        protected abstract string PoiTableName { get; }
        protected abstract string BuildingGroupTableName { get; }
        protected abstract string BuildingTableName { get; }
        protected abstract string PropertyTableName { get; }
        protected abstract string LandmarkTableName { get; }
        protected abstract string JunctionTableName { get; }
        protected abstract string GpsLineTableName { get; }
        protected abstract string RestrictionTableName { get; }
        protected abstract string StreetTableName { get; }
        protected abstract string LandmarkBoundaryTableName { get; }
        protected abstract string SectionBoundaryTableName { get; }

        protected abstract string RegionTableName { get; }
        protected abstract string StreetTextTableName { get; }

        protected abstract string HighwayLineTableName { get; }
        protected abstract string MajorRoadLineTableName { get; }
        protected abstract string MinorRoadLineTableName { get; }
        protected abstract string RailwayTableName { get; }

        protected abstract string HighwayOutlineTableName { get; }
        protected abstract string MajorRoadOutlineTableName { get; }
        protected abstract string MinorRoadOutlineTableName { get; }

        protected abstract string BuildingLineTableName { get; }
        protected abstract string BuildingPolygonTableName { get; }

        protected abstract string BungalowLineTableName { get; }
        protected abstract string BungalowPolygonTableName { get; }

        protected abstract string ResidentialLineTableName { get; }
        protected abstract string ResidentialPolygonTableName { get; }
        protected abstract string ResidentialPolygon2TableName { get; }

        protected abstract string ShopLineTableName { get; }
        protected abstract string ShopPolygonTableName { get; }

        protected abstract string IndustrialLineTableName { get; }
        protected abstract string IndustrialPolygonTableName { get; }

        protected abstract string CommercialLineTableName { get; }
        protected abstract string CommercialPolygonTableName { get; }

        protected abstract string OtherLineTableName { get; }
        protected abstract string OtherPolygonTableName { get; }

        protected abstract string ParkingLineTableName { get; }
        protected abstract string ParkingPolygonTableName { get; }

        protected abstract string LotLineTableName { get; }
        protected abstract string LotPolygonTableName { get; }

        protected abstract string HydrographicLineTableName { get; }
        protected abstract string HydrographicPolygonTableName { get; }

        protected abstract string GreenaryLineTableName { get; }
        protected abstract string GreenaryPolygonTableName { get; }

        protected abstract string WorkAreaTableName { get; }
        protected abstract string StreetANDTableName { get; }
        protected abstract string PropertyANDTableName { get; }
        protected abstract string JunctionANDTableName { get; }
        protected abstract string BuildingANDTableName { get; }
        protected abstract string LandmarkANDTableName { get; }
        protected abstract string LandmarkBoundaryANDTableName { get; }
        protected abstract string BuildingGroupANDTableName { get; }
        
        public ILayer Layer
        {
            get { return _layer; }
        }

        public MapCreatorByGroup(IWorkspace caduWorkspace, IWorkspace clbWorkspace)
        {
            _layer = new GroupLayerClass();
            _layer.Name = Name;
            _layer.Expanded = false;

            _caduWorkspace = caduWorkspace;
            _clbWorkspace = clbWorkspace;
        }

        public virtual ILayer CreateLayerGroupSupervisor()
        {
            bool ANDStatus_filter = false;
            AddPoiLayer();
            AddBuildingGroupLayer(ANDStatus_filter);
            AddBuildingGroupANDLayer(ANDStatus_filter);
            AddBuildingLayer(ANDStatus_filter);
            AddBuildingANDLayer(ANDStatus_filter);
            AddPropertyLayer(ANDStatus_filter);
            AddPropertyANDLayer(ANDStatus_filter);
            AddLandmarkLayer(ANDStatus_filter);
            AddLandmarkANDLayer(ANDStatus_filter);
            AddJunctionLayer(ANDStatus_filter);
            AddJunctionANDLayer(ANDStatus_filter);
            AddGpsLineLayer();
            AddStreetLayers(ANDStatus_filter);
            AddStreetANDLayer(ANDStatus_filter);
            AddRoadLineLayers();
            AddRoadOutlineLayers();
            AddBuildingLineLayer();
            AddBungalowLineLayer();
            AddResidentialLineLayer();
            AddShopLineLayer();
            AddIndustrialLineLayer();
            AddCommercialLineLayer();
            AddOtherLineLayer();
            AddParkingLineLayer();
            AddLotLineLayer();
            AddHydrographicLineLayer();
            AddGreenaryLineLayer();
            AddBuildingPolygonLayer();
            AddBungalowPolygonLayer();
            AddResidentialPolygonLayer();
            AddResidentialPolygon2Layer();
            AddShopPolygonLayer();
            AddIndustrialPolygonLayer();
            AddCommercialPolygonLayer();
            AddOtherPolygonLayer();
            AddParkingPolygonLayer();
            AddLotPolygonLayer();
            AddWorkAreaLayer();
            AddLandmarkBoundaryLayer();
            AddLandmarkBoundaryANDLayer(ANDStatus_filter);
            AddSectionBoundaryLayer();
            AddRegionBoundaryLayer();
            AddHydrographicPolygonLayer();
            AddGreenaryPolygonLayer();
            
            return _layer;
        }

        public virtual ILayer CreateLayerGroupAND()
        {
            bool ANDStatus_filter = true;

            // noraini ali - Jul 2020 - Rearrange layer by group for User AND
            // Group AND layer
            AddBuildingGroupANDLayer(ANDStatus_filter);
            AddBuildingANDLayer(ANDStatus_filter);
            AddJunctionANDLayer(ANDStatus_filter);
            AddLandmarkANDLayer(ANDStatus_filter);
            AddPropertyANDLayer(ANDStatus_filter);
            AddStreetANDLayer(ANDStatus_filter);

            // Group ADM layer
            AddBuildingGroupLayer(ANDStatus_filter);           
            AddBuildingLayer(ANDStatus_filter);
            AddJunctionLayer(ANDStatus_filter);
            AddLandmarkLayer(ANDStatus_filter);
            AddPropertyLayer(ANDStatus_filter);
            AddStreetLayers(ANDStatus_filter);

            AddGpsLineLayer(); // new layer (not exist in CADU1)

            // Group landmark Boundary ADM & AND, Work Area Layer
            AddLandmarkBoundaryANDLayer(ANDStatus_filter);
            AddLandmarkBoundaryLayer();

            AddWorkAreaLayer();

            // Group Landbase Area Line
            AddGroupAreaLine();

            // Group landbase Lot Line
            AddGroupLandbaseLotLine();

            // Group Road Centerline
            AddRoadLineLayers();

            // Group Landbase Road Outline
            AddRoadOutlineLayers();

            // Group Landbase Area Polygon
            AddGroupLandbaseAreaPolygon();

            // Group Landbase Lot Polygon
            AddGroupLandbaseLotPolygon();

            AddHydrographicPolygonLayer();

            AddSectionBoundaryLayer();   // new layer (not exist in CADU1)

            // Group Region
            AddRegionBoundaryLayer();

            return _layer;
        }

        public virtual void selectAllLayers()
        {
        }

        private void AddPoiLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(PoiTableName);

            // set symbolMarker style for Poi Belong to Property
            ISimpleMarkerSymbol simpleMarkerSymbol1 = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol1.Color = ColorUtils.Get(255, 128, 128);
            simpleMarkerSymbol1.Size = 6;
            simpleMarkerSymbol1.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol1.Outline = true;
            simpleMarkerSymbol1.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol1.OutlineSize = 1;

            // set symbolMarker style for Poi belong to Building
            ISimpleMarkerSymbol simpleMarkerSymbol2 = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol2.Color = ColorUtils.Get(0, 0, 255);
            simpleMarkerSymbol2.Size = 6;
            simpleMarkerSymbol2.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol2.Outline = true;
            simpleMarkerSymbol2.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol2.OutlineSize = 1;

            // set symbolMarker style for Poi belong to Landmark
            ISimpleMarkerSymbol simpleMarkerSymbol3 = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol3.Color = ColorUtils.Get(23, 246, 68);
            simpleMarkerSymbol3.Size = 6;
            simpleMarkerSymbol3.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol3.Outline = true;
            simpleMarkerSymbol3.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol3.OutlineSize = 1;

            // set default symbolMarker for Poi belong Property
            IUniqueValueRenderer uniqueValueRenderer = new UniqueValueRendererClass();
            uniqueValueRenderer.FieldCount = 1;
            string field = GPoi.REF_TYPE;
            uniqueValueRenderer.set_Field(0, field);
            uniqueValueRenderer.DefaultSymbol = (ISymbol)simpleMarkerSymbol1;
            uniqueValueRenderer.DefaultLabel = "Property";
            uniqueValueRenderer.UseDefaultSymbol = true;

            // set symbolMarker for Poi belong Building
            uniqueValueRenderer.AddValue("3", "", (ISymbol)simpleMarkerSymbol2);
            uniqueValueRenderer.set_Heading("3", "");
            uniqueValueRenderer.set_Label("3", "Building");

            // set symbolMarker for Poi belong Landmark
            uniqueValueRenderer.AddValue("4", "", (ISymbol)simpleMarkerSymbol3);
            uniqueValueRenderer.set_Heading("4", "");
            uniqueValueRenderer.set_Label("4", "Landmark");
            // added end 

            string expression =
            "Function FindLabel ( [" + GPoi.NAME + "], [" + GPoi.NAME2 + "] ) \n" +
                "FindLabel = PCase( [" + GPoi.NAME + "]  & \" \" & [" + GPoi.NAME2 + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            annotateLayerProperties.WhereClause = GPoi.DISPLAY_TEXT + " = 1";
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Poi";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)uniqueValueRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddBuildingGroupLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = BuildingGroupTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", BuildingGroupTableName, GBuildingGroup.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", BuildingGroupTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(255, 6, 239);
            simpleMarkerSymbol.Size = 5;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(253, 249, 255);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GBuildingGroup.NAME + "] ) \n" +
                "FindLabel = PCase( [" + GBuildingGroup.NAME + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;
            labelEngineLayerProperties.Symbol.Font.Bold = true;
            labelEngineLayerProperties.Symbol.Size = 9;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Building Group";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddBuildingLayer(bool filter)
        {  
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = BuildingTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", BuildingTableName, GBuilding.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", BuildingTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(104, 104, 104);
            simpleMarkerSymbol.Size = 4;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GBuilding.NAME + "], [" + GBuilding.NAME2 + "] ) \n" +
                "FindLabel = PCase( [" + GBuilding.NAME + "]  & \" \" & [" + GBuilding.NAME2 + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Building";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddPropertyLayer(bool filter)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();

            ListStringBuilder tables = new ListStringBuilder();
            tables.Add(PropertyTableName);
            tables.Add(GPropertyType.TABLE_NAME);

            ListStringBuilder subFields = new ListStringBuilder();
            subFields.Add("{0}.OBJECTID", PropertyTableName);
            subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.ABBREVIATION);
            subFields.Add("{0}.{1}", PropertyTableName, GProperty.LOT);
            subFields.Add("{0}.{1}", PropertyTableName, GProperty.HOUSE);
            subFields.Add("{0}.SHAPE", PropertyTableName);

            ListStringBuilder whereClauses = new ListStringBuilder();
            whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyTableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
            if (filter)
            { whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", PropertyTableName, GProperty.AND_STATUS); }

            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = tables.Join(",");
            queryDef.SubFields = subFields.Join(",");
            queryDef.WhereClause = whereClauses.Join(" AND ");

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", PropertyTableName);

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(255, 170, 0);
            simpleMarkerSymbol.Size = 4;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();

            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.PointPlacementMethod = esriOverposterPointPlacementMethod.esriOnTopPoint;
            basicOverposterLayerProperties.PointPlacementOnTop = true;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            string expression =
            "Function FindLabel ( [" + PropertyTableName + "." + GProperty.LOT + "], [" + PropertyTableName + "." + GProperty.HOUSE + "], [" + GPropertyType.TABLE_NAME + "." + GPropertyType.ABBREVIATION + "]) \n" +
                "FindLabel = BuildLabel([" + PropertyTableName + "." + GProperty.LOT + "], [" + PropertyTableName + "." + GProperty.HOUSE + "], [" + GPropertyType.TABLE_NAME + "." + GPropertyType.ABBREVIATION + "]) \n" +
            "End Function \n" +

            "Function BuildLabel(strLot, strHouse, propertyType) \n" +
                "Dim strOutput \n" +
                "If Not IsNull(strLot) Then \n" +
                    "strOutput = strLot \n" +
                "End If \n" +
                "If Not IsNull(strHouse) Then \n" +
                    "strOutput = strOutput & \" \" & strHouse \n" +
                "End If \n" +
                "strOutput = strOutput & vbNewLine & vbNewLine & propertyType \n" +
                "BuildLabel = strOutput \n" +
            "End Function";

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Property";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddLandmarkLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = LandmarkTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", LandmarkTableName, GLandmark.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", LandmarkTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(170, 255, 0);
            simpleMarkerSymbol.Size = 4;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GLandmark.NAME + "], [" + GLandmark.NAME2 + "] ) \n" +
                "FindLabel = PCase( [" + GLandmark.NAME + "]  & \" \" & [" + GLandmark.NAME2 + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Landmark";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddJunctionLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = JunctionTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", JunctionTableName, GJunction.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", JunctionTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(255, 0, 0);
            simpleMarkerSymbol.Size = 4;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            layer.Name = "Junction";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddGpsLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(GpsLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(0, 38, 115);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Gps Line";
            layer.MinimumScale = 80000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private ILayer CreateRestrictionLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(RestrictionTableName);

            stdole.IFontDisp fontDisp = ((stdole.IFontDisp)(new stdole.StdFont()));
            fontDisp.Name = "ESRI Arrowhead";
            fontDisp.Size = 8;

            ICharacterMarkerSymbol characterMarkerSymbol = new CharacterMarkerSymbolClass();
            characterMarkerSymbol.Angle = 0;
            characterMarkerSymbol.CharacterIndex = 36;
            characterMarkerSymbol.Color = ColorUtils.Get(255, 0, 0);
            characterMarkerSymbol.Font = fontDisp;
            characterMarkerSymbol.Size = 8;
            characterMarkerSymbol.XOffset = 0;
            characterMarkerSymbol.YOffset = 0;

            IMarkerLineSymbol markerLineSymbol = new MarkerLineSymbolClass();
            markerLineSymbol.MarkerSymbol = characterMarkerSymbol;

            ILineProperties lineProperties = (ILineProperties)markerLineSymbol;
            lineProperties.Offset = 0;

            ITemplate template = lineProperties.Template;
            template.ClearPatternElements();
            template.Interval = 1;
            template.AddPatternElement(1, 70);

            lineProperties.Template = template;

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 0);
            simpleLineSymbol.Style = esriSimpleLineStyle.esriSLSSolid;
            simpleLineSymbol.Width = 1;

            IMultiLayerLineSymbol multiLayerLineSymbol = new MultiLayerLineSymbolClass();
            multiLayerLineSymbol.AddLayer(simpleLineSymbol);
            multiLayerLineSymbol.AddLayer(markerLineSymbol);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)multiLayerLineSymbol;

            layer.Name = "Restriction";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateDirectionLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(StreetTableName);

            stdole.IFontDisp fontDisp = ((stdole.IFontDisp)(new stdole.StdFont()));
            fontDisp.Name = "ESRI Arrowhead";
            fontDisp.Size = 8;

            ICharacterMarkerSymbol characterMarkerSymbol = new CharacterMarkerSymbolClass();
            characterMarkerSymbol.Angle = 0;
            characterMarkerSymbol.CharacterIndex = 36;
            characterMarkerSymbol.Color = ColorUtils.Get(0, 0, 255);
            characterMarkerSymbol.Font = fontDisp;
            characterMarkerSymbol.Size = 8;
            characterMarkerSymbol.XOffset = 0;
            characterMarkerSymbol.YOffset = 0;

            IMarkerLineSymbol markerLineSymbol = new MarkerLineSymbolClass();
            markerLineSymbol.MarkerSymbol = characterMarkerSymbol;

            ILineProperties lineProperties = (ILineProperties)markerLineSymbol;
            lineProperties.Offset = 0;

            ITemplate template = lineProperties.Template;
            template.ClearPatternElements();
            template.Interval = 1;
            template.AddPatternElement(1, 50);

            lineProperties.Template = template;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)markerLineSymbol;

            layer.Name = "Direction";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            IFeatureLayerDefinition featureLayerDefinition = (IFeatureLayerDefinition)layer;
            featureLayerDefinition.DefinitionExpression = GStreet.DIRECTION + " = 2";

            return layer;
        }

        private ILayer CreateStreetNavigationStatusLayer(double minScale, double maxScale, bool displayAnno, double width, bool filter)
        {
            ListStringBuilder tables = new ListStringBuilder();
            tables.Add(StreetTableName);
            tables.Add(GStreetType.TABLE_NAME);

            ListStringBuilder subFields = new ListStringBuilder();
            subFields.Add("{0}.OBJECTID", StreetTableName);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NETWORK_CLASS);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
            subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.DIRECTION);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAVI_STATUS);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.AND_STATUS);
            subFields.Add("{0}.SHAPE", StreetTableName);

            ListStringBuilder whereClauses = new ListStringBuilder();
            whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
            if (filter)
            {
                { whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", StreetTableName, GStreet.AND_STATUS); }
            }
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = tables.Join(",");
            queryDef.SubFields = subFields.Join(",");
            queryDef.WhereClause = whereClauses.Join(" AND ");

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", StreetTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            ISymbolLevels symbolLevels = (ISymbolLevels)layer;
            symbolLevels.UseSymbolLevels = false;

            //1
            ISimpleLineSymbol simpleLineSymbol1 = new SimpleLineSymbolClass();
            simpleLineSymbol1.Color = ColorUtils.Get(0, 255, 0);
            simpleLineSymbol1.Width = width;

            //2
            ISimpleLineSymbol simpleLineSymbol2 = new SimpleLineSymbolClass();
            simpleLineSymbol2.Color = ColorUtils.Get(255, 0, 0);
            simpleLineSymbol2.Width = width;

            //3
            ISimpleLineSymbol simpleLineSymbol3 = new SimpleLineSymbolClass();
            simpleLineSymbol3.Color = ColorUtils.Get(102, 153, 255);
            simpleLineSymbol3.Width = width;

            IUniqueValueRenderer uniqueValueRenderer = new UniqueValueRendererClass();
            uniqueValueRenderer.FieldCount = 1;
            string field = GStreet.NAVI_STATUS;
            uniqueValueRenderer.set_Field(0, field);

            uniqueValueRenderer.AddValue("1", "", (ISymbol)simpleLineSymbol1);
            uniqueValueRenderer.set_Heading("1", "");
            uniqueValueRenderer.set_Label("1", "Ready");

            uniqueValueRenderer.AddValue("0", "", (ISymbol)simpleLineSymbol2);
            uniqueValueRenderer.set_Heading("0", "");
            uniqueValueRenderer.set_Label("0", "Not Ready");

            uniqueValueRenderer.AddValue("<Null>", "", (ISymbol)simpleLineSymbol3);
            uniqueValueRenderer.set_Heading("<Null>", "");
            uniqueValueRenderer.set_Label("<Null>", "Unknown");

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();

            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;

            ILineLabelPosition lineLabelPosition = new LineLabelPositionClass();
            lineLabelPosition.ProduceCurvedLabels = true;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.LineLabelPosition = lineLabelPosition;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();

            string expression =
            "Function FindLabel ( [" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "], [" + StreetTableName + "." + GStreet.NAME + "] ) \n" +
                "FindLabel = PCase( trim([" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "]) & \" \" & [" + StreetTableName + "." + GStreet.NAME + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = displayAnno;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)uniqueValueRenderer;
            layer.Cached = true;

            return layer;
        }

        private void AddStreetLayers(bool filter)
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Street";
            groupLayer.Expanded = false;

            ILayer restrictionLayer = CreateRestrictionLayer();
            groupLayer.Add(restrictionLayer);
            
            ILayer directionLayer = CreateDirectionLayer();
            groupLayer.Add(directionLayer);
            
            ILayer navigationStatusLayer = CreateStreetNavigationStatusLayer(80000, 0, false, 2.0, filter);
            navigationStatusLayer.Name = "Navigation Status";
            groupLayer.Add(navigationStatusLayer);

            ILayer streetLayer = CreateStreetLayer(80000, 0, true, 2.0, filter);
            streetLayer.Name = "Network Class";
            groupLayer.Add(streetLayer);

            ILayer streetTextLayer = CreateStreetTextLayer(80000, 0, true, 2.0);
            streetTextLayer.Name = "Street Text NEPS";
            groupLayer.Add(streetTextLayer);

            _layer.Add(groupLayer);
        }

        private ILayer CreateStreetLayer(double minScale, double maxScale, bool displayAnno, double width, bool filter)
        {
            ListStringBuilder tables = new ListStringBuilder();
            tables.Add(StreetTableName);
            tables.Add(GStreetType.TABLE_NAME);

            ListStringBuilder subFields = new ListStringBuilder();
            subFields.Add("{0}.OBJECTID", StreetTableName);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NETWORK_CLASS);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
            subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.DIRECTION);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.NAVI_STATUS);
            subFields.Add("{0}.{1}", StreetTableName, GStreet.AND_STATUS);
            subFields.Add("{0}.SHAPE", StreetTableName);

            ListStringBuilder whereClauses = new ListStringBuilder();
            whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);

            if (filter)
            { 
                whereClauses.Add("({0}.{1} IS null OR {0}.{1} = 0)", StreetTableName, GStreet.AND_STATUS);
            }

            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = tables.Join(",");
            queryDef.SubFields = subFields.Join(",");
            queryDef.WhereClause = whereClauses.Join(" AND ");

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", StreetTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            ISymbolLevels symbolLevels = (ISymbolLevels)layer;
            symbolLevels.UseSymbolLevels = false;

            //1
            ISimpleLineSymbol simpleLineSymbol1 = new SimpleLineSymbolClass();
            simpleLineSymbol1.Color = ColorUtils.Get(250, 158, 37);
            simpleLineSymbol1.Width = width;

            //2
            ISimpleLineSymbol simpleLineSymbol2 = new SimpleLineSymbolClass();
            simpleLineSymbol2.Color = ColorUtils.Get(255, 0, 197);
            simpleLineSymbol2.Width = width;

            //3
            ISimpleLineSymbol simpleLineSymbol3 = new SimpleLineSymbolClass();
            simpleLineSymbol3.Color = ColorUtils.Get(255, 225, 104);
            simpleLineSymbol3.Width = width;

            //4
            ISimpleLineSymbol simpleLineSymbol4 = new SimpleLineSymbolClass();
            simpleLineSymbol4.Color = ColorUtils.Get(85, 255, 0);
            simpleLineSymbol4.Width = width;

            //5
            ISimpleLineSymbol simpleLineSymbol5 = new SimpleLineSymbolClass();
            simpleLineSymbol5.Color = ColorUtils.Get(153, 102, 5);
            simpleLineSymbol5.Width = width;

            //6
            ISimpleLineSymbol simpleLineSymbol6 = new SimpleLineSymbolClass();
            simpleLineSymbol6.Color = ColorUtils.Get(102, 153, 255);
            simpleLineSymbol6.Width = width;

            IUniqueValueRenderer uniqueValueRenderer = new UniqueValueRendererClass();
            uniqueValueRenderer.FieldCount = 1;
            string field = StreetTableName + "." + GStreet.NETWORK_CLASS;
            uniqueValueRenderer.set_Field(0, field);

            uniqueValueRenderer.AddValue("1", "", (ISymbol)simpleLineSymbol1);
            uniqueValueRenderer.set_Heading("1", "");
            uniqueValueRenderer.set_Label("1", "Highway");

            uniqueValueRenderer.AddValue("2", "", (ISymbol)simpleLineSymbol2);
            uniqueValueRenderer.set_Heading("2", "");
            uniqueValueRenderer.set_Label("2", "Major Road");

            uniqueValueRenderer.AddValue("3", "", (ISymbol)simpleLineSymbol3);
            uniqueValueRenderer.set_Heading("3", "");
            uniqueValueRenderer.set_Label("3", "Secondary Road");

            uniqueValueRenderer.AddValue("4", "", (ISymbol)simpleLineSymbol4);
            uniqueValueRenderer.set_Heading("4", "");
            uniqueValueRenderer.set_Label("4", "Normal Road");

            uniqueValueRenderer.AddValue("-1", "", (ISymbol)simpleLineSymbol5);
            uniqueValueRenderer.set_Heading("-1", "");
            uniqueValueRenderer.set_Label("-1", "Closed Road");

            uniqueValueRenderer.AddValue("<Null>", "", (ISymbol)simpleLineSymbol6);
            uniqueValueRenderer.set_Heading("<Null>", "");
            uniqueValueRenderer.set_Label("<Null>", "Unknown");

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();

            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;

            ILineLabelPosition lineLabelPosition = new LineLabelPositionClass();
            lineLabelPosition.ProduceCurvedLabels = true;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.LineLabelPosition = lineLabelPosition;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();

            string expression =
            "Function FindLabel ( [" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "], [" + StreetTableName + "." + GStreet.NAME + "] ) \n" +
                "FindLabel = PCase( trim([" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "]) & \" \" & [" + StreetTableName + "." + GStreet.NAME + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = displayAnno;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)uniqueValueRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateStreetTextLayer(double minScale, double maxScale, bool displayAnno, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(StreetTextTableName);

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            labelEngineLayerProperties.Symbol.Font.Italic = true;
            labelEngineLayerProperties.Symbol.Color = ColorUtils.Get(227, 110, 60);

            ILineLabelPosition lineLabelPosition = new LineLabelPositionClass();
            lineLabelPosition.ProduceCurvedLabels = true;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.LineLabelPosition = lineLabelPosition;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();

            string expression =
            "Function FindLabel ( [" + GStreetText.STREET_TEXT + "] ) \n" +
                    "FindLabel = PCase( [" + GStreetText.STREET_TEXT + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(227, 110, 60);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.MaximumScale = maxScale;
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private void AddLandmarkBoundaryLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(LandmarkBoundaryTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(211, 255, 190);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Landmark Boundary";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddSectionBoundaryLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(SectionBoundaryTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(190, 210, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Section Boundary";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddRegionBoundaryLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(RegionTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(153, 255, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            string expression =
                 "Function FindLabel ( [" + GRegion.INDEX + "] ) \n" +
                     "FindLabel = PCase( [" + GRegion.INDEX + "] ) \n" +
                 "End Function \n" +

                 "Function PCase(strInput) \n" +
                     "Dim iPosition \n" +
                     "Dim iSpace \n" +
                     "Dim strOutput \n" +
                     "iPosition = 1 \n" +
                     "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                         "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                         "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                         "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                         "iPosition = iSpace + 1 \n" +
                     "Loop \n" +
                     "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                     "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                     "PCase = strOutput \n" +
                 "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;
            labelEngineLayerProperties.Symbol.Font.Bold = true;
            labelEngineLayerProperties.Symbol.Size = 9;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Region Boundary";
            layer.MinimumScale = 500000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddRoadLineLayers()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Road CenterLine";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateMajorRoadLineLayer(5000, 0, 1));
            groupLayer.Add(CreateMinorRoadLineLayer(5000, 0, 1));
            groupLayer.Add(CreateHighwayLineLayer(5000, 0, 1));

            _layer.Add(groupLayer);
        }

        private ILayer CreateHighwayLineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(HighwayLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Highway Line";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateMajorRoadLineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(MajorRoadLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Major Road Line";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateMinorRoadLineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(MinorRoadLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Minor Road Line";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateRailwayLayer(double minScale, double maxScale)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();

            ICartographicLineSymbol cartographicLineSymbol = new CartographicLineSymbolClass();
            cartographicLineSymbol.Color = ColorUtils.Get(0, 0, 0);
            cartographicLineSymbol.Cap = esriLineCapStyle.esriLCSButt;
            cartographicLineSymbol.Join = esriLineJoinStyle.esriLJSRound;
            cartographicLineSymbol.Width = 0.8;

            IHashLineSymbol hashLineSymbol = new HashLineSymbolClass();
            hashLineSymbol.Color = ColorUtils.Get(0, 0, 0);
            hashLineSymbol.Angle = 90;
            hashLineSymbol.Width = 4;

            ILineProperties lineProperties = (ILineProperties)hashLineSymbol;
            lineProperties.Offset = 0;

            ITemplate template = lineProperties.Template;
            template.ClearPatternElements();
            template.Interval = 1;
            template.AddPatternElement(1, 7);

            lineProperties.Template = template;

            IMultiLayerLineSymbol multiLayerLineSymbol = new MultiLayerLineSymbolClass();
            multiLayerLineSymbol.AddLayer(cartographicLineSymbol);
            multiLayerLineSymbol.AddLayer(hashLineSymbol);

            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(RailwayTableName);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)multiLayerLineSymbol;

            layer.Name = "Railway";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private void AddRoadOutlineLayers()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Road Outline";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateMajorRoadOutlineLayer(5000, 0, 1));
            groupLayer.Add(CreateMinorRoadOutlineLayer(5000, 0, 1));
            groupLayer.Add(CreateHighwayOutlineLayer(5000, 0, 1));

            _layer.Add(groupLayer);
        }

        private ILayer CreateHighwayOutlineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(HighwayOutlineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Highway Outline";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateMajorRoadOutlineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(MajorRoadOutlineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Major Road Outline";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private ILayer CreateMinorRoadOutlineLayer(double minScale, double maxScale, double width)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(MinorRoadOutlineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 255);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Minor Road Outline";
            layer.MaximumScale = maxScale;
            layer.MinimumScale = minScale;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            return layer;
        }

        private void AddBuildingLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BuildingLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Building Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddBuildingPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BuildingPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(245, 239, 220);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Building Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddBungalowLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BungalowLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Bungalow Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddBungalowPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BungalowPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(255, 234, 199);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Bungalow Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddResidentialLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Residential Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddResidentialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(190, 210, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Residential Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddResidentialPolygon2Layer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialPolygon2TableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(190, 210, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Residential Polygon2";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddShopLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ShopLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Shop Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddShopPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ShopPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(232, 190, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Shop Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddIndustrialLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(IndustrialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Industrial Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddIndustrialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(IndustrialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(242, 224, 194);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Industrial Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddCommercialLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(CommercialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Commercial Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddCommercialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(CommercialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(245, 202, 122);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Commercial Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddOtherLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(OtherLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Other Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddOtherPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(OtherPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(186, 188, 219);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Other Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddParkingLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ParkingLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Parking Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddParkingPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ParkingPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(225, 225, 225);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Parking Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddLotLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(LotLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Lot Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddLotPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(LotPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(240, 235, 235);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Lot Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddHydrographicLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(HydrographicLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Hydrographic Line";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddHydrographicPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(HydrographicPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(165, 191, 221);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Hydrographic Polygon";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddGroupAreaLine()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Area Line";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateGreenaryLineLayer());
            groupLayer.Add(CreateCommercialLineLayer());
            groupLayer.Add(CreateIndustrialLineLayer());
            groupLayer.Add(CreateResidentialLayer());

            _layer.Add(groupLayer);

        }

        // noraini ali - And Group Layer for AND User
        private ILayer CreateGreenaryLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(GreenaryLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Greenary Line";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateCommercialLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(CommercialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Commercial Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateIndustrialLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(IndustrialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Industrial Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateResidentialLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Residential Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private void AddGroupLandbaseLotLine()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Lot Line";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateLotLineLineLayer());
            groupLayer.Add(CreateBunglowLineLayer());
            groupLayer.Add(CreateBuildingLineLayer());
            groupLayer.Add(CreateShopLineLayer());
            groupLayer.Add(CreateParkingLineLayer());
            groupLayer.Add(CreateOtherLineLayer());
            groupLayer.Add(CreateHydrographicLineLayer());
            groupLayer.Add(CreateRailwayLayer(5000, 0));

            _layer.Add(groupLayer);
        }

        private ILayer CreateLotLineLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(LotLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Lot Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateBunglowLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BungalowLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Bungalow Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateBuildingLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BuildingLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Building Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateShopLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ShopLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Shop Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateParkingLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ParkingLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Parking Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateOtherLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(OtherLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Other Line";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateHydrographicLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(HydrographicLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Hydrographic Line";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private void AddGroupLandbaseAreaPolygon()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Area Polygon";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateResidentialPolygonLayer());
            groupLayer.Add(CreateIndustrialPolygonLayer());
            groupLayer.Add(CreateGreenaryPolygonLayer());
            groupLayer.Add(CreateCommercialPolygonLayer());

            _layer.Add(groupLayer);
        }

        private ILayer CreateResidentialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(190, 210, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Residential Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateIndustrialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(IndustrialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(242, 224, 194);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Industrial Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateGreenaryPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(GreenaryPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(115, 178, 115);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Greenary Polygon";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateBuildingPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BuildingPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(245, 239, 220);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Building Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private ILayer CreateCommercialPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(CommercialPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(245, 202, 122);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Commercial Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }

        private void AddGroupLandbaseLotPolygon()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            groupLayer.Name = "Lot Polygon";
            groupLayer.Expanded = false;
            groupLayer.Visible = false;

            groupLayer.Add(CreateShopPolygonLayer());
            groupLayer.Add(CreateResidentialPolygon2Layer());
            groupLayer.Add(CreateOtherPolygonLayer());
            groupLayer.Add(CreateBungalowPolygonLayer());
            groupLayer.Add(CreateParkingPolygonLayer());
            groupLayer.Add(CreateLotPolygonLayer());
            groupLayer.Add(CreateBuildingPolygonLayer());

            _layer.Add(groupLayer);
        }
        private ILayer CreateShopPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ShopPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(232, 190, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Shop Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateResidentialPolygon2Layer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ResidentialPolygon2TableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(190, 210, 255);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Residential Polygon2";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateOtherPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(OtherPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(186, 188, 219);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Other Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateBungalowPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(BungalowPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(255, 234, 199);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Bungalow Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateParkingPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(ParkingPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(225, 225, 225);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Parking Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        private ILayer CreateLotPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(LotPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(240, 235, 235);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Lot Polygon";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            return layer;
        }
        //end added noraini ali - Jul 2020


        private void AddGreenaryLineLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(GreenaryLineTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            layer.Name = "Greenary Line";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }



        private void AddGreenaryPolygonLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _clbWorkspace.OpenFeatureClass(GreenaryPolygonTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.4;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(115, 178, 115);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Greenary Polygon";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            layer.Visible = false;

            _layer.Add(layer);
        }

        private void AddWorkAreaLayer()
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(WorkAreaTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(104, 104, 104);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(255, 198, 179);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            string expression =
           "Function FindLabel ( [" + GWorkArea.AREA_SEGMENT + "] ) \n" +
               "FindLabel = PCase( [" + GWorkArea.AREA_SEGMENT + "] ) \n" +
           "End Function \n" +

           "Function PCase(strInput) \n" +
               "Dim iPosition \n" +
               "Dim iSpace \n" +
               "Dim strOutput \n" +
               "iPosition = 1 \n" +
               "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                   "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                   "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                   "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                   "iPosition = iSpace + 1 \n" +
               "Loop \n" +
               "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
               "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
               "PCase = strOutput \n" +
           "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;
            labelEngineLayerProperties.Symbol.Font.Bold = true;
            labelEngineLayerProperties.Symbol.Size = 12;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Work Area";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            ILayerEffects LayerEffects = layer as ILayerEffects;
            LayerEffects.Transparency = 60;

            _layer.Add(layer);
        }

        private void AddBuildingGroupANDLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = BuildingGroupANDTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} <> 2", BuildingGroupANDTableName, GBuildingGroupAND.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", BuildingGroupANDTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(0, 204, 15);
            simpleMarkerSymbol.Size = 4.0;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSSquare;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GBuildingGroupAND.NAME + "] ) \n" +
                "FindLabel = PCase( [" + GBuildingGroupAND.NAME + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;
            labelEngineLayerProperties.Symbol.Font.Bold = true;
            labelEngineLayerProperties.Symbol.Size = 9;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Building Group AND";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddBuildingANDLayer (bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryDef.Tables = BuildingANDTableName;
            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} <> 2", BuildingANDTableName, GBuildingAND.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", BuildingANDTableName);
            
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(0, 153, 51);
            simpleMarkerSymbol.Size = 4.0;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSSquare;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GBuildingAND.NAME + "], [" + GBuildingAND.NAME2 + "] ) \n" +
                "FindLabel = PCase( [" + GBuildingAND.NAME + "]  & \" \" & [" + GBuildingAND.NAME2 + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Building AND";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddStreetANDLayer(bool filter)
        {
            ListStringBuilder tables = new ListStringBuilder();
            tables.Add(StreetANDTableName);
            tables.Add(GStreetType.TABLE_NAME);

            ListStringBuilder subFields = new ListStringBuilder();
            subFields.Add("{0}.OBJECTID", StreetANDTableName);
            subFields.Add("{0}.{1}", StreetANDTableName, GStreetAND.TYPE);
            subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
            subFields.Add("{0}.{1}", StreetANDTableName, GStreetAND.NAME);
            subFields.Add("{0}.{1}", StreetANDTableName, GStreetAND.NAME2);
            subFields.Add("{0}.SHAPE", StreetANDTableName);

            ListStringBuilder whereClauses = new ListStringBuilder();           
            if (filter)
            {
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetANDTableName, GStreetAND.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                whereClauses.Add("{0}.{1} <> 2", StreetANDTableName, GStreetAND.AND_STATUS); }
            else
            {
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetANDTableName, GStreetAND.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
            }

            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = tables.Join(",");
            queryDef.SubFields = subFields.Join(",");
            queryDef.WhereClause = whereClauses.Join(" AND ");

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", StreetANDTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            ISymbolLevels symbolLevels = (ISymbolLevels)layer;
            symbolLevels.UseSymbolLevels = false;

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 2.0;
            simpleLineSymbol.Color = ColorUtils.Get(0, 153, 51);

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleLineSymbol;

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();

            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;

            ILineLabelPosition lineLabelPosition = new LineLabelPositionClass();
            lineLabelPosition.ProduceCurvedLabels = true;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.LineLabelPosition = lineLabelPosition;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();

            string expression =
            "Function FindLabel ( [" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "], [" + StreetANDTableName + "." + GStreetAND.NAME + "] ) \n" +
                "FindLabel = PCase( trim([" + GStreetType.TABLE_NAME + "." + GStreetType.NAME + "]) & \" \" & [" + StreetANDTableName + "." + GStreetAND.NAME + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Street AND";
            //layer.MaximumScale = 0;
            layer.MinimumScale = 80000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);

        }
        private void AddJunctionANDLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryDef.Tables = JunctionANDTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} <> 2", JunctionANDTableName, GJunctionAND.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", JunctionANDTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(0, 153, 51);
            simpleMarkerSymbol.Size = 4.0;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSDiamond;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            layer.Name = "Junction AND";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddPropertyANDLayer(bool filter)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();

            ListStringBuilder tables = new ListStringBuilder();
            tables.Add(PropertyANDTableName);
            tables.Add(GPropertyType.TABLE_NAME);

            ListStringBuilder subFields = new ListStringBuilder();
            subFields.Add("{0}.OBJECTID", PropertyANDTableName);
            subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.ABBREVIATION);
            subFields.Add("{0}.{1}", PropertyANDTableName, GPropertyAND.LOT);
            subFields.Add("{0}.{1}", PropertyANDTableName, GPropertyAND.HOUSE);
            subFields.Add("{0}.SHAPE", PropertyANDTableName);

            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            queryDef.Tables = tables.Join(",");
            queryDef.SubFields = subFields.Join(",");

            ListStringBuilder whereClauses = new ListStringBuilder();
            whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyANDTableName, GPropertyAND.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
            if (filter)
            { whereClauses.Add("{0}.{1} <> 2", PropertyANDTableName, GPropertyAND.AND_STATUS); ; }

            queryDef.WhereClause = whereClauses.Join(" AND ");

            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", PropertyANDTableName);

            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(0, 153, 51);
            simpleMarkerSymbol.Size = 6.0;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSCircle;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();

            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;

            IBasicOverposterLayerProperties basicOverposterLayerProperties = labelEngineLayerProperties.BasicOverposterLayerProperties;
            basicOverposterLayerProperties.PointPlacementMethod = esriOverposterPointPlacementMethod.esriOnTopPoint;
            basicOverposterLayerProperties.PointPlacementOnTop = true;

            labelEngineLayerProperties.BasicOverposterLayerProperties = basicOverposterLayerProperties;

            string expression =
                "Function FindLabel ( [" + PropertyANDTableName + "." + GPropertyAND.LOT + "], [" + PropertyANDTableName + "." + GPropertyAND.HOUSE + "], [" + GPropertyType.TABLE_NAME + "." + GPropertyType.ABBREVIATION + "]) \n" +
                    "FindLabel = BuildLabel([" + PropertyANDTableName + "." + GPropertyAND.LOT + "], [" + PropertyANDTableName + "." + GPropertyAND.HOUSE + "], [" + GPropertyType.TABLE_NAME + "." + GPropertyType.ABBREVIATION + "]) \n" +
                "End Function \n" +

                "Function BuildLabel(strLot, strHouse, propertyType) \n" +
                    "Dim strOutput \n" +
                    "If Not IsNull(strLot) Then \n" +
                        "strOutput = strLot \n" +
                    "End If \n" +
                    "If Not IsNull(strHouse) Then \n" +
                        "strOutput = strOutput & \" \" & strHouse \n" +
                    "End If \n" +
                    "strOutput = strOutput & vbNewLine & vbNewLine & propertyType \n" +
                    "BuildLabel = strOutput \n" +
                "End Function";

            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Property AND";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddLandmarkANDLayer(bool filter)
        {
            IQueryDef queryDef = _caduWorkspace.CreateQueryDef();
            IQueryName2 queryName2 = new FeatureQueryNameClass();
            queryDef.Tables = LandmarkANDTableName;

            if (filter)
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} <> 2", LandmarkANDTableName, GLandmarkAND.AND_STATUS);
                queryDef.WhereClause = whereClauses.Join("AND");
            }

            queryName2.QueryDef = queryDef;
            queryName2.PrimaryKey = string.Format("{0}.OBJECTID", LandmarkANDTableName);

            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(queryName2);

            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = ColorUtils.Get(0, 153, 51);
            simpleMarkerSymbol.Size = 6.0;
            simpleMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSDiamond;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = ColorUtils.Get(0, 0, 0);
            simpleMarkerSymbol.OutlineSize = 1;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleMarkerSymbol;

            string expression =
            "Function FindLabel ( [" + GLandmark.NAME + "], [" + GLandmark.NAME2 + "] ) \n" +
                "FindLabel = PCase( [" + GLandmark.NAME + "]  & \" \" & [" + GLandmark.NAME2 + "] ) \n" +
            "End Function \n" +

            "Function PCase(strInput) \n" +
                "Dim iPosition \n" +
                "Dim iSpace \n" +
                "Dim strOutput \n" +
                "iPosition = 1 \n" +
                "Do While InStr(iPosition, strInput, \" \", 1) <> 0 \n" +
                    "iSpace = InStr(iPosition, strInput, \" \", 1) \n" +
                    "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                    "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition)) \n" +
                    "iPosition = iSpace + 1 \n" +
                "Loop \n" +
                "strOutput = strOutput & UCase(Mid(strInput, iPosition, 1)) \n" +
                "strOutput = strOutput & LCase(Mid(strInput, iPosition + 1)) \n" +
                "PCase = strOutput \n" +
            "End Function";

            IAnnotateLayerProperties annotateLayerProperties = new LabelEngineLayerPropertiesClass();
            ILabelEngineLayerProperties labelEngineLayerProperties = (ILabelEngineLayerProperties)annotateLayerProperties;
            labelEngineLayerProperties.IsExpressionSimple = false;
            IAnnotationExpressionEngine annotationExpressionEngine = new AnnotationVBScriptEngineClass();
            labelEngineLayerProperties.ExpressionParser = annotationExpressionEngine;
            labelEngineLayerProperties.Expression = expression;

            IAnnotateLayerPropertiesCollection annotateLayerPropertiesCollection = new AnnotateLayerPropertiesCollectionClass();
            annotateLayerPropertiesCollection.Add(annotateLayerProperties);

            layer.Name = "Landmark AND";
            layer.MinimumScale = 3000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = true;
            layer.AnnotationProperties = annotateLayerPropertiesCollection;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;

            _layer.Add(layer);
        }

        private void AddLandmarkBoundaryANDLayer(bool filter)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            IFeatureClass featureClass = _caduWorkspace.OpenFeatureClass(LandmarkBoundaryANDTableName);

            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 0.26;
            simpleLineSymbol.Color = ColorUtils.Get(0, 0, 0);

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = ColorUtils.Get(0, 204, 15);
            simpleFillSymbol.Style = esriSimpleFillStyle.esriSFSSolid;
            simpleFillSymbol.Outline = simpleLineSymbol;

            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = (ISymbol)simpleFillSymbol;

            layer.Name = "Landmark Boundary AND";
            layer.MinimumScale = 50000;
            layer.FeatureClass = featureClass;
            layer.DisplayAnnotation = false;
            layer.Renderer = (IFeatureRenderer)simpleRenderer;
            layer.Cached = true;
            _layer.Add(layer);
        }
    }
}


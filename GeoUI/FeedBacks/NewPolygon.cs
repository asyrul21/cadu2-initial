﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.FeedBacks
{
    public class NewPolygon : NewFeedBack
    {
        protected INewPolygonFeedback _feedback;
        protected List<MovePoint> _vertices;
        protected bool _showVertex;
        protected IScreenDisplay _screenDisplay;
        protected IRgbColor _vertexColor;
        protected int _vertexSize;
        protected esriSimpleMarkerStyle _vertexStyle;

        public int PointCount { protected set; get; }

        public IPoint FromPoint
        {
            set
            {
                IPolygon polygon = (IPolygon)Geometry;
                polygon.FromPoint = value;
            }
            get
            {
                IPolygon polygon = (IPolygon)Geometry;
                return polygon.FromPoint;
            }
        }

        public IPoint ToPoint
        {
            set
            {
                IPolygon polygon = (IPolygon)Geometry;
                polygon.ToPoint = value;
            }
            get
            {
                IPolygon polygon = (IPolygon)Geometry;
                return polygon.ToPoint;
            }
        }

        public double Length
        {
            get
            {
                IPolygon polygon = (IPolygon)Geometry;
                return polygon.Length;
            }
        }

        public NewPolygon(IScreenDisplay display, IRgbColor color)
            : this(display, color, 2)
        {
        }

        public NewPolygon(IScreenDisplay display, IRgbColor color, double width)
            : this(display, color, width, esriSimpleLineStyle.esriSLSSolid)
        {
        }

        public NewPolygon(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style)
            : this(display, color, width, style, true)
        {
        }

        public NewPolygon(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style, bool showVertex)
        {
            IsStarted = false;
            _feedback = new NewPolygonFeedbackClass();
            _vertices = new List<MovePoint>();
            // has bug
            _showVertex = false;
            //_showVertex = showVertex;

            // Marker symbols
            ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
            simpleLineSymbol.Color = color;
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Style = style;

            _feedback.Display = display;
            _screenDisplay = display;

            _vertexSize = 8;
            _vertexStyle = esriSimpleMarkerStyle.esriSMSSquare;
            _vertexColor = ColorUtils.Get(Color.Green);
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(point);
            if (_showVertex)
            {
                AddVertex(point);
            }
            PointCount++;
            Geometry = null;
            IsStarted = true;
        }

        public override void AddPoint(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.AddPoint(point);
            if (_showVertex)
            {
                AddVertex(point);
            }
            PointCount++;
        }

        private void AddVertex(IPoint point)
        {
            MovePoint movePoint = new MovePoint(_screenDisplay, _vertexColor, _vertexColor, _vertexSize, _vertexStyle);
            movePoint.Point = point;
            movePoint.Start(point);
            _vertices.Add(movePoint);
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
        }

        public void RemoveLastPoint()
        {
            if (!IsStarted)
            {
                return;
            }
            if (PointCount <= 2)
            {
                return;
            }
            IPointCollection pointCollection = Stop() as IPointCollection;
            if (pointCollection == null)
            {
                throw new Exception("No point created.");
            }
            if (pointCollection.PointCount > 0)
            {
                Start(pointCollection.Point[0]);
                for (int count = 1; count < pointCollection.PointCount - 2; count++)
                {
                    AddPoint(pointCollection.Point[count]);
                }
            }
            else
            {
                throw new Exception("No point added.");
            }
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            Geometry = _feedback.Stop();
            foreach (MovePoint movePoint in _vertices)
            {
                movePoint.Stop();
            }
            _vertices.Clear();
            GC.Collect();
            PointCount = 0;
            IsStarted = false;
            return Geometry;
        }

        public override void Refresh(int hDC)
        {
            foreach (MovePoint movePoint in _vertices)
            {
                movePoint.Refresh(hDC);
            }
            _feedback.Refresh(hDC);
        }
    }
}

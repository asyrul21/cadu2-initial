﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.FeedBacks
{
    public class MoveLine : MoveFeedBack, IDisposable
    {
        protected IMoveLineFeedback _feedback;

        public IPolyline Line { set; get; }

        public MoveLine(IScreenDisplay display)
            : this(display, ColorUtils.Select)
        {
        }

        public MoveLine(IScreenDisplay display, IRgbColor color)
            : this(display, color, 2)
        {
        }

        public MoveLine(IScreenDisplay display, IRgbColor color, double width)
            : this(display, color, width, esriSimpleLineStyle.esriSLSSolid)
        {
        }

        public MoveLine(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style)
            : this(display, color, width, esriSimpleLineStyle.esriSLSSolid, false)
        {
        }

        public MoveLine(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style, bool showVertex)
        {
            IsStarted = false;
            _feedback = new MoveLineFeedbackClass();

            ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
            simpleLineSymbol.Color = color;
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Style = style;

            _feedback.Display = display;
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(Line, point);
            IsStarted = true;
        }

        public override void Restart(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.Stop();
            _feedback.Start(Line, point);
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            IPolyline line = _feedback.Stop();
            IsStarted = false;
            return line;
        }

        public override void Refresh(int hDC)
        {
            _feedback.Refresh(hDC);
        }

        public void Dispose()
        {
            _feedback.Display = null;
            while (Marshal.ReleaseComObject(_feedback) != 0) { }
            _feedback = null;
        }
    }
}

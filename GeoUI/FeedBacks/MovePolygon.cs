﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.FeedBacks
{
    public class MovePolygon : MoveFeedBack, IDisposable
    {
        protected IMovePolygonFeedback _feedback;

        public IPolygon Polygon { set; get; }

        public MovePolygon(IScreenDisplay display)
            : this(display, ColorUtils.Select, ColorUtils.Select)
        {
        }

        public MovePolygon(IScreenDisplay display, IRgbColor color)
            : this(display, color, color)
        {
        }

        public MovePolygon(IScreenDisplay display, IRgbColor outlineColor, IRgbColor fillColor)
            : this(display, outlineColor, fillColor, 2)
        {
        }

        public MovePolygon(IScreenDisplay display, IRgbColor outlineColor, IRgbColor fillColor, double outlineWidth)
            : this(display, outlineColor, fillColor, outlineWidth, esriSimpleFillStyle.esriSFSHollow)
        {
        }

        public MovePolygon(IScreenDisplay display, IRgbColor outlineColor, IRgbColor fillColor, double outlineWidth, esriSimpleFillStyle style)
        {
            IsStarted = false;
            _feedback = new MovePolygonFeedbackClass();

            ISimpleFillSymbol simpleFillSymbol = (ISimpleFillSymbol)_feedback.Symbol;

            ILineSymbol lineSymbol = simpleFillSymbol.Outline;
            lineSymbol.Color = outlineColor;
            lineSymbol.Width = outlineWidth;

            simpleFillSymbol.Color = fillColor;
            simpleFillSymbol.Outline = lineSymbol;
            simpleFillSymbol.Style = style;

            _feedback.Display = display;
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(Polygon, point);
            IsStarted = true;
        }

        public override void Restart(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.Stop();
            _feedback.Start(Polygon, point);
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            IPolygon polygon = _feedback.Stop();
            IsStarted = false;
            return polygon;
        }

        public override void Refresh(int hDC)
        {
            _feedback.Refresh(hDC);
        }

        public void Dispose()
        {
            _feedback.Display = null;
            while (Marshal.ReleaseComObject(_feedback) != 0) { }
            _feedback = null;
        }
    }
}

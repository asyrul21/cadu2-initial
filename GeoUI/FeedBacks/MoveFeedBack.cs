﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.FeedBacks
{
    public abstract class MoveFeedBack : FeedBack
    {
        public abstract void Restart(IPoint point);
    }
}

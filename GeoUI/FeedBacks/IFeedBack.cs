﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.FeedBacks
{
    public interface IFeedBack
    {
        bool IsStarted { get; }

        void Start(IPoint point);

        void MoveTo(IPoint point);

        IGeometry Stop();

        void Refresh(int hDC);
    }
}

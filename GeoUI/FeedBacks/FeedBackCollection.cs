﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.FeedBacks
{
    public class FeedBackCollection : FeedBack
    {
        protected List<FeedBack> _feedBacks;

        public FeedBackCollection()
        {
            _feedBacks = new List<FeedBack>();
        }

        public void Add(FeedBack feedBack)
        {
            _feedBacks.Add(feedBack);
        }

        public void Clear()
        {
            foreach (FeedBack feedBack in _feedBacks)
            {
                feedBack.Stop();
            }
            _feedBacks.Clear();
            IsStarted = false;
        }

        public override void Start(IPoint point)
        {
            foreach (FeedBack feedBack in _feedBacks)
            {
                feedBack.Start(point);
            }
            IsStarted = true;
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            foreach (FeedBack feedBack in _feedBacks)
            {
                feedBack.MoveTo(point);
            }
        }

        public override IGeometry Stop()
        {
            Clear();
            return null;
        }

        public override void Refresh(int hDC)
        {
            foreach (FeedBack feedBack in _feedBacks)
            {
                feedBack.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.FeedBacks
{
    public abstract class FeedBack : IFeedBack
    {
        public IGeometry Geometry { protected set; get; }

        public bool IsStarted { protected set; get; }

        public abstract void Start(IPoint point);

        public abstract void MoveTo(IPoint point);

        public abstract IGeometry Stop();

        public abstract void Refresh(int hDC);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.FeedBacks
{
    public class MovePoint : MoveFeedBack, IDisposable
    {
        protected IMovePointFeedback _feedback;
        private IPoint _startPoint;

        private double OffsetX
        {
            get
            {
                return _startPoint.X - Point.X;
            }
        }

        private double OffsetY
        {
            get
            {
                return _startPoint.Y - Point.Y;
            }
        }

        public IPoint Point { set; get; }

        public MovePoint(IScreenDisplay display)
            : this(display, ColorUtils.Select, ColorUtils.Select)
        {
        }

        public MovePoint(IScreenDisplay display, IRgbColor color)
            : this(display, color, color)
        {
        }

        public MovePoint(IScreenDisplay display, IRgbColor color, double size)
            : this(display, color, color, size)
        {
        }

        public MovePoint(IScreenDisplay display, IRgbColor color, IRgbColor outlineColor)
            : this(display, color, outlineColor, 8)
        {
        }

        public MovePoint(IScreenDisplay display, IRgbColor color, IRgbColor outlineColor, double size)
            : this(display, color, outlineColor, size, esriSimpleMarkerStyle.esriSMSCircle)
        {
        }

        public MovePoint(IScreenDisplay display, IRgbColor color, IRgbColor outlineColor, double size, esriSimpleMarkerStyle style)
        {
            IsStarted = false;
            _feedback = new MovePointFeedbackClass();

            // Marker symbols
            ISimpleMarkerSymbol simpleMarkerSymbol = (ISimpleMarkerSymbol)_feedback.Symbol;
            simpleMarkerSymbol.Color = color;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = outlineColor;
            simpleMarkerSymbol.Size = size;
            simpleMarkerSymbol.Style = style;

            _feedback.Display = display;
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(Point, point);
            _startPoint = point;
            IsStarted = true;
        }

        public override void Restart(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.Stop();
            Start(point);
        }

        public override void MoveTo(IPoint point)
        {
            MoveTo(point, false);
        }

        public void MoveTo(IPoint point, bool useOffset)
        {
            if (!IsStarted)
            {
                return;
            }
            if (useOffset)
            {
                IPoint offSetPoint = new PointClass();
                offSetPoint.X = point.X - OffsetX;
                offSetPoint.Y = point.Y - OffsetY;
                _feedback.MoveTo(offSetPoint);
            }
            else
            {
                _feedback.MoveTo(point);
            }
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            Geometry = _feedback.Stop();
            _startPoint = null;
            IsStarted = false;
            return Geometry;
        }

        public override void Refresh(int hDC)
        {
            _feedback.Refresh(hDC);
        }

        public void Dispose()
        {
            _feedback.Display = null;
            while (Marshal.ReleaseComObject(_feedback) != 0) { }
            _feedback = null;
        }
    }
}

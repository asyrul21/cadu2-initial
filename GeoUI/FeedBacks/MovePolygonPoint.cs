﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Utilities;
using System.Drawing;

namespace Geomatic.UI.FeedBacks
{
    public class MovePolygonPoint : FeedBack
    {
        public IPolygon Polygon { set; get; }
        public int PointCount { protected set; get; }
        public int Index { set; get; }

        public IEnumerable<IPoint> GetVertices()
        {
            IPointCollection4 pointCollection = (IPointCollection4)Polygon;
            for (int count = 0; count < pointCollection.PointCount; count++)
            {
                yield return pointCollection.get_Point(count);
            }
        }

        public List<IPoint> Vertices
        {
            get
            {
                return GetVertices().ToList();
            }
        }

        protected IPolygonMovePointFeedback _feedback;
        protected List<MovePoint> _vertices;
        protected bool _showVertex;
        protected IScreenDisplay _screenDisplay;
        protected IRgbColor _vertexColor;
        protected int _vertexSize;
        protected esriSimpleMarkerStyle _vertexStyle;

        public MovePolygonPoint(IScreenDisplay display, IRgbColor color)
            : this(display, color, 1)
        {
        }

        public MovePolygonPoint(IScreenDisplay display, IRgbColor color, double width)
            : this(display, color, width, esriSimpleLineStyle.esriSLSSolid)
        {
        }

        public MovePolygonPoint(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style)
            : this(display, color, width, style, false)
        {
        }

        public MovePolygonPoint(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style, bool showVertex)
        {
            IsStarted = false;
            Index = 0;
            _feedback = new PolygonMovePointFeedbackClass();
            _vertices = new List<MovePoint>();

            ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
            simpleLineSymbol.Color = color;
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Style = style;

            _feedback.Display = display;
            _screenDisplay = display;

            _vertexSize = 8;
            _vertexStyle = esriSimpleMarkerStyle.esriSMSSquare;
            _vertexColor = ColorUtils.Get(Color.Green);
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(Polygon, Index, point);
            if (_showVertex)
            {
                foreach (IPoint vertex in GetVertices())
                {
                    MovePoint movePoint = new MovePoint(_screenDisplay, _vertexColor, _vertexColor, _vertexSize, _vertexStyle);
                    movePoint.Point = vertex;
                    movePoint.Start(point);
                    _vertices.Add(movePoint);
                }
            }

            IsStarted = true;
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
            //_vertices[Index].MoveTo(point);
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            IPolygon polygon = _feedback.Stop();
            Index = 0;
            foreach (MovePoint movePoint in _vertices)
            {
                movePoint.Stop();
            }
            _vertices.Clear();
            GC.Collect();
            IsStarted = false;
            return polygon;
        }

        public override void Refresh(int hDC)
        {
            _feedback.Refresh(hDC);
            foreach (MovePoint movePoint in _vertices)
            {
                movePoint.Refresh(hDC);
            }
        }
    }
}

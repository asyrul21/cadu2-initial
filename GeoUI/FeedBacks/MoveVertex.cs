﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using System.Runtime.InteropServices;

namespace Geomatic.UI.FeedBacks
{
    public class MoveVertex : FeedBack
    {
        protected IVertexFeedback _feedback;
        protected IScreenDisplay _display;
        protected IColor _color;
        protected double _width;
        protected esriSimpleLineStyle _style;

        public MoveVertex(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style)
        {
            IsStarted = false;
            _display = display;
            _color = color;
            _width = width;
            _style = style;
        }

        public void AddSegment(ISegment segment, bool fromPointIsAnchor)
        {
            if (_feedback == null)
            {
                _feedback = new VertexFeedbackClass();

                ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
                simpleLineSymbol.Color = _color;
                simpleLineSymbol.Width = _width;
                simpleLineSymbol.Style = _style;

                _feedback.Display = _display;
            }
            _feedback.AddSegment(segment, fromPointIsAnchor);
        }

        public override void Start(IPoint point)
        {
            if (_feedback == null)
            {
                _feedback = new VertexFeedbackClass();

                ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
                simpleLineSymbol.Color = _color;
                simpleLineSymbol.Width = _width;
                simpleLineSymbol.Style = _style;

                _feedback.Display = _display;
            }
            _feedback.MoveTo(point);
            IsStarted = true;
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
        }

        public override IGeometry Stop()
        {
            _feedback = null;
            IsStarted = false;
            return null;
        }

        public override void Refresh(int hDC)
        {
            if (_feedback != null)
            {
                _feedback.Refresh(hDC);
            }
        }
    }
}

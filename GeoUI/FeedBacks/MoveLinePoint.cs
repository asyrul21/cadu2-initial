﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Utilities;
using System.Drawing;

namespace Geomatic.UI.FeedBacks
{
    public class MoveLinePoint : FeedBack
    {
        public IPolyline Line { set; get; }
        public int PointCount { protected set; get; }
        public int Index { set; get; }

        protected ILineMovePointFeedback _feedback;
        protected List<MovePoint> _vertices;
        protected bool _showVertex;
        protected IScreenDisplay _screenDisplay;
        protected IRgbColor _vertexColor;
        protected int _vertexSize;
        protected esriSimpleMarkerStyle _vertexStyle;

        public MoveLinePoint(IScreenDisplay display, IRgbColor color)
            : this(display, color, 2)
        {
        }

        public MoveLinePoint(IScreenDisplay display, IRgbColor color, double width)
            : this(display, color, width, esriSimpleLineStyle.esriSLSSolid)
        {
        }

        public MoveLinePoint(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style)
            : this(display, color, width, style, true)
        {
        }

        public MoveLinePoint(IScreenDisplay display, IRgbColor color, double width, esriSimpleLineStyle style, bool showVertex)
        {
            IsStarted = false;
            PointCount = 0;
            _feedback = new LineMovePointFeedbackClass();
            _vertices = new List<MovePoint>();
            _showVertex = false;

            ISimpleLineSymbol simpleLineSymbol = (ISimpleLineSymbol)_feedback.Symbol;
            simpleLineSymbol.Color = color;
            simpleLineSymbol.Width = width;
            simpleLineSymbol.Style = style;

            _feedback.Display = display;
            _screenDisplay = display;

            _vertexSize = 8;
            _vertexStyle = esriSimpleMarkerStyle.esriSMSSquare;
            _vertexColor = ColorUtils.Get(Color.Green);
        }

        public override void Start(IPoint point)
        {
            _feedback.Start(Line, Index, point);
            IsStarted = true;
        }

        public override void MoveTo(IPoint point)
        {
            if (!IsStarted)
            {
                return;
            }
            _feedback.MoveTo(point);
        }

        public override IGeometry Stop()
        {
            if (!IsStarted)
            {
                return null;
            }
            IPolyline line = _feedback.Stop();
            IsStarted = false;
            return line;
        }

        public override void Refresh(int hDC)
        {
            _feedback.Refresh(hDC);
        }
    }
}

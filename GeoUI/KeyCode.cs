﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI
{
    public class KeyCode
    {
        public const int No_Key_Pressed = 0;
        public const int Shift = 1;
        public const int Ctrl = 2;
        public const int Shift_And_Ctrl = 3;
        public const int Alt = 4;
        public const int Shift_And_Alt = 5;
        public const int Ctrl_And_Alt = 6;
        public const int Shift_Ctrl_And_Alt = 7;
    }
}

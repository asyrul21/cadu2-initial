﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI
{
    public class MouseKey
    {
        public const int Left = 1;
        public const int Right = 2;
        public const int Left_And_Right = 3;
        public const int Middle = 4;
        public const int Left_And_Middle = 5;
        public const int Right_And_Middle = 6;
        public const int All = 7;
    }
}

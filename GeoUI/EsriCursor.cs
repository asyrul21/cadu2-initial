﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;

namespace Geomatic.UI
{
    public class EsriCursor
    {
        public static int GetHandle(esriSystemMouseCursor mouseCursor)
        {
            ISystemMouseCursor systemMouseCursor = new SystemMouseCursor();
            systemMouseCursor.Load(mouseCursor);
            return systemMouseCursor.Cursor;
        }
    }
}

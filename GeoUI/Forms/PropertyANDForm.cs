﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Commands;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Validators;
using Geomatic.Core.Sessions;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms
{
    public partial class PropertyANDForm : CollapsibleForm
    {
        StringBuilder sb = new StringBuilder();
        public static string sendtextLotHse;
        public int? SelectedConstructionStatus
        {
            get { return (cbConstStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstStatus.SelectedItem).Code; }
        }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.Code; }
        }

        public string InsertedLot
        {
            get { return txtLot.Text; }
        }

        public string InsertedHouse
        {
            get { return txtHouse.Text; }
        }

        public int InsertedQuantity
        {
            get { return numOfFloors.Value; }
        }

        public int? SelectedYearInstall
        {
            get { return (cbYearInstall.SelectedItem == null) ? (int?)null : Convert.ToInt32(cbYearInstall.SelectedItem); }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        protected GPropertyAND _propertyAND;

        public PropertyANDForm()
            : this(null)
        {
        }

        public PropertyANDForm(GPropertyAND property)
        {
            InitializeComponent();
            _commandPool.Register(Command.EditableItem,
             lblConstructionStatus, cbConstStatus,
             cbType,
             txtLot, txtHouse,
             cbYearInstall, cbSource,
             lblSource, cbSource,
             btnApply
             );
            _propertyAND = property;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateConstructionStatus(cbConstStatus);
            ComboBoxUtils.PopulatePropertyType(cbType);
            ComboBoxUtils.PopulateYear(cbYearInstall);

            controlFloor.Columns.Add("Id");
            controlFloor.Columns.Add("Floor Number");
            controlFloor.Columns.Add("Unit No.");

            controlPoi.Columns.Add("Id");
            controlPoi.Columns.Add("Code");
            controlPoi.Columns.Add("Name");
            controlPoi.Columns.Add("Name2");
            controlPoi.Columns.Add("Navi_Status");
            controlPoi.Columns.Add("Url");
            controlPoi.Columns.Add("Description");

            txtId.Text = _propertyAND.OriId.ToString();

            cbConstStatus.Text = _propertyAND.ConstructionStatusValue;

            cbType.Text = _propertyAND.TypeAbbreviationValue + " - " + _propertyAND.TypeValue;

            txtLot.Text = _propertyAND.Lot;
            txtHouse.Text = _propertyAND.House;
            numOfFloors.Value = _propertyAND.NumberOfFloor.HasValue ? _propertyAND.NumberOfFloor.Value : 0;
            cbYearInstall.Text = _propertyAND.YearInstall.ToString();
            cbSource.Text = _propertyAND.SourceValue;

            txtDateCreated.Text = _propertyAND.DateCreated;
            txtDateUpdated.Text = _propertyAND.DateUpdated;
            txtCreator.Text = _propertyAND.CreatedBy;
            txtUpdater.Text = _propertyAND.UpdatedBy;

            GStreet street = _propertyAND.GetStreet();

            if (street != null)
            {
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null)
                {
                    txtStreetId.Text = streetAND.OriId.ToString();
                    txtStreetType.Text = streetAND.TypeValue;
                    txtStreetName.Text = streetAND.Name;
                    txtStreetName2.Text = streetAND.Name2;
                    txtSection.Text = streetAND.Section;
                    txtPostcode.Text = streetAND.Postcode;
                    txtCity.Text = streetAND.City;
                    txtState.Text = streetAND.State;
                }
                else
                {
                    txtStreetId.Text = street.OID.ToString();
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }
            PopulatePoi();
            PopulateFloor();
        }

        public void disablePOIEdit()
        {
            controlPoi.Enabled = false;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedType >= 10)
            {
                numOfFloors.Value = (cbType.SelectedItem == null) ? 0 : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.DefaultFloor.Value;
                PropTypeHasMultiFloor();
            }
            else
            {
                controlFloor.Enabled = false;
            }
        }

        // noraini ali - Jul 2020 -  Allow to create for certain property type only
        private void PropTypeHasMultiFloor()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyType> query = new Query<GPropertyType>();
            query.Obj.Code = SelectedType;
            query.AddClause(() => query.Obj.FloorType, "IN ('MFA')");

            if (repo.Count<GPropertyType>(query) > 0)
            {
                controlFloor.Enabled = true;
            }
            else
            {
                controlFloor.Enabled = false;
            }
        }

        // noraini ali - OKT 2020 - Checking status allow to change property type
        // Get Floor Addres, if change type to non-MFA
        // Get Building associate to this property, if change type to non-MSA
        protected bool StatusAllowToChangePropType()
        {
            // Noraini ali - OKT 2020
            // floor address exist- not allow to change prop type in MFA to non-MFA
            if (controlFloor.Items.Count > 0 && !GetGroupPropType("MFA"))
            {
                MessageBox.Show("Floor address exist, Please delete floor address first to change non-floor address type!", "Property Type",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            // checking exisitng prop type have AND building
            // Get AND building first because ADM/AND building still associate this property
            // not allow change MSA Type to non-MSA Type

            GBuildingAND buildingAND = _propertyAND.GetbuildingAND();
            if (buildingAND != null && buildingAND.AndStatus != 2 && ChangePropTypeNoBuilding())
            //if (_propertyAND.HasBuildingAND() && ChangePropTypeNoBuilding())
            {
                MessageBox.Show("Building exist for this Property, Please delete building first to change non-building type!", "Property Type",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {   // Original ADM Building associate to this property 
                // Only checking Original ADM not reassociate to ohers
                GBuilding building = _propertyAND.Getbuilding();
                if (building != null && building.AndStatus != 2 && ChangePropTypeNoBuilding())
                //if (_propertyAND.HasBuilding() && ChangePropTypeNoBuilding())
                {
                    MessageBox.Show("Building exist for this Property, Please delete building first to change non-building type!", "Property Type",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            return true;
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Construction Status
            bool isConstructionStatusValid = PropertyValidator.CheckConstructionStatus(SelectedConstructionStatus);
            LabelColor(lblConstructionStatus, isConstructionStatusValid);
            pass &= isConstructionStatusValid;

            // Type
            bool isTypeValid = true;
            //bool isTypeValid = PropertyValidator.CheckType(SelectedType, _propertyAND.HasBuilding());
            if (SelectedType == 1 || SelectedType == 2)
            {
                isTypeValid = false;
                MessageBox.Show("Please select property type!", "Property Type",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            // noraini ali - Feb 2021
            if (cbType.SelectedItem == null)
            {
                isTypeValid = false;
            }
            else
            {
                isTypeValid = StatusAllowToChangePropType();
            }
            // end

            LabelColor(lblType, isTypeValid);
            pass &= isTypeValid;

            // noraini ali - Jun 2020 - comment to validate property Lot/House because feature cannot update during create Work Area
            // Lot / House
            bool isLotHouseValid = PropertyValidator.CheckLotHouse(txtLot.Text, txtHouse.Text);
            LabelColor(lblLot, isLotHouseValid);
            LabelColor(lblHouse, isLotHouseValid);
            pass &= isLotHouseValid;

            // Year Install
            bool isYearValid = PropertyValidator.CheckYear(SelectedYearInstall);
            LabelColor(lblYearInstall, isYearValid);
            pass &= isYearValid;
            
            // Source
            bool isSourceValid = PropertyValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public virtual void SetValues()
        {
            _propertyAND.ConstructionStatus = SelectedConstructionStatus;
            _propertyAND.Type = SelectedType;
            _propertyAND.NumberOfFloor = InsertedQuantity;
            _propertyAND.Lot = InsertedLot;
            _propertyAND.House = InsertedHouse;
            _propertyAND.Source = SelectedSource.ToString();
            _propertyAND.YearInstall = SelectedYearInstall;
        }

        private void PopulatePoi()
        {
            controlPoi.BeginUpdate();
            controlPoi.Sorting = SortOrder.None;
            controlPoi.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _propertyAND.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.NavigationStatusValue); 
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);                   
                item.Tag = poi;

                items.Add(item);
            }

            controlPoi.Items.AddRange(items.ToArray());
            controlPoi.FixColumnWidth();
            controlPoi.EndUpdate();
        }

        private void PopulateFloor()
        {
            controlFloor.BeginUpdate();
            controlFloor.Sorting = SortOrder.None;
            controlFloor.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GFloor floor in _propertyAND.GetFloors())
            {
                ListViewItem item = new ListViewItem();
                item.Text = floor.OID.ToString();
                item.SubItems.Add(floor.Number);
                item.SubItems.Add(floor.NumUnit);
                item.Tag = floor;

                items.Add(item);
            }

            controlFloor.Items.AddRange(items.ToArray());
            controlFloor.FixColumnWidth();
            controlFloor.EndUpdate();
            txtTotUnit.Text = controlFloor.Items.Count.ToString(); // added by noraini
        }

		
        private void controlFloor_AddClick(object sender, EventArgs e)
        {
            //RepositoryFactory repo = new RepositoryFactory(_propertyAND.SegmentName);
            //GFloor newFloor = repo.NewObj<GFloor>();
            //newFloor.PropertyId = _propertyAND.OriId;
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Add floor address button clicked");
            sb.Append("\n");

            if ((txtLot.Text != string.Empty) && (txtHouse.Text != string.Empty))
            {
                sendtextLotHse = txtHouse.Text;
            }
            else if ((txtLot.Text == string.Empty) && (txtHouse.Text != string.Empty))
            {
                sendtextLotHse = txtHouse.Text;
            }
            else if ((txtLot.Text != string.Empty) && (txtHouse.Text == string.Empty))
            {
                sendtextLotHse = txtLot.Text;
            }

			#region OLD Floor Format
			/*
            using (AddFloorForm form = new AddFloorForm(newFloor))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    //repo.Delete(newFloor);
                    return;
                }
                using (new WaitCursor())
                {
                    int floorAmount = form.SelectedNumOfFloors;
                    int startFloor = form.SelectedStartFloor;
                    int unitAmount = form.SelectedNumOfUnit;
                    // added by noraini ali - Feb 2019
                    string floorPrefix = form.SelectedFloorPrefix;
                    string floorPostfix = form.SelectedFloorPostfix;
                    string UnitPrefix = form.SelectedUnitPrefix;
                    string UnitPostfix = form.SelectedUnitPostfix;
                    // end added
                    int endFloorLoop = startFloor + floorAmount;

                    // noraini ali - Mar 2019 - separate format floor & Unit
                    for (int floor = startFloor; floor < endFloorLoop; floor++)
                    {
                        string floorNumString = floor.ToString();
                        switch (form.SelectedFloorNumFormat)
                        {
                            default:
                            case "1": break;
                            case "01": floorNumString = floor.ToString("00"); break;
                            case "001": floorNumString = floor.ToString("000"); break;
                            case "0001": floorNumString = floor.ToString("0000"); break;
                        }

                        // added by noraini ali - Feb 2019
                        if ((!string.IsNullOrEmpty(floorPrefix) || (!string.IsNullOrEmpty(floorPostfix))))
                        {
                            if (!string.IsNullOrEmpty(floorPrefix))
                                floorNumString = string.Format("{0}{1}", floorPrefix, floorNumString);

                            if (!string.IsNullOrEmpty(floorPostfix))
                                floorNumString = string.Format("{0}{1}", floorNumString, floorPostfix);
                        }
                        newFloor.Number = string.Format("{0}", floorNumString);

                        if (unitAmount <= 0)
                        {   
                            for (int unit = 1; unit <= 1; unit++)
                            {
                                newFloor.NumUnit = "-";

                                //validation
                                bool noDuplicate = CheckDuplicateFormat(newFloor.NumUnit, newFloor.Number);

                                if (noDuplicate)
                                {
                                    //insert new floor into repo

                                    newFloor = repo.Insert(newFloor);
                                    System.Diagnostics.Debug.WriteLine("Inserting: " + newFloor);

                                    ListViewItem item = new ListViewItem();
                                    item.Text = newFloor.OID.ToString();
                                    item.SubItems.Add(newFloor.Number);
                                    item.SubItems.Add(newFloor.NumUnit);
                                    item.Tag = newFloor;
                                    controlFloor.Items.Add(item);
                                    controlFloor.FixColumnWidth();
                                }
                            }
                        }
                        else
                        //if (unitAmount >= 1)
                        {
                            for (int unit = 1; unit <= unitAmount; unit++)
                            {
                                string unitNumString = unit.ToString();
                                switch (form.SelectedUnitNoFormat)
                                {
                                    default:
                                    case "1": break;
                                    case "01": unitNumString = unit.ToString("00"); break;
                                    case "001": unitNumString = unit.ToString("000"); break;
                                    case "0001": unitNumString = unit.ToString("0000"); break;
                                }

                                if ((!string.IsNullOrEmpty(UnitPrefix) || (!string.IsNullOrEmpty(UnitPostfix))))
                                {
                                    if (!string.IsNullOrEmpty(UnitPrefix))
                                        unitNumString = string.Format("{0}{1}", UnitPrefix, unitNumString);

                                    if (!string.IsNullOrEmpty(UnitPostfix))
                                        unitNumString = string.Format("{0}{1}", unitNumString, UnitPostfix);
                                }
                                // end added

                                newFloor.NumUnit = string.Format("{0}", unitNumString);

                                //validation
                                bool noDuplicate = CheckDuplicateFormat(newFloor.NumUnit, newFloor.Number);

                                if (noDuplicate)
                                {
                                    //insert new floor into repo
                                    newFloor = repo.Insert(newFloor);
                                    System.Diagnostics.Debug.WriteLine("Inserting: " + newFloor);

                                    ListViewItem item = new ListViewItem();
                                    item.Text = newFloor.OID.ToString();
                                    item.SubItems.Add(newFloor.Number);
                                    item.SubItems.Add(newFloor.NumUnit);
                                    item.Tag = newFloor;
                                    controlFloor.Items.Add(item);
                                    controlFloor.FixColumnWidth();
                                }
                            }
                        }
                    }                    
                }
            }
			*/
			#endregion
			
			// noraini - Feb 2022
			// use MFA format address sama as MSA format address form
			using (AddFloorForm form2 = new AddFloorForm())
			{
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening add floor address format form completed");
                sb.Append("\n");

                if (form2.ShowDialog(this) != DialogResult.OK)
				{
					return;
				}

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button add floor address format form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
				{
					int floorAmount = form2.SelectedNumOfFloors;
					int aptAmount = form2.SelectedNumOfApt;
					int startFloor = form2.SelectedStartFloor;
					int startUnit = form2.SelectedStartApt;

                    // get floor and Unit digit
                    int FloorDigitNum = form2.FloorDigit;
					int UnitDigitNum = form2.UnitDigit;

					int endFloorLoop = startFloor + floorAmount;
					int endUnitLoop = startUnit + aptAmount;

					// allow user to keyin floor num as alphanumeric value 
					// like 'G','LG' etc
					bool textflrNum = false;
					if (string.IsNullOrEmpty(form2.SelectedTextFloorNum))
					{
						textflrNum = true;
					}

					RepositoryFactory repo = new RepositoryFactory(_propertyAND.SegmentName);
					GFloor newFloor = repo.NewObj<GFloor>();
					newFloor.PropertyId = _propertyAND.OriId;

					for (int floor = startFloor; floor < endFloorLoop; floor++)
					{
						string floorNumString = "";

						if (textflrNum)
						{
							floorNumString = floor.ToString();
						}
						else
						{
							floorNumString = form2.SelectedTextFloorNum;
						}

						newFloor.Number = string.Format("{0}", floorNumString);
						int i = 0;

						for (int unit = startUnit; unit < endUnitLoop; unit++)
						{
							string strFormat = "";
                            if (unit != startUnit) i++;

                            // string format 1
                            strFormat = AddressFormat.GenerateAddressFormat(form2.SelectedFormat1, form2.Prefix1, form2.SelectedBlock, floor, unit,
                                                        startUnit, i, form2.ChkAptChar1, FloorDigitNum, UnitDigitNum);
                            string totalstringFormat = strFormat;

                            // string format 2
                            strFormat = AddressFormat.GenerateAddressFormat(form2.SelectedFormat2, form2.Prefix2, form2.SelectedBlock, floor, unit,
                                                        startUnit, i, form2.ChkAptChar2, FloorDigitNum, UnitDigitNum);
                            totalstringFormat = totalstringFormat + strFormat;

                            // string format 3
                            strFormat = AddressFormat.GenerateAddressFormat(form2.SelectedFormat3, form2.Prefix3, form2.SelectedBlock, floor, unit,
                                                    startUnit, i, form2.ChkAptChar3, FloorDigitNum, UnitDigitNum);
                            totalstringFormat = totalstringFormat + strFormat;

							// string format 4
							strFormat = "";

							if (!string.IsNullOrEmpty(form2.Prefix4))
                                strFormat = string.Format("{0}", form2.Prefix4);

							totalstringFormat = totalstringFormat + strFormat;
							
							newFloor.NumUnit = string.Format("{0}", totalstringFormat); ;
							bool noDuplicate = CheckDuplicateFormat(newFloor.NumUnit, newFloor.Number);

							if (noDuplicate)
							{
								newFloor = repo.Insert(newFloor);
								ListViewItem item = new ListViewItem();
								item.Text = newFloor.OID.ToString();
								item.SubItems.Add(newFloor.Number);
								item.SubItems.Add(newFloor.NumUnit);
								item.SubItems.Add("-");
								item.Tag = newFloor;
								controlFloor.Items.Add(item);
								controlFloor.FixColumnWidth();
							}
						}
					}
				}
			}
			// added by noraini - Feb2019
			txtTotUnit.Text = controlFloor.Items.Count.ToString();
            // end added by noraini
        }
		

        private void controlFloor_EditClick(object sender, EventArgs e)
        {
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Edit floor address button clicked");
            sb.Append("\n");

            if (controlFloor.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_propertyAND.SegmentName);
            ListViewItem item = controlFloor.SelectedItems[0];
            GFloor Floor = (GFloor)item.Tag;
            GFloor floor = repo.GetById<GFloor>(Floor.OID);

            using (EditFloorForm form = new EditFloorForm(floor))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening edit floor address format form completed.");
                sb.Append("\n");

                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button edit floor address format form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
                {
                    form.SetValues();
                    //repo.Update(floor, true, true);
                    repo.Update(floor);

                    item.SubItems.Clear();
                    item.Text = floor.OID.ToString();
                    item.SubItems.Add(floor.Number);
                    item.SubItems.Add(floor.NumUnit);
                    item.Tag = floor;
                    controlFloor.FixColumnWidth();
                }
            }
        }

        private void controlFloor_DeleteClick(object sender, EventArgs e)
        {
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Delete floor address button clicked");
            sb.Append("\n");

            using (new WaitCursor())
            {
                if (controlFloor.SelectedItems.Count == 0)
                {
                    return;
                }
                // modified by Noraini - Feb2019
                RepositoryFactory repo = new RepositoryFactory(_propertyAND.SegmentName);

                foreach (ListViewItem item in controlFloor.SelectedItems)
                {
                    GFloor floor = (GFloor)item.Tag;
                    //GFloor floor = repo.GetById<GFloor>(Floor.OID);
                    repo.Delete(floor);
                    item.Remove();
                    controlFloor.FixColumnWidth();
                }
                txtTotUnit.Text = controlFloor.Items.Count.ToString();
            }
        }
		

        private bool CheckDuplicateFormat(string unit, string floor)
        {
            bool noDuplicate = true;
            foreach (GFloor floorX in _propertyAND.GetFloors())
            {
                if (unit == floorX.NumUnit && floor == floorX.Number)
                {
                    noDuplicate = false;
                    MessageBox.Show("MutiFloor Format is duplicate", "MultiFloor Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
            }
            return noDuplicate;
        }

        protected bool GetGroupPropType(string GroupPropertyType)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyType> query = new Query<GPropertyType>();
            query.Obj.Code = SelectedType;
            query.AddClause(() => query.Obj.FloorType, "IN ('" + GroupPropertyType + "')");

            return repo.Count<GPropertyType>(query) > 0 ? true : false;
        }

        protected bool ChangePropTypeNoBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyPermission> query = new Query<GPropertyPermission>();
            query.Obj.Code = SelectedType;
            return repo.Count<GPropertyPermission>(query) > 0 ? true : false;
        }

        public string MessageButtonClick()
        {
            return "\n" + sb.ToString();
        }
    }
	
}

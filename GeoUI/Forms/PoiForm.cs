﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Add;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Validators;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.UI.Forms
{
    public partial class PoiForm : CollapsibleForm
    {
        #region Properties

        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public GFloor SelectedFloor { protected set; get; }

        public string InsertedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string InsertedName2
        {
            get { return txtName2.Text; }
        }

        public string InsertedAbbreviation
        {
            get { return txtAbbreviation.Text; }
        }

        public string InsertedDescription
        {
            get { return txtDescription.Text; }
        }

        public string InsertedUrl
        {
            get { return txtUrl.Text; }
        }

        public int InsertedFilterLevel
        {
            get { return numFilterLevel.Value; }
        }

        public int? DisplayText
        {
            get { return (chkDisplayText.Checked) ? 1 : 0; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        #endregion

        protected GPoi _poi;

        public PoiForm()
            : this(null)
        {
        }

        public PoiForm(GPoi poi)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
                btnSetCode,
                lblCode, txtCode,
                lblName, txtName,
                lblName2, txtName2,
                lblAbbreviation, txtAbbreviation,
                lblDescription, txtDescription,
                lblUrl, txtUrl,
                lblFilterLevel, numFilterLevel,
                chkFamous,
                chkDisplayText,
                lblSource, cbSource,
                btnApply
                );

            _poi = poi;
        }

        protected override void Form_Load()
        {
            if (_poi.ParentType == (int)ParentClass.Building || _poi.ParentType == (int)ParentClass.Landmark)
            {
                btnSetFloor.Enabled = false;
            }

            controlPhone.Columns.Add("Phone Number");
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);

            txtId.Text = _poi.OID.ToString();

            GCode1 code1 = _poi.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _poi.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _poi.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            cbNaviStatus.Text = _poi.NavigationStatusValue;
            txtCode.Text = _poi.Code;
            txtName.Text = _poi.Name;
            txtName2.Text = _poi.Name2;
            txtUrl.Text = _poi.Url;
            txtDescription.Text = _poi.Description;
            chkFamous.Checked = _poi.IsFamous;
            chkDisplayText.Checked = (_poi.DisplayText == 1);
            numFilterLevel.Value = _poi.FilterLevel.HasValue ? _poi.FilterLevel.Value : 0;
            cbSource.Text = _poi.SourceValue;
            txtParentId.Text = _poi.ParentId.HasValue ? _poi.ParentId.Value.ToString() : "";
            txtParentType.Text = _poi.ParentTypeValue;

            txtDateCreated.Text = _poi.DateCreated;
            txtDateUpdated.Text = _poi.DateUpdated;
            txtCreator.Text = _poi.CreatedBy;
            txtUpdater.Text = _poi.UpdatedBy;

            GStreet street = _poi.GetStreet();
            if (street != null)
            {
                txtStreetId.Text = street.OID.ToString();
                txtStreetType.Text = street.TypeValue;
                txtStreetName.Text = street.Name;
                txtStreetName2.Text = street.Name2;
                txtSection.Text = street.Section;
                txtPostcode.Text = street.Postcode;
                txtCity.Text = street.City;
                txtState.Text = street.State;
            }

            PopulateFloor();
            PopulatePhones();
        }

        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                if (code3.IsStandardName) //auto naming
                {
                    txtName.Text = code3.CodeName;
                    txtName2.Text = string.Empty;
                    txtName.Enabled = false;
                    txtName2.Enabled = false;
                }
                else
                {
                    txtName.Enabled = true;
                    txtName2.Enabled = true;
                }

                //update category control
                GCode1 code1 = code3.GetCode1();
                GCode2 code2 = code3.GetCode2();
                categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
                categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
                categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            }
        }

        private void btnSetFloor_Click(object sender, EventArgs e)
        {
            if (_poi.ParentType == (int)ParentClass.Building || _poi.ParentType == (int)ParentClass.Landmark)
            {
                return;
            }

            GProperty property = ((IGPropertyPoi)_poi).GetParent() as GProperty;

            if (property == null)
            {
                return;
            }

            List<GFloor> floors = property.GetFloors().ToList();

            using (ChooseFloorForm chooseForm = new ChooseFloorForm(floors))
            {
                if (chooseForm.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GFloor floor = (GFloor)chooseForm.SelectedRow;

                txtFloor.Text = floor == null ? string.Empty : floor.Number;

                SelectedFloor = floor;
            }
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Code
            bool isCodeValid = PoiValidator.CheckCode(txtCode.Text);
            LabelColor(lblCode, isCodeValid);
            pass &= isCodeValid;

            // Name
            bool isNameValid = PoiValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Name 2
            bool isName2Valid = PoiValidator.CheckName2(txtName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Url
            bool isUrlValid = PoiValidator.CheckUrl(txtUrl.Text);
            LabelColor(lblUrl, isUrlValid);
            pass &= isUrlValid;

            // Source
            bool isSourceValid = PoiValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        protected void PopulateFloor()
        {
            GFloor floor = _poi.GetFloor();
            if (floor != null)
            {
                txtFloor.Text = floor.Number;
                SelectedFloor = floor;
            }
        }

        protected void PopulatePhones()
        {
            controlPhone.BeginUpdate();
            controlPhone.Sorting = SortOrder.None;
            controlPhone.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoiPhone phone in _poi.GetPhones())
            {
                ListViewItem item = CreatePhoneItem(phone);
                items.Add(item);
            }

            controlPhone.Items.AddRange(items.ToArray());
            controlPhone.FixColumnWidth();
            controlPhone.EndUpdate();
        }

        public void SetValues()
        {
            _poi.NavigationStatus = SelectedNavigationStatus;
            _poi.Code = InsertedCode;
            _poi.Name = InsertedName;
            _poi.Name2 = InsertedName2;
            _poi.Abbreviation = InsertedAbbreviation;
            _poi.Description = InsertedDescription;
            _poi.Url = InsertedUrl;
            _poi.Famous = chkFamous.Checked ? "F" : string.Empty;
            _poi.DisplayText = DisplayText;
            _poi.FilterLevel = InsertedFilterLevel;
            _poi.Source = SelectedSource;
            _poi.FloorId = SelectedFloor == null ? 0 : SelectedFloor.OID;
        }

        private void controlPhone_AssociateClick(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(_poi.SegmentName);
            GPoiPhone newPhone = repo.NewObj<GPoiPhone>();
            newPhone.RefId = _poi.OID;
            newPhone = repo.Insert(newPhone, false);

            using (AddPoiPhoneForm form = new AddPoiPhoneForm(newPhone))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    //MessageBox.Show("Invalid format!");
                    repo.Delete(newPhone);
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();

                    repo.Update(newPhone);

                    ListViewItem item = CreatePhoneItem(newPhone);
                    controlPhone.Items.Add(item);
                }
            }
        }

        private void controlPhone_DissociateClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlPhone.SelectedItems.Count == 0)
                {
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory(_poi.SegmentName);
                ListViewItem item = controlPhone.SelectedItems[0];
                GPoiPhone phone = (GPoiPhone)item.Tag;
                repo.Delete(phone);
                item.Remove();
            }
        }

        protected ListViewItem CreatePhoneItem(GPoiPhone phone)
        {
            ListViewItem item = new ListViewItem();
            item.Text = phone.Number;
            item.Tag = phone;
            return item;
        }
    }
}

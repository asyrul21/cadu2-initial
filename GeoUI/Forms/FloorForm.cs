﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Controls;
using Geomatic.Core.Utilities;
using Geomatic.Core.Validators;
using Geomatic.Core.Rows;
using Geomatic.UI.Properties;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms
{
    public partial class FloorForm : CollapsibleForm
    {

        public int SelectedNumOfFloors
        {
            get { return numOfFloors.Value; }
        }

        public int SelectedStartFloor
        {
            get { return numStartFloor.Value; }
        }

        public int SelectedNumOfUnit
        {
            get { return numOfUnit.Value; }
        }

        public string SelectedFloorPrefix
        {
            get { return txtFloorPrefix.Text; }
        }

        public string SelectedFloorPostfix
        {
            get { return txtFloorPostfix.Text; }
        }

        public string SelectedUnitPrefix
        {
            get { return txtUnitPrefix.Text; }
        }

        public string SelectedUnitPostfix
        {
            get { return txtUnitPostfix.Text; }
        }

        /* comment out by noraini
        public string SelectedFlrStringFormat
        {
            get { return cbFloorFormat.Text; }
        }

        public string SelectedBlockName
        {
            get { return txtBlockName.Text; }
        }
        */
        public string SelectedFloorNumFormat
        {
            get { return cbFloorNumFormat.Text; }
        }

        public string SelectedUnitNoFormat
        {
            get { return cbUnitNoFormat.Text; }
        }

        protected GFloor _floor;

        public FloorForm()
            : this(null)
        {
        }

        public FloorForm(GFloor floor)
        {
            InitializeComponent();
            _floor = floor;
        }

        protected override void Form_Load()
        {
            GProperty property = _floor.GetProperty();

            if ((property.House != null) && (property.Lot != null))
            {
                txtLot.Text = property.House;
            }
            else if ((property.House != null) && (property.Lot == null))
            {
                txtLot.Text = property.House;
            }
            else if ((property.House == null) && (property.Lot != null))
            {
                txtLot.Text = property.Lot;
            }
            else
            {
                txtLot.Text = PropertyForm.sendtextLotHse; // Noraini
                //txtLot.Text = string.Empty;
            }
            txtUnitPrefix.Text = string.Format("{0}-", txtLot.Text);
            //string houseLot = txtLot.Text;
            //txtUnitPrefix.Text = (houseLot + "-");
            cbFloorNumFormat.SelectedIndex = 0;
            cbUnitNoFormat.SelectedIndex = 0;

            // noraini ali - OKt 2021 - Display Username & Date Infomation
            txtCreator.Text = _floor.CreatedBy;
            txtDateCreated.Text = _floor.DateCreated;
            txtUpdater.Text = _floor.UpdatedBy;
            txtDateUpdated.Text = _floor.DateUpdated;
            // end
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {

                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected override bool ValidateValues()
        {

            bool pass = true;

            // FloorNumberFormat
            bool isFloorNumFormatValid = true;
            isFloorNumFormatValid &= cbFloorNumFormat.Text != "";
            pass &= isFloorNumFormatValid;

            // UnitNumberFormat
            bool isUnitNumFormatValid = true;
            isUnitNumFormatValid &= cbUnitNoFormat.Text != "";
            pass &= isUnitNumFormatValid;

            /* comment by noraini
            // FloorFormat
            bool isFloorFormatValid = true;
            isFloorFormatValid &= cbFloorFormat.Text != "";
            pass &= isFloorFormatValid;

            // BlockName
            bool isBlockNameInputValid = true;
            if (SelectedFlrStringFormat.Contains("[BLOCK]")) isBlockNameInputValid &= SelectedBlockName != "";
            pass &= isBlockNameInputValid;

            lblFloorFormat.ForeColor = isFloorFormatValid ? Color.Black : Color.Red;
            //lblBlockName.ForeColor = isBlockNameValid ? Color.Black : Color.Red;
            lblFloorNumFormat.ForeColor = isFloorNumFormatValid ? Color.Black : Color.Red;
            lblUnitNoFormat.ForeColor = isUnitNumFormatValid ? Color.Black : Color.Red;

            if (!isBlockNameInputValid)
            {
                lblFloorFormat.ForeColor = isBlockNameInputValid ? Color.Black : Color.Red;
                lblBlockName.ForeColor = isBlockNameInputValid ? Color.Black : Color.Red;
            }
            */

            return pass;

        }

        private void PopulatePreview()
        {
            // added by noraini ali - Feb 2019
            string preview = "";          

            string floorPrefix = SelectedFloorPrefix;
            string floorPostfix = SelectedFloorPostfix;
            string FloorNumFormat = SelectedFloorNumFormat;

            if (!string.IsNullOrEmpty(floorPrefix))
                FloorNumFormat = string.Format("{0}{1}", floorPrefix, FloorNumFormat);

            if (!string.IsNullOrEmpty(floorPostfix))
                FloorNumFormat = string.Format("{0}{1}", FloorNumFormat, floorPostfix);

            preview = string.Format("{0}", FloorNumFormat);

            int unitNum = SelectedNumOfUnit;
            if (unitNum > 0)
            {
                string unitPrefix = SelectedUnitPrefix;
                string unitPostfix = SelectedUnitPostfix;
                string unitNumFormat = SelectedUnitNoFormat;
                preview = "";

                if (!string.IsNullOrEmpty(unitPrefix))
                    unitNumFormat = string.Format("{0}{1}", unitPrefix, unitNumFormat);

                if (!string.IsNullOrEmpty(unitPostfix))
                    unitNumFormat = string.Format("{0}{1}", unitNumFormat, unitPostfix);

                preview = string.Format("{0}  {1}", FloorNumFormat, unitNumFormat);
            }

            lblPreviewText.Text = preview;

            // end added

            /* comment by noraini
            string preview = SelectedFlrStringFormat;
            if (preview.Contains("[BLOCK]") && SelectedBlockName != "")
                preview = preview.Replace("[BLOCK]", SelectedBlockName);

            // added by noraini ali - Feb 2019
            string floorPrefix = SelectedFloorPrefix;
            string floorPostfix = SelectedFloorPostfix;
            string FloorNumFormat = SelectedFloorNumFormat;

            if (!string.IsNullOrEmpty(floorPrefix))
                FloorNumFormat = string.Format("{0}{1}", floorPrefix, FloorNumFormat);

            if (!string.IsNullOrEmpty(floorPostfix))
                FloorNumFormat = string.Format("{0}{1}", FloorNumFormat, floorPostfix);

            if (preview.Contains("[FLR]") && FloorNumFormat != "")
                preview = preview.Replace("[FLR]", FloorNumFormat);

            string unitPrefix = SelectedUnitPrefix;
            string unitPostfix = SelectedUnitPostfix;
            string unitNumFormat = SelectedUnitNoFormat;

            if (!string.IsNullOrEmpty(unitPrefix))
                unitNumFormat = string.Format("{0}{1}", unitPrefix, unitNumFormat);

            if (!string.IsNullOrEmpty(unitPostfix))
                unitNumFormat = string.Format("{0}{1}", unitNumFormat, unitPostfix);

            if (preview.Contains("[UNIT]") && unitNumFormat != "")
                preview = preview.Replace("[UNIT]", unitNumFormat);

            // comment by noraini
            //if (preview.Contains("[FLR]") && SelectedFloorNumFormat != "")
            //    preview = preview.Replace("[FLR]", SelectedFloorNumFormat);

            //if (preview.Contains("[UNIT]") && SelectedUnitNoFormat != "")
            //    preview = preview.Replace("[UNIT]", SelectedUnitNoFormat);
            // end comment by noraini
            */
        }

        private void cbFloorFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        
        //private void txtFloorName_TextChanged(object sender, EventArgs e)
        //{
        //    PopulatePreview();
        //}

        private void cbFloorNumFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbUnitNoFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtFloorPrefix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtFloorPostfix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtUnitPrefix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtUnitPostfix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        // noraini ali - Mac 2019 - allow unit number value zero 
        private void numOfUnit_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
            int unitNum = SelectedNumOfUnit;

            txtUnitPostfix.Enabled = true;
            txtUnitPrefix.Enabled = true;
            cbUnitNoFormat.Enabled = true;
            if (unitNum < 1)
            {
                txtUnitPostfix.Enabled = false;
                txtUnitPrefix.Enabled = false;
                cbUnitNoFormat.Enabled = false;
            }
        }
    }
}

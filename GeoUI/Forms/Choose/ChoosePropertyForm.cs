﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChoosePropertyForm : ChooseFeatureForm
    {
        private List<GProperty> _properties;

        public ChoosePropertyForm(List<GProperty> properties)
        {
            InitializeComponent();
            _properties = properties;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Property");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Construction Status");
            lvList.Columns.Add("Type");
            lvList.Columns.Add("Lot");
            lvList.Columns.Add("House");
            lvList.Columns.Add("Year Install");
            lvList.Columns.Add("Number of Floor");
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GProperty property in _properties)
            {
                // noraini ali - add in list Property AND 
                GPropertyAND propertyAND = property.GetPropertyANDId();
                if (propertyAND != null)
                {
                    ListViewItem item = CreateItem(propertyAND);
                    items.Add(item);
                }
                else
                {
                    ListViewItem item = CreateItem(property);
                    items.Add(item);
                }
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GProperty property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(property.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add(property.Lot);
            item.SubItems.Add(property.House);
            item.SubItems.Add((property.YearInstall.HasValue) ? property.YearInstall.Value.ToString() : string.Empty);
            item.SubItems.Add((property.NumberOfFloor.HasValue) ? property.NumberOfFloor.Value.ToString() : string.Empty);

            GStreet street = property.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(property.CreatedBy);
            item.SubItems.Add(property.DateCreated);
            item.SubItems.Add(property.UpdatedBy);
            item.SubItems.Add(property.DateUpdated);
            item.Tag = property;
            return item;
        }

        private ListViewItem CreateItem(GPropertyAND property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(property.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add(property.Lot);
            item.SubItems.Add(property.House);
            item.SubItems.Add((property.YearInstall.HasValue) ? property.YearInstall.Value.ToString() : string.Empty);
            item.SubItems.Add((property.NumberOfFloor.HasValue) ? property.NumberOfFloor.Value.ToString() : string.Empty);

            GStreet street = property.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(property.CreatedBy);
            item.SubItems.Add(property.DateCreated);
            item.SubItems.Add(property.UpdatedBy);
            item.SubItems.Add(property.DateUpdated);
            item.Tag = property;
            return item;
        }
    }
}

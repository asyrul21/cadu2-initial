﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseMultipleForm : ChooseFeatureForm
    {
        public virtual IEnumerable<IGFeature> SelectedFeatures
        {
            get
            {
                List<IGFeature> list = new List<IGFeature>();
                foreach (ListViewItem item in lvList.SelectedItems)
                {
                    list.Add((IGFeature)item.Tag);
                }
                return list;
            }
        }

        public ChooseMultipleForm()
        {
            InitializeComponent();
        }

        protected override bool ValidateValues()
        {
            return lvList.SelectedItems.Count > 0;
        }
    }
}

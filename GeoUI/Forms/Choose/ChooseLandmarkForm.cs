﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseLandmarkForm : ChooseFeatureForm
    {
        private List<GLandmark> _landmarks;

        public ChooseLandmarkForm(List<GLandmark> landmarks)
        {
            InitializeComponent();
            _landmarks = landmarks;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Landmark");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Code");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Name2");
            lvList.Columns.Add("Navigation Status");
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GLandmark landmark in _landmarks)
            {
                // noraini ali - add in list Landmark AND 
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                if (landmarkAND != null)
                {
                    ListViewItem item = CreateItem(landmarkAND);
                    items.Add(item);
                }
                else
                {
                    ListViewItem item = CreateItem(landmark);
                    items.Add(item);
                }
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.Code));
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");

            GStreet street = landmark.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }

        private ListViewItem CreateItem(GLandmarkAND landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.Code));
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");

            GStreet street = landmark.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }
    }
}

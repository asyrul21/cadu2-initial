﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChoosePoiCodeForm : ChooseTreeForm
    {
        public virtual GCode3 SelectedCode
        {
            get
            {
                return (tvResult.SelectedNode == null) ? null : tvResult.SelectedNode.Tag as GCode3; ;
            }
        }

        public ChoosePoiCodeForm()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose POI Code");

            foreach (GCode1 code1 in GCode1.GetAll(false))
            {
                TreeNode node = new TreeNode(code1.Description);
                node.Tag = code1;
                int index = tvResult.Nodes.Add(node);
                PopulateNode1(code1, index);
            }
        }

        protected override void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                tvResult.BeginUpdate();
                using (new WaitCursor())
                {
                    string searchTerm = txtSearchTerm.Text;
                    RepositoryFactory repo = new RepositoryFactory();

                    Query<GCode3> query = new Query<GCode3>();
                    query.Operator = Operator.OR;
                    query.IsCaseSensitive = false;
                    query.MatchType = MatchType.Contains;
                    query.Obj.CodeName = searchTerm;
                    query.Obj.Description = searchTerm;
                    query.Obj.Code1 = searchTerm;
                    query.Obj.Code2 = searchTerm;
                    query.Obj.Code3 = searchTerm;

                    foreach (TreeNode node in tvResult.Nodes)
                    {
                        ClearNode(node);
                    }

                    foreach (GCode3 code3 in repo.Search(query))
                    {
                        foreach (TreeNode node in tvResult.Nodes)
                        {
                            FindNode(node, code3);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                tvResult.EndUpdate();
            }
        }

        private void FindNode(TreeNode node, GCode3 code3)
        {
            if (node.Tag is GCode3)
            {
                if (((GCode3)node.Tag).Equals(code3))
                {
                    ExpandNode(node);
                    node.BackColor = Color.Yellow;
                }
            }

            foreach (TreeNode node1 in node.Nodes)
            {
                FindNode(node1, code3);
            }
        }

        private void PopulateNode1(GCode1 code, int index)
        {
            foreach (GCode2 code2 in code.GetCode2s(false))
            {
                TreeNode node = new TreeNode(code2.Description);
                node.Tag = code2;
                int index2 = tvResult.Nodes[index].Nodes.Add(node);
                PopulateNode2(code2, index, index2);
            }
        }

        private void PopulateNode2(GCode2 code2, int index1, int index2)
        {
            foreach (GCode3 code3 in code2.GetCode3s(false))
            {
                TreeNode node = new TreeNode(string.Format("{0} ( {1} )", code3.Description, code3.GetCodes()));
                node.Tag = code3;
                node.ForeColor = code3.IsStandardName ? Color.Blue : Color.Black;
                tvResult.Nodes[index1].Nodes[index2].Nodes.Add(node);
            }
        }

        protected override bool ValidateValues()
        {
            if (tvResult.SelectedNode == null)
                return false;
            if (tvResult.SelectedNode.Tag == null)
                return false;
            if (tvResult.SelectedNode.Level != 2)
                return false;

            return true;
        }
    }
}

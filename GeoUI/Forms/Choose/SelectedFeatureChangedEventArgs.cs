﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Choose
{
    public class SelectedFeatureChangedEventArgs : EventArgs
    {
        public readonly IGFeature Feature;

        public SelectedFeatureChangedEventArgs(IGFeature feature)
        {
            this.Feature = feature;
        }
    }
}

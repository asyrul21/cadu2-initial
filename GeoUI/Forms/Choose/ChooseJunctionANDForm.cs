﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseJunctionANDForm : ChooseFeatureForm
    {
        private List<GJunctionAND> _junctionsAND;

        public ChooseJunctionANDForm(List<GJunctionAND> junctions)
        {
            InitializeComponent();
            _junctionsAND = junctions;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Junction AND");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Type");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GJunctionAND junction in _junctionsAND)
            {
                ListViewItem item = CreateItem(junction);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GJunctionAND junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }
    }
}

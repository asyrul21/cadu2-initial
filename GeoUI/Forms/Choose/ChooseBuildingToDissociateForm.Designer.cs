﻿using System;

namespace Geomatic.UI.Forms.Choose
{
    partial class ChooseBuildingToDissociateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // lvList
            // 
            this.lvList.Location = new System.Drawing.Point(12, 33);
            this.lvList.Size = new System.Drawing.Size(360, 388);
            // ChooseBuildingToDissociateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 462);
            this.Name = "ChooseBuildingToDissociateForm";
            this.Text = "ChooseBuildingToDissociateForm";
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lblRecordText, 0);
            this.Controls.SetChildIndex(this.btnAccept, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.lvList, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseJunctionForm : ChooseFeatureForm
    {
        private List<GJunction> _junctions;

        public ChooseJunctionForm(List<GJunction> junctions)
        {
            InitializeComponent();
            _junctions = junctions;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Junction");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Type");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GJunction junction in _junctions)
            {
                // noraini ali - add in list BuildingAND 
                GJunctionAND junctionAND = junction.GetJunctionANDId();
                if (junctionAND != null)
                {
                    ListViewItem item = CreateItem(junctionAND);
                    items.Add(item);
                }
                else
                {
                    ListViewItem item = CreateItem(junction);
                    items.Add(item);
                }
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GJunction junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }

        private ListViewItem CreateItem(GJunctionAND junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }
    }
}

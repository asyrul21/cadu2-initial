﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseSectionBoundaryForm : ChooseFeatureForm
    {
        private List<GSectionBoundary> _sectionBoundaries;

        public ChooseSectionBoundaryForm(List<GSectionBoundary> sectionBoundaries)
        {
            InitializeComponent();
            RefreshTitle("Choose Section Boundary");
            _sectionBoundaries = sectionBoundaries;
        }

        protected override void Form_Load()
        {
            lvList.Columns.Add("Id");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GSectionBoundary sectionBoundary in _sectionBoundaries)
            {
                ListViewItem item = CreateItem(sectionBoundary);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GSectionBoundary sectionBoundary)
        {
            ListViewItem item = new ListViewItem();
            item.Text = sectionBoundary.OID.ToString();
            item.SubItems.Add(sectionBoundary.Name);
            item.SubItems.Add(sectionBoundary.City);
            item.SubItems.Add(sectionBoundary.State);
            item.SubItems.Add(sectionBoundary.CreatedBy);
            item.SubItems.Add(sectionBoundary.DateCreated);
            item.SubItems.Add(sectionBoundary.UpdatedBy);
            item.SubItems.Add(sectionBoundary.DateUpdated);
            item.Tag = sectionBoundary;
            return item;
        }
    }
}

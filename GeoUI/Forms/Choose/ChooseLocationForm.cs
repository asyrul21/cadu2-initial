﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.Search;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.UI.Properties;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseLocationForm : LocationForm2
    {
        public virtual GLocation SelectedLocation
        {
            get
            {
                return (lvResult.SelectedItems.Count > 0) ? (GLocation)lvResult.SelectedItems[0].Tag : null;
            }
        }

        public ChooseLocationForm()
            : this(SegmentName.None)
        {
        }

        public ChooseLocationForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Choose Location");
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private bool ValidateValues()
        {
            return lvResult.SelectedItems.Count == 1;
        }

        private void lvResult_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void ChooseLocationForm_Load(object sender, EventArgs e)
        {
            cbSection.SelectedIndex = Settings.Default.PickedSectionIndex;
            cbCity.SelectedIndex = Settings.Default.PickedCityIndex;
            cbState.SelectedIndex = Settings.Default.PickedStateIndex;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Settings.Default.PickedSectionIndex = cbSection.FindString(cbSection.Text);
            Settings.Default.PickedCityIndex = cbCity.FindString(cbCity.Text);
            Settings.Default.PickedStateIndex = cbState.FindString(cbState.Text);
            
            if (cbSection.Text == "")
            {
                Settings.Default.PickedSectionIndex = -1;
            }
            if (cbCity.Text == "")
            {
                Settings.Default.PickedCityIndex = -1;
            }
            if (cbState.Text == "")
            {
                Settings.Default.PickedStateIndex = -1;
            }

            Settings.Default.Save();
        }
    }
}

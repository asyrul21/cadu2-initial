﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseRowForm : ChooseForm
    {
        public virtual IGRow SuggestedRow { set; get; }

        public virtual IGRow SelectedRow
        {
            get
            {
                return (lvList.SelectedItems.Count > 0) ? (IGRow)lvList.SelectedItems[0].Tag : null;
            }
        }

        public ChooseRowForm()
        {
            InitializeComponent();
        }

        private void ChooseRowForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            try
            {
                using (new WaitCursor())
                {
                    AcceptButton = btnAccept;
                    lvList.Sorting = SortOrder.None;
                    lvList.BeginUpdate();
                    Form_Load();
                    if (SuggestedRow == null)
                    {
                        return;
                    }
                    foreach (ListViewItem item in lvList.Items)
                    {
                        IGRow row = (IGRow)item.Tag;
                        if (SuggestedRow.OID == row.OID)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                lblRecordText.Text = lvList.Items.Count.ToString();
                lvList.FixColumnWidth();
                lvList.EndUpdate();
            }
        }
    }
}

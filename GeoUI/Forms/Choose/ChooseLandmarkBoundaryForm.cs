﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseLandmarkBoundaryForm : ChooseFeatureForm
    {
        private List<GLandmarkBoundary> _landmarkBoundaries;

        public ChooseLandmarkBoundaryForm(List<GLandmarkBoundary> landmarkBoundaries)
        {
            InitializeComponent();
            _landmarkBoundaries = landmarkBoundaries;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Landmark Boundary");

            lvList.Columns.Add("Id");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GLandmarkBoundary landmarkBoundary in _landmarkBoundaries)
            {
                ListViewItem item = CreateItem(landmarkBoundary);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GLandmarkBoundary landmarkBoundary)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmarkBoundary.OID.ToString();
            item.Tag = landmarkBoundary;
            return item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChoosePoiForm : ChooseFeatureForm
    {
        private List<GPoi> _pois;

        public ChoosePoiForm(List<GPoi> pois)
        {
            InitializeComponent();
            _pois = pois;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Poi");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Code");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Name2");
            lvList.Columns.Add("Navi Status");
            lvList.Columns.Add("Url");
            lvList.Columns.Add("Description");          
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _pois)
            {
                ListViewItem item = CreateItem(poi);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(poi.Code));
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add((poi.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(poi.Url);
            item.SubItems.Add(poi.Description);
            
            GStreet street = poi.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(poi.CreatedBy);
            item.SubItems.Add(poi.DateCreated);
            item.SubItems.Add(poi.UpdatedBy);
            item.SubItems.Add(poi.DateUpdated);

            item.Tag = poi;
            return item;
        }
    }
}

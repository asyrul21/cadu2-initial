﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseBuildingForm : ChooseFeatureForm
    {
        private List<GBuilding> _buildings;
        private GBuilding buildings;

        public ChooseBuildingForm(List<GBuilding> buildings)
        {
            InitializeComponent();
            _buildings = buildings;
        }

        public ChooseBuildingForm(GBuilding buildings)
        {
            this.buildings = buildings;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Building");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Construction Status");
            lvList.Columns.Add("Code");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Name2");
            lvList.Columns.Add("Property Type");
            lvList.Columns.Add("Lot");
            lvList.Columns.Add("House");
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GBuilding building in _buildings)
            {
                // noraini ali - add in list BuildingAND 
                GBuilding buildingAND = building.GetBuildingANDId();
                if (buildingAND != null)
                {
                    ListViewItem item = CreateItem(buildingAND);
                    items.Add(item);
                }
                else
                {
                    ListViewItem item = CreateItem(building);
                    items.Add(item);
                }
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private ListViewItem CreateItem(GBuildingAND building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseBuildingGroupANDForm : ChooseFeatureForm
    {
        private List<GBuildingGroupAND> _buildingGroupsAND;

        public ChooseBuildingGroupANDForm(List<GBuildingGroupAND> buildingGroups)
        {
            InitializeComponent();
            _buildingGroupsAND = buildingGroups;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Building Group AND");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Code");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Navigation Status");
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GBuildingGroupAND bGroup in _buildingGroupsAND)
            {
                ListViewItem item = CreateItem(bGroup);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GBuildingGroupAND buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add(buildingGroup.Code);
            item.SubItems.Add(buildingGroup.Name);
            item.SubItems.Add((buildingGroup.IsNavigationReady) ? "Ready" : "Not Ready");

            GStreet street = buildingGroup.GetStreet();
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);

            item.SubItems.Add(buildingGroup.CreatedBy);
            item.SubItems.Add(buildingGroup.DateCreated);
            item.SubItems.Add(buildingGroup.UpdatedBy);
            item.SubItems.Add(buildingGroup.DateUpdated);
            item.Tag = buildingGroup;
            return item;
        }
    }
}

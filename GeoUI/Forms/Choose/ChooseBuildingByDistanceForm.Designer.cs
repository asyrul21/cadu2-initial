﻿using System;

namespace Geomatic.UI.Forms.Choose
{
    partial class ChooseBuildingByDistanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbDistance = new System.Windows.Forms.ComboBox();
            this.lblDistance = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lvList
            // 
            this.lvList.Location = new System.Drawing.Point(12, 33);
            this.lvList.Size = new System.Drawing.Size(360, 388);
            // 
            // cbDistance
            // 
            this.cbDistance.FormattingEnabled = true;
            this.cbDistance.Location = new System.Drawing.Point(110, 6);
            this.cbDistance.Name = "cbDistance";
            this.cbDistance.Size = new System.Drawing.Size(121, 21);
            this.cbDistance.TabIndex = 3;
            this.cbDistance.SelectedIndexChanged += new System.EventHandler(this.cbDistance_SelectedIndexChanged);
            this.cbDistance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbDistance_KeyDown);
            this.cbDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbDistance_KeyPress);
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Location = new System.Drawing.Point(12, 9);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(92, 13);
            this.lblDistance.TabIndex = 4;
            this.lblDistance.Text = "Max Distance (m):";
            // 
            // ChooseBuildingByDistanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 462);
            this.Controls.Add(this.lblDistance);
            this.Controls.Add(this.cbDistance);
            this.Name = "ChooseBuildingByDistanceForm";
            this.Text = "ChooseBuildingByDistanceForm";
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lblRecordText, 0);
            this.Controls.SetChildIndex(this.btnAccept, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.lvList, 0);
            this.Controls.SetChildIndex(this.cbDistance, 0);
            this.Controls.SetChildIndex(this.lblDistance, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbDistance;
        private System.Windows.Forms.Label lblDistance;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseTreeForm : BaseForm
    {
        public ChooseTreeForm()
        {
            InitializeComponent();
        }

        protected void ChooseTreeForm_Load(object sender, EventArgs e)
        {
            if (DesignMode) { return; }
            try
            {
                tvResult.BeginUpdate();
                using (new WaitCursor())
                {
                    Form_Load();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                tvResult.EndUpdate();
            }
        }

        protected virtual void Form_Load()
        {
        }

        protected virtual void btnSearch_Click(object sender, EventArgs e)
        {

        }

        protected void ClearNode(TreeNode node)
        {
            node.BackColor = Color.White;
            foreach (TreeNode node1 in node.Nodes)
            {
                ClearNode(node1);
            }
        }

        protected void ExpandNode(TreeNode node)
        {
            if (node.Parent != null && !node.Parent.IsExpanded)
            {
                node.Parent.Expand();
            }

            if (node.Parent != null)
            {
                ExpandNode(node.Parent);
            }
        }

        protected void menuExpandAll_Click(object sender, EventArgs e)
        {
            tvResult.BeginUpdate();
            tvResult.ExpandAll();
            tvResult.EndUpdate();
        }

        protected void menuCollapseAll_Click(object sender, EventArgs e)
        {
            tvResult.BeginUpdate();
            tvResult.CollapseAll();
            tvResult.EndUpdate();
        }

        protected void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected void tvResult_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnOk;
        }

        protected void tvResult_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                menu.Show(tvResult, e.X, e.Y);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (ValidateValues())
                DialogResult = DialogResult.OK;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual bool ValidateValues()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseFloorForm : ChooseRowForm
    {
        private List<GFloor> _floors;

        public ChooseFloorForm(List<GFloor> floors)
        {
            InitializeComponent();
            _floors = floors;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Floor");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Number");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GFloor floor in _floors)
            {
                ListViewItem item = CreateItem(floor);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GFloor floor)
        {
            ListViewItem item = new ListViewItem();
            item.Text = floor.OID.ToString();
            item.SubItems.Add(floor.Number);

            item.SubItems.Add(floor.CreatedBy);
            item.SubItems.Add(floor.DateCreated);
            item.SubItems.Add(floor.UpdatedBy);
            item.SubItems.Add(floor.DateUpdated);
            item.Tag = floor;
            return item;
        }
    }
}

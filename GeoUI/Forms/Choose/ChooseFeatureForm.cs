﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseFeatureForm : ChooseForm
    {
        public event EventHandler<SelectedFeatureChangedEventArgs> SelectedFeatureChanged;

        public virtual IGFeature SuggestedFeature { set; get; }

        public virtual IGFeature SelectedFeature
        {
            get
            {
                return (lvList.SelectedItems.Count > 0) ? (IGFeature)lvList.SelectedItems[0].Tag : null;
            }
        }

        public ChooseFeatureForm()
        {
            InitializeComponent();
        }

        private void ChooseFeatureForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            try
            {
                using (new WaitCursor())
                {
                    AcceptButton = btnAccept;
                    lvList.Sorting = SortOrder.None;
                    lvList.BeginUpdate();
                    Form_Load();
                    if (SuggestedFeature == null)
                    {
                        return;
                    }
                    foreach (ListViewItem item in lvList.Items)
                    {
                        IGFeature feature = (IGFeature)item.Tag;
                        if (SuggestedFeature.OID == feature.OID)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                lblRecordText.Text = lvList.Items.Count.ToString();
                lvList.FixColumnWidth();
                lvList.EndUpdate();
            }
        }

        private void lvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvList.SelectedItems.Count == 0)
            {
                OnSelectedFeatureChanged(new SelectedFeatureChangedEventArgs(null));
            }
            else
            {
                IGFeature feature = (IGFeature)lvList.SelectedItems[0].Tag;
                OnSelectedFeatureChanged(new SelectedFeatureChangedEventArgs(feature));
            }
        }

        private void OnSelectedFeatureChanged(SelectedFeatureChangedEventArgs e)
        {
            if (SelectedFeatureChanged != null)
            {
                SelectedFeatureChanged(this, e);
            }
        }
    }
}

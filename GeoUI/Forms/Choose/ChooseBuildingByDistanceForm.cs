﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Earthworm.AO;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI.FeedBacks;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseBuildingByDistanceForm : ChooseMultipleForm
    {
        private GBuildingGroup _buldingGroup;
        private int _defaultMaxDistance = 500;

        private int? SelectedDistance
        {
            get
            {
                int testInt;
                return (cbDistance.SelectedItem == null) ? (int.TryParse(cbDistance.Text, out testInt)) ? testInt : (int?)null : (int)cbDistance.SelectedItem;
                //return _defaultMaxDistance;
            }
        }

        public ChooseBuildingByDistanceForm(GBuildingGroup buildingGroup)
        {
            InitializeComponent();
            _buldingGroup = buildingGroup;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Building");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Construction Status");
            lvList.Columns.Add("Code");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Name2");
            lvList.Columns.Add("Distance");
            lvList.Columns.Add("Lot");
            lvList.Columns.Add("House");
            lvList.Columns.Add("Street Type");
            lvList.Columns.Add("Street Name");
            lvList.Columns.Add("Street Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            PopulateDistance();
            PopulateBuildings();
            cbDistance.Text = this._defaultMaxDistance.ToString();
        }

        private void PopulateBuildings()
        {
            lvList.Items.Clear();
            using (new WaitCursor())
            {
                //MessageBox.Show("Selected Distance: " + SelectedDistance);
                if (!SelectedDistance.HasValue)
                {
                    return;
                }
                List<ListViewItem> items = new List<ListViewItem>();

                RepositoryFactory repo = new RepositoryFactory(_buldingGroup.SegmentName);
                Query<GBuilding> query = new Query<GBuilding>(_buldingGroup.SegmentName);
                query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
                IPoint point = _buldingGroup.Point;
                query.Geometry = point.Buffer(SelectedDistance.Value);

                foreach (GBuilding building in repo.SpatialSearch<GBuilding>(query))
                {
                    double distance = building.Shape.DistanceTo(point);

                    // noraini ali - Sept 2020 - only list within Work Area
                    if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                    {
                        GBuildingAND buildingAND = building.GetBuildingANDId();
                        if (buildingAND != null)
                        {
                            if (buildingAND.HasGroup() || buildingAND.AndStatus == 2)
                            {
                                continue;
                            }

                            if (buildingAND.AreaId == _buldingGroup.AreaId) 
                            {
                                ListViewItem item = CreateItem(buildingAND, distance);
                                items.Add(item);
                            }
                        }
                        else
                        {
                            if (building.HasGroup() || building.AndStatus == 2)
                            {
                                continue;
                            }

                            if (building.AreaId == _buldingGroup.AreaId) 
                            {
                                ListViewItem item = CreateItem(building, distance);
                                items.Add(item);
                            }
                        }
                    }
                    // end 
                    else  // non user AND
                    {
                        if (building.HasGroup())
                        {
                            continue;
                        }

                        ListViewItem item = CreateItem(building, distance);
                        items.Add(item);
                    }
                }
                lvList.Items.AddRange(items.ToArray());
                lvList.FixColumnWidth();
                lblRecordText.Text = lvList.Items.Count.ToString();
            }
        }

        private void PopulateDistance()
        {
            cbDistance.Items.Add(5000);
            cbDistance.Items.Add(2500);
            cbDistance.Items.Add(1000);
            cbDistance.Items.Add(500);
            cbDistance.Items.Add(400);
            cbDistance.Items.Add(300);
            cbDistance.Items.Add(200);
            cbDistance.Items.Add(100);
            cbDistance.Items.Add(50);
            cbDistance.Items.Add(25);
            cbDistance.Items.Add(10);
        }

        private void cbDistance_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateBuildings();
        }

        private void cbDistance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                PopulateBuildings();
            }
        }

        private void cbDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                PopulateBuildings();
            }
        }

        private ListViewItem CreateItem(GBuilding building, double distance)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(distance.ToString());

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private ListViewItem CreateItem(GBuildingAND building, double distance)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(distance.ToString());

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }
    }
}

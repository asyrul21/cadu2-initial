﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Choose
{
    public static class ChooseFormFactory
    {
        private static Dictionary<Type, Type> _library;

        static ChooseFormFactory()
        {
            _library = new Dictionary<Type, Type>();
            Register<GBuilding, ChooseBuildingForm>();
            Register<GBuildingGroup, ChooseBuildingGroupForm>();
            Register<GStreet, ChooseStreetForm>();
            Register<GStreetRestriction, ChooseStreetRestrictionForm>();
            Register<GJunction, ChooseJunctionForm>();
            Register<GProperty, ChoosePropertyForm>();
            Register<GPoi, ChoosePoiForm>();
            Register<GLandmark, ChooseLandmarkForm>();
            Register<GLandmarkBoundary, ChooseLandmarkBoundaryForm>();
            Register<GSectionBoundary, ChooseSectionBoundaryForm>();

            // added by noraini ali - Mei 2020
            //Register<GPropertyAND, ChoosePropertyANDForm>();
            //Register<GLandmarkAND, ChooseLandmarkANDForm>();
            //Register<GBuildingAND, ChooseBuildingANDForm>();
            //Register<GJunctionAND, ChooseJunctionANDForm>();
            //Register<GBuildingGroupAND, ChooseBuildingGroupANDForm>();
            Register<GStreetAND, ChooseStreetANDForm>();
            // end
        }

        private static void Register<TKey, TValue>()
            where TKey : IGFeature
            where TValue : ChooseFeatureForm
        {
            if (_library.ContainsKey(typeof(TKey)))
            {
                throw new Exception(string.Format("Duplicate key detected. {0}", typeof(TKey)));
            }
            _library.Add(typeof(TKey), typeof(TValue));
        }

        public static ChooseFeatureForm CreateForm<T>(List<T> features) where T : IGFeature
        {
            if (!_library.ContainsKey(typeof(T)))
            {
                throw new Exception(string.Format("Unknown persistence object type. {0}", typeof(T)));
            }

            Type TValue;
            _library.TryGetValue(typeof(T), out TValue);

            return (ChooseFeatureForm)Activator.CreateInstance(TValue, features);
        }
    }
}

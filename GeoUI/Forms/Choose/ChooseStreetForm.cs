﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseStreetForm : ChooseFeatureForm
    {
        private List<GStreet> _streets;

        public ChooseStreetForm(List<GStreet> streets)
        {
            InitializeComponent();
            _streets = streets;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Street");

            lvList.Columns.Add("Id");
            lvList.Columns.Add("Navigation Status");
            lvList.Columns.Add("Type");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Name2");
            lvList.Columns.Add("Section");
            lvList.Columns.Add("Postcode");
            lvList.Columns.Add("City");
            lvList.Columns.Add("State");
            lvList.Columns.Add("Speed Limit");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");
            lvList.Columns.Add("Updated By");
            lvList.Columns.Add("Date Updated");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GStreet street in _streets)
            {
                // noraini ali - add in list streetAND 
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null)
                {
                    ListViewItem item = CreateItem(streetAND);
                    items.Add(item);
                }
                else
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add((street.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);
            item.SubItems.Add((street.SpeedLimit.HasValue) ? street.SpeedLimit.ToString() : string.Empty);
            item.SubItems.Add(street.CreatedBy);
            item.SubItems.Add(street.DateCreated);
            item.SubItems.Add(street.UpdatedBy);
            item.SubItems.Add(street.DateUpdated);
            item.Tag = street;
            return item;
        }

        private ListViewItem CreateItem(GStreetAND street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OriId.ToString();
            item.SubItems.Add((street.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.State);
            item.SubItems.Add((street.SpeedLimit.HasValue) ? street.SpeedLimit.ToString() : string.Empty);
            item.SubItems.Add(street.CreatedBy);
            item.SubItems.Add(street.DateCreated);
            item.SubItems.Add(street.UpdatedBy);
            item.SubItems.Add(street.DateUpdated);
            item.Tag = street;
            return item;
        }
    }
}

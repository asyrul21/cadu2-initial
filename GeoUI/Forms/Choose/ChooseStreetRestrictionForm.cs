﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseStreetRestrictionForm : ChooseFeatureForm
    {
        private List<GStreetRestriction> _streetRestrictions;

        public ChooseStreetRestrictionForm(List<GStreetRestriction> streetRestrictions)
        {
            InitializeComponent();
            RefreshTitle("Choose Street Restriction");
            _streetRestrictions = streetRestrictions;
        }

        protected override void Form_Load()
        {
            lvList.Columns.Add("Id");
            lvList.Columns.Add("Status");
            lvList.Columns.Add("Navigation Status");
            lvList.Columns.Add("Junction Id");
            lvList.Columns.Add("Start Id");
            lvList.Columns.Add("End Id 1");
            lvList.Columns.Add("End Id 2");
            lvList.Columns.Add("End Id 3");
            lvList.Columns.Add("End Id 4");
            lvList.Columns.Add("End Id 5");
            lvList.Columns.Add("End Id 6");
            lvList.Columns.Add("End Id 7");
            lvList.Columns.Add("End Id 8");
            lvList.Columns.Add("End Id 9");
            lvList.Columns.Add("End Id 10");
            lvList.Columns.Add("Created By");
            lvList.Columns.Add("Date Created");

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GStreetRestriction streetRestriction in _streetRestrictions)
            {
                ListViewItem item = CreateItem(streetRestriction);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GStreetRestriction streetRestriction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = streetRestriction.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(streetRestriction.StatusValue));
            item.SubItems.Add((streetRestriction.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(streetRestriction.JunctionId.HasValue ? streetRestriction.JunctionId.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.StartId.HasValue ? streetRestriction.StartId.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId1.HasValue ? streetRestriction.EndId1.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId2.HasValue ? streetRestriction.EndId2.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId3.HasValue ? streetRestriction.EndId3.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId4.HasValue ? streetRestriction.EndId4.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId5.HasValue ? streetRestriction.EndId5.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId6.HasValue ? streetRestriction.EndId6.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId7.HasValue ? streetRestriction.EndId7.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId8.HasValue ? streetRestriction.EndId8.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId9.HasValue ? streetRestriction.EndId9.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.EndId10.HasValue ? streetRestriction.EndId10.ToString() : string.Empty);
            item.SubItems.Add(streetRestriction.CreatedBy);
            item.SubItems.Add(streetRestriction.DateCreated);
            item.Tag = streetRestriction;
            return item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseUserForm : BaseForm
    {
        public virtual GUser SelectedUser
        {
            get
            {
                return (lvList.SelectedItems.Count > 0) ? (GUser)lvList.SelectedItems[0].Tag : null;
            }
        }

        public ChooseUserForm()
        {
            InitializeComponent();
        }

        private void ChooseUserForm_Load(object sender, EventArgs e)
        {
            RefreshTitle("Choose User");
            lvList.Columns.Add("Name");
            lvList.Columns.Add("Group");

            using (new WaitCursor())
            {
                PopulateUsers();
            }
        }

        private void PopulateUsers()
        {
            lvList.BeginUpdate();
            lvList.Sorting = SortOrder.None;
            lvList.Items.Clear();
            List<ListViewItem> items = new List<ListViewItem>();
            foreach (GUser user in GUser.GetAll())
            {
                ListViewItem item = CreateItem(user);
                items.Add(item);
            }
            lvList.Items.AddRange(items.ToArray());
            lvList.FixColumnWidth();
            lvList.EndUpdate();
        }

        private ListViewItem CreateItem(GUser user)
        {
            ListViewItem item = new ListViewItem();
            item.Text = user.Name;
            GUserGroup userGroup = user.GetGroup();
            item.SubItems.Add((userGroup == null) ? string.Empty : userGroup.Name);
            item.Tag = user;
            return item;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual bool ValidateValues()
        {
            return lvList.SelectedItems.Count == 1;
        }
    }
}

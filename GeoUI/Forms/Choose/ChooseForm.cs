﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseForm : BaseForm
    {
        public ChooseForm()
        {
            InitializeComponent();
        }

        private void ChooseForm_Shown(object sender, EventArgs e)
        {
            if (!lvList.Focused)
            {
                lvList.Focus();
            }
        }

        protected virtual void Form_Load()
        {
            throw new NotImplementedException("Not implemented.");
        }

        protected virtual bool ValidateValues()
        {
            return lvList.SelectedItems.Count == 1;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

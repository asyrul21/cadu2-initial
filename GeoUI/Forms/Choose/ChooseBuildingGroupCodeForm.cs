﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using System.Windows.Forms;
using System.Drawing;
using Geomatic.Core.Search;


namespace Geomatic.UI.Forms.Choose
{
    public partial class ChooseBuildingGroupCodeForm : ChoosePoiCodeForm
    {
        IEnumerable<GCode1> code1List;
        IEnumerable<GCode2> code2List;
        IEnumerable<GCode3> code3List;

        public ChooseBuildingGroupCodeForm()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            RefreshTitle("Choose Building Group Code");

            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode3> query = new Query<GCode3>();
            query.Obj.BuildingGroupFlag = 1;

            code1List = repo.Search<GCode1>();
            code2List = repo.Search<GCode2>();
            code3List = repo.Search(query);

            foreach (GCode1 code1 in code1List)
            {
                if (isBGroupCode(code1))
                {
                    TreeNode newNode = new TreeNode(code1.Description);
                    newNode.Tag = code1;
                    tvResult.Nodes.Add(newNode);

                    int index = tvResult.Nodes.Count;
                    if (index > 0)
                        PopulateTreeNode1(code1, index);
                }
            }
        }

        private void PopulateTreeNode1(GCode1 code, int index)
        {
            foreach (GCode2 code2 in code.GetCode2s())
            {
                if (isBGroupCode(code2))
                {
                    TreeNode newNode = new TreeNode(code2.Description);
                    newNode.Tag = code2;
                    tvResult.Nodes[index - 1].Nodes.Add(newNode);

                    int index2 = tvResult.Nodes[index - 1].Nodes.Count;
                    if (index2 > 0)
                        PopulateTreeNode2(code2, index, index2);
                }
            }
        }

        private void PopulateTreeNode2(GCode2 code2, int index1, int index2)
        {
            foreach (GCode3 code3 in code2.GetCode3s())
            {
                if (isBGroupCode(code3))
                {
                    TreeNode newNode = new TreeNode(string.Format("{0} ({1})", code3.Description, code3.GetCodes()));
                    newNode.ForeColor = code3.IsStandardName ? Color.DarkCyan : Color.RosyBrown;
                    newNode.Tag = code3;
                    tvResult.Nodes[index1 - 1].Nodes[index2 - 1].Nodes.Add(newNode);
                }
            }
        }

        private bool isBGroupCode(GCode1 code1)
        {
            foreach (GCode3 code3 in code3List)
            {
                if (code1.Code1 == code3.Code1)
                    return true;
            }

            return false;
        }

        private bool isBGroupCode(GCode2 code2)
        {
            foreach (GCode3 code3 in code3List)
            {
                if (code2.Code2 == code3.Code2)
                    return true;
            }

            return false;
        }

        private bool isBGroupCode(GCode3 code)
        {
            foreach (GCode3 code3 in code3List)
            {
                if (code.Code3 == code3.Code3)
                    return true;
            }

            return false;
        }

        private GCode1 GetCode1(string code)
        {
            foreach (GCode1 code1 in code1List)
            {
                if (code1.Code1 == code)
                    return code1;
            }
            return null;
        }

        private GCode2 GetCode2(string code)
        {
            foreach (GCode2 code2 in code2List)
            {
                if (code2.Code2 == code)
                    return code2;
            }
            return null;
        }
    }
}

﻿namespace Geomatic.UI.Forms.Documents
{
    partial class MapDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapDocument));
            this.axMapControl = new ESRI.ArcGIS.Controls.AxMapControl();
            this.stepStrip = new System.Windows.Forms.StatusStrip();
            this.lblStepText = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.axMapControl)).BeginInit();
            this.stepStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // axMapControl
            // 
            this.axMapControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axMapControl.Location = new System.Drawing.Point(0, 23);
            this.axMapControl.Name = "axMapControl";
            this.axMapControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMapControl.OcxState")));
            this.axMapControl.Size = new System.Drawing.Size(284, 238);
            this.axMapControl.TabIndex = 0;
            this.axMapControl.OnMouseMove += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnMouseMoveEventHandler(this.axMapControl_OnMouseMove);
            this.axMapControl.OnBeforeScreenDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnBeforeScreenDrawEventHandler(this.axMapControl_OnBeforeScreenDraw);
            this.axMapControl.OnAfterScreenDraw += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnAfterScreenDrawEventHandler(this.axMapControl_OnAfterScreenDraw);
            this.axMapControl.OnExtentUpdated += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnExtentUpdatedEventHandler(this.axMapControl_OnExtentUpdated);
            // 
            // stepStrip
            // 
            this.stepStrip.BackColor = System.Drawing.Color.Gold;
            this.stepStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.stepStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStepText});
            this.stepStrip.Location = new System.Drawing.Point(0, 0);
            this.stepStrip.Name = "stepStrip";
            this.stepStrip.Size = new System.Drawing.Size(284, 22);
            this.stepStrip.SizingGrip = false;
            this.stepStrip.TabIndex = 2;
            this.stepStrip.Text = "stepStrip";
            // 
            // lblStepText
            // 
            this.lblStepText.Name = "lblStepText";
            this.lblStepText.Size = new System.Drawing.Size(0, 17);
            // 
            // MapDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.stepStrip);
            this.Controls.Add(this.axMapControl);
            this.Name = "MapDocument";
            this.Text = "MapDocument";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MapDocument_FormClosed);
            this.Load += new System.EventHandler(this.MapDocument_Load);
            this.Shown += new System.EventHandler(this.MapDocument_Shown);
            this.ResizeBegin += new System.EventHandler(this.MapDocument_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MapDocument_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.axMapControl)).EndInit();
            this.stepStrip.ResumeLayout(false);
            this.stepStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ESRI.ArcGIS.Controls.AxMapControl axMapControl;
        private System.Windows.Forms.StatusStrip stepStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStepText;
    }
}
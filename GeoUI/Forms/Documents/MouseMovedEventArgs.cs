﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.Documents
{
    public class MouseMovedEventArgs : EventArgs
    {
        public double X { private set; get; }
        public double Y { private set; get; }

        public MouseMovedEventArgs(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}

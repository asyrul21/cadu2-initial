﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.Forms.Documents
{
    public class ExtentUpdatedEventArgs : EventArgs
    {
        public IEnvelope Envelope { private set; get; }
        public double MapScale { private set; get; }
        public string MapUnit { private set; get; }

        public ExtentUpdatedEventArgs(IEnvelope envelope, double mapScale, string mapUnit)
        {
            this.Envelope = envelope;
            this.MapScale = mapScale;
            this.MapUnit = mapUnit;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.Display;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.UI.Utilities.Elements;
using Geomatic.UI.Utilities.Symbols;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Views.LayerViews;
using ESRI.ArcGIS.SampleTools;
using ESRI.ArcGIS.Geoprocessor;
using Geomatic.UI.FeedBacks;
using ESRI.ArcGIS.DataSourcesRaster;
using Point = ESRI.ArcGIS.Geometry.Point;
using ESRI.ArcGIS.DataManagementTools;
using System.IO;
using System.IO.Compression;
using Ionic.Zip;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms.Documents
{
    public partial class MapDocument : Document
    {
        private const double SCALE = 1000;

        public event EventHandler<MouseMovedEventArgs> MouseMoved;
        public event EventHandler<ScreenDrawnEventArgs> ScreenDrawn;
        public event EventHandler<ExtentUpdatedEventArgs> ExtendUpdated;
        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private WaitCursor _waitCursor;
        private SelectionLayer _selectionLayer;
        private IGeometry _drawingShape;
        private IElement _markerElement;

		// initialization
		private IPointCollection m_pSourceCollection;
		private IPointCollection m_pTargetCollection;
		private IElementCollection m_pSourceCollElement;
		private IElementCollection m_pTargetCollElement;
		private bool m_bTakePointUse1;
		private bool m_bTakePointUse2;
		private IRasterLayer m_pCurrentRasterLayer;
		private double rasterDx;
		private double rasterDy;
		private bool m_bTakeSource;
		private bool m_bTakeTarget;
		private string rasterPath;
		private string rasterFullPath;
		private string rasterFile;
		private string rasterName;
		private string WOMSDirectory = @"Z:\USERS\SCAN\";

		private SelectionLayer SelectionLayer
        {
            get
            {
                if (_selectionLayer == null)
                {
                    _selectionLayer = new SelectionLayer();
                    _selectionLayer.LayerSelectionChanged += new EventHandler<SelectionChangedEventArgs>(SelectionLayer_SelectionChanged);

                    //Set the selectionLayer label as low priority to avoid conflict feature class label
                    IBarrierProperties docBarrierProperties;

                    docBarrierProperties = _selectionLayer as IBarrierProperties;
                    docBarrierProperties.Weight = (int)esriBasicOverposterWeight.esriNoWeight;

                    AddLayer(_selectionLayer);
                }
                return _selectionLayer;
            }
        }

        public IMapControl4 MapControl
        {
            get { return axMapControl.Object as IMapControl4; }
        }

        public IMap Map
        {
            get { return MapControl.Map; }
        }

        private IActiveView ActiveView
        {
            get { return MapControl.ActiveView; }
        }

        public IEnvelope Extent
        {
            get { return ActiveView.Extent; }
        }

        public bool CanUndoExtent
        {
            get { return ActiveView.ExtentStack.CanUndo(); }
        }

        public bool CanRedoExtent
        {
            get { return ActiveView.ExtentStack.CanRedo(); }
        }

        public int SelectionCount
        {
            get { return SelectionLayer.SelectionCount; }
        }

        public MapDocument()
            : this(string.Empty)
        {
        }

        public MapDocument(string title)
        {
            InitializeComponent();
            RefreshTitle(title);
        }

        private void MapDocument_Load(object sender, EventArgs e)
        {
        }

        private void MapDocument_Shown(object sender, EventArgs e)
        {
            axMapControl_OnExtentUpdated(null, null);
        }

        private void MapDocument_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClearSelection();
        }

        private void MapDocument_ResizeBegin(object sender, EventArgs e)
        {
            this.SuspendLayout();
            MapControl.SuppressResizeDrawing(true, 0);
        }

        private void MapDocument_ResizeEnd(object sender, EventArgs e)
        {
            this.ResumeLayout();
            MapControl.SuppressResizeDrawing(false, 0);
        }

        public void OpenFile(string fileName)
        {
            fileName = System.IO.Path.GetFullPath(fileName);

            if (!MapControl.CheckMxFile(fileName))
                return;

            MapControl.LoadMxFile(fileName, 0, Type.Missing);
            FileName = fileName;
        }

        public void ShowContextMenu(int x, int y)
        {
            ContextMenuStrip.Show(axMapControl, x, y);
        }

        public void ShowContextMenu(ContextMenuStrip contextMenu, int x, int y)
        {
            contextMenu.Show(axMapControl, x, y);
        }

        public void SetMapName(string name)
        {
            Map.Name = name;
        }

        public void SelectFeature(IGFeature feature)
        {
            SelectionLayer.Add(feature);
        }

        public void SelectFeatures(IEnumerable<IGFeature> features)
        {
            SelectFeatures(features, false);
        }

        public void SelectFeatures(IEnumerable<IGFeature> features, bool clear)
        {
            SelectionLayer.Add(features, clear);
        }

        public void SetDrawingShape(IGeometry geometry)
        {
            _drawingShape = geometry;
        }

        public void SetScale(double scale)
        {
            if (MapControl.MapScale != scale)
            {
                MapControl.MapScale = scale;
                ActiveView.Refresh();
            }
        }

        public void SetExtent(IEnvelope envelope)
        {
            ActiveView.Extent = envelope;
            ActiveView.Refresh();
        }

        public void FullExtent()
        {
            SetExtent(MapControl.FullExtent);
        }

        public void UndoExtent()
        {
            if (!CanUndoExtent)
                return;

            ActiveView.ExtentStack.Undo();
            ActiveView.Refresh();
        }

        public void RedoExtent()
        {
            if (!CanRedoExtent)
                return;

            ActiveView.ExtentStack.Redo();
            ActiveView.Refresh();
        }

        public void AddMarker(double x, double y)
        {
            IPoint point = new PointClass();
            point.PutCoords(x, y);
            AddMarker(point);
        }

        public void AddMarker(IPoint point)
        {
            stdole.IFontDisp fontDisp = ((stdole.IFontDisp)(new stdole.StdFont()));
            fontDisp.Name = "ESRI Default Marker";
            fontDisp.Size = 8;

            ICharacterMarkerSymbol characterMarkerSymbol = new CharacterMarkerSymbolClass();
            characterMarkerSymbol.Angle = 0;
            characterMarkerSymbol.CharacterIndex = 68;
            characterMarkerSymbol.Color = ColorUtils.Get(255, 0, 0);
            characterMarkerSymbol.Font = fontDisp;
            characterMarkerSymbol.Size = 32;
            characterMarkerSymbol.XOffset = 0;
            characterMarkerSymbol.YOffset = 0;

            IMarkerElement markerElement = new MarkerElementClass();
            markerElement.Symbol = characterMarkerSymbol;

            IElement element = (IElement)markerElement;
            element.Geometry = point;

            AddMarker(element);
        }

        public void AddMarker(IElement element)
        {
            RemoveMarker();
            _markerElement = element;
            AddGraphic(element);
        }

        public void RemoveMarker()
        {
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)Map;
            if (_markerElement != null)
            {
                graphicsContainer.DeleteElement(_markerElement);
                _markerElement = null;
            }
        }

        public void AddGraphic(IGeometry geometry)
        {
            IElement element = ElementFactory.CreateElement(geometry, ColorUtils.Get(255, 85, 0), ColorUtils.Get(Color.Green));
            element.Geometry = geometry;
            AddGraphic(element);
        }

        public void AddGraphic(IElement element)
        {
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)Map;
            if (element != null)
            {
                graphicsContainer.AddElement(element, 0);
            }
        }

        public void ClearGraphics()
        {
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)Map;
            IElement element;
            graphicsContainer.Reset();
            while ((element = graphicsContainer.Next()) != null)
            {
                if (element == _markerElement)
                {
                    continue;
                }
                graphicsContainer.DeleteElement(element);
            }
        }

        public IArray ReadMx(string fileName)
        {
            IArray array = MapControl.ReadMxMaps(fileName, null);
            if (array.Count == 0)
            {
                return null;
            }
            return (array.Count == 0) ? null : array;
        }

        public void ZoomToPoint(double x, double y)
        {
            IPoint point = new PointClass();
            point.PutCoords(x, y);
            ZoomToPoint(point);
        }

        public void ZoomToPoint(IPoint point)
        {
            IEnvelope envelope = MapUtils.GetExtent(ActiveView, point, SCALE);
            SetExtent(envelope);
        }

        public void PanToPoint(double x, double y)
        {
            IPoint point = new PointClass();
            point.PutCoords(x, y);
            PanToPoint(point);
        }

        public void PanToPoint(IPoint point)
        {
            IEnvelope envelope = ActiveView.Extent;
            envelope.CenterAt(point);
            SetExtent(envelope);
        }

        public void ZoomToFeatures(IEnumerable<IGFeature> features)
        {
            IEnvelope envelope = new EnvelopeClass();
            foreach (IGFeature feature in features)
            {
                envelope.Union(feature.Shape.Envelope);
            }

            // Point
            if (envelope.Width == 0 || envelope.Height == 0)
            {
                double x = envelope.XMin;
                double y = envelope.YMin;

                IPoint point = new PointClass();
                point.PutCoords(x, y);

                envelope = MapUtils.GetExtent(ActiveView, point, SCALE);
            }
            else
            {
                envelope.Expand(1.2, 1.2, true);
            }

            SetExtent(envelope);
        }

        public void ZoomToFeature(IGFeature feature)
        {
            IEnvelope envelope = feature.Shape.Envelope;

            // Point
            if (envelope.Width == 0 || envelope.Height == 0)
            {
                double x = envelope.XMin;
                double y = envelope.YMin;

                IPoint point = new PointClass();
                point.PutCoords(x, y);

                envelope = MapUtils.GetExtent(ActiveView, point, SCALE);
            }
            else
            {
                envelope.Expand(1.2, 1.2, true);
            }

            SetExtent(envelope);
        }

        public void ZoomToSelection()
        {
            if (SelectionCount == 0)
            {
                return;
            }

            IEnvelope envelope = new EnvelopeClass();

            foreach (IGFeature feature in SelectionLayer.Selections)
            {
                envelope.Union(feature.Shape.Envelope);
            }

            // Point
            if (envelope.Width == 0 || envelope.Height == 0)
            {
                double x = envelope.XMin;
                double y = envelope.YMin;

                IPoint point = new PointClass();
                point.PutCoords(x, y);

                envelope = MapUtils.GetExtent(ActiveView, point, SCALE);
            }
            else
            {
                envelope.Expand(1.2, 1.2, true);
            }

            SetExtent(envelope);
        }

        public void AddShape(string path, string file)
        {
            IFeatureClass featureClass = WorkspacePool.Instance.OpenShapeWorkspace(path).OpenFeatureClass(file);
            AddFeatureClass(featureClass);
        }

        public void AddRaster(string path, string file)
        {
            AddRaster(WorkspacePool.Instance.OpenRasterWorkspace(path).OpenRaster(file));
        }

        public void AddRaster(IRasterDataset rasterDataset)
        {
            IRasterLayer layer = new RasterLayerClass();
            layer.Name = rasterDataset.CompleteName;
            layer.CreateFromDataset(rasterDataset);
            AddLayer(layer);

            ILayer newlySelectedLayer = GetLayer(layer.Name);

            using (LayerPropertyForm form = new LayerPropertyForm())
            {
                form.SetLayer(newlySelectedLayer);
                form.ShowDialog(this);
            }
            RefreshMap();
        }

        public void ConvertGPSCadToShp(string path, string file)
        {
            try
            {
                CADtoFeatureClass convertCAD = new CADtoFeatureClass();
                string DGNfile = path + "\\" + file;
                convertCAD.input_CAD_file = (@DGNfile);

                string SHPfile = DGNfile.Replace(".dgn", ".shp");
                convertCAD.output_feature_class = (@SHPfile);

                Geoprocessor GP = new Geoprocessor();
                GP.Execute(convertCAD, null);

                MessageBox.Show("Convert Completed");
            }
            catch
            {
                throw;
            }
        }

        public void AddGdb(string path)
        {
            List<IDataset> datasets = new List<IDataset>();
            using (new WaitCursor())
            {
                foreach (IDataset dataset in WorkspacePool.Instance.OpenFileGdbWorkspace(path).GetDatasets())
                {
                    switch (dataset.Type)
                    {
                        case esriDatasetType.esriDTFeatureDataset:
                            foreach (IDataset innerDataset in ((IFeatureDataset)dataset).GetSubDataset())
                            {
                                switch (innerDataset.Type)
                                {
                                    case esriDatasetType.esriDTFeatureClass:
                                    case esriDatasetType.esriDTRasterDataset:
                                        datasets.Add(dataset);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            break;
                        case esriDatasetType.esriDTFeatureClass:
                        case esriDatasetType.esriDTRasterDataset:
                            datasets.Add(dataset);
                            break;
                        default:
                            break;
                    }
                }
            }

            using (ChooseLayerForm chooseLayerForm = new ChooseLayerForm())
            {
                chooseLayerForm.LoadDataset(datasets);
                if (chooseLayerForm.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    foreach (IDataset dataset in chooseLayerForm.SelectedDatasets.Sort(DatasetSortOrder.Ascending))
                    {
                        AddDataset(dataset);
                    }
                }
            }
        }

        public void AddDataset(IDataset dataset)
        {
            switch (dataset.Type)
            {
                case esriDatasetType.esriDTFeatureClass:
                    AddFeatureClass((IFeatureClass)dataset);
                    break;
                case esriDatasetType.esriDTRasterDataset:
                    AddRaster((IRasterDataset)dataset);
                    break;
                default:
                    break;
            }
        }

        public void AddFeatureClass(IFeatureClass featureClass)
        {
            AddFeatureClass(featureClass, featureClass.AliasName);
        }

        public void AddFeatureClass(IFeatureClass featureClass, string name)
        {
            ISimpleRenderer simpleRenderer = new SimpleRendererClass();
            simpleRenderer.Symbol = SymbolFactory.CreateSymbol(featureClass.ShapeType);
            IFeatureRenderer featureRenderer = (IFeatureRenderer)simpleRenderer;
            AddFeatureClass(featureClass, featureClass.AliasName, featureRenderer);
        }

        public void AddFeatureClass(IFeatureClass featureClass, string name, IFeatureRenderer renderer)
        {
            IGeoFeatureLayer layer = new FeatureLayerClass();
            layer.Name = name;
            layer.FeatureClass = featureClass;
            layer.Renderer = renderer;
            AddLayer(layer);
        }

        public void AddLayers(List<ILayer> layers)
        {
            foreach (ILayer layer in layers)
            {
                AddLayer(layer);
            }
        }

        public void AddLayer(ILayer layer)
        {
            if (IsDisposed)
                return;
            if (layer == null)
                return;
            Map.AddLayer(layer);
        }

        public void DeleteLayer(ILayer layer)
        {
            if (IsDisposed)
                return;
            if (layer == null)
                return;
            Map.DeleteLayer(layer);
        }

        /// <summary>
        /// Get first returned layer.
        /// </summary>
        /// <param name="layerName"></param>
        /// <returns></returns>
        public ILayer GetLayer(string layerName)
        {
            IList<ILayer> layers = GetLayers(layerName).ToList();
            return (layers.Count > 0) ? layers[0] : null;
        }

        public IEnumerable<ILayer> GetLayers(string layerName)
        {
            layerName = layerName.ToLower();
            for (int count = 0; count < Map.LayerCount; count++)
            {
                foreach (ILayer layer in GetLayers(Map.get_Layer(count), layerName))
                {
                    yield return layer;
                }
            }
        }

        private IEnumerable<ILayer> GetLayers(ILayer layer, string layerName)
        {
            if (layer is IGroupLayer)
            {
                ICompositeLayer compositeLayer = (ICompositeLayer)layer;
                for (int count = 0; count < compositeLayer.Count; count++)
                {
                    foreach (ILayer innerLayer in GetLayers(compositeLayer.get_Layer(count), layerName))
                    {
                        yield return innerLayer;
                    }
                }
            }
            else if (layer is IFeatureLayer)
            {
                IFeatureLayer featureLayer = (IFeatureLayer)layer;
                if (featureLayer.FeatureClass != null)
                {
                    IDataset dataset = featureLayer.FeatureClass as IDataset;
                    if (dataset != null)
                    {
                        if (dataset.Name.ToLower() == layerName)
                            yield return layer;
                    }
                }
            }
            else
            {
                if (layer.Name.ToLower() == layerName)
                    yield return layer;
            }
        }

        public IEnumerable<ILayer> GetLayers()
        {
            for (int count = 0; count < Map.LayerCount; count++)
            {
                foreach (ILayer layer in GetLayers(Map.get_Layer(count)))
                {
                    yield return layer;
                }
            }
        }

        private IEnumerable<ILayer> GetLayers(ILayer layer)
        {
            if (layer is IGroupLayer)
            {
                ICompositeLayer compositeLayer = (ICompositeLayer)layer;
                for (int count = 0; count < compositeLayer.Count; count++)
                {
                    foreach (ILayer innerLayer in GetLayers(compositeLayer.get_Layer(count)))
                    {
                        yield return innerLayer;
                    }
                }
            }
            else
            {
                yield return layer;
            }
        }

        public IEnumerable<IStandaloneTable> GetStandaloneTables()
        {
            IStandaloneTableCollection tableCollection = (IStandaloneTableCollection)Map;
            for (int count = 0; count < tableCollection.StandaloneTableCount - 1; count++)
            {
                yield return tableCollection.get_StandaloneTable(count);
            }
        }

        private bool IsLayerInGroup(ILayer layer)
        {
            List<ILayer> layers = new List<ILayer>();
            for (int count = 0; count < Map.LayerCount; count++)
            {
                layers.Add(Map.get_Layer(count));
            }
            if (layers.Contains(layer))
            {
                return false;
            }
            for (int count = 0; count < Map.LayerCount; count++)
            {
                ILayer innerLayer = Map.get_Layer(count);
                if (innerLayer is IGroupLayer)
                {
                    IGroupLayer groupLayer = (IGroupLayer)innerLayer;
                    if (IsLayerInGroup(groupLayer, layer))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsLayerInGroup(IGroupLayer groupLayer, ILayer layer)
        {
            List<ILayer> layers = groupLayer.GetLayers().ToList();
            if (layers.Contains(layer))
            {
                return true;
            }
            foreach (ILayer innerLayer in groupLayer.GetLayers())
            {
                if (innerLayer is IGroupLayer)
                {
                    IGroupLayer innerGroupLayer = (IGroupLayer)innerLayer;
                    if (IsLayerInGroup(innerGroupLayer, layer))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private int GetLayerIndex(ILayer layer)
        {
            if (IsLayerInGroup(layer))
            {
                IGroupLayer groupLayer = GetLayerGroup(layer);
                return groupLayer.GetLayers().ToList().IndexOf(layer);
            }
            else
            {
                List<ILayer> layers = new List<ILayer>();
                for (int count = 0; count < Map.LayerCount; count++)
                {
                    layers.Add(Map.get_Layer(count));
                }
                if (!layers.Contains(layer))
                {
                    throw new Exception("Layer not in map.");
                }
                return layers.IndexOf(layer);
            }
        }

        private IGroupLayer GetLayerGroup(ILayer layer)
        {
            List<ILayer> layers = new List<ILayer>();
            for (int count = 0; count < Map.LayerCount; count++)
            {
                layers.Add(Map.get_Layer(count));
            }
            if (layers.Contains(layer))
            {
                throw new Exception("Layer not in group.");
            }
            for (int count = 0; count < Map.LayerCount; count++)
            {
                ILayer innerLayer = Map.get_Layer(count);
                if (innerLayer is IGroupLayer)
                {
                    IGroupLayer innerGroupLayer = (IGroupLayer)innerLayer;
                    IGroupLayer groupLayer = GetLayerGroup(innerGroupLayer, layer);
                    if (groupLayer != null)
                    {
                        return groupLayer;
                    }
                }
            }
            throw new Exception("Cannot find layer in group.");
        }

        private IGroupLayer GetLayerGroup(IGroupLayer groupLayer, ILayer layer)
        {
            List<ILayer> layers = groupLayer.GetLayers().ToList();
            if (layers.Contains(layer))
            {
                return groupLayer;
            }
            foreach (ILayer innerLayer in groupLayer.GetLayers())
            {
                if (innerLayer is IGroupLayer)
                {
                    IGroupLayer innerGroupLayer = (IGroupLayer)innerLayer;
                    IGroupLayer groupLayer2 = GetLayerGroup(innerGroupLayer, layer);
                    if (groupLayer2 != null)
                    {
                        return groupLayer2;
                    }
                }
            }
            return null;
        }

        public void MoveLayerUp(ILayer layer)
        {
            if (IsLayerInGroup(layer))
            {
                IGroupLayer groupLayer = GetLayerGroup(layer);
                int index = GetLayerIndex(layer);
                if (index == 0)
                {
                    return;
                }
                MoveLayer(groupLayer, layer, index - 1);
            }
            else
            {
                int index = GetLayerIndex(layer);
                if (index == 0)
                {
                    return;
                }
                Map.MoveLayer(layer, index - 1);
            }
        }

        public void MoveLayerDown(ILayer layer)
        {
            if (IsLayerInGroup(layer))
            {
                IGroupLayer groupLayer = GetLayerGroup(layer);
                int index = GetLayerIndex(layer);
                if (index == groupLayer.GetLayerCount() - 1)
                {
                    return;
                }
                MoveLayer(groupLayer, layer, index + 1);
            }
            else
            {
                int index = GetLayerIndex(layer);
                if (index == Map.LayerCount - 1)
                {
                    return;
                }
                Map.MoveLayer(layer, index + 1);
            }
        }

        public void MoveLayerToTop(ILayer layer)
        {
            if (IsLayerInGroup(layer))
            {
                IGroupLayer groupLayer = GetLayerGroup(layer);
                MoveLayer(groupLayer, layer, 0);
            }
            else
            {
                Map.MoveLayer(layer, 0);
            }
        }

        public void MoveLayerToBottom(ILayer layer)
        {
            if (IsLayerInGroup(layer))
            {
                IGroupLayer groupLayer = GetLayerGroup(layer);
                MoveLayer(groupLayer, layer, groupLayer.GetLayerCount() - 1);
            }
            else
            {
                Map.MoveLayer(layer, Map.LayerCount - 1);
            }
        }

        private void MoveLayer(IGroupLayer groupLayer, ILayer layer, int toIndex)
        {
            if (toIndex > groupLayer.GetLayerCount() - 1)
            {
                return;
            }
            List<ILayer> layers = groupLayer.GetLayers().ToList();
            groupLayer.Clear();

            int index = layers.IndexOf(layer);
            ILayer tempLayer = layers[toIndex];
            layers[toIndex] = layer;
            layers[index] = tempLayer;

            for (int count = 0; count < layers.Count; count++)
            {
                groupLayer.Add(layers[count]);
            }
        }

        private void ChangeAllLayersVersion(IWorkspace oldVersion, IWorkspace newVersion)
        {
            IFeatureWorkspace oldWorkspace = (IFeatureWorkspace)oldVersion;
            IFeatureWorkspace newWorkspace = (IFeatureWorkspace)newVersion;

            IMapAdmin2 mapAdmin2 = (IMapAdmin2)Map;

            foreach (ILayer layer in GetLayers())
            {
                if (layer is IFeatureLayer)
                {
                    IFeatureClass oldFeatureClass;
                    IFeatureClass newFeatureClass;
                    IFeatureLayer featureLayer = (IFeatureLayer)layer;
                    if (featureLayer.ChangeWorkspace(oldWorkspace, newWorkspace, out oldFeatureClass, out newFeatureClass))
                    {
                        mapAdmin2.FireChangeFeatureClass(oldFeatureClass, newFeatureClass);
                    }
                }
                if (layer is ITopologyLayer)
                {
                    ITopologyLayer topologyLayer = (ITopologyLayer)layer;
                    topologyLayer.ChangeWorkspace(oldWorkspace, newWorkspace);
                }
            }

            foreach (IStandaloneTable standaloneTable in GetStandaloneTables())
            {
                ITable oldTable;
                ITable newTable;
                if (standaloneTable.ChangeWorkspace(oldWorkspace, newWorkspace, out oldTable, out newTable))
                {
                    mapAdmin2.FireChangeTable(oldTable, newTable);
                }
            }

            mapAdmin2.FireChangeVersion((IVersion)oldWorkspace, (IVersion)newWorkspace);

            RefreshMap();
        }

        public void PartialRefresh(esriViewDrawPhase esriViewDrawPhase, object obj, IEnvelope envelope)
        {
            ActiveView.PartialRefresh(esriViewDrawPhase, obj, envelope);
        }

        /// <summary>
        /// Clear selections
        /// </summary>
        public void ClearSelection()
        {
            SelectionLayer.Clear();
        }

        public void RefreshSelection()
        {
            PartialRefresh(esriViewDrawPhase.esriViewGraphics, SelectionLayer, null);
        }

        public void RefreshMap()
        {
            ActiveView.Refresh();
        }

        private void axMapControl_OnBeforeScreenDraw(object sender, IMapControlEvents2_OnBeforeScreenDrawEvent e)
        {
            if (_waitCursor == null)
            {
                _waitCursor = new WaitCursor();
            }
            OnScreenDrawn(new ScreenDrawnEventArgs(true));
        }

        private void axMapControl_OnAfterScreenDraw(object sender, IMapControlEvents2_OnAfterScreenDrawEvent e)
        {
            if (_waitCursor != null)
            {
                _waitCursor.Dispose();
                _waitCursor = null;
            }
            OnScreenDrawn(new ScreenDrawnEventArgs(false));
        }

        private void axMapControl_OnMouseMove(object sender, IMapControlEvents2_OnMouseMoveEvent e)
        {
            if (MapControl == null)
                return;

            if (e == null)
                return;

            double? x = e.mapX;
            double? y = e.mapY;

            if (!x.HasValue || !y.HasValue)
                return;

            OnMouseMoved(new MouseMovedEventArgs(e.mapX, e.mapY));
        }

        private void axMapControl_OnExtentUpdated(object sender, IMapControlEvents2_OnExtentUpdatedEvent e)
        {
            if (MapControl == null)
                return;

            double? mapScale = MapControl.MapScale;
            string mapUnits = MapControl.MapUnits.ToString();

            mapUnits = Regex.Replace(mapUnits, "^esri", "", RegexOptions.CultureInvariant | RegexOptions.Compiled | RegexOptions.IgnoreCase);

            if (!mapScale.HasValue || string.IsNullOrEmpty(mapUnits))
                return;

            OnExtentUpdated(new ExtentUpdatedEventArgs(Extent, mapScale.Value, mapUnits));
        }

        private void SelectionLayer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnSelectionChanged(e);
        }

        public void OnStartOperation(IEnumerable<string> names)
        {
            //IGroupLayer groupLayer = new GroupLayerClass();
            //foreach (string name in names)
            //{
            //    foreach (ILayer layer in GetLayers(name))
            //    {
            //        groupLayer.Add(layer);
            //    }
            //}
            //ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, groupLayer, ActiveView.ScreenDisplay.DisplayTransformation.FittedBounds);
        }

        public void OnStopOperation(IEnumerable<string> names)
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            foreach (string name in names)
            {
                foreach (ILayer layer in GetLayers(name))
                {
                    groupLayer.Add(layer);
                }
            }
            ClearSelection();
            ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, groupLayer, ActiveView.ScreenDisplay.DisplayTransformation.FittedBounds);
        }

        public void OnAbortOperation(IEnumerable<string> names)
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            foreach (string name in names)
            {
                foreach (ILayer layer in GetLayers(name))
                {
                    groupLayer.Add(layer);
                }
            }
            ClearSelection();
            ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, groupLayer, ActiveView.ScreenDisplay.DisplayTransformation.FittedBounds);
        }

        public void OnUndo()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            foreach (ILayer layer in GetLayers())
            {
                groupLayer.Add(layer);
            }
            ClearSelection();
            ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, groupLayer, ActiveView.ScreenDisplay.DisplayTransformation.FittedBounds);
        }

        public void OnRedo()
        {
            IGroupLayer groupLayer = new GroupLayerClass();
            foreach (ILayer layer in GetLayers())
            {
                groupLayer.Add(layer);
            }
            ClearSelection();
            ActiveView.PartialRefresh(esriViewDrawPhase.esriViewGeography, groupLayer, ActiveView.ScreenDisplay.DisplayTransformation.FittedBounds);
        }

        public void OnWorkspaceChanged(IWorkspace oldWorkspace, IWorkspace workspace)
        {
        }

        private void OnMouseMoved(MouseMovedEventArgs e)
        {
            if (MouseMoved != null)
                MouseMoved(this, e);
        }

        private void OnScreenDrawn(ScreenDrawnEventArgs e)
        {
            if (ScreenDrawn != null)
                ScreenDrawn(this, e);
        }

        private void OnExtentUpdated(ExtentUpdatedEventArgs e)
        {
            if (ExtendUpdated != null)
                ExtendUpdated(this, e);
        }

        private void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(this, e);
        }

        public void Step(string format, params object[] args)
        {
            Step(string.Format(format, args));
        }

        public void Step(string stepMessage)
        {
            lblStepText.Text = stepMessage;
        }

        // noraini ali - OKT 2020
        // function highlight feature geomerty type point & line only
        // use GrphicContainer to save new element layer
        public void HighlightSelection()
        {
            if (SelectionCount == 0)
            {
                return;
            }

            IGraphicsContainer graphicsContainer = (IGraphicsContainer)Map;
            graphicsContainer.DeleteAllElements();

            foreach (IGFeature feature in SelectionLayer.Selections)
            {
                IGeometry geometry = feature.Shape;
                switch (geometry.GeometryType)
                {
                    case esriGeometryType.esriGeometryPoint:
                    case esriGeometryType.esriGeometryPolyline:

                        IElement element = ElementFactory.CreateElement(geometry, ColorUtils.Get(255, 0, 0), ColorUtils.Get(0, 0, 0));
                        if (element != null)
                        {
                            element.Geometry = geometry;
                            graphicsContainer.AddElement(element, 0);
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        // noraini ali - OKT 2020
        // function highlight feature geomerty type point & line only
        // use MovePoint/MoveLine Graphic to highlight feature
        public void HighlightSelectionFeature()
        {
            foreach (IGFeature feature in SelectionLayer.Selections)
            { 
                IGeometry geometry = feature.Shape;
                switch (geometry.GeometryType)
                {
                    case esriGeometryType.esriGeometryPoint:
                        IPoint point = new PointClass();
                        point.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMax);
                        MovePoint movePoint = new MovePoint(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0),  ColorUtils.Get(0, 0, 0), 10);
                        movePoint.Point = point;
                        movePoint.Start(point);
                        break;

                    case esriGeometryType.esriGeometryPolyline:
                        IPolyline polyline = (IPolyline)geometry;
                        MoveLine moveline = new MoveLine(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0), 2);
                        moveline.Line = polyline;
                        moveline.Start(polyline.FromPoint);
                        break;

                    case esriGeometryType.esriGeometryPolygon:
                        IPolygon Polygon = (IPolygon)geometry;
                        MovePolygon movepolygon = new MovePolygon(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0));
                        movepolygon.Polygon = Polygon;
                        movepolygon.Start(Polygon.FromPoint);
                        break;

                    default:
                        break;
                }
            }
        }

        // noraini ali - OKT 2020
        // to clear highlight feature
        public void HighlightClear()
        {
            IGraphicsContainer graphicsContainer = (IGraphicsContainer)Map;
            graphicsContainer.DeleteAllElements();
        }

		public void HighlightSelectionFeature(IGFeature feature)
		{
			IGeometry geometry = feature.Shape;
			switch (geometry.GeometryType)
			{
				case esriGeometryType.esriGeometryPoint:
					IPoint point = new PointClass();
					point.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMax);
					MovePoint movePoint = new MovePoint(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0), ColorUtils.Get(0, 0, 0), 10);
					movePoint.Point = point;
					movePoint.Start(point);
					break;

				case esriGeometryType.esriGeometryPolyline:
					IPolyline polyline = (IPolyline)geometry;
					MoveLine moveline = new MoveLine(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0), 2);
					moveline.Line = polyline;
					moveline.Start(polyline.FromPoint);
					break;

				case esriGeometryType.esriGeometryPolygon:
					IPolygon Polygon = (IPolygon)geometry;
					MovePolygon movepolygon = new MovePolygon(ActiveView.ScreenDisplay, ColorUtils.Get(255, 0, 0));
					movepolygon.Polygon = Polygon;
					movepolygon.Start(Polygon.FromPoint);
					break;

				default:
					break;
			}
		}

		#region [Raster] syafiq - July 2021

		// check whether there is any raster opened or not, if not prompt message.
		private bool ChkRasterExist()
		{
			if (m_pCurrentRasterLayer == null)
			{
				MessageBox.Show("There is no raster layer in the map");
				return true;
			}
			return false;
		}

		// Open Raster
		public void OpenRaster(string path, string file)
		{
			if (m_pCurrentRasterLayer != null)
			{
				MessageBox.Show("There is already a raster layer in the map. Please close existing raster in order to proceed.");
				return;
			}

			rasterPath = path;
			rasterFullPath = path + "\\" + file;
			rasterFile = file;
			rasterName = file.Remove(file.LastIndexOf("."));


			// open the selected raster.
			IRasterDataset raster = WorkspacePool.Instance.OpenRasterWorkspace(path).OpenRaster(file);
			m_pCurrentRasterLayer = new RasterLayer();
			m_pCurrentRasterLayer.Name = raster.CompleteName;
			m_pCurrentRasterLayer.CreateFromDataset(raster);
			AddLayer(m_pCurrentRasterLayer);

			// make the raster layer 40% transparent.
			ILayerEffects pLayerEffects;
			pLayerEffects = (ILayerEffects)m_pCurrentRasterLayer;
			pLayerEffects.Transparency = 40;
			m_pCurrentRasterLayer = (IRasterLayer)pLayerEffects;

			//set raster projection to be same as map projection. 
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			if (m_pCurrentRasterLayer.AreaOfInterest.LowerLeft.X != 0)
			{
				//set active view to raster extent.
				ActiveView.Extent = m_pCurrentRasterLayer.AreaOfInterest;
			}
			else
			{
				MessageBox.Show("The Raster does not have any georeference. Please pan to desire location and click fit to display button.");
			}

			//refresh view
			RefreshMap();
		}

		// to delete raster from map.
		public void CloseRaster()
		{
			if (ChkRasterExist())
			{
				return;
			}

			for (int count = 0; count < Map.LayerCount; count++)
			{
				foreach (ILayer layer in GetLayers(Map.get_Layer(count)))
				{
					if (layer is IRasterLayer && layer.Name == m_pCurrentRasterLayer.Name)
					{
						DeleteLayer(layer);
						rasterPath = null;
						rasterFullPath = null;
						rasterFile = null;
						rasterName = null;
						break;
					}
				}
			}
			m_pCurrentRasterLayer = null;
		}

		// to draw the taken points on map.
		private void DrawTakenPoints(IPoint takenpoint)
		{
			IActiveView pActiveView = (IActiveView)Map;
			IElement pntElement;

			IMarkerElement pMElement = new MarkerElementClass();

			IColor pColor;
			pColor = new RgbColor();
			if (m_bTakeSource)
				pColor = ColorUtils.Get(255, 0, 0);
			else if (m_bTakeTarget)
				pColor = ColorUtils.Get(0, 255, 0);

			IMarkerSymbol pMarkerSymbol;
			ISimpleMarkerSymbol pSMarkerSymbol;

			pSMarkerSymbol = new SimpleMarkerSymbol();
			pSMarkerSymbol.Style = esriSimpleMarkerStyle.esriSMSX;
			pSMarkerSymbol.Color = pColor;
			pMarkerSymbol = pSMarkerSymbol;

			pMElement.Symbol = pMarkerSymbol;

			pntElement = (IElement)pMElement;
			pntElement.Geometry = takenpoint;

			IGraphicsContainer pGC;
			pGC = (IGraphicsContainer)pActiveView;
			pGC.AddElement(pntElement, 0);
			if (m_bTakeSource)
				m_pSourceCollElement.Add(pntElement);
			else if (m_bTakeTarget)
				m_pTargetCollElement.Add(pntElement);
			pActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
			m_bTakeSource = false;
			m_bTakeTarget = false;
		}

		// to add source point based on mouse clicks
		public void AddSourcePoint(int x, int y)
		{
			if (ChkRasterExist())
				return;

			if (m_pSourceCollection != null && m_pTargetCollection == null)
			{
				MessageBox.Show("You need to select Target point before adding more Source point.");
				return;
			}

			if (m_pSourceCollection != null && m_pTargetCollection != null)
			{
				if (m_pSourceCollection.PointCount - m_pTargetCollection.PointCount >= 1)
				{
					MessageBox.Show("You need to select Target point before adding more Source point.");
					return;
				}
			}

			IActiveView pActiveView = (IActiveView)Map;
			IPoint pPoint = pActiveView.ScreenDisplay.DisplayTransformation.ToMapPoint(x, y);

			m_bTakeSource = true;
			IClone pclone1;
			IPoint pclonePnt;
			pclone1 = (IClone)pPoint;
			pclonePnt = (IPoint)pclone1;
			pclonePnt = pclonePnt.Clone();

			// add the source points to point collection.
			if (!m_bTakePointUse1)
			{
				m_bTakePointUse1 = true;
				m_pSourceCollection = new Multipoint();
				m_pSourceCollection.AddPoint(pPoint);
				m_pSourceCollElement = new ElementCollection();
				DrawTakenPoints(pclonePnt);
			}
			else
			{
				m_pSourceCollection.AddPoint(pPoint);
				DrawTakenPoints(pclonePnt);
			}
		}

		// to add target point based on mouse clicks
		public void AddTargetPoint(int x, int y)
		{
			if (ChkRasterExist())
				return;

			if (m_pSourceCollection == null && m_pTargetCollection == null)
			{
				MessageBox.Show("You need to select Source Point before adding Target point.");
				return;
			}

			if (m_pSourceCollection != null && m_pTargetCollection != null)
			{
				if (m_pSourceCollection.PointCount == m_pTargetCollection.PointCount)
				{
					MessageBox.Show("You need to select Source Point before adding more Target point.");
					return;
				}
			}

			IActiveView pActiveView = (IActiveView)Map;
			IPoint pPoint = pActiveView.ScreenDisplay.DisplayTransformation.ToMapPoint(x, y);

			m_bTakeTarget = true;
			IClone pclone1;
			IPoint pclonePnt;
			pclone1 = (IClone)pPoint;
			pclonePnt = (IPoint)pclone1;
			pclonePnt = pclonePnt.Clone();

			// add the target points to point collection.
			if (!m_bTakePointUse2)
			{
				m_bTakePointUse2 = true;
				m_pTargetCollection = new Multipoint();
				m_pTargetCollection.AddPoint(pPoint);
				m_pTargetCollElement = new ElementCollection();
				DrawTakenPoints(pclonePnt);
			}
			else
			{
				m_pTargetCollection.AddPoint(pPoint);
				DrawTakenPoints(pclonePnt);
			}
		}

		private double PointDistance(double p1X, double p1Y, double p2X, double p2Y)
		{
			var dx = p1X - p2X;
			var dy = p1Y - p2Y;
			var dist = Math.Sqrt(dx * dx + dy * dy);
			return dist;
		}

		// delete point pair (source and target) based on mouse click
		public void DeletePoint(int x, int y)
		{
			//check raster exist
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			IPoint pPoint = pActiveView.ScreenDisplay.DisplayTransformation.ToMapPoint(x, y);

			int i;
			int nearestIndex = -1;
			double nearestPointDistance = 20;

			for (i = 0; i <= m_pSourceCollection.PointCount - 1; i++)
			{
				double iPointDistance = PointDistance(m_pSourceCollection.Point[i].X, m_pSourceCollection.Point[i].Y, pPoint.X, pPoint.Y);

				if (iPointDistance < nearestPointDistance) {
					nearestPointDistance = iPointDistance;
					nearestIndex = i;
				}
			}

			if (nearestIndex == -1)
			{
				MessageBox.Show("No nearest point selected!");
			}
			else
			{
				if (m_pSourceCollElement != null)
				{
					IElement pQueryElement;
					int linkedFeatureID;

					pQueryElement = new MarkerElement();
					m_pSourceCollElement.QueryItem(nearestIndex, out pQueryElement, out linkedFeatureID);
					IGraphicsContainer pGC;
					pGC = (IGraphicsContainer)pActiveView;

					pGC.DeleteElement(pQueryElement);
					m_pSourceCollElement.Remove(pQueryElement);
					m_pSourceCollection.RemovePoints(nearestIndex, 1);
					pActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
				}
				if (m_pTargetCollElement != null)
				{
					IElement pQueryElement;
					int linkedFeatureID;

					pQueryElement = new MarkerElement();
					m_pTargetCollElement.QueryItem(nearestIndex, out pQueryElement, out linkedFeatureID);
					IGraphicsContainer pGC;
					pGC = (IGraphicsContainer)pActiveView;

					pGC.DeleteElement(pQueryElement);
					m_pTargetCollElement.Remove(pQueryElement);
					m_pTargetCollection.RemovePoints(nearestIndex, 1);
					pActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
				}
			}		
		}

		// to delete all the previously added points.
		public void DeleteAllPoints()
		{
			IActiveView pActiveView = (IActiveView)Map;
			int i;
			int linkedFeatureID;
			IElement pQueryElement;

			if (m_pSourceCollElement != null)
			{
				for (i = 0; i <= m_pSourceCollElement.Count - 1; i++)
				{
					pQueryElement = new MarkerElement();
					m_pSourceCollElement.QueryItem(i, out pQueryElement, out linkedFeatureID);
					IGraphicsContainer pGC;
					pGC = (IGraphicsContainer)pActiveView;
					if (pQueryElement != null)
					{
						pGC.DeleteElement(pQueryElement);
						m_pSourceCollection.RemovePoints(0, 1);
					}
				}
				m_pSourceCollElement.Clear();
			}

			if (m_pTargetCollElement != null)
			{
				for (i = 0; i <= m_pTargetCollElement.Count - 1; i++)
				{
					pQueryElement = new MarkerElement();
					m_pTargetCollElement.QueryItem(i, out pQueryElement, out linkedFeatureID);
					IGraphicsContainer pGC;
					pGC = (IGraphicsContainer)pActiveView;
					pGC.DeleteElement(pQueryElement);
					m_pTargetCollection.RemovePoints(0, 1);
				}
				m_pTargetCollElement.Clear();
			}

			pActiveView.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
		}

		// to update raster location on map according to source points and target points specified.
		public void UpdateAlignment()
		{
			if (m_pSourceCollection == null || m_pTargetCollection == null)
			{
				MessageBox.Show("You have not specified the source and target points");
				return;
			}

			if (m_pSourceCollection.PointCount != m_pTargetCollection.PointCount)
			{
				MessageBox.Show("You don't have the same number of pair for source point and target point.");
				return;
			}
			else if (m_pSourceCollection.PointCount == 0 && m_pTargetCollection.PointCount == 0) {
				MessageBox.Show("You have not specified source point and target point.");
				return;
			}
			else if (m_pSourceCollection.PointCount == 1 && m_pTargetCollection.PointCount == 1)
			{
				MessageBox.Show("Insufficient source point and target point. Please add more point.");
				return;
			}
			else
			{
				IActiveView pActiveView = (IActiveView)Map;
				// set raster projection to be same as map projection.
				m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;
				IGeoReference pIGeoReference;
				pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;

				if (m_pSourceCollection.PointCount == 2 && m_pTargetCollection.PointCount == 2)
				{
					pIGeoReference.TwoPointsAdjust(m_pSourceCollection, m_pTargetCollection);
				}
				else
				{
					// to warp the points at the location specified.
					pIGeoReference.TwoPointsAdjust(m_pSourceCollection, m_pTargetCollection);
					pIGeoReference.Warp(m_pSourceCollection, m_pTargetCollection, 0);
				}
				
				// refresh view.
				RefreshMap();
			}
		}

		// to revert raster to last save point.
		public void FitToDisplay()
		{
			IActiveView pActiveView = (IActiveView)Map;

			if (ChkRasterExist())
				return;

			if (m_pCurrentRasterLayer.AreaOfInterest.LowerLeft.X == 0)
			{
				FitNewScan();
				RefreshMap();
			}
			else
			{
				IGeoReference pIGeoReference;
				pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
				pIGeoReference.Reset();
				RefreshMap();
			}
		}

		// to put the ungeoreference raster to fit the display extent.
		private void FitNewScan()
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// to get the center point of the raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// to get the center point of the active view
			double centerXView = (pActiveView.Extent.XMin + pActiveView.Extent.XMax) / 2;
			double ceterYView = (pActiveView.Extent.YMin + pActiveView.Extent.YMax) / 2;

			// to calculate the move point
			rasterDx = centerXView - centerXRaster;
			rasterDy = ceterYView - centerYRaster;

			ITransform2D pTrans2d;
			IGeometry pGmtry = m_pCurrentRasterLayer.AreaOfInterest;
			pTrans2d = (ITransform2D)pGmtry;
			pTrans2d.Move(rasterDx, rasterDy);

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;

			IRasterProps pRasterProp;
			pRasterProp = (IRasterProps)m_pCurrentRasterLayer.Raster;

			IPointCollection pSourcePoints;
			IPointCollection pTargetPoints;
			IPoint pPoint;

			pSourcePoints = new Multipoint();
			pTargetPoints = new Multipoint();

			IEnvelope pRasterExt = m_pCurrentRasterLayer.AreaOfInterest;

			// set target points using current view extent properties.
			pPoint = new Point();
			pPoint.PutCoords(pGmtry.Envelope.XMin, pGmtry.Envelope.YMin);
			pTargetPoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pGmtry.Envelope.XMax, pGmtry.Envelope.YMin);
			pTargetPoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pGmtry.Envelope.XMax, pGmtry.Envelope.YMax);
			pTargetPoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pGmtry.Envelope.XMin, pGmtry.Envelope.YMax);
			pTargetPoints.AddPoint(pPoint);

			// set source points using raster extent properties.
			pPoint = new Point();
			pPoint.PutCoords(pRasterExt.XMin, pRasterExt.YMin);
			pSourcePoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pRasterExt.XMax, pRasterExt.YMin);
			pSourcePoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pRasterExt.XMax, pRasterExt.YMax);
			pSourcePoints.AddPoint(pPoint);
			pPoint = new Point();
			pPoint.PutCoords(pRasterExt.XMin, pRasterExt.YMax);
			pSourcePoints.AddPoint(pPoint);

			// to warp the points at the location specified.
			pIGeoReference.TwoPointsAdjust(pSourcePoints, pTargetPoints);
			pIGeoReference.Warp(pSourcePoints, pTargetPoints, 0);
		}

		// to register the raster.
		public void RegisterRaster()
		{
			if (ChkRasterExist())
				return;

			// to register raster on map.
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.Register();

			RefreshMap();
		}

		// to build pyramid of  the raster
		public void BuildPyramid()
		{
			if (ChkRasterExist())
				return;

			//to build pyramid
			BuildPyramids buildpyr;
			buildpyr = new BuildPyramids();
			buildpyr.in_raster_dataset = rasterFullPath;

			using (new WaitCursor())
			{
				Geoprocessor GP = new Geoprocessor();
				GP.Execute(buildpyr, null);
			}
			
			MessageBox.Show("Build Pyramid Complete", "Success");
		}

		// to move raster to new location
		public void MoveRaster(int x, int y)
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// translate point to map point
			IPoint pPoint = pActiveView.ScreenDisplay.DisplayTransformation.ToMapPoint(x, y);

			// to get the center point of the raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// to calculate the shift point
			rasterDx = pPoint.X - centerXRaster;
			rasterDy = pPoint.Y - centerYRaster;

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			// shift the raster
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.Shift(rasterDx, rasterDy);

			// refresh view
			RefreshMap();
		}

		// to rotate the raster clockwise 90'
		public void RotateRasterClockwise()
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// to get the center point of the raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			IPoint centerRasterPoint = new Point();
			centerRasterPoint.PutCoords(centerXRaster, centerYRaster);

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			// rotate the raster
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.Rotate(centerRasterPoint, -90);

			// refresh view
			RefreshMap();
		}

		// to rotate the raster anti-clockwise 90'
		public void RotateRasterAntiClockwise()
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// to get the center point of the raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			IPoint centerRasterPoint = new Point();
			centerRasterPoint.PutCoords(centerXRaster, centerYRaster);

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			// rotate the raster
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.Rotate(centerRasterPoint, 90);

			// refresh view
			RefreshMap();
		}

		// to scale up raster
		public void ScaleUpRaster()
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// to get the center point of the current raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			// shift the raster
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.ReScale(2, 2);

			// to get the center point of the raster after scale
			double scaledXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double scaledYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// to calculate the shift point
			rasterDx = centerXRaster - scaledXRaster;
			rasterDy = centerYRaster - scaledYRaster;

			//shift raster to intial place because after rescale raster move to other location
			pIGeoReference.Shift(rasterDx, rasterDy);

			// refresh view
			RefreshMap();
		}

		// to scale up raster
		public void ScaleDownRaster()
		{
			if (ChkRasterExist())
				return;

			IActiveView pActiveView = (IActiveView)Map;

			// to get the center point of the current raster
			double centerXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double centerYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// set raster projection to be same as map projection.
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;

			// shift the raster
			IGeoReference pIGeoReference;
			pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
			pIGeoReference.ReScale(0.5, 0.5);

			// to get the center point of the raster after scale
			double scaledXRaster = (m_pCurrentRasterLayer.AreaOfInterest.XMin + m_pCurrentRasterLayer.AreaOfInterest.XMax) / 2;
			double scaledYRaster = (m_pCurrentRasterLayer.AreaOfInterest.YMin + m_pCurrentRasterLayer.AreaOfInterest.YMax) / 2;

			// to calculate the shift point
			rasterDx = centerXRaster - scaledXRaster;
			rasterDy = centerYRaster - scaledYRaster;

			//shift raster to intial place because after rescale raster move to other location
			pIGeoReference.Shift(rasterDx, rasterDy);

			// refresh view
			RefreshMap();
		}

		// Open Raster in Paint
		public void OpenRasterInPaint(string path, string file)
		{
			rasterFullPath = path + "\\" + file;
			System.Diagnostics.Process.Start("mspaint", rasterFullPath);
		}

		private List<string> GetAllRasterFileForExport(string path, string name)
		{
			// get all files in raster folder
			string[] FileNames = Directory.GetFiles(path);

			// add all rasters file to a list
			List<string> RastersFile = new List<string>();
			foreach (var FileName in FileNames)
			{
				if (FileName.Contains(name))
				{
					RastersFile.Add(FileName);
				}
			}

			// add all required file to a list
			List<string> RequiredRastersFile = new List<string>();
			if (rasterFile == name + ".tif")
			{
				RequiredRastersFile.Add(path + "\\" + name + ".aux");
				RequiredRastersFile.Add(path + "\\" + name + ".tfwx");
				RequiredRastersFile.Add(path + "\\" + name + ".tif");
				RequiredRastersFile.Add(path + "\\" + name + ".tif.aux.xml");
			}
			else if (rasterFile == name + ".tiff")
			{
				RequiredRastersFile.Add(path + "\\" + name + ".aux");
				RequiredRastersFile.Add(path + "\\" + name + ".tfwx");
				RequiredRastersFile.Add(path + "\\" + name + ".tiff");
				RequiredRastersFile.Add(path + "\\" + name + ".tiff.aux.xml");
			}
			else if (rasterFile == name + ".cit")
			{
				RequiredRastersFile.Add(path + "\\" + name + ".aux");
				RequiredRastersFile.Add(path + "\\" + name + ".ctwx");
				RequiredRastersFile.Add(path + "\\" + name + ".cit");
				RequiredRastersFile.Add(path + "\\" + name + ".cit.aux.xml");
			}

			// filter for required files only
			List<string> CommonFile = RastersFile.Intersect(RequiredRastersFile).ToList();

			return CommonFile;
		}

		private void CopyRasterFile(string source, string destination)
		{
			// create the directory if it does not exist directory
			if (!Directory.Exists(destination))
			{
				Directory.CreateDirectory(destination);
			}
			// copy based on raster type
			if (rasterFile == rasterName + ".tif")
			{
				File.Copy(source + rasterName + ".aux", destination + rasterName + ".aux", true);
				File.Copy(source + rasterName + ".tfwx", destination + rasterName + ".tfwx", true);
				File.Copy(source + rasterName + ".tif", destination + rasterName + ".tif", true);
				File.Copy(source + rasterName + ".tif.aux.xml", destination + rasterName + ".tif.aux.xml", true);
			}
			else if (rasterFile == rasterName + ".tiff")
			{
				File.Copy(source + rasterName + ".aux", destination + rasterName + ".aux", true);
				File.Copy(source + rasterName + ".tfwx", destination + rasterName + ".tfwx", true);
				File.Copy(source + rasterName + ".tiff", destination + rasterName + ".tif", true);
				File.Copy(source + rasterName + ".tiff.aux.xml", destination + rasterName + ".tif.aux.xml", true);
			}
			else if (rasterFile == rasterName + ".cit")
			{
				File.Copy(source + rasterName + ".aux", destination + rasterName + ".aux", true);
				File.Copy(source + rasterName + ".ctwx", destination + rasterName + ".ctwx", true);
				File.Copy(source + rasterName + ".cit", destination + rasterName + ".cit", true);
				File.Copy(source + rasterName + ".cit.aux.xml", destination + rasterName + ".cit.aux.xml", true);
			}
			if (File.Exists(source + rasterName + ".rrd"))
			{
				File.Copy(source + rasterName + ".rrd", destination + rasterName + ".rrd", true);
			}
		}

		private void ZipRasterFolder(string folder, string path, string zipName)
		{
			using (ZipFile zip = new ZipFile())
			{
				// zip the directory
				using (new WaitCursor())
				{
					zip.AddDirectory(folder);
					zip.Save(path + zipName);			
				}
			}
        }

    private void MoveZipFile(string source, string destination)
        {
            using (new WaitCursor())
            {
                try
                {
                    string rasterFile = System.IO.Path.GetFileName(destination);
                    string sourceDirectoryPath = System.IO.Path.GetDirectoryName(source);
                  
                    if (source == destination) // source same as destination
                    {
                        // display success message
                        string messageSuccess = string.Format("File successfully exported to \n{0}", destination);
                        MessageBox.Show(messageSuccess, "Success");
                    }
                    else // source not same as destination
                    {
                        switch (sourceDirectoryPath) //if source from Z directory (same directory to destination)
                        {
                            case @"Z:\USERS\Scan":
                                destination = @"Z:\USERS\Scan\" + rasterFile;
                                break;
                            case @"Z:\Users\Scan":
                                destination = @"Z:\Users\Scan\" + rasterFile;
                                break;
                            case @"Z:\Users\SCAN":
                                destination = @"Z:\Users\SCAN\" + rasterFile;
                                break;
                            case @"Z:\Users\scan":
                                destination = @"Z:\Users\scan\" + rasterFile;
                                break;
                            case @"Z:\users\Scan":
                                destination = @"Z:\users\Scan\" + rasterFile;
                                break;
                            case @"Z:\users\SCAN":
                                destination = @"Z:\users\SCAN\" + rasterFile;
                                break;
                            case @"Z:\users\scan":
                                destination = @"Z:\users\scan\" + rasterFile;
                                break;
                            default:
                                break;
                        }
                        
                        // Check if source same as destination after new destination definition
                        if (source == destination)
                        {
                            // display success message
                            string messageSuccess1 = string.Format("File successfully exported to \n{0}", destination);
                            MessageBox.Show(messageSuccess1, "Success");
                        }
                        else
                        {
                            //if the file exist on destination, delete the destination file
                            if (File.Exists(destination))
                            {
                                File.Delete(destination);
                            }

                            // move the file
                            File.Move(source, destination);
                            // display success message
                            string messageSuccess2 = string.Format("File successfully exported to \n{0}", destination);
                            MessageBox.Show(messageSuccess2, "Success");
                        }
                    }         
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error Move Raster to Path");
                }
            }
        }

    public void ExportRaster()
		{
			// check if raster layer exist
			if (ChkRasterExist())
				return;
			// get raster files
			List<string> RastersFile = GetAllRasterFileForExport(rasterPath, rasterName);
			// check if files is complete
			if (RastersFile.Count < 4) //raster not complete
			{
				string messageError = string.Format("Incomplete file! Have you register your raster?");
				MessageBox.Show(messageError, "Error!");
				return;
			}
			else //raster complete
			{
                // if WOMS Directory Not exist, throw error
                if (!Directory.Exists(WOMSDirectory))
                {
                    //string messageError = string.Format("Cannot connect to {0}\nPlease ensure that the path are available & you have access to the path!", WOMSDirectory);
                    //MessageBox.Show(messageError, "Error!");
                    //return;

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                        .SetDefaultButton(MessageBoxDefaultButton.Button2)
                        .SetCaption("Error! Cannot connect to {0}", WOMSDirectory)
                        .SetText("Please ensure that the path are available & you have access to the path! \n\nDo you wish to use current path ({0}) instead?", rasterPath + "\\");

                        if (confirmBox.Show() == DialogResult.Yes)
                        {
                            WOMSDirectory = rasterPath + "\\";
                        }
                        else if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }
                }

                using (new WaitCursor())
				{
					// define directories
					string sourceFolder = rasterPath + "\\";
					string destinationFolder = rasterPath + "\\" + rasterName + "\\";
					string zipName = string.Format("{0}.zip", rasterName);
					string sourceZipFile = sourceFolder + zipName;
					string destinationZipFile = WOMSDirectory + zipName;
					// copy raster to a folder
					CopyRasterFile(sourceFolder, destinationFolder);
					// zip raster folder
					ZipRasterFolder(destinationFolder, sourceFolder, zipName);
					// move zip file
					MoveZipFile(sourceZipFile, destinationZipFile);
					// delete the raster folder
					Directory.Delete(destinationFolder, true);
					WOMSDirectory = @"Z:\USERS\SCAN\";
				}					
			}
		}

		public void RectifyRaster()
		{
			// check if raster layer exist
			if (ChkRasterExist())
				return;
			string ExportedDirectory = WOMSDirectory + rasterName + "\\";
			// set active view to raster
			IActiveView pActiveView = (IActiveView)Map;
			m_pCurrentRasterLayer.SpatialReference = Map.SpatialReference;
			// get raster files
			List<string> RastersFile = GetAllRasterFileForExport(rasterPath, rasterName);
			// check if files is complete
			if (RastersFile.Count < 4) //raster not complete
			{
				string messageError = string.Format("Incomplete file! Have you register your raster?");
				MessageBox.Show(messageError, "Error!");
				return;
			}
			else //raster complete
			{
				// if WOMS Directory Not exist, throw error
				if (!Directory.Exists(WOMSDirectory))
				{
					string messageError = string.Format("Cannot connect to {0}\nPlease ensure that the path are available & you have access to the path!", WOMSDirectory);
					MessageBox.Show(messageError, "Error!");
					return;
				}
				using (new WaitCursor())
				{
					//if directory exist, delete the directory
					if (Directory.Exists(ExportedDirectory))
					{
						Directory.Delete(ExportedDirectory, true);
					}
					//create directory
					Directory.CreateDirectory(ExportedDirectory);
					IGeoReference pIGeoReference;
					pIGeoReference = (IGeoReference)m_pCurrentRasterLayer;
					if (System.IO.Path.GetExtension(rasterFullPath) ==  ".tiff")
					{
						//create .tif
						File.Copy(rasterPath + "\\" + rasterName + ".tiff", rasterPath + "\\" + rasterName + ".tif", true);
						File.Copy(rasterPath + "\\" + rasterName + ".tiff.aux.xml", rasterPath + "\\" + rasterName + ".tif.aux.xml", true);
						pIGeoReference.Rectify(ExportedDirectory + rasterName + ".tif", "TIFF");
					}
					else
					{
						pIGeoReference.Rectify(ExportedDirectory + rasterFile, "TIFF");
					}
						
				}

				if (System.IO.Path.GetExtension(rasterFullPath) == ".tiff")
				{
					string messageSuccess = string.Format("File successfully exported to {0}\nFilename : {1}", ExportedDirectory, rasterName + ".tif");
					MessageBox.Show(messageSuccess, "Success");
					File.Delete(rasterPath + "\\" + rasterName + ".tif");
					File.Delete(rasterPath + "\\" + rasterName + ".tif.aux.xml");
				}
				else
				{
					string messageSuccess = string.Format("File successfully exported to {0}\nFilename : {1}", ExportedDirectory, rasterFile);
					MessageBox.Show(messageSuccess, "Success");
				}


				RefreshMap();
			}

		}
		#endregion
	}
}

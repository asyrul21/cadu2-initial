﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace Geomatic.UI.Forms.Documents
{
    public partial class Document : DockContent
    {
        public string FileName { protected set; get; }

        public Document()
        {
            InitializeComponent();
        }

        public void RefreshTitle(string title)
        {
            Text = title;
        }

        public void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.Documents
{
    public class ScreenDrawnEventArgs : EventArgs
    {
        public bool IsDrawing { private set; get; }

        public ScreenDrawnEventArgs(bool isDrawing)
        {
            IsDrawing = isDrawing;
        }
    }
}

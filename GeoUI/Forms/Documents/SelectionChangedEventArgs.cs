﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Documents
{
    public class SelectionChangedEventArgs : EventArgs
    {
        public int Count { private set; get; }
        public List<IGFeature> Selections { private set; get; }

        public SelectionChangedEventArgs(int count, List<IGFeature> selections)
        {
            Count = count;
            Selections = selections;
        }
    }
}

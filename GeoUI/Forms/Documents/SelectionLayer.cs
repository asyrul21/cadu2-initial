﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Display;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using Geomatic.UI.Utilities.Elements;

namespace Geomatic.UI.Forms.Documents
{
    public class SelectionLayer : CompositeGraphicsLayerClass
    {
        public event EventHandler<SelectionChangedEventArgs> LayerSelectionChanged;

        protected const string NAME = "Selections";
        protected List<IGFeature> _selections;

        protected IRgbColor _color = ColorUtils.Select;

        protected IRgbColor _outlineColor = ColorUtils.Select;

        public int SelectionCount
        {
            get
            {
                return _selections.Count;
            }
        }

        public IEnumerable<IGFeature> Selections
        {
            get
            {
                foreach (IGFeature feature in _selections)
                {
                    yield return feature;
                }
            }
        }

        public SelectionLayer()
            : this(NAME, true)
        {

        }

        public SelectionLayer(string name)
            : this(name, true)
        {

        }

        public SelectionLayer(string name, bool cached)
        {
            _selections = new List<IGFeature>();
            Name = name;
            Cached = cached;
        }

        public void Add(IGFeature feature)
        {
            _selections.Add(feature);
            OnSelectionChanged();
        }

        public void Add(IEnumerable<IGFeature> features)
        {
            Add(features, false);
        }

        public void Add(IEnumerable<IGFeature> features, bool clear)
        {
            if (clear)
            {
                _selections.Clear();
            }
            _selections.AddRange(features);
            OnSelectionChanged();
        }

        public void Remove(IGFeature feature)
        {
            if (!_selections.Contains(feature))
            {
                return;
            }
            _selections.Remove(feature);
            OnSelectionChanged();
        }

        public void Clear()
        {
            _selections.Clear();
            OnSelectionChanged();
        }

        protected void OnSelectionChanged()
        {
            IGraphicsContainer graphicsContainer = this;
            graphicsContainer.DeleteAllElements();

            foreach (IGFeature feature in _selections)
            {
                IGeometry geometry = feature.Shape;
                IElement element = ElementFactory.CreateElement(geometry, _color, _outlineColor);
                if (element != null)
                {
                    element.Geometry = geometry;
                    graphicsContainer.AddElement(element, 0);
                }
            }
            if (LayerSelectionChanged != null)
            {
                LayerSelectionChanged(this, new SelectionChangedEventArgs(SelectionCount, _selections));
            }
        }
    }
}

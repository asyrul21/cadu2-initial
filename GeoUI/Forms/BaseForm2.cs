﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Commands;
using Geomatic.Core.Sessions;

namespace Geomatic.UI.Forms
{
    public partial class BaseForm2 : BaseForm
    {
        protected CommandPool _commandPool;

        public BaseForm2()
        {
            InitializeComponent();
            _commandPool = new CommandPool();
        }

        private void BaseForm2_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    if (_commandPool.Registered(Command.NavigationItem))
                    {
                        _commandPool.Disable(Command.NavigationItem);
                    }
                }

                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms
{
    public partial class PoiPhoneForm : CollapsibleForm
    {
        public string SelectedNumber
        {
            get
            {
                return cbAreaCode.Text + txtNumber.Text;
            }
        }

        protected GPoiPhone _phone;

        public PoiPhoneForm()
            : this(null)
        {
        }

        public PoiPhoneForm(GPoiPhone phone)
        {
            InitializeComponent();
            _phone = phone;
        }

        protected override void Form_Load()
        {
            cbAreaCode.Items.AddRange(_phone.GetAreaCodes().ToArray());
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            pass &= !string.IsNullOrEmpty(cbAreaCode.Text);
            pass &= StringUtils.IsInt(cbAreaCode.Text);

            pass &= !string.IsNullOrEmpty(txtNumber.Text);
            pass &= StringUtils.IsInt(txtNumber.Text);

            pass &= _phone.GetFormat().IsMatch(SelectedNumber) || _phone.GetMobileFormat().IsMatch(SelectedNumber) || _phone.GetTollFreeFormat().IsMatch(SelectedNumber);

            //added by asyrul
            if (!pass)
            {
                MessageBox.Show("Invalid number format!");
            }
            //added end

            return pass;
        }

        public void SetValues()
        {
            _phone.Number = SelectedNumber;
        }
    }
}

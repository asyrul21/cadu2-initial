﻿namespace Geomatic.UI.Forms
{
    partial class VerificationWebForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnVerifyFeature = new System.Windows.Forms.Button();
            this.btnRevertFeature = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFeatureId = new System.Windows.Forms.Label();
            this.txtFeatureId = new System.Windows.Forms.TextBox();
            this.lblFeatureName = new System.Windows.Forms.Label();
            this.txtStatusVerify = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtWebPlcName2 = new System.Windows.Forms.TextBox();
            this.txtWebObjId = new System.Windows.Forms.TextBox();
            this.txtAdmPlcName2 = new System.Windows.Forms.TextBox();
            this.txtAdmPlcName = new System.Windows.Forms.TextBox();
            this.txtAdmPOICode = new System.Windows.Forms.TextBox();
            this.txtAdmObjId = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtWebPlcName = new System.Windows.Forms.TextBox();
            this.txtWebPOICode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtWebModDate = new System.Windows.Forms.TextBox();
            this.txtWebModUser = new System.Windows.Forms.TextBox();
            this.txtWebStatus = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWebRefType = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtWebRefId = new System.Windows.Forms.TextBox();
            this.txtAdmModDate = new System.Windows.Forms.TextBox();
            this.txtAdmModUser = new System.Windows.Forms.TextBox();
            this.txtAdmStatus = new System.Windows.Forms.TextBox();
            this.txtAdmRefType = new System.Windows.Forms.TextBox();
            this.txtAdmRefId = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.1635F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.8365F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 154F));
            this.tableLayoutPanel3.Controls.Add(this.btnClose, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnVerifyFeature, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnRevertFeature, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 340);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(416, 29);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClose.Image = global::Geomatic.UI.Properties.Resources.cancel;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(333, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnVerifyFeature
            // 
            this.btnVerifyFeature.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnVerifyFeature.Image = global::Geomatic.UI.Properties.Resources.apply;
            this.btnVerifyFeature.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerifyFeature.Location = new System.Drawing.Point(3, 3);
            this.btnVerifyFeature.Name = "btnVerifyFeature";
            this.btnVerifyFeature.Size = new System.Drawing.Size(82, 23);
            this.btnVerifyFeature.TabIndex = 1;
            this.btnVerifyFeature.Text = "Verify";
            this.btnVerifyFeature.UseVisualStyleBackColor = true;
            this.btnVerifyFeature.Click += new System.EventHandler(this.BtnVerify_Click);
            // 
            // btnRevertFeature
            // 
            this.btnRevertFeature.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnRevertFeature.Image = global::Geomatic.UI.Properties.Resources.delete;
            this.btnRevertFeature.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRevertFeature.Location = new System.Drawing.Point(105, 3);
            this.btnRevertFeature.Name = "btnRevertFeature";
            this.btnRevertFeature.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnRevertFeature.Size = new System.Drawing.Size(82, 23);
            this.btnRevertFeature.TabIndex = 0;
            this.btnRevertFeature.Text = "Revert";
            this.btnRevertFeature.UseVisualStyleBackColor = true;
            this.btnRevertFeature.Click += new System.EventHandler(this.BtnRevert_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.34368F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.65633F));
            this.tableLayoutPanel1.Controls.Add(this.lblFeatureId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFeatureId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFeatureName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtStatusVerify, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(419, 56);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblFeatureId
            // 
            this.lblFeatureId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFeatureId.AutoSize = true;
            this.lblFeatureId.Location = new System.Drawing.Point(35, 5);
            this.lblFeatureId.Name = "lblFeatureId";
            this.lblFeatureId.Size = new System.Drawing.Size(64, 13);
            this.lblFeatureId.TabIndex = 0;
            this.lblFeatureId.Text = "Feature Id  :";
            this.lblFeatureId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFeatureId
            // 
            this.txtFeatureId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFeatureId.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFeatureId.Enabled = false;
            this.txtFeatureId.Location = new System.Drawing.Point(105, 3);
            this.txtFeatureId.Name = "txtFeatureId";
            this.txtFeatureId.Size = new System.Drawing.Size(311, 20);
            this.txtFeatureId.TabIndex = 1;
            // 
            // lblFeatureName
            // 
            this.lblFeatureName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFeatureName.AutoSize = true;
            this.lblFeatureName.Location = new System.Drawing.Point(27, 33);
            this.lblFeatureName.Name = "lblFeatureName";
            this.lblFeatureName.Size = new System.Drawing.Size(72, 13);
            this.lblFeatureName.TabIndex = 2;
            this.lblFeatureName.Text = "Status Verify :";
            this.lblFeatureName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtStatusVerify
            // 
            this.txtStatusVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatusVerify.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStatusVerify.Enabled = false;
            this.txtStatusVerify.Location = new System.Drawing.Point(105, 30);
            this.txtStatusVerify.Name = "txtStatusVerify";
            this.txtStatusVerify.ReadOnly = true;
            this.txtStatusVerify.Size = new System.Drawing.Size(311, 20);
            this.txtStatusVerify.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.75961F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.70192F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.36364F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtWebPlcName2, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtWebObjId, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmPlcName2, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmPlcName, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmPOICode, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmObjId, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtWebPlcName, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtWebPOICode, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.txtWebModDate, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.txtWebModUser, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.txtWebStatus, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.txtWebRefType, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtWebRefId, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmModDate, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmModUser, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmStatus, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmRefType, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.txtAdmRefId, 2, 5);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 67);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(419, 267);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Place Name2 :";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Place Name :";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "POI Code :";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ObjectId :";
            // 
            // txtWebPlcName2
            // 
            this.txtWebPlcName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebPlcName2.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebPlcName2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebPlcName2.Location = new System.Drawing.Point(109, 112);
            this.txtWebPlcName2.Name = "txtWebPlcName2";
            this.txtWebPlcName2.Size = new System.Drawing.Size(151, 13);
            this.txtWebPlcName2.TabIndex = 16;
            // 
            // txtWebObjId
            // 
            this.txtWebObjId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebObjId.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebObjId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebObjId.Enabled = false;
            this.txtWebObjId.Location = new System.Drawing.Point(109, 34);
            this.txtWebObjId.Name = "txtWebObjId";
            this.txtWebObjId.ReadOnly = true;
            this.txtWebObjId.Size = new System.Drawing.Size(151, 13);
            this.txtWebObjId.TabIndex = 10;
            // 
            // txtAdmPlcName2
            // 
            this.txtAdmPlcName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmPlcName2.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmPlcName2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmPlcName2.Location = new System.Drawing.Point(269, 112);
            this.txtAdmPlcName2.Name = "txtAdmPlcName2";
            this.txtAdmPlcName2.Size = new System.Drawing.Size(144, 13);
            this.txtAdmPlcName2.TabIndex = 17;
            // 
            // txtAdmPlcName
            // 
            this.txtAdmPlcName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmPlcName.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmPlcName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmPlcName.Location = new System.Drawing.Point(269, 86);
            this.txtAdmPlcName.Name = "txtAdmPlcName";
            this.txtAdmPlcName.Size = new System.Drawing.Size(144, 13);
            this.txtAdmPlcName.TabIndex = 15;
            // 
            // txtAdmPOICode
            // 
            this.txtAdmPOICode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmPOICode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmPOICode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmPOICode.Location = new System.Drawing.Point(269, 60);
            this.txtAdmPOICode.Name = "txtAdmPOICode";
            this.txtAdmPOICode.ReadOnly = true;
            this.txtAdmPOICode.Size = new System.Drawing.Size(144, 13);
            this.txtAdmPOICode.TabIndex = 13;
            // 
            // txtAdmObjId
            // 
            this.txtAdmObjId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmObjId.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmObjId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmObjId.Location = new System.Drawing.Point(269, 34);
            this.txtAdmObjId.Name = "txtAdmObjId";
            this.txtAdmObjId.ReadOnly = true;
            this.txtAdmObjId.Size = new System.Drawing.Size(144, 13);
            this.txtAdmObjId.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(109, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 23);
            this.label10.TabIndex = 27;
            this.label10.Text = "WEB POI";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(269, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 23);
            this.label11.TabIndex = 28;
            this.label11.Text = "ADM POI";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWebPlcName
            // 
            this.txtWebPlcName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebPlcName.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebPlcName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebPlcName.Location = new System.Drawing.Point(109, 86);
            this.txtWebPlcName.Name = "txtWebPlcName";
            this.txtWebPlcName.Size = new System.Drawing.Size(151, 13);
            this.txtWebPlcName.TabIndex = 14;
            // 
            // txtWebPOICode
            // 
            this.txtWebPOICode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebPOICode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebPOICode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebPOICode.Location = new System.Drawing.Point(109, 60);
            this.txtWebPOICode.Name = "txtWebPOICode";
            this.txtWebPOICode.ReadOnly = true;
            this.txtWebPOICode.Size = new System.Drawing.Size(151, 13);
            this.txtWebPOICode.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Modified Date :";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Modified User :";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Status :";
            // 
            // txtWebModDate
            // 
            this.txtWebModDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebModDate.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebModDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebModDate.Location = new System.Drawing.Point(109, 244);
            this.txtWebModDate.Name = "txtWebModDate";
            this.txtWebModDate.Size = new System.Drawing.Size(151, 13);
            this.txtWebModDate.TabIndex = 25;
            // 
            // txtWebModUser
            // 
            this.txtWebModUser.AllowDrop = true;
            this.txtWebModUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebModUser.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebModUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebModUser.Location = new System.Drawing.Point(109, 216);
            this.txtWebModUser.Name = "txtWebModUser";
            this.txtWebModUser.Size = new System.Drawing.Size(151, 13);
            this.txtWebModUser.TabIndex = 22;
            // 
            // txtWebStatus
            // 
            this.txtWebStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebStatus.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebStatus.Location = new System.Drawing.Point(109, 190);
            this.txtWebStatus.Name = "txtWebStatus";
            this.txtWebStatus.Size = new System.Drawing.Size(151, 13);
            this.txtWebStatus.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ref Feature :";
            // 
            // txtWebRefType
            // 
            this.txtWebRefType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebRefType.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebRefType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebRefType.Location = new System.Drawing.Point(109, 164);
            this.txtWebRefType.Name = "txtWebRefType";
            this.txtWebRefType.Size = new System.Drawing.Size(151, 13);
            this.txtWebRefType.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 138);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Ref Id :";
            // 
            // txtWebRefId
            // 
            this.txtWebRefId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWebRefId.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWebRefId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebRefId.Location = new System.Drawing.Point(109, 138);
            this.txtWebRefId.Name = "txtWebRefId";
            this.txtWebRefId.Size = new System.Drawing.Size(151, 13);
            this.txtWebRefId.TabIndex = 30;
            // 
            // txtAdmModDate
            // 
            this.txtAdmModDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmModDate.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmModDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmModDate.Location = new System.Drawing.Point(269, 244);
            this.txtAdmModDate.Name = "txtAdmModDate";
            this.txtAdmModDate.Size = new System.Drawing.Size(144, 13);
            this.txtAdmModDate.TabIndex = 26;
            // 
            // txtAdmModUser
            // 
            this.txtAdmModUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmModUser.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmModUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmModUser.Location = new System.Drawing.Point(269, 216);
            this.txtAdmModUser.Name = "txtAdmModUser";
            this.txtAdmModUser.Size = new System.Drawing.Size(144, 13);
            this.txtAdmModUser.TabIndex = 23;
            // 
            // txtAdmStatus
            // 
            this.txtAdmStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmStatus.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmStatus.Location = new System.Drawing.Point(269, 190);
            this.txtAdmStatus.Name = "txtAdmStatus";
            this.txtAdmStatus.Size = new System.Drawing.Size(144, 13);
            this.txtAdmStatus.TabIndex = 21;
            // 
            // txtAdmRefType
            // 
            this.txtAdmRefType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmRefType.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmRefType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmRefType.Location = new System.Drawing.Point(269, 164);
            this.txtAdmRefType.Name = "txtAdmRefType";
            this.txtAdmRefType.Size = new System.Drawing.Size(144, 13);
            this.txtAdmRefType.TabIndex = 19;
            // 
            // txtAdmRefId
            // 
            this.txtAdmRefId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdmRefId.BackColor = System.Drawing.SystemColors.Menu;
            this.txtAdmRefId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdmRefId.Location = new System.Drawing.Point(269, 138);
            this.txtAdmRefId.Name = "txtAdmRefId";
            this.txtAdmRefId.Size = new System.Drawing.Size(144, 13);
            this.txtAdmRefId.TabIndex = 31;
            // 
            // VerificationWebForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 381);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "VerificationWebForm";
            this.Text = "Verification WEB Form";
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblFeatureId;
        private System.Windows.Forms.TextBox txtFeatureId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnRevertFeature;
        private System.Windows.Forms.Button btnVerifyFeature;
        private System.Windows.Forms.Button btnClose;
        //private Controls.SortableListView sortableListView1;
        private System.Windows.Forms.Label lblFeatureName;
        private System.Windows.Forms.TextBox txtStatusVerify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWebObjId;
        private System.Windows.Forms.TextBox txtAdmObjId;
        private System.Windows.Forms.TextBox txtWebPOICode;
        private System.Windows.Forms.TextBox txtAdmPOICode;
        private System.Windows.Forms.TextBox txtWebPlcName;
        private System.Windows.Forms.TextBox txtAdmPlcName;
        private System.Windows.Forms.TextBox txtWebPlcName2;
        private System.Windows.Forms.TextBox txtAdmPlcName2;
        private System.Windows.Forms.TextBox txtWebRefType;
        private System.Windows.Forms.TextBox txtAdmRefType;
        private System.Windows.Forms.TextBox txtWebStatus;
        private System.Windows.Forms.TextBox txtAdmStatus;
        private System.Windows.Forms.TextBox txtWebModUser;
        private System.Windows.Forms.TextBox txtAdmModUser;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txtAdmModDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWebModDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtWebRefId;
        private System.Windows.Forms.TextBox txtAdmRefId;
        //private System.Windows.Forms.Button btnShow1;
    }
}
﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms
{
    public partial class LocationForm2 : BaseForm
    {
        protected SegmentName _segmentName { set; get; }
        protected bool _hideDeleted;

        public string SelectedState
        {
            get
            {
                return (cbState.SelectedItem == null) ? null : ((GState)cbState.SelectedItem).Code;
            }
        }

        public LocationForm2()
            : this(SegmentName.None)
        {
        }

        public LocationForm2(SegmentName segmentName)
        {
            InitializeComponent();
            _segmentName = segmentName;
            _hideDeleted = true;
        }

        private void LocationForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Original Section");
            lvResult.Columns.Add("Original City");
            lvResult.Columns.Add("Original State");
            lvResult.Columns.Add("Del Flag");
            lvResult.Columns.Add("New Flag");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            //ComboBoxUtils.PopulateState(cbState);
            //foreach (GState state in cbState.Items)
            //{
            //    if (state.Segment == _segmentName.ToString())
            //    {
            //        cbState.Text = state.Code;
            //    }
            //}

            //added by asyrul
            switch (_segmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASCity(cbCity);
                    ComboBoxUtils.PopulateASState(cbState);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    ComboBoxUtils.PopulateJHState(cbState);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    ComboBoxUtils.PopulateJPState(cbState);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    ComboBoxUtils.PopulateKGState(cbState);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    ComboBoxUtils.PopulateKKState(cbState);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    ComboBoxUtils.PopulateKNState(cbState);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    ComboBoxUtils.PopulateKVState(cbState);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    ComboBoxUtils.PopulateMKState(cbState);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    ComboBoxUtils.PopulatePGState(cbState);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    ComboBoxUtils.PopulateTGState(cbState);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", _segmentName));
            }

            //added end
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    lvResult.Sorting = SortOrder.None;
                    lvResult.BeginUpdate();
                    lvResult.Items.Clear();
                    DoSearch();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (lvResult.Items.Count > 0)
                {
                    lvResult.Items[0].Selected = true;
                    if (!lvResult.Focused)
                    {
                        lvResult.Focus();
                    }
                }
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual void DoSearch()
        {
            int? id = StringUtils.CheckInt(txtId.Text);
            string section = StringUtils.CheckNull(cbSection.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List <ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory();
            if (id.HasValue)
            {
                GLocation location = repo.GetById<GLocation>(id.Value);
                if (location != null)
                {
                    ListViewItem item = CreateItem(location);
                    items.Add(item);
                }
            }
            else
            {
                Query<GLocation> query = new Query<GLocation>();
                query.MatchType = MatchType.Contains;
                query.Operator = Operator.AND;

                if (!string.IsNullOrEmpty(section))
                {
                    //query.AddClause(() => query.Obj.Section, @"LIKE q'[%" + section + @"%]'");
                    query.AddClause(() => query.Obj.Section, @"LIKE q'[" + section + @"]'");
                }
                else
                {
                    query.Obj.Section = section;
                }

                // noraini - feb 2022 - update search location by segment
                string stateCode = "";
                if (string.IsNullOrEmpty(cbState.Text))
                {
                    stateCode = "'" + GetStateCode(_segmentName, cbState.Items[0].ToString()) + "'";
                    int index = cbState.Items.Count;
                    if (index > 1)
                    {
                        for (int i = 1; i < index; i++)
                        {
                            stateCode = stateCode + ",'" + GetStateCode(_segmentName, cbState.Items[i].ToString()) + "'"; ;
                        }
                    }
                }
                else
                {
                    stateCode = "'" + GetStateCode(_segmentName, cbState.Text.ToString()) + "'";
                }
                query.AddClause(() => query.Obj.State, "IN (" + stateCode + ")");
                // end

                query.Obj.City = city;
                //query.Obj.State = SelectedState;
                //query.Obj.State = state;
                if (_hideDeleted)
                {
                    query.Obj.DelFlag = 0;
                }

                foreach (GLocation location in repo.Search(query, true))
                {
                    ListViewItem item = CreateItem(location);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        protected ListViewItem CreateItem(GLocation location)
        {
            ListViewItem item = new ListViewItem();
            item.Text = location.OID.ToString();
            item.SubItems.Add(location.Section);
            item.SubItems.Add(location.City);
            item.SubItems.Add(location.State);
            item.SubItems.Add(location.OriginalSection);
            item.SubItems.Add(location.OriginalCity);
            item.SubItems.Add(location.OriginalState);
            item.SubItems.Add(location.DelFlagValue);
            item.SubItems.Add(location.NewFlagValue);
            item.SubItems.Add(location.CreatedBy);
            item.SubItems.Add(location.DateCreated);
            item.SubItems.Add(location.UpdatedBy);
            item.SubItems.Add(location.DateUpdated);
            if (location.DelFlag == 1)
            {
                item.BackColor = Color.Red;
                item.ForeColor = Color.White;
            }
            else
            {
                if (location.NewFlag == 1)
                {
                    item.BackColor = Color.Green;
                    item.ForeColor = Color.White;
                }
            }
            item.Tag = location;
            return item;
        }

        public string GetStateCode(SegmentName segmentName, string StateName)
        {
            string stateCode = "";
           
            switch (_segmentName)
            {
                case SegmentName.AS:
                    foreach (GState State in GState.GetAll(false, "AS"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.JH:
                    foreach (GState State in GState.GetAll(false, "JH"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.JP:
                    foreach (GState State in GState.GetAll(false, "JP"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.KG:
                    foreach (GState State in GState.GetAll(false, "KG"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.KK:
                    foreach (GState State in GState.GetAll(false, "KK"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.KN:
                    foreach (GState State in GState.GetAll(false, "KN"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.KV:
                    foreach (GState State in GState.GetAll(false, "KV"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.MK:
                    foreach (GState State in GState.GetAll(false, "MK"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.PG:
                    foreach (GState State in GState.GetAll(false, "PG"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                case SegmentName.TG:
                    foreach (GState State in GState.GetAll(false, "TG"))
                    {
                        if (State.Name == StateName)
                            stateCode = State.Code.ToString();
                    }
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", _segmentName));
            }

            return stateCode;
        }
    }
}

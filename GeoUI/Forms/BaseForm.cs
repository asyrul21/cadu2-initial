﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        protected void LabelColor(Label label, bool valid)
        {
            label.ForeColor = valid ? Color.Black : Color.Red;
        }

        protected void RefreshTitle(string title)
        {
            Text = title;
        }

        protected void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using System.Text.RegularExpressions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms
{
    public partial class UserForm : BaseForm
    {
        public string InsertedUsername
        {
            get { return txtUsername.Text; }
        }

        public string InsertedPassword
        {
            get { return txtPassword.Text; }
        }

        public UserForm()
        {
            InitializeComponent();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                AcceptButton = btnApply;
                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual bool ValidateValues()
        {
            bool pass = true;

            // Name
            bool isNameValid = true;
            isNameValid &= !Regex.IsMatch(txtUsername.Text, @"\s{2,}", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            isNameValid &= !Regex.IsMatch(txtUsername.Text, @"^\s+", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            isNameValid &= !Regex.IsMatch(txtUsername.Text, @"\s+$", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            isNameValid &= !string.IsNullOrEmpty(txtUsername.Text);

            pass &= isNameValid;
            lblUsername.ForeColor = isNameValid ? Color.Black : Color.Red;

            //Password
            bool isPasswordValid = true;
            isPasswordValid &= txtPassword.Text.Length > 0;
            pass &= isPasswordValid;
            lblPassword.ForeColor = isPasswordValid ? Color.Black : Color.Red;

            return pass;
        }
    }
}

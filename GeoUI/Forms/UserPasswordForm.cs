﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Sessions;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;

namespace Geomatic.UI.Forms
{
    public partial class UserPasswordForm : BaseForm
    {
        public UserPasswordForm()
        {
            InitializeComponent();
            RefreshTitle("Change Password");
        }

        private void UserPasswordForm_Load(object sender, EventArgs e)
        {
            if (Session.User == null)
            {
                throw new Exception("No user logged in.");
            }
        }

        private void chkShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            bool show = chkShowPassword.Checked;
            txtOldPassword.PasswordChar = show ? '\0' : '*';
            txtNewPassword.PasswordChar = show ? '\0' : '*';
            txtConfirmPassword.PasswordChar = show ? '\0' : '*';
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            InfoMessageBox box = new InfoMessageBox();
            box.SetCaption("Change Password")
               .SetButtons(MessageBoxButtons.OK);
            string oldPassword = txtOldPassword.Text;
            string newPassword = txtNewPassword.Text;
            string confirmPassword = txtConfirmPassword.Text;
            if (Session.User.Password != oldPassword)
            {
                box.SetText("Your old password is incorrect.")
                    .Show(this);
                return;
            }
            if (!GUser.PasswordFormat.IsMatch(newPassword))
            {
                box.SetText("Your new password must be minimum 6 alphanumeric.")
                    .Show(this);
                return;
            }
            if (newPassword != confirmPassword)
            {
                box.SetText("Your new password and confirm password do not match.")
                    .Show(this);
                return;
            }

            Session.User.Password = newPassword;
            RepositoryFactory repo = new RepositoryFactory();
            repo.Update(Session.User);

            box.SetText("Password updated.")
                .Show(this);
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Controls;
using Geomatic.UI.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms
{
    public partial class JunctionANDForm : CollapsibleForm
    {
        protected GJunctionAND _junctionAND;
        protected int? _prevSelectedType;

        public int? SelectedType
        {
            get
            {
                return (cbType.SelectedItem == null) ? (int?)null : ((GJunctionType)cbType.SelectedItem).Code;
            }
            set
            {
                GJunctionType junctionType = GJunctionType.Get(value);
                cbType.Text = StringUtils.TrimSpaces((junctionType == null) ? string.Empty : junctionType.Name);
            }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public JunctionANDForm()
            : this(null)
        {
        }

        public JunctionANDForm(GJunctionAND junctionAND)
        {
            InitializeComponent();
            _junctionAND = junctionAND;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateJunctionType(cbType);
            ComboBoxUtils.PopulateSource(cbSource);

            txtId.Text = _junctionAND.OriId.ToString();

            txtName.Text = _junctionAND.Name;
            cbType.Text = _junctionAND.TypeValue;
            cbSource.Text = _junctionAND.SourceValue;

            txtDateCreated.Text = _junctionAND.DateCreated;
            txtDateUpdated.Text = _junctionAND.DateUpdated;
            txtCreator.Text = _junctionAND.CreatedBy;
            txtUpdater.Text = _junctionAND.UpdatedBy;


            controlRoute.Columns.Add("Id");
            controlRoute.Columns.Add("Type");
            controlRoute.Columns.Add("In Toll Segment");
            controlRoute.Columns.Add("In Toll Id");
            controlRoute.Columns.Add("In Toll Name");
            controlRoute.Columns.Add("Out Toll Segment");
            controlRoute.Columns.Add("Out Toll Id");
            controlRoute.Columns.Add("Out Toll Name");
            controlRoute.Columns.Add("Created By");
            controlRoute.Columns.Add("Date Created");
            controlRoute.Columns.Add("Updated By");
            controlRoute.Columns.Add("Date Updated");

            PopulateRoute();

            if (_junctionAND.Type != GJunctionType.TOLL_GATE && _junctionAND.Type != GJunctionType.IN_TOLL && _junctionAND.Type != GJunctionType.OUT_TOLL)
            {
                EnableAdditionalProperties(false);
            }
        }

        private void EnableAdditionalProperties(bool enable)
        {
            additionalGroup.Enabled = enable;
            additionalGroup.IsCollapsed = !enable;
            group_OnCollapsedChanged(null, null);
        }

        private void PopulateRoute()
        {
            controlRoute.BeginUpdate();
            controlRoute.Sorting = SortOrder.None;
            controlRoute.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();
            List<GTollRoute> tollRoutes = _junctionAND.GetTollRoutes().ToList();

            foreach (GTollRoute tollRoute in tollRoutes)
            {
                ListViewItem item = CreateItem(tollRoute);
                items.Add(item);
            }
            controlRoute.Items.AddRange(items.ToArray());
            controlRoute.FixColumnWidth();
            controlRoute.EndUpdate();
        }

        private ListViewItem CreateItem(GTollRoute tollRoute)
        {
            ListViewItem item = new ListViewItem();
            item.Text = tollRoute.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(tollRoute.TypeValue));
            item.SubItems.Add(tollRoute.InSegment);
            item.SubItems.Add(tollRoute.InID.ToString());
            item.SubItems.Add(tollRoute.GetInName());
            item.SubItems.Add(tollRoute.OutSegment);
            item.SubItems.Add(tollRoute.OutID.ToString());
            item.SubItems.Add(tollRoute.GetOutName());
            item.SubItems.Add(tollRoute.CreatedBy);
            item.SubItems.Add(tollRoute.DateCreated);
            item.SubItems.Add(tollRoute.UpdatedBy);
            item.SubItems.Add(tollRoute.DateUpdated);
            item.Tag = tollRoute;
            return item;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Type
            bool isTypeValid = JunctionValidator.CheckType(SelectedType);
            LabelColor(lblType, isTypeValid);
            pass &= isTypeValid;

            // Name
            bool isNameValid = JunctionValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Source
            bool isSourceValid = JunctionValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _junctionAND.Type = SelectedType;
            _junctionAND.Name = InsertedName;
            _junctionAND.Source = SelectedSource;
        }

        private void controlRoute_AddClick(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(_junctionAND.SegmentName);
            GTollRoute newTollRoute = repo.NewObj<GTollRoute>();
            newTollRoute = repo.Insert(newTollRoute, false);

            if (SelectedType == GJunctionType.OUT_TOLL)
            {
                newTollRoute.OutSegment = _junctionAND.SegmentName.ToString();
                newTollRoute.OutID = _junctionAND.OID;
                newTollRoute.Type = 1; //Close Toll
            }
            else
            {
                newTollRoute.InSegment = _junctionAND.SegmentName.ToString();
                newTollRoute.InID = _junctionAND.OID;
                newTollRoute.Type = 0; //Open Toll
            }

            using (AddTollRouteForm form = new AddTollRouteForm(newTollRoute, SelectedType))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    repo.Delete(newTollRoute);
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(newTollRoute);

                    ListViewItem item = new ListViewItem();
                    item.Text = newTollRoute.OID.ToString();
                    item.SubItems.Add(StringUtils.TrimSpaces(newTollRoute.TypeValue));
                    item.SubItems.Add(newTollRoute.InSegment);
                    item.SubItems.Add(newTollRoute.InID.ToString());
                    item.SubItems.Add(newTollRoute.GetInName());
                    item.SubItems.Add(newTollRoute.OutSegment);
                    item.SubItems.Add(newTollRoute.OutID.ToString());
                    item.SubItems.Add(newTollRoute.GetOutName());
                    item.SubItems.Add(newTollRoute.CreatedBy);
                    item.SubItems.Add(newTollRoute.DateCreated);
                    item.SubItems.Add(newTollRoute.UpdatedBy);
                    item.SubItems.Add(newTollRoute.DateUpdated);
                    item.Tag = newTollRoute;
                    controlRoute.Items.Add(item);
                    controlRoute.FixColumnWidth();
                }
            }
        }

        private void controlRoute_EditClick(object sender, EventArgs e)
        {
            if (controlRoute.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_junctionAND.SegmentName);
            ListViewItem item = controlRoute.SelectedItems[0];
            GTollRoute tollRoute = (GTollRoute)item.Tag;

            using (EditTollRouteForm form = new EditTollRouteForm(tollRoute, SelectedType))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(tollRoute);

                    item.SubItems.Clear();
                    item.Text = tollRoute.OID.ToString();
                    item.SubItems.Add(StringUtils.TrimSpaces(tollRoute.TypeValue));
                    item.SubItems.Add(tollRoute.InSegment);
                    item.SubItems.Add(tollRoute.InID.ToString());
                    item.SubItems.Add(tollRoute.GetInName());
                    item.SubItems.Add(tollRoute.OutSegment);
                    item.SubItems.Add(tollRoute.OutID.ToString());
                    item.SubItems.Add(tollRoute.GetOutName());
                    item.SubItems.Add(tollRoute.CreatedBy);
                    item.SubItems.Add(tollRoute.DateCreated);
                    item.SubItems.Add(tollRoute.UpdatedBy);
                    item.SubItems.Add(tollRoute.DateUpdated);
                    item.Tag = tollRoute;
                    controlRoute.FixColumnWidth();
                }
            }
        }

        private void controlRoute_DeleteClick(object sender, EventArgs e)
        {
            if (controlRoute.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_junctionAND.SegmentName);
            ListViewItem item = controlRoute.SelectedItems[0];
            GTollRoute tollRoute = (GTollRoute)item.Tag;

            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                          .SetDefaultButton(MessageBoxDefaultButton.Button2)
                          .SetCaption("Delete Toll Route")
                          .SetText("Are you sure you want to delete this toll route?\nId: {0}", tollRoute.OID);

                if (confirmBox.Show() == DialogResult.No)
                {
                    return;
                }
            }

            using (new WaitCursor())
            {

                foreach (GTollPrice price in tollRoute.GetPrices())
                {
                    repo.Delete(price);
                }
                repo.Delete(tollRoute);
                item.Remove();
                controlRoute.FixColumnWidth();
            }
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (SelectedType)
            {
                case GJunctionType.IN_TOLL:
                    if (_junctionAND.HasOutTollRoute())
                    {
                        SelectedType = _prevSelectedType;
                        throw new QualityControlException("Invalid Junction type. This junction used for out toll.");
                    }
                    EnableAdditionalProperties(true);
                    break;
                case GJunctionType.OUT_TOLL:
                    if (_junctionAND.HasInTollRoute())
                    {
                        SelectedType = _prevSelectedType;
                        throw new QualityControlException("Invalid Junction type. This junction used for in toll.");
                    }
                    EnableAdditionalProperties(true);
                    break;
                case GJunctionType.TOLL_GATE:
                    EnableAdditionalProperties(true);
                    break;
                default:
                    if (_junctionAND.HasTollRoute())
                    {
                        SelectedType = _prevSelectedType;
                        throw new QualityControlException("Invalid Junction type. This junction has toll routes.");
                    }
                    EnableAdditionalProperties(false);
                    break;
            }
            _prevSelectedType = SelectedType;
        }
    }
}

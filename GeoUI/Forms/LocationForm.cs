﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;
using Geomatic.UI.Controls;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms
{
    public partial class LocationForm : CollapsibleForm
    {
        public string InsertedSection
        {
            get { return cbSection.Text; }
        }

        public string InsertedCity
        {
            get { return cbCity.Text; }
        }

        //public string SelectedState
        //{
        //    get { return (cbState.SelectedItem == null) ? null : ((GState)cbState.SelectedItem).Code; }
        //}

        public string SelectedState
        {
            get { return (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null; }
        }

        //added by asyrul
        public int IsCheckedForDeletion
        {
            get { return (chkDeleted.Checked) ? 1 : 0; }
        }
        //added end

        public LocationForm()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateState(cbState);
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
           OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // City
            pass &= !string.IsNullOrEmpty(InsertedCity);
            lblCity.ForeColor = InsertedCity != "" ? Color.Black : Color.Red;

            // State
            pass &= !string.IsNullOrEmpty(SelectedState);
            lblState.ForeColor = !string.IsNullOrEmpty(SelectedState) ? Color.Black : Color.Red;

            return pass;
        }
    }
}

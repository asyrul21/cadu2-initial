﻿namespace Geomatic.UI.Forms
{
    partial class BuildingANDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.lblPropertyId = new System.Windows.Forms.Label();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pagePoi = new System.Windows.Forms.TabPage();
            this.controlPoi = new Geomatic.UI.Controls.ChildControl();
            this.pageMultiStorey = new System.Windows.Forms.TabPage();
            this.controlMultiStorey = new Geomatic.UI.Controls.ChildControl();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblConstructionStatus = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.btnSetCode = new System.Windows.Forms.Button();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.cbUnit = new System.Windows.Forms.ComboBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.txtSpace = new System.Windows.Forms.TextBox();
            this.lblSpace = new System.Windows.Forms.Label();
            this.lblTotalForecastUnit = new System.Windows.Forms.Label();
            this.totalForecastUnit = new Geomatic.UI.Controls.IntUpDown();
            this.lblForecastSource = new System.Windows.Forms.Label();
            this.cbForecastSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblNumOfFloor = new System.Windows.Forms.Label();
            this.numOfFloor = new Geomatic.UI.Controls.IntUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotalApt = new System.Windows.Forms.TextBox();
            this.categoryControl = new Geomatic.UI.Controls.CodeControl();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pagePoi.SuspendLayout();
            this.pageMultiStorey.SuspendLayout();
            this.addressGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalForecastUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloor)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 612);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel2);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 542);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 12;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(554, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.tableLayoutPanel4);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 498);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(560, 44);
            this.relationGroup.TabIndex = 11;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.txtPropertyId, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblPropertyId, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(554, 25);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPropertyId.BackColor = System.Drawing.SystemColors.Control;
            this.txtPropertyId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPropertyId.Location = new System.Drawing.Point(113, 3);
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.ReadOnly = true;
            this.txtPropertyId.Size = new System.Drawing.Size(161, 20);
            this.txtPropertyId.TabIndex = 1;
            // 
            // lblPropertyId
            // 
            this.lblPropertyId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPropertyId.AutoSize = true;
            this.lblPropertyId.Location = new System.Drawing.Point(46, 6);
            this.lblPropertyId.Name = "lblPropertyId";
            this.lblPropertyId.Size = new System.Drawing.Size(61, 13);
            this.lblPropertyId.TabIndex = 0;
            this.lblPropertyId.Text = "Property Id:";
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabAdditional);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 301);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(560, 197);
            this.additionalGroup.TabIndex = 10;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Controls.Add(this.pagePoi);
            this.tabAdditional.Controls.Add(this.pageMultiStorey);
            this.tabAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAdditional.Location = new System.Drawing.Point(3, 16);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(554, 178);
            this.tabAdditional.TabIndex = 0;
            // 
            // pagePoi
            // 
            this.pagePoi.Controls.Add(this.controlPoi);
            this.pagePoi.Location = new System.Drawing.Point(4, 22);
            this.pagePoi.Name = "pagePoi";
            this.pagePoi.Padding = new System.Windows.Forms.Padding(3);
            this.pagePoi.Size = new System.Drawing.Size(546, 152);
            this.pagePoi.TabIndex = 1;
            this.pagePoi.Text = "POI";
            this.pagePoi.UseVisualStyleBackColor = true;
            // 
            // controlPoi
            // 
            this.controlPoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPoi.Location = new System.Drawing.Point(3, 3);
            this.controlPoi.Name = "controlPoi";
            this.controlPoi.Size = new System.Drawing.Size(540, 146);
            this.controlPoi.TabIndex = 0;
            // 
            // pageMultiStorey
            // 
            this.pageMultiStorey.Controls.Add(this.controlMultiStorey);
            this.pageMultiStorey.Location = new System.Drawing.Point(4, 22);
            this.pageMultiStorey.Name = "pageMultiStorey";
            this.pageMultiStorey.Padding = new System.Windows.Forms.Padding(3);
            this.pageMultiStorey.Size = new System.Drawing.Size(546, 152);
            this.pageMultiStorey.TabIndex = 0;
            this.pageMultiStorey.Text = "Multi Storey Address";
            this.pageMultiStorey.UseVisualStyleBackColor = true;
            // 
            // controlMultiStorey
            // 
            this.controlMultiStorey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlMultiStorey.Location = new System.Drawing.Point(3, 3);
            this.controlMultiStorey.MultiSelect = true;
            this.controlMultiStorey.Name = "controlMultiStorey";
            this.controlMultiStorey.Size = new System.Drawing.Size(540, 146);
            this.controlMultiStorey.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.controlMultiStorey.TabIndex = 0;
            this.controlMultiStorey.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlMultiStorey_AddClick);
            this.controlMultiStorey.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlMultiStorey_EditClick);
            this.controlMultiStorey.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlMultiStorey_DeleteClick);
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.tableLayoutPanel3);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = true;
            this.addressGroup.Location = new System.Drawing.Point(12, 281);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(560, 20);
            this.addressGroup.TabIndex = 9;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtHouse, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtLot, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtPostcode, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetType, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetType, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblSection, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtSection, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblCity, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtState, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtCity, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblState, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblPostcode, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName2, 3, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(554, 1);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(398, 3);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.ReadOnly = true;
            this.txtHouse.Size = new System.Drawing.Size(153, 20);
            this.txtHouse.TabIndex = 3;
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(113, 3);
            this.txtLot.Name = "txtLot";
            this.txtLot.ReadOnly = true;
            this.txtLot.Size = new System.Drawing.Size(153, 20);
            this.txtLot.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(351, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 1);
            this.label2.TabIndex = 2;
            this.label2.Text = "House:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 1);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lot:";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(398, 3);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(153, 20);
            this.txtPostcode.TabIndex = 13;
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(398, 3);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(153, 20);
            this.txtStreetName.TabIndex = 7;
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 3);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(153, 20);
            this.txtStreetType.TabIndex = 5;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(42, 0);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 1);
            this.lblStreetType.TabIndex = 4;
            this.lblStreetType.Text = "Street Type:";
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(323, 0);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 1);
            this.lblStreetName.TabIndex = 6;
            this.lblStreetName.Text = "Street Name:";
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(317, 0);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 1);
            this.lblStreetName2.TabIndex = 8;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 0);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 1);
            this.lblSection.TabIndex = 10;
            this.lblSection.Text = "Section:";
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 3);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(153, 20);
            this.txtSection.TabIndex = 11;
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 0);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 1);
            this.lblCity.TabIndex = 14;
            this.lblCity.Text = "City:";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(398, 3);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(153, 20);
            this.txtState.TabIndex = 17;
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 3);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(153, 20);
            this.txtCity.TabIndex = 15;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(357, 0);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 1);
            this.lblState.TabIndex = 16;
            this.lblState.Text = "State:";
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(337, 0);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 1);
            this.lblPostcode.TabIndex = 12;
            this.lblPostcode.Text = "Postcode:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(398, 3);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(153, 20);
            this.txtStreetName2.TabIndex = 9;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.txtName2, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtName, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblConstructionStatus, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lblCode, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lblName, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lblName2, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.cbConstructionStatus, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.btnSetCode, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblNaviStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cbUnit, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblUnit, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.txtSpace, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblSpace, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.lblTotalForecastUnit, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.totalForecastUnit, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.lblForecastSource, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.cbForecastSource, 3, 5);
            this.tableLayoutPanel.Controls.Add(this.lblNumOfFloor, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.numOfFloor, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.txtTotalApt, 1, 6);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 97);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 7;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 184);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(401, 107);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(156, 21);
            this.cbSource.TabIndex = 18;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(351, 110);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 17;
            this.lblSource.Text = "Source:";
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(401, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(156, 21);
            this.cbNaviStatus.TabIndex = 3;
            // 
            // txtName2
            // 
            this.txtName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName2.BackColor = System.Drawing.SystemColors.Window;
            this.txtName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName2.Location = new System.Drawing.Point(401, 55);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(156, 20);
            this.txtName2.TabIndex = 10;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(113, 55);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(156, 20);
            this.txtName.TabIndex = 8;
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCode.Location = new System.Drawing.Point(113, 29);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(156, 20);
            this.txtCode.TabIndex = 5;
            // 
            // lblConstructionStatus
            // 
            this.lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblConstructionStatus.AutoSize = true;
            this.lblConstructionStatus.Location = new System.Drawing.Point(5, 6);
            this.lblConstructionStatus.Name = "lblConstructionStatus";
            this.lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            this.lblConstructionStatus.TabIndex = 0;
            this.lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblCode
            // 
            this.lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(72, 32);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(35, 13);
            this.lblCode.TabIndex = 4;
            this.lblCode.Text = "Code:";
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(69, 58);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 7;
            this.lblName.Text = "Name:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(351, 58);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(44, 13);
            this.lblName2.TabIndex = 9;
            this.lblName2.Text = "Name2:";
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(113, 3);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(156, 21);
            this.cbConstructionStatus.TabIndex = 1;
            // 
            // btnSetCode
            // 
            this.btnSetCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSetCode.Location = new System.Drawing.Point(275, 33);
            this.btnSetCode.Name = "btnSetCode";
            this.btnSetCode.Size = new System.Drawing.Size(32, 12);
            this.btnSetCode.TabIndex = 6;
            this.btnSetCode.Text = "...";
            this.btnSetCode.UseVisualStyleBackColor = true;
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(301, 6);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 2;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // cbUnit
            // 
            this.cbUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUnit.FormattingEnabled = true;
            this.cbUnit.Location = new System.Drawing.Point(401, 81);
            this.cbUnit.Name = "cbUnit";
            this.cbUnit.Size = new System.Drawing.Size(156, 21);
            this.cbUnit.TabIndex = 14;
            // 
            // lblUnit
            // 
            this.lblUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(366, 84);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(29, 13);
            this.lblUnit.TabIndex = 13;
            this.lblUnit.Text = "Unit:";
            // 
            // txtSpace
            // 
            this.txtSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSpace.BackColor = System.Drawing.SystemColors.Window;
            this.txtSpace.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpace.Location = new System.Drawing.Point(113, 81);
            this.txtSpace.Name = "txtSpace";
            this.txtSpace.Size = new System.Drawing.Size(156, 20);
            this.txtSpace.TabIndex = 12;
            // 
            // lblSpace
            // 
            this.lblSpace.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSpace.AutoSize = true;
            this.lblSpace.Location = new System.Drawing.Point(66, 84);
            this.lblSpace.Name = "lblSpace";
            this.lblSpace.Size = new System.Drawing.Size(41, 13);
            this.lblSpace.TabIndex = 11;
            this.lblSpace.Text = "Space:";
            // 
            // lblTotalForecastUnit
            // 
            this.lblTotalForecastUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTotalForecastUnit.AutoSize = true;
            this.lblTotalForecastUnit.Location = new System.Drawing.Point(6, 136);
            this.lblTotalForecastUnit.Name = "lblTotalForecastUnit";
            this.lblTotalForecastUnit.Size = new System.Drawing.Size(101, 13);
            this.lblTotalForecastUnit.TabIndex = 21;
            this.lblTotalForecastUnit.Text = "Forecast Num Unit :";
            // 
            // totalForecastUnit
            // 
            this.totalForecastUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.totalForecastUnit.BackColor = System.Drawing.SystemColors.Window;
            this.totalForecastUnit.Location = new System.Drawing.Point(113, 133);
            this.totalForecastUnit.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.totalForecastUnit.Name = "totalForecastUnit";
            this.totalForecastUnit.Size = new System.Drawing.Size(156, 20);
            this.totalForecastUnit.TabIndex = 22;
            this.totalForecastUnit.Value = 0;
            // 
            // lblForecastSource
            // 
            this.lblForecastSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblForecastSource.AutoSize = true;
            this.lblForecastSource.Location = new System.Drawing.Point(278, 136);
            this.lblForecastSource.Name = "lblForecastSource";
            this.lblForecastSource.Size = new System.Drawing.Size(117, 13);
            this.lblForecastSource.TabIndex = 23;
            this.lblForecastSource.Text = "MSA Forecast Source :";
            // 
            // cbForecastSource
            // 
            this.cbForecastSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbForecastSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbForecastSource.FormattingEnabled = true;
            this.cbForecastSource.Location = new System.Drawing.Point(401, 133);
            this.cbForecastSource.Name = "cbForecastSource";
            this.cbForecastSource.Size = new System.Drawing.Size(156, 21);
            this.cbForecastSource.TabIndex = 24;
            // 
            // lblNumOfFloor
            // 
            this.lblNumOfFloor.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNumOfFloor.AutoSize = true;
            this.lblNumOfFloor.Location = new System.Drawing.Point(49, 110);
            this.lblNumOfFloor.Name = "lblNumOfFloor";
            this.lblNumOfFloor.Size = new System.Drawing.Size(58, 13);
            this.lblNumOfFloor.TabIndex = 15;
            this.lblNumOfFloor.Text = "Num Floor:";
            // 
            // numOfFloor
            // 
            this.numOfFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloor.Location = new System.Drawing.Point(113, 107);
            this.numOfFloor.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numOfFloor.Name = "numOfFloor";
            this.numOfFloor.Size = new System.Drawing.Size(156, 20);
            this.numOfFloor.TabIndex = 16;
            this.numOfFloor.Value = 0;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Floor Num Unit :";
            // 
            // txtTotalApt
            // 
            this.txtTotalApt.Location = new System.Drawing.Point(113, 159);
            this.txtTotalApt.Name = "txtTotalApt";
            this.txtTotalApt.ReadOnly = true;
            this.txtTotalApt.Size = new System.Drawing.Size(156, 20);
            this.txtTotalApt.TabIndex = 20;
            // 
            // categoryControl
            // 
            this.categoryControl.Code1Text = "";
            this.categoryControl.Code2Text = "";
            this.categoryControl.Code3Text = "";
            this.categoryControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.categoryControl.Location = new System.Drawing.Point(12, 51);
            this.categoryControl.Name = "categoryControl";
            this.categoryControl.Size = new System.Drawing.Size(560, 46);
            this.categoryControl.TabIndex = 7;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(60, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Original Id :";
            // 
            // BuildingANDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 738);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.categoryControl);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "BuildingANDForm";
            this.Text = "BuildingForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.categoryControl, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pagePoi.ResumeLayout(false);
            this.pageMultiStorey.ResumeLayout(false);
            this.addressGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalForecastUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        protected System.Windows.Forms.TextBox txtPropertyId;
        protected System.Windows.Forms.Label lblPropertyId;
        protected Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pageMultiStorey;
        protected Controls.ChildControl controlMultiStorey;
        protected System.Windows.Forms.TabPage pagePoi;
        protected Controls.ChildControl controlPoi;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.TextBox txtHouse;
        protected System.Windows.Forms.TextBox txtLot;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.Label lblSource;
        protected Controls.UpperComboBox cbNaviStatus;
        protected System.Windows.Forms.TextBox txtName2;
        protected System.Windows.Forms.TextBox txtName;
        protected System.Windows.Forms.TextBox txtCode;
        protected System.Windows.Forms.Label lblConstructionStatus;
        protected System.Windows.Forms.Label lblCode;
        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.Label lblName2;
        protected Controls.UpperComboBox cbConstructionStatus;
        protected System.Windows.Forms.Button btnSetCode;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected System.Windows.Forms.ComboBox cbUnit;
        protected System.Windows.Forms.Label lblUnit;
        protected Controls.IntUpDown numOfFloor;
        protected System.Windows.Forms.TextBox txtSpace;
        protected System.Windows.Forms.Label lblNumOfFloor;
        protected System.Windows.Forms.Label lblSpace;
        protected Controls.CodeControl categoryControl;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotalApt;
        private System.Windows.Forms.Label lblTotalForecastUnit;
        private Controls.IntUpDown totalForecastUnit;
        private System.Windows.Forms.Label lblForecastSource;
        private Controls.UpperComboBox cbForecastSource;
    }
}
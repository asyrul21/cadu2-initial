﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms
{
    public partial class SectionBoundaryForm : CollapsibleForm
    {
        #region Properties

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string InsertedCity
        {
            get { return txtCity.Text; }
        }

        public string InsertedState
        {
            get { return txtState.Text; }
        }

        #endregion

        protected GSectionBoundary _sectionBoundary;

        public SectionBoundaryForm()
            : this(null)
        {
        }

        public SectionBoundaryForm(GSectionBoundary sectionBoundary)
        {
            InitializeComponent();
            _sectionBoundary = sectionBoundary;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateSource(cbSource);

            // section boundary
            txtId.Text = _sectionBoundary.OID.ToString();
            txtName.Text = _sectionBoundary.Name;
            txtCity.Text = _sectionBoundary.City;
            txtState.Text = _sectionBoundary.State;
            cbSource.Text = _sectionBoundary.SourceValue;
        }

        private void btnPickLocation_Click(object sender, EventArgs e)
        {
            using (ChooseLocationForm form = new ChooseLocationForm(_sectionBoundary.SegmentName))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GLocation location = form.SelectedLocation;
                txtName.Text = location.Section;
                txtCity.Text = location.City;
                txtState.Text = location.State;
            }
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Name
            bool isNameValid = SectionBoundaryValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // City
            bool isCityValid = SectionBoundaryValidator.CheckCity(txtCity.Text);
            LabelColor(lblCity, isCityValid);
            pass &= isCityValid;

            // State
            bool isStateValid = SectionBoundaryValidator.CheckState(txtState.Text);
            LabelColor(lblState, isStateValid);
            pass &= isStateValid;

            // Source
            bool isSourceValid = SectionBoundaryValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _sectionBoundary.Name = InsertedName;
            _sectionBoundary.City = InsertedCity;
            _sectionBoundary.State = InsertedState;
            _sectionBoundary.Source = SelectedSource;
        }
    }
}

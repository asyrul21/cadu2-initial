﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.UI.Forms
{
    public partial class ChooseLayerForm : BaseForm
    {
        public IEnumerable<IDataset> SelectedDatasets
        {
            get
            {
                foreach (ListViewItem item in lvLayers.Items)
                {
                    if (!item.Checked)
                    {
                        continue;
                    }
                    IDataset dataset = item.Tag as IDataset;
                    if (dataset != null)
                    {
                        yield return dataset;
                    }
                }
            }
        }

        public ChooseLayerForm()
        {
            InitializeComponent();
            RefreshTitle("Choose Layer");
        }

        public void LoadDataset(List<IDataset> datasets)
        {
            lvLayers.BeginUpdate();
            lvLayers.Sorting = SortOrder.None;
            lvLayers.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();
            foreach (IDataset dataset in datasets)
            {
                ListViewItem item = new ListViewItem();
                item.Text = dataset.Name;
                item.Tag = dataset;
                items.Add(item);
            }
            lvLayers.Items.AddRange(items.ToArray());

            lvLayers.FixColumnWidth();
            lvLayers.EndUpdate();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                lvLayers.BeginUpdate();
                foreach (ListViewItem item in lvLayers.Items)
                {
                    item.Checked = true;
                }
                lvLayers.EndUpdate();
            }
        }

        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                lvLayers.BeginUpdate();
                foreach (ListViewItem item in lvLayers.Items)
                {
                    item.Checked = false;
                }
                lvLayers.EndUpdate();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

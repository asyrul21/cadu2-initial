﻿using Geomatic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.OpenMap
{
    interface IOptionPanel
    {
        bool Visible { set; get; }
        IEnumerable<SegmentName> Segments { get; }
    }
}

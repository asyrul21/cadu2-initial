﻿using Geomatic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.OpenMap
{
    public partial class SingleSegmentPanel : OptionPanel
    {
        public override IEnumerable<SegmentName> Segments
        {
            get
            {
                return _segments;
            }
        }

        private List<SegmentName> _segments;

        public SingleSegmentPanel()
        {
            InitializeComponent();
            _segments = new List<SegmentName>();
        }

        private void rbSegment_CheckedChanged(object sender, EventArgs e)
        {
            _segments.Clear();
            if (rbAs.Checked)
            {
                _segments.Add(SegmentName.AS);
            }
            else if (rbJh.Checked)
            {
                _segments.Add(SegmentName.JH);
            }
            else if (rbJp.Checked)
            {
                _segments.Add(SegmentName.JP);
            }
            else if (rbKg.Checked)
            {
                _segments.Add(SegmentName.KG);
            }
            else if (rbKk.Checked)
            {
                _segments.Add(SegmentName.KK);
            }
            else if (rbKn.Checked)
            {
                _segments.Add(SegmentName.KN);
            }
            else if (rbKv.Checked)
            {
                _segments.Add(SegmentName.KV);
            }
            else if (rbMk.Checked)
            {
                _segments.Add(SegmentName.MK);
            }
            else if (rbPg.Checked)
            {
                _segments.Add(SegmentName.PG);
            }
            else if (rbTg.Checked)
            {
                _segments.Add(SegmentName.TG);
            }
            else
            {
                throw new Exception("Unknown Segment.");
            }
        }
    }
}

﻿using Geomatic.Core;
using Geomatic.UI.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.OpenMap
{
    public partial class OpenMapForm : BaseForm
    {
        public IEnumerable<SegmentName> Segments
        {
            get
            {
                return (SelectedOption == null) ? null : SelectedOption.Value.Segments;
            }
        }

        private ComboBoxItem<string, IOptionPanel> SelectedOption
        {
            get
            {
                return cbOption.SelectedItem as ComboBoxItem<string, IOptionPanel>;
            }
        }

        public OpenMapForm()
        {
            InitializeComponent();
            RefreshTitle("Open Map");
            PopulateType();
        }

        private void OpenMapForm_Load(object sender, EventArgs e)
        {
            cbOption.SelectedIndex = 0;
        }

        private void PopulateType()
        {
            AddPanel("Single Segment", new SingleSegmentPanel());
            AddPanel("Multiple Segments", new MultipleSegmentsPanel());
        }

        private void AddPanel(string key, IOptionPanel optionPanel)
        {
            if (!(optionPanel is Control))
            {
                throw new Exception("Not a control");
            }
            cbOption.Items.Add(new ComboBoxItem<string, IOptionPanel>(key, optionPanel));
            Control control = (Control)optionPanel;
            control.Dock = DockStyle.Fill;
            panel.Controls.Add(control);
        }

        private void cbOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ComboBoxItem<string, IOptionPanel> item in cbOption.Items)
            {
                item.Value.Visible = (item == SelectedOption);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (Segments.Count() > 0)
            {
                DialogResult = DialogResult.OK;
                // noraini - initialize value for pick location
                Settings.Default.PickedSectionIndex = -1;
                Settings.Default.PickedCityIndex = -1;
                Settings.Default.PickedStateIndex = -1;
                // end
            }
        }
    }
}

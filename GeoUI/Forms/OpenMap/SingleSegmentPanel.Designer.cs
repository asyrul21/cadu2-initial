﻿namespace Geomatic.UI.Forms.OpenMap
{
    partial class SingleSegmentPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupSegment = new System.Windows.Forms.GroupBox();
            this.rbTg = new System.Windows.Forms.RadioButton();
            this.rbPg = new System.Windows.Forms.RadioButton();
            this.rbMk = new System.Windows.Forms.RadioButton();
            this.rbKv = new System.Windows.Forms.RadioButton();
            this.rbKn = new System.Windows.Forms.RadioButton();
            this.rbKk = new System.Windows.Forms.RadioButton();
            this.rbKg = new System.Windows.Forms.RadioButton();
            this.rbJp = new System.Windows.Forms.RadioButton();
            this.rbJh = new System.Windows.Forms.RadioButton();
            this.rbAs = new System.Windows.Forms.RadioButton();
            this.groupSegment.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupSegment
            // 
            this.groupSegment.Controls.Add(this.rbTg);
            this.groupSegment.Controls.Add(this.rbPg);
            this.groupSegment.Controls.Add(this.rbMk);
            this.groupSegment.Controls.Add(this.rbKv);
            this.groupSegment.Controls.Add(this.rbKn);
            this.groupSegment.Controls.Add(this.rbKk);
            this.groupSegment.Controls.Add(this.rbKg);
            this.groupSegment.Controls.Add(this.rbJp);
            this.groupSegment.Controls.Add(this.rbJh);
            this.groupSegment.Controls.Add(this.rbAs);
            this.groupSegment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupSegment.Location = new System.Drawing.Point(0, 0);
            this.groupSegment.Name = "groupSegment";
            this.groupSegment.Size = new System.Drawing.Size(300, 300);
            this.groupSegment.TabIndex = 0;
            this.groupSegment.TabStop = false;
            this.groupSegment.Text = "Select a Segment";
            // 
            // rbTg
            // 
            this.rbTg.AutoSize = true;
            this.rbTg.Location = new System.Drawing.Point(43, 246);
            this.rbTg.Name = "rbTg";
            this.rbTg.Size = new System.Drawing.Size(161, 17);
            this.rbTg.TabIndex = 9;
            this.rbTg.TabStop = true;
            this.rbTg.Text = "TG - Terengganu && Kelantan";
            this.rbTg.UseVisualStyleBackColor = true;
            this.rbTg.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbPg
            // 
            this.rbPg.AutoSize = true;
            this.rbPg.Location = new System.Drawing.Point(43, 222);
            this.rbPg.Name = "rbPg";
            this.rbPg.Size = new System.Drawing.Size(86, 17);
            this.rbPg.TabIndex = 8;
            this.rbPg.TabStop = true;
            this.rbPg.Text = "PG - Penang";
            this.rbPg.UseVisualStyleBackColor = true;
            this.rbPg.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbMk
            // 
            this.rbMk.AutoSize = true;
            this.rbMk.Location = new System.Drawing.Point(43, 198);
            this.rbMk.Name = "rbMk";
            this.rbMk.Size = new System.Drawing.Size(174, 17);
            this.rbMk.TabIndex = 7;
            this.rbMk.TabStop = true;
            this.rbMk.Text = "MK - Melaka && Negeri Sembilan";
            this.rbMk.UseVisualStyleBackColor = true;
            this.rbMk.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbKv
            // 
            this.rbKv.AutoSize = true;
            this.rbKv.Location = new System.Drawing.Point(43, 174);
            this.rbKv.Name = "rbKv";
            this.rbKv.Size = new System.Drawing.Size(167, 17);
            this.rbKv.TabIndex = 6;
            this.rbKv.TabStop = true;
            this.rbKv.Text = "KV - Kuala Lumpur && Selangor";
            this.rbKv.UseVisualStyleBackColor = true;
            this.rbKv.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbKn
            // 
            this.rbKn.AutoSize = true;
            this.rbKn.Location = new System.Drawing.Point(43, 150);
            this.rbKn.Name = "rbKn";
            this.rbKn.Size = new System.Drawing.Size(86, 17);
            this.rbKn.TabIndex = 5;
            this.rbKn.TabStop = true;
            this.rbKn.Text = "KN - Pahang";
            this.rbKn.UseVisualStyleBackColor = true;
            this.rbKn.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbKk
            // 
            this.rbKk.AutoSize = true;
            this.rbKk.Location = new System.Drawing.Point(43, 126);
            this.rbKk.Name = "rbKk";
            this.rbKk.Size = new System.Drawing.Size(127, 17);
            this.rbKk.TabIndex = 4;
            this.rbKk.TabStop = true;
            this.rbKk.Text = "KK - Sabah && Labuan";
            this.rbKk.UseVisualStyleBackColor = true;
            this.rbKk.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbKg
            // 
            this.rbKg.AutoSize = true;
            this.rbKg.Location = new System.Drawing.Point(43, 102);
            this.rbKg.Name = "rbKg";
            this.rbKg.Size = new System.Drawing.Size(91, 17);
            this.rbKg.TabIndex = 3;
            this.rbKg.TabStop = true;
            this.rbKg.Text = "KG - Sarawak";
            this.rbKg.UseVisualStyleBackColor = true;
            this.rbKg.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbJp
            // 
            this.rbJp.AutoSize = true;
            this.rbJp.Location = new System.Drawing.Point(43, 78);
            this.rbJp.Name = "rbJp";
            this.rbJp.Size = new System.Drawing.Size(74, 17);
            this.rbJp.TabIndex = 2;
            this.rbJp.TabStop = true;
            this.rbJp.Text = "JP - Perak";
            this.rbJp.UseVisualStyleBackColor = true;
            this.rbJp.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbJh
            // 
            this.rbJh.AutoSize = true;
            this.rbJh.Location = new System.Drawing.Point(43, 54);
            this.rbJh.Name = "rbJh";
            this.rbJh.Size = new System.Drawing.Size(73, 17);
            this.rbJh.TabIndex = 1;
            this.rbJh.TabStop = true;
            this.rbJh.Text = "JH - Johor";
            this.rbJh.UseVisualStyleBackColor = true;
            this.rbJh.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // rbAs
            // 
            this.rbAs.AutoSize = true;
            this.rbAs.Location = new System.Drawing.Point(43, 30);
            this.rbAs.Name = "rbAs";
            this.rbAs.Size = new System.Drawing.Size(116, 17);
            this.rbAs.TabIndex = 0;
            this.rbAs.TabStop = true;
            this.rbAs.Text = "AS - Kedah && Perlis";
            this.rbAs.UseVisualStyleBackColor = true;
            this.rbAs.CheckedChanged += new System.EventHandler(this.rbSegment_CheckedChanged);
            // 
            // SingleSegmentPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupSegment);
            this.Name = "SingleSegmentPanel";
            this.groupSegment.ResumeLayout(false);
            this.groupSegment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupSegment;
        private System.Windows.Forms.RadioButton rbAs;
        private System.Windows.Forms.RadioButton rbTg;
        private System.Windows.Forms.RadioButton rbPg;
        private System.Windows.Forms.RadioButton rbMk;
        private System.Windows.Forms.RadioButton rbKv;
        private System.Windows.Forms.RadioButton rbKn;
        private System.Windows.Forms.RadioButton rbKk;
        private System.Windows.Forms.RadioButton rbKg;
        private System.Windows.Forms.RadioButton rbJp;
        private System.Windows.Forms.RadioButton rbJh;
    }
}

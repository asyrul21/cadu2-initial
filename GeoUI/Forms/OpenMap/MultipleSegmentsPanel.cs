﻿using Geomatic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.OpenMap
{
    public partial class MultipleSegmentsPanel : OptionPanel
    {
        public override IEnumerable<SegmentName> Segments
        {
            get
            {
                return _segments;
            }
        }

        private List<SegmentName> _segments;

        public MultipleSegmentsPanel()
        {
            InitializeComponent();
            _segments = new List<SegmentName>();
        }

        private void cbSegment_CheckedChanged(object sender, EventArgs e)
        {
            _segments.Clear();
            if (cbAs.Checked)
            {
                _segments.Add(SegmentName.AS);
            }
            if (cbJh.Checked)
            {
                _segments.Add(SegmentName.JH);
            }
            if (cbJp.Checked)
            {
                _segments.Add(SegmentName.JP);
            }
            if (cbKg.Checked)
            {
                _segments.Add(SegmentName.KG);
            }
            if (cbKk.Checked)
            {
                _segments.Add(SegmentName.KK);
            }
            if (cbKn.Checked)
            {
                _segments.Add(SegmentName.KN);
            }
            if (cbKv.Checked)
            {
                _segments.Add(SegmentName.KV);
            }
            if (cbMk.Checked)
            {
                _segments.Add(SegmentName.MK);
            }
            if (cbPg.Checked)
            {
                _segments.Add(SegmentName.PG);
            }
            if (cbTg.Checked)
            {
                _segments.Add(SegmentName.TG);
            }
        }
    }
}

﻿namespace Geomatic.UI.Forms.OpenMap
{
    partial class MultipleSegmentsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupSegment = new System.Windows.Forms.GroupBox();
            this.cbTg = new System.Windows.Forms.CheckBox();
            this.cbPg = new System.Windows.Forms.CheckBox();
            this.cbMk = new System.Windows.Forms.CheckBox();
            this.cbKv = new System.Windows.Forms.CheckBox();
            this.cbKn = new System.Windows.Forms.CheckBox();
            this.cbKk = new System.Windows.Forms.CheckBox();
            this.cbKg = new System.Windows.Forms.CheckBox();
            this.cbJp = new System.Windows.Forms.CheckBox();
            this.cbJh = new System.Windows.Forms.CheckBox();
            this.cbAs = new System.Windows.Forms.CheckBox();
            this.groupSegment.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupSegment
            // 
            this.groupSegment.Controls.Add(this.cbTg);
            this.groupSegment.Controls.Add(this.cbPg);
            this.groupSegment.Controls.Add(this.cbMk);
            this.groupSegment.Controls.Add(this.cbKv);
            this.groupSegment.Controls.Add(this.cbKn);
            this.groupSegment.Controls.Add(this.cbKk);
            this.groupSegment.Controls.Add(this.cbKg);
            this.groupSegment.Controls.Add(this.cbJp);
            this.groupSegment.Controls.Add(this.cbJh);
            this.groupSegment.Controls.Add(this.cbAs);
            this.groupSegment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupSegment.Location = new System.Drawing.Point(0, 0);
            this.groupSegment.Name = "groupSegment";
            this.groupSegment.Size = new System.Drawing.Size(300, 300);
            this.groupSegment.TabIndex = 0;
            this.groupSegment.TabStop = false;
            this.groupSegment.Text = "Select at least a Segment";
            // 
            // cbTg
            // 
            this.cbTg.AutoSize = true;
            this.cbTg.Location = new System.Drawing.Point(43, 247);
            this.cbTg.Name = "cbTg";
            this.cbTg.Size = new System.Drawing.Size(162, 17);
            this.cbTg.TabIndex = 19;
            this.cbTg.Text = "TG - Terengganu && Kelantan";
            this.cbTg.UseVisualStyleBackColor = true;
            this.cbTg.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbPg
            // 
            this.cbPg.AutoSize = true;
            this.cbPg.Location = new System.Drawing.Point(43, 223);
            this.cbPg.Name = "cbPg";
            this.cbPg.Size = new System.Drawing.Size(87, 17);
            this.cbPg.TabIndex = 18;
            this.cbPg.Text = "PG - Penang";
            this.cbPg.UseVisualStyleBackColor = true;
            this.cbPg.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbMk
            // 
            this.cbMk.AutoSize = true;
            this.cbMk.Location = new System.Drawing.Point(43, 199);
            this.cbMk.Name = "cbMk";
            this.cbMk.Size = new System.Drawing.Size(175, 17);
            this.cbMk.TabIndex = 17;
            this.cbMk.Text = "MK - Melaka && Negeri Sembilan";
            this.cbMk.UseVisualStyleBackColor = true;
            this.cbMk.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbKv
            // 
            this.cbKv.AutoSize = true;
            this.cbKv.Location = new System.Drawing.Point(43, 175);
            this.cbKv.Name = "cbKv";
            this.cbKv.Size = new System.Drawing.Size(168, 17);
            this.cbKv.TabIndex = 16;
            this.cbKv.Text = "KV - Kuala Lumpur && Selangor";
            this.cbKv.UseVisualStyleBackColor = true;
            this.cbKv.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbKn
            // 
            this.cbKn.AutoSize = true;
            this.cbKn.Location = new System.Drawing.Point(43, 151);
            this.cbKn.Name = "cbKn";
            this.cbKn.Size = new System.Drawing.Size(87, 17);
            this.cbKn.TabIndex = 15;
            this.cbKn.Text = "KN - Pahang";
            this.cbKn.UseVisualStyleBackColor = true;
            this.cbKn.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbKk
            // 
            this.cbKk.AutoSize = true;
            this.cbKk.Location = new System.Drawing.Point(43, 127);
            this.cbKk.Name = "cbKk";
            this.cbKk.Size = new System.Drawing.Size(128, 17);
            this.cbKk.TabIndex = 14;
            this.cbKk.Text = "KK - Sabah && Labuan";
            this.cbKk.UseVisualStyleBackColor = true;
            this.cbKk.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbKg
            // 
            this.cbKg.AutoSize = true;
            this.cbKg.Location = new System.Drawing.Point(43, 103);
            this.cbKg.Name = "cbKg";
            this.cbKg.Size = new System.Drawing.Size(92, 17);
            this.cbKg.TabIndex = 13;
            this.cbKg.Text = "KG - Sarawak";
            this.cbKg.UseVisualStyleBackColor = true;
            this.cbKg.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbJp
            // 
            this.cbJp.AutoSize = true;
            this.cbJp.Location = new System.Drawing.Point(43, 79);
            this.cbJp.Name = "cbJp";
            this.cbJp.Size = new System.Drawing.Size(75, 17);
            this.cbJp.TabIndex = 12;
            this.cbJp.Text = "JP - Perak";
            this.cbJp.UseVisualStyleBackColor = true;
            this.cbJp.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbJh
            // 
            this.cbJh.AutoSize = true;
            this.cbJh.Location = new System.Drawing.Point(43, 55);
            this.cbJh.Name = "cbJh";
            this.cbJh.Size = new System.Drawing.Size(74, 17);
            this.cbJh.TabIndex = 11;
            this.cbJh.Text = "JH - Johor";
            this.cbJh.UseVisualStyleBackColor = true;
            this.cbJh.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // cbAs
            // 
            this.cbAs.AutoSize = true;
            this.cbAs.Location = new System.Drawing.Point(43, 31);
            this.cbAs.Name = "cbAs";
            this.cbAs.Size = new System.Drawing.Size(117, 17);
            this.cbAs.TabIndex = 10;
            this.cbAs.Text = "AS - Kedah && Perlis";
            this.cbAs.UseVisualStyleBackColor = true;
            this.cbAs.CheckedChanged += new System.EventHandler(this.cbSegment_CheckedChanged);
            // 
            // MultipleSegmentsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupSegment);
            this.Name = "MultipleSegmentsPanel";
            this.groupSegment.ResumeLayout(false);
            this.groupSegment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupSegment;
        private System.Windows.Forms.CheckBox cbTg;
        private System.Windows.Forms.CheckBox cbPg;
        private System.Windows.Forms.CheckBox cbMk;
        private System.Windows.Forms.CheckBox cbKv;
        private System.Windows.Forms.CheckBox cbKn;
        private System.Windows.Forms.CheckBox cbKk;
        private System.Windows.Forms.CheckBox cbKg;
        private System.Windows.Forms.CheckBox cbJp;
        private System.Windows.Forms.CheckBox cbJh;
        private System.Windows.Forms.CheckBox cbAs;

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.Add;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms
{
    public partial class AccessibilityForm : BaseForm
    {
        public AccessibilityForm()
        {
            InitializeComponent();
        }

        private void AccessibilityForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {
            RefreshTitle("Accessibility");

            controlGroup.Columns.Add("Name");

            controlUser.Columns.Add("Name");
            controlUser.Columns.Add("Group");

            PopulateGroup();
            PopulateUser();
        }

        private void PopulateGroup()
        {
            controlGroup.BeginUpdate();
            controlGroup.Sorting = SortOrder.None;
            controlGroup.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();
            foreach (GUserGroup userGroup in GUserGroup.GetAll())
            {
                ListViewItem item = new ListViewItem();
                item.Text = userGroup.Name;
                item.Tag = userGroup;

                items.Add(item);
            }
            controlGroup.Items.AddRange(items.ToArray());
            controlGroup.FixColumnWidth();
            controlGroup.EndUpdate();
        }

        private void PopulateUser()
        {
            controlUser.BeginUpdate();
            controlUser.Sorting = SortOrder.None;
            controlUser.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();
            foreach (GUser user in GUser.GetAll())
            {
                ListViewItem item = new ListViewItem();

                // modified by noraini - 17/05/2019 Add group Name in User list
                item.Text = user.Name;
                GUserGroup userGroup = user.GetGroup();
                item.SubItems.Add((userGroup == null) ? string.Empty : userGroup.Name);
                // end modify
                item.Tag = user; 
                items.Add(item);
            }
            controlUser.Items.AddRange(items.ToArray());
            controlUser.FixColumnWidth();
            controlUser.EndUpdate();
        }

        private void controlGroup_AddClick(object sender, EventArgs e)
        {
            using (AddUserGroupForm form = new AddUserGroupForm())
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    GUserGroup userGroup = repo.NewObj<GUserGroup>();
                    userGroup.Name = form.InsertedName;
                    userGroup = repo.Insert(userGroup);

                    foreach (GFunction function in form.FunctionsForAssociate)
                    {
                        GUserGroupFunction userGroupFunction = repo.NewObj<GUserGroupFunction>();
                        userGroupFunction.GroupId = userGroup.OID;
                        userGroupFunction.FunctionId = function.OID;
                        userGroupFunction = repo.Insert(userGroupFunction);
                    }

                    foreach (GUser user in form.UsersForAssociate)
                    {
                        user.GroupId = userGroup.OID;
                        repo.Update(user);
                    }

                    ListViewItem item = new ListViewItem();
                    item.Text = userGroup.Name;
                    item.Tag = userGroup;
                    controlGroup.Items.Add(item);
                }
            }
        }

        private void controlGroup_EditClick(object sender, EventArgs e)
        {
            if (controlGroup.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = controlGroup.SelectedItems[0];
            GUserGroup userGroup = (GUserGroup)item.Tag;

            using (EditUserGroupForm form = new EditUserGroupForm(userGroup))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    userGroup.Name = form.InsertedName;
                    repo.Update(userGroup);
                    item.Text = userGroup.Name;
                    item.Tag = userGroup;

                    foreach (GFunction function in form.FunctionsForAssociate)
                    {
                        Query<GUserGroupFunction> query = new Query<GUserGroupFunction>();
                        query.Obj.GroupId = userGroup.OID;
                        query.Obj.FunctionId = function.OID;

                        if (repo.Count(query) == 0)
                        {
                            GUserGroupFunction userGroupFunction = repo.NewObj<GUserGroupFunction>();
                            userGroupFunction.GroupId = userGroup.OID;
                            userGroupFunction.FunctionId = function.OID;
                            userGroupFunction = repo.Insert(userGroupFunction);
                        }
                    }

                    foreach (GFunction function in form.FunctionsForDissociate)
                    {
                        Query<GUserGroupFunction> query = new Query<GUserGroupFunction>();
                        query.Obj.GroupId = userGroup.OID;
                        query.Obj.FunctionId = function.OID;

                        foreach (GUserGroupFunction userGroupFunction in repo.Search(query))
                        {
                            repo.Delete(userGroupFunction);
                        }
                    }

                    foreach (GUser user in form.UsersForAssociate)
                    {
                        user.GroupId = userGroup.OID;
                        repo.Update(user);
                    }

                    foreach (GUser user in form.UsersForDissociate)
                    {
                        if (user.GroupId == userGroup.OID)
                        {
                            user.GroupId = null;
                            repo.Update(user);
                        }
                    }
                }
            }
        }

        private void controlGroup_DeleteClick(object sender, EventArgs e)
        {
            if (controlGroup.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = controlGroup.SelectedItems[0];
            GUserGroup userGroup = (GUserGroup)item.Tag;

            using (QuestionMessageBox box = new QuestionMessageBox())
            {
                box.SetText("Are you sure you want to delete {0}?", userGroup.Name);
                if (box.Show(this) != DialogResult.Yes)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();

                    // added by noraini 21/05/2019 - Not allow to delete group if user exit
                    if (userGroup.HasUsers())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to delete Group. User exist in the Group");
                            box1.Show();
                        }
                        return;
                      }

                    foreach (GUserGroupFunction userGroupFunction in userGroup.GetGroupFunctions())
                    {
                        repo.Delete(userGroupFunction);
                    }

                    //foreach (GUser user in userGroup.GetUsers())
                    //{
                    //    user.GroupId = null;
                    //    repo.Update(user);
                    //}

                    repo.Delete(userGroup);
                    item.Remove();
                }
            }
        }

        private void controlUser_AddClick(object sender, EventArgs e)
        {
            using (AddUserForm form = new AddUserForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    GUser user = repo.NewObj<GUser>();
                    user.Name = form.InsertedUsername;
                    user.Password = form.InsertedPassword;
                    user = repo.Insert(user);

                    ListViewItem item = new ListViewItem();
                    item.Text = user.Name;
                    item.Tag = user;
                    controlUser.Items.Add(item);
                }
            }
        }

        private void controlUser_EditClick(object sender, EventArgs e)
        {
            if (controlUser.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = controlUser.SelectedItems[0];
            GUser user = (GUser)item.Tag;

            using (EditUserForm form = new EditUserForm(user))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    user.Name = form.InsertedUsername;
                    user.Password = form.InsertedPassword;
                    repo.Update(user);
                    item.Text = user.Name;
                    item.Tag = user;
                }
            }
        }

        private void controlUser_DeleteClick(object sender, EventArgs e)
        {
            if (controlUser.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = controlUser.SelectedItems[0];
            GUser user = (GUser)item.Tag;

            using (QuestionMessageBox box = new QuestionMessageBox())
            {
                box.SetText("Are you sure you want to delete {0}?", user.Name);
                if (box.Show(this) != DialogResult.Yes)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    repo.Delete(user);
                    item.Remove();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected ListViewItem CreateItem(GUserGroup userGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = userGroup.Name;
            item.Tag = userGroup;
            return item;
        }
    }
}

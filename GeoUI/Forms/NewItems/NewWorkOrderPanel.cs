﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.NewItems
{
    public partial class NewWorkOrderPanel : NewItemPanel
    {
        private SegmentName _segmentName;

        public NewWorkOrderPanel(SegmentName segmentName)
        {
            InitializeComponent();
            _segmentName = segmentName;
        }

        private void NewWorkOrderPanel_Load(object sender, EventArgs e)
        {
            foreach (IVersion version in Session.Current.Cadu.Versions())
            {
                lbOrders.Items.Add(version.VersionName);
            }
        }

        public override bool Create()
        {
            bool ok = true;

            ok &= StringUtils.IsInt(txtId.Text);

            if (ok)
            {
                Session.Current.CreateVersion(txtId.Text.ToString());
                //Session.CurrentVersion = Session.Current[_segmentName, txtId.Text.ToString()];
            }
            return ok;
        }

        public override bool Open()
        {
            if (SelectedItem == null)
            {
                return false;
            }

            //Session.CurrentVersion = Session.Current[_segmentName, SelectedItem];
            return true;
        }

        private void OpenWorkOrder(object sender, EventArgs e)
        {
            if (SelectedItem == null)
            {
                return;
            }

            //Session.CurrentVersion = Session.Current[_segmentName, SelectedItem];
        }

        private String SelectedItem
        {
            get { return (lbOrders.SelectedItem == null) ? null : lbOrders.SelectedItem.ToString(); }
        }


        private void lbOrders_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnButtonEnable();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.NewItems
{
    interface INewItem
    {
        INewItemPanel Panel { get; }
        bool Create();
        bool Open();
    }
}

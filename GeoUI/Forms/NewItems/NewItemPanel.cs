﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.NewItems
{
    public partial class NewItemPanel : UserControl, INewItemPanel
    {
        public event EventHandler ButtonEnabled;

        public NewItemPanel()
        {
            InitializeComponent();
        }

        public virtual bool Create()
        {
            throw new Exception("Not implemented.");
        }

        public virtual bool Open()
        {
            throw new Exception("Not implemented.");
        }

        protected void OnButtonEnable()
        {
            if (ButtonEnabled != null)
                ButtonEnabled(this, new EventArgs());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;

namespace Geomatic.UI.Forms.NewItems
{
    public partial class NewItemForm : Form
    {
        private SegmentName _segmentName;

        private INewItem SelectedItem
        {
            get { return (lbItems.SelectedItem == null) ? null : (INewItem)lbItems.SelectedItem; }
        }

        public NewItemForm(SegmentName segmentName)
        {
            InitializeComponent();
            _segmentName = segmentName;
        }

        private void NewItemForm_Load(object sender, EventArgs e)
        {
            RefreshTitle("New");
            AddItem(new NewWorkOrderItem(_segmentName));
        }

        private void NewItemForm_Shown(object sender, EventArgs e)
        {
            lbItems_SelectedIndexChanged(null, null);
            btnOpen.Enabled = false;
        }

        private void lbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCreate.Enabled = (SelectedItem != null);

            RefreshTitle((SelectedItem == null) ? "New" : SelectedItem.ToString());

            foreach (INewItem item in lbItems.Items)
            {
                item.Panel.Visible = (SelectedItem == null) ? false : SelectedItem.Equals(item);
            }

            if (SelectedItem != null)
                SelectedItem.Panel.ButtonEnabled += OnButtonEnable;
        }

        private void AddItem(INewItem item)
        {
            item.Panel.Parent = panelItem;
            item.Panel.Dock = DockStyle.Fill;
            lbItems.Items.Add(item);
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (SelectedItem == null)
            {
                return;
            }

            if (SelectedItem.Create())
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected void RefreshTitle(string title)
        {
            Text = title;
        }

        protected void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }

        
        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (SelectedItem.Open())
            {
                DialogResult = DialogResult.OK;
            }
            
        }

        private void OnButtonEnable(object sender, EventArgs e)
        {
            btnOpen.Enabled = true;
        }
    }
}

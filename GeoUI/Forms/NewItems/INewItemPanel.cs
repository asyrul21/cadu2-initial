﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.NewItems
{
    interface INewItemPanel
    {
        bool Visible { get; set; }
        Control Parent { get; set; }
        DockStyle Dock { get; set; }
        bool Create();
        bool Open();
        event EventHandler ButtonEnabled;
    }
}

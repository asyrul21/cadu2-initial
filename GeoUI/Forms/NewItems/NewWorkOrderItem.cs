﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Sessions;
using Geomatic.Core;
using Geomatic.Core.Sessions.Workspaces;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.UI.Forms.NewItems
{
    class NewWorkOrderItem : NewItem
    {
        private SegmentName _segmentName;
        protected INewItemPanel _panel;

        public NewWorkOrderItem(SegmentName segmentName)
        {
            _segmentName = segmentName;
        }

        public override INewItemPanel Panel
        {
            get
            {
                if (_panel == null)
                {
                    _panel = new NewWorkOrderPanel(_segmentName);
                }
                return _panel;
            }
        }

        public override string ToString()
        {
            return "Work Order";
        }
    }
}

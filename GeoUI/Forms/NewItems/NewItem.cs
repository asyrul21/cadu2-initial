﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.NewItems
{
    abstract class NewItem : INewItem
    {
        public abstract INewItemPanel Panel { get; }
        public virtual bool Create()
        {
            return Panel.Create();
        }
        public virtual bool Open()
        {
            return Panel.Open();
        }
    }
}

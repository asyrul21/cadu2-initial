﻿namespace Geomatic.UI.Forms.NewItems
{
    partial class NewWorkOrderPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.centreContainer = new System.Windows.Forms.SplitContainer();
            this.boxOrders = new System.Windows.Forms.GroupBox();
            this.lbOrders = new System.Windows.Forms.ListBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.RichTextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.centreContainer)).BeginInit();
            this.centreContainer.Panel1.SuspendLayout();
            this.centreContainer.Panel2.SuspendLayout();
            this.centreContainer.SuspendLayout();
            this.boxOrders.SuspendLayout();
            this.SuspendLayout();
            // 
            // centreContainer
            // 
            this.centreContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.centreContainer.Location = new System.Drawing.Point(0, 0);
            this.centreContainer.Name = "centreContainer";
            // 
            // centreContainer.Panel1
            // 
            this.centreContainer.Panel1.Controls.Add(this.boxOrders);
            // 
            // centreContainer.Panel2
            // 
            this.centreContainer.Panel2.Controls.Add(this.lblDescription);
            this.centreContainer.Panel2.Controls.Add(this.txtDescription);
            this.centreContainer.Panel2.Controls.Add(this.txtId);
            this.centreContainer.Panel2.Controls.Add(this.lblId);
            this.centreContainer.Size = new System.Drawing.Size(400, 300);
            this.centreContainer.SplitterDistance = 133;
            this.centreContainer.SplitterWidth = 10;
            this.centreContainer.TabIndex = 1;
            // 
            // boxOrders
            // 
            this.boxOrders.Controls.Add(this.lbOrders);
            this.boxOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.boxOrders.Location = new System.Drawing.Point(0, 0);
            this.boxOrders.Name = "boxOrders";
            this.boxOrders.Size = new System.Drawing.Size(133, 300);
            this.boxOrders.TabIndex = 2;
            this.boxOrders.TabStop = false;
            this.boxOrders.Text = "WorkOrders";
            // 
            // lbOrders
            // 
            this.lbOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOrders.FormattingEnabled = true;
            this.lbOrders.Location = new System.Drawing.Point(3, 16);
            this.lbOrders.Name = "lbOrders";
            this.lbOrders.Size = new System.Drawing.Size(127, 281);
            this.lbOrders.TabIndex = 0;
            this.lbOrders.SelectedIndexChanged += new System.EventHandler(this.lbOrders_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(14, 45);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Location = new System.Drawing.Point(83, 42);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(124, 76);
            this.txtDescription.TabIndex = 4;
            this.txtDescription.Text = "";
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.Location = new System.Drawing.Point(83, 16);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(124, 20);
            this.txtId.TabIndex = 3;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(58, 19);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 2;
            this.lblId.Text = "Id:";
            // 
            // NewWorkOrderPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.centreContainer);
            this.Name = "NewWorkOrderPanel";
            this.Load += new System.EventHandler(this.NewWorkOrderPanel_Load);
            this.centreContainer.Panel1.ResumeLayout(false);
            this.centreContainer.Panel2.ResumeLayout(false);
            this.centreContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.centreContainer)).EndInit();
            this.centreContainer.ResumeLayout(false);
            this.boxOrders.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer centreContainer;
        private System.Windows.Forms.ListBox lbOrders;
        private System.Windows.Forms.GroupBox boxOrders;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.RichTextBox txtDescription;
    }
}

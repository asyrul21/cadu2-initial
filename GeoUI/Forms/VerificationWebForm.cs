﻿using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using System;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Rows;
using GPoi = Geomatic.Core.Features.GPoi;

namespace Geomatic.UI.Forms
{
    public partial class VerificationWebForm : BaseForm
    {
        protected SegmentName _SegmentName { set; get; }
        public string remark = "";

        public string StatusVerify
        {
            get { return txtStatusVerify.Text; }
        }

        public VerificationWebForm(string featureId, string remarkVerify, SegmentName SegmentName)
        {
            InitializeComponent();
            txtFeatureId.Text = featureId;
            _SegmentName = SegmentName;
            txtStatusVerify.Text = remarkVerify;
            Form_Load();
        }

        protected void Form_Load()
        {
            RefreshTitle("CADU3 WEB Verification Form");
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (StatusVerify.Contains("Success Verify"))
            {
                btnVerifyFeature.Enabled = false;
                btnRevertFeature.Enabled = false;
            }
            ListAttribute();         
        }

        #region Display attribute Web and Adm POI
        private void ListAttribute()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(_SegmentName);
                GPoiWeb poiWeb = repo.GetById<GPoiWeb>(id.Value);
                fillWebPoi(poiWeb);

                if (!string.IsNullOrEmpty(poiWeb.OriId.ToString()) && poiWeb.OriId.Value > 0)
                {
                    GPoi poi = repo.GetById<GPoi>(poiWeb.OriId.Value);
                    fillAdmPoi(poi);
                }
            }
        }

        private void fillWebPoi(GPoiWeb poi)
        {
            txtWebObjId.Text = poi.OID.ToString();
            txtWebPlcName.Text = poi.Name;
            txtWebPlcName2.Text = poi.Name2;
            txtWebPOICode.Text = (poi.Code == null) ? string.Empty : poi.Code.ToString();
            txtWebStatus.Text = poi.StatusType;
            txtWebRefId.Text = poi.ParentId.ToString();
            txtWebRefType.Text = poi.ParentTypeName;
            txtWebModUser.Text = poi.UpdatedBy;
            txtWebModDate.Text = poi.DateUpdated.ToString();
        }

        private void fillAdmPoi(GPoi poi)
        {
            txtAdmObjId.Text = poi.OID.ToString();
            txtAdmPlcName.Text = poi.Name;
            txtAdmPlcName2.Text = poi.Name2;
            txtAdmPOICode.Text = poi.Code.ToString();
            txtAdmStatus.Text = poi.UpdateStatusType.ToString();
            txtAdmRefId.Text = poi.ParentId.ToString();
            txtAdmRefType.Text = poi.ParentTypeValue;
            txtAdmModUser.Text = poi.UpdatedBy;
            txtAdmModDate.Text = poi.DateUpdated.ToString();
        }
        #endregion

        #region Verify process
        private void BtnVerify_Click(object sender, EventArgs e)
        {
            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                          .SetDefaultButton(MessageBoxDefaultButton.Button1)
                          .SetCaption("Verify CADU3 Web")
                          .SetText("Are you sure you want to Proceed Verify id {0}?", txtFeatureId.Text);
                if (confirmBox.Show(this) != DialogResult.Yes)
                {
                    return;
                }
            }

            bool success = false;
            try
            {
                using (new WaitCursor())
                {
                    VerifyCadu3WebForm verifyMainForm = new VerifyCadu3WebForm(_SegmentName);
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    remark = verifyMainForm.Verify(id.Value);
                    bool remarkstatus = remark.Contains("Success");
                    if (remarkstatus)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        #endregion

        #region revert process 
        private void BtnRevert_Click(object sender, EventArgs e)
        {
            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                 .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                 .SetCaption("Revert Feature AND")
                                 .SetText("Are you sure you want to Proceed Revert id {0}?", txtFeatureId.Text);
                if (confirmBox.Show(this) != DialogResult.Yes)
                {
                    return;
                }
            }

            bool success = false;
            try
            {
                using (new WaitCursor())
                {
                    Revert();
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void Revert()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(_SegmentName);
            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GPoiWeb poiWeb = repo.GetById<GPoiWeb>(id.Value);

                    int OriId = Int32.Parse(poiWeb.OriId.ToString());
                    GPoi poi = repo.GetById<GPoi>(OriId);

                    repo.StartTransaction();

                    switch (poiWeb.StatusType)
                    {
                        case "ADD":
                            remark = "Success Revert";
                            success = true;
                            break;

                        case "EDIT":
                            remark = "Success Revert";
                            success = true;
                            break;

                        case "DELETE":
                            remark = "Success Revert";
                            success = true;
                            break;

                        default:
                            throw new Exception(string.Format("Unknown status. {0}", poiWeb.StatusType));
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        #endregion

        private void BtnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

    }
}

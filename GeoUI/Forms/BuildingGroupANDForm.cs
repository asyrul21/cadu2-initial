﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Commands;
using Geomatic.Core.Exceptions;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI.Forms.Views;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;
using Geomatic.UI.FeedBacks;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms
{
    public partial class BuildingGroupANDForm : CollapsibleForm
    {
        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string InsertedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public string SelectedForecastSource
        {
            get { return (cbForecastSource.SelectedItem == null) ? string.Empty : ((GSource)cbForecastSource.SelectedItem).Code.ToString(); }
        }

        public int InsertedTotalForecastUnit
        {
            get { return totalForecastUnit.Value; }
        }

        public string TotalUnit
        {
            get { return txtTotalUnit.Text ?? "0"; }
        }

        protected GBuildingGroupAND _buildingGroupAND;

        public BuildingGroupANDForm()
            : this(null)
        {
        }

        public BuildingGroupANDForm(GBuildingGroupAND buildingGroupAND)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
              btnSetCode,
              lblCode, txtCode,
              lblName, txtName,
              lblSource, cbSource,
              lblForecastSource, cbForecastSource,
              lblForecastUnit, totalForecastUnit,
              lblTotalNumUnit, txtTotalUnit, 
              btnApply
              );

            _buildingGroupAND = buildingGroupAND;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateForecastSource(cbForecastSource);

            controlBuilding.Columns.Add("Id");
            controlBuilding.Columns.Add("Construction Status");
            controlBuilding.Columns.Add("Code");
            controlBuilding.Columns.Add("Name");
            controlBuilding.Columns.Add("Name2");
            controlBuilding.Columns.Add("Floor Num Unit");
            controlBuilding.Columns.Add("Lot");
            controlBuilding.Columns.Add("House");
            controlBuilding.Columns.Add("Street Type");
            controlBuilding.Columns.Add("Street Name");
            controlBuilding.Columns.Add("Street Name2");
            controlBuilding.Columns.Add("Section");
            controlBuilding.Columns.Add("Postcode");
            controlBuilding.Columns.Add("City");
            controlBuilding.Columns.Add("State");
            controlBuilding.Columns.Add("Created By");
            controlBuilding.Columns.Add("Date Created");
            controlBuilding.Columns.Add("Updated By");
            controlBuilding.Columns.Add("Date Updated");

            txtId.Text = _buildingGroupAND.OriId.ToString();

            cbNaviStatus.Text = _buildingGroupAND.NavigationStatusValue;
            txtName.Text = _buildingGroupAND.Name;
            txtCode.Text = _buildingGroupAND.Code;
            cbSource.Text = _buildingGroupAND.SourceValue;
            GCode1 code1 = _buildingGroupAND.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _buildingGroupAND.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _buildingGroupAND.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            txtDateCreated.Text = _buildingGroupAND.DateCreated;
            txtDateUpdated.Text = _buildingGroupAND.DateUpdated;
            txtCreator.Text = _buildingGroupAND.CreatedBy;
            txtUpdater.Text = _buildingGroupAND.UpdatedBy;

            cbForecastSource.Text = _buildingGroupAND.ForecastSourceValue;
            //txtTotalUnit.Text = _buildingGroupAND.NumUnit.ToString();
            int UnitCount = _buildingGroupAND.NumUnit.HasValue ? _buildingGroupAND.NumUnit.Value : 0;
            txtTotalUnit.Text = UnitCount.ToString();

            int forecastUnitCount = _buildingGroupAND.ForecastNumUnit.HasValue ? _buildingGroupAND.ForecastNumUnit.Value : 0;
            totalForecastUnit.Value = forecastUnitCount;

            if (_buildingGroupAND.BuldingNamePos == 1)
            {
                cbMirror.Checked = true;
            }
            else
            {
                cbMirror.Checked = false;
            }

            GStreet street = _buildingGroupAND.GetStreet();

            if (street != null)
            {
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null)
                {
                    txtStreetId.Text = streetAND.OriId.ToString();
                    txtStreetType.Text = streetAND.TypeValue;
                    txtStreetName.Text = streetAND.Name;
                    txtStreetName2.Text = streetAND.Name2;
                    txtSection.Text = streetAND.Section;
                    txtPostcode.Text = streetAND.Postcode;
                    txtCity.Text = streetAND.City;
                    txtState.Text = streetAND.State;

                }
                else
                {
                    txtStreetId.Text = street.OID.ToString();
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }

            PopulateBuildings();

            // end
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Code
            bool isCodeValid = BuildingGroupValidator.CheckCode(txtCode.Text);
            LabelColor(lblCode, isCodeValid);
            pass = isCodeValid;

            // Name
            bool isNameValid = BuildingGroupValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Source
            bool isSourceValid = BuildingGroupValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            // forcast Source
            bool isForecastSourceValid = BuildingGroupValidator.CheckSource(SelectedForecastSource);
            LabelColor(lblForecastSource, isForecastSourceValid);
            pass &= isForecastSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _buildingGroupAND.NavigationStatus = SelectedNavigationStatus;
            _buildingGroupAND.Code = InsertedCode;
            _buildingGroupAND.Name = InsertedName;
            _buildingGroupAND.Source = SelectedSource;
            _buildingGroupAND.ForecastNumUnit = InsertedTotalForecastUnit;
            _buildingGroupAND.NumUnit = Int32.Parse(TotalUnit);
            _buildingGroupAND.ForecastSource = SelectedForecastSource;
        }

        protected void PopulateBuildings()
        {
            int totolFloorUnit = 0;

            controlBuilding.BeginUpdate();
            controlBuilding.Sorting = SortOrder.None;
            controlBuilding.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GBuilding building in _buildingGroupAND.GetBuildings())
            {
                // noraini ali - sept 2020 - 
                // Only List Building Associate to Building Group Id where building not exist AND 
                if (building.AndStatus != 2)
                {
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND == null)
                    {
                        ListViewItem item = CreateItem(building);
                        items.Add(item);
                        if (!string.IsNullOrEmpty(building.Unit.ToString()))
                        {
                            totolFloorUnit += building.Unit.Value;
                        }
                    }
                }
            }

            // noraini ali - sept 2020 - 
            // List all Building AND associate to Building Group Id
            foreach (GBuildingAND buildingAND in _buildingGroupAND.GetBuildingsAND())
            {
                if (buildingAND.AndStatus != 2)
                {
                    ListViewItem item = CreateItem(buildingAND);
                    items.Add(item);
                    if (!string.IsNullOrEmpty(buildingAND.Unit.ToString()))
                    {
                        totolFloorUnit += buildingAND.Unit.Value;
                    }
                }
            }
            // end

            controlBuilding.Items.AddRange(items.ToArray());
            controlBuilding.FixColumnWidth();
            controlBuilding.EndUpdate();

            txtTotalUnit.Text = totolFloorUnit.ToString();

        }

        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                GCode1 code1 = code3.GetCode1();
                if (code1 != null)
                {
                    categoryControl.Code1Text = code1.Description;
                }
                GCode2 code2 = code3.GetCode2();
                if (code2 != null)
                {
                    categoryControl.Code2Text = code2.Description;
                }
                if (code3 != null)
                {
                    categoryControl.Code3Text = code3.Description;
                }
            }
        }

        protected ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add((building.Unit == null) ? "0" : building.Unit.ToString());

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        protected ListViewItem CreateItem(GBuildingAND building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OriId.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add((building.Unit == null) ? "0" : building.Unit.ToString());

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private void controlBuilding_AssociateClick1(object sender, EventArgs e)
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Please use function: Building> Associate Building Group");
                box.Show();
            }

            return;
        }

        private void controlBuilding_DissociateClick1(object sender, EventArgs e)
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Please use function: Building> Dissociate Building Group");
                box.Show();
            }

            return;
        }

        private void controlBuilding_AssociateClick(object sender, EventArgs e)
        {
            // original

            using (ChooseBuildingByDistanceForm chooseForm = new ChooseBuildingByDistanceForm(_buildingGroupAND))
            {
                if (chooseForm.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory(_buildingGroupAND.SegmentName);
                    foreach (IGFeature feature in chooseForm.SelectedFeatures)
                    {
                        // noraini ali - Sept 2020
                        // Get feature name between ADM & AND
                        string strtableName = feature.TableName.ToString();
                        bool b1 = strtableName.Contains("AND");
                        if (b1)
                        {
                            // get Building AND - Update GroupId
                            GBuildingAND buildingAND = (GBuildingAND)feature;
                            buildingAND.GroupId = _buildingGroupAND.OriId;

                            // Update Building AND - Updated By and Date Updated 
                            buildingAND.UpdatedBy = Geomatic.Core.Sessions.Session.User.Name;
                            buildingAND.DateUpdated = DateTime.Now.ToString("yyyyMMdd");

                            repo.Update(buildingAND);

                            ListViewItem item = CreateItem(buildingAND);
                            controlBuilding.Items.Add(item);
                            controlBuilding.FixColumnWidth();

                            repo.Update(_buildingGroupAND);
                        }
                        // end
                        else
                        {
                            GBuilding building = (GBuilding)feature;

                            // Update AndStatus for Building & Create Building AND & Updadate GroupId 
                            CreateUpdateBuildingAND(repo, building);
                            GBuildingAND buildingAnd = building.GetBuildingANDId();
                            ListViewItem item = CreateItem(buildingAnd);
                            controlBuilding.Items.Add(item);
                            controlBuilding.FixColumnWidth();
                        }
                    }
                }
            }
        }

        private void controlBuilding_DissociateClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlBuilding.SelectedItems.Count == 0)
                {
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory(_buildingGroupAND.SegmentName);

                // noraini ali - allow to select multiple rows & get AND building
                //ListViewItem item1 = controlBuilding.SelectedItems[0];
                foreach (ListViewItem item in controlBuilding.SelectedItems)
                {
                    IGFeature feature = (IGFeature)item.Tag;
                    string strtableName = feature.TableName.ToString();
                    bool b1 = strtableName.Contains("AND");
                    if (b1)
                    {
                        GBuildingAND buildingAND = (GBuildingAND)item.Tag;
                        if ((buildingAND.AreaId == null) || ((int)buildingAND.AreaId != (int)_buildingGroupAND.AreaId))
                        {
                            throw new QualityControlException("Cannot Dissassociate building in different or out of Work Area");
                        }

                        buildingAND.GroupId = 0;

                        // Update Building AND - Updated By and Date Updated 
                        buildingAND.UpdatedBy = Geomatic.Core.Sessions.Session.User.Name;
                        buildingAND.DateUpdated = DateTime.Now.ToString("yyyyMMdd");

                        // update building AND - att with status & andstatus
                        repo.Update(buildingAND);
                        item.Remove();
                        controlBuilding.FixColumnWidth();

                        // update building group AND - status & andstatus
                        repo.Update(_buildingGroupAND);
                    }
                    else
                    {
                        GBuilding building = (GBuilding)item.Tag;

                        if ((building.AreaId == null) || ((int)building.AreaId != (int)_buildingGroupAND.AreaId))
                        {
                            throw new QualityControlException("Cannot Dissassociate building in different or out of Work Area");
                        }

                        CreateUpdateBuildingAND(repo, building);
                        GBuildingAND buildingAnd = building.GetBuildingANDId();
                        buildingAnd.GroupId = 0;

                        item.Remove();
                        controlBuilding.FixColumnWidth();

                        // update ANDSTATUS for building Group 
                        repo.Update(_buildingGroupAND);
                    }
                }
                // end
            }
        }

        public void CreateUpdateBuildingAND(RepositoryFactory repo, GBuilding building)
        {
            // get building AND 
            GBuildingAND buildingAnd = building.GetBuildingANDId();
            if (buildingAnd == null)
            {
                // Create building AND & Copy data from building ADM
                GBuildingAND buildingAND = repo.NewObj<GBuildingAND>();
                buildingAND.CopyFrom(building);

                // update building belong to Builidng Group Id
                buildingAND.GroupId = _buildingGroupAND.OriId;

                // Update new value - Updated By and Date Updated 
                buildingAND.UpdatedBy = Geomatic.Core.Sessions.Session.User.Name;
                buildingAND.DateUpdated = DateTime.Now.ToString("yyyyMMdd");

                // insert data building AND
                buildingAnd = repo.Insert(buildingAND, false);
                repo.Update(buildingAnd);

                // update ADM - status & andstatus
                repo.UpdateByAND(building, true);
            }
        }

        private void cbMirror_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMirror.Checked)
            {
                lblMirror.Text = "BLOCK A SUNWAY SUTERA CONDOMINIUM";
                _buildingGroupAND.BuldingNamePos = 1;
            }
            else
            {
                lblMirror.Text = "SUNWAY SUTERA CONDOMINIUM BLOCK A";
                _buildingGroupAND.BuldingNamePos = null;
            }
        }

        /// noraini ali - OKT 2020 - to hightlight Building - ada problem to get screen display & use graphicsContainer
        //rivate void controlBuilding_highlightBuilding(object sender, EventArgs e)
        // 
        //   if (controlBuilding.SelectedItems.Count == 0)
        //   {
        //   return;
        //   }
        //
        //   foreach (ListViewItem item in controlBuilding.SelectedItems)
        //   {
        //       IGFeature feature = (IGFeature)item.Tag;
        //       IActiveView Activeview = MapDocument.ActiveView;
        //       MapDocument.SelectFeature(feature);
        //
        //       IGeometry geometry = feature.Shape;
        //       switch (geometry.GeometryType)
        //       {
        //           case esriGeometryType.esriGeometryPoint:
        //               IPoint point = new PointClass();
        //               point.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMax);
        //
        //               MovePoint movePoint = new MovePoint(Activeview, ColorUtils.Get(255, 0, 0), ColorUtils.Get(0, 0, 0), 10);
        //               movePoint.Point = point;
        //               movePoint.Start(point);
        //               break;
        //
        //           case esriGeometryType.esriGeometryPolyline:
        //
        //               IPolyline polyline = (IPolyline)geometry;
        //               MoveLine moveline = new MoveLine(Activeview, ColorUtils.Get(255, 0, 0), 2);
        //               moveline.Line = polyline;
        //               moveline.Start(polyline.FromPoint);
        //               break;
        //
        //           default:
        //               break;
        //       }
        //   }

        //   // when use add new element for highlighted --- problem to graphicsContainer ....
        //   foreach (ListViewItem item in controlBuilding.SelectedItems)
        //   {
        //       IGFeature feature = (IGFeature)item.Tag;
        //       string strtableName = feature.TableName.ToString();
        //       bool b1 = strtableName.Contains("ADM");
        //       IGeometry geometry;
        //   
        //       if (b1)
        //       {
        //           GBuilding building = (GBuilding)item.Tag;
        //           geometry = building.Shape;
        //       }
        //       else
        //       {
        //           GBuildingAND buildingAND = (GBuildingAND)item.Tag;
        //           geometry = buildingAND.Shape;
        //       }
        //       
        //       //IGraphicsContainer graphicsContainer = (IGraphicsContainer)this;
        //       //graphicsContainer.DeleteAllElements();
        //   
        //       switch (geometry.GeometryType)
        //       {
        //           case esriGeometryType.esriGeometryPoint:
        //           case esriGeometryType.esriGeometryPolyline:
        //   
        //               IElement element = ElementFactory.CreateElement(geometry, ColorUtils.Get(255, 0, 0), ColorUtils.Get(0, 0, 0));
        //               if (element != null)
        //               {
        //                   element.Geometry = geometry;
        //   
        //                   //graphicsContainer.AddElement(element, 0);
        //               }
        //               break;
        //   
        //           default:
        //               break;
        //       }
        //   }
        //}
    }
}

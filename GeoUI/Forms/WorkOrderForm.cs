﻿using Geomatic.Core.Sessions;
using Geomatic.UI.WomsSR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms
{
    public partial class WorkOrderForm : CollapsibleForm
    {
        private string[] regionIndexArray;
        string[] lnd_index = new string[4];

        public string Username
        {
            get { return usernameTextbox.Text; }
        }

        public string PrtName
        {
            get { return prtnameCombobox.SelectedItem.ToString(); }
        }

        public string WoUrgency
        {
            get { return wourgencyCombobox.SelectedItem.ToString(); }
        }

        public string WoType
        {
            get { return wotypeTextbox.Text; }
        }

        public string RequiredDate
        {
            get { return requireddateDatepicker.Value.ToString("dd/MM/yyyy"); }
        }

        public string WoSummary
        {
            get { return wosummaryTextbox.Text; }
        }

        public string WoDescription
        {
            get { return wodescriptionTextbox.Text; }
        }

        public string LandType
        {
            get { return "Admin"; }
        }

        public string DigitizerName
        {
            get { return usernameTextbox.Text; }
        }

        public string WoCreateDate
        {
            get { return requireddateDatepicker.Value.ToString("dd/MM/yyyy"); }
        }

        public string WoSendDate
        {
            get { return requireddateDatepicker.Value.ToString("dd/MM/yyyy"); }
        }

        public string QDateAssign
        {
            get { return requireddateDatepicker.Value.ToString("dd/MM/yyyy"); }
        }

        public string QExpectedComplitionDate
        {
            get { return requireddateDatepicker.Value.ToString("dd/MM/yyyy"); }
        }

        public WorkOrderForm()
        {
            InitializeComponent();
            usernameTextbox.Text = Session.User.Name;
        }

        public void SetIndexArray(IList<string> regionIndex)
        {
            regionIndexArray = regionIndex.ToArray();
            //Loop through regionIndex array

            for (int i = 0; i < lnd_index.Length; i++)
            {
                if (i < regionIndexArray.Length)
                {
                    lnd_index[i] = regionIndexArray[i];
                }
                else
                {
                    lnd_index[i] = "";
                }
            }
        }

        public string SubmitToWorms()
        {
            //Connect to GeoUi Connected Services WormsSR 
            var binding = new System.ServiceModel.BasicHttpBinding();
            var endPointAddress = new System.ServiceModel.EndpointAddress("http://sysdevweb/API/WOMS/WebService.asmx");
            var client = new WomsWSSoapClient(binding, endPointAddress);

            //Submit to WormsSR to get work order no.
            String wo_num = client.CreateANDWorkOrder(Username, PrtName, WoUrgency, WoType, LandType, RequiredDate, WoSummary, WoDescription, WoCreateDate, WoSendDate, lnd_index[0], lnd_index[1], lnd_index[2], lnd_index[3], DigitizerName, QDateAssign, QExpectedComplitionDate);
            return wo_num;
        }

        public string GetWorkOrderDescription() {
            return WoDescription;
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // PRT NAME
            bool prtnameNotEmpty = !(prtnameCombobox.SelectedItem == null);
            LabelColor(prtnameLabel, prtnameNotEmpty);
            pass &= prtnameNotEmpty;

            // WO Description
            bool wodescriptionNotEmpty = !String.IsNullOrEmpty(WoDescription);
            LabelColor(wodescriptionLabel, wodescriptionNotEmpty);
            pass &= wodescriptionNotEmpty;

            return pass;
        }
    }
}

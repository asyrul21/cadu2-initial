﻿namespace Geomatic.UI.Forms
{
    partial class VerificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRevertFeature = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnVerifyFeature = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFeatureId = new System.Windows.Forms.Label();
            this.txtFeatureId = new System.Windows.Forms.TextBox();
            this.lblFeatureName = new System.Windows.Forms.Label();
            this.txtFeatureName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 81);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(419, 298);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.4F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.6F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196F));
            this.tableLayoutPanel3.Controls.Add(this.btnRevertFeature, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnClose, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnVerifyFeature, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 401);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(419, 31);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnRevertFeature
            // 
            this.btnRevertFeature.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnRevertFeature.Image = global::Geomatic.UI.Properties.Resources.delete;
            this.btnRevertFeature.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRevertFeature.Location = new System.Drawing.Point(3, 4);
            this.btnRevertFeature.Name = "btnRevertFeature";
            this.btnRevertFeature.Size = new System.Drawing.Size(110, 23);
            this.btnRevertFeature.TabIndex = 0;
            this.btnRevertFeature.Text = "Revert To Original";
            this.btnRevertFeature.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRevertFeature.UseVisualStyleBackColor = true;
            this.btnRevertFeature.Click += new System.EventHandler(this.BtnRevertFeature_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(336, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnVerifyFeature
            // 
            this.btnVerifyFeature.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnVerifyFeature.Image = global::Geomatic.UI.Properties.Resources.apply;
            this.btnVerifyFeature.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerifyFeature.Location = new System.Drawing.Point(119, 4);
            this.btnVerifyFeature.Name = "btnVerifyFeature";
            this.btnVerifyFeature.Size = new System.Drawing.Size(84, 23);
            this.btnVerifyFeature.TabIndex = 1;
            this.btnVerifyFeature.Text = "Verify";
            this.btnVerifyFeature.UseVisualStyleBackColor = true;
            this.btnVerifyFeature.Click += new System.EventHandler(this.BtnVerifyFeature_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.28349F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.71651F));
            this.tableLayoutPanel1.Controls.Add(this.lblFeatureId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFeatureId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFeatureName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtFeatureName, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(336, 56);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblFeatureId
            // 
            this.lblFeatureId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFeatureId.AutoSize = true;
            this.lblFeatureId.Location = new System.Drawing.Point(31, 5);
            this.lblFeatureId.Name = "lblFeatureId";
            this.lblFeatureId.Size = new System.Drawing.Size(64, 13);
            this.lblFeatureId.TabIndex = 0;
            this.lblFeatureId.Text = "Feature Id  :";
            this.lblFeatureId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFeatureId
            // 
            this.txtFeatureId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFeatureId.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFeatureId.Enabled = false;
            this.txtFeatureId.Location = new System.Drawing.Point(101, 3);
            this.txtFeatureId.Name = "txtFeatureId";
            this.txtFeatureId.Size = new System.Drawing.Size(232, 20);
            this.txtFeatureId.TabIndex = 1;
            // 
            // lblFeatureName
            // 
            this.lblFeatureName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFeatureName.AutoSize = true;
            this.lblFeatureName.Location = new System.Drawing.Point(15, 33);
            this.lblFeatureName.Name = "lblFeatureName";
            this.lblFeatureName.Size = new System.Drawing.Size(80, 13);
            this.lblFeatureName.TabIndex = 2;
            this.lblFeatureName.Text = "Feature Name :";
            this.lblFeatureName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFeatureName
            // 
            this.txtFeatureName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFeatureName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFeatureName.Enabled = false;
            this.txtFeatureName.Location = new System.Drawing.Point(101, 30);
            this.txtFeatureName.Name = "txtFeatureName";
            this.txtFeatureName.ReadOnly = true;
            this.txtFeatureName.Size = new System.Drawing.Size(232, 20);
            this.txtFeatureName.TabIndex = 3;
            // 
            // VerificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 441);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "VerificationForm";
            this.Text = "Form1";
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblFeatureId;
        private System.Windows.Forms.TextBox txtFeatureId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnRevertFeature;
        private System.Windows.Forms.Button btnVerifyFeature;
        private System.Windows.Forms.Button btnClose;
        //private Controls.SortableListView sortableListView1;
        private System.Windows.Forms.Label lblFeatureName;
        private System.Windows.Forms.TextBox txtFeatureName;
        protected System.Windows.Forms.ListView listView1;
        //private System.Windows.Forms.Button btnShow1;
    }
}
﻿using System;
using Geomatic.UI.Controls;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms
{
    public partial class PropertyMultiForm : CollapsibleForm
    {
        public static string sendtextLotHse;
        public int? SelectedConstructionStatus
        {
            get { return (cbConstStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstStatus.SelectedItem).Code; }
        }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.Code; }
        }
     
        public int InsertedQuantity
        {
            get { return numOfFloors.Value; }
        }

        public int? SelectedYearInstall
        {
            get { return (cbYearInstall.SelectedItem == null) ? (int?)null : Convert.ToInt32(cbYearInstall.SelectedItem); }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public int NumIncrement
        {
            get { return numIncrement.Value; }
        }

        protected GProperty _property;

        public PropertyMultiForm()
            : this(null)
        {
        }

        public PropertyMultiForm(GPropertyAND property)
        {
            InitializeComponent();
            _property = property;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }
        
        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedType >= 10)
            {
                numOfFloors.Value = (cbType.SelectedItem == null) ? 0 : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.DefaultFloor.Value;
            }
        }

        public virtual void SetValues()
        {
            _property.ConstructionStatus = SelectedConstructionStatus;
            _property.Type = SelectedType;
            _property.NumberOfFloor = InsertedQuantity;
            _property.Source = SelectedSource.ToString();
            _property.YearInstall = SelectedYearInstall;
        }
        
        protected bool GetGroupPropType(string GroupPropertyType)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyType> query = new Query<GPropertyType>();
            query.Obj.Code = SelectedType;
            query.AddClause(() => query.Obj.FloorType, "IN ('" + GroupPropertyType + "')");
        
            return repo.Count<GPropertyType>(query) > 0 ? true : false;
        }
    }
}

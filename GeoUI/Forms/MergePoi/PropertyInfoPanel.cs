﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core;

namespace Geomatic.UI.Forms.MergePoi
{
    public partial class PropertyInfoPanel : ParentInfoPanel
    {
        public PropertyInfoPanel()
        {
            InitializeComponent();
        }

        public override void LoadFeature(IGPoiParent poiParent)
        {
            if (poiParent is GProperty)
            {
                GProperty property = (GProperty)poiParent;
                txtId.Text = property.OID.ToString();
                txtLot.Text = property.Lot;
                txtHouse.Text = property.House;
                GStreet street = property.GetStreet();
                if (street != null)
                {
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }
        }
    }
}

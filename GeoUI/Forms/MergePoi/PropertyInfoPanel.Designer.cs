﻿namespace Geomatic.UI.Forms.MergePoi
{
    partial class PropertyInfoPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.lblFeatureType = new System.Windows.Forms.Label();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.lblFeatureTypeText = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtId, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.lblId, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.lblFeatureType, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtHouse, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.txtLot, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.txtPostcode, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.txtStreetName, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtStreetType, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblStreetType, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lblStreetName, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.lblStreetName2, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.lblSection, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.txtSection, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.lblCity, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.txtState, 3, 5);
            this.tableLayoutPanel.Controls.Add(this.txtCity, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.lblState, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.lblPostcode, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.txtStreetName2, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblFeatureTypeText, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(300, 170);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(238, 4);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(59, 20);
            this.txtId.TabIndex = 3;
            // 
            // lblId
            // 
            this.lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(213, 7);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(19, 13);
            this.lblId.TabIndex = 2;
            this.lblId.Text = "Id:";
            // 
            // lblFeatureType
            // 
            this.lblFeatureType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFeatureType.AutoSize = true;
            this.lblFeatureType.Location = new System.Drawing.Point(9, 7);
            this.lblFeatureType.Name = "lblFeatureType";
            this.lblFeatureType.Size = new System.Drawing.Size(73, 13);
            this.lblFeatureType.TabIndex = 0;
            this.lblFeatureType.Text = "Feature Type:";
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(238, 32);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.ReadOnly = true;
            this.txtHouse.Size = new System.Drawing.Size(59, 20);
            this.txtHouse.TabIndex = 7;
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(88, 32);
            this.txtLot.Name = "txtLot";
            this.txtLot.ReadOnly = true;
            this.txtLot.Size = new System.Drawing.Size(59, 20);
            this.txtLot.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "House:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lot:";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(238, 116);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(59, 20);
            this.txtPostcode.TabIndex = 17;
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(238, 60);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(59, 20);
            this.txtStreetName.TabIndex = 11;
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(88, 60);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(59, 20);
            this.txtStreetType.TabIndex = 9;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(17, 63);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 13);
            this.lblStreetType.TabIndex = 8;
            this.lblStreetType.Text = "Street Type:";
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(163, 63);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 10;
            this.lblStreetName.Text = "Street Name:";
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(157, 91);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 13);
            this.lblStreetName2.TabIndex = 12;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(36, 119);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 14;
            this.lblSection.Text = "Section:";
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(88, 116);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(59, 20);
            this.txtSection.TabIndex = 15;
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(55, 148);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 18;
            this.lblCity.Text = "City:";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(238, 145);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(59, 20);
            this.txtState.TabIndex = 21;
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(88, 145);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(59, 20);
            this.txtCity.TabIndex = 19;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(197, 148);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 20;
            this.lblState.Text = "State:";
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(177, 119);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 16;
            this.lblPostcode.Text = "Postcode:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(238, 88);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(59, 20);
            this.txtStreetName2.TabIndex = 13;
            // 
            // lblFeatureTypeText
            // 
            this.lblFeatureTypeText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFeatureTypeText.AutoSize = true;
            this.lblFeatureTypeText.Location = new System.Drawing.Point(88, 7);
            this.lblFeatureTypeText.Name = "lblFeatureTypeText";
            this.lblFeatureTypeText.Size = new System.Drawing.Size(59, 13);
            this.lblFeatureTypeText.TabIndex = 1;
            this.lblFeatureTypeText.Text = "Property";
            this.lblFeatureTypeText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PropertyInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "PropertyInfoPanel";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.TextBox txtHouse;
        protected System.Windows.Forms.TextBox txtLot;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.Label lblFeatureType;
        protected System.Windows.Forms.Label lblFeatureTypeText;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Label lblId;
    }
}

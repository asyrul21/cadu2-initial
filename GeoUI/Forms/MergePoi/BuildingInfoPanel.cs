﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.MergePoi
{
    public partial class BuildingInfoPanel : ParentInfoPanel
    {
        public BuildingInfoPanel()
        {
            InitializeComponent();
        }

        public override void LoadFeature(IGPoiParent poiParent)
        {
            if (poiParent is GBuilding)
            {
                GBuilding building = (GBuilding)poiParent;
                txtId.Text = building.OID.ToString();
                txtName.Text = building.Name;
                txtName2.Text = building.Name2;

                GProperty property = building.GetProperty();
                if (property != null)
                {
                    txtLot.Text = property.Lot;
                    txtHouse.Text = property.House;
                }

                GStreet street = building.GetStreet();
                if (street != null)
                {
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }
        }
    }
}

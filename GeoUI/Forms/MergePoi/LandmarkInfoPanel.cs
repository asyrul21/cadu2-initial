﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.MergePoi
{
    public partial class LandmarkInfoPanel : ParentInfoPanel
    {
        public LandmarkInfoPanel()
        {
            InitializeComponent();
        }

        public override void LoadFeature(IGPoiParent poiParent)
        {
            if (poiParent is GLandmark)
            {
                GLandmark landmark = (GLandmark)poiParent;
                txtId.Text = landmark.OID.ToString();
                txtName.Text = landmark.Name;
                txtName2.Text = landmark.Name2;

                GStreet street = landmark.GetStreet();
                if (street != null)
                {
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }
        }
    }
}

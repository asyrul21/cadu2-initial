﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.MergePoi
{
    public partial class MergePoiForm : BaseForm
    {
        public IEnumerable<GPoi> Pois1
        {
            get
            {
                foreach (ListViewItem item in lvPoi1.Items)
                {
                    yield return item.Tag as GPoi;
                }
            }
        }

        public IEnumerable<GPoi> Pois2
        {
            get
            {
                foreach (ListViewItem item in lvPoi2.Items)
                {
                    yield return item.Tag as GPoi;
                }
            }
        }

        private IGPoiParent _poiParent1;
        private IGPoiParent _poiParent2;

        public MergePoiForm()
            : this(null, null)
        {

        }

        public MergePoiForm(IGPoiParent poiParent1, IGPoiParent poiParent2)
        {
            InitializeComponent();
            Text = "Merge Poi";
            _poiParent1 = poiParent1;
            _poiParent2 = poiParent2;
        }

        private void MergePoiForm_Load(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                ParentInfoPanel infoPanel1 = InfoPanelFactory.Create(_poiParent1);
                infoPanel1.LoadFeature(_poiParent1);
                infoPanel1.Dock = DockStyle.Fill;
                parent1Panel.Controls.Add(infoPanel1);

                ParentInfoPanel infoPanel2 = InfoPanelFactory.Create(_poiParent2);
                infoPanel2.LoadFeature(_poiParent2);
                infoPanel2.Dock = DockStyle.Fill;
                parent2Panel.Controls.Add(infoPanel2);

                lvPoi1.Columns.Add("Id");
                lvPoi1.Columns.Add("Code");
                lvPoi1.Columns.Add("Name");
                lvPoi1.Columns.Add("Name2");
                lvPoi1.Columns.Add("Description");
                lvPoi1.Columns.Add("Url");

                lvPoi2.Columns.Add("Id");
                lvPoi2.Columns.Add("Code");
                lvPoi2.Columns.Add("Name");
                lvPoi2.Columns.Add("Name2");
                lvPoi2.Columns.Add("Description");
                lvPoi2.Columns.Add("Url");

                PopulatePoi1();
                PopulatePoi2();
            }
        }

        private void PopulatePoi1()
        {
            lvPoi1.BeginUpdate();
            lvPoi1.Sorting = SortOrder.None;
            lvPoi1.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _poiParent1.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);
                item.Tag = poi;

                items.Add(item);
            }

            lvPoi1.Items.AddRange(items.ToArray());
            lvPoi1.FixColumnWidth();
            lvPoi1.EndUpdate();

            if (lvPoi1.Items.Count > 0)
            {
                lvPoi1.Items[0].Selected = true;
            }
            else
            {
                lvPoi1_SelectedIndexChanged(null, null);
            }
        }

        private void PopulatePoi2()
        {
            lvPoi2.BeginUpdate();
            lvPoi2.Sorting = SortOrder.None;
            lvPoi2.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _poiParent2.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);
                item.Tag = poi;

                items.Add(item);
            }

            lvPoi2.Items.AddRange(items.ToArray());
            lvPoi2.FixColumnWidth();
            lvPoi2.EndUpdate();

            if (lvPoi2.Items.Count > 0)
            {
                lvPoi2.Items[0].Selected = true;
            }
            else
            {
                lvPoi2_SelectedIndexChanged(null, null);
            }
        }

        private void lvPoi1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnToRight.Enabled = lvPoi1.SelectedItems.Count > 0;
        }

        private void lvPoi2_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnToLeft.Enabled = lvPoi2.SelectedItems.Count > 0;
        }

        private void btnToRight_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvPoi1.SelectedItems)
            {
                GPoi poi = (GPoi)item.Tag;
                if (poi.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        using (InfoMessageBox box = new InfoMessageBox())
                        {
                            box.SetText("Navigation ready lock.");
                            box.Show(this);
                        }   
                        continue;
                    }
                }
                item.Remove();
                lvPoi2.Items.Add(item);
            }
            lvPoi1.FixColumnWidth();
            lvPoi2.FixColumnWidth();
        }

        private void btnToLeft_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvPoi2.SelectedItems)
            {
                GPoi poi = (GPoi)item.Tag;
                if (poi.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        using (InfoMessageBox box = new InfoMessageBox())
                        {
                            box.SetText("Navigation ready lock.");
                            box.Show(this);
                        }
                        continue;
                    }
                }
                item.Remove();
                lvPoi1.Items.Add(item);
            }
            lvPoi1.FixColumnWidth();
            lvPoi2.FixColumnWidth();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

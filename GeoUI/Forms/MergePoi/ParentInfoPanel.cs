﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.MergePoi
{
    public partial class ParentInfoPanel : UserControl, IParentInfoPanel
    {
        public ParentInfoPanel()
        {
            InitializeComponent();
        }

        public virtual void LoadFeature(IGPoiParent poiParent)
        {
        }
    }
}

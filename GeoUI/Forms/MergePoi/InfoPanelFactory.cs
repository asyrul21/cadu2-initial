﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.MergePoi
{
    class InfoPanelFactory
    {
        public static ParentInfoPanel Create(IGPoiParent poiParent)
        {
            if (poiParent is GProperty)
            {
                return new PropertyInfoPanel();
            }
            if (poiParent is GBuilding)
            {
                return new BuildingInfoPanel();
            }
            if (poiParent is GLandmark)
            {
                return new LandmarkInfoPanel();
            }
            return new ParentInfoPanel();
        }
    }
}

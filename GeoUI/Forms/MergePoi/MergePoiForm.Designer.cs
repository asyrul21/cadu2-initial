﻿namespace Geomatic.UI.Forms.MergePoi
{
    partial class MergePoiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvPoi1 = new Geomatic.UI.Controls.SortableListView();
            this.lvPoi2 = new Geomatic.UI.Controls.SortableListView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnToLeft = new System.Windows.Forms.Button();
            this.btnToRight = new System.Windows.Forms.Button();
            this.parent1Panel = new System.Windows.Forms.Panel();
            this.parent2Panel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvPoi1
            // 
            this.lvPoi1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPoi1.FullRowSelect = true;
            this.lvPoi1.HideSelection = false;
            this.lvPoi1.Location = new System.Drawing.Point(3, 203);
            this.lvPoi1.Name = "lvPoi1";
            this.lvPoi1.Size = new System.Drawing.Size(403, 153);
            this.lvPoi1.TabIndex = 2;
            this.lvPoi1.UseCompatibleStateImageBehavior = false;
            this.lvPoi1.View = System.Windows.Forms.View.Details;
            this.lvPoi1.SelectedIndexChanged += new System.EventHandler(this.lvPoi1_SelectedIndexChanged);
            // 
            // lvPoi2
            // 
            this.lvPoi2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPoi2.FullRowSelect = true;
            this.lvPoi2.HideSelection = false;
            this.lvPoi2.Location = new System.Drawing.Point(454, 203);
            this.lvPoi2.Name = "lvPoi2";
            this.lvPoi2.Size = new System.Drawing.Size(403, 153);
            this.lvPoi2.TabIndex = 3;
            this.lvPoi2.UseCompatibleStateImageBehavior = false;
            this.lvPoi2.View = System.Windows.Forms.View.Details;
            this.lvPoi2.SelectedIndexChanged += new System.EventHandler(this.lvPoi2_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(797, 377);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Location = new System.Drawing.Point(716, 377);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lvPoi2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lvPoi1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.parent1Panel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.parent2Panel, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(860, 359);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(412, 203);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(36, 153);
            this.panel1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnToLeft, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnToRight, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(36, 153);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnToLeft
            // 
            this.btnToLeft.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnToLeft.Location = new System.Drawing.Point(3, 79);
            this.btnToLeft.Name = "btnToLeft";
            this.btnToLeft.Size = new System.Drawing.Size(30, 30);
            this.btnToLeft.TabIndex = 1;
            this.btnToLeft.Text = "<";
            this.btnToLeft.UseVisualStyleBackColor = true;
            this.btnToLeft.Click += new System.EventHandler(this.btnToLeft_Click);
            // 
            // btnToRight
            // 
            this.btnToRight.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnToRight.Location = new System.Drawing.Point(3, 43);
            this.btnToRight.Name = "btnToRight";
            this.btnToRight.Size = new System.Drawing.Size(30, 30);
            this.btnToRight.TabIndex = 0;
            this.btnToRight.Text = ">";
            this.btnToRight.UseVisualStyleBackColor = true;
            this.btnToRight.Click += new System.EventHandler(this.btnToRight_Click);
            // 
            // parent1Panel
            // 
            this.parent1Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parent1Panel.Location = new System.Drawing.Point(3, 3);
            this.parent1Panel.Name = "parent1Panel";
            this.parent1Panel.Size = new System.Drawing.Size(403, 194);
            this.parent1Panel.TabIndex = 0;
            // 
            // parent2Panel
            // 
            this.parent2Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parent2Panel.Location = new System.Drawing.Point(454, 3);
            this.parent2Panel.Name = "parent2Panel";
            this.parent2Panel.Size = new System.Drawing.Size(403, 194);
            this.parent2Panel.TabIndex = 0;
            // 
            // MergePoiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 412);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Name = "MergePoiForm";
            this.Text = "MergePoiForm";
            this.Load += new System.EventHandler(this.MergePoiForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private Controls.SortableListView lvPoi1;
        private Controls.SortableListView lvPoi2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnToRight;
        private System.Windows.Forms.Panel parent1Panel;
        private System.Windows.Forms.Panel parent2Panel;
        private System.Windows.Forms.Button btnToLeft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}
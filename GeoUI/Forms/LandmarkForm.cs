﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.UI.Forms
{
    public partial class LandmarkForm : CollapsibleForm
    {
        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string SelectedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string InsertedName2
        {
            get { return txtName2.Text; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        protected GLandmark _landmark;

        public LandmarkForm()
            : this(null)
        {
        }

        public LandmarkForm(GLandmark landmark)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
                btnSetCode,
                lblCode, txtCode,
                lblName, txtName,
                lblName2, txtName2,
                lblSource, cbSource,
                btnApply);

            _landmark = landmark;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);

            controlPoi.Columns.Add("Id");
            controlPoi.Columns.Add("Code");
            controlPoi.Columns.Add("Name");
            controlPoi.Columns.Add("Name2");
            controlPoi.Columns.Add("Navi_Status");
            controlPoi.Columns.Add("Description");
            controlPoi.Columns.Add("Url");

            txtId.Text = _landmark.OID.ToString();

            cbNaviStatus.Text = _landmark.NavigationStatusValue;
            txtName.Text = _landmark.Name;
            txtName2.Text = _landmark.Name2;
            txtCode.Text = _landmark.Code;
            GCode1 code1 = _landmark.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _landmark.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _landmark.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            cbSource.Text = _landmark.SourceValue;
            txtDateCreated.Text = _landmark.DateCreated;
            txtDateUpdated.Text = _landmark.DateUpdated;
            txtCreator.Text = _landmark.CreatedBy;
            txtUpdater.Text = _landmark.UpdatedBy;

            GStreet street = _landmark.GetStreet();
            if (street != null)
            {
                GStreetAND StreetAND = street.GetStreetANDId();

                if (StreetAND != null)
                {
                    txtStreetId.Text = StreetAND.OriId.ToString();
                    txtStreetType.Text = StreetAND.TypeValue;
                    txtStreetName.Text = StreetAND.Name;
                    txtStreetName2.Text = StreetAND.Name2;
                    txtSection.Text = StreetAND.Section;
                    txtPostcode.Text = StreetAND.Postcode;
                    txtCity.Text = StreetAND.City;
                    txtState.Text = StreetAND.State;
                }
                else
                {
                    txtStreetId.Text = street.OID.ToString();
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtCity.Text = street.City;
                    txtSection.Text = street.Section;
                    txtState.Text = street.State;
                }
            }
            if (Session.User.GetGroup().Name != "AND")
            {
                PopulatePoi();
            }
        }

        public void disablePOIEdit()
        {
            controlPoi.Enabled = false;
        }

        protected void PopulatePoi()
        {
            controlPoi.BeginUpdate();
            controlPoi.Sorting = SortOrder.None;
            controlPoi.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _landmark.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.NavigationStatusValue);
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);
                item.Tag = poi;

                items.Add(item);
            }
            controlPoi.Items.AddRange(items.ToArray());
            controlPoi.FixColumnWidth();
            controlPoi.EndUpdate();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Name
            bool isNameValid = LandmarkValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Name 2
            bool isName2Valid = LandmarkValidator.CheckName2(txtName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Source
            bool isSourceValid = LandmarkValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _landmark.NavigationStatus = SelectedNavigationStatus;
            _landmark.Code = SelectedCode;
            _landmark.Name = InsertedName;
            _landmark.Name2 = InsertedName2;
            _landmark.Source = SelectedSource;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                GCode1 code1 = code3.GetCode1();
                GCode2 code2 = code3.GetCode2();
                categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
                categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
                categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            }
        }

        private void controlPoi_AddClick(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(_landmark.SegmentName);
            GPoi newPoi = repo.NewObj<GPoi>();
            newPoi.Init();
            newPoi.ParentId = _landmark.OID;
            newPoi.ParentType = (int)ParentClass.Landmark;
            newPoi.Shape = _landmark.Shape;
            newPoi = repo.Insert(newPoi, false);

            using (AddPoiForm form = new AddPoiForm(newPoi))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    repo.Delete(newPoi);
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(newPoi);

                    ListViewItem item = new ListViewItem();
                    item.Text = newPoi.OID.ToString();
                    item.SubItems.Add(newPoi.Code);
                    item.SubItems.Add(newPoi.Name);
                    item.SubItems.Add(newPoi.Name2);
                    item.SubItems.Add(newPoi.NavigationStatusValue);
                    item.SubItems.Add(newPoi.Description);
                    item.SubItems.Add(newPoi.Url);
                    item.Tag = newPoi;
                    controlPoi.Items.Add(item);
                    controlPoi.FixColumnWidth();
                }
            }
        }

        private void controlPoi_EditClick(object sender, EventArgs e)
        {
            if (controlPoi.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_landmark.SegmentName);
            ListViewItem item = controlPoi.SelectedItems[0];
            GPoi poi = (GPoi)item.Tag;

            using (EditPoiForm form = new EditPoiForm(poi))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(poi);

                    item.SubItems.Clear();
                    item.Text = poi.OID.ToString();
                    item.SubItems.Add(poi.Code);
                    item.SubItems.Add(poi.Name);
                    item.SubItems.Add(poi.Name2);
                    item.SubItems.Add(poi.NavigationStatusValue);
                    item.SubItems.Add(poi.Description);
                    item.SubItems.Add(poi.Url);
                    item.Tag = poi;
                    controlPoi.FixColumnWidth();
                }
            }
        }

        private void controlPoi_DeleteClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlPoi.SelectedItems.Count == 0)
                {
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory(_landmark.SegmentName);
                ListViewItem item = controlPoi.SelectedItems[0];
                GPoi poi = (GPoi)item.Tag;
                if (poi.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        using (InfoMessageBox box = new InfoMessageBox())
                        {
                            box.SetText("Navigation ready lock.");
                            box.Show(this);
                        }
                        return;
                    }
                }
                repo.Delete(poi);
                item.Remove();
                controlPoi.FixColumnWidth();
            }
        }
    }
}

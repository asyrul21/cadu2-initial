﻿namespace Geomatic.UI.Forms
{
    partial class StreetRestrictionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.updatesLayout = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.lvlStreetType = new System.Windows.Forms.Label();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.txtEndId10 = new System.Windows.Forms.TextBox();
            this.txtEndId8 = new System.Windows.Forms.TextBox();
            this.txtEndId6 = new System.Windows.Forms.TextBox();
            this.txtEndId4 = new System.Windows.Forms.TextBox();
            this.txtEndId2 = new System.Windows.Forms.TextBox();
            this.txtEndId9 = new System.Windows.Forms.TextBox();
            this.txtEndId7 = new System.Windows.Forms.TextBox();
            this.txtEndId5 = new System.Windows.Forms.TextBox();
            this.txtEndId3 = new System.Windows.Forms.TextBox();
            this.lblEndId10 = new System.Windows.Forms.Label();
            this.lblEndId8 = new System.Windows.Forms.Label();
            this.lblEndId6 = new System.Windows.Forms.Label();
            this.lblEndId4 = new System.Windows.Forms.Label();
            this.lblEndId2 = new System.Windows.Forms.Label();
            this.lblEndId9 = new System.Windows.Forms.Label();
            this.lblEndId7 = new System.Windows.Forms.Label();
            this.lblEndId5 = new System.Windows.Forms.Label();
            this.lblEndId3 = new System.Windows.Forms.Label();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.lblJunctionId = new System.Windows.Forms.Label();
            this.txtJunctionId = new System.Windows.Forms.TextBox();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.txtStartId = new System.Windows.Forms.TextBox();
            this.lblStartId = new System.Windows.Forms.Label();
            this.lblEndId1 = new System.Windows.Forms.Label();
            this.txtEndId1 = new System.Windows.Forms.TextBox();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.updatesLayout.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 373);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.updatesLayout);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 303);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 8;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // updatesLayout
            // 
            this.updatesLayout.ColumnCount = 4;
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.Controls.Add(this.txtDateUpdated, 3, 1);
            this.updatesLayout.Controls.Add(this.lblCreator, 0, 0);
            this.updatesLayout.Controls.Add(this.txtDateCreated, 3, 0);
            this.updatesLayout.Controls.Add(this.txtUpdater, 1, 1);
            this.updatesLayout.Controls.Add(this.lblUpdater, 0, 1);
            this.updatesLayout.Controls.Add(this.lblDateUpdated, 2, 1);
            this.updatesLayout.Controls.Add(this.txtCreator, 1, 0);
            this.updatesLayout.Controls.Add(this.lblDateCreated, 2, 0);
            this.updatesLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatesLayout.Location = new System.Drawing.Point(3, 16);
            this.updatesLayout.Name = "updatesLayout";
            this.updatesLayout.RowCount = 2;
            this.updatesLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.Size = new System.Drawing.Size(554, 51);
            this.updatesLayout.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblStreetName, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.lvlStreetType, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.txtStreetName, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtStreetType, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.txtEndId10, 3, 8);
            this.tableLayoutPanel.Controls.Add(this.txtEndId8, 3, 7);
            this.tableLayoutPanel.Controls.Add(this.txtEndId6, 3, 6);
            this.tableLayoutPanel.Controls.Add(this.txtEndId4, 3, 5);
            this.tableLayoutPanel.Controls.Add(this.txtEndId2, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.txtEndId9, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.txtEndId7, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.txtEndId5, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.txtEndId3, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.lblEndId10, 2, 8);
            this.tableLayoutPanel.Controls.Add(this.lblEndId8, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.lblEndId6, 2, 6);
            this.tableLayoutPanel.Controls.Add(this.lblEndId4, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.lblEndId2, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.lblEndId9, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.lblEndId7, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.lblEndId5, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.lblEndId3, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.lblJunctionId, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.txtJunctionId, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblNaviStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.txtStartId, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblStartId, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.lblEndId1, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.txtEndId1, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 51);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 9;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 252);
            this.tableLayoutPanel.TabIndex = 7;
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(318, 63);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 6;
            this.lblStreetName.Text = "Street Name:";
            // 
            // lvlStreetType
            // 
            this.lvlStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lvlStreetType.AutoSize = true;
            this.lvlStreetType.Location = new System.Drawing.Point(42, 63);
            this.lvlStreetType.Name = "lvlStreetType";
            this.lvlStreetType.Size = new System.Drawing.Size(65, 13);
            this.lvlStreetType.TabIndex = 4;
            this.lvlStreetType.Text = "Street Type:";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(393, 60);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(164, 20);
            this.txtStreetName.TabIndex = 7;
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 60);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(164, 20);
            this.txtStreetType.TabIndex = 5;
            // 
            // txtEndId10
            // 
            this.txtEndId10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId10.Location = new System.Drawing.Point(393, 228);
            this.txtEndId10.Name = "txtEndId10";
            this.txtEndId10.ReadOnly = true;
            this.txtEndId10.Size = new System.Drawing.Size(164, 20);
            this.txtEndId10.TabIndex = 31;
            // 
            // txtEndId8
            // 
            this.txtEndId8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId8.Location = new System.Drawing.Point(393, 200);
            this.txtEndId8.Name = "txtEndId8";
            this.txtEndId8.ReadOnly = true;
            this.txtEndId8.Size = new System.Drawing.Size(164, 20);
            this.txtEndId8.TabIndex = 27;
            // 
            // txtEndId6
            // 
            this.txtEndId6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId6.Location = new System.Drawing.Point(393, 172);
            this.txtEndId6.Name = "txtEndId6";
            this.txtEndId6.ReadOnly = true;
            this.txtEndId6.Size = new System.Drawing.Size(164, 20);
            this.txtEndId6.TabIndex = 23;
            // 
            // txtEndId4
            // 
            this.txtEndId4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId4.Location = new System.Drawing.Point(393, 144);
            this.txtEndId4.Name = "txtEndId4";
            this.txtEndId4.ReadOnly = true;
            this.txtEndId4.Size = new System.Drawing.Size(164, 20);
            this.txtEndId4.TabIndex = 19;
            // 
            // txtEndId2
            // 
            this.txtEndId2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId2.Location = new System.Drawing.Point(393, 116);
            this.txtEndId2.Name = "txtEndId2";
            this.txtEndId2.ReadOnly = true;
            this.txtEndId2.Size = new System.Drawing.Size(164, 20);
            this.txtEndId2.TabIndex = 15;
            // 
            // txtEndId9
            // 
            this.txtEndId9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId9.Location = new System.Drawing.Point(113, 228);
            this.txtEndId9.Name = "txtEndId9";
            this.txtEndId9.ReadOnly = true;
            this.txtEndId9.Size = new System.Drawing.Size(164, 20);
            this.txtEndId9.TabIndex = 29;
            // 
            // txtEndId7
            // 
            this.txtEndId7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId7.Location = new System.Drawing.Point(113, 200);
            this.txtEndId7.Name = "txtEndId7";
            this.txtEndId7.ReadOnly = true;
            this.txtEndId7.Size = new System.Drawing.Size(164, 20);
            this.txtEndId7.TabIndex = 25;
            // 
            // txtEndId5
            // 
            this.txtEndId5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId5.Location = new System.Drawing.Point(113, 172);
            this.txtEndId5.Name = "txtEndId5";
            this.txtEndId5.ReadOnly = true;
            this.txtEndId5.Size = new System.Drawing.Size(164, 20);
            this.txtEndId5.TabIndex = 21;
            // 
            // txtEndId3
            // 
            this.txtEndId3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId3.Location = new System.Drawing.Point(113, 144);
            this.txtEndId3.Name = "txtEndId3";
            this.txtEndId3.ReadOnly = true;
            this.txtEndId3.Size = new System.Drawing.Size(164, 20);
            this.txtEndId3.TabIndex = 17;
            // 
            // lblEndId10
            // 
            this.lblEndId10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId10.AutoSize = true;
            this.lblEndId10.Location = new System.Drawing.Point(331, 231);
            this.lblEndId10.Name = "lblEndId10";
            this.lblEndId10.Size = new System.Drawing.Size(56, 13);
            this.lblEndId10.TabIndex = 30;
            this.lblEndId10.Text = "End Id 10:";
            // 
            // lblEndId8
            // 
            this.lblEndId8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId8.AutoSize = true;
            this.lblEndId8.Location = new System.Drawing.Point(337, 203);
            this.lblEndId8.Name = "lblEndId8";
            this.lblEndId8.Size = new System.Drawing.Size(50, 13);
            this.lblEndId8.TabIndex = 26;
            this.lblEndId8.Text = "End Id 8:";
            // 
            // lblEndId6
            // 
            this.lblEndId6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId6.AutoSize = true;
            this.lblEndId6.Location = new System.Drawing.Point(337, 175);
            this.lblEndId6.Name = "lblEndId6";
            this.lblEndId6.Size = new System.Drawing.Size(50, 13);
            this.lblEndId6.TabIndex = 22;
            this.lblEndId6.Text = "End Id 6:";
            // 
            // lblEndId4
            // 
            this.lblEndId4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId4.AutoSize = true;
            this.lblEndId4.Location = new System.Drawing.Point(337, 147);
            this.lblEndId4.Name = "lblEndId4";
            this.lblEndId4.Size = new System.Drawing.Size(50, 13);
            this.lblEndId4.TabIndex = 18;
            this.lblEndId4.Text = "End Id 4:";
            // 
            // lblEndId2
            // 
            this.lblEndId2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId2.AutoSize = true;
            this.lblEndId2.Location = new System.Drawing.Point(337, 119);
            this.lblEndId2.Name = "lblEndId2";
            this.lblEndId2.Size = new System.Drawing.Size(50, 13);
            this.lblEndId2.TabIndex = 14;
            this.lblEndId2.Text = "End Id 2:";
            // 
            // lblEndId9
            // 
            this.lblEndId9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId9.AutoSize = true;
            this.lblEndId9.Location = new System.Drawing.Point(57, 231);
            this.lblEndId9.Name = "lblEndId9";
            this.lblEndId9.Size = new System.Drawing.Size(50, 13);
            this.lblEndId9.TabIndex = 28;
            this.lblEndId9.Text = "End Id 9:";
            // 
            // lblEndId7
            // 
            this.lblEndId7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId7.AutoSize = true;
            this.lblEndId7.Location = new System.Drawing.Point(57, 203);
            this.lblEndId7.Name = "lblEndId7";
            this.lblEndId7.Size = new System.Drawing.Size(50, 13);
            this.lblEndId7.TabIndex = 24;
            this.lblEndId7.Text = "End Id 7:";
            // 
            // lblEndId5
            // 
            this.lblEndId5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId5.AutoSize = true;
            this.lblEndId5.Location = new System.Drawing.Point(57, 175);
            this.lblEndId5.Name = "lblEndId5";
            this.lblEndId5.Size = new System.Drawing.Size(50, 13);
            this.lblEndId5.TabIndex = 20;
            this.lblEndId5.Text = "End Id 5:";
            // 
            // lblEndId3
            // 
            this.lblEndId3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId3.AutoSize = true;
            this.lblEndId3.Location = new System.Drawing.Point(57, 147);
            this.lblEndId3.Name = "lblEndId3";
            this.lblEndId3.Size = new System.Drawing.Size(50, 13);
            this.lblEndId3.TabIndex = 16;
            this.lblEndId3.Text = "End Id 3:";
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(393, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(164, 21);
            this.cbNaviStatus.TabIndex = 1;
            // 
            // lblJunctionId
            // 
            this.lblJunctionId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblJunctionId.AutoSize = true;
            this.lblJunctionId.Location = new System.Drawing.Point(45, 91);
            this.lblJunctionId.Name = "lblJunctionId";
            this.lblJunctionId.Size = new System.Drawing.Size(62, 13);
            this.lblJunctionId.TabIndex = 8;
            this.lblJunctionId.Text = "Junction Id:";
            // 
            // txtJunctionId
            // 
            this.txtJunctionId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtJunctionId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJunctionId.Location = new System.Drawing.Point(113, 88);
            this.txtJunctionId.Name = "txtJunctionId";
            this.txtJunctionId.ReadOnly = true;
            this.txtJunctionId.Size = new System.Drawing.Size(164, 20);
            this.txtJunctionId.TabIndex = 9;
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(293, 7);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 0;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // txtStartId
            // 
            this.txtStartId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStartId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStartId.Location = new System.Drawing.Point(393, 88);
            this.txtStartId.Name = "txtStartId";
            this.txtStartId.ReadOnly = true;
            this.txtStartId.Size = new System.Drawing.Size(164, 20);
            this.txtStartId.TabIndex = 11;
            // 
            // lblStartId
            // 
            this.lblStartId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStartId.AutoSize = true;
            this.lblStartId.Location = new System.Drawing.Point(343, 91);
            this.lblStartId.Name = "lblStartId";
            this.lblStartId.Size = new System.Drawing.Size(44, 13);
            this.lblStartId.TabIndex = 10;
            this.lblStartId.Text = "Start Id:";
            // 
            // lblEndId1
            // 
            this.lblEndId1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblEndId1.AutoSize = true;
            this.lblEndId1.Location = new System.Drawing.Point(57, 119);
            this.lblEndId1.Name = "lblEndId1";
            this.lblEndId1.Size = new System.Drawing.Size(50, 13);
            this.lblEndId1.TabIndex = 12;
            this.lblEndId1.Text = "End Id 1:";
            // 
            // txtEndId1
            // 
            this.txtEndId1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId1.Location = new System.Drawing.Point(113, 116);
            this.txtEndId1.Name = "txtEndId1";
            this.txtEndId1.ReadOnly = true;
            this.txtEndId1.Size = new System.Drawing.Size(164, 20);
            this.txtEndId1.TabIndex = 13;
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(393, 31);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(164, 21);
            this.cbSource.TabIndex = 3;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(343, 35);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 2;
            this.lblSource.Text = "Source:";
            // 
            // StreetRestrictionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 410);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "StreetRestrictionForm";
            this.Text = "StreetRestrictionForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.updatesLayout.ResumeLayout(false);
            this.updatesLayout.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel updatesLayout;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Label lblJunctionId;
        protected System.Windows.Forms.TextBox txtJunctionId;
        protected System.Windows.Forms.Label lblStartId;
        protected System.Windows.Forms.TextBox txtEndId1;
        protected System.Windows.Forms.TextBox txtStartId;
        protected System.Windows.Forms.Label lblEndId1;
        protected Controls.UpperComboBox cbNaviStatus;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected System.Windows.Forms.Label lblSource;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.TextBox txtEndId2;
        protected System.Windows.Forms.TextBox txtEndId9;
        protected System.Windows.Forms.TextBox txtEndId7;
        protected System.Windows.Forms.TextBox txtEndId5;
        protected System.Windows.Forms.TextBox txtEndId3;
        protected System.Windows.Forms.Label lblEndId10;
        protected System.Windows.Forms.Label lblEndId8;
        protected System.Windows.Forms.Label lblEndId6;
        protected System.Windows.Forms.Label lblEndId4;
        protected System.Windows.Forms.Label lblEndId2;
        protected System.Windows.Forms.Label lblEndId9;
        protected System.Windows.Forms.Label lblEndId7;
        protected System.Windows.Forms.Label lblEndId5;
        protected System.Windows.Forms.Label lblEndId3;
        protected System.Windows.Forms.TextBox txtEndId10;
        protected System.Windows.Forms.TextBox txtEndId8;
        protected System.Windows.Forms.TextBox txtEndId6;
        protected System.Windows.Forms.TextBox txtEndId4;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.Label lvlStreetType;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TextBox txtStreetType;
    }
}
﻿using System;

namespace Geomatic.UI.Forms
{
    partial class VerifyWorkArea2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtWorkOrderNo = new System.Windows.Forms.TextBox();
            this.txtCompleteFlag = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFixBugWA = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSegmentName = new System.Windows.Forms.TextBox();
            this.cbWorkAreaId = new System.Windows.Forms.ComboBox();
            this.btnCancelVerify = new System.Windows.Forms.Button();
            this.btnStartVerify = new System.Windows.Forms.Button();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.btnFilterADD = new System.Windows.Forms.Button();
            this.btnVerifyADD = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.chkProperty = new System.Windows.Forms.CheckBox();
            this.chkLandmark = new System.Windows.Forms.CheckBox();
            this.chkJunction = new System.Windows.Forms.CheckBox();
            this.chkStreet = new System.Windows.Forms.CheckBox();
            this.chkBuilding = new System.Windows.Forms.CheckBox();
            this.chkBuildingGroup = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Enabled = false;
            this.btnShowAll.Location = new System.Drawing.Point(356, 486);
            // 
            // btnShow
            // 
            this.btnShow.Enabled = false;
            this.btnShow.Location = new System.Drawing.Point(356, 486);
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(599, 486);
            this.btnCancel.Text = "Close";
            // 
            // btnSearch
            // 
            this.btnSearch.Enabled = false;
            this.btnSearch.Location = new System.Drawing.Point(437, 486);
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 164);
            this.lvResult.Size = new System.Drawing.Size(662, 317);
            this.lvResult.Click += new System.EventHandler(this.lvResult_Click);
            this.lvResult.DoubleClick += new System.EventHandler(this.btnShow_Click);
            this.lvResult.Enter += new System.EventHandler(this.lvResult_Enter);
            // 
            // lblResult
            // 
            this.lblResult.Location = new System.Drawing.Point(151, 491);
            // 
            // lblRecord
            // 
            this.lblRecord.Location = new System.Drawing.Point(100, 491);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Enabled = false;
            this.btnSaveAs.Location = new System.Drawing.Point(518, 486);
            // 
            // txtLimit
            // 
            this.txtLimit.Location = new System.Drawing.Point(49, 488);
            // 
            // lblLimit
            // 
            this.lblLimit.Location = new System.Drawing.Point(12, 491);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "User Name :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWorkOrderNo
            // 
            this.txtWorkOrderNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWorkOrderNo.Location = new System.Drawing.Point(130, 68);
            this.txtWorkOrderNo.Name = "txtWorkOrderNo";
            this.txtWorkOrderNo.Size = new System.Drawing.Size(138, 20);
            this.txtWorkOrderNo.TabIndex = 5;
            // 
            // txtCompleteFlag
            // 
            this.txtCompleteFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCompleteFlag.Location = new System.Drawing.Point(382, 36);
            this.txtCompleteFlag.Name = "txtCompleteFlag";
            this.txtCompleteFlag.Size = new System.Drawing.Size(116, 20);
            this.txtCompleteFlag.TabIndex = 6;
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserName.Location = new System.Drawing.Point(382, 68);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(116, 20);
            this.txtUserName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Complete Flag :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnFixBugWA
            // 
            this.btnFixBugWA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnFixBugWA.Enabled = false;
            this.btnFixBugWA.Location = new System.Drawing.Point(14, 65);
            this.btnFixBugWA.Name = "btnFixBugWA";
            this.btnFixBugWA.Size = new System.Drawing.Size(135, 25);
            this.btnFixBugWA.TabIndex = 12;
            this.btnFixBugWA.Text = "Fix Bug WA";
            this.btnFixBugWA.UseVisualStyleBackColor = true;
            this.btnFixBugWA.Click += new System.EventHandler(this.btnFixBugWA_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtSegmentName, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtWorkOrderNo, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label3, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtCompleteFlag, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtUserName, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.cbWorkAreaId, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.32222F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.32222F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.35557F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(501, 95);
            this.tableLayoutPanel3.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Work Area :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Segment Name :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Work Order No :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSegmentName
            // 
            this.txtSegmentName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSegmentName.Location = new System.Drawing.Point(130, 36);
            this.txtSegmentName.Name = "txtSegmentName";
            this.txtSegmentName.Size = new System.Drawing.Size(138, 20);
            this.txtSegmentName.TabIndex = 15;
            // 
            // cbWorkAreaId
            // 
            this.cbWorkAreaId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbWorkAreaId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbWorkAreaId.FormattingEnabled = true;
            this.cbWorkAreaId.Location = new System.Drawing.Point(130, 5);
            this.cbWorkAreaId.MaxDropDownItems = 5;
            this.cbWorkAreaId.Name = "cbWorkAreaId";
            this.cbWorkAreaId.Size = new System.Drawing.Size(138, 21);
            this.cbWorkAreaId.Sorted = true;
            this.cbWorkAreaId.TabIndex = 11;
            this.cbWorkAreaId.TextChanged += new System.EventHandler(this.cbWorkAreaId_TextChanged);
            // 
            // btnCancelVerify
            // 
            this.btnCancelVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnCancelVerify.Enabled = false;
            this.btnCancelVerify.Location = new System.Drawing.Point(14, 34);
            this.btnCancelVerify.Name = "btnCancelVerify";
            this.btnCancelVerify.Size = new System.Drawing.Size(135, 25);
            this.btnCancelVerify.TabIndex = 16;
            this.btnCancelVerify.Text = "Unlock WA";
            this.btnCancelVerify.UseVisualStyleBackColor = true;
            this.btnCancelVerify.Click += new System.EventHandler(this.btnCancelVerify_Click);
            // 
            // btnStartVerify
            // 
            this.btnStartVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnStartVerify.Enabled = false;
            this.btnStartVerify.Location = new System.Drawing.Point(14, 3);
            this.btnStartVerify.Name = "btnStartVerify";
            this.btnStartVerify.Size = new System.Drawing.Size(135, 25);
            this.btnStartVerify.TabIndex = 15;
            this.btnStartVerify.Text = "Lock WA";
            this.btnStartVerify.UseVisualStyleBackColor = true;
            this.btnStartVerify.Click += new System.EventHandler(this.btnStartVerify_Click);
            // 
            // btnDisplay
            // 
            this.btnDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisplay.Enabled = false;
            this.btnDisplay.Location = new System.Drawing.Point(249, 486);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(98, 23);
            this.btnDisplay.TabIndex = 14;
            this.btnDisplay.Text = "Display Attribute";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // btnFilterADD
            // 
            this.btnFilterADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnFilterADD.AutoSize = true;
            this.btnFilterADD.Enabled = false;
            this.btnFilterADD.Location = new System.Drawing.Point(14, 96);
            this.btnFilterADD.Name = "btnFilterADD";
            this.btnFilterADD.Size = new System.Drawing.Size(135, 25);
            this.btnFilterADD.TabIndex = 18;
            this.btnFilterADD.Text = "Filter New Feature";
            this.btnFilterADD.UseVisualStyleBackColor = true;
            this.btnFilterADD.Click += new System.EventHandler(this.btnFilterADDStatus_Click);
            // 
            // btnVerifyADD
            // 
            this.btnVerifyADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnVerifyADD.AutoSize = true;
            this.btnVerifyADD.Enabled = false;
            this.btnVerifyADD.Location = new System.Drawing.Point(14, 127);
            this.btnVerifyADD.Name = "btnVerifyADD";
            this.btnVerifyADD.Size = new System.Drawing.Size(134, 26);
            this.btnVerifyADD.TabIndex = 19;
            this.btnVerifyADD.Text = "Verify New Feature";
            this.btnVerifyADD.UseVisualStyleBackColor = true;
            this.btnVerifyADD.Click += new System.EventHandler(this.btnVerifyADD_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnStartVerify, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCancelVerify, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnVerifyADD, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnFilterADD, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnFixBugWA, 0, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(507, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(163, 156);
            this.tableLayoutPanel2.TabIndex = 20;
            // 
            // chkProperty
            // 
            this.chkProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkProperty.AutoSize = true;
            this.chkProperty.Checked = true;
            this.chkProperty.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProperty.Location = new System.Drawing.Point(6, 6);
            this.chkProperty.Name = "chkProperty";
            this.chkProperty.Size = new System.Drawing.Size(145, 17);
            this.chkProperty.TabIndex = 0;
            this.chkProperty.Text = "Property";
            this.chkProperty.UseVisualStyleBackColor = true;
            // 
            // chkLandmark
            // 
            this.chkLandmark.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkLandmark.AutoSize = true;
            this.chkLandmark.Checked = true;
            this.chkLandmark.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLandmark.Location = new System.Drawing.Point(6, 32);
            this.chkLandmark.Name = "chkLandmark";
            this.chkLandmark.Size = new System.Drawing.Size(145, 18);
            this.chkLandmark.TabIndex = 1;
            this.chkLandmark.Text = "Landmark";
            this.chkLandmark.UseVisualStyleBackColor = true;
            // 
            // chkJunction
            // 
            this.chkJunction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkJunction.AutoSize = true;
            this.chkJunction.Checked = true;
            this.chkJunction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkJunction.Location = new System.Drawing.Point(314, 32);
            this.chkJunction.Name = "chkJunction";
            this.chkJunction.Size = new System.Drawing.Size(147, 18);
            this.chkJunction.TabIndex = 2;
            this.chkJunction.Text = "Junction";
            this.chkJunction.UseVisualStyleBackColor = true;
            // 
            // chkStreet
            // 
            this.chkStreet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkStreet.AutoSize = true;
            this.chkStreet.Checked = true;
            this.chkStreet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStreet.Location = new System.Drawing.Point(314, 6);
            this.chkStreet.Name = "chkStreet";
            this.chkStreet.Size = new System.Drawing.Size(147, 17);
            this.chkStreet.TabIndex = 3;
            this.chkStreet.Text = "Street";
            this.chkStreet.UseVisualStyleBackColor = true;
            // 
            // chkBuilding
            // 
            this.chkBuilding.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBuilding.AutoSize = true;
            this.chkBuilding.Checked = true;
            this.chkBuilding.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuilding.Location = new System.Drawing.Point(160, 6);
            this.chkBuilding.Name = "chkBuilding";
            this.chkBuilding.Size = new System.Drawing.Size(145, 17);
            this.chkBuilding.TabIndex = 4;
            this.chkBuilding.Text = "Building";
            this.chkBuilding.UseVisualStyleBackColor = true;
            // 
            // chkBuildingGroup
            // 
            this.chkBuildingGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBuildingGroup.AutoSize = true;
            this.chkBuildingGroup.Checked = true;
            this.chkBuildingGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuildingGroup.Location = new System.Drawing.Point(160, 32);
            this.chkBuildingGroup.Name = "chkBuildingGroup";
            this.chkBuildingGroup.Size = new System.Drawing.Size(145, 18);
            this.chkBuildingGroup.TabIndex = 5;
            this.chkBuildingGroup.Text = "Building Group";
            this.chkBuildingGroup.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.chkBuildingGroup, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.chkBuilding, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkStreet, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkJunction, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.chkLandmark, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.chkProperty, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(34, 104);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(467, 56);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // VerifyWorkArea2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 521);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.tableLayoutPanel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "VerifyWorkArea2Form";
            this.Text = "VerifyWorkArea";
            this.Controls.SetChildIndex(this.tableLayoutPanel3, 0);
            this.Controls.SetChildIndex(this.btnDisplay, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnFixBugWA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtWorkOrderNo;
        private System.Windows.Forms.TextBox txtCompleteFlag;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.ComboBox cbWorkAreaId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Button btnStartVerify;
        private System.Windows.Forms.Button btnCancelVerify;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSegmentName;
        private System.Windows.Forms.Button btnFilterADD;
        private System.Windows.Forms.Button btnVerifyADD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox chkProperty;
        private System.Windows.Forms.CheckBox chkLandmark;
        private System.Windows.Forms.CheckBox chkJunction;
        private System.Windows.Forms.CheckBox chkStreet;
        private System.Windows.Forms.CheckBox chkBuilding;
        private System.Windows.Forms.CheckBox chkBuildingGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
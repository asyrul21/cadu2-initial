﻿using System;
using System.Windows.Forms;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Edit;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms
{
    public partial class PropertyMultiANDForm : CollapsibleForm
    {
        public static string sendtextLotHse;
        public int? SelectedConstructionStatus
        {
            get { return (cbConstStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstStatus.SelectedItem).Code; }
        }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.Code; }
        }
     
        public int InsertedQuantity
        {
            get { return numOfFloors.Value; }
        }

        public int? SelectedYearInstall
        {
            get { return (cbYearInstall.SelectedItem == null) ? (int?)null : Convert.ToInt32(cbYearInstall.SelectedItem); }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public int NumIncrement
        {
            get { return numIncrement.Value; }
        }

        protected GPropertyAND _propertyAND;

        public PropertyMultiANDForm()
            : this(null)
        {
        }

        public PropertyMultiANDForm(GPropertyAND property)
        {
            InitializeComponent();
            _propertyAND = property;
        }

       //protected override void Form_Load()
       //{
       //    ComboBoxUtils.PopulateSource(cbSource);
       //    ComboBoxUtils.PopulateConstructionStatus(cbConstStatus);
       //    ComboBoxUtils.PopulatePropertyType(cbType);
       //    ComboBoxUtils.PopulateYear(cbYearInstall);
       //
       //    txtId.Text = _propertyAND.OriId.ToString();
       //    cbConstStatus.Text = _propertyAND.ConstructionStatusValue;
       //    cbType.Text = _propertyAND.TypeAbbreviationValue + " - " + _propertyAND.TypeValue;
       //    numOfFloors.Value = _propertyAND.NumberOfFloor.HasValue ? _propertyAND.NumberOfFloor.Value : 0;
       //    cbYearInstall.Text = _propertyAND.YearInstall.ToString();
       //    cbSource.Text = _propertyAND.SourceValue;
       //}

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }
        
        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedType >= 10)
            {
                numOfFloors.Value = (cbType.SelectedItem == null) ? 0 : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.DefaultFloor.Value;
            }
        }
        //
        //protected override bool ValidateValues()
        //{
        //    bool pass = true;
        //
        //    // Construction Status
        //    bool isConstructionStatusValid = PropertyValidator.CheckConstructionStatus(SelectedConstructionStatus);
        //    LabelColor(lblConstructionStatus, isConstructionStatusValid);
        //    pass &= isConstructionStatusValid;
        //
        //    // Type
        //    bool isTypeValid = PropertyValidator.CheckType(SelectedType, _propertyAND.HasBuilding());
        //    Console.WriteLine("Type validation " + isTypeValid);
        //
        //    if (SelectedType == 1 || SelectedType == 2)
        //    {
        //        isTypeValid = false;
        //        MessageBox.Show("Please select property type!", "Property Type",
        //        MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    }
        //    LabelColor(lblType, isTypeValid);
        //    pass &= isTypeValid;
        //
        //    // Year Install
        //    bool isYearValid = PropertyValidator.CheckYear(SelectedYearInstall);
        //    LabelColor(lblYearInstall, isYearValid);
        //    pass &= isYearValid;
        //    
        //    // Source
        //    bool isSourceValid = PropertyValidator.CheckSource(SelectedSource);
        //    LabelColor(lblSource, isSourceValid);
        //    pass &= isSourceValid;
        //
        //    return pass;
        //}
        //
        public virtual void SetValues()
        {
            _propertyAND.ConstructionStatus = SelectedConstructionStatus;
            _propertyAND.Type = SelectedType;
            _propertyAND.NumberOfFloor = InsertedQuantity;
            _propertyAND.Source = SelectedSource.ToString();
            _propertyAND.YearInstall = SelectedYearInstall;
        }
        
        protected bool GetGroupPropType(string GroupPropertyType)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyType> query = new Query<GPropertyType>();
            query.Obj.Code = SelectedType;
            query.AddClause(() => query.Obj.FloorType, "IN ('" + GroupPropertyType + "')");
        
            return repo.Count<GPropertyType>(query) > 0 ? true : false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}

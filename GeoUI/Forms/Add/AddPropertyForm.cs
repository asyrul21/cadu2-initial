﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Sessions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.UI.Forms.Edit;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddPropertyForm : PropertyForm
    {
        public AddPropertyForm(GProperty property)
            : base(property)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Property");

            if (Session.User.GetGroup().Name == "AND")
            {
                disablePOIEdit();
            }

            additionalGroup.IsCollapsed = true;
            relationGroup.IsCollapsed = true;
            updateGroup.IsCollapsed = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.UI.Forms.Choose;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddUserGroupForm : UserGroupForm
    {
        public AddUserGroupForm()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add User Group");
        }
    }
}

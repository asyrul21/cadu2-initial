﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddBuildingGroupForm : BuildingGroupForm
    {
        public AddBuildingGroupForm(GBuildingGroup buildingGroup)
            :base(buildingGroup)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Building Group");

            additionalGroup.IsCollapsed = true;
            relationGroup.IsCollapsed = true;
            updateGroup.IsCollapsed = true;
        }
    }
}

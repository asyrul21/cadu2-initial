﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddTollRouteForm : TollRouteForm
    {
        public AddTollRouteForm(GTollRoute tollRoute, int? junctionType)
            : base(tollRoute, junctionType)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Toll Route Form");

        }

        protected override bool ValidateValues()
        {
            bool pass = base.ValidateValues();

            if (!pass)
            {
                return false;
            }

            // check if route already exists
            RepositoryFactory repo = new RepositoryFactory();
            Query<GTollRoute> query = new Query<GTollRoute>();
            query.Obj.InSegment = SelectedInTollSegment.ToString();
            query.Obj.InID = SelectedInTollID;

            query.Obj.Type = SelectedTollType;
            if(SelectedTollType == 1)
            {
                query.Obj.OutSegment = SelectedOutTollSegment.ToString();
                query.Obj.OutID = SelectedOutTollID;
            }

            bool isOk = repo.Count(query) == 0;

            pass &= isOk;

            if(!isOk)
                throw new QualityControlException("Route already exists.");
            
            return pass;
        }
    }
}

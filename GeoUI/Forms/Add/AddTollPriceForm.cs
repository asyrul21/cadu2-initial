﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddTollPriceForm : TollPriceForm
    {
        public AddTollPriceForm(GTollPrice tollPrice)
            : base(tollPrice)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Toll Price Form");

        }

    }
}

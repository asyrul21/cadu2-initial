﻿namespace Geomatic.UI.Forms.Add
{
    partial class AddPropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pageFloor.SuspendLayout();
            this.pagePoi.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateGroup
            // 
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            // 
            // relationGroup
            // 
            this.relationGroup.Size = new System.Drawing.Size(560, 44);
            // 
            // additionalGroup
            // 
            this.additionalGroup.Size = new System.Drawing.Size(560, 197);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Size = new System.Drawing.Size(554, 178);
            // 
            // controlFloor
            // 
            this.controlFloor.MultiSelect = true;
            // 
            // pagePoi
            // 
            this.pagePoi.Size = new System.Drawing.Size(546, 152);
            // 
            // controlPoi
            // 
            this.controlPoi.Size = new System.Drawing.Size(540, 146);
            // 
            // addressGroup
            // 
            this.addressGroup.Size = new System.Drawing.Size(560, 120);
            // 
            // txtId
            // 
            this.txtId.Size = new System.Drawing.Size(560, 20);
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(560, 8);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // AddPropertyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 619);
            this.Name = "AddPropertyForm";
            this.Text = "AddPropertyForm";
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pageFloor.ResumeLayout(false);
            this.pagePoi.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddPoiForm : PoiForm
    {
        public AddPoiForm(GPoi poi)
            : base(poi)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Poi");

            additionalGroup.IsCollapsed = true;
            relationGroup.IsCollapsed = true;
            updateGroup.IsCollapsed = true;
        }
    }
}

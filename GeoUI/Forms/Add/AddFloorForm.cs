﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddFloorForm : AddressFormatForm
    {
        //public AddFloorForm(GFloor floor)
        //    : base(floor)
        //{
        //    InitializeComponent();
        //}

		public AddFloorForm()
		{
			InitializeComponent();
		}

		protected override void Form_Load()
        {
            base.Form_Load();
			RefreshFloorTitle();

			//updateGroup.IsCollapsed = true;
			//
			//// noraini ali - Display User & Date Information
			//txtCreator.Text = Geomatic.Core.Sessions.Session.User.Name;
			//txtDateCreated.Text = DateTime.Now.ToString("yyyyMMdd");
			//txtUpdater.Text = Geomatic.Core.Sessions.Session.User.Name;
			//txtDateUpdated.Text = DateTime.Now.ToString("yyyyMMdd");
			//// end
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using System.Text.RegularExpressions;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddPropertyMultipleForm : BaseForm
    {
        public enum LotHouseType
        {
            Lot,
            House
        }

        #region Properties

        public LotHouseType SelectedLotHouseType
        {
            get
            {
                return rbLot.Checked ? LotHouseType.Lot : LotHouseType.House;
            }
        }

        public int SelectedAmount { get { return numAmount.Value; } }

        public int SelectedIncrement { get { return numIncrement.Value; } }

        public int SelectedStartNo { get { return numStartNo.Value; } }

        public string SelectedPrefix { get { return txtPrefix.Text; } }

        public string SelectedPostfix { get { return txtPostfix.Text; } }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.Code; }
        }

        public int SelectedQuantity
        {
            get { return numOfFloors.Value; }
        }

        public int? SelectedConstructionStatus
        {
            get { return (cbConstStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstStatus.SelectedItem).Code; }
        }

        public int? SelectedYearInstall
        {
            get { return (cbYearInstall.SelectedItem == null) ? (int?)null : Convert.ToInt32(cbYearInstall.SelectedItem); }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        #endregion

        public AddPropertyMultipleForm()
        {
            InitializeComponent();
        }

        private void AddPropertyMultipleForm_Load(object sender, EventArgs e)
        {
            if (DesignMode) { return; }
            try
            {
                using (new WaitCursor())
                {
                    Form_Load();
                }
            }
            catch (Exception ex)
            {
                using (ErrorMessageBox box = new ErrorMessageBox())
                {
                    box.SetText(ex.Message).Show();
                }
            }
        }

        protected virtual void Form_Load()
        {
            RefreshTitle("Add Multiple Properties");

            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateConstructionStatus(cbConstStatus);
            ComboBoxUtils.PopulatePropertyType(cbType);
            ComboBoxUtils.PopulateYear(cbYearInstall);

            cbConstStatus.Text = GConstructionStatus.DEFAULT;
            cbSource.Text = GSource.DEFAULT;
            cbYearInstall.Text = DateTime.Now.Year.ToString();
        }

        private void PopulatePreview()
        {
            /*
            {Prefix}{No}{Postfix}
            {Prefix}{No}-{Postfix}
            {Prefix}-{No}{Postfix}
            {Prefix}-{No}-{Postfix}
            {Prefix}/{No}/{Postfix}
            {Prefix}-{No}/{Postfix}
            {Prefix}/{No}-{Postfix} */

            lblPreviewText.Text = string.Format("{0}{1}{2}", txtPrefix.Text, numStartNo.Value, txtPostfix.Text);
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            numOfFloors.Value = (cbType.SelectedItem == null) ? 0 : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.DefaultFloor.Value;
        }

        private void txtPrefix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtPostfix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void numStartNo_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public bool ValidateValues()
        {
            bool pass = true;

            // Construction Status
            pass &= SelectedConstructionStatus.HasValue;
            lblConStatus.ForeColor = SelectedConstructionStatus.HasValue ? Color.Black : Color.Red;

            // Property Type
            pass &= SelectedType.HasValue;
            lblType.ForeColor = SelectedType.HasValue ? Color.Black : Color.Red;

            // Year Install
            pass &= SelectedYearInstall.HasValue;
            lblYearInstall.ForeColor = SelectedYearInstall.HasValue ? Color.Black : Color.Red;

            // Source
            pass &= !string.IsNullOrEmpty(SelectedSource);
            lblSource.ForeColor = !string.IsNullOrEmpty(SelectedSource) ? Color.Black : Color.Red;

            return pass;
        }

    }
}

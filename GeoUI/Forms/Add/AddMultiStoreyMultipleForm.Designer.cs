﻿namespace Geomatic.UI.Forms.Add
{
    partial class AddMultiStoreyMultipleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNoOfFloors = new System.Windows.Forms.Label();
            this.lblStartFloor = new System.Windows.Forms.Label();
            this.lblNoOfApt = new System.Windows.Forms.Label();
            this.numStartFloor = new Geomatic.UI.Controls.IntUpDown();
            this.numOfApt = new Geomatic.UI.Controls.IntUpDown();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFormat = new System.Windows.Forms.Label();
            this.txtFloorPostfix = new System.Windows.Forms.TextBox();
            this.txtAptPrefix = new System.Windows.Forms.TextBox();
            this.txtAptPostfix = new System.Windows.Forms.TextBox();
            this.cbAptNumberFormat = new System.Windows.Forms.ComboBox();
            this.lblFloorNumFormat = new System.Windows.Forms.Label();
            this.lblAptNumberFormat = new System.Windows.Forms.Label();
            this.lblPreview = new System.Windows.Forms.Label();
            this.lblPreviewText = new System.Windows.Forms.Label();
            this.cbFloorNumFormat = new System.Windows.Forms.ComboBox();
            this.txtFloorPrefix = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfApt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 239);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch Setup";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.02703F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.97298F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel1.Controls.Add(this.lblNoOfFloors, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblStartFloor, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblNoOfApt, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.numStartFloor, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.numOfApt, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.numOfFloors, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblFormat, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtFloorPostfix, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtAptPrefix, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtAptPostfix, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbAptNumberFormat, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblFloorNumFormat, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblAptNumberFormat, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblPreview, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblPreviewText, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbFloorNumFormat, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtFloorPrefix, 2, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(559, 220);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblNoOfFloors
            // 
            this.lblNoOfFloors.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNoOfFloors.AutoSize = true;
            this.lblNoOfFloors.Location = new System.Drawing.Point(17, 8);
            this.lblNoOfFloors.Name = "lblNoOfFloors";
            this.lblNoOfFloors.Size = new System.Drawing.Size(90, 13);
            this.lblNoOfFloors.TabIndex = 4;
            this.lblNoOfFloors.Text = "Number of Floors:";
            // 
            // lblStartFloor
            // 
            this.lblStartFloor.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStartFloor.AutoSize = true;
            this.lblStartFloor.Location = new System.Drawing.Point(29, 38);
            this.lblStartFloor.Name = "lblStartFloor";
            this.lblStartFloor.Size = new System.Drawing.Size(78, 13);
            this.lblStartFloor.TabIndex = 4;
            this.lblStartFloor.Text = "Start Floor No :";
            // 
            // lblNoOfApt
            // 
            this.lblNoOfApt.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNoOfApt.AutoSize = true;
            this.lblNoOfApt.Location = new System.Drawing.Point(44, 62);
            this.lblNoOfApt.Name = "lblNoOfApt";
            this.lblNoOfApt.Size = new System.Drawing.Size(63, 26);
            this.lblNoOfApt.TabIndex = 2;
            this.lblNoOfApt.Text = "Number of Apartments:";
            // 
            // numStartFloor
            // 
            this.numStartFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numStartFloor.Location = new System.Drawing.Point(113, 35);
            this.numStartFloor.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numStartFloor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartFloor.Name = "numStartFloor";
            this.numStartFloor.Size = new System.Drawing.Size(290, 20);
            this.numStartFloor.TabIndex = 5;
            this.numStartFloor.Value = 1;
            // 
            // numOfApt
            // 
            this.numOfApt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfApt.Location = new System.Drawing.Point(113, 65);
            this.numOfApt.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numOfApt.Name = "numOfApt";
            this.numOfApt.Size = new System.Drawing.Size(290, 20);
            this.numOfApt.TabIndex = 3;
            this.numOfApt.Value = 1;
            this.numOfApt.ValueChanged += new System.EventHandler(this.numOfApt_ValueChanged);
            // 
            // numOfFloors
            // 
            this.numOfFloors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloors.Location = new System.Drawing.Point(113, 5);
            this.numOfFloors.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numOfFloors.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.Size = new System.Drawing.Size(290, 20);
            this.numOfFloors.TabIndex = 4;
            this.numOfFloors.Value = 1;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(501, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Postfix";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(427, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Prefix";
            // 
            // lblFormat
            // 
            this.lblFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFormat.AutoSize = true;
            this.lblFormat.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblFormat.Location = new System.Drawing.Point(113, 95);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(290, 17);
            this.lblFormat.TabIndex = 13;
            this.lblFormat.Text = "Format";
            this.lblFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblFormat.UseCompatibleTextRendering = true;
            // 
            // txtFloorPostfix
            // 
            this.txtFloorPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloorPostfix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloorPostfix.Location = new System.Drawing.Point(484, 121);
            this.txtFloorPostfix.Name = "txtFloorPostfix";
            this.txtFloorPostfix.Size = new System.Drawing.Size(72, 20);
            this.txtFloorPostfix.TabIndex = 17;
            this.txtFloorPostfix.TextChanged += new System.EventHandler(this.txtFloorPostfix_TextChanged);
            // 
            // txtAptPrefix
            // 
            this.txtAptPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAptPrefix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAptPrefix.Location = new System.Drawing.Point(409, 155);
            this.txtAptPrefix.Name = "txtAptPrefix";
            this.txtAptPrefix.Size = new System.Drawing.Size(69, 20);
            this.txtAptPrefix.TabIndex = 18;
            this.txtAptPrefix.TextChanged += new System.EventHandler(this.txtAptPrefix_TextChanged);
            // 
            // txtAptPostfix
            // 
            this.txtAptPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAptPostfix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAptPostfix.Location = new System.Drawing.Point(484, 155);
            this.txtAptPostfix.Name = "txtAptPostfix";
            this.txtAptPostfix.Size = new System.Drawing.Size(72, 20);
            this.txtAptPostfix.TabIndex = 19;
            this.txtAptPostfix.TextChanged += new System.EventHandler(this.txtAptPostfix_TextChanged);
            // 
            // cbAptNumberFormat
            // 
            this.cbAptNumberFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAptNumberFormat.DisplayMember = "1";
            this.cbAptNumberFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAptNumberFormat.FormattingEnabled = true;
            this.cbAptNumberFormat.Items.AddRange(new object[] {
            "1",
            "01",
            "001",
            "0001"});
            this.cbAptNumberFormat.Location = new System.Drawing.Point(113, 154);
            this.cbAptNumberFormat.Name = "cbAptNumberFormat";
            this.cbAptNumberFormat.Size = new System.Drawing.Size(290, 21);
            this.cbAptNumberFormat.TabIndex = 12;
            this.cbAptNumberFormat.ValueMember = "1";
            this.cbAptNumberFormat.SelectedIndexChanged += new System.EventHandler(this.cbAptNumberFormat_SelectedIndexChanged);
            // 
            // lblFloorNumFormat
            // 
            this.lblFloorNumFormat.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFloorNumFormat.AutoSize = true;
            this.lblFloorNumFormat.Location = new System.Drawing.Point(57, 124);
            this.lblFloorNumFormat.Name = "lblFloorNumFormat";
            this.lblFloorNumFormat.Size = new System.Drawing.Size(50, 13);
            this.lblFloorNumFormat.TabIndex = 8;
            this.lblFloorNumFormat.Text = "Floor No:";
            // 
            // lblAptNumberFormat
            // 
            this.lblAptNumberFormat.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAptNumberFormat.AutoSize = true;
            this.lblAptNumberFormat.Location = new System.Drawing.Point(32, 158);
            this.lblAptNumberFormat.Name = "lblAptNumberFormat";
            this.lblAptNumberFormat.Size = new System.Drawing.Size(75, 13);
            this.lblAptNumberFormat.TabIndex = 8;
            this.lblAptNumberFormat.Text = "Apartment No:";
            // 
            // lblPreview
            // 
            this.lblPreview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPreview.AutoSize = true;
            this.lblPreview.Location = new System.Drawing.Point(59, 193);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(48, 13);
            this.lblPreview.TabIndex = 10;
            this.lblPreview.Text = "Preview:";
            // 
            // lblPreviewText
            // 
            this.lblPreviewText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreviewText.AutoSize = true;
            this.lblPreviewText.Location = new System.Drawing.Point(113, 193);
            this.lblPreviewText.Name = "lblPreviewText";
            this.lblPreviewText.Size = new System.Drawing.Size(290, 13);
            this.lblPreviewText.TabIndex = 11;
            this.lblPreviewText.Text = "[ Preview ]";
            this.lblPreviewText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbFloorNumFormat
            // 
            this.cbFloorNumFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFloorNumFormat.DisplayMember = "1";
            this.cbFloorNumFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFloorNumFormat.FormattingEnabled = true;
            this.cbFloorNumFormat.Items.AddRange(new object[] {
            "1",
            "01",
            "001",
            "0001"});
            this.cbFloorNumFormat.Location = new System.Drawing.Point(113, 120);
            this.cbFloorNumFormat.Name = "cbFloorNumFormat";
            this.cbFloorNumFormat.Size = new System.Drawing.Size(290, 21);
            this.cbFloorNumFormat.TabIndex = 8;
            this.cbFloorNumFormat.ValueMember = "1";
            this.cbFloorNumFormat.SelectedIndexChanged += new System.EventHandler(this.cbFloorNumFormat_SelectedIndexChanged);
            // 
            // txtFloorPrefix
            // 
            this.txtFloorPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloorPrefix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloorPrefix.Location = new System.Drawing.Point(409, 121);
            this.txtFloorPrefix.Name = "txtFloorPrefix";
            this.txtFloorPrefix.Size = new System.Drawing.Size(69, 20);
            this.txtFloorPrefix.TabIndex = 16;
            this.txtFloorPrefix.TextChanged += new System.EventHandler(this.txtFloorPrefix_TextChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(502, 268);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Location = new System.Drawing.Point(421, 268);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 4;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // AddMultiStoreyMultipleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 302);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Name = "AddMultiStoreyMultipleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddMultiStoreyMultipleForm";
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfApt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Geomatic.UI.Controls.IntUpDown numOfFloors;
        private Geomatic.UI.Controls.IntUpDown numOfApt;
        private System.Windows.Forms.Label lblStartFloor;
        private System.Windows.Forms.Label lblNoOfApt;
        protected System.Windows.Forms.Label lblNoOfFloors;
        private Geomatic.UI.Controls.IntUpDown numStartFloor;
        private System.Windows.Forms.Label lblPreview;
        private System.Windows.Forms.Label lblPreviewText;
        private System.Windows.Forms.Label lblAptNumberFormat;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblFloorNumFormat;
        protected System.Windows.Forms.ComboBox cbAptNumberFormat;
        protected System.Windows.Forms.ComboBox cbFloorNumFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFormat;
        private System.Windows.Forms.TextBox txtFloorPrefix;
        private System.Windows.Forms.TextBox txtAptPrefix;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFloorPostfix;
        private System.Windows.Forms.TextBox txtAptPostfix;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;
using Geomatic.Core.Rows;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddStreetForm : StreetForm
    {
        public AddStreetForm(GStreet street)
            : base(street)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            RefreshTitle("Add Street");

            if (!Session.User.CanDo(Command.NavigationItem))
            {
                usageGroup.IsCollapsed = true;
            }
            if (Session.User.GetGroup().Name == "AND")
            {
                usageGroup.IsCollapsed = true;
                relationGroup.IsCollapsed = true;
                updateGroup.IsCollapsed = true;
                cbConstructionStatus.Enabled = false;
                cbNetworkClass.Enabled = false;
                cbFilterLevel.Enabled = false;
                cbTollType.Enabled = false;
                cbDivider.Enabled = false;
                cbNaviStatus.Enabled = false;
                cbCategory.Enabled = false;
                cbClass.Enabled = false;
                cbDesign.Enabled = false;
                cbDirection.Enabled = false;
            }
        }
    }
}

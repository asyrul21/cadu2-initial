﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddMultiStoreyForm : MultiStoreyForm
    {
        public AddMultiStoreyForm(GMultiStorey multiStorey)
            : base(multiStorey)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add MultiStorey Address");

            updateGroup.IsCollapsed = false;
        }
    }
}

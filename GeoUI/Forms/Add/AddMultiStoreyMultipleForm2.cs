﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddMultiStoreyMultipleForm2 : Form
    {
        #region properties
        public int SelectedNumOfFloors
        {
            get { return numOfFloors.Value; }
        }
        public int SelectedNumOfApt
        {
            get { return numOfApts.Value; }
        }
        public int SelectedStartFloor
        {
            get { return numStartFloor.Value; }
        }
        public int SelectedStartApt
        {
            get { return numStartApt.Value; }
        }
        public string SelectedBlock
        {
            get { return txtBlock.Text; }
        }
        public string Prefix1
        {
            get { return txtPrefix1.Text; }
        }
        public string Prefix2
        {
            get { return txtPrefix2.Text; }
        }
        public string Prefix3
        {
            get { return txtPrefix3.Text; }
        }
        public string Prefix4
        {
            get { return txtPrefix4.Text; }
        }
        public string SelectedFormat1
        {
            get { return cbFormat1.Text; }
        }
        public string SelectedFormat2
        {
            get { return cbFormat2.Text; }
        }
        public string SelectedFormat3
        {
            get { return cbFormat3.Text; }
        }
        public string SelectedTextFloorNum
        {
            get { return txtFloorsNum.Text; }
        }
        public bool ChkAptChar1
        {
            get { return chkUnitChar1.Checked; }
        }
        public bool ChkAptChar2
        {
            get { return chkUnitChar2.Checked; }
        }
        public bool ChkAptChar3
        {
            get { return chkUnitChar3.Checked; }
        }
        public bool ChkFlrDigit
        {
            get { return chkFlrDigit.Checked; }
        }
        public bool ChkUnitDigit
        {
            get { return chkUnitDigit.Checked; }
        }
        #endregion

        public AddMultiStoreyMultipleForm2()
        {
            InitializeComponent();
        }

        public void AddMultiStoreyMultipleForm2_Load()
        {
            if (DesignMode) { return; }
            try
            {
                using (new WaitCursor())
                {
                    Form_Load();
                }
            }
            catch (Exception ex)
            {
                using (ErrorMessageBox box = new ErrorMessageBox())
                {
                    box.SetText(ex.Message).Show();
                }
            }
        }

        protected virtual void Form_Load()
        {
            RefreshTitle("Add Multiple Storey");
            chkUnitChar1.Enabled = false;
            chkUnitChar2.Enabled = false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }

        private void txtPrefix1_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbFormat1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtPrefix2_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void cbFormat2_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtPrefix3_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void cbFormat3_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private bool chkPrefixWithAlphabet(string Prefix)
        {
            Regex rgx = new Regex("[^A-Z]");
            string strPrefix = Prefix;
            strPrefix = rgx.Replace(strPrefix, "");
            if (!string.IsNullOrEmpty(strPrefix))
                return true;

            return false;
        }

        private void txtPrefix4_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtBlock_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numStartApt_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numStartFloor_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numOfFloors_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numOfApts_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        public virtual bool ValidateValues()
        {
            bool pass = true;

            bool isFloorValid = true;
            isFloorValid &= SelectedNumOfFloors.ToString() != "";
            pass &= isFloorValid;

            bool isAptValid = true;
            isAptValid &= SelectedNumOfApt.ToString() != "";
            pass &= isAptValid;

            bool isAptCharValid1 = true;
            if (ChkAptChar1)
            {
                isAptValid &= SelectedNumOfApt < 26;
                pass &= isAptCharValid1; 
            }

            bool isAptCharValid2 = true;
            if (ChkAptChar2)
            {
                isAptValid &= SelectedNumOfApt < 26;
                pass &= isAptCharValid2;
            }

            bool isAptCharValid3 = true;
            if (ChkAptChar3)
            {
                isAptCharValid3 &= SelectedNumOfApt < 26;
                pass &= isAptCharValid3;
            }

            lblNumOfFloors.ForeColor = isFloorValid ? Color.Black : Color.Red;
            lblNumOfApts.ForeColor = isAptValid ? Color.Black : Color.Red;

            return pass;
        }

        private void btnApply_Click_1(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                  DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void PopulatePreview()
        {
            RefreshEnableChkCharBox();
            InitialPrefix();

            string preview = "";

            string strFormat1 = "";
            string strFormat2 = "";
            string strFormat3 = "";
            string strFormat4 = "";

            // display floor preview
            previewfloor();

            // Display Apartment preview
            // String Format 1
            strFormat1 = string.Format("{0}{1}", Prefix1, strFormat1);
            if (cbFormat1.Text == "BLOCK")
            {
                strFormat1 = string.Format("{0}{1}", strFormat1, SelectedBlock);
            }
            else if (cbFormat1.Text == "FLR")
            {
                string floorNumString = FloorNumStringDigit();
                strFormat1 = string.Format("{0}{1}", strFormat1, floorNumString);
            }
            else if (cbFormat1.Text == "UNIT")
            {
                // generate digit format 
                string UnitNumString = UnitNumStringDigit();

                // default tick checkbox Generate A->Z 
                // untick is maintain value as prefix
                if (ChkAptChar1)
                {
                    if (chkPrefixWithAlphabet(Prefix1))
                    {
                        strFormat1 = string.Format("{0}", strFormat1);
                    }
                    else
                    {
                        strFormat1 = string.Format("{0}{1}", strFormat1, UnitNumString);
                    }
                }
                else
                {
                    strFormat1 = string.Format("{0}{1}", strFormat1, UnitNumString);
                }
            }
            preview = string.Format("{0}", strFormat1);

            // String Format 2
            strFormat2 = string.Format("{0}{1}", Prefix2, strFormat2);
            if (cbFormat2.Text == "BLOCK")
            {
                strFormat2 = string.Format("{0}{1}", strFormat2, SelectedBlock);
            }
            else if (cbFormat2.Text == "FLR")
            {
                string floorNumString = FloorNumStringDigit();
                strFormat2 = string.Format("{0}{1}", strFormat2, floorNumString);
            }
            else if (cbFormat2.Text == "UNIT")
            {
                string UnitNumString = UnitNumStringDigit();

                // default tick checkbox Generate A->Z 
                // untick is maintain value as prefix
                if (ChkAptChar2)
                {
                    if (chkPrefixWithAlphabet(Prefix2))
                    {
                        strFormat2 = string.Format("{0}", strFormat2);
                    }
                    else
                    {
                        strFormat2 = string.Format("{0}{1}", strFormat2, UnitNumString);
                    }
                }
                else
                {
                    strFormat2 = string.Format("{0}{1}", strFormat2, UnitNumString);
                }
            }
            preview = string.Format("{0} {1}", strFormat1, strFormat2);

            // String Format 3
            strFormat3 = string.Format("{0}{1}", Prefix3, strFormat3);
            if (cbFormat3.Text == "BLOCK")
            {
                strFormat3 = string.Format("{0}{1}", strFormat3, SelectedBlock);
            }
            else if (cbFormat3.Text == "FLR")
            {
                string floorNumString = FloorNumStringDigit();
                strFormat3 = string.Format("{0}{1}", strFormat3, floorNumString);
            }
            else if (cbFormat3.Text == "UNIT")
            {
                string UnitNumString = UnitNumStringDigit();

                // default tick checkbox Generate A->Z 
                // untick is maintain value as prefix
                if (ChkAptChar3)
                {
                    if (chkPrefixWithAlphabet(Prefix3))
                    {
                        strFormat3 = string.Format("{0}", strFormat3);
                    }
                    else
                    {
                        strFormat3 = string.Format("{0}{1}", strFormat3, UnitNumString);
                    }
                }
                else
                {
                    strFormat3 = string.Format("{0}{1}", strFormat3, UnitNumString);
                }
            }
            preview = string.Format("{0} {1} {2}", strFormat1, strFormat2, strFormat3);

            strFormat4 = string.Format("{0}{1}", strFormat4, Prefix4);
            preview = string.Format("{0} {1} {2} {3}", strFormat1, strFormat2, strFormat3, strFormat4);

            lblPreviewText.Text = preview;
        }

        // default tick checkbox is genereate the Single digit for floor/unit -> 1,2..10..99..100
        // untick to generate floor/unit by total num digit -> if total num digit 3 like 100 -> 001,002..010..099..100
        private string FloorNumStringDigit()
        {
            string NumString = "";
            NumString = SelectedStartFloor.ToString();
            int totalfloor = SelectedNumOfFloors + SelectedStartFloor - 1;

            if (!chkFlrDigit.Checked)
            {
                int DigitLen = totalfloor.ToString().Length;
                switch (DigitLen)
                {
                    default:
                    case 1: break;
                    case 2: NumString = SelectedStartFloor.ToString("00"); break;
                    case 3: NumString = SelectedStartFloor.ToString("000"); break;
                    case 4: NumString = SelectedStartFloor.ToString("0000"); break;
                }
            }
            return NumString;
        }

        private string UnitNumStringDigit()
        {
            string NumString = "";
            NumString = SelectedStartApt.ToString();
            int TotalUnit = SelectedNumOfApt + SelectedStartApt - 1;

            if (!chkUnitDigit.Checked)
            {
                int DigitLen = TotalUnit.ToString().Length;
                switch (DigitLen)
                {
                    default:
                    case 1: break;
                    case 2: NumString = SelectedStartApt.ToString("00"); break;
                    case 3: NumString = SelectedStartApt.ToString("000"); break;
                    case 4: NumString = SelectedStartApt.ToString("0000"); break;
                }
            }
            return NumString;
        }

        private void previewfloor()
        {
            lblFlrNoPreview.Text = numStartFloor.Value.ToString();
            if (!string.IsNullOrEmpty(txtFloorsNum.Text))
            {
                lblFlrNoPreview.Text = txtFloorsNum.Text;
            } 
        }

        private void InitialPrefix()
        {
            if (string.IsNullOrEmpty(cbFormat1.Text))
            {
                txtPrefix1.Text = "";
            }
            if (string.IsNullOrEmpty(cbFormat2.Text))
            {
                txtPrefix2.Text = "";
            }
            if (string.IsNullOrEmpty(cbFormat3.Text))
            {
                txtPrefix3.Text = "";
            }
        }

        private void chkUnitChar1_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitChar2_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitChar3_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkFlrDigit_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitDigit_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void RefreshEnableChkCharBox()
        {
            chkUnitChar1.Enabled = false;
            chkUnitChar2.Enabled = false;
            chkUnitChar3.Enabled = false;

            if (cbFormat1.Text == "UNIT")
                chkUnitChar1.Enabled = true;

            if (cbFormat2.Text == "UNIT")
                chkUnitChar2.Enabled = true;

            if (cbFormat3.Text == "UNIT")
                chkUnitChar3.Enabled = true;
        }

        private void txtFloorsNum_TextChanged(object sender, EventArgs e)
        {
            // display floor preview
            previewfloor();
        }
    }
}

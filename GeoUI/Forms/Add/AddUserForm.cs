﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddUserForm : UserForm
    {
        public AddUserForm()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add User");
        }

        protected override bool ValidateValues()
        {
            bool pass = base.ValidateValues();

            if (!pass)
            {
                return false;
            }

            // check if name already exists
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUser> query = new Query<GUser>();
            query.Obj.Name = txtUsername.Text;

            bool isOk = repo.Count(query) == 0;

            lblUsername.ForeColor = isOk ? Color.Black : Color.Red;

            pass &= isOk;

            return pass;
        }
    }
}

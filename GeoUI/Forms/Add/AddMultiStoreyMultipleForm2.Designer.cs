﻿namespace Geomatic.UI.Forms.Add
{
    partial class AddMultiStoreyMultipleForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMultiStoreyMultipleForm2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNumOfFloors = new System.Windows.Forms.Label();
            this.lblNumOfApts = new System.Windows.Forms.Label();
            this.lblFloorStartNo = new System.Windows.Forms.Label();
            this.lblAptStartNo = new System.Windows.Forms.Label();
            this.lblBlock = new System.Windows.Forms.Label();
            this.lblFloorsNum = new System.Windows.Forms.Label();
            this.txtBlock = new System.Windows.Forms.TextBox();
            this.txtFloorsNum = new System.Windows.Forms.TextBox();
            this.chkFlrDigit = new System.Windows.Forms.CheckBox();
            this.chkUnitDigit = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPrefix1 = new System.Windows.Forms.Label();
            this.lblFomat1 = new System.Windows.Forms.Label();
            this.lblPrefix2 = new System.Windows.Forms.Label();
            this.lblFormat2 = new System.Windows.Forms.Label();
            this.lblPrefix3 = new System.Windows.Forms.Label();
            this.lblFormat3 = new System.Windows.Forms.Label();
            this.lblPrefix4 = new System.Windows.Forms.Label();
            this.txtPrefix1 = new System.Windows.Forms.TextBox();
            this.cbFormat1 = new System.Windows.Forms.ComboBox();
            this.cbFormat2 = new System.Windows.Forms.ComboBox();
            this.txtPrefix2 = new System.Windows.Forms.TextBox();
            this.txtPrefix3 = new System.Windows.Forms.TextBox();
            this.cbFormat3 = new System.Windows.Forms.ComboBox();
            this.txtPrefix4 = new System.Windows.Forms.TextBox();
            this.chkUnitChar1 = new System.Windows.Forms.CheckBox();
            this.chkUnitChar2 = new System.Windows.Forms.CheckBox();
            this.chkUnitChar3 = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPreviewText = new System.Windows.Forms.Label();
            this.lblFlrNoPreview = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.numOfApts = new Geomatic.UI.Controls.IntUpDown();
            this.numStartFloor = new Geomatic.UI.Controls.IntUpDown();
            this.numStartApt = new Geomatic.UI.Controls.IntUpDown();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfApts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartApt)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.lblNumOfFloors, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numOfFloors, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.numOfApts, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblNumOfApts, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.numStartFloor, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.numStartApt, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblFloorStartNo, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAptStartNo, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblBlock, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblFloorsNum, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtBlock, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtFloorsNum, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.chkFlrDigit, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkUnitDigit, 5, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // lblNumOfFloors
            // 
            resources.ApplyResources(this.lblNumOfFloors, "lblNumOfFloors");
            this.lblNumOfFloors.Name = "lblNumOfFloors";
            // 
            // lblNumOfApts
            // 
            resources.ApplyResources(this.lblNumOfApts, "lblNumOfApts");
            this.lblNumOfApts.Name = "lblNumOfApts";
            // 
            // lblFloorStartNo
            // 
            resources.ApplyResources(this.lblFloorStartNo, "lblFloorStartNo");
            this.lblFloorStartNo.Name = "lblFloorStartNo";
            // 
            // lblAptStartNo
            // 
            resources.ApplyResources(this.lblAptStartNo, "lblAptStartNo");
            this.lblAptStartNo.Name = "lblAptStartNo";
            // 
            // lblBlock
            // 
            resources.ApplyResources(this.lblBlock, "lblBlock");
            this.lblBlock.Name = "lblBlock";
            // 
            // lblFloorsNum
            // 
            resources.ApplyResources(this.lblFloorsNum, "lblFloorsNum");
            this.lblFloorsNum.Name = "lblFloorsNum";
            // 
            // txtBlock
            // 
            resources.ApplyResources(this.txtBlock, "txtBlock");
            this.txtBlock.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBlock.Name = "txtBlock";
            this.txtBlock.TextChanged += new System.EventHandler(this.txtBlock_TextChanged);
            // 
            // txtFloorsNum
            // 
            resources.ApplyResources(this.txtFloorsNum, "txtFloorsNum");
            this.txtFloorsNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloorsNum.Name = "txtFloorsNum";
            this.txtFloorsNum.TextChanged += new System.EventHandler(this.txtFloorsNum_TextChanged);
            // 
            // chkFlrDigit
            // 
            resources.ApplyResources(this.chkFlrDigit, "chkFlrDigit");
            this.chkFlrDigit.Checked = true;
            this.chkFlrDigit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFlrDigit.Name = "chkFlrDigit";
            this.chkFlrDigit.UseVisualStyleBackColor = true;
            this.chkFlrDigit.CheckedChanged += new System.EventHandler(this.chkFlrDigit_CheckedChanged);
            // 
            // chkUnitDigit
            // 
            resources.ApplyResources(this.chkUnitDigit, "chkUnitDigit");
            this.chkUnitDigit.Checked = true;
            this.chkUnitDigit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUnitDigit.Name = "chkUnitDigit";
            this.chkUnitDigit.UseVisualStyleBackColor = true;
            this.chkUnitDigit.CheckedChanged += new System.EventHandler(this.chkUnitDigit_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.lblPrefix1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFomat1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPrefix2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFormat2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPrefix3, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFormat3, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPrefix4, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtPrefix1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbFormat1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbFormat2, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtPrefix2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtPrefix3, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbFormat3, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtPrefix4, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.chkUnitChar1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.chkUnitChar2, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.chkUnitChar3, 5, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // lblPrefix1
            // 
            resources.ApplyResources(this.lblPrefix1, "lblPrefix1");
            this.lblPrefix1.Name = "lblPrefix1";
            // 
            // lblFomat1
            // 
            resources.ApplyResources(this.lblFomat1, "lblFomat1");
            this.lblFomat1.Name = "lblFomat1";
            // 
            // lblPrefix2
            // 
            resources.ApplyResources(this.lblPrefix2, "lblPrefix2");
            this.lblPrefix2.Name = "lblPrefix2";
            // 
            // lblFormat2
            // 
            resources.ApplyResources(this.lblFormat2, "lblFormat2");
            this.lblFormat2.Name = "lblFormat2";
            // 
            // lblPrefix3
            // 
            resources.ApplyResources(this.lblPrefix3, "lblPrefix3");
            this.lblPrefix3.Name = "lblPrefix3";
            // 
            // lblFormat3
            // 
            resources.ApplyResources(this.lblFormat3, "lblFormat3");
            this.lblFormat3.Name = "lblFormat3";
            // 
            // lblPrefix4
            // 
            resources.ApplyResources(this.lblPrefix4, "lblPrefix4");
            this.lblPrefix4.Name = "lblPrefix4";
            // 
            // txtPrefix1
            // 
            this.txtPrefix1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtPrefix1, "txtPrefix1");
            this.txtPrefix1.Name = "txtPrefix1";
            this.txtPrefix1.TextChanged += new System.EventHandler(this.txtPrefix1_TextChanged);
            // 
            // cbFormat1
            // 
            this.cbFormat1.FormattingEnabled = true;
            this.cbFormat1.Items.AddRange(new object[] {
            resources.GetString("cbFormat1.Items"),
            resources.GetString("cbFormat1.Items1"),
            resources.GetString("cbFormat1.Items2"),
            resources.GetString("cbFormat1.Items3")});
            resources.ApplyResources(this.cbFormat1, "cbFormat1");
            this.cbFormat1.Name = "cbFormat1";
            this.cbFormat1.SelectedIndexChanged += new System.EventHandler(this.cbFormat1_SelectedIndexChanged);
            // 
            // cbFormat2
            // 
            this.cbFormat2.FormattingEnabled = true;
            this.cbFormat2.Items.AddRange(new object[] {
            resources.GetString("cbFormat2.Items"),
            resources.GetString("cbFormat2.Items1"),
            resources.GetString("cbFormat2.Items2"),
            resources.GetString("cbFormat2.Items3")});
            resources.ApplyResources(this.cbFormat2, "cbFormat2");
            this.cbFormat2.Name = "cbFormat2";
            this.cbFormat2.SelectedIndexChanged += new System.EventHandler(this.cbFormat2_SelectedIndexChanged);
            // 
            // txtPrefix2
            // 
            this.txtPrefix2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtPrefix2, "txtPrefix2");
            this.txtPrefix2.Name = "txtPrefix2";
            this.txtPrefix2.TextChanged += new System.EventHandler(this.txtPrefix2_TextChanged);
            // 
            // txtPrefix3
            // 
            this.txtPrefix3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtPrefix3, "txtPrefix3");
            this.txtPrefix3.Name = "txtPrefix3";
            this.txtPrefix3.TextChanged += new System.EventHandler(this.txtPrefix3_TextChanged);
            // 
            // cbFormat3
            // 
            this.cbFormat3.FormattingEnabled = true;
            this.cbFormat3.Items.AddRange(new object[] {
            resources.GetString("cbFormat3.Items"),
            resources.GetString("cbFormat3.Items1"),
            resources.GetString("cbFormat3.Items2"),
            resources.GetString("cbFormat3.Items3")});
            resources.ApplyResources(this.cbFormat3, "cbFormat3");
            this.cbFormat3.Name = "cbFormat3";
            this.cbFormat3.SelectedIndexChanged += new System.EventHandler(this.cbFormat3_SelectedIndexChanged);
            // 
            // txtPrefix4
            // 
            this.txtPrefix4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            resources.ApplyResources(this.txtPrefix4, "txtPrefix4");
            this.txtPrefix4.Name = "txtPrefix4";
            this.txtPrefix4.TextChanged += new System.EventHandler(this.txtPrefix4_TextChanged);
            // 
            // chkUnitChar1
            // 
            resources.ApplyResources(this.chkUnitChar1, "chkUnitChar1");
            this.chkUnitChar1.Name = "chkUnitChar1";
            this.chkUnitChar1.UseVisualStyleBackColor = true;
            this.chkUnitChar1.CheckedChanged += new System.EventHandler(this.chkUnitChar1_CheckedChanged);
            // 
            // chkUnitChar2
            // 
            resources.ApplyResources(this.chkUnitChar2, "chkUnitChar2");
            this.chkUnitChar2.Name = "chkUnitChar2";
            this.chkUnitChar2.UseVisualStyleBackColor = true;
            this.chkUnitChar2.CheckedChanged += new System.EventHandler(this.chkUnitChar2_CheckedChanged);
            // 
            // chkUnitChar3
            // 
            resources.ApplyResources(this.chkUnitChar3, "chkUnitChar3");
            this.chkUnitChar3.Name = "chkUnitChar3";
            this.chkUnitChar3.UseVisualStyleBackColor = true;
            this.chkUnitChar3.CheckedChanged += new System.EventHandler(this.chkUnitChar3_CheckedChanged);
            // 
            // btnApply
            // 
            resources.ApplyResources(this.btnApply, "btnApply");
            this.btnApply.Name = "btnApply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click_1);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // lblPreviewText
            // 
            resources.ApplyResources(this.lblPreviewText, "lblPreviewText");
            this.lblPreviewText.Name = "lblPreviewText";
            // 
            // lblFlrNoPreview
            // 
            resources.ApplyResources(this.lblFlrNoPreview, "lblFlrNoPreview");
            this.lblFlrNoPreview.Name = "lblFlrNoPreview";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel3);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.lblFlrNoPreview, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblPreviewText, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // numOfFloors
            // 
            resources.ApplyResources(this.numOfFloors, "numOfFloors");
            this.numOfFloors.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.Value = 1;
            this.numOfFloors.ValueChanged += new System.EventHandler(this.numOfFloors_ValueChanged);
            // 
            // numOfApts
            // 
            resources.ApplyResources(this.numOfApts, "numOfApts");
            this.numOfApts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfApts.Name = "numOfApts";
            this.numOfApts.Value = 1;
            this.numOfApts.ValueChanged += new System.EventHandler(this.numOfApts_ValueChanged);
            // 
            // numStartFloor
            // 
            resources.ApplyResources(this.numStartFloor, "numStartFloor");
            this.numStartFloor.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numStartFloor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartFloor.Name = "numStartFloor";
            this.numStartFloor.Value = 1;
            this.numStartFloor.ValueChanged += new System.EventHandler(this.numStartFloor_ValueChanged);
            // 
            // numStartApt
            // 
            resources.ApplyResources(this.numStartApt, "numStartApt");
            this.numStartApt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartApt.Name = "numStartApt";
            this.numStartApt.Value = 1;
            this.numStartApt.ValueChanged += new System.EventHandler(this.numStartApt_ValueChanged);
            // 
            // AddMultiStoreyMultipleForm2
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddMultiStoreyMultipleForm2";
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfApts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartApt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblNumOfFloors;
        private System.Windows.Forms.Label lblNumOfApts;
        private System.Windows.Forms.Label lblFloorStartNo;
        private System.Windows.Forms.Label lblAptStartNo;
        private System.Windows.Forms.Label lblBlock;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Controls.IntUpDown numOfFloors;
        private Controls.IntUpDown numOfApts;
        private Controls.IntUpDown numStartFloor;
        private Controls.IntUpDown numStartApt;
        private System.Windows.Forms.TextBox txtBlock;
        private System.Windows.Forms.Label lblPrefix1;
        private System.Windows.Forms.Label lblFomat1;
        private System.Windows.Forms.Label lblPrefix2;
        private System.Windows.Forms.Label lblFormat2;
        private System.Windows.Forms.Label lblPrefix3;
        private System.Windows.Forms.Label lblFormat3;
        private System.Windows.Forms.Label lblPrefix4;
        private System.Windows.Forms.TextBox txtPrefix1;
        private System.Windows.Forms.ComboBox cbFormat1;
        private System.Windows.Forms.ComboBox cbFormat2;
        private System.Windows.Forms.TextBox txtPrefix2;
        private System.Windows.Forms.TextBox txtPrefix3;
        private System.Windows.Forms.ComboBox cbFormat3;
        private System.Windows.Forms.TextBox txtPrefix4;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblFloorsNum;
        private System.Windows.Forms.TextBox txtFloorsNum;
        private System.Windows.Forms.Label lblPreviewText;
        private System.Windows.Forms.CheckBox chkUnitChar3;
        private System.Windows.Forms.CheckBox chkUnitChar2;
        private System.Windows.Forms.CheckBox chkUnitChar1;
        private System.Windows.Forms.CheckBox chkFlrDigit;
        private System.Windows.Forms.CheckBox chkUnitDigit;
        private System.Windows.Forms.Label lblFlrNoPreview;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    }
}
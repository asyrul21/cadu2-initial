﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddBuildingForm : BuildingForm
    {
        public AddBuildingForm(GBuilding building)
            : base(building)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Add Building");

            if (Session.User.GetGroup().Name == "AND")
            {
                disablePOIEdit();
            }

            additionalGroup.IsCollapsed = true;
            relationGroup.IsCollapsed = true;
            updateGroup.IsCollapsed = true;
        }
    }
}

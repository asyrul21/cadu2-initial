﻿namespace Geomatic.UI.Forms.Add
{
    partial class AddPropertyMultipleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLotHouseType = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbLot = new System.Windows.Forms.RadioButton();
            this.rbHouse = new System.Windows.Forms.RadioButton();
            this.lblIncrement = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.lblPrefix = new System.Windows.Forms.Label();
            this.lblPostFix = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblPreview = new System.Windows.Forms.Label();
            this.numIncrement = new Geomatic.UI.Controls.IntUpDown();
            this.numStartNo = new Geomatic.UI.Controls.IntUpDown();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.txtPostfix = new System.Windows.Forms.TextBox();
            this.lblPreviewText = new System.Windows.Forms.Label();
            this.numAmount = new Geomatic.UI.Controls.IntUpDown();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.lblType = new System.Windows.Forms.Label();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbYearInstall = new System.Windows.Forms.ComboBox();
            this.lblYearInstall = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.cbConstStatus = new System.Windows.Forms.ComboBox();
            this.lblConStatus = new System.Windows.Forms.Label();
            this.lblNumOfFloors = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAmount)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 185);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 223);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch Setup";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.66234F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.33766F));
            this.tableLayoutPanel1.Controls.Add(this.lblLotHouseType, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblIncrement, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblStart, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPrefix, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblPostFix, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblAmount, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblPreview, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.numIncrement, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.numStartNo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtPrefix, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPostfix, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblPreviewText, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.numAmount, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(251, 204);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblLotHouseType
            // 
            this.lblLotHouseType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLotHouseType.AutoSize = true;
            this.lblLotHouseType.Location = new System.Drawing.Point(57, 23);
            this.lblLotHouseType.Name = "lblLotHouseType";
            this.lblLotHouseType.Size = new System.Drawing.Size(34, 13);
            this.lblLotHouseType.TabIndex = 0;
            this.lblLotHouseType.Text = "Type:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rbLot, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbHouse, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(97, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(151, 54);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // rbLot
            // 
            this.rbLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rbLot.AutoSize = true;
            this.rbLot.Checked = true;
            this.rbLot.Location = new System.Drawing.Point(3, 5);
            this.rbLot.Name = "rbLot";
            this.rbLot.Size = new System.Drawing.Size(145, 17);
            this.rbLot.TabIndex = 0;
            this.rbLot.TabStop = true;
            this.rbLot.Text = "Lot";
            this.rbLot.UseVisualStyleBackColor = true;
            // 
            // rbHouse
            // 
            this.rbHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rbHouse.AutoSize = true;
            this.rbHouse.Location = new System.Drawing.Point(3, 32);
            this.rbHouse.Name = "rbHouse";
            this.rbHouse.Size = new System.Drawing.Size(145, 17);
            this.rbHouse.TabIndex = 1;
            this.rbHouse.Text = "House";
            this.rbHouse.UseVisualStyleBackColor = true;
            // 
            // lblIncrement
            // 
            this.lblIncrement.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblIncrement.AutoSize = true;
            this.lblIncrement.Location = new System.Drawing.Point(34, 67);
            this.lblIncrement.Name = "lblIncrement";
            this.lblIncrement.Size = new System.Drawing.Size(57, 13);
            this.lblIncrement.TabIndex = 3;
            this.lblIncrement.Text = "Increment:";
            // 
            // lblStart
            // 
            this.lblStart.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(59, 95);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(32, 13);
            this.lblStart.TabIndex = 5;
            this.lblStart.Text = "Start:";
            // 
            // lblPrefix
            // 
            this.lblPrefix.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPrefix.AutoSize = true;
            this.lblPrefix.Location = new System.Drawing.Point(55, 123);
            this.lblPrefix.Name = "lblPrefix";
            this.lblPrefix.Size = new System.Drawing.Size(36, 13);
            this.lblPrefix.TabIndex = 7;
            this.lblPrefix.Text = "Prefix:";
            // 
            // lblPostFix
            // 
            this.lblPostFix.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostFix.AutoSize = true;
            this.lblPostFix.Location = new System.Drawing.Point(50, 151);
            this.lblPostFix.Name = "lblPostFix";
            this.lblPostFix.Size = new System.Drawing.Size(41, 13);
            this.lblPostFix.TabIndex = 9;
            this.lblPostFix.Text = "Postfix:";
            // 
            // lblAmount
            // 
            this.lblAmount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(45, 207);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(46, 13);
            this.lblAmount.TabIndex = 1;
            this.lblAmount.Text = "Amount:";
            // 
            // lblPreview
            // 
            this.lblPreview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPreview.AutoSize = true;
            this.lblPreview.Location = new System.Drawing.Point(43, 179);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(48, 13);
            this.lblPreview.TabIndex = 11;
            this.lblPreview.Text = "Preview:";
            // 
            // numIncrement
            // 
            this.numIncrement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numIncrement.Location = new System.Drawing.Point(97, 64);
            this.numIncrement.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numIncrement.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numIncrement.Name = "numIncrement";
            this.numIncrement.Size = new System.Drawing.Size(151, 20);
            this.numIncrement.TabIndex = 4;
            this.numIncrement.Value = 1;
            // 
            // numStartNo
            // 
            this.numStartNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numStartNo.Location = new System.Drawing.Point(97, 92);
            this.numStartNo.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numStartNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartNo.Name = "numStartNo";
            this.numStartNo.Size = new System.Drawing.Size(151, 20);
            this.numStartNo.TabIndex = 6;
            this.numStartNo.Value = 1;
            // 
            // txtPrefix
            // 
            this.txtPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrefix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrefix.Location = new System.Drawing.Point(97, 120);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(151, 20);
            this.txtPrefix.TabIndex = 8;
            this.txtPrefix.TextChanged += new System.EventHandler(this.txtPrefix_TextChanged);
            // 
            // txtPostfix
            // 
            this.txtPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostfix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostfix.Location = new System.Drawing.Point(97, 148);
            this.txtPostfix.Name = "txtPostfix";
            this.txtPostfix.Size = new System.Drawing.Size(151, 20);
            this.txtPostfix.TabIndex = 10;
            this.txtPostfix.TextChanged += new System.EventHandler(this.txtPostfix_TextChanged);
            // 
            // lblPreviewText
            // 
            this.lblPreviewText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreviewText.AutoSize = true;
            this.lblPreviewText.Location = new System.Drawing.Point(97, 179);
            this.lblPreviewText.Name = "lblPreviewText";
            this.lblPreviewText.Size = new System.Drawing.Size(151, 13);
            this.lblPreviewText.TabIndex = 12;
            this.lblPreviewText.Text = "[ Preview ]";
            this.lblPreviewText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numAmount
            // 
            this.numAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numAmount.Location = new System.Drawing.Point(97, 204);
            this.numAmount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numAmount.Name = "numAmount";
            this.numAmount.Size = new System.Drawing.Size(151, 20);
            this.numAmount.TabIndex = 2;
            this.numAmount.Value = 2;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Location = new System.Drawing.Point(110, 410);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(191, 410);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 167);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Common Attributes";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.36364F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.63636F));
            this.tableLayoutPanel2.Controls.Add(this.numOfFloors, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblType, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbType, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbYearInstall, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblYearInstall, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblSource, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.cbSource, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.cbConstStatus, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblConStatus, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblNumOfFloors, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(251, 148);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // numOfFloors
            // 
            this.numOfFloors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloors.Location = new System.Drawing.Point(94, 65);
            this.numOfFloors.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.ReadOnly = true;
            this.numOfFloors.Size = new System.Drawing.Size(154, 20);
            this.numOfFloors.TabIndex = 4;
            this.numOfFloors.Value = 1;
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(54, 38);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type:";
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(94, 34);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(154, 21);
            this.cbType.TabIndex = 3;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // cbYearInstall
            // 
            this.cbYearInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbYearInstall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYearInstall.FormattingEnabled = true;
            this.cbYearInstall.Location = new System.Drawing.Point(94, 94);
            this.cbYearInstall.Name = "cbYearInstall";
            this.cbYearInstall.Size = new System.Drawing.Size(154, 21);
            this.cbYearInstall.TabIndex = 7;
            // 
            // lblYearInstall
            // 
            this.lblYearInstall.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblYearInstall.AutoSize = true;
            this.lblYearInstall.Location = new System.Drawing.Point(26, 98);
            this.lblYearInstall.Name = "lblYearInstall";
            this.lblYearInstall.Size = new System.Drawing.Size(62, 13);
            this.lblYearInstall.TabIndex = 6;
            this.lblYearInstall.Text = "Year Install:";
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(44, 128);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 8;
            this.lblSource.Text = "Source:";
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(94, 124);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(154, 21);
            this.cbSource.TabIndex = 9;
            // 
            // cbConstStatus
            // 
            this.cbConstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConstStatus.FormattingEnabled = true;
            this.cbConstStatus.Location = new System.Drawing.Point(94, 4);
            this.cbConstStatus.Name = "cbConstStatus";
            this.cbConstStatus.Size = new System.Drawing.Size(154, 21);
            this.cbConstStatus.TabIndex = 1;
            // 
            // lblConStatus
            // 
            this.lblConStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblConStatus.AutoSize = true;
            this.lblConStatus.Location = new System.Drawing.Point(19, 2);
            this.lblConStatus.Name = "lblConStatus";
            this.lblConStatus.Size = new System.Drawing.Size(69, 26);
            this.lblConStatus.TabIndex = 0;
            this.lblConStatus.Text = "Construction Status:";
            // 
            // lblNumOfFloors
            // 
            this.lblNumOfFloors.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNumOfFloors.AutoSize = true;
            this.lblNumOfFloors.Location = new System.Drawing.Point(13, 68);
            this.lblNumOfFloors.Name = "lblNumOfFloors";
            this.lblNumOfFloors.Size = new System.Drawing.Size(75, 13);
            this.lblNumOfFloors.TabIndex = 4;
            this.lblNumOfFloors.Text = "Num of Floors:";
            // 
            // AddPropertyMultipleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 478);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox1);
            this.Name = "AddPropertyMultipleForm";
            this.Text = "AddPropertyMultipleForm";
            this.Load += new System.EventHandler(this.AddPropertyMultipleForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAmount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblPreviewText;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblPostFix;
        private System.Windows.Forms.Label lblPrefix;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblPreview;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.TextBox txtPostfix;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.Label lblType;
        protected Geomatic.UI.Controls.UpperComboBox cbType;
        protected System.Windows.Forms.Label lblNumOfFloors;
        protected System.Windows.Forms.Label lblConStatus;
        protected System.Windows.Forms.ComboBox cbConstStatus;
        protected System.Windows.Forms.Label lblYearInstall;
        protected System.Windows.Forms.ComboBox cbYearInstall;
        protected System.Windows.Forms.Label lblSource;
        protected Geomatic.UI.Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.Label lblLotHouseType;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rbLot;
        private System.Windows.Forms.RadioButton rbHouse;
        private Geomatic.UI.Controls.IntUpDown numAmount;
        private Geomatic.UI.Controls.IntUpDown numStartNo;
        private Geomatic.UI.Controls.IntUpDown numOfFloors;
        private System.Windows.Forms.Label lblIncrement;
        private Geomatic.UI.Controls.IntUpDown numIncrement;
    }
}
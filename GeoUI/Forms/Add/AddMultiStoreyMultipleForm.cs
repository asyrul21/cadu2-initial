﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using System.Text.RegularExpressions;

namespace Geomatic.UI.Forms.Add
{
    public partial class AddMultiStoreyMultipleForm : Form
    {
        #region properties
        public int SelectedNumOfFloors
        {
            get { return numOfFloors.Value; }
        }

        public int SelectedStartFloor
        {
            get { return numStartFloor.Value; }
        }

        public int SelectedNumOfApt
        {
            get { return numOfApt.Value; }
        }

        public string SelectedFloorPrefix
        {
            get { return txtFloorPrefix.Text; }
        }

        public string SelectedFloorPostfix
        {
            get { return txtFloorPostfix.Text; }
        }

        public string SelectedAptPrefix
        {
            get { return txtAptPrefix.Text; }
        }

        public string SelectedAptPostfix
        {
            get { return txtAptPostfix.Text; }
        }

        /*
        // comment by noraini - Feb 2019
        public string SelectedBlockName
        {
            get { return txtBlockName.Text; }
        }

        public string SelectedAptStringFormat
        {
            get { return cbStringFormat.Text; }
        }
        // end comment */

        public string SelectedFloorNumFormat
        {
            get { return cbFloorNumFormat.Text; }
        }

        public string SelectedAptNumFormat
        {
            get { return cbAptNumberFormat.Text; }
        }
        #endregion

        public AddMultiStoreyMultipleForm()
        {
            InitializeComponent();
            // added by noraini ali - Feb 2019
            cbFloorNumFormat.SelectedIndex = 0;
            cbAptNumberFormat.SelectedIndex = 0;
            string DefaultLotHseNo = BuildingForm.sendtextLotHse;
            txtAptPrefix.Text = string.Format("{0}-", DefaultLotHseNo);
            // end added
        }

        public void AddMultiStoreyMultipleForm_Load()
        {
            if (DesignMode) { return; }
            try
            {
                using (new WaitCursor())
                {
                    Form_Load();
                }
            }
            catch (Exception ex)
            {
                using (ErrorMessageBox box = new ErrorMessageBox())
                {
                    box.SetText(ex.Message).Show();
                }
            }

        }

        protected virtual void Form_Load()
        {
            RefreshTitle("Add Multiple Storey");

        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public virtual bool ValidateValues()
        {
            bool pass = true;

            //// Number
            //bool isStringFormatValid = true;
            //isStringFormatValid &= cbStringFormat.Text != "";
            //pass &= isStringFormatValid;

            //bool isBlockNameValid = true;
            //isBlockNameValid &= txtBlockName.Text != "";
            //pass &= isBlockNameValid;

            bool isFloorNumFormatValid = true;
            isFloorNumFormatValid &= cbFloorNumFormat.Text != "";
            pass &= isFloorNumFormatValid;

            bool isAptNumFormatValid = true;
            isAptNumFormatValid &= cbAptNumberFormat.Text != "";
            pass &= isAptNumFormatValid;

            //bool isBlockNameInputValid = true;
            //if (SelectedAptStringFormat.Contains("[BLOCK]")) isBlockNameInputValid &= SelectedBlockName != "";
            //pass &= isBlockNameInputValid;

            //lblStringFormat.ForeColor = isStringFormatValid ? Color.Black : Color.Red;
            //lblBlockName.ForeColor = isBlockNameValid ? Color.Black : Color.Red;
            lblFloorNumFormat.ForeColor = isFloorNumFormatValid ? Color.Black : Color.Red;
            lblAptNumberFormat.ForeColor = isAptNumFormatValid ? Color.Black : Color.Red;

            //if (!isBlockNameInputValid)
            //{
            //    lblStringFormat.ForeColor = isBlockNameInputValid ? Color.Black : Color.Red;
            //    lblBlockName.ForeColor = isBlockNameInputValid ? Color.Black : Color.Red;
            //}

            return pass;
        }

        protected void RefreshTitle(string title)
        {
            Text = title;
        }

        protected void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }

        private void PopulatePreview()
        {
            /* comment by noraini ali - Feb 2019
            string preview = SelectedAptStringFormat;
            if (preview.Contains("[BLOCK]") && SelectedBlockName != "")
                preview = preview.Replace("[BLOCK]", SelectedBlockName);

            if (preview.Contains("[FLR]") && SelectedFloorNumFormat != "")
                preview = preview.Replace("[FLR]", SelectedFloorNumFormat);

            if (preview.Contains("[APT]") && SelectedAptNumFormat != "")
                preview = preview.Replace("[APT]", SelectedAptNumFormat);
            */

            // added by noraini ali - Feb 2019
            string preview = "";

            string floorPrefix = SelectedFloorPrefix;
            string floorPostfix = SelectedFloorPostfix;
            string FloorNumFormat = SelectedFloorNumFormat;

            if (!string.IsNullOrEmpty(floorPrefix))
                FloorNumFormat = string.Format("{0}{1}", floorPrefix, FloorNumFormat);

            if (!string.IsNullOrEmpty(floorPostfix))
                FloorNumFormat = string.Format("{0}{1}", FloorNumFormat, floorPostfix);

            preview = string.Format("{0}", FloorNumFormat);

            int AptNum = SelectedNumOfApt;
            if (AptNum > 0)
            {
                string AptPrefix = SelectedAptPrefix;
                string AptPostfix = SelectedAptPostfix;
                string AptNumFormat = SelectedAptNumFormat;
                preview = "";

                if (!string.IsNullOrEmpty(AptPrefix))
                    AptNumFormat = string.Format("{0}{1}", AptPrefix, AptNumFormat);

                if (!string.IsNullOrEmpty(AptPostfix))
                    AptNumFormat = string.Format("{0}{1}", AptNumFormat, AptPostfix);

                preview = string.Format("{0}  {1}", FloorNumFormat, AptNumFormat);
            }
            lblPreviewText.Text = preview;
        }

        private void cbAptStringFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtBlockName_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbFloorNumFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbAptNumberFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtFloorPrefix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtFloorPostfix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtAptPrefix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtAptPostfix_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void numOfApt_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
            int AptNum = SelectedNumOfApt;

            txtAptPostfix.Enabled = true;
            txtAptPrefix.Enabled = true;
            cbAptNumberFormat.Enabled = true;
            if (AptNum < 1)
            {
                txtAptPostfix.Enabled = false;
                txtAptPrefix.Enabled = false;
                cbAptNumberFormat.Enabled = false;
            }
        }
    }
}

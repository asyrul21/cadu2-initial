﻿namespace Geomatic.UI.Forms
{
    partial class AccessibilityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.controlGroup = new Geomatic.UI.Controls.ChildControl();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.pageGroup = new System.Windows.Forms.TabPage();
            this.pageUser = new System.Windows.Forms.TabPage();
            this.controlUser = new Geomatic.UI.Controls.ChildControl();
            this.tabControl.SuspendLayout();
            this.pageGroup.SuspendLayout();
            this.pageUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(447, 377);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // controlGroup
            // 
            this.controlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlGroup.Location = new System.Drawing.Point(3, 3);
            this.controlGroup.Name = "controlGroup";
            this.controlGroup.Size = new System.Drawing.Size(496, 327);
            this.controlGroup.TabIndex = 0;
            this.controlGroup.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlGroup_AddClick);
            this.controlGroup.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlGroup_EditClick);
            this.controlGroup.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlGroup_DeleteClick);
            this.controlGroup.DoubleClick += new System.EventHandler(this.controlGroup_EditClick);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.pageGroup);
            this.tabControl.Controls.Add(this.pageUser);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(510, 359);
            this.tabControl.TabIndex = 0;
            // 
            // pageGroup
            // 
            this.pageGroup.Controls.Add(this.controlGroup);
            this.pageGroup.Location = new System.Drawing.Point(4, 22);
            this.pageGroup.Name = "pageGroup";
            this.pageGroup.Padding = new System.Windows.Forms.Padding(3);
            this.pageGroup.Size = new System.Drawing.Size(502, 333);
            this.pageGroup.TabIndex = 0;
            this.pageGroup.Text = "Group";
            this.pageGroup.UseVisualStyleBackColor = true;
            // 
            // pageUser
            // 
            this.pageUser.Controls.Add(this.controlUser);
            this.pageUser.Location = new System.Drawing.Point(4, 22);
            this.pageUser.Name = "pageUser";
            this.pageUser.Padding = new System.Windows.Forms.Padding(3);
            this.pageUser.Size = new System.Drawing.Size(502, 333);
            this.pageUser.TabIndex = 1;
            this.pageUser.Text = "User";
            this.pageUser.UseVisualStyleBackColor = true;
            // 
            // controlUser
            // 
            this.controlUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlUser.Location = new System.Drawing.Point(3, 3);
            this.controlUser.Name = "controlUser";
            this.controlUser.Size = new System.Drawing.Size(496, 327);
            this.controlUser.TabIndex = 0;
            this.controlUser.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlUser_AddClick);
            this.controlUser.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlUser_EditClick);
            this.controlUser.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlUser_DeleteClick);
            this.controlUser.DoubleClick += new System.EventHandler(this.controlUser_EditClick);
            // 
            // AccessibilityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 412);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnClose);
            this.Name = "AccessibilityForm";
            this.Text = "AccessibilityForm";
            this.Load += new System.EventHandler(this.AccessibilityForm_Load);
            this.tabControl.ResumeLayout(false);
            this.pageGroup.ResumeLayout(false);
            this.pageUser.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Button btnClose;
        protected Geomatic.UI.Controls.ChildControl controlGroup;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage pageGroup;
        private System.Windows.Forms.TabPage pageUser;
        private Geomatic.UI.Controls.ChildControl controlUser;
    }
}
﻿using Geomatic.Core.Features;
using Geomatic.UI.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using ESRI.ArcGIS.Display;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Search;
using System.IO;
using System.Text;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms
{
    public partial class VerifyWorkAreaForm : CollapsibleForm
    {
        public event EventHandler<SelectedFeatureChangedEventArgs> SelectedFeatureChanged;

        int gListView1LostFocusItem = -1;
        private string FeatureID = "";
        private string FeatureName = "";

        #region 
        public int? ChooseJunction
        {
            get { return (!chkJunction.Checked) ? 1 : 0; }
        }

        public int? ChooseStreet
        {
            get { return (!chkStreet.Checked) ? 1 : 0; }
        }

        public int? ChooseProperty
        {
            get { return (!chkProperty.Checked) ? 1 : 0; }
        }

        public int? ChooseLandmark
        {
            get { return (!chkLandmark.Checked) ? 1 : 0; }
        }

        public int? ChooseBuilding
        {
            get { return (!chkBuilding.Checked) ? 1 : 0; }
        }

        public int? ChooseBuildingGroup
        {
            get { return (!chkBuildingGroup.Checked) ? 1 : 0; }
        }
        #endregion

        private void VerifyWorkAreaForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                AcceptButton = btnReview;
                Form_Load();
            }
        }

        protected GWorkArea _workarea;

        public VerifyWorkAreaForm(GWorkArea workarea)
        {
            InitializeComponent();
            _workarea = workarea;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Verification Work Area");

            txtWorkAreaId.Text = _workarea.OID.ToString();
            txtWAStatus.Text = GetFlagName(_workarea.flag);  // noraini ali -Jun 2020 - display flag name
            txtWAOwner.Text = _workarea.user_id;
            txtWONo.Text = _workarea.Wo_no;

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Feature Name");
            lvResult.Columns.Add("AND Status Flag");
            lvResult.Columns.Add("AND Status Name");
            lvResult.Columns.Add("Remark");
            PopulateFeatureByWA();

            // noraini ali - Jul 2020
            // display message to prompt supervisor save data before proceed Verification
            if (lvResult.Items.Count > 0)
            {
                string msgtext = "Please save data to excel before proceed Verification Process";
                MessageBox.Show(msgtext, "Verification Module", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            // end
        }

        #region populate Feature and And Status belong to Work Area Id
        protected ListViewItem CreateitemProperty(GProperty property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add("Property");
            item.SubItems.Add(property.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(property.AndStatus));
            item.SubItems.Add("");
            return item;
        }
        protected ListViewItem CreateitemLandmark(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add("Landmark");
            item.SubItems.Add(landmark.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(landmark.AndStatus));
            item.SubItems.Add("");
            item.Tag = landmark;
            return item;
        }
        protected ListViewItem CreateitemJunction(GJunction junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add("Junction");
            item.SubItems.Add(junction.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(junction.AndStatus));
            item.SubItems.Add("");
            item.Tag = junction;
            return item;
        }
        protected ListViewItem CreateitemStreet(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add("Street");
            item.SubItems.Add(street.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(street.AndStatus));
            item.SubItems.Add("");
            item.Tag = street;
            return item;
        }
        protected ListViewItem CreateitemBuilding(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add("Building");
            item.SubItems.Add(building.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(building.AndStatus));
            item.SubItems.Add("");
            item.Tag = building;
            return item;
        }
        protected ListViewItem CreateitemBuildingGroup(GBuildingGroup buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add("BuildingGroup");
            item.SubItems.Add(buildingGroup.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(buildingGroup.AndStatus));
            item.SubItems.Add("");
            item.Tag = buildingGroup;
            return item;
        }
        #endregion      

        private string GetnameAndStatus(int? andStatus)
        {
            string AndStatusName = "";

            if (andStatus == 1)
            { AndStatusName = "NEW"; }
            else if (andStatus == 2)
            { AndStatusName = "DELETE"; }
            else if (andStatus == 3)
            { AndStatusName = "EDIT GRAPHIC & ATT"; }
            else if (andStatus == 4)
            { AndStatusName = "EDIT ATT ONLY"; }

            return AndStatusName;
        }
        private void BtnReview_Click(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                return;
            }

            if (lvResult.Items.Count > 0)
            {
                lvResult.Items[0].Selected = true;
                if (!lvResult.Focused)
                {
                    lvResult.Focus();
                }
            }
            DisplayAttributeToVerification();
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        private void LvResult_Click(object sender, EventArgs e)
        {
            btnReview.Enabled = true;
            gListView1LostFocusItem = lvResult.FocusedItem.Index;
            if (lvResult.Items[gListView1LostFocusItem].BackColor == Color.ForestGreen ||
               lvResult.Items[gListView1LostFocusItem].BackColor == Color.IndianRed)
            {
                btnReview.Enabled = false;
            }
        }

        private void LvResult_MouseDoubleClick(object sender, EventArgs e)
        {
            DisplayAttributeToVerification();
        }

        private void DisplayAttributeToVerification()
        {
            btnReview.Enabled = true;

            gListView1LostFocusItem = lvResult.FocusedItem.Index;
            if (lvResult.Items[gListView1LostFocusItem].BackColor == Color.ForestGreen ||
               lvResult.Items[gListView1LostFocusItem].BackColor == Color.IndianRed)
            {
                { btnReview.Enabled = false; }
            }
            else
            {
                ListViewItem item1 = lvResult.Items[gListView1LostFocusItem];
                FeatureID = item1.Text;
                FeatureName = item1.SubItems[1].Text;

                // Noraini Ali - to highlight feature 
                //RepositoryFactory repo = new RepositoryFactory(_workarea.SegmentName);
                //int? id = StringUtils.CheckInt(FeatureID);
                //
                //GProperty property = repo.GetById<GProperty>(id);
                //HighlightSelected(property);
                //
                //GBuilding building = repo.GetById<GBuilding>(id);
                //HighlightSelected(building);

                //

                using (VerificationForm form = new VerificationForm(FeatureID, FeatureName, _workarea.WorkAreaSegmentName))
                {
                    if (form.ShowDialog(this) != DialogResult.OK)
                    {
                        return;
                    }
                    using (new WaitCursor())
                    {
                        if (lvResult.Items.Count > 0)
                        {
                            lvResult.Items[gListView1LostFocusItem].Selected = true;

                            // Case Verify AND Feature
                            if (form.verifystatus == 0)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Verification");
                            }
                            else if (form.verifystatus == 1)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Successful Verification");
                            }
                            else if (form.verifystatus == 2)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Verify Process");
                            }

                            // Case Revert AND Feature
                            if (form.revertstatus == 0)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Revert Process");
                            }
                            else if (form.revertstatus == 1)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Successful Revert Process");
                            }
                            else if (form.revertstatus == 2)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Revert Process.");
                            }

                            if (form.verifystatus == 1 || form.revertstatus == 1)
                            {
                                lvResult.Items[gListView1LostFocusItem].BackColor = Color.ForestGreen;
                                lvResult.Items[gListView1LostFocusItem].ForeColor = Color.White;
                            }
                            else
                            {
                                lvResult.Items[gListView1LostFocusItem].BackColor = Color.IndianRed;
                                lvResult.Items[gListView1LostFocusItem].ForeColor = Color.White;
                            }
                            if (!lvResult.Focused)
                            {
                                lvResult.Focus();
                            }
                        }
                    }
                }
            }
        }

        private void HighlightSelected(GBuilding building)
        {
            throw new NotImplementedException();
        }

        private void HighlightSelected(GProperty property)
        {
            throw new NotImplementedException();
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            PopulateFeatureByWA();
        }

        private void PopulateFeatureByWA()
        {
            lvResult.BeginUpdate();
            lvResult.Sorting = SortOrder.None;
            lvResult.Items.Clear();

            try
            {
                List<ListViewItem> items = new List<ListViewItem>();
                if (chkProperty.Checked)
                {
                    foreach (GProperty property in _workarea.GetPropertyAndStatus(false))
                    {
                        ListViewItem item = CreateitemProperty(property);
                        item.Tag = property;
                        items.Add(item);
                    }
                }
                if (chkLandmark.Checked)
                {
                    foreach (GLandmark landmark in _workarea.GetLandmarkAndStatus(true))
                    {
                        ListViewItem item = CreateitemLandmark(landmark);
                        item.Tag = landmark;
                        items.Add(item);
                    }
                }
                if (chkJunction.Checked)
                {
                    foreach (GJunction junction in _workarea.GetJunctionAndStatus(false))
                    {
                        ListViewItem item = CreateitemJunction(junction);
                        item.Tag = junction;
                        items.Add(item);
                    }
                }
                if (chkStreet.Checked)
                {
                    foreach (GStreet street in _workarea.GetStreetAndStatus(false))
                    {
                        ListViewItem item = CreateitemStreet(street);
                        item.Tag = street;
                        items.Add(item);
                    }
                }
                if (chkBuilding.Checked)
                {
                    foreach (GBuilding building in _workarea.GetBuildingAndStatus(false))
                    {
                        ListViewItem item = CreateitemBuilding(building);
                        item.Tag = building;
                        items.Add(item);
                    }
                }
                if (chkBuildingGroup.Checked)
                {
                    foreach (GBuildingGroup buildingGroup in _workarea.GetBuildingGroupAndStatus(false))
                    {
                        ListViewItem item = CreateitemBuildingGroup(buildingGroup);
                        items.Add(item);
                    }
                }

                lvResult.Items.AddRange(items.ToArray());

                if (lvResult.Items.Count > 0)
                {
                    lvResult.Items[0].Selected = true;
                    if (!lvResult.Focused)
                    {
                        lvResult.Focus();
                    }
                }
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        private void UpdateSelectedItem(ListViewItem item, string txtName, string txtProcess)
        {
            if (txtName == "Property")
            {
                GProperty property = (GProperty)item.Tag;
                item.SubItems.Clear();
                //item.Text = property.OID.ToString() + "-Property";
                item.Text = property.OID.ToString();
                item.SubItems.Add("Property");
                item.SubItems.Add(property.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(property.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = property;
            }
            else if (txtName == "Junction")
            {
                GJunction junction = (GJunction)item.Tag;
                item.SubItems.Clear();
                //item.Text = junction.OID.ToString() + "-Junction";
                item.Text = junction.OID.ToString();
                item.SubItems.Add("Junction");
                item.SubItems.Add(junction.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(junction.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = junction;
            }
            else if (txtName == "Building")
            {
                GBuilding building = (GBuilding)item.Tag;
                item.SubItems.Clear();
                //item.Text = building.OID.ToString() + "-Building";
                item.Text = building.OID.ToString();
                item.SubItems.Add("Building");
                item.SubItems.Add(building.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(building.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = building;
            }
            else if (txtName == "Street")
            {
                GStreet street = (GStreet)item.Tag;
                item.SubItems.Clear();
                //item.Text = street.OID.ToString() + "-Street";
                item.Text = street.OID.ToString();
                item.SubItems.Add("Street");
                item.SubItems.Add(street.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(street.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = street;
            }
            else if (txtName == "Landmark")
            {
                GLandmark landmark = (GLandmark)item.Tag;
                item.SubItems.Clear();
                //item.Text = landmark.OID.ToString() + "-Landmark";
                item.Text = landmark.OID.ToString();
                item.SubItems.Add("Landmark");
                item.SubItems.Add(landmark.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(landmark.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = landmark;
            }
            else if (txtName == "BuildingGroup")
            {
                GBuildingGroup buildingGroup = (GBuildingGroup)item.Tag;
                item.SubItems.Clear();
                //item.Text = buildingGroup.OID.ToString() + "-BuildingGroup";
                item.Text = buildingGroup.OID.ToString();
                item.SubItems.Add("BuildingGroup");
                item.SubItems.Add(buildingGroup.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(buildingGroup.AndStatus));
                item.SubItems.Add(txtProcess);
                item.Tag = buildingGroup;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void lvResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                OnSelectedFeatureChanged(new SelectedFeatureChangedEventArgs(null));
            }
            else
            {
                IGFeature feature = (IGFeature)lvResult.SelectedItems[0].Tag;
                OnSelectedFeatureChanged(new SelectedFeatureChangedEventArgs(feature));
            }
        }

        private void OnSelectedFeatureChanged(SelectedFeatureChangedEventArgs e)
        {
            if (SelectedFeatureChanged != null)
            {
                SelectedFeatureChanged(this, e);
            }
        }

        private string GetFlagName(string flag)
        {
            string flagName = "";

            if (flag == null)
            { flagName = "New"; }
            else if (flag == "0")
            { flagName = "Open"; }
            else if (flag == "1")
            { flagName = "Closed"; }
            else if (flag == "2")
            { flagName = "Verifying"; }

            return flagName;
        }
        
        // noraini ali - Jul 2020 - Add button save As
        private void BtnSaveVerify_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            using (new WaitCursor())
            {
                string fileName = saveFileDialog.FileName;

                if (!File.Exists(fileName))
                {
                    FileStream fileStream = File.Create(fileName);
                    fileStream.Dispose();
                }

                using (StreamWriter streamWriter = new StreamWriter(fileName, false))
                {
                    List<string> columns = new List<string>();
                    foreach (ColumnHeader columnHeader in lvResult.Columns)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("\"");
                        sb.Append(columnHeader.Text);
                        sb.Append("\"");
                        columns.Add(sb.ToString());
                    }
                    streamWriter.WriteLine(string.Join(",", columns.ToArray()));

                    foreach (ListViewItem item in lvResult.Items)
                    {
                        List<string> subItems = new List<string>();
                        foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("\"");
                            sb.Append(subItem.Text);
                            sb.Append("\"");
                            subItems.Add(sb.ToString());
                        }
                        streamWriter.WriteLine(string.Join(",", subItems.ToArray()));
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.UI.Forms.Views
{
    public partial class OutputView : View
    {
        public OutputView()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            RefreshTitle("Output");
        }

        public override void OnActiveDocumentChanged(MapDocument document)
        {

        }

        public void AppendMessage(string message)
        {
            outputTextBox.AppendText(string.Format("[{0}] {1}\n", DateTime.Now, message));
            outputTextBox.ScrollToCaret();
        }
    }
}

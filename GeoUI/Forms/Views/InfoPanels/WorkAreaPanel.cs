﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features;
using Geomatic.UI.Properties;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class WorkAreaPanel : InfoPanel
    {
        public WorkAreaPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            if (!(feature is GWorkArea workarea))
            {
                return;
            }

            infoId.DisplayText = workarea.OID.ToString();
            infoName.DisplayText = workarea.Wo_no;
            infoUserName.DisplayText = workarea.user_id;
            infoAreaSegment.DisplayText = workarea.WorkAreaSegmentName;

            // Noraini ali - Display work area datetime in normal format
            string dd = workarea.DateCreated1.ToString();
            DateTime NormalFormat = DisplayNormalDateTimeFormat(dd);
            infoDateCreated.DisplayText = NormalFormat.ToString("yyyyMMdd");

            dd = workarea.DateUpdated1.ToString();
            NormalFormat = DisplayNormalDateTimeFormat(dd);
            infoDateModified.DisplayText = NormalFormat.ToString("yyyyMMdd");

            //infoDateCreated.DisplayText = workarea.DateCreated1;
            //infoDateModified.DisplayText = workarea.DateUpdated1;

            // noraini ali - Jun 2020 CADU2 AND - display flag name
            if (workarea.flag == null)
            { 
                infoFlag.DisplayText = "New";
            }
            else if(workarea.flag == "0")
            {
                infoFlag.DisplayText = "Open";
            }
            else if (workarea.flag == "1")
            {
                infoFlag.DisplayText = "Closed";
                // Noraini ali - to keep WA id use for verification module
                Settings.Default.PickedWorkAreaIdIndex = workarea.OID;
            }
            else if (workarea.flag == "2")
            {
                infoFlag.DisplayText = "Verifying";
                // Noraini ali - to keep WA id use for verification module
                Settings.Default.PickedWorkAreaIdIndex = workarea.OID;
            }
        }

        private DateTime DisplayNormalDateTimeFormat(string dd)
        {
            DateTime NormalFormatDate;
            string[] formats = {"MM/dd/yyyy hh:mm:ss tt", "MM/dd/yyyy hh:mm:ss", "MM/dd/yyyy h:mm:ss", "MM/dd/yyyy h:mm:ss tt",
                                "dd/MM/yyyy hh:mm:ss tt", "dd/MM/yyyy hh:mm:ss", "dd/MM/yyyy h:mm:ss", "dd/MM/yyyy h:mm:ss tt",
                                "dd/MM/yyyy HH:mm:ss", "MM/dd/yyyy HH:mm:ss", "d/d/yyyy HH:mm:ss", "dd/MM/yyy HH:mm:ss",
                                "M/dd/yyyy hh:mm:ss tt", "M/dd/yyyy hh:mm:ss", "M/dd/yyyy h:mm:ss", "M/dd/yyyy h:mm:ss tt",
                                "d/MM/yyyy hh:mm:ss tt", "d/MM/yyyy hh:mm:ss", "d/MM/yyyy h:mm:ss", "d/MM/yyyy h:mm:ss tt",
                                "d/M/yyyy hh:mm:ss tt", "d/M/yyyy hh:mm:ss", "d/M/yyyy h:mm:ss", "d/M/yyyy h:mm:ss tt",
                                "M/d/yyyy hh:mm:ss tt", "M/d/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", "M/d/yyyy h:mm:ss tt",
                                "MM/dd/yy hh:mm:ss tt", "MM/dd/yy hh:mm:ss", "MM/dd/yy h:mm:ss", "MM/dd/yy h:mm:ss tt",
                                "dd/MM/yy hh:mm:ss tt", "dd/MM/yy hh:mm:ss", "dd/MM/yy h:mm:ss", "dd/MM/yy h:mm:ss tt",
                                "M/dd/yy hh:mm:ss tt", "M/dd/yy hh:mm:ss", "M/dd/yy h:mm:ss", "M/dd/yy h:mm:ss tt",
                                "d/MM/yy hh:mm:ss tt", "d/MM/yy hh:mm:ss", "d/MM/yy h:mm:ss", "d/MM/yy h:mm:ss tt",
                                "d/M/yy hh:mm:ss tt", "d/M/yy hh:mm:ss", "d/M/yy h:mm:ss", "d/M/yy h:mm:ss tt",
                                "dd-MMM-yyyy hh:mm:ss tt", "dd-MMM-yyyy hh:mm:ss", "dd-MMM-yyyy h:mm:ss tt",
                                "dd-MMM-yyyy hh:mm tt", "dd-MMM-yyyy hh:mm", "dd-MMM-yyyy h:mm tt",
                                "dd-MMM-yy hh:mm:ss tt", "dd-MMM-yy hh:mm:ss", "dd-MMM-yy h:mm:ss tt",
                                "dd-MMM-yy hh:mm tt", "dd-MMM-yy hh:mm", "dd-MMM-yy h:mm tt",
                                "dd-MM-yyyy", "yyyyMMdd", "MMddyyyy", "ddMMyyyy", "yyMMdd", "MMddyy", "ddMMyy"};

            DateTime.TryParseExact(dd, formats,
            System.Globalization.CultureInfo.InvariantCulture,
            System.Globalization.DateTimeStyles.None, out NormalFormatDate);
            return NormalFormatDate;
        }
    }
}

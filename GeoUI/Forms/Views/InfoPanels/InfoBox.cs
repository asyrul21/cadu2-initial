﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class InfoBox : UserControl
    {
        public InfoBox()
        {
            InitializeComponent();
        }

        [Browsable(true), Category("Info")]
        public string DisplayLabel
        {
            set { lbl.Text = value; }
            get { return lbl.Text; }
        }

        [Browsable(true), Category("Info")]
        public string DisplayText
        {
            set { txt.Text = value; }
            get { return txt.Text; }
        }
    }
}

﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class JunctionPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoDateUpdated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUpdatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoY = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoX = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoDateUpdated
            // 
            this.infoDateUpdated.DisplayLabel = "Date Updated:";
            this.infoDateUpdated.DisplayText = "";
            this.infoDateUpdated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateUpdated.Location = new System.Drawing.Point(0, 132);
            this.infoDateUpdated.Name = "infoDateUpdated";
            this.infoDateUpdated.Size = new System.Drawing.Size(284, 22);
            this.infoDateUpdated.TabIndex = 6;
            // 
            // infoUpdatedBy
            // 
            this.infoUpdatedBy.DisplayLabel = "Updated By:";
            this.infoUpdatedBy.DisplayText = "";
            this.infoUpdatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUpdatedBy.Location = new System.Drawing.Point(0, 110);
            this.infoUpdatedBy.Name = "infoUpdatedBy";
            this.infoUpdatedBy.Size = new System.Drawing.Size(284, 22);
            this.infoUpdatedBy.TabIndex = 5;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 88);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(284, 22);
            this.infoDateCreated.TabIndex = 4;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 66);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(284, 22);
            this.infoCreatedBy.TabIndex = 3;
            // 
            // infoName
            // 
            this.infoName.DisplayLabel = "Name:";
            this.infoName.DisplayText = "";
            this.infoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName.Location = new System.Drawing.Point(0, 44);
            this.infoName.Name = "infoName";
            this.infoName.Size = new System.Drawing.Size(284, 22);
            this.infoName.TabIndex = 2;
            // 
            // infoType
            // 
            this.infoType.DisplayLabel = "Type:";
            this.infoType.DisplayText = "";
            this.infoType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoType.Location = new System.Drawing.Point(0, 22);
            this.infoType.Name = "infoType";
            this.infoType.Size = new System.Drawing.Size(284, 22);
            this.infoType.TabIndex = 1;
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(284, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoY
            // 
            this.infoY.DisplayLabel = "Y:";
            this.infoY.DisplayText = "";
            this.infoY.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoY.Location = new System.Drawing.Point(0, 176);
            this.infoY.Name = "infoY";
            this.infoY.Size = new System.Drawing.Size(284, 22);
            this.infoY.TabIndex = 8;
            // 
            // infoX
            // 
            this.infoX.DisplayLabel = "X:";
            this.infoX.DisplayText = "";
            this.infoX.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoX.Location = new System.Drawing.Point(0, 154);
            this.infoX.Name = "infoX";
            this.infoX.Size = new System.Drawing.Size(284, 22);
            this.infoX.TabIndex = 7;
            // 
            // JunctionPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoY);
            this.Controls.Add(this.infoX);
            this.Controls.Add(this.infoDateUpdated);
            this.Controls.Add(this.infoUpdatedBy);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoName);
            this.Controls.Add(this.infoType);
            this.Controls.Add(this.infoId);
            this.Name = "JunctionPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoDateUpdated;
        private InfoBox infoUpdatedBy;
        private InfoBox infoDateCreated;
        private InfoBox infoCreatedBy;
        private InfoBox infoName;
        private InfoBox infoType;
        private InfoBox infoId;
        private InfoBox infoY;
        private InfoBox infoX;
    }
}

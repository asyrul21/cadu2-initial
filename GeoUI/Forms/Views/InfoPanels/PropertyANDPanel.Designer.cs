﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class PropertyANDPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoDateUpdated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUpdatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoState = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCity = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoPostcode = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoSection = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetName2 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoHouse = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoLot = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoWorkArea = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoY = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoX = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoDateUpdated
            // 
            this.infoDateUpdated.DisplayLabel = "Date Updated:";
            this.infoDateUpdated.DisplayText = "";
            this.infoDateUpdated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateUpdated.Location = new System.Drawing.Point(0, 308);
            this.infoDateUpdated.Name = "infoDateUpdated";
            this.infoDateUpdated.Size = new System.Drawing.Size(250, 22);
            this.infoDateUpdated.TabIndex = 14;
            // 
            // infoUpdatedBy
            // 
            this.infoUpdatedBy.DisplayLabel = "Updated By:";
            this.infoUpdatedBy.DisplayText = "";
            this.infoUpdatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUpdatedBy.Location = new System.Drawing.Point(0, 286);
            this.infoUpdatedBy.Name = "infoUpdatedBy";
            this.infoUpdatedBy.Size = new System.Drawing.Size(250, 22);
            this.infoUpdatedBy.TabIndex = 13;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 264);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(250, 22);
            this.infoDateCreated.TabIndex = 12;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 242);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(250, 22);
            this.infoCreatedBy.TabIndex = 11;
            // 
            // infoState
            // 
            this.infoState.DisplayLabel = "State:";
            this.infoState.DisplayText = "";
            this.infoState.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoState.Location = new System.Drawing.Point(0, 220);
            this.infoState.Name = "infoState";
            this.infoState.Size = new System.Drawing.Size(250, 22);
            this.infoState.TabIndex = 10;
            // 
            // infoCity
            // 
            this.infoCity.DisplayLabel = "City:";
            this.infoCity.DisplayText = "";
            this.infoCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCity.Location = new System.Drawing.Point(0, 198);
            this.infoCity.Name = "infoCity";
            this.infoCity.Size = new System.Drawing.Size(250, 22);
            this.infoCity.TabIndex = 9;
            // 
            // infoPostcode
            // 
            this.infoPostcode.DisplayLabel = "Postcode:";
            this.infoPostcode.DisplayText = "";
            this.infoPostcode.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoPostcode.Location = new System.Drawing.Point(0, 176);
            this.infoPostcode.Name = "infoPostcode";
            this.infoPostcode.Size = new System.Drawing.Size(250, 22);
            this.infoPostcode.TabIndex = 8;
            // 
            // infoSection
            // 
            this.infoSection.DisplayLabel = "Section:";
            this.infoSection.DisplayText = "";
            this.infoSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoSection.Location = new System.Drawing.Point(0, 154);
            this.infoSection.Name = "infoSection";
            this.infoSection.Size = new System.Drawing.Size(250, 22);
            this.infoSection.TabIndex = 7;
            // 
            // infoStreetName2
            // 
            this.infoStreetName2.DisplayLabel = "Street Name2:";
            this.infoStreetName2.DisplayText = "";
            this.infoStreetName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetName2.Location = new System.Drawing.Point(0, 132);
            this.infoStreetName2.Name = "infoStreetName2";
            this.infoStreetName2.Size = new System.Drawing.Size(250, 22);
            this.infoStreetName2.TabIndex = 6;
            // 
            // infoStreetName
            // 
            this.infoStreetName.DisplayLabel = "Street Name:";
            this.infoStreetName.DisplayText = "";
            this.infoStreetName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetName.Location = new System.Drawing.Point(0, 110);
            this.infoStreetName.Name = "infoStreetName";
            this.infoStreetName.Size = new System.Drawing.Size(250, 22);
            this.infoStreetName.TabIndex = 5;
            // 
            // infoStreetType
            // 
            this.infoStreetType.DisplayLabel = "Street Type:";
            this.infoStreetType.DisplayText = "";
            this.infoStreetType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetType.Location = new System.Drawing.Point(0, 88);
            this.infoStreetType.Name = "infoStreetType";
            this.infoStreetType.Size = new System.Drawing.Size(250, 22);
            this.infoStreetType.TabIndex = 4;
            // 
            // infoHouse
            // 
            this.infoHouse.DisplayLabel = "House:";
            this.infoHouse.DisplayText = "";
            this.infoHouse.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoHouse.Location = new System.Drawing.Point(0, 66);
            this.infoHouse.Name = "infoHouse";
            this.infoHouse.Size = new System.Drawing.Size(250, 22);
            this.infoHouse.TabIndex = 3;
            // 
            // infoLot
            // 
            this.infoLot.DisplayLabel = "Lot:";
            this.infoLot.DisplayText = "";
            this.infoLot.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoLot.Location = new System.Drawing.Point(0, 44);
            this.infoLot.Name = "infoLot";
            this.infoLot.Size = new System.Drawing.Size(250, 22);
            this.infoLot.TabIndex = 2;
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Ori_Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(250, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoType
            // 
            this.infoType.DisplayLabel = "Type:";
            this.infoType.DisplayText = "";
            this.infoType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoType.Location = new System.Drawing.Point(0, 22);
            this.infoType.Name = "infoType";
            this.infoType.Size = new System.Drawing.Size(250, 22);
            this.infoType.TabIndex = 1;
            // 
            // infoStreetId
            // 
            this.infoStreetId.DisplayLabel = "Street Id:";
            this.infoStreetId.DisplayText = "";
            this.infoStreetId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetId.Location = new System.Drawing.Point(0, 330);
            this.infoStreetId.Name = "infoStreetId";
            this.infoStreetId.Size = new System.Drawing.Size(250, 22);
            this.infoStreetId.TabIndex = 17;
            // 
            // infoWorkArea
            // 
            this.infoWorkArea.DisplayLabel = "Work Area:";
            this.infoWorkArea.DisplayText = "";
            this.infoWorkArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoWorkArea.Location = new System.Drawing.Point(0, 352);
            this.infoWorkArea.Name = "infoWorkArea";
            this.infoWorkArea.Size = new System.Drawing.Size(250, 22);
            this.infoWorkArea.TabIndex = 18;
            // 
            // infoY
            // 
            this.infoY.DisplayLabel = "Y:";
            this.infoY.DisplayText = "";
            this.infoY.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoY.Location = new System.Drawing.Point(0, 396);
            this.infoY.Name = "infoY";
            this.infoY.Size = new System.Drawing.Size(250, 22);
            this.infoY.TabIndex = 20;
            // 
            // infoX
            // 
            this.infoX.DisplayLabel = "X:";
            this.infoX.DisplayText = "";
            this.infoX.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoX.Location = new System.Drawing.Point(0, 374);
            this.infoX.Name = "infoX";
            this.infoX.Size = new System.Drawing.Size(250, 22);
            this.infoX.TabIndex = 19;
            // 
            // PropertyANDPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoY);
            this.Controls.Add(this.infoX);
            this.Controls.Add(this.infoWorkArea);
            this.Controls.Add(this.infoStreetId);
            this.Controls.Add(this.infoDateUpdated);
            this.Controls.Add(this.infoUpdatedBy);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoState);
            this.Controls.Add(this.infoCity);
            this.Controls.Add(this.infoPostcode);
            this.Controls.Add(this.infoSection);
            this.Controls.Add(this.infoStreetName2);
            this.Controls.Add(this.infoStreetName);
            this.Controls.Add(this.infoStreetType);
            this.Controls.Add(this.infoHouse);
            this.Controls.Add(this.infoLot);
            this.Controls.Add(this.infoType);
            this.Controls.Add(this.infoId);
            this.Name = "PropertyANDPanel";
            this.Size = new System.Drawing.Size(250, 400);
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoDateUpdated;
        private InfoBox infoUpdatedBy;
        private InfoBox infoDateCreated;
        private InfoBox infoCreatedBy;
        private InfoBox infoState;
        private InfoBox infoCity;
        private InfoBox infoPostcode;
        private InfoBox infoSection;
        private InfoBox infoStreetName2;
        private InfoBox infoStreetName;
        private InfoBox infoStreetType;
        private InfoBox infoHouse;
        private InfoBox infoLot;
        private InfoBox infoId;
        private InfoBox infoType;
        private InfoBox infoStreetId;
        private InfoBox infoWorkArea;
        private InfoBox infoY;
        private InfoBox infoX;
    }
}

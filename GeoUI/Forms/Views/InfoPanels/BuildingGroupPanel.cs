﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class BuildingGroupPanel : InfoPanel
    {
        public BuildingGroupPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GBuildingGroup buildingGroup = feature as GBuildingGroup;
            if (buildingGroup == null)
            {
                return;
            }

            infoId.DisplayText = buildingGroup.OID.ToString();
            infoName.DisplayText = buildingGroup.Name;
            infoCreatedBy.DisplayText = buildingGroup.CreatedBy;
            infoDateCreated.DisplayText = buildingGroup.DateCreated;
            infoUpdatedBy.DisplayText = buildingGroup.UpdatedBy;
            infoDateUpdated.DisplayText = buildingGroup.DateUpdated;
            infoX.DisplayText = buildingGroup.X.ToString();
            infoY.DisplayText = buildingGroup.Y.ToString();
            infoStreetId.DisplayText = buildingGroup.StreetId.ToString();
            infoNumUnit.DisplayText = buildingGroup.NumUnit.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class BuildingGroupANDPanel : InfoPanel
    {
        public BuildingGroupANDPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GBuildingGroupAND buildingGroupAND = feature as GBuildingGroupAND;
            if (buildingGroupAND == null)
            {
                return;
            }
            infoId.DisplayText = buildingGroupAND.OriId.ToString();
            infoName.DisplayText = buildingGroupAND.Name;
            infoWorkArea1.DisplayText = buildingGroupAND.AreaId.ToString();
            infoStreetId.DisplayText = buildingGroupAND.StreetId.ToString();
            infoDateCreated.DisplayText = buildingGroupAND.DateCreated;
            infoCreatedBy.DisplayText = buildingGroupAND.CreatedBy;
            infoDateUpdated.DisplayText = buildingGroupAND.DateUpdated;
            infoUpdatedBy.DisplayText = buildingGroupAND.UpdatedBy;
            infoX.DisplayText = buildingGroupAND.X.ToString();
            infoY.DisplayText = buildingGroupAND.Y.ToString();
            infoNumUnit.DisplayText = buildingGroupAND.NumUnit.ToString();
        }
    }
}

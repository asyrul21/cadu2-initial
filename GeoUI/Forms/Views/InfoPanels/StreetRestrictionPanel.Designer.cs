﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class StreetRestrictionPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoNaviStatus = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoJunctionId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStartId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId1 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId2 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId3 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId4 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId5 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId6 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId7 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId8 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId9 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoEndId10 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(284, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoNaviStatus
            // 
            this.infoNaviStatus.DisplayLabel = "Navigation Status:";
            this.infoNaviStatus.DisplayText = "";
            this.infoNaviStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoNaviStatus.Location = new System.Drawing.Point(0, 22);
            this.infoNaviStatus.Name = "infoNaviStatus";
            this.infoNaviStatus.Size = new System.Drawing.Size(284, 22);
            this.infoNaviStatus.TabIndex = 1;
            // 
            // infoJunctionId
            // 
            this.infoJunctionId.DisplayLabel = "Junction Id:";
            this.infoJunctionId.DisplayText = "";
            this.infoJunctionId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoJunctionId.Location = new System.Drawing.Point(0, 44);
            this.infoJunctionId.Name = "infoJunctionId";
            this.infoJunctionId.Size = new System.Drawing.Size(284, 22);
            this.infoJunctionId.TabIndex = 2;
            // 
            // infoStartId
            // 
            this.infoStartId.DisplayLabel = "Start Id:";
            this.infoStartId.DisplayText = "";
            this.infoStartId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStartId.Location = new System.Drawing.Point(0, 66);
            this.infoStartId.Name = "infoStartId";
            this.infoStartId.Size = new System.Drawing.Size(284, 22);
            this.infoStartId.TabIndex = 3;
            // 
            // infoEndId1
            // 
            this.infoEndId1.DisplayLabel = "End Id 1:";
            this.infoEndId1.DisplayText = "";
            this.infoEndId1.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId1.Location = new System.Drawing.Point(0, 88);
            this.infoEndId1.Name = "infoEndId1";
            this.infoEndId1.Size = new System.Drawing.Size(284, 22);
            this.infoEndId1.TabIndex = 4;
            // 
            // infoEndId2
            // 
            this.infoEndId2.DisplayLabel = "End Id 2:";
            this.infoEndId2.DisplayText = "";
            this.infoEndId2.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId2.Location = new System.Drawing.Point(0, 110);
            this.infoEndId2.Name = "infoEndId2";
            this.infoEndId2.Size = new System.Drawing.Size(284, 22);
            this.infoEndId2.TabIndex = 5;
            // 
            // infoEndId3
            // 
            this.infoEndId3.DisplayLabel = "End Id 3:";
            this.infoEndId3.DisplayText = "";
            this.infoEndId3.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId3.Location = new System.Drawing.Point(0, 132);
            this.infoEndId3.Name = "infoEndId3";
            this.infoEndId3.Size = new System.Drawing.Size(284, 22);
            this.infoEndId3.TabIndex = 6;
            // 
            // infoEndId4
            // 
            this.infoEndId4.DisplayLabel = "End Id 4:";
            this.infoEndId4.DisplayText = "";
            this.infoEndId4.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId4.Location = new System.Drawing.Point(0, 154);
            this.infoEndId4.Name = "infoEndId4";
            this.infoEndId4.Size = new System.Drawing.Size(284, 22);
            this.infoEndId4.TabIndex = 7;
            // 
            // infoEndId5
            // 
            this.infoEndId5.DisplayLabel = "End Id 5:";
            this.infoEndId5.DisplayText = "";
            this.infoEndId5.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId5.Location = new System.Drawing.Point(0, 176);
            this.infoEndId5.Name = "infoEndId5";
            this.infoEndId5.Size = new System.Drawing.Size(284, 22);
            this.infoEndId5.TabIndex = 8;
            // 
            // infoEndId6
            // 
            this.infoEndId6.DisplayLabel = "End Id 6:";
            this.infoEndId6.DisplayText = "";
            this.infoEndId6.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId6.Location = new System.Drawing.Point(0, 198);
            this.infoEndId6.Name = "infoEndId6";
            this.infoEndId6.Size = new System.Drawing.Size(284, 22);
            this.infoEndId6.TabIndex = 9;
            // 
            // infoEndId7
            // 
            this.infoEndId7.DisplayLabel = "End Id 7:";
            this.infoEndId7.DisplayText = "";
            this.infoEndId7.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId7.Location = new System.Drawing.Point(0, 220);
            this.infoEndId7.Name = "infoEndId7";
            this.infoEndId7.Size = new System.Drawing.Size(284, 22);
            this.infoEndId7.TabIndex = 10;
            // 
            // infoEndId8
            // 
            this.infoEndId8.DisplayLabel = "End Id 8:";
            this.infoEndId8.DisplayText = "";
            this.infoEndId8.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId8.Location = new System.Drawing.Point(0, 242);
            this.infoEndId8.Name = "infoEndId8";
            this.infoEndId8.Size = new System.Drawing.Size(284, 22);
            this.infoEndId8.TabIndex = 11;
            // 
            // infoEndId9
            // 
            this.infoEndId9.DisplayLabel = "End Id 9:";
            this.infoEndId9.DisplayText = "";
            this.infoEndId9.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId9.Location = new System.Drawing.Point(0, 264);
            this.infoEndId9.Name = "infoEndId9";
            this.infoEndId9.Size = new System.Drawing.Size(284, 22);
            this.infoEndId9.TabIndex = 12;
            // 
            // infoEndId10
            // 
            this.infoEndId10.DisplayLabel = "End Id 10:";
            this.infoEndId10.DisplayText = "";
            this.infoEndId10.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoEndId10.Location = new System.Drawing.Point(0, 286);
            this.infoEndId10.Name = "infoEndId10";
            this.infoEndId10.Size = new System.Drawing.Size(284, 22);
            this.infoEndId10.TabIndex = 13;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 308);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(284, 22);
            this.infoCreatedBy.TabIndex = 14;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 330);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(284, 22);
            this.infoDateCreated.TabIndex = 15;
            // 
            // StreetRestrictionPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoEndId10);
            this.Controls.Add(this.infoEndId9);
            this.Controls.Add(this.infoEndId8);
            this.Controls.Add(this.infoEndId7);
            this.Controls.Add(this.infoEndId6);
            this.Controls.Add(this.infoEndId5);
            this.Controls.Add(this.infoEndId4);
            this.Controls.Add(this.infoEndId3);
            this.Controls.Add(this.infoEndId2);
            this.Controls.Add(this.infoEndId1);
            this.Controls.Add(this.infoStartId);
            this.Controls.Add(this.infoJunctionId);
            this.Controls.Add(this.infoNaviStatus);
            this.Controls.Add(this.infoId);
            this.Name = "StreetRestrictionPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoId;
        private InfoBox infoNaviStatus;
        private InfoBox infoJunctionId;
        private InfoBox infoStartId;
        private InfoBox infoEndId1;
        private InfoBox infoEndId2;
        private InfoBox infoEndId3;
        private InfoBox infoEndId4;
        private InfoBox infoEndId5;
        private InfoBox infoEndId6;
        private InfoBox infoEndId7;
        private InfoBox infoEndId8;
        private InfoBox infoEndId9;
        private InfoBox infoEndId10;
        private InfoBox infoCreatedBy;
        private InfoBox infoDateCreated;
    }
}

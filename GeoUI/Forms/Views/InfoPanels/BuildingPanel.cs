﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class BuildingPanel : InfoPanel
    {
        public BuildingPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GBuilding building = feature as GBuilding;
            if (building == null)
            {
                return;
            }
            infoId.DisplayText = building.OID.ToString();
            infoName.DisplayText = building.Name;
            //infoName2.DisplayText = building.Name2;
            infoStreetType.DisplayText = building.StreetTypeValue;
            infoStreetName.DisplayText = building.StreetName;
            //infoStreetName2.DisplayText = building.StreetName2;
            //infoSection.DisplayText = building.Section;
            //infoPostcode.DisplayText = building.Postcode;
            //infoCity.DisplayText = building.City;
            //infoState.DisplayText = building.State;
            infoCreatedBy.DisplayText = building.CreatedBy;
            infoDateCreated.DisplayText = building.DateCreated;
            infoUpdatedBy.DisplayText = building.UpdatedBy;
            infoDateUpdated.DisplayText = building.DateUpdated;
            infoPropertyId.DisplayText = building.PropertyId.ToString();
            infoGroupId.DisplayText = building.GroupId.ToString();
            infoX.DisplayText = building.X.ToString();
            infoY.DisplayText = building.Y.ToString();
            infoFloorNumUnit.DisplayText = building.Unit.ToString();
        }
    }
}

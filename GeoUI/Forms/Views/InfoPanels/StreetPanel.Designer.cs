﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class StreetPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName2 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoSection = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoPostcode = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCity = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoState = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUpdatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateUpdated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoNetworkClass = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoClass = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCategory = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoFilterLevel = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoNaviStatus = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoSubCity = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDirection = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoSpeedLimit = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoLanes = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoFromNode = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoToNode = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(267, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoType
            // 
            this.infoType.DisplayLabel = "Type:";
            this.infoType.DisplayText = "";
            this.infoType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoType.Location = new System.Drawing.Point(0, 198);
            this.infoType.Name = "infoType";
            this.infoType.Size = new System.Drawing.Size(267, 22);
            this.infoType.TabIndex = 9;
            // 
            // infoName
            // 
            this.infoName.DisplayLabel = "Name:";
            this.infoName.DisplayText = "";
            this.infoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName.Location = new System.Drawing.Point(0, 220);
            this.infoName.Name = "infoName";
            this.infoName.Size = new System.Drawing.Size(267, 22);
            this.infoName.TabIndex = 10;
            // 
            // infoName2
            // 
            this.infoName2.DisplayLabel = "Name2:";
            this.infoName2.DisplayText = "";
            this.infoName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName2.Location = new System.Drawing.Point(0, 242);
            this.infoName2.Name = "infoName2";
            this.infoName2.Size = new System.Drawing.Size(267, 22);
            this.infoName2.TabIndex = 11;
            // 
            // infoSection
            // 
            this.infoSection.DisplayLabel = "Section:";
            this.infoSection.DisplayText = "";
            this.infoSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoSection.Location = new System.Drawing.Point(0, 264);
            this.infoSection.Name = "infoSection";
            this.infoSection.Size = new System.Drawing.Size(267, 22);
            this.infoSection.TabIndex = 12;
            // 
            // infoPostcode
            // 
            this.infoPostcode.DisplayLabel = "Postcode:";
            this.infoPostcode.DisplayText = "";
            this.infoPostcode.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoPostcode.Location = new System.Drawing.Point(0, 286);
            this.infoPostcode.Name = "infoPostcode";
            this.infoPostcode.Size = new System.Drawing.Size(267, 22);
            this.infoPostcode.TabIndex = 13;
            // 
            // infoCity
            // 
            this.infoCity.DisplayLabel = "City:";
            this.infoCity.DisplayText = "";
            this.infoCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCity.Location = new System.Drawing.Point(0, 308);
            this.infoCity.Name = "infoCity";
            this.infoCity.Size = new System.Drawing.Size(267, 22);
            this.infoCity.TabIndex = 14;
            // 
            // infoState
            // 
            this.infoState.DisplayLabel = "State:";
            this.infoState.DisplayText = "";
            this.infoState.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoState.Location = new System.Drawing.Point(0, 352);
            this.infoState.Name = "infoState";
            this.infoState.Size = new System.Drawing.Size(267, 22);
            this.infoState.TabIndex = 16;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 374);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(267, 22);
            this.infoCreatedBy.TabIndex = 17;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 396);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(267, 22);
            this.infoDateCreated.TabIndex = 18;
            // 
            // infoUpdatedBy
            // 
            this.infoUpdatedBy.DisplayLabel = "Updated By:";
            this.infoUpdatedBy.DisplayText = "";
            this.infoUpdatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUpdatedBy.Location = new System.Drawing.Point(0, 418);
            this.infoUpdatedBy.Name = "infoUpdatedBy";
            this.infoUpdatedBy.Size = new System.Drawing.Size(267, 22);
            this.infoUpdatedBy.TabIndex = 19;
            // 
            // infoDateUpdated
            // 
            this.infoDateUpdated.DisplayLabel = "Date Updated:";
            this.infoDateUpdated.DisplayText = "";
            this.infoDateUpdated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateUpdated.Location = new System.Drawing.Point(0, 440);
            this.infoDateUpdated.Name = "infoDateUpdated";
            this.infoDateUpdated.Size = new System.Drawing.Size(267, 22);
            this.infoDateUpdated.TabIndex = 20;
            // 
            // infoNetworkClass
            // 
            this.infoNetworkClass.DisplayLabel = "Network Class:";
            this.infoNetworkClass.DisplayText = "";
            this.infoNetworkClass.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoNetworkClass.Location = new System.Drawing.Point(0, 44);
            this.infoNetworkClass.Name = "infoNetworkClass";
            this.infoNetworkClass.Size = new System.Drawing.Size(267, 22);
            this.infoNetworkClass.TabIndex = 2;
            // 
            // infoClass
            // 
            this.infoClass.DisplayLabel = "Class:";
            this.infoClass.DisplayText = "";
            this.infoClass.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoClass.Location = new System.Drawing.Point(0, 66);
            this.infoClass.Name = "infoClass";
            this.infoClass.Size = new System.Drawing.Size(267, 22);
            this.infoClass.TabIndex = 3;
            // 
            // infoCategory
            // 
            this.infoCategory.DisplayLabel = "Category:";
            this.infoCategory.DisplayText = "";
            this.infoCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCategory.Location = new System.Drawing.Point(0, 88);
            this.infoCategory.Name = "infoCategory";
            this.infoCategory.Size = new System.Drawing.Size(267, 22);
            this.infoCategory.TabIndex = 4;
            // 
            // infoFilterLevel
            // 
            this.infoFilterLevel.DisplayLabel = "Filter Level:";
            this.infoFilterLevel.DisplayText = "";
            this.infoFilterLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoFilterLevel.Location = new System.Drawing.Point(0, 110);
            this.infoFilterLevel.Name = "infoFilterLevel";
            this.infoFilterLevel.Size = new System.Drawing.Size(267, 22);
            this.infoFilterLevel.TabIndex = 5;
            // 
            // infoNaviStatus
            // 
            this.infoNaviStatus.DisplayLabel = "Navigation Status:";
            this.infoNaviStatus.DisplayText = "";
            this.infoNaviStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoNaviStatus.Location = new System.Drawing.Point(0, 22);
            this.infoNaviStatus.Name = "infoNaviStatus";
            this.infoNaviStatus.Size = new System.Drawing.Size(267, 22);
            this.infoNaviStatus.TabIndex = 1;
            // 
            // infoSubCity
            // 
            this.infoSubCity.DisplayLabel = "Sub City:";
            this.infoSubCity.DisplayText = "";
            this.infoSubCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoSubCity.Location = new System.Drawing.Point(0, 330);
            this.infoSubCity.Name = "infoSubCity";
            this.infoSubCity.Size = new System.Drawing.Size(267, 22);
            this.infoSubCity.TabIndex = 15;
            // 
            // infoDirection
            // 
            this.infoDirection.DisplayLabel = "Direction:";
            this.infoDirection.DisplayText = "";
            this.infoDirection.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDirection.Location = new System.Drawing.Point(0, 132);
            this.infoDirection.Name = "infoDirection";
            this.infoDirection.Size = new System.Drawing.Size(267, 22);
            this.infoDirection.TabIndex = 6;
            // 
            // infoSpeedLimit
            // 
            this.infoSpeedLimit.DisplayLabel = "Speed Limit:";
            this.infoSpeedLimit.DisplayText = "";
            this.infoSpeedLimit.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoSpeedLimit.Location = new System.Drawing.Point(0, 154);
            this.infoSpeedLimit.Name = "infoSpeedLimit";
            this.infoSpeedLimit.Size = new System.Drawing.Size(267, 22);
            this.infoSpeedLimit.TabIndex = 7;
            // 
            // infoLanes
            // 
            this.infoLanes.DisplayLabel = "No of Lanes:";
            this.infoLanes.DisplayText = "";
            this.infoLanes.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoLanes.Location = new System.Drawing.Point(0, 176);
            this.infoLanes.Name = "infoLanes";
            this.infoLanes.Size = new System.Drawing.Size(267, 22);
            this.infoLanes.TabIndex = 8;
            // 
            // infoFromNode
            // 
            this.infoFromNode.DisplayLabel = "From Node:";
            this.infoFromNode.DisplayText = "";
            this.infoFromNode.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoFromNode.Location = new System.Drawing.Point(0, 462);
            this.infoFromNode.Name = "infoFromNode";
            this.infoFromNode.Size = new System.Drawing.Size(267, 22);
            this.infoFromNode.TabIndex = 21;
            // 
            // infoToNode
            // 
            this.infoToNode.DisplayLabel = "To Node:";
            this.infoToNode.DisplayText = "";
            this.infoToNode.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoToNode.Location = new System.Drawing.Point(0, 484);
            this.infoToNode.Name = "infoToNode";
            this.infoToNode.Size = new System.Drawing.Size(267, 22);
            this.infoToNode.TabIndex = 22;
            // 
            // StreetPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoToNode);
            this.Controls.Add(this.infoFromNode);
            this.Controls.Add(this.infoDateUpdated);
            this.Controls.Add(this.infoUpdatedBy);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoState);
            this.Controls.Add(this.infoSubCity);
            this.Controls.Add(this.infoCity);
            this.Controls.Add(this.infoPostcode);
            this.Controls.Add(this.infoSection);
            this.Controls.Add(this.infoName2);
            this.Controls.Add(this.infoName);
            this.Controls.Add(this.infoType);
            this.Controls.Add(this.infoLanes);
            this.Controls.Add(this.infoSpeedLimit);
            this.Controls.Add(this.infoDirection);
            this.Controls.Add(this.infoFilterLevel);
            this.Controls.Add(this.infoCategory);
            this.Controls.Add(this.infoClass);
            this.Controls.Add(this.infoNetworkClass);
            this.Controls.Add(this.infoNaviStatus);
            this.Controls.Add(this.infoId);
            this.Name = "StreetPanel";
            this.Size = new System.Drawing.Size(267, 480);
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoId;
        private InfoBox infoType;
        private InfoBox infoName;
        private InfoBox infoName2;
        private InfoBox infoSection;
        private InfoBox infoPostcode;
        private InfoBox infoCity;
        private InfoBox infoState;
        private InfoBox infoCreatedBy;
        private InfoBox infoDateCreated;
        private InfoBox infoUpdatedBy;
        private InfoBox infoDateUpdated;
        private InfoBox infoNetworkClass;
        private InfoBox infoClass;
        private InfoBox infoCategory;
        private InfoBox infoFilterLevel;
        private InfoBox infoNaviStatus;
        private InfoBox infoSubCity;
        private InfoBox infoDirection;
        private InfoBox infoSpeedLimit;
        private InfoBox infoLanes;
        private InfoBox infoFromNode;
        private InfoBox infoToNode;
    }
}

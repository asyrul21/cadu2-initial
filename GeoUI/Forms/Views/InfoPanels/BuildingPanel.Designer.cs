﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class BuildingPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoDateUpdated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUpdatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoPropertyId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoGroupId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoX = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoY = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoFloorNumUnit = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoDateUpdated
            // 
            this.infoDateUpdated.DisplayLabel = "Date Updated:";
            this.infoDateUpdated.DisplayText = "";
            this.infoDateUpdated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateUpdated.Location = new System.Drawing.Point(0, 154);
            this.infoDateUpdated.Name = "infoDateUpdated";
            this.infoDateUpdated.Size = new System.Drawing.Size(267, 22);
            this.infoDateUpdated.TabIndex = 13;
            // 
            // infoUpdatedBy
            // 
            this.infoUpdatedBy.DisplayLabel = "Updated By:";
            this.infoUpdatedBy.DisplayText = "";
            this.infoUpdatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUpdatedBy.Location = new System.Drawing.Point(0, 132);
            this.infoUpdatedBy.Name = "infoUpdatedBy";
            this.infoUpdatedBy.Size = new System.Drawing.Size(267, 22);
            this.infoUpdatedBy.TabIndex = 12;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 110);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(267, 22);
            this.infoDateCreated.TabIndex = 11;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 88);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(267, 22);
            this.infoCreatedBy.TabIndex = 10;
            // 
            // infoName
            // 
            this.infoName.DisplayLabel = "Name:";
            this.infoName.DisplayText = "";
            this.infoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName.Location = new System.Drawing.Point(0, 22);
            this.infoName.Name = "infoName";
            this.infoName.Size = new System.Drawing.Size(267, 22);
            this.infoName.TabIndex = 1;
            // 
            // infoStreetType
            // 
            this.infoStreetType.DisplayLabel = "Street Type:";
            this.infoStreetType.DisplayText = "";
            this.infoStreetType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetType.Location = new System.Drawing.Point(0, 44);
            this.infoStreetType.Name = "infoStreetType";
            this.infoStreetType.Size = new System.Drawing.Size(267, 22);
            this.infoStreetType.TabIndex = 3;
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(267, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoStreetName
            // 
            this.infoStreetName.DisplayLabel = "Street Name:";
            this.infoStreetName.DisplayText = "";
            this.infoStreetName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetName.Location = new System.Drawing.Point(0, 66);
            this.infoStreetName.Name = "infoStreetName";
            this.infoStreetName.Size = new System.Drawing.Size(267, 22);
            this.infoStreetName.TabIndex = 4;
            // 
            // infoPropertyId
            // 
            this.infoPropertyId.DisplayLabel = "Property Id:";
            this.infoPropertyId.DisplayText = "";
            this.infoPropertyId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoPropertyId.Location = new System.Drawing.Point(0, 176);
            this.infoPropertyId.Name = "infoPropertyId";
            this.infoPropertyId.Size = new System.Drawing.Size(267, 22);
            this.infoPropertyId.TabIndex = 16;
            // 
            // infoGroupId
            // 
            this.infoGroupId.DisplayLabel = "Group Id:";
            this.infoGroupId.DisplayText = "";
            this.infoGroupId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoGroupId.Location = new System.Drawing.Point(0, 198);
            this.infoGroupId.Name = "infoGroupId";
            this.infoGroupId.Size = new System.Drawing.Size(267, 22);
            this.infoGroupId.TabIndex = 17;
            // 
            // infoX
            // 
            this.infoX.DisplayLabel = "X:";
            this.infoX.DisplayText = "";
            this.infoX.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoX.Location = new System.Drawing.Point(0, 220);
            this.infoX.Name = "infoX";
            this.infoX.Size = new System.Drawing.Size(267, 22);
            this.infoX.TabIndex = 18;
            // 
            // infoY
            // 
            this.infoY.DisplayLabel = "Y:";
            this.infoY.DisplayText = "";
            this.infoY.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoY.Location = new System.Drawing.Point(0, 242);
            this.infoY.Name = "infoY";
            this.infoY.Size = new System.Drawing.Size(267, 22);
            this.infoY.TabIndex = 19;
            // 
            // infoFloorNumUnit
            // 
            this.infoFloorNumUnit.DisplayLabel = "Floor Num Unit:";
            this.infoFloorNumUnit.DisplayText = "";
            this.infoFloorNumUnit.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoFloorNumUnit.Location = new System.Drawing.Point(0, 264);
            this.infoFloorNumUnit.Name = "infoFloorNumUnit";
            this.infoFloorNumUnit.Size = new System.Drawing.Size(267, 22);
            this.infoFloorNumUnit.TabIndex = 20;
            // 
            // BuildingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoFloorNumUnit);
            this.Controls.Add(this.infoY);
            this.Controls.Add(this.infoX);
            this.Controls.Add(this.infoGroupId);
            this.Controls.Add(this.infoPropertyId);
            this.Controls.Add(this.infoDateUpdated);
            this.Controls.Add(this.infoUpdatedBy);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoStreetName);
            this.Controls.Add(this.infoStreetType);
            this.Controls.Add(this.infoName);
            this.Controls.Add(this.infoId);
            this.Name = "BuildingPanel";
            this.Size = new System.Drawing.Size(267, 398);
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoDateUpdated;
        private InfoBox infoUpdatedBy;
        private InfoBox infoDateCreated;
        private InfoBox infoCreatedBy;
        private InfoBox infoName;
        private InfoBox infoStreetType;
        private InfoBox infoId;
        private InfoBox infoStreetName;
        private InfoBox infoPropertyId;
        private InfoBox infoGroupId;
        private InfoBox infoX;
        private InfoBox infoY;
        private InfoBox infoFloorNumUnit;
    }
}

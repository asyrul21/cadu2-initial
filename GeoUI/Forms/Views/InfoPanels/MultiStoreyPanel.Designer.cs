﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class MultiStoreyPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoY = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoX = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoY
            // 
            this.infoY.DisplayLabel = "Y:";
            this.infoY.DisplayText = "";
            this.infoY.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoY.Location = new System.Drawing.Point(0, 22);
            this.infoY.Name = "infoY";
            this.infoY.Size = new System.Drawing.Size(284, 22);
            this.infoY.TabIndex = 1;
            // 
            // infoX
            // 
            this.infoX.DisplayLabel = "X:";
            this.infoX.DisplayText = "";
            this.infoX.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoX.Location = new System.Drawing.Point(0, 0);
            this.infoX.Name = "infoX";
            this.infoX.Size = new System.Drawing.Size(284, 22);
            this.infoX.TabIndex = 0;
            // 
            // MultiStoreyPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoY);
            this.Controls.Add(this.infoX);
            this.Name = "MultiStoreyPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoY;
        private InfoBox infoX;
    }
}

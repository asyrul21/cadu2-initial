﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class JunctionANDPanel : InfoPanel
    {
        public JunctionANDPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GJunctionAND junctionAND = feature as GJunctionAND;
            if (junctionAND == null)
            {
                return;
            }
            infoId.DisplayText = junctionAND.OriId.ToString();
            infoType.DisplayText = junctionAND.TypeValue;
            infoName.DisplayText = junctionAND.Name;
            infoCreatedBy.DisplayText = junctionAND.CreatedBy;
            infoDateCreated.DisplayText = junctionAND.DateCreated;
            infoUpdatedBy.DisplayText = junctionAND.UpdatedBy;
            infoDateUpdated.DisplayText = junctionAND.DateUpdated;
            infoX.DisplayText = junctionAND.X.ToString();
            infoY.DisplayText = junctionAND.Y.ToString();
        }
    }
}

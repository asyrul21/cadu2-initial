﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class FloorPanel : InfoPanel
    {
        public FloorPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GFloor floor = feature as GFloor;
            if (floor == null)
            {
                return;
            }

            infoX.DisplayText = floor.X.ToString();
            infoY.DisplayText = floor.Y.ToString();
        }
    }
}

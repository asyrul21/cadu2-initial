﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class PropertyPanel : InfoPanel
    {
        public PropertyPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GProperty property = feature as GProperty;
            if (property == null)
            {
                return;
            }
            infoId.DisplayText = property.OID.ToString();
            infoType.DisplayText = property.TypeValue;
            infoLot.DisplayText = property.Lot;
            infoHouse.DisplayText = property.House;
            infoStreetType.DisplayText = property.StreetTypeValue;
            infoStreetName.DisplayText = property.StreetName;
            infoStreetName2.DisplayText = property.StreetName2;
            infoSection.DisplayText = property.Section;
            infoPostcode.DisplayText = property.Postcode;
            infoCity.DisplayText = property.City;
            infoState.DisplayText = property.State;
            infoCreatedBy.DisplayText = property.CreatedBy;
            infoDateCreated.DisplayText = property.DateCreated;
            infoUpdatedBy.DisplayText = property.UpdatedBy;
            infoDateUpdated.DisplayText = property.DateUpdated;
            infoX.DisplayText = property.X.ToString();
            infoY.DisplayText = property.Y.ToString();
            infoStreetId.DisplayText = property.StreetId.ToString();
        }
    }
}

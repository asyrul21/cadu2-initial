﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public class InfoPanelFactory
    {
        public static IInfoPanel CreatePanel(IGFeature feature)
        {
            if (feature is GStreet)
            {
                return new StreetPanel();
            }
            if (feature is GJunction)
            {
                return new JunctionPanel();
            }
            if (feature is GProperty)
            {
                return new PropertyPanel();
            }
            if (feature is GBuilding)
            {
                return new BuildingPanel();
            }
            if (feature is GBuildingGroup)
            {
                return new BuildingGroupPanel();
            }
            if (feature is GLandmark)
            {
                return new LandmarkPanel();
            }
            if (feature is GPoi)
            {
                return new PoiPanel();
            }
            if (feature is GStreetRestriction)
            {
                return new StreetRestrictionPanel();
            }
            if (feature is GFloor)
            {
                return new FloorPanel();
            }
            if (feature is GMultiStorey)
            {
                return new MultiStoreyPanel();
            }
            // added by noraini ali on 14 Jan 2019
            if (feature is Core.Features.GRegion)
            {
                return new RegionPanel();
            }
            // end added

            // noraini - add AND Feature
            if (feature is Core.Features.GWorkArea)
            {
                return new WorkAreaPanel();
            } 
            if (feature is GPropertyAND)
            {
                return new PropertyANDPanel();
            }
            if (feature is GLandmarkAND)
            {
                return new LandmarkANDPanel();
            }
            if (feature is GBuildingAND)
            {
                return new BuildingANDPanel();
            }
            if (feature is GStreetAND)
            {
                return new StreetANDPanel();
            }
            if (feature is GJunctionAND)
            {
                return new JunctionANDPanel();
            }
            if (feature is GBuildingGroupAND)
            {
                return new BuildingGroupANDPanel();
            }
            else
            {
                return new InfoPanel();
            }

        }
    }
}

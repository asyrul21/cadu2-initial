﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class LandmarkPanel : InfoPanel
    {
        public LandmarkPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GLandmark landmark = feature as GLandmark;
            if (landmark == null)
            {
                return;
            }
            infoId.DisplayText = landmark.OID.ToString();
            infoName.DisplayText = landmark.Name;
            infoName2.DisplayText = landmark.Name2;
            infoStreetType.DisplayText = landmark.StreetTypeValue;
            infoStreetName.DisplayText = landmark.StreetName;
            infoStreetName2.DisplayText = landmark.StreetName2;
            infoSection.DisplayText = landmark.Section;
            infoPostcode.DisplayText = landmark.Postcode;
            infoCity.DisplayText = landmark.City;
            infoState.DisplayText = landmark.State;
            infoCreatedBy.DisplayText = landmark.CreatedBy;
            infoDateCreated.DisplayText = landmark.DateCreated;
            infoUpdatedBy.DisplayText = landmark.UpdatedBy;
            infoDateUpdated.DisplayText = landmark.DateUpdated;
            infoX.DisplayText = landmark.X.ToString();
            infoY.DisplayText = landmark.Y.ToString();
            infoStreetId.DisplayText = landmark.StreetId.ToString();
        }
    }
}

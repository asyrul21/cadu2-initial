﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class InfoPanel : UserControl, IInfoPanel
    {
        public InfoPanel()
        {
            InitializeComponent();
        }

        public virtual void LoadInfo(IGFeature feature)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class StreetANDPanel : InfoPanel
    {
        public StreetANDPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GStreetAND street = feature as GStreetAND;
            if (street == null)
            {
                return;
            }
            infoId.DisplayText = street.OriId.ToString();
            infoNaviStatus.DisplayText = street.NavigationStatusValue;
            infoNetworkClass.DisplayText = street.NetworkClassValue;
            infoClass.DisplayText = street.ClassValue;
            infoCategory.DisplayText = street.CategoryValue;
            infoFilterLevel.DisplayText = street.FilterLevelValue;
            infoDirection.DisplayText = street.DirectionValue;
            infoSpeedLimit.DisplayText = street.SpeedLimit.HasValue ? street.SpeedLimit.Value.ToString() : string.Empty;
            infoLanes.DisplayText = street.NumOfLanes.HasValue ? street.NumOfLanes.Value.ToString() : string.Empty;
            infoType.DisplayText = street.TypeValue;
            infoName.DisplayText = street.Name;
            infoName2.DisplayText = street.Name2;
            infoSection.DisplayText = street.Section;
            infoPostcode.DisplayText = street.Postcode;
            infoCity.DisplayText = street.City;
            infoSubCity.DisplayText = street.SubCity;
            infoState.DisplayText = street.State;
            infoCreatedBy.DisplayText = street.CreatedBy;
            infoDateCreated.DisplayText = street.DateCreated;
            infoUpdatedBy.DisplayText = street.UpdatedBy;
            infoDateUpdated.DisplayText = street.DateUpdated;
            infoWorkArea.DisplayText = street.AreaId.ToString();
            infoFromNode.DisplayText = street.FromNodeId.ToString();
            infoToNode.DisplayText = street.ToNodeId.ToString();
        }
    }
}

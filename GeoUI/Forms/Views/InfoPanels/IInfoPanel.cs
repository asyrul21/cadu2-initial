﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public interface IInfoPanel
    {
        void LoadInfo(IGFeature feature);
        DockStyle Dock { set; get; }
    }
}

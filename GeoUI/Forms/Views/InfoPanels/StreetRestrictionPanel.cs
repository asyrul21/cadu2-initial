﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class StreetRestrictionPanel : InfoPanel
    {
        public StreetRestrictionPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GStreetRestriction restriction = feature as GStreetRestriction;
            if (restriction == null)
            {
                return;
            }
            infoId.DisplayText = restriction.OID.ToString();
            infoNaviStatus.DisplayText = restriction.NavigationStatusValue;
            infoJunctionId.DisplayText = restriction.JunctionId.ToString();
            infoStartId.DisplayText = restriction.StartId.ToString();
            infoEndId1.DisplayText = restriction.EndId1.ToString();
            infoEndId2.DisplayText = restriction.EndId2.ToString();
            infoEndId3.DisplayText = restriction.EndId3.ToString();
            infoEndId4.DisplayText = restriction.EndId4.ToString();
            infoEndId5.DisplayText = restriction.EndId5.ToString();
            infoEndId6.DisplayText = restriction.EndId6.ToString();
            infoEndId7.DisplayText = restriction.EndId7.ToString();
            infoEndId8.DisplayText = restriction.EndId8.ToString();
            infoEndId9.DisplayText = restriction.EndId9.ToString();
            infoEndId10.DisplayText = restriction.EndId10.ToString();
            infoCreatedBy.DisplayText = restriction.CreatedBy;
            infoDateCreated.DisplayText = restriction.DateCreated;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class MultiStoreyPanel : InfoPanel
    {
        public MultiStoreyPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GMultiStorey multiStorey = feature as GMultiStorey;
            if (multiStorey == null)
            {
                return;
            }

            infoX.DisplayText = multiStorey.X.ToString();
            infoY.DisplayText = multiStorey.Y.ToString();
        }
    }
}

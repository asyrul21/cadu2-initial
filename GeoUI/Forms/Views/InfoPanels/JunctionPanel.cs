﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class JunctionPanel : InfoPanel
    {
        public JunctionPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GJunction junction = feature as GJunction;
            if (junction == null)
            {
                return;
            }
            infoId.DisplayText = junction.OID.ToString();
            infoType.DisplayText = junction.TypeValue;
            infoName.DisplayText = junction.Name;
            infoCreatedBy.DisplayText = junction.CreatedBy;
            infoDateCreated.DisplayText = junction.DateCreated;
            infoUpdatedBy.DisplayText = junction.UpdatedBy;
            infoDateUpdated.DisplayText = junction.DateUpdated;
            infoX.DisplayText = junction.X.ToString();
            infoY.DisplayText = junction.Y.ToString();
        }
    }
}

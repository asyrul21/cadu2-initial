﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class WorkAreaPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUserName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoAreaSegment = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoFlag = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateModified = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoName
            // 
            this.infoName.DisplayLabel = "Work Order No:";
            this.infoName.DisplayText = "";
            this.infoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName.Location = new System.Drawing.Point(0, 22);
            this.infoName.Name = "infoName";
            this.infoName.Size = new System.Drawing.Size(284, 22);
            this.infoName.TabIndex = 2;
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(284, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoUserName
            // 
            this.infoUserName.DisplayLabel = "Username:";
            this.infoUserName.DisplayText = "";
            this.infoUserName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUserName.Location = new System.Drawing.Point(0, 44);
            this.infoUserName.Name = "infoUserName";
            this.infoUserName.Size = new System.Drawing.Size(284, 22);
            this.infoUserName.TabIndex = 3;
            // 
            // infoAreaSegment
            // 
            this.infoAreaSegment.DisplayLabel = "Area Segment:";
            this.infoAreaSegment.DisplayText = "";
            this.infoAreaSegment.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoAreaSegment.Location = new System.Drawing.Point(0, 66);
            this.infoAreaSegment.Name = "infoAreaSegment";
            this.infoAreaSegment.Size = new System.Drawing.Size(284, 22);
            this.infoAreaSegment.TabIndex = 4;
            // 
            // infoFlag
            // 
            this.infoFlag.DisplayLabel = "Complete Flag";
            this.infoFlag.DisplayText = "";
            this.infoFlag.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoFlag.Location = new System.Drawing.Point(0, 88);
            this.infoFlag.Name = "infoFlag";
            this.infoFlag.Size = new System.Drawing.Size(284, 22);
            this.infoFlag.TabIndex = 5;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created :";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 110);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(284, 22);
            this.infoDateCreated.TabIndex = 6;
            // 
            // infoDateModified
            // 
            this.infoDateModified.DisplayLabel = "Date Modified :";
            this.infoDateModified.DisplayText = "";
            this.infoDateModified.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateModified.Location = new System.Drawing.Point(0, 132);
            this.infoDateModified.Name = "infoDateModified";
            this.infoDateModified.Size = new System.Drawing.Size(284, 22);
            this.infoDateModified.TabIndex = 7;
            // 
            // WorkAreaPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoDateModified);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoFlag);
            this.Controls.Add(this.infoAreaSegment);
            this.Controls.Add(this.infoUserName);
            this.Controls.Add(this.infoName);
            this.Controls.Add(this.infoId);
            this.Name = "WorkAreaPanel";
            this.ResumeLayout(false);

        }

        #endregion
        private InfoBox infoName;
        private InfoBox infoId;
        private InfoBox infoUserName;
        private InfoBox infoAreaSegment;
        private InfoBox infoFlag;
        private InfoBox infoDateCreated;
        private InfoBox infoDateModified;
    }
}

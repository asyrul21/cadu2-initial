﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class BuildingANDPanel : InfoPanel
    {
        public BuildingANDPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GBuildingAND buildingAND = feature as GBuildingAND;
            if (buildingAND == null)
            {
                return;
            }
            infoId.DisplayText = buildingAND.OriId.ToString();
            infoName.DisplayText = buildingAND.Name;
            //infoName2.DisplayText = buildingAND.Name2;
            infoStreetType.DisplayText = buildingAND.StreetTypeValue;
            infoStreetName.DisplayText = buildingAND.StreetName;
            //infoStreetName2.DisplayText = buildingAND.StreetName2;
            //infoSection.DisplayText = buildingAND.Section;
            //infoPostcode.DisplayText = buildingAND.Postcode;
            //infoCity.DisplayText = buildingAND.City;
            //infoState.DisplayText = buildingAND.State;
            infoCreatedBy.DisplayText = buildingAND.CreatedBy;
            infoDateCreated.DisplayText = buildingAND.DateCreated;
            infoUpdatedBy.DisplayText = buildingAND.UpdatedBy;
            infoDateUpdated.DisplayText = buildingAND.DateUpdated;
            infoX.DisplayText = buildingAND.X.ToString();
            infoY.DisplayText = buildingAND.Y.ToString();
            infoPropertyId.DisplayText = buildingAND.PropertyId.ToString();
            infoGroupId.DisplayText = buildingAND.GroupId.ToString();
            infoWorkArea.DisplayText = buildingAND.AreaId.ToString();
            infoFloorNumUnit.DisplayText = buildingAND.Unit.ToString();
        }
    }
}

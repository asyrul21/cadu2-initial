﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class RegionPanel : InfoPanel
    {
        public RegionPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GRegion region = feature as GRegion;
            if (region == null)
            {
                return;
            }
            infoId.DisplayText = region.OID.ToString();
            infoName.DisplayText = region.index;
        }
    }
}

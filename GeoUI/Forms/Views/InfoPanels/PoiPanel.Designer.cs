﻿namespace Geomatic.UI.Forms.Views.InfoPanels
{
    partial class PoiPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoDateUpdated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoUpdatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoDateCreated = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCreatedBy = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoState = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoCity = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoPostcode = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoSection = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetName2 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoStreetType = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName2 = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoName = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoId = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoY = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.infoX = new Geomatic.UI.Forms.Views.InfoPanels.InfoBox();
            this.SuspendLayout();
            // 
            // infoDateUpdated
            // 
            this.infoDateUpdated.DisplayLabel = "Date Updated:";
            this.infoDateUpdated.DisplayText = "";
            this.infoDateUpdated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateUpdated.Location = new System.Drawing.Point(0, 286);
            this.infoDateUpdated.Name = "infoDateUpdated";
            this.infoDateUpdated.Size = new System.Drawing.Size(284, 22);
            this.infoDateUpdated.TabIndex = 13;
            // 
            // infoUpdatedBy
            // 
            this.infoUpdatedBy.DisplayLabel = "Updated By:";
            this.infoUpdatedBy.DisplayText = "";
            this.infoUpdatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoUpdatedBy.Location = new System.Drawing.Point(0, 264);
            this.infoUpdatedBy.Name = "infoUpdatedBy";
            this.infoUpdatedBy.Size = new System.Drawing.Size(284, 22);
            this.infoUpdatedBy.TabIndex = 12;
            // 
            // infoDateCreated
            // 
            this.infoDateCreated.DisplayLabel = "Date Created:";
            this.infoDateCreated.DisplayText = "";
            this.infoDateCreated.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoDateCreated.Location = new System.Drawing.Point(0, 242);
            this.infoDateCreated.Name = "infoDateCreated";
            this.infoDateCreated.Size = new System.Drawing.Size(284, 22);
            this.infoDateCreated.TabIndex = 11;
            // 
            // infoCreatedBy
            // 
            this.infoCreatedBy.DisplayLabel = "Created By:";
            this.infoCreatedBy.DisplayText = "";
            this.infoCreatedBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCreatedBy.Location = new System.Drawing.Point(0, 220);
            this.infoCreatedBy.Name = "infoCreatedBy";
            this.infoCreatedBy.Size = new System.Drawing.Size(284, 22);
            this.infoCreatedBy.TabIndex = 10;
            // 
            // infoState
            // 
            this.infoState.DisplayLabel = "State:";
            this.infoState.DisplayText = "";
            this.infoState.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoState.Location = new System.Drawing.Point(0, 198);
            this.infoState.Name = "infoState";
            this.infoState.Size = new System.Drawing.Size(284, 22);
            this.infoState.TabIndex = 9;
            // 
            // infoCity
            // 
            this.infoCity.DisplayLabel = "City:";
            this.infoCity.DisplayText = "";
            this.infoCity.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoCity.Location = new System.Drawing.Point(0, 176);
            this.infoCity.Name = "infoCity";
            this.infoCity.Size = new System.Drawing.Size(284, 22);
            this.infoCity.TabIndex = 8;
            // 
            // infoPostcode
            // 
            this.infoPostcode.DisplayLabel = "Postcode:";
            this.infoPostcode.DisplayText = "";
            this.infoPostcode.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoPostcode.Location = new System.Drawing.Point(0, 154);
            this.infoPostcode.Name = "infoPostcode";
            this.infoPostcode.Size = new System.Drawing.Size(284, 22);
            this.infoPostcode.TabIndex = 7;
            // 
            // infoSection
            // 
            this.infoSection.DisplayLabel = "Section:";
            this.infoSection.DisplayText = "";
            this.infoSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoSection.Location = new System.Drawing.Point(0, 132);
            this.infoSection.Name = "infoSection";
            this.infoSection.Size = new System.Drawing.Size(284, 22);
            this.infoSection.TabIndex = 6;
            // 
            // infoStreetName2
            // 
            this.infoStreetName2.DisplayLabel = "Street Name2:";
            this.infoStreetName2.DisplayText = "";
            this.infoStreetName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetName2.Location = new System.Drawing.Point(0, 110);
            this.infoStreetName2.Name = "infoStreetName2";
            this.infoStreetName2.Size = new System.Drawing.Size(284, 22);
            this.infoStreetName2.TabIndex = 5;
            // 
            // infoStreetName
            // 
            this.infoStreetName.DisplayLabel = "Street Name:";
            this.infoStreetName.DisplayText = "";
            this.infoStreetName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetName.Location = new System.Drawing.Point(0, 88);
            this.infoStreetName.Name = "infoStreetName";
            this.infoStreetName.Size = new System.Drawing.Size(284, 22);
            this.infoStreetName.TabIndex = 4;
            // 
            // infoStreetType
            // 
            this.infoStreetType.DisplayLabel = "Street Type:";
            this.infoStreetType.DisplayText = "";
            this.infoStreetType.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoStreetType.Location = new System.Drawing.Point(0, 66);
            this.infoStreetType.Name = "infoStreetType";
            this.infoStreetType.Size = new System.Drawing.Size(284, 22);
            this.infoStreetType.TabIndex = 3;
            // 
            // infoName2
            // 
            this.infoName2.DisplayLabel = "Name2:";
            this.infoName2.DisplayText = "";
            this.infoName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName2.Location = new System.Drawing.Point(0, 44);
            this.infoName2.Name = "infoName2";
            this.infoName2.Size = new System.Drawing.Size(284, 22);
            this.infoName2.TabIndex = 2;
            // 
            // infoName
            // 
            this.infoName.DisplayLabel = "Name:";
            this.infoName.DisplayText = "";
            this.infoName.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoName.Location = new System.Drawing.Point(0, 22);
            this.infoName.Name = "infoName";
            this.infoName.Size = new System.Drawing.Size(284, 22);
            this.infoName.TabIndex = 1;
            // 
            // infoId
            // 
            this.infoId.DisplayLabel = "Id:";
            this.infoId.DisplayText = "";
            this.infoId.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoId.Location = new System.Drawing.Point(0, 0);
            this.infoId.Name = "infoId";
            this.infoId.Size = new System.Drawing.Size(284, 22);
            this.infoId.TabIndex = 0;
            // 
            // infoY
            // 
            this.infoY.DisplayLabel = "Y:";
            this.infoY.DisplayText = "";
            this.infoY.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoY.Location = new System.Drawing.Point(0, 330);
            this.infoY.Name = "infoY";
            this.infoY.Size = new System.Drawing.Size(284, 22);
            this.infoY.TabIndex = 15;
            // 
            // infoX
            // 
            this.infoX.DisplayLabel = "X:";
            this.infoX.DisplayText = "";
            this.infoX.Dock = System.Windows.Forms.DockStyle.Top;
            this.infoX.Location = new System.Drawing.Point(0, 308);
            this.infoX.Name = "infoX";
            this.infoX.Size = new System.Drawing.Size(284, 22);
            this.infoX.TabIndex = 14;
            // 
            // PoiPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.infoY);
            this.Controls.Add(this.infoX);
            this.Controls.Add(this.infoDateUpdated);
            this.Controls.Add(this.infoUpdatedBy);
            this.Controls.Add(this.infoDateCreated);
            this.Controls.Add(this.infoCreatedBy);
            this.Controls.Add(this.infoState);
            this.Controls.Add(this.infoCity);
            this.Controls.Add(this.infoPostcode);
            this.Controls.Add(this.infoSection);
            this.Controls.Add(this.infoStreetName2);
            this.Controls.Add(this.infoStreetName);
            this.Controls.Add(this.infoStreetType);
            this.Controls.Add(this.infoName2);
            this.Controls.Add(this.infoName);
            this.Controls.Add(this.infoId);
            this.Name = "PoiPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private InfoBox infoDateUpdated;
        private InfoBox infoUpdatedBy;
        private InfoBox infoDateCreated;
        private InfoBox infoCreatedBy;
        private InfoBox infoState;
        private InfoBox infoCity;
        private InfoBox infoPostcode;
        private InfoBox infoSection;
        private InfoBox infoStreetName2;
        private InfoBox infoStreetName;
        private InfoBox infoStreetType;
        private InfoBox infoName2;
        private InfoBox infoName;
        private InfoBox infoId;
        private InfoBox infoY;
        private InfoBox infoX;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class LandmarkANDPanel : InfoPanel
    {
        public LandmarkANDPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            if (!(feature is GLandmarkAND landmarkAND))
            {
                return;
            }
            infoId.DisplayText = landmarkAND.OriId.ToString();
            infoName.DisplayText = landmarkAND.Name;
            infoName2.DisplayText = landmarkAND.Name2;
            infoStreetType.DisplayText = landmarkAND.StreetTypeValue;
            infoStreetName.DisplayText = landmarkAND.StreetName;
            infoStreetName2.DisplayText = landmarkAND.StreetName2;
            infoSection.DisplayText = landmarkAND.Section;
            infoPostcode.DisplayText = landmarkAND.Postcode;
            infoCity.DisplayText = landmarkAND.City;
            infoState.DisplayText = landmarkAND.State;
            infoCreatedBy.DisplayText = landmarkAND.CreatedBy;
            infoDateCreated.DisplayText = landmarkAND.DateCreated;
            infoUpdatedBy.DisplayText = landmarkAND.UpdatedBy;
            infoDateUpdated.DisplayText = landmarkAND.DateUpdated;
            infoX.DisplayText = landmarkAND.X.ToString();
            infoY.DisplayText = landmarkAND.Y.ToString();
            infoStreetId.DisplayText = landmarkAND.StreetId.ToString();
            infoWorkArea.DisplayText = landmarkAND.AreaId.ToString();
        }
    }
}

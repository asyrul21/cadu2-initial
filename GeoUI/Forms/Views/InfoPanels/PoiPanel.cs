﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Features.JoinedFeatures;

namespace Geomatic.UI.Forms.Views.InfoPanels
{
    public partial class PoiPanel : InfoPanel
    {
        public PoiPanel()
        {
            InitializeComponent();
        }

        public override void LoadInfo(IGFeature feature)
        {
            GPoi poi = feature as GPoi;
            if (poi == null)
            {
                return;
            }
            infoId.DisplayText = poi.OID.ToString();
            infoName.DisplayText = poi.Name;
            infoName2.DisplayText = poi.Name2;
            infoStreetType.DisplayText = poi.StreetTypeValue;
            infoStreetName.DisplayText = poi.StreetName;
            infoStreetName2.DisplayText = poi.StreetName2;
            infoSection.DisplayText = poi.Section;
            infoPostcode.DisplayText = poi.Postcode;
            infoCity.DisplayText = poi.City;
            infoState.DisplayText = poi.State;
            infoCreatedBy.DisplayText = poi.CreatedBy;
            infoDateCreated.DisplayText = poi.DateCreated;
            infoUpdatedBy.DisplayText = poi.UpdatedBy;
            infoDateUpdated.DisplayText = poi.DateUpdated;
            infoX.DisplayText = poi.X.ToString();
            infoY.DisplayText = poi.Y.ToString();
        }
    }
}

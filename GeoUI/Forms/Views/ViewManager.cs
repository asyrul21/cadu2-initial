﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.UI.Forms.Views
{
    public class ViewManager : IDisposable
    {
        private Dictionary<Type, IView> _views;
        private Form _host;
        private DockPanel _dockPanel;

        public ViewManager(Form host, DockPanel dockPanel)
        {
            _views = new Dictionary<Type, IView>();
            _host = host;
            _dockPanel = dockPanel;
        }

        public IView GetView<T>() where T : IView
        {
            string key = typeof(T).ToString();
            IView view;
            if (_views.ContainsKey(typeof(T)))
            {
                _views.TryGetValue(typeof(T), out view);
            }
            else
            {
                view = ViewFactory.Create<T>();
                _views.Add(typeof(T), view);
            }
            return view;
        }

        public IEnumerable<IView> GetViews()
        {
            return _views.Values;
        }

        public void OnActiveDocumentChanged(MapDocument document)
        {
            foreach (IView view in GetViews())
            {
                view.OnActiveDocumentChanged(document);
            }
        }

        public void Dispose()
        {
            _views.Clear();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.Documents;
using Geomatic.Core.Features;
using Geomatic.UI.Forms.Views.InfoPanels;

namespace Geomatic.UI.Forms.Views
{
    public partial class SelectionsView : View
    {
        private MapDocument _mapDocument;

        public SelectionsView()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            RefreshTitle("Selections");
        }

        public override void OnActiveDocumentChanged(MapDocument document)
        {
            _mapDocument = document;
        }

        public void OnSelectionChanged(List<IGFeature> features)
        {
            lbSelections.BeginUpdate();
            try
            {
                lbSelections.Items.Clear();
                lbSelections.Items.AddRange(features.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                if (lbSelections.Items.Count == 0)
                {
                    panelInfo.Controls.Clear();
                }
                else if (lbSelections.Items.Count > 0)
                {
                    lbSelections.SelectedIndex = 0;
                }
                lbSelections.EndUpdate();
            }
        }

        private void lbSelections_SelectedIndexChanged(object sender, EventArgs e)
        {
            IGFeature feature = lbSelections.SelectedItem as IGFeature;
            if (feature == null)
            {
                return;
            }
            using (new WaitCursor())
            {
                IInfoPanel infoPanel = InfoPanelFactory.CreatePanel(feature);
                infoPanel.LoadInfo(feature);
                infoPanel.Dock = DockStyle.Fill;
                panelInfo.Controls.Clear();
                panelInfo.Controls.Add((Control)infoPanel);
            }
        }

        private void lbSelections_DoubleClick(object sender, EventArgs e)
        {
            IGFeature feature = lbSelections.SelectedItem as IGFeature;
            if (feature == null)
            {
                return;
            }
            if (_mapDocument != null)
            {
                _mapDocument.ZoomToFeature(feature);
            }
        }
    }
}

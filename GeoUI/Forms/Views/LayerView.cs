﻿using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Controls;
using Geomatic.UI.Forms.Documents;
using Geomatic.UI.Forms.Views.LayerViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;


namespace Geomatic.UI.Forms.Views
{

    public partial class LayerView : View
    {
        public event EventHandler<SelectedLayerChangedEventArgs> SelectedLayerChanged;
        private GeoMapDocument _mapDocument;

        //added by asyrul
        private IEnumerable<ILayer> defaultLayers
        {
            get
            {
                return _mapDocument.GetLayers();
            }
        }

        //added end

        private ITOCControl2 TocControl
        {
            get
            {
                return axTOCControl.Object as ITOCControl2;
            }
        }

        private ILayer SelectedLayer
        {
            get
            {
                IBasicMap map = new MapClass();
                ILayer layer = new FeatureLayerClass();
                object legendGroup = new object();
                object index = new object();
                esriTOCControlItem item = new esriTOCControlItem();
                TocControl.GetSelectedItem(ref item, ref map, ref layer, ref legendGroup, ref index);
                return layer;
            }
        }

        public LayerView()
        {
            InitializeComponent();
            RefreshTitle("Layer");
        }

        protected override void Form_Load()
        {
            OnSelectedLayerChanged(new SelectedLayerChangedEventArgs(SelectedLayer));
        }

        public override void OnActiveDocumentChanged(GeoMapDocument document)
        {
            TocControl.SetBuddyControl((document == null) ? null : document.MapControl);
            _mapDocument = document;
            OnSelectedLayerChanged(new SelectedLayerChangedEventArgs(SelectedLayer));
        }

        private void axTOCControl_OnMouseUp(object sender, ITOCControlEvents_OnMouseUpEvent e)
        {
            OnSelectedLayerChanged(new SelectedLayerChangedEventArgs(SelectedLayer));
            if (e.button == MouseKey.Right)
            {
                if (SelectedLayer == null)
                {
                    return;
                }
                contextMenu.Show(this, e.x, e.y);
            }
        }

        private void OnMoveUp(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.MoveLayerUp(SelectedLayer);
            TocControl.Update();
        }

        private void OnMoveDown(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.MoveLayerDown(SelectedLayer);
            TocControl.Update();
        }

        private void OnMoveToTop(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.MoveLayerToTop(SelectedLayer);
            TocControl.Update();
        }

        private void OnMoveToBottom(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.MoveLayerToBottom(SelectedLayer);
            TocControl.Update();
        }

        private void OnZoomToLayer(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (SelectedLayer.AreaOfInterest == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.SetExtent(SelectedLayer.AreaOfInterest.Envelope);
        }

        private void OnRemoveLayer(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            if (_mapDocument == null)
            {
                return;
            }
            _mapDocument.DeleteLayer(SelectedLayer);
            OnSelectedLayerChanged(new SelectedLayerChangedEventArgs(SelectedLayer));
        }

        private void OnProperties(object sender, EventArgs e)
        {
            if (SelectedLayer == null)
            {
                return;
            }
            using (LayerPropertyForm form = new LayerPropertyForm())
            {
                form.SetLayer(SelectedLayer);
                form.ShowDialog(this);
            }
            _mapDocument.RefreshMap();
        }

        private void showDefaultLayers()
        {
            Console.WriteLine("Printing default layers visibility...");
            foreach (ILayer layer in this.defaultLayers)
            {
                Console.WriteLine(layer.Name + " : " + layer.Visible);
            }
        }

        //added by asyrul
        private bool toggleOn = false;
        private IEnumerable<ILayer> newLayers;
        private void OnSelectAllLayer(object sender, EventArgs e)
        {
            if (_mapDocument == null)
            {
                return;
            }
            showDefaultLayers();

            if (!toggleOn)
            {
                newLayers = defaultLayers;
                foreach (ILayer layer in newLayers)
                {
                    layer.Visible = true;
                }
                this.toggleOn = true;
            }
            else
            {
                foreach (ILayer layer in newLayers)
                {
                    if(layer.Name != "State" && layer.Name != "Selections")
                    {
                        layer.Visible = false;
                    }
                    //layer.Visible = false;
                }
                //newLayers = defaultLayers;
                this.toggleOn = false;
            }


            TocControl.Update();
            //_mapDocument.Update();
            _mapDocument.RefreshMap();
        }

        //added end

        private void OnSelectedLayerChanged(SelectedLayerChangedEventArgs e)
        {
            bool enable = e.SelectedLayer != null;
            btnMoveUp.Enabled = enable;
            btnMoveDown.Enabled = enable;
            btnMoveToTop.Enabled = enable;
            btnMoveToBottom.Enabled = enable;
            btnZoomToLayer.Enabled = enable;
            btnRemove.Enabled = enable;
            menuZoomToLayer.Enabled = enable;
            menuRemove.Enabled = enable;
            menuProperties.Enabled = enable;

            if (SelectedLayerChanged != null)
                SelectedLayerChanged(this, e);
        }
    }
}

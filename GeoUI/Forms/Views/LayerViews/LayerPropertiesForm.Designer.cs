﻿namespace Geomatic.UI.Forms.Views.LayerViews
{
    partial class LayerPropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.trackTransparency = new System.Windows.Forms.TrackBar();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.intTransparency = new Geomatic.UI.Controls.IntUpDown();
            this.lblTransparency = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackTransparency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTransparency)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(158, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // trackTransparency
            // 
            this.trackTransparency.Location = new System.Drawing.Point(339, 12);
            this.trackTransparency.Maximum = 100;
            this.trackTransparency.Name = "trackTransparency";
            this.trackTransparency.Size = new System.Drawing.Size(150, 45);
            this.trackTransparency.TabIndex = 4;
            this.trackTransparency.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackTransparency.ValueChanged += new System.EventHandler(this.trackTransparency_ValueChanged);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(396, 227);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(477, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // intTransparency
            // 
            this.intTransparency.Location = new System.Drawing.Point(495, 13);
            this.intTransparency.Name = "intTransparency";
            this.intTransparency.Size = new System.Drawing.Size(57, 20);
            this.intTransparency.TabIndex = 5;
            this.intTransparency.Value = 0;
            this.intTransparency.ValueChanged += new System.EventHandler(this.intTransparency_ValueChanged);
            // 
            // lblTransparency
            // 
            this.lblTransparency.AutoSize = true;
            this.lblTransparency.Location = new System.Drawing.Point(258, 15);
            this.lblTransparency.Name = "lblTransparency";
            this.lblTransparency.Size = new System.Drawing.Size(75, 13);
            this.lblTransparency.TabIndex = 3;
            this.lblTransparency.Text = "Transparency:";
            // 
            // LayerPropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 262);
            this.Controls.Add(this.lblTransparency);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.intTransparency);
            this.Controls.Add(this.trackTransparency);
            this.Controls.Add(this.comboBox1);
            this.Name = "LayerPropertiesForm";
            this.Text = "LayerPropertiesForm";
            this.Load += new System.EventHandler(this.LayerPropertiesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackTransparency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTransparency)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TrackBar trackTransparency;
        private Controls.IntUpDown intTransparency;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTransparency;
    }
}
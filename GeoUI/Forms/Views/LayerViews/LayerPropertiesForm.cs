﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;

namespace Geomatic.UI.Forms.Views.LayerViews
{
    public partial class LayerPropertiesForm : BaseForm
    {
        private ILayer _layer;

        private IFeatureLayer FeatureLayer
        {
            get
            {
                return _layer as IFeatureLayer;
            }
        }

        private IGeoFeatureLayer GeoFeatureLayer
        {
            get
            {
                return _layer as IGeoFeatureLayer;
            }
        }

        private IFeatureRenderer Renderer
        {
            get
            {
                return (GeoFeatureLayer == null) ? null : GeoFeatureLayer.Renderer;
            }
        }

        private ILayerEffects LayerEffects
        {
            get
            {
                return FeatureLayer as ILayerEffects;
            }
        }

        public LayerPropertiesForm()
        {
            InitializeComponent();
            RefreshTitle("Layer Properties");
            AcceptButton = btnApply;
        }

        public void SetLayer(ILayer layer)
        {
            _layer = layer;
        }

        private void LayerPropertiesForm_Load(object sender, EventArgs e)
        {
            if (Renderer == null)
            {
                return;
            }

            ILayerEffects layerEffects = LayerEffects;
            if (layerEffects.SupportsTransparency)
            {
                trackTransparency.Value = layerEffects.Transparency;
            }
            else
            {
                intTransparency.Enabled = false;
                trackTransparency.Enabled = false;
            }

            IFeatureRenderer renderer = Renderer;
            if (renderer is ISimpleRenderer)
            {
                ISimpleRenderer simpleRenderer = (ISimpleRenderer)renderer;
                ISymbol symbol = simpleRenderer.Symbol;
                if (symbol is ISimpleMarkerSymbol)
                {
                    ISimpleMarkerSymbol simpleMarkerSymbol = (ISimpleMarkerSymbol)symbol;

                }
                else if (symbol is ISimpleLineSymbol)
                {

                }
                else if (symbol is SimpleFillSymbolClass)
                {

                }
            }
            else if (renderer is IUniqueValueRenderer)
            {
                IUniqueValueRenderer uniqueValueRenderer = (IUniqueValueRenderer)renderer;
                //ISymbol symbol = uniqueValueRenderer.Symbol;

            }
            else
            {

            }
        }

        private void trackTransparency_ValueChanged(object sender, EventArgs e)
        {
            if (intTransparency.Value != trackTransparency.Value)
            {
                intTransparency.Value = trackTransparency.Value;
            }
        }

        private void intTransparency_ValueChanged(object sender, EventArgs e)
        {
            if (trackTransparency.Value != intTransparency.Value)
            {
                trackTransparency.Value = intTransparency.Value;
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                LayerEffects.Transparency = short.Parse(trackTransparency.Value.ToString());
            }
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

﻿using ESRI.ArcGIS.Carto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Views.LayerViews
{
    public partial class LayerPropertyForm : BaseForm
    {
        public LayerPropertyForm()
        {
            InitializeComponent();
        }

        private ILayer _layer;

        private ILayerEffects LayerEffects
        {
            get
            {
                return _layer as ILayerEffects;   //casting
            }
        }

        public void SetLayer(ILayer layer)
        {
            _layer = layer;
            LayerNameTextBox.Text = layer.Name;
            ILayerEffects layerEffects = LayerEffects;
            if (layerEffects.SupportsTransparency)
            {
                TransparencyInt.Value = LayerEffects.Transparency;
                TransparencyTrack.Value = LayerEffects.Transparency;
            }
        }


        private void TransparencyTrack_Scroll(object sender, EventArgs e)
        {
            if (TransparencyInt.Value != TransparencyTrack.Value)
            {
                TransparencyInt.Value = TransparencyTrack.Value;
            }
        }

        private void TransparencyInt_ValueChanged(object sender, EventArgs e)
        {
            if (TransparencyTrack.Value != TransparencyInt.Value)
            {
                TransparencyTrack.Value = TransparencyInt.Value;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            ILayerEffects layerEffects = LayerEffects;
            if (layerEffects.SupportsTransparency)
            {
                using (new WaitCursor())
                {
                    LayerEffects.Transparency = short.Parse(TransparencyTrack.Value.ToString());
                }
                DialogResult = DialogResult.OK;
            }
        }
    }
}

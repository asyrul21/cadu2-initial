﻿namespace Geomatic.UI.Forms.Views.LayerViews
{
    partial class LayerPropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransparencyTrack = new System.Windows.Forms.TrackBar();
            this.TransparencyLabel = new System.Windows.Forms.Label();
            this.TransparencyInt = new Geomatic.UI.Controls.IntUpDown();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.RasterNameLabel = new System.Windows.Forms.Label();
            this.LayerNameTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TransparencyTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransparencyInt)).BeginInit();
            this.SuspendLayout();
            // 
            // TransparencyTrack
            // 
            this.TransparencyTrack.Location = new System.Drawing.Point(82, 52);
            this.TransparencyTrack.Maximum = 100;
            this.TransparencyTrack.Name = "TransparencyTrack";
            this.TransparencyTrack.Size = new System.Drawing.Size(150, 45);
            this.TransparencyTrack.TabIndex = 5;
            this.TransparencyTrack.TickStyle = System.Windows.Forms.TickStyle.None;
            this.TransparencyTrack.Scroll += new System.EventHandler(this.TransparencyTrack_Scroll);
            // 
            // TransparencyLabel
            // 
            this.TransparencyLabel.AutoSize = true;
            this.TransparencyLabel.Location = new System.Drawing.Point(13, 54);
            this.TransparencyLabel.Name = "TransparencyLabel";
            this.TransparencyLabel.Size = new System.Drawing.Size(75, 13);
            this.TransparencyLabel.TabIndex = 6;
            this.TransparencyLabel.Text = "Transparency:";
            // 
            // TransparencyInt
            // 
            this.TransparencyInt.Location = new System.Drawing.Point(233, 52);
            this.TransparencyInt.Name = "TransparencyInt";
            this.TransparencyInt.Size = new System.Drawing.Size(57, 20);
            this.TransparencyInt.TabIndex = 7;
            this.TransparencyInt.Value = 0;
            this.TransparencyInt.ValueChanged += new System.EventHandler(this.TransparencyInt_ValueChanged);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(135, 89);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 8;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.Location = new System.Drawing.Point(216, 89);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 9;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // RasterNameLabel
            // 
            this.RasterNameLabel.AutoSize = true;
            this.RasterNameLabel.Location = new System.Drawing.Point(12, 19);
            this.RasterNameLabel.Name = "RasterNameLabel";
            this.RasterNameLabel.Size = new System.Drawing.Size(67, 13);
            this.RasterNameLabel.TabIndex = 10;
            this.RasterNameLabel.Text = "Layer Name:";
            // 
            // LayerNameTextBox
            // 
            this.LayerNameTextBox.Location = new System.Drawing.Point(90, 16);
            this.LayerNameTextBox.Name = "LayerNameTextBox";
            this.LayerNameTextBox.ReadOnly = true;
            this.LayerNameTextBox.Size = new System.Drawing.Size(202, 20);
            this.LayerNameTextBox.TabIndex = 11;
            // 
            // LayerPropertyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 124);
            this.Controls.Add(this.LayerNameTextBox);
            this.Controls.Add(this.RasterNameLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.TransparencyInt);
            this.Controls.Add(this.TransparencyLabel);
            this.Controls.Add(this.TransparencyTrack);
            this.Name = "LayerPropertyForm";
            this.Text = "LayerPropertyForm";
            ((System.ComponentModel.ISupportInitialize)(this.TransparencyTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransparencyInt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar TransparencyTrack;
        private System.Windows.Forms.Label TransparencyLabel;
        private Controls.IntUpDown TransparencyInt;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label RasterNameLabel;
        private System.Windows.Forms.TextBox LayerNameTextBox;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geomatic.UI.Forms.Views
{
    internal class ViewFactory
    {
        public static T Create<T>() where T : IView
        {
            return Activator.CreateInstance<T>();
        }
    }
}

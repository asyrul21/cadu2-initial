﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.UI.Forms.Views
{
    public partial class MiniMapView : View
    {
        public MiniMapView()
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            RefreshTitle("Mini Map");
        }

        public override void OnActiveDocumentChanged(MapDocument document)
        {

        }
    }
}

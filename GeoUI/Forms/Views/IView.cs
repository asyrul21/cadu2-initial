﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.UI.Forms.Views
{
    public interface IView
    {
        void OnActiveDocumentChanged(MapDocument document);
        void Show(DockPanel dockPanel);
        void Activate();
        void Hide();
    }
}

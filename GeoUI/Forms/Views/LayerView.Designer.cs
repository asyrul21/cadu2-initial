﻿namespace Geomatic.UI.Forms.Views
{
    partial class LayerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator separator01;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayerView));
            this.axTOCControl = new ESRI.ArcGIS.Controls.AxTOCControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuZoomToLayer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.menuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.btnMoveUp = new System.Windows.Forms.ToolStripButton();
            this.btnMoveDown = new System.Windows.Forms.ToolStripButton();
            this.btnMoveToTop = new System.Windows.Forms.ToolStripButton();
            this.btnMoveToBottom = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnZoomToLayer = new System.Windows.Forms.ToolStripButton();
            this.btnRemove = new System.Windows.Forms.ToolStripButton();
            this.selectAllLayer = new System.Windows.Forms.ToolStripButton();
            separator01 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.axTOCControl)).BeginInit();
            this.contextMenu.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // separator01
            // 
            separator01.Name = "separator01";
            separator01.Size = new System.Drawing.Size(151, 6);
            // 
            // axTOCControl
            // 
            this.axTOCControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axTOCControl.Location = new System.Drawing.Point(0, 25);
            this.axTOCControl.Name = "axTOCControl";
            this.axTOCControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTOCControl.OcxState")));
            this.axTOCControl.Size = new System.Drawing.Size(284, 237);
            this.axTOCControl.TabIndex = 1;
            this.axTOCControl.OnMouseUp += new ESRI.ArcGIS.Controls.ITOCControlEvents_Ax_OnMouseUpEventHandler(this.axTOCControl_OnMouseUp);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuZoomToLayer,
            this.menuRemove,
            separator01,
            this.menuProperties});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(155, 76);
            // 
            // menuZoomToLayer
            // 
            this.menuZoomToLayer.Image = ((System.Drawing.Image)(resources.GetObject("menuZoomToLayer.Image")));
            this.menuZoomToLayer.Name = "menuZoomToLayer";
            this.menuZoomToLayer.Size = new System.Drawing.Size(154, 22);
            this.menuZoomToLayer.Text = "Zoom To Layer";
            this.menuZoomToLayer.Click += new System.EventHandler(this.OnZoomToLayer);
            // 
            // menuRemove
            // 
            this.menuRemove.Image = ((System.Drawing.Image)(resources.GetObject("menuRemove.Image")));
            this.menuRemove.Name = "menuRemove";
            this.menuRemove.Size = new System.Drawing.Size(154, 22);
            this.menuRemove.Text = "Remove";
            this.menuRemove.Click += new System.EventHandler(this.OnRemoveLayer);
            // 
            // menuProperties
            // 
            this.menuProperties.Name = "menuProperties";
            this.menuProperties.Size = new System.Drawing.Size(154, 22);
            this.menuProperties.Text = "Properties";
            this.menuProperties.Click += new System.EventHandler(this.OnProperties);
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMoveUp,
            this.btnMoveDown,
            this.btnMoveToTop,
            this.btnMoveToBottom,
            this.toolStripSeparator1,
            this.btnZoomToLayer,
            this.btnRemove,
            this.selectAllLayer});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(284, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip";
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveUp.Image")));
            this.btnMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(23, 22);
            this.btnMoveUp.Text = "Move Up";
            this.btnMoveUp.Click += new System.EventHandler(this.OnMoveUp);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveDown.Image")));
            this.btnMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(23, 22);
            this.btnMoveDown.Text = "Move Down";
            this.btnMoveDown.Click += new System.EventHandler(this.OnMoveDown);
            // 
            // btnMoveToTop
            // 
            this.btnMoveToTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveToTop.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveToTop.Image")));
            this.btnMoveToTop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveToTop.Name = "btnMoveToTop";
            this.btnMoveToTop.Size = new System.Drawing.Size(23, 22);
            this.btnMoveToTop.Text = "Move To Top";
            this.btnMoveToTop.Click += new System.EventHandler(this.OnMoveToTop);
            // 
            // btnMoveToBottom
            // 
            this.btnMoveToBottom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveToBottom.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveToBottom.Image")));
            this.btnMoveToBottom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveToBottom.Name = "btnMoveToBottom";
            this.btnMoveToBottom.Size = new System.Drawing.Size(23, 22);
            this.btnMoveToBottom.Text = "Move To Bottom";
            this.btnMoveToBottom.Click += new System.EventHandler(this.OnMoveToBottom);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnZoomToLayer
            // 
            this.btnZoomToLayer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnZoomToLayer.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomToLayer.Image")));
            this.btnZoomToLayer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoomToLayer.Name = "btnZoomToLayer";
            this.btnZoomToLayer.Size = new System.Drawing.Size(23, 22);
            this.btnZoomToLayer.Text = "Zoom To Layer";
            this.btnZoomToLayer.Click += new System.EventHandler(this.OnZoomToLayer);
            // 
            // btnRemove
            // 
            this.btnRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(23, 22);
            this.btnRemove.Text = "Remove";
            this.btnRemove.Click += new System.EventHandler(this.OnRemoveLayer);
            // 
            // selectAllLayer
            // 
            this.selectAllLayer.CheckOnClick = true;
            this.selectAllLayer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.selectAllLayer.Image = ((System.Drawing.Image)(resources.GetObject("selectAllLayer.Image")));
            this.selectAllLayer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAllLayer.Name = "selectAllLayer";
            this.selectAllLayer.Size = new System.Drawing.Size(23, 22);
            this.selectAllLayer.Text = "Select All";
            this.selectAllLayer.Click += new System.EventHandler(this.OnSelectAllLayer);
            // 
            // LayerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.axTOCControl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LayerView";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockLeftAutoHide;
            this.Text = "LayerView";
            ((System.ComponentModel.ISupportInitialize)(this.axTOCControl)).EndInit();
            this.contextMenu.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private ESRI.ArcGIS.Controls.AxTOCControl axTOCControl;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem menuRemove;
        private System.Windows.Forms.ToolStripMenuItem menuZoomToLayer;
        private System.Windows.Forms.ToolStripMenuItem menuProperties;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton btnMoveUp;
        private System.Windows.Forms.ToolStripButton btnMoveDown;
        private System.Windows.Forms.ToolStripButton btnMoveToTop;
        private System.Windows.Forms.ToolStripButton btnMoveToBottom;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnZoomToLayer;
        private System.Windows.Forms.ToolStripButton btnRemove;
        private System.Windows.Forms.ToolStripButton selectAllLayer;
    }
}
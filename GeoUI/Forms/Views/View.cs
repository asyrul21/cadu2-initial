﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.UI.Forms.Views
{
    public partial class View : DockContent, IView
    {
        public View()
        {
            InitializeComponent();
        }

        private void View_Load(object sender, EventArgs e)
        {
            Form_Load();
        }

        protected virtual void Form_Load()
        {
            // do nothing
        }

        public virtual void OnActiveDocumentChanged(MapDocument document)
        {
            throw new Exception("View not handled on document changed.");
        }

        public void RefreshTitle(string title)
        {
            Text = title;
        }

        public void RefreshTitle(string format, params object[] args)
        {
            RefreshTitle(string.Format(format, args));
        }
    }
}

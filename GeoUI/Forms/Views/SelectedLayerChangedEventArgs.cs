﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;

namespace Geomatic.UI.Forms.Views
{
    public class SelectedLayerChangedEventArgs : EventArgs
    {
        public ILayer SelectedLayer { private set; get; }

        public SelectedLayerChangedEventArgs(ILayer selectedLayer)
        {
            SelectedLayer = selectedLayer;
        }
    }
}

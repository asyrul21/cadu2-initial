﻿namespace Geomatic.UI.Forms
{
    partial class StreetANDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.updatesLayout = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.relationLayout = new System.Windows.Forms.TableLayoutPanel();
            this.txtToId = new System.Windows.Forms.TextBox();
            this.txtFromId = new System.Windows.Forms.TextBox();
            this.lblToId = new System.Windows.Forms.Label();
            this.lblFromId = new System.Windows.Forms.Label();
            this.usageGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.usageLayout2 = new System.Windows.Forms.TableLayoutPanel();
            this.chkCar = new System.Windows.Forms.CheckBox();
            this.chkBus = new System.Windows.Forms.CheckBox();
            this.chkBicycle = new System.Windows.Forms.CheckBox();
            this.chkDelivery = new System.Windows.Forms.CheckBox();
            this.chkTruck = new System.Windows.Forms.CheckBox();
            this.chkEmergency = new System.Windows.Forms.CheckBox();
            this.chkPedestrian = new System.Windows.Forms.CheckBox();
            this.usageLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lblLanes = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblElevation = new System.Windows.Forms.Label();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblMaxWeight = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblSpeedLimit = new System.Windows.Forms.Label();
            this.numLanes = new Geomatic.UI.Controls.IntUpDown();
            this.numElevation = new Geomatic.UI.Controls.IntUpDown();
            this.numSpeedLimit = new Geomatic.UI.Controls.IntUpDown();
            this.numWeight = new Geomatic.UI.Controls.DoubleUpDown();
            this.numLength = new Geomatic.UI.Controls.DoubleUpDown();
            this.numWidth = new Geomatic.UI.Controls.DoubleUpDown();
            this.numHeight = new Geomatic.UI.Controls.DoubleUpDown();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.addressLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lblSubCity = new System.Windows.Forms.Label();
            this.txtSubCity = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.btnPickLocation = new System.Windows.Forms.Button();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.ckShowText = new System.Windows.Forms.CheckBox();
            this.generalLayout = new System.Windows.Forms.TableLayoutPanel();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.cbDirection = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.cbClass = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNetworkClass = new Geomatic.UI.Controls.UpperComboBox();
            this.lblConstructionStatus = new System.Windows.Forms.Label();
            this.lblDirection = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblNetworkClass = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.cbStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cbCategory = new Geomatic.UI.Controls.UpperComboBox();
            this.lblFilterLevel = new System.Windows.Forms.Label();
            this.cbFilterLevel = new Geomatic.UI.Controls.UpperComboBox();
            this.lblDesign = new System.Windows.Forms.Label();
            this.cbDesign = new Geomatic.UI.Controls.UpperComboBox();
            this.lblTollType = new System.Windows.Forms.Label();
            this.cbTollType = new Geomatic.UI.Controls.UpperComboBox();
            this.lblDivider = new System.Windows.Forms.Label();
            this.cbDivider = new Geomatic.UI.Controls.UpperComboBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.updatesLayout.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.relationLayout.SuspendLayout();
            this.usageGroup.SuspendLayout();
            this.usageLayout2.SuspendLayout();
            this.usageLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLanes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numElevation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeedLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            this.addressGroup.SuspendLayout();
            this.addressLayout.SuspendLayout();
            this.generalLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 621);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.updatesLayout);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 551);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 11;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // updatesLayout
            // 
            this.updatesLayout.ColumnCount = 4;
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.updatesLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.Controls.Add(this.txtDateUpdated, 3, 1);
            this.updatesLayout.Controls.Add(this.lblCreator, 0, 0);
            this.updatesLayout.Controls.Add(this.txtDateCreated, 3, 0);
            this.updatesLayout.Controls.Add(this.txtUpdater, 1, 1);
            this.updatesLayout.Controls.Add(this.lblUpdater, 0, 1);
            this.updatesLayout.Controls.Add(this.lblDateUpdated, 2, 1);
            this.updatesLayout.Controls.Add(this.txtCreator, 1, 0);
            this.updatesLayout.Controls.Add(this.lblDateCreated, 2, 0);
            this.updatesLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatesLayout.Location = new System.Drawing.Point(3, 16);
            this.updatesLayout.Name = "updatesLayout";
            this.updatesLayout.RowCount = 2;
            this.updatesLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.updatesLayout.Size = new System.Drawing.Size(554, 51);
            this.updatesLayout.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.relationLayout);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 507);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(560, 44);
            this.relationGroup.TabIndex = 10;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // relationLayout
            // 
            this.relationLayout.ColumnCount = 4;
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.relationLayout.Controls.Add(this.txtToId, 3, 0);
            this.relationLayout.Controls.Add(this.txtFromId, 1, 0);
            this.relationLayout.Controls.Add(this.lblToId, 2, 0);
            this.relationLayout.Controls.Add(this.lblFromId, 0, 0);
            this.relationLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.relationLayout.Location = new System.Drawing.Point(3, 16);
            this.relationLayout.Name = "relationLayout";
            this.relationLayout.RowCount = 1;
            this.relationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.relationLayout.Size = new System.Drawing.Size(554, 25);
            this.relationLayout.TabIndex = 0;
            // 
            // txtToId
            // 
            this.txtToId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtToId.BackColor = System.Drawing.SystemColors.Control;
            this.txtToId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtToId.Location = new System.Drawing.Point(390, 3);
            this.txtToId.Name = "txtToId";
            this.txtToId.ReadOnly = true;
            this.txtToId.Size = new System.Drawing.Size(161, 20);
            this.txtToId.TabIndex = 3;
            // 
            // txtFromId
            // 
            this.txtFromId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFromId.BackColor = System.Drawing.SystemColors.Control;
            this.txtFromId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFromId.Location = new System.Drawing.Point(113, 3);
            this.txtFromId.Name = "txtFromId";
            this.txtFromId.ReadOnly = true;
            this.txtFromId.Size = new System.Drawing.Size(161, 20);
            this.txtFromId.TabIndex = 1;
            // 
            // lblToId
            // 
            this.lblToId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblToId.AutoSize = true;
            this.lblToId.Location = new System.Drawing.Point(349, 6);
            this.lblToId.Name = "lblToId";
            this.lblToId.Size = new System.Drawing.Size(35, 13);
            this.lblToId.TabIndex = 2;
            this.lblToId.Text = "To Id:";
            // 
            // lblFromId
            // 
            this.lblFromId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFromId.AutoSize = true;
            this.lblFromId.Location = new System.Drawing.Point(62, 6);
            this.lblFromId.Name = "lblFromId";
            this.lblFromId.Size = new System.Drawing.Size(45, 13);
            this.lblFromId.TabIndex = 0;
            this.lblFromId.Text = "From Id:";
            // 
            // usageGroup
            // 
            this.usageGroup.Controls.Add(this.usageLayout2);
            this.usageGroup.Controls.Add(this.usageLayout);
            this.usageGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.usageGroup.IsCollapsed = false;
            this.usageGroup.Location = new System.Drawing.Point(12, 360);
            this.usageGroup.Name = "usageGroup";
            this.usageGroup.Size = new System.Drawing.Size(560, 147);
            this.usageGroup.TabIndex = 9;
            this.usageGroup.TabStop = false;
            this.usageGroup.Text = "Usage";
            this.usageGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // usageLayout2
            // 
            this.usageLayout2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usageLayout2.ColumnCount = 4;
            this.usageLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.usageLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.usageLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.usageLayout2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.usageLayout2.Controls.Add(this.chkCar, 2, 0);
            this.usageLayout2.Controls.Add(this.chkBus, 1, 0);
            this.usageLayout2.Controls.Add(this.chkBicycle, 0, 0);
            this.usageLayout2.Controls.Add(this.chkDelivery, 3, 0);
            this.usageLayout2.Controls.Add(this.chkTruck, 2, 1);
            this.usageLayout2.Controls.Add(this.chkEmergency, 0, 1);
            this.usageLayout2.Controls.Add(this.chkPedestrian, 1, 1);
            this.usageLayout2.Location = new System.Drawing.Point(3, 92);
            this.usageLayout2.Name = "usageLayout2";
            this.usageLayout2.RowCount = 2;
            this.usageLayout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.usageLayout2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.usageLayout2.Size = new System.Drawing.Size(530, 46);
            this.usageLayout2.TabIndex = 0;
            // 
            // chkCar
            // 
            this.chkCar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkCar.AutoSize = true;
            this.chkCar.Location = new System.Drawing.Point(267, 3);
            this.chkCar.Name = "chkCar";
            this.chkCar.Size = new System.Drawing.Size(42, 17);
            this.chkCar.TabIndex = 2;
            this.chkCar.Text = "Car";
            this.chkCar.UseVisualStyleBackColor = true;
            // 
            // chkBus
            // 
            this.chkBus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkBus.AutoSize = true;
            this.chkBus.Location = new System.Drawing.Point(135, 3);
            this.chkBus.Name = "chkBus";
            this.chkBus.Size = new System.Drawing.Size(44, 17);
            this.chkBus.TabIndex = 1;
            this.chkBus.Text = "Bus";
            this.chkBus.UseVisualStyleBackColor = true;
            // 
            // chkBicycle
            // 
            this.chkBicycle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkBicycle.AutoSize = true;
            this.chkBicycle.Location = new System.Drawing.Point(3, 3);
            this.chkBicycle.Name = "chkBicycle";
            this.chkBicycle.Size = new System.Drawing.Size(60, 17);
            this.chkBicycle.TabIndex = 0;
            this.chkBicycle.Text = "Bicycle";
            this.chkBicycle.UseVisualStyleBackColor = true;
            // 
            // chkDelivery
            // 
            this.chkDelivery.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkDelivery.AutoSize = true;
            this.chkDelivery.Location = new System.Drawing.Point(399, 3);
            this.chkDelivery.Name = "chkDelivery";
            this.chkDelivery.Size = new System.Drawing.Size(64, 17);
            this.chkDelivery.TabIndex = 3;
            this.chkDelivery.Text = "Delivery";
            this.chkDelivery.UseVisualStyleBackColor = true;
            // 
            // chkTruck
            // 
            this.chkTruck.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkTruck.AutoSize = true;
            this.chkTruck.Location = new System.Drawing.Point(267, 26);
            this.chkTruck.Name = "chkTruck";
            this.chkTruck.Size = new System.Drawing.Size(54, 17);
            this.chkTruck.TabIndex = 6;
            this.chkTruck.Text = "Truck";
            this.chkTruck.UseVisualStyleBackColor = true;
            // 
            // chkEmergency
            // 
            this.chkEmergency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkEmergency.AutoSize = true;
            this.chkEmergency.Location = new System.Drawing.Point(3, 26);
            this.chkEmergency.Name = "chkEmergency";
            this.chkEmergency.Size = new System.Drawing.Size(79, 17);
            this.chkEmergency.TabIndex = 4;
            this.chkEmergency.Text = "Emergency";
            this.chkEmergency.UseVisualStyleBackColor = true;
            // 
            // chkPedestrian
            // 
            this.chkPedestrian.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkPedestrian.AutoSize = true;
            this.chkPedestrian.Location = new System.Drawing.Point(135, 26);
            this.chkPedestrian.Name = "chkPedestrian";
            this.chkPedestrian.Size = new System.Drawing.Size(76, 17);
            this.chkPedestrian.TabIndex = 5;
            this.chkPedestrian.Text = "Pedestrian";
            this.chkPedestrian.UseVisualStyleBackColor = true;
            // 
            // usageLayout
            // 
            this.usageLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usageLayout.ColumnCount = 6;
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.usageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.Controls.Add(this.lblLanes, 0, 2);
            this.usageLayout.Controls.Add(this.lblHeight, 4, 1);
            this.usageLayout.Controls.Add(this.lblElevation, 4, 0);
            this.usageLayout.Controls.Add(this.lblWidth, 2, 1);
            this.usageLayout.Controls.Add(this.lblMaxWeight, 2, 0);
            this.usageLayout.Controls.Add(this.lblLength, 0, 1);
            this.usageLayout.Controls.Add(this.lblSpeedLimit, 0, 0);
            this.usageLayout.Controls.Add(this.numLanes, 1, 2);
            this.usageLayout.Controls.Add(this.numElevation, 5, 0);
            this.usageLayout.Controls.Add(this.numSpeedLimit, 1, 0);
            this.usageLayout.Controls.Add(this.numWeight, 3, 0);
            this.usageLayout.Controls.Add(this.numLength, 1, 1);
            this.usageLayout.Controls.Add(this.numWidth, 3, 1);
            this.usageLayout.Controls.Add(this.numHeight, 5, 1);
            this.usageLayout.Location = new System.Drawing.Point(3, 16);
            this.usageLayout.Name = "usageLayout";
            this.usageLayout.RowCount = 3;
            this.usageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.usageLayout.Size = new System.Drawing.Size(530, 76);
            this.usageLayout.TabIndex = 1;
            // 
            // lblLanes
            // 
            this.lblLanes.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLanes.AutoSize = true;
            this.lblLanes.Location = new System.Drawing.Point(16, 56);
            this.lblLanes.Name = "lblLanes";
            this.lblLanes.Size = new System.Drawing.Size(91, 13);
            this.lblLanes.TabIndex = 12;
            this.lblLanes.Text = "Number of Lanes:";
            // 
            // lblHeight
            // 
            this.lblHeight.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHeight.AutoSize = true;
            this.lblHeight.Location = new System.Drawing.Point(418, 31);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(41, 13);
            this.lblHeight.TabIndex = 10;
            this.lblHeight.Text = "Height:";
            // 
            // lblElevation
            // 
            this.lblElevation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblElevation.AutoSize = true;
            this.lblElevation.Location = new System.Drawing.Point(405, 6);
            this.lblElevation.Name = "lblElevation";
            this.lblElevation.Size = new System.Drawing.Size(54, 13);
            this.lblElevation.TabIndex = 4;
            this.lblElevation.Text = "Elevation:";
            // 
            // lblWidth
            // 
            this.lblWidth.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblWidth.AutoSize = true;
            this.lblWidth.Location = new System.Drawing.Point(245, 31);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(38, 13);
            this.lblWidth.TabIndex = 8;
            this.lblWidth.Text = "Width:";
            // 
            // lblMaxWeight
            // 
            this.lblMaxWeight.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMaxWeight.AutoSize = true;
            this.lblMaxWeight.Location = new System.Drawing.Point(216, 6);
            this.lblMaxWeight.Name = "lblMaxWeight";
            this.lblMaxWeight.Size = new System.Drawing.Size(67, 13);
            this.lblMaxWeight.TabIndex = 2;
            this.lblMaxWeight.Text = "Max Weight:";
            // 
            // lblLength
            // 
            this.lblLength.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(64, 31);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(43, 13);
            this.lblLength.TabIndex = 6;
            this.lblLength.Text = "Length:";
            // 
            // lblSpeedLimit
            // 
            this.lblSpeedLimit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSpeedLimit.AutoSize = true;
            this.lblSpeedLimit.Location = new System.Drawing.Point(42, 6);
            this.lblSpeedLimit.Name = "lblSpeedLimit";
            this.lblSpeedLimit.Size = new System.Drawing.Size(65, 13);
            this.lblSpeedLimit.TabIndex = 0;
            this.lblSpeedLimit.Text = "Speed Limit:";
            // 
            // numLanes
            // 
            this.numLanes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numLanes.Location = new System.Drawing.Point(113, 53);
            this.numLanes.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numLanes.Name = "numLanes";
            this.numLanes.Size = new System.Drawing.Size(60, 20);
            this.numLanes.TabIndex = 13;
            this.numLanes.Value = 0;
            // 
            // numElevation
            // 
            this.numElevation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numElevation.Location = new System.Drawing.Point(465, 3);
            this.numElevation.Name = "numElevation";
            this.numElevation.Size = new System.Drawing.Size(62, 20);
            this.numElevation.TabIndex = 5;
            this.numElevation.Value = 0;
            // 
            // numSpeedLimit
            // 
            this.numSpeedLimit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numSpeedLimit.Location = new System.Drawing.Point(113, 3);
            this.numSpeedLimit.Maximum = new decimal(new int[] {
            110,
            0,
            0,
            0});
            this.numSpeedLimit.Name = "numSpeedLimit";
            this.numSpeedLimit.Size = new System.Drawing.Size(60, 20);
            this.numSpeedLimit.TabIndex = 1;
            this.numSpeedLimit.Value = 0;
            // 
            // numWeight
            // 
            this.numWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numWeight.DecimalPlaces = 2;
            this.numWeight.Location = new System.Drawing.Point(289, 3);
            this.numWeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWeight.Name = "numWeight";
            this.numWeight.Size = new System.Drawing.Size(60, 20);
            this.numWeight.TabIndex = 3;
            this.numWeight.Value = 0D;
            // 
            // numLength
            // 
            this.numLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numLength.DecimalPlaces = 2;
            this.numLength.Location = new System.Drawing.Point(113, 28);
            this.numLength.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numLength.Name = "numLength";
            this.numLength.Size = new System.Drawing.Size(60, 20);
            this.numLength.TabIndex = 7;
            this.numLength.Value = 0D;
            // 
            // numWidth
            // 
            this.numWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numWidth.DecimalPlaces = 2;
            this.numWidth.Location = new System.Drawing.Point(289, 28);
            this.numWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numWidth.Name = "numWidth";
            this.numWidth.Size = new System.Drawing.Size(60, 20);
            this.numWidth.TabIndex = 9;
            this.numWidth.Value = 0D;
            // 
            // numHeight
            // 
            this.numHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numHeight.DecimalPlaces = 2;
            this.numHeight.Location = new System.Drawing.Point(465, 28);
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(62, 20);
            this.numHeight.TabIndex = 11;
            this.numHeight.Value = 0D;
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.addressLayout);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = false;
            this.addressGroup.Location = new System.Drawing.Point(12, 203);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(560, 157);
            this.addressGroup.TabIndex = 8;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // addressLayout
            // 
            this.addressLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addressLayout.ColumnCount = 4;
            this.addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.addressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.addressLayout.Controls.Add(this.lblSubCity, 2, 3);
            this.addressLayout.Controls.Add(this.txtSubCity, 3, 3);
            this.addressLayout.Controls.Add(this.lblType, 0, 0);
            this.addressLayout.Controls.Add(this.btnPickLocation, 3, 4);
            this.addressLayout.Controls.Add(this.cbType, 1, 0);
            this.addressLayout.Controls.Add(this.cbName, 3, 0);
            this.addressLayout.Controls.Add(this.lblName, 2, 0);
            this.addressLayout.Controls.Add(this.lblName2, 2, 1);
            this.addressLayout.Controls.Add(this.cbName2, 3, 1);
            this.addressLayout.Controls.Add(this.lblSection, 0, 2);
            this.addressLayout.Controls.Add(this.txtSection, 1, 2);
            this.addressLayout.Controls.Add(this.lblCity, 0, 3);
            this.addressLayout.Controls.Add(this.txtCity, 1, 3);
            this.addressLayout.Controls.Add(this.lblPostcode, 2, 2);
            this.addressLayout.Controls.Add(this.txtPostcode, 3, 2);
            this.addressLayout.Controls.Add(this.lblState, 0, 4);
            this.addressLayout.Controls.Add(this.txtState, 1, 4);
            this.addressLayout.Controls.Add(this.ckShowText, 1, 1);
            this.addressLayout.Location = new System.Drawing.Point(3, 16);
            this.addressLayout.Name = "addressLayout";
            this.addressLayout.RowCount = 5;
            this.addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.addressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.addressLayout.Size = new System.Drawing.Size(530, 135);
            this.addressLayout.TabIndex = 0;
            // 
            // lblSubCity
            // 
            this.lblSubCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSubCity.AutoSize = true;
            this.lblSubCity.Location = new System.Drawing.Point(323, 84);
            this.lblSubCity.Name = "lblSubCity";
            this.lblSubCity.Size = new System.Drawing.Size(49, 13);
            this.lblSubCity.TabIndex = 12;
            this.lblSubCity.Text = "Sub City:";
            // 
            // txtSubCity
            // 
            this.txtSubCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSubCity.Location = new System.Drawing.Point(378, 81);
            this.txtSubCity.Name = "txtSubCity";
            this.txtSubCity.Size = new System.Drawing.Size(149, 20);
            this.txtSubCity.TabIndex = 13;
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(73, 6);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type:";
            // 
            // btnPickLocation
            // 
            this.btnPickLocation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnPickLocation.Location = new System.Drawing.Point(427, 108);
            this.btnPickLocation.Name = "btnPickLocation";
            this.btnPickLocation.Size = new System.Drawing.Size(100, 23);
            this.btnPickLocation.TabIndex = 16;
            this.btnPickLocation.Text = "Pick Location";
            this.btnPickLocation.UseVisualStyleBackColor = true;
            this.btnPickLocation.Click += new System.EventHandler(this.btnPickLocation_Click);
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(113, 3);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(149, 21);
            this.cbType.TabIndex = 1;
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(378, 3);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(149, 21);
            this.cbName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(334, 6);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(328, 32);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(44, 13);
            this.lblName2.TabIndex = 4;
            this.lblName2.Text = "Name2:";
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Location = new System.Drawing.Point(378, 29);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(149, 21);
            this.cbName2.TabIndex = 5;
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 58);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 6;
            this.lblSection.Text = "Section:";
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 55);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(149, 20);
            this.txtSection.TabIndex = 7;
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 84);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 10;
            this.lblCity.Text = "City:";
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 81);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(149, 20);
            this.txtCity.TabIndex = 11;
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(317, 58);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 8;
            this.lblPostcode.Text = "Postcode:";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(378, 55);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(149, 20);
            this.txtPostcode.TabIndex = 9;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(72, 113);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 14;
            this.lblState.Text = "State:";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(113, 109);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(149, 20);
            this.txtState.TabIndex = 15;
            // 
            // ckShowText
            // 
            this.ckShowText.AutoSize = true;
            this.ckShowText.Checked = true;
            this.ckShowText.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckShowText.Location = new System.Drawing.Point(113, 29);
            this.ckShowText.Name = "ckShowText";
            this.ckShowText.Size = new System.Drawing.Size(109, 17);
            this.ckShowText.TabIndex = 17;
            this.ckShowText.Text = "Show Text NEPS";
            this.ckShowText.UseVisualStyleBackColor = true;
            // 
            // generalLayout
            // 
            this.generalLayout.ColumnCount = 4;
            this.generalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.generalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.generalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.generalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.generalLayout.Controls.Add(this.cbSource, 3, 5);
            this.generalLayout.Controls.Add(this.cbDirection, 3, 4);
            this.generalLayout.Controls.Add(this.lblSource, 2, 5);
            this.generalLayout.Controls.Add(this.cbClass, 3, 2);
            this.generalLayout.Controls.Add(this.cbNetworkClass, 1, 2);
            this.generalLayout.Controls.Add(this.lblConstructionStatus, 0, 0);
            this.generalLayout.Controls.Add(this.lblDirection, 2, 4);
            this.generalLayout.Controls.Add(this.lblStatus, 0, 1);
            this.generalLayout.Controls.Add(this.lblNetworkClass, 0, 2);
            this.generalLayout.Controls.Add(this.lblClass, 2, 2);
            this.generalLayout.Controls.Add(this.cbStatus, 1, 1);
            this.generalLayout.Controls.Add(this.cbConstructionStatus, 1, 0);
            this.generalLayout.Controls.Add(this.lblNaviStatus, 2, 0);
            this.generalLayout.Controls.Add(this.cbNaviStatus, 3, 0);
            this.generalLayout.Controls.Add(this.lblCategory, 2, 1);
            this.generalLayout.Controls.Add(this.cbCategory, 3, 1);
            this.generalLayout.Controls.Add(this.lblFilterLevel, 0, 3);
            this.generalLayout.Controls.Add(this.cbFilterLevel, 1, 3);
            this.generalLayout.Controls.Add(this.lblDesign, 2, 3);
            this.generalLayout.Controls.Add(this.cbDesign, 3, 3);
            this.generalLayout.Controls.Add(this.lblTollType, 0, 4);
            this.generalLayout.Controls.Add(this.cbTollType, 1, 4);
            this.generalLayout.Controls.Add(this.lblDivider, 0, 5);
            this.generalLayout.Controls.Add(this.cbDivider, 1, 5);
            this.generalLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.generalLayout.Location = new System.Drawing.Point(12, 51);
            this.generalLayout.Name = "generalLayout";
            this.generalLayout.RowCount = 6;
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.generalLayout.Size = new System.Drawing.Size(560, 152);
            this.generalLayout.TabIndex = 7;
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(393, 128);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(164, 21);
            this.cbSource.TabIndex = 23;
            // 
            // cbDirection
            // 
            this.cbDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDirection.FormattingEnabled = true;
            this.cbDirection.Location = new System.Drawing.Point(393, 103);
            this.cbDirection.Name = "cbDirection";
            this.cbDirection.Size = new System.Drawing.Size(164, 21);
            this.cbDirection.TabIndex = 19;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(343, 132);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 22;
            this.lblSource.Text = "Source:";
            // 
            // cbClass
            // 
            this.cbClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(393, 53);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(164, 21);
            this.cbClass.TabIndex = 11;
            // 
            // cbNetworkClass
            // 
            this.cbNetworkClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNetworkClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNetworkClass.FormattingEnabled = true;
            this.cbNetworkClass.Location = new System.Drawing.Point(113, 53);
            this.cbNetworkClass.Name = "cbNetworkClass";
            this.cbNetworkClass.Size = new System.Drawing.Size(164, 21);
            this.cbNetworkClass.TabIndex = 9;
            // 
            // lblConstructionStatus
            // 
            this.lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblConstructionStatus.AutoSize = true;
            this.lblConstructionStatus.Location = new System.Drawing.Point(5, 6);
            this.lblConstructionStatus.Name = "lblConstructionStatus";
            this.lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            this.lblConstructionStatus.TabIndex = 0;
            this.lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblDirection
            // 
            this.lblDirection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDirection.AutoSize = true;
            this.lblDirection.Location = new System.Drawing.Point(335, 106);
            this.lblDirection.Name = "lblDirection";
            this.lblDirection.Size = new System.Drawing.Size(52, 13);
            this.lblDirection.TabIndex = 18;
            this.lblDirection.Text = "Direction:";
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(67, 31);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(40, 13);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Status:";
            // 
            // lblNetworkClass
            // 
            this.lblNetworkClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNetworkClass.AutoSize = true;
            this.lblNetworkClass.Location = new System.Drawing.Point(29, 56);
            this.lblNetworkClass.Name = "lblNetworkClass";
            this.lblNetworkClass.Size = new System.Drawing.Size(78, 13);
            this.lblNetworkClass.TabIndex = 8;
            this.lblNetworkClass.Text = "Network Class:";
            // 
            // lblClass
            // 
            this.lblClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(352, 56);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(35, 13);
            this.lblClass.TabIndex = 10;
            this.lblClass.Text = "Class:";
            // 
            // cbStatus
            // 
            this.cbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(113, 28);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(164, 21);
            this.cbStatus.TabIndex = 5;
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(113, 3);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(164, 21);
            this.cbConstructionStatus.TabIndex = 1;
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(293, 6);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 2;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(393, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(164, 21);
            this.cbNaviStatus.TabIndex = 3;
            // 
            // lblCategory
            // 
            this.lblCategory.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(335, 31);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(52, 13);
            this.lblCategory.TabIndex = 6;
            this.lblCategory.Text = "Category:";
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(393, 28);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(164, 21);
            this.cbCategory.TabIndex = 7;
            // 
            // lblFilterLevel
            // 
            this.lblFilterLevel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFilterLevel.AutoSize = true;
            this.lblFilterLevel.Location = new System.Drawing.Point(46, 81);
            this.lblFilterLevel.Name = "lblFilterLevel";
            this.lblFilterLevel.Size = new System.Drawing.Size(61, 13);
            this.lblFilterLevel.TabIndex = 12;
            this.lblFilterLevel.Text = "Filter Level:";
            // 
            // cbFilterLevel
            // 
            this.cbFilterLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilterLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFilterLevel.FormattingEnabled = true;
            this.cbFilterLevel.Location = new System.Drawing.Point(113, 78);
            this.cbFilterLevel.Name = "cbFilterLevel";
            this.cbFilterLevel.Size = new System.Drawing.Size(164, 21);
            this.cbFilterLevel.TabIndex = 13;
            // 
            // lblDesign
            // 
            this.lblDesign.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDesign.AutoSize = true;
            this.lblDesign.Location = new System.Drawing.Point(344, 81);
            this.lblDesign.Name = "lblDesign";
            this.lblDesign.Size = new System.Drawing.Size(43, 13);
            this.lblDesign.TabIndex = 14;
            this.lblDesign.Text = "Design:";
            // 
            // cbDesign
            // 
            this.cbDesign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDesign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDesign.FormattingEnabled = true;
            this.cbDesign.Location = new System.Drawing.Point(393, 78);
            this.cbDesign.Name = "cbDesign";
            this.cbDesign.Size = new System.Drawing.Size(164, 21);
            this.cbDesign.TabIndex = 15;
            // 
            // lblTollType
            // 
            this.lblTollType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTollType.AutoSize = true;
            this.lblTollType.Location = new System.Drawing.Point(53, 106);
            this.lblTollType.Name = "lblTollType";
            this.lblTollType.Size = new System.Drawing.Size(54, 13);
            this.lblTollType.TabIndex = 16;
            this.lblTollType.Text = "Toll Type:";
            // 
            // cbTollType
            // 
            this.cbTollType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTollType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTollType.FormattingEnabled = true;
            this.cbTollType.Location = new System.Drawing.Point(113, 103);
            this.cbTollType.Name = "cbTollType";
            this.cbTollType.Size = new System.Drawing.Size(164, 21);
            this.cbTollType.TabIndex = 17;
            // 
            // lblDivider
            // 
            this.lblDivider.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDivider.AutoSize = true;
            this.lblDivider.Location = new System.Drawing.Point(64, 132);
            this.lblDivider.Name = "lblDivider";
            this.lblDivider.Size = new System.Drawing.Size(43, 13);
            this.lblDivider.TabIndex = 20;
            this.lblDivider.Text = "Divider:";
            // 
            // cbDivider
            // 
            this.cbDivider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDivider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDivider.FormattingEnabled = true;
            this.cbDivider.Location = new System.Drawing.Point(113, 128);
            this.cbDivider.Name = "cbDivider";
            this.cbDivider.Size = new System.Drawing.Size(164, 21);
            this.cbDivider.TabIndex = 21;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(57, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Original Id:";
            // 
            // StreetANDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 658);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.usageGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.generalLayout);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "StreetANDForm";
            this.Text = "StreetForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.generalLayout, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.usageGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.updatesLayout.ResumeLayout(false);
            this.updatesLayout.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.relationLayout.ResumeLayout(false);
            this.relationLayout.PerformLayout();
            this.usageGroup.ResumeLayout(false);
            this.usageLayout2.ResumeLayout(false);
            this.usageLayout2.PerformLayout();
            this.usageLayout.ResumeLayout(false);
            this.usageLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLanes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numElevation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeedLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            this.addressGroup.ResumeLayout(false);
            this.addressLayout.ResumeLayout(false);
            this.addressLayout.PerformLayout();
            this.generalLayout.ResumeLayout(false);
            this.generalLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel updatesLayout;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel relationLayout;
        protected System.Windows.Forms.TextBox txtToId;
        protected System.Windows.Forms.TextBox txtFromId;
        protected System.Windows.Forms.Label lblToId;
        protected System.Windows.Forms.Label lblFromId;
        protected Controls.CollapsibleGroupBox usageGroup;
        protected System.Windows.Forms.TableLayoutPanel usageLayout2;
        protected System.Windows.Forms.CheckBox chkCar;
        protected System.Windows.Forms.CheckBox chkBus;
        protected System.Windows.Forms.CheckBox chkBicycle;
        protected System.Windows.Forms.CheckBox chkDelivery;
        protected System.Windows.Forms.CheckBox chkTruck;
        protected System.Windows.Forms.CheckBox chkEmergency;
        protected System.Windows.Forms.CheckBox chkPedestrian;
        protected System.Windows.Forms.TableLayoutPanel usageLayout;
        protected System.Windows.Forms.Label lblLanes;
        protected System.Windows.Forms.Label lblHeight;
        protected System.Windows.Forms.Label lblElevation;
        protected System.Windows.Forms.Label lblWidth;
        protected System.Windows.Forms.Label lblMaxWeight;
        protected System.Windows.Forms.Label lblLength;
        protected System.Windows.Forms.Label lblSpeedLimit;
        protected Controls.IntUpDown numLanes;
        protected Controls.IntUpDown numElevation;
        protected Controls.IntUpDown numSpeedLimit;
        protected Controls.DoubleUpDown numWeight;
        protected Controls.DoubleUpDown numLength;
        protected Controls.DoubleUpDown numWidth;
        protected Controls.DoubleUpDown numHeight;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel addressLayout;
        protected System.Windows.Forms.Label lblType;
        protected System.Windows.Forms.Button btnPickLocation;
        protected Controls.UpperComboBox cbType;
        protected Controls.UpperComboBox cbName;
        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.Label lblName2;
        protected Controls.UpperComboBox cbName2;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TableLayoutPanel generalLayout;
        protected Controls.UpperComboBox cbDivider;
        protected System.Windows.Forms.Label lblDivider;
        protected Controls.UpperComboBox cbDirection;
        protected Controls.UpperComboBox cbFilterLevel;
        protected Controls.UpperComboBox cbCategory;
        protected Controls.UpperComboBox cbClass;
        protected Controls.UpperComboBox cbNetworkClass;
        protected Controls.UpperComboBox cbTollType;
        protected System.Windows.Forms.Label lblTollType;
        protected Controls.UpperComboBox cbDesign;
        protected System.Windows.Forms.Label lblDesign;
        protected System.Windows.Forms.Label lblConstructionStatus;
        protected System.Windows.Forms.Label lblDirection;
        protected System.Windows.Forms.Label lblFilterLevel;
        protected System.Windows.Forms.Label lblStatus;
        protected System.Windows.Forms.Label lblNetworkClass;
        protected System.Windows.Forms.Label lblClass;
        protected Controls.UpperComboBox cbStatus;
        protected System.Windows.Forms.Label lblCategory;
        protected Controls.UpperComboBox cbConstructionStatus;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected Controls.UpperComboBox cbNaviStatus;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        protected System.Windows.Forms.Label lblSubCity;
        protected System.Windows.Forms.TextBox txtSubCity;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.CheckBox ckShowText;
    }
}
﻿
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Logs;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Search;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ILog = Geomatic.Core.Logs.ILog;
using Geomatic.UI.Properties;
using System.IO;
using ESRI.ArcGIS.Geodatabase;
using System.Linq;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;

namespace Geomatic.UI.Forms
{
    public partial class VerifyWorkArea2Form : SearchFeatureForm
    {
        protected SegmentName _segmentName { set; get; }

        string WASegmentName = "";
        int gListView1LostFocusItem = -1;
        private string FeatureID = "";
        private string FeatureName = "";
        string pathString = "";

        public VerifyWorkArea2Form(SegmentName segmentName)
           : base(segmentName)
        {
            InitializeComponent();
            _segmentName = segmentName;
            RefreshTitle("Supervisor Verification Form");
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            // noraini ali - value passing from search WA - only status Close or Verifying
            if (Settings.Default.PickedWorkAreaIdIndex > 0)
                cbWorkAreaId.Text = Settings.Default.PickedWorkAreaIdIndex.ToString();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Feature Name");
            //lvResult.Columns.Add("AND Status Flag");
            lvResult.Columns.Add("AND Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Remark");

            switch (_segmentName)
            {
                case SegmentName.AS:
                    WASegmentName = "AS";
                    break;
                case SegmentName.JH:
                    WASegmentName = "JH";
                    break;
                case SegmentName.JP:
                    WASegmentName = "JP";
                    break;
                case SegmentName.KG:
                    WASegmentName = "KG";
                    break;
                case SegmentName.KK:
                    WASegmentName = "KK";
                    break;
                case SegmentName.KN:
                    WASegmentName = "KN";
                    break;
                case SegmentName.KV:
                    WASegmentName = "KV";
                    break;
                case SegmentName.MK:
                    WASegmentName = "MK";
                    break;
                case SegmentName.PG:
                    WASegmentName = "PG";
                    break;
                case SegmentName.TG:
                    WASegmentName = "TG";
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", _segmentName));
            }

            ComboBoxUtils.PopulateWorkArea(cbWorkAreaId, WASegmentName);

            // noraini ali - OKT 2020 - Create folder to save report file
            //pathString = @"c:\Report_verify\";
            //System.IO.Directory.CreateDirectory(pathString);
            CreateFolderToSaveFile();
        }

        private void CreateFolderToSaveFile()
        {
            // noraini ali - OKT 2020 - Create folder to save report file
            string folderReportByDate = DateTime.Now.ToString("yyyyMMdd");
            string DirString = @"Z:\";
            if (!Directory.Exists(DirString))
            {
                pathString = @"C:\AND_REPORT\" + folderReportByDate + "\\";
                System.IO.Directory.CreateDirectory(pathString);
            }
            else
            {
                pathString = @"Z:\AND_REPORT\" + folderReportByDate + "\\";
                System.IO.Directory.CreateDirectory(pathString);
            }
        }

        private void PopulatePreview()
        {
            btnSearch.Enabled = true;
            EnabledButton(false);
            EnabledRgButton(false);

            if (cbWorkAreaId.Text != null)
            {
                int? id = StringUtils.CheckInt(cbWorkAreaId.Text);
                List<ListViewItem> items = new List<ListViewItem>();
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                if (id.HasValue)
                {
                    GWorkArea work_area = repo.GetById<GWorkArea>(id.Value);
                    if (work_area != null)
                    {
                        txtSegmentName.Text = work_area.WorkAreaSegmentName;
                        txtCompleteFlag.Text = GetFlagName(work_area.flag);
                        txtUserName.Text = work_area.user_id;
                        txtWorkOrderNo.Text = work_area.Wo_no;
                    }

                    if (txtCompleteFlag.Text == "Verifying" || txtCompleteFlag.Text == "Closed")
                    {
                        if (lvResult.Items.Count > 0)
                        {
                            EnabledButton(true);
                            EnabledRgButton(true);
                        }
                        else
                        {
                            btnStartVerify.Enabled = true;
                            btnCancelVerify.Enabled = true;
                        }
                    }
                }
                else
                {
                    btnSearch.Enabled = false;
                    InitTextBox();
                }
            }
            else
            {
                InitTextBox();
                lvResult.Items.Clear();
                EnabledButton(false);
                EnabledRgButton(false);
            }
        }

        private void cbWorkAreaId_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
            lvResult.Items.Clear();
            EnabledButton(false);
        }

        private void VerifyWorkArea2Form_Shown(object sender, EventArgs e)
        {}

        private void lvResult_Click(object sender, EventArgs e)
        {
            gListView1LostFocusItem = lvResult.FocusedItem.Index;
            if (lvResult.Items[gListView1LostFocusItem].BackColor == Color.ForestGreen ||
               lvResult.Items[gListView1LostFocusItem].BackColor == Color.IndianRed)
            {
                btnDisplay.Enabled = false;
            }
            else
            {
                if (txtCompleteFlag.Text == "Verifying")
                { 
                    btnDisplay.Enabled = true;
                }
            }
        }

        private void AutoSaveFileResult()
        {
            string fileName = SegmentName.ToString() + "_WA_" + cbWorkAreaId.Text.ToString() + "_" + txtWorkOrderNo.Text.ToString();
            ILog VerificationLog = new CsvLog(pathString + fileName);

            // write column header 
            List<string> columns = new List<string>();
            foreach (ColumnHeader columnHeader in lvResult.Columns)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("\"");
                sb.Append(columnHeader.Text);
                sb.Append("\"");
                columns.Add(sb.ToString());
            }
            VerificationLog.Log(string.Join(",", columns.ToArray()));

            // write result 
            foreach (ListViewItem item in lvResult.Items)
            {
                List<string> subItems = new List<string>();
                foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("\"");
                    sb.Append(subItem.Text);
                    sb.Append("\"");
                    subItems.Add(sb.ToString());
                }
                VerificationLog.Log(string.Join(",", subItems.ToArray()));
            }
        }

        private void PopulateFeatureByWA(bool filterADD)
        {
            int? id = StringUtils.CheckInt(cbWorkAreaId.Text);
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            GWorkArea _workarea = repo.GetById<GWorkArea>(id.Value);

            lvResult.BeginUpdate();
            lvResult.Sorting = SortOrder.None;
            lvResult.Items.Clear();

            try
            {
                List<ListViewItem> items = new List<ListViewItem>();
                if (chkProperty.Checked)
                {
                    foreach (GProperty property in _workarea.GetPropertyAndStatus(filterADD))
                    {
                        ListViewItem item = CreateitemProperty(property);
                        item.Tag = property;
                        items.Add(item);
                    }
                }
                if (chkLandmark.Checked)
                {
                    foreach (GLandmark landmark in _workarea.GetLandmarkAndStatus(true, filterADD))
                    {
                        ListViewItem item = CreateitemLandmark(landmark);
                        item.Tag = landmark;
                        items.Add(item);
                    }
                }
                if (chkJunction.Checked)
                {
                    foreach (GJunction junction in _workarea.GetJunctionAndStatus(filterADD))
                    {
                        ListViewItem item = CreateitemJunction(junction);
                        item.Tag = junction;
                        items.Add(item);
                    }
                }
                if (chkStreet.Checked)
                {
                    foreach (GStreet street in _workarea.GetStreetAndStatus(filterADD))
                    {
                        ListViewItem item = CreateitemStreet(street);
                        item.Tag = street;
                        items.Add(item);
                    }
                }
                if (chkBuilding.Checked)
                {
                    foreach (GBuilding building in _workarea.GetBuildingAndStatus(filterADD))
                    {
                        ListViewItem item = CreateitemBuilding(building);
                        item.Tag = building;
                        items.Add(item);
                    }
                }
                if (chkBuildingGroup.Checked)
                {
                    foreach (GBuildingGroup buildingGroup in _workarea.GetBuildingGroupAndStatus(filterADD))
                    {
                        ListViewItem item = CreateitemBuildingGroup(buildingGroup);
                        items.Add(item);
                    }
                }

                lvResult.Items.AddRange(items.ToArray());

                if (lvResult.Items.Count > 0 && txtCompleteFlag.Text == "Verifying")
                {
                    lvResult.Items[0].Selected = true;
                    if (!lvResult.Focused)
                    {
                        lvResult.Focus();
                    }

                    EnabledButton(true);
                }
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        #region populate Feature and And Status belong to Work Area Id
        protected ListViewItem CreateitemProperty(GProperty property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add("Property");
            //item.SubItems.Add(property.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(property.AndStatus));
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.Tag = property;
            return item;
        }
        protected ListViewItem CreateitemLandmark(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add("Landmark");
            //item.SubItems.Add(landmark.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(landmark.AndStatus));
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.Tag = landmark;
            return item;
        }
        protected ListViewItem CreateitemJunction(GJunction junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add("Junction");
            //item.SubItems.Add(junction.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(junction.AndStatus));
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.Tag = junction;
            return item;
        }
        protected ListViewItem CreateitemStreet(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add("Street");
            //item.SubItems.Add(street.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(street.AndStatus));
            item.SubItems.Add(street.NavigationStatusValue);
            item.SubItems.Add("");
            item.Tag = street;
            return item;
        }
        protected ListViewItem CreateitemBuilding(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add("Building");
            //item.SubItems.Add(building.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(building.AndStatus));
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.Tag = building;
            return item;
        }
        protected ListViewItem CreateitemBuildingGroup(GBuildingGroup buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add("BuildingGroup");
            //item.SubItems.Add(buildingGroup.AndStatus.ToString());
            item.SubItems.Add(GetnameAndStatus(buildingGroup.AndStatus));
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.Tag = buildingGroup;
            return item;
        }
        #endregion

        private string GetnameAndStatus(int? andStatus)
        {
            string AndStatusName = "";

            if (andStatus == 1)
            { AndStatusName = "New"; }
            else if (andStatus == 2)
            { AndStatusName = "Delete"; }
            else if (andStatus == 3)
            { AndStatusName = "Edit Graphic & Attributes"; }
            else if (andStatus == 4)
            { AndStatusName = "Edit Attributes Only"; }

            return AndStatusName;
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            PopulateFeatureByWA(false);
            EnabledButton(false);
            EnabledRgButton(false);

            if (lvResult.Items.Count > 0)
            {
                if (txtCompleteFlag.Text == "Verifying" || txtCompleteFlag.Text == "Closed")
                {
                    EnabledButton(true);
                    EnabledRgButton(true);
                }

                // noraini ali - OKT 2020 - to create filename by segment, work area id & work order no
                saveFileDialog.InitialDirectory = pathString;
                string fileName = SegmentName.ToString() + "_WA_" + cbWorkAreaId.Text.ToString() + "_" + txtWorkOrderNo.Text.ToString();
                string pathString1 = System.IO.Path.Combine(pathString, fileName);
                saveFileDialog.FileName = pathString1;
                AutoSaveFileResult();
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                return;
            }

            List<IGFeature> collection;

            using (new WaitCursor())
            {
                BuildCollection(out collection);
            }

            OnShowMap(new ShowMapEventArgs(collection), false);
        }

        private void lvResult_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnShow;
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            DisplayAttributeToVerification();
        }

        private void DisplayAttributeToVerification()
        {
            // noraini ali - prompt supervisor to lock WA before proceed to fixed buq or verify
            if (txtCompleteFlag.Text.ToString() == "Closed")
            {
                MessageBox.Show("Work Area currently closed, Please lock this Work Area to proceed verification.");
                return;
            }

            gListView1LostFocusItem = lvResult.FocusedItem.Index;
            if (lvResult.Items[gListView1LostFocusItem].BackColor == Color.ForestGreen ||
               lvResult.Items[gListView1LostFocusItem].BackColor == Color.IndianRed)
            {
                btnDisplay.Enabled = false;
            }
            else
            {
                btnDisplay.Enabled = true;
                ListViewItem item1 = lvResult.Items[gListView1LostFocusItem];
                FeatureID = item1.Text;
                FeatureName = item1.SubItems[1].Text;

                using (VerificationForm form = new VerificationForm(FeatureID, FeatureName, txtSegmentName.Text))
                {
                    if (form.ShowDialog(this) != DialogResult.OK)
                    {
                        return;
                    }
                    using (new WaitCursor())
                    {
                        if (lvResult.Items.Count > 0)
                        {
                            lvResult.Items[gListView1LostFocusItem].Selected = true;

                            // Case Verify AND Feature
                            if (form.verifystatus == 0)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Verification");
                            }
                            else if (form.verifystatus == 1)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Successful Verification");
                            }
                            else if (form.verifystatus == 2)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Verify Process");
                            }

                            // Case Revert AND Feature
                            if (form.revertstatus == 0)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Revert Process");
                            }
                            else if (form.revertstatus == 1)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Successful Revert Process");
                            }
                            else if (form.revertstatus == 2)
                            {
                                ListViewItem item = lvResult.SelectedItems[0];
                                UpdateSelectedItem(item, FeatureName, "Fail Revert Process.");
                            }

                            if (form.verifystatus == 1 || form.revertstatus == 1)
                            {
                                lvResult.Items[gListView1LostFocusItem].BackColor = Color.ForestGreen;
                                lvResult.Items[gListView1LostFocusItem].ForeColor = Color.White;
                            }
                            else
                            {
                                lvResult.Items[gListView1LostFocusItem].BackColor = Color.IndianRed;
                                lvResult.Items[gListView1LostFocusItem].ForeColor = Color.White;
                            }

                            if (!lvResult.Focused)
                            {
                                lvResult.Focus();
                            }
                        }
                    }
                }
            }
        }

        private void UpdateSelectedItem(ListViewItem item, string txtName, string txtProcess)
        {
            if (txtName == "Property")
            {
                GProperty property = (GProperty)item.Tag;
                item.SubItems.Clear();
                //item.Text = property.OID.ToString() + "-Property";
                item.Text = property.OID.ToString();
                item.SubItems.Add("Property");
                //item.SubItems.Add(property.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(property.AndStatus));
                item.SubItems.Add("");
                item.SubItems.Add(txtProcess);
                item.Tag = property;
            }
            else if (txtName == "Junction")
            {
                GJunction junction = (GJunction)item.Tag;
                item.SubItems.Clear();
                //item.Text = junction.OID.ToString() + "-Junction";
                item.Text = junction.OID.ToString();
                item.SubItems.Add("Junction");
                //item.SubItems.Add(junction.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(junction.AndStatus));
                item.SubItems.Add("");
                item.SubItems.Add(txtProcess);
                item.Tag = junction;
            }
            else if (txtName == "Building")
            {
                GBuilding building = (GBuilding)item.Tag;
                item.SubItems.Clear();
                //item.Text = building.OID.ToString() + "-Building";
                item.Text = building.OID.ToString();
                item.SubItems.Add("Building");
                //item.SubItems.Add(building.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(building.AndStatus));
                item.SubItems.Add("");
                item.SubItems.Add(txtProcess);
                item.Tag = building;
            }
            else if (txtName == "Street")
            {
                GStreet street = (GStreet)item.Tag;
                item.SubItems.Clear();
                //item.Text = street.OID.ToString() + "-Street";
                item.Text = street.OID.ToString();
                item.SubItems.Add("Street");
                //item.SubItems.Add(street.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(street.AndStatus));
                item.SubItems.Add(street.NavigationStatusValue);
                item.SubItems.Add(txtProcess);
                item.Tag = street;
            }
            else if (txtName == "Landmark")
            {
                GLandmark landmark = (GLandmark)item.Tag;
                item.SubItems.Clear();
                //item.Text = landmark.OID.ToString() + "-Landmark";
                item.Text = landmark.OID.ToString();
                item.SubItems.Add("Landmark");
                //item.SubItems.Add(landmark.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(landmark.AndStatus));
                item.SubItems.Add("");
                item.SubItems.Add(txtProcess);
                item.Tag = landmark;
            }
            else if (txtName == "BuildingGroup")
            {
                GBuildingGroup buildingGroup = (GBuildingGroup)item.Tag;
                item.SubItems.Clear();
                //item.Text = buildingGroup.OID.ToString() + "-BuildingGroup";
                item.Text = buildingGroup.OID.ToString();
                item.SubItems.Add("BuildingGroup");
                //item.SubItems.Add(buildingGroup.AndStatus.ToString());
                item.SubItems.Add(GetnameAndStatus(buildingGroup.AndStatus));
                item.SubItems.Add("");
                item.SubItems.Add(txtProcess);
                item.Tag = buildingGroup;
            }
        }

        private string GetFlagName(string flag)
        {
            string flagName = "";

            if (flag == null)
            { flagName = "New"; }
            else if (flag == "0")
            { flagName = "Open"; }
            else if (flag == "1")
            { flagName = "Closed"; }
            else if (flag == "2")
            { flagName = "Verifying"; }

            return flagName;
        }

        private void btnStartVerify_Click(object sender, EventArgs e)
        {
            int? id = StringUtils.CheckInt(cbWorkAreaId.Text);
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            GWorkArea _workarea = repo.GetById<GWorkArea>(id.Value);
            _workarea.flag = "2";
            _workarea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");

            using (new WaitCursor())
            {
                repo.StartTransaction();
            }
            repo.Update(_workarea);
            PopulatePreview();
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Work Area Locked Successful.");
                box.Show();
            }
            repo.EndTransaction();

            // auto save
            using (new WaitCursor())
            {
                Session.Current.Cadu.Save(true);
            }
        }

        private void btnCancelVerify_Click(object sender, EventArgs e)
        {
            int? id = StringUtils.CheckInt(cbWorkAreaId.Text);
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            GWorkArea _workarea = repo.GetById<GWorkArea>(id.Value);
            _workarea.flag = "1";
            _workarea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");

            using (new WaitCursor())
            {
                repo.StartTransaction();
            }
            repo.Update(_workarea);
            PopulatePreview();
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Work Area Unlock Successful.");
                box.Show();
            }
            repo.EndTransaction();
        }

        // noraini ali - Apr 2021
        // new button to filter all new created feature by User AND
        // 
        private void btnFilterADDStatus_Click(object sender, EventArgs e)
        {
            PopulateFeatureByWA(true);

            if (lvResult.Items.Count == 0)
            {
                EnabledButton(false);
            }
            else
            {
                EnabledButton(true);
            }
        }

        // noraini ali - Apr 2021
        // new button to allow Supervisor verify all new created feature by User AND
        //
        private void btnVerifyADD_Click(object sender, EventArgs e)
        {
            // noraini ali - prompt supervisor to lock WA before proceed to fixed buq or verify
            if (txtCompleteFlag.Text.ToString() == "Closed")
            {
                MessageBox.Show("Work Area currently closed, Please lock this Work Area to proceed verification.");
                return;
            }

            // ask user to confirm proceeed all this data
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Confirmation to Verify.")
                      .SetText("Are you sure you want to verify all this result list ?");

            if (confirmBox.Show() == DialogResult.No)
            {
                return;
            }

            // unable to verify if in result list not only New Status
            foreach (ListViewItem item in lvResult.Items)
            {
                int column = 0;
                List<string> subItems = new List<string>();
                foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("\"");
                    sb.Append(subItem.Text);
                    sb.Append("\"");
                    subItems.Add(sb.ToString());
                    if (column == 2 && subItem.Text != "New")
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Only Allow New AND Status to verify.");
                            box1.Show();
                        }
                        return;
                    }
                    column++;
                }
            }

            // proceed to verify all new data
            int featureCount = lvResult.Items.Count;

            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
            {
                form.setMinimun(0);
                form.setMaximum(featureCount);
                form.Show();

                int currentCount = 0;

                foreach (ListViewItem item in lvResult.Items)
                {
                    string featurename = "";
                    IGFeature feature = (IGFeature)item.Tag;
                    string strtableName = feature.TableName.ToString();
                    featurename = Getfeaturename(strtableName);

                    switch (featurename)
                    {
                        case "PROPERTY":
                            VerifyNewANDPropery(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        case "BUILING_GROUP":
                            VerifyNewANDBuildingGroup(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        case "BUILING":
                            VerifyNewANDBuilding(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        case "STREET":
                            VerifyNewANDStreet(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        case "JUNCTION":
                            VerifyNewANDJunction(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        case "LANDMARK":
                            VerifyNewANDLandmark(feature.OID);
                            currentCount = currentCount + 1;
                            form.setValue(currentCount);
                            break;

                        default:
                            throw new Exception(string.Format("Unknown feature. {0}", featurename));
                    }
                }
            }
            PopulateFeatureByWA(false);
        }

        // noraini ali - apr 2021
        // verify New Property
        private void VerifyNewANDPropery(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                    GProperty property = repo.GetById<GProperty>(featureOID);
                    GPropertyAND propertyAND = property.GetPropertyANDId(true);
                    if (propertyAND != null)
                    {
                        property.AndStatus = 0;
                        repo.UpdateExt(property, true);
                        repo.Delete(propertyAND);
                        success = true;
                    }    
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        // verify New Building
        private void VerifyNewANDBuilding(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                    GBuilding building = repo.GetById<GBuilding>(featureOID);
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND != null)
                    {
                        building.AndStatus = 0;
                        repo.UpdateExt(building, true);
                        repo.Delete(buildingAND);
                        success = true;
                    }
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        // verify New Building Group
        private void VerifyNewANDBuildingGroup(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                    GBuildingGroup buildingGrp = repo.GetById<GBuildingGroup>(featureOID);
                    GBuildingGroupAND buildingGrpAND = buildingGrp.GetBuildingGroupANDId();
                    if (buildingGrpAND != null)
                    {
                        buildingGrp.AndStatus = 0;
                        repo.UpdateExt(buildingGrp, true);
                        repo.Delete(buildingGrpAND);
                        success = true;
                    }
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        // verify New Landmark
        private void VerifyNewANDLandmark(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                    GLandmark landmark = repo.GetById<GLandmark>(featureOID);
                    GLandmarkAND landmarkAND = landmark.GetLandmarkANDId(true);
                    if (landmarkAND != null)
                    {
                        landmark.AndStatus = 0;
                        repo.UpdateExt(landmark, true);
                        repo.Delete(landmarkAND);
                        success = true;
                    }
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        // verify New Junction
        private void VerifyNewANDJunction(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                    GJunction junction = repo.GetById<GJunction>(featureOID);
                    GJunctionAND junctionAND = junction.GetJunctionANDId(true);
                    if (junctionAND != null)
                    {
                        junction.AndStatus = 0;
                        repo.UpdateExt(junction, true);
                        repo.Delete(junctionAND);
                        success = true;
                    }
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        // verify New Street
        private void VerifyNewANDStreet(int featureOID)
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    GStreet street = repo.GetById<GStreet>(featureOID);
                    GStreetAND streetAND = street.GetStreetANDId(true);
                    if (streetAND != null)
                    {
                        street.AndStatus = 0;
                        repo.UpdateExt(street, true);
                        repo.Delete(streetAND);
                        success = true;
                    }
                }
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        private string Getfeaturename(string tableName)
        {
            string featurename = "";
            bool b1 = tableName.Contains("PROPERTY");
            if (b1)
            {
                featurename = "PROPERTY";
                return featurename;
            }

            b1 = tableName.Contains("BUILDING_GROUP");
            if (b1)
            {
                featurename = "BUILING_GROUP";
                return featurename;
            }

            b1 = tableName.Contains("BUILDING");
            if (b1)
            {
                featurename = "BUILING";
                return featurename;
            }

            b1 = tableName.Contains("LANDMARK");
            if (b1)
            {
                featurename = "LANDMARK";
                return featurename;
            }

            b1 = tableName.Contains("STREET");
            if (b1)
            {
                featurename = "STREET";
                return featurename;
            }

            b1 = tableName.Contains("JUNCTION");
            if (b1)
            {
                featurename = "JUNCTION";
                return featurename;
            }
            return featurename;
        }

        private void EnabledButton(bool flagstatus)
        {
            btnShow.Enabled = flagstatus;
            btnSaveAs.Enabled = flagstatus;
            btnDisplay.Enabled = flagstatus;
        }
        private void EnabledRgButton(bool flagstatus)
        {
            btnStartVerify.Enabled = flagstatus;
            btnCancelVerify.Enabled = flagstatus;
            btnFixBugWA.Enabled = flagstatus;
            btnFilterADD.Enabled = flagstatus;
            btnVerifyADD.Enabled = flagstatus;
        }

        private void InitTextBox()
        {
            txtSegmentName.Text = "";
            txtCompleteFlag.Text = "";
            txtUserName.Text = "";
            txtWorkOrderNo.Text = "";
        }

        // noraini ali - Mei 2021
        // fixed bug from cadu1 problem feature in WA not have WA id
        private void btnFixBugWA_Click(object sender, EventArgs e)
        {
            int featureCount = 0;

            // noraini ali - prompt supervisor to lock WA before proceed to fixed buq or verify
            if (txtCompleteFlag.Text.ToString() == "Closed")
            {
                MessageBox.Show("Work Area currently closed, Please lock this Work Area to proceed verification.");
                return;
            }

            // read WA id
            int? id = StringUtils.CheckInt(cbWorkAreaId.Text);
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (id.HasValue)
            {
                GWorkArea work_area = repo.GetById<GWorkArea>(id.Value);
                if (work_area != null)
                {
                    List<GBuilding> building;
                    List<GLandmark> landmark;
                    List<GBuildingGroup> buildingGroup;
                    List<GProperty> property;
                    List<GJunction> junction;
                    List<GStreet> street;

                    building = repo.SpatialSearch<GBuilding>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    landmark = repo.SpatialSearch<GLandmark>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    buildingGroup = repo.SpatialSearch<GBuildingGroup>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    property = repo.SpatialSearch<GProperty>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    junction = repo.SpatialSearch<GJunction>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    street = repo.SpatialSearch<GStreet>(work_area.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                    featureCount = street.Count() + property.Count() + junction.Count() + building.Count() + landmark.Count() + buildingGroup.Count();

                    string str = "";

                    if (property.Count() > 0)
                        str = str + "\nTotol property : " + property.Count();
                    if (landmark.Count() > 0)
                        str = str + "\nTotol landmark: " + landmark.Count();
                    if (junction.Count() > 0)
                        str = str + "\nTotol junction: " + junction.Count();
                    if (street.Count() > 0)
                        str = str + "\nTotol street: " + street.Count();
                    if (building.Count() > 0)
                        str = str + "\nTotol building: " + building.Count();
                    if (buildingGroup.Count() > 0)
                        str = str + "\nTotol building group: " + buildingGroup.Count();

                    str = str + "\n\nTotol ADM feature : " + featureCount;

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Info feature in Work Area :")
                                  .SetText(str + "\n\nConfirm to proceed ? ");
                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        // feature with area ID, need to remove the area ID from feature
                        int currentCount = 0;
                        int featureANDCount = 0;

                        using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
                        {
                            form.setMinimun(0);
                            form.setMaximum(featureCount);
                            form.Show();

                            property.ForEach(prop =>
                            {
                                prop.AreaId = work_area.OID;                        
                                GPropertyAND propAND = prop.GetPropertyANDId(true);
                                if (propAND != null)
                                {
                                    propAND.AreaId = work_area.OID;
                                    if (propAND.AndStatus > 0)
                                    {
                                        propAND.UpdateStatus = propAND.AndStatus;
                                        prop.UpdateStatus = propAND.AndStatus;
                                        prop.AndStatus = propAND.AndStatus;  
                                    }
                                    repo.UpdateRemainStatus(propAND, true);
                                    featureANDCount = featureANDCount + 1;
                                }
                            
                                repo.UpdateRemainStatus(prop, true);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            });

                            street.ForEach(Street =>
                            {
                                Street.AreaId = work_area.OID;
                                GStreetAND StreetAND = Street.GetStreetANDId(true);
                                if (StreetAND != null)
                                {
                                    StreetAND.AreaId = work_area.OID;
                                    if (StreetAND.AndStatus > 0)
                                    {
                                        StreetAND.UpdateStatus = StreetAND.AndStatus;
                                        Street.UpdateStatus = StreetAND.AndStatus;
                                        Street.AndStatus = StreetAND.AndStatus;
                                    }
                                    repo.UpdateRemainStatus(StreetAND, true);
                                    featureANDCount = featureANDCount + 1;
                                }
                                repo.UpdateRemainStatus(Street, true);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            });

                            junction.ForEach(junc =>
                            {
                                junc.AreaId = work_area.OID;
                                GJunctionAND juncAND = junc.GetJunctionANDId(true);
                                if (juncAND != null)
                                {
                                    juncAND.AreaId = work_area.OID;
                                    if (juncAND.AndStatus > 0)
                                    {
                                        juncAND.UpdateStatus = juncAND.AndStatus;
                                        junc.UpdateStatus = juncAND.AndStatus;
                                        junc.AndStatus = juncAND.AndStatus;
                                    }
                                    repo.UpdateRemainStatus(juncAND, true);
                                    featureANDCount = featureANDCount + 1;
                                }
                                repo.UpdateRemainStatus(junc, true);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            });

                           building.ForEach(build =>
                           {
                               build.AreaId = work_area.OID;
                               GBuildingAND buildAND = build.GetBuildingANDId(true);
                               if (buildAND != null)
                               {
                                   buildAND.AreaId = work_area.OID;
                                   if (buildAND.AndStatus > 0)
                                   {
                                       buildAND.UpdateStatus = buildAND.AndStatus;
                                       build.UpdateStatus = buildAND.AndStatus;
                                       build.AndStatus = buildAND.AndStatus;
                                   }
                                   repo.UpdateRemainStatus(buildAND, true);
                                   featureANDCount = featureANDCount + 1;
                               }
                               repo.UpdateRemainStatus(build, true);
                               currentCount = currentCount + 1;
                               form.setValue(currentCount);
                           });
                           
                           landmark.ForEach(lnd =>
                           {
                               lnd.AreaId = work_area.OID;
                               GLandmarkAND lndAND = lnd.GetLandmarkANDId(true);
                               if (lndAND != null)
                               {
                                   lndAND.AreaId = work_area.OID;
                                   if (lndAND.AndStatus > 0)
                                   {
                                       lndAND.UpdateStatus = lndAND.AndStatus;
                                       lnd.UpdateStatus = lndAND.AndStatus;
                                       lnd.AndStatus = lndAND.AndStatus;
                                   }
                                   repo.UpdateRemainStatus(lndAND, true);
                                   featureANDCount = featureANDCount + 1;
                               }
                               repo.UpdateRemainStatus(lnd, true);
                               currentCount = currentCount + 1;
                               form.setValue(currentCount);
                           });
                           
                           buildingGroup.ForEach(buildGrp =>
                           {
                               buildGrp.AreaId = work_area.OID;
                               GBuildingGroupAND buildGrpAND = buildGrp.GetBuildingGroupANDId(true);
                               if (buildGrpAND != null)
                               {
                                   buildGrpAND.AreaId = work_area.OID;
                                   if (buildGrpAND.AndStatus > 0)
                                   { 
                                       buildGrpAND.UpdateStatus = buildGrpAND.AndStatus;
                                       buildGrp.UpdateStatus = buildGrpAND.AndStatus;
                                       buildGrp.AndStatus = buildGrpAND.AndStatus;
                                   }
                                   repo.UpdateRemainStatus(buildGrpAND, true);
                                   featureANDCount = featureANDCount + 1;
                               }
                               repo.UpdateRemainStatus(buildGrp, true);
                               currentCount = currentCount + 1;
                               form.setValue(currentCount);
                           });
                        }

                        repo.EndTransaction();

                        string str1 = "Total Fixed Bug \nADM Feature : " + featureCount;
                        str1 = str1 + "\nAND Feature : " + featureANDCount;
                        MessageBox.Show(str1);
                    }

                    catch (Exception ex)
                    {
                        using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                        {
                            box.Show();
                        }
                        repo.AbortTransaction();
                    }
                }
            }
        }
    }
}

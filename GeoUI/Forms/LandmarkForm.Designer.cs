﻿namespace Geomatic.UI.Forms
{
    partial class LandmarkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblStreetId;
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtStreetId = new System.Windows.Forms.TextBox();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pagePoi = new System.Windows.Forms.TabPage();
            this.controlPoi = new Geomatic.UI.Controls.ChildControl();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblSection = new System.Windows.Forms.Label();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnSetCode = new System.Windows.Forms.Button();
            this.cbNaviStatus = new System.Windows.Forms.ComboBox();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.categoryControl = new Geomatic.UI.Controls.CodeControl();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            lblStreetId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pagePoi.SuspendLayout();
            this.addressGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 640);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // lblStreetId
            // 
            lblStreetId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetId.AutoSize = true;
            lblStreetId.Location = new System.Drawing.Point(57, 6);
            lblStreetId.Name = "lblStreetId";
            lblStreetId.Size = new System.Drawing.Size(50, 13);
            lblStreetId.TabIndex = 0;
            lblStreetId.Text = "Street Id:";
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel4);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 570);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 12;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(554, 51);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.tableLayoutPanel3);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 526);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(560, 44);
            this.relationGroup.TabIndex = 11;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtStreetId, 1, 0);
            this.tableLayoutPanel3.Controls.Add(lblStreetId, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(554, 25);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtStreetId
            // 
            this.txtStreetId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetId.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetId.Location = new System.Drawing.Point(113, 3);
            this.txtStreetId.Name = "txtStreetId";
            this.txtStreetId.ReadOnly = true;
            this.txtStreetId.Size = new System.Drawing.Size(161, 20);
            this.txtStreetId.TabIndex = 1;
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabAdditional);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 329);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(560, 197);
            this.additionalGroup.TabIndex = 10;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Controls.Add(this.pagePoi);
            this.tabAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAdditional.Location = new System.Drawing.Point(3, 16);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(554, 178);
            this.tabAdditional.TabIndex = 0;
            // 
            // pagePoi
            // 
            this.pagePoi.Controls.Add(this.controlPoi);
            this.pagePoi.Location = new System.Drawing.Point(4, 22);
            this.pagePoi.Name = "pagePoi";
            this.pagePoi.Padding = new System.Windows.Forms.Padding(3);
            this.pagePoi.Size = new System.Drawing.Size(546, 152);
            this.pagePoi.TabIndex = 1;
            this.pagePoi.Text = "POI";
            this.pagePoi.UseVisualStyleBackColor = true;
            // 
            // controlPoi
            // 
            this.controlPoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPoi.Location = new System.Drawing.Point(3, 3);
            this.controlPoi.Name = "controlPoi";
            this.controlPoi.Size = new System.Drawing.Size(540, 146);
            this.controlPoi.TabIndex = 0;
            this.controlPoi.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_AddClick);
            this.controlPoi.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_EditClick);
            this.controlPoi.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_DeleteClick);
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.tableLayoutPanel2);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = false;
            this.addressGroup.Location = new System.Drawing.Point(12, 209);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(560, 120);
            this.addressGroup.TabIndex = 9;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtPostcode, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblPostcode, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtStreetType, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblStreetType, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtSection, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblSection, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblStreetName, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtStreetName, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblStreetName2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtStreetName2, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblState, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtState, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblCity, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtCity, 1, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(554, 101);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.BackColor = System.Drawing.SystemColors.Control;
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(390, 53);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(161, 20);
            this.txtPostcode.TabIndex = 9;
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(329, 56);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 8;
            this.lblPostcode.Text = "Postcode:";
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 3);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(161, 20);
            this.txtStreetType.TabIndex = 1;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(42, 6);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 13);
            this.lblStreetType.TabIndex = 0;
            this.lblStreetType.Text = "Street Type:";
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.BackColor = System.Drawing.SystemColors.Control;
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 53);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(161, 20);
            this.txtSection.TabIndex = 7;
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 56);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 6;
            this.lblSection.Text = "Section:";
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(315, 6);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 2;
            this.lblStreetName.Text = "Street Name:";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(390, 3);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName.TabIndex = 3;
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(309, 31);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 13);
            this.lblStreetName2.TabIndex = 4;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(390, 28);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName2.TabIndex = 5;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(349, 81);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 12;
            this.lblState.Text = "State:";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.BackColor = System.Drawing.SystemColors.Control;
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(390, 78);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(161, 20);
            this.txtState.TabIndex = 13;
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 81);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 10;
            this.lblCity.Text = "City:";
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.BackColor = System.Drawing.SystemColors.Control;
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 78);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(161, 20);
            this.txtCity.TabIndex = 11;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblName, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lblCode, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lblName2, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtName, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.txtName2, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.btnSetCode, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.lblNaviStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 3);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 103);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 106);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(69, 58);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name:";
            // 
            // lblCode
            // 
            this.lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(72, 32);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(35, 13);
            this.lblCode.TabIndex = 2;
            this.lblCode.Text = "Code:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(343, 58);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(44, 13);
            this.lblName2.TabIndex = 7;
            this.lblName2.Text = "Name2:";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(113, 55);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(164, 20);
            this.txtName.TabIndex = 6;
            // 
            // txtName2
            // 
            this.txtName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName2.BackColor = System.Drawing.Color.White;
            this.txtName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName2.Location = new System.Drawing.Point(393, 55);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(164, 20);
            this.txtName2.TabIndex = 8;
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.Location = new System.Drawing.Point(113, 29);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(164, 20);
            this.txtCode.TabIndex = 3;
            // 
            // btnSetCode
            // 
            this.btnSetCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSetCode.Location = new System.Drawing.Point(283, 29);
            this.btnSetCode.Name = "btnSetCode";
            this.btnSetCode.Size = new System.Drawing.Size(32, 20);
            this.btnSetCode.TabIndex = 4;
            this.btnSetCode.Text = "...";
            this.btnSetCode.UseVisualStyleBackColor = true;
            this.btnSetCode.Click += new System.EventHandler(this.btnSetCode_Click);
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(393, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(164, 21);
            this.cbNaviStatus.TabIndex = 1;
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(293, 6);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 0;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(393, 81);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(164, 21);
            this.cbSource.TabIndex = 10;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(343, 85);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 9;
            this.lblSource.Text = "Source:";
            // 
            // categoryControl
            // 
            this.categoryControl.Code1Text = "";
            this.categoryControl.Code2Text = "";
            this.categoryControl.Code3Text = "";
            this.categoryControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.categoryControl.Location = new System.Drawing.Point(12, 51);
            this.categoryControl.Name = "categoryControl";
            this.categoryControl.Size = new System.Drawing.Size(560, 52);
            this.categoryControl.TabIndex = 7;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // LandmarkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 677);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.categoryControl);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "LandmarkForm";
            this.Text = "LandmarkForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.categoryControl, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pagePoi.ResumeLayout(false);
            this.addressGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pagePoi;
        protected Controls.ChildControl controlPoi;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.Label lblCode;
        protected System.Windows.Forms.Label lblName2;
        protected System.Windows.Forms.TextBox txtName;
        protected System.Windows.Forms.TextBox txtName2;
        protected System.Windows.Forms.TextBox txtCode;
        protected System.Windows.Forms.Button btnSetCode;
        protected System.Windows.Forms.ComboBox cbNaviStatus;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.Label lblSource;
        protected Controls.CodeControl categoryControl;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.TextBox txtStreetId;
    }
}
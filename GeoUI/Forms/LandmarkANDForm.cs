﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.UI.Forms
{
    public partial class LandmarkANDForm : CollapsibleForm
    {
        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string SelectedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string InsertedName2
        {
            get { return txtName2.Text; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        protected GLandmarkAND _landmarkAND;

        public LandmarkANDForm()
            : this(null)
        {
        }

        public LandmarkANDForm(GLandmarkAND landmark)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
                btnSetCode,
                lblCode, txtCode,
                lblName, txtName,
                lblName2, txtName2,
                lblSource, cbSource,
                btnApply);

            _landmarkAND = landmark;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);

            controlPoi.Columns.Add("Id");
            controlPoi.Columns.Add("Code");
            controlPoi.Columns.Add("Name");
            controlPoi.Columns.Add("Name2");
            controlPoi.Columns.Add("Navi_Status");
            controlPoi.Columns.Add("Description");
            controlPoi.Columns.Add("Url");

            txtId.Text = _landmarkAND.OriId.ToString();

            cbNaviStatus.Text = _landmarkAND.NavigationStatusValue;
            txtName.Text = _landmarkAND.Name;
            txtName2.Text = _landmarkAND.Name2;
            txtCode.Text = _landmarkAND.Code;
            GCode1 code1 = _landmarkAND.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _landmarkAND.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _landmarkAND.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            cbSource.Text = _landmarkAND.SourceValue;
            txtDateCreated.Text = _landmarkAND.DateCreated;
            txtDateUpdated.Text = _landmarkAND.DateUpdated;
            txtCreator.Text = _landmarkAND.CreatedBy;
            txtUpdater.Text = _landmarkAND.UpdatedBy;

            GStreet street = _landmarkAND.GetStreet();
            if (street != null)
            {
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null)
                {
                    txtStreetId.Text = streetAND.OriId.ToString();
                    txtStreetType.Text = streetAND.TypeValue;
                    txtStreetName.Text = streetAND.Name;
                    txtStreetName2.Text = streetAND.Name2;
                    txtCity.Text = streetAND.City;
                    txtSection.Text = streetAND.Section;
                    txtState.Text = streetAND.State;
                }
                else
                {
                    txtStreetId.Text = street.OID.ToString();
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtCity.Text = street.City;
                    txtSection.Text = street.Section;
                    txtState.Text = street.State;
                }
            }
            if (Session.User.GetGroup().Name != "AND")
            {
                PopulatePoi();
            }
        }

        public void disablePOIEdit()
        {
            controlPoi.Enabled = false;
        }

        protected void PopulatePoi()
        {
            controlPoi.BeginUpdate();
            controlPoi.Sorting = SortOrder.None;
            controlPoi.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _landmarkAND.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.NavigationStatusValue);
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);
                item.Tag = poi;

                items.Add(item);
            }
            controlPoi.Items.AddRange(items.ToArray());
            controlPoi.FixColumnWidth();
            controlPoi.EndUpdate();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Name
            bool isNameValid = LandmarkValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Name 2
            bool isName2Valid = LandmarkValidator.CheckName2(txtName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Source
            bool isSourceValid = LandmarkValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _landmarkAND.NavigationStatus = SelectedNavigationStatus;
            _landmarkAND.Code = SelectedCode;
            _landmarkAND.Name = InsertedName;
            _landmarkAND.Name2 = InsertedName2;
            _landmarkAND.Source = SelectedSource;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                GCode1 code1 = code3.GetCode1();
                GCode2 code2 = code3.GetCode2();
                categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
                categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
                categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            }
        }
    }
}

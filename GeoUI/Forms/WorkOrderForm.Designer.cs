﻿namespace Geomatic.UI.Forms
{
    partial class WorkOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameTextbox = new System.Windows.Forms.TextBox();
            this.prtnameLabel = new System.Windows.Forms.Label();
            this.prtnameCombobox = new System.Windows.Forms.ComboBox();
            this.wourgencyLabel = new System.Windows.Forms.Label();
            this.wourgencyCombobox = new System.Windows.Forms.ComboBox();
            this.wotypeLabel = new System.Windows.Forms.Label();
            this.wotypeTextbox = new System.Windows.Forms.TextBox();
            this.requireddateLabel = new System.Windows.Forms.Label();
            this.requireddateDatepicker = new System.Windows.Forms.DateTimePicker();
            this.wosummaryLabel = new System.Windows.Forms.Label();
            this.wosummaryTextbox = new System.Windows.Forms.TextBox();
            this.wodescriptionLabel = new System.Windows.Forms.Label();
            this.wodescriptionTextbox = new System.Windows.Forms.TextBox();
            this.requiredLabel = new System.Windows.Forms.Label();
            this.formLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.None;
            this.bottomPanel.Location = new System.Drawing.Point(18, 310);
            this.bottomPanel.Size = new System.Drawing.Size(680, 57);
            // 
            // formLayoutPanel
            // 
            this.formLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.formLayoutPanel.ColumnCount = 4;
            this.formLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.formLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.formLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.formLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.formLayoutPanel.Controls.Add(this.usernameLabel, 0, 0);
            this.formLayoutPanel.Controls.Add(this.usernameTextbox, 1, 0);
            this.formLayoutPanel.Controls.Add(this.prtnameLabel, 2, 0);
            this.formLayoutPanel.Controls.Add(this.prtnameCombobox, 3, 0);
            this.formLayoutPanel.Controls.Add(this.wourgencyLabel, 0, 1);
            this.formLayoutPanel.Controls.Add(this.wourgencyCombobox, 1, 1);
            this.formLayoutPanel.Controls.Add(this.wotypeLabel, 0, 2);
            this.formLayoutPanel.Controls.Add(this.wotypeTextbox, 1, 2);
            this.formLayoutPanel.Controls.Add(this.requireddateLabel, 2, 2);
            this.formLayoutPanel.Controls.Add(this.requireddateDatepicker, 3, 2);
            this.formLayoutPanel.Controls.Add(this.wosummaryLabel, 0, 3);
            this.formLayoutPanel.Controls.Add(this.wosummaryTextbox, 1, 3);
            this.formLayoutPanel.Controls.Add(this.wodescriptionLabel, 2, 3);
            this.formLayoutPanel.Controls.Add(this.wodescriptionTextbox, 3, 3);
            this.formLayoutPanel.Controls.Add(this.requiredLabel, 0, 4);
            this.formLayoutPanel.Location = new System.Drawing.Point(18, 16);
            this.formLayoutPanel.Name = "formLayoutPanel";
            this.formLayoutPanel.RowCount = 5;
            this.formLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.formLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.formLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.formLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.formLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.formLayoutPanel.Size = new System.Drawing.Size(680, 277);
            this.formLayoutPanel.TabIndex = 4;
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(3, 21);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(60, 13);
            this.usernameLabel.TabIndex = 0;
            this.usernameLabel.Text = "User Name";
            // 
            // usernameTextbox
            // 
            this.usernameTextbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.usernameTextbox.Location = new System.Drawing.Point(105, 17);
            this.usernameTextbox.Name = "usernameTextbox";
            this.usernameTextbox.ReadOnly = true;
            this.usernameTextbox.Size = new System.Drawing.Size(200, 20);
            this.usernameTextbox.TabIndex = 1;
            // 
            // prtnameLabel
            // 
            this.prtnameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.prtnameLabel.AutoSize = true;
            this.prtnameLabel.Location = new System.Drawing.Point(343, 21);
            this.prtnameLabel.Name = "prtnameLabel";
            this.prtnameLabel.Size = new System.Drawing.Size(67, 13);
            this.prtnameLabel.TabIndex = 2;
            this.prtnameLabel.Text = "* PRT Name";
            // 
            // prtnameCombobox
            // 
            this.prtnameCombobox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.prtnameCombobox.FormattingEnabled = true;
            this.prtnameCombobox.Items.AddRange(new object[] {
            "pas",
            "pbp",
            "pjb",
            "pjp",
            "pkg",
            "pkk",
            "pkl",
            "pkn",
            "pmk",
            "ppg",
            "ppj",
            "psb",
            "pst",
            "ptg",
            "msc",
            "tpg",
            "kel",
            "rsh",
            "tl",
            "sbh",
            "kg",
            "mr"});
            this.prtnameCombobox.Location = new System.Drawing.Point(445, 17);
            this.prtnameCombobox.Name = "prtnameCombobox";
            this.prtnameCombobox.Size = new System.Drawing.Size(200, 21);
            this.prtnameCombobox.TabIndex = 3;
            // 
            // wourgencyLabel
            // 
            this.wourgencyLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wourgencyLabel.AutoSize = true;
            this.wourgencyLabel.Location = new System.Drawing.Point(3, 76);
            this.wourgencyLabel.Name = "wourgencyLabel";
            this.wourgencyLabel.Size = new System.Drawing.Size(69, 13);
            this.wourgencyLabel.TabIndex = 4;
            this.wourgencyLabel.Text = "WO Urgency";
            // 
            // wourgencyCombobox
            // 
            this.wourgencyCombobox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wourgencyCombobox.FormattingEnabled = true;
            this.wourgencyCombobox.Items.AddRange(new object[] {
            "Low",
            "Normal",
            "Urgent",
            "Adhoc"});
            this.wourgencyCombobox.Location = new System.Drawing.Point(105, 72);
            this.wourgencyCombobox.Name = "wourgencyCombobox";
            this.wourgencyCombobox.Size = new System.Drawing.Size(200, 21);
            this.wourgencyCombobox.TabIndex = 5;
            // 
            // wotypeLabel
            // 
            this.wotypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wotypeLabel.AutoSize = true;
            this.wotypeLabel.Location = new System.Drawing.Point(3, 131);
            this.wotypeLabel.Name = "wotypeLabel";
            this.wotypeLabel.Size = new System.Drawing.Size(53, 13);
            this.wotypeLabel.TabIndex = 6;
            this.wotypeLabel.Text = "WO Type";
            // 
            // wotypeTextbox
            // 
            this.wotypeTextbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wotypeTextbox.Location = new System.Drawing.Point(105, 127);
            this.wotypeTextbox.Name = "wotypeTextbox";
            this.wotypeTextbox.ReadOnly = true;
            this.wotypeTextbox.Size = new System.Drawing.Size(200, 20);
            this.wotypeTextbox.TabIndex = 7;
            this.wotypeTextbox.Text = "Admin";
            // 
            // requireddateLabel
            // 
            this.requireddateLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.requireddateLabel.AutoSize = true;
            this.requireddateLabel.Location = new System.Drawing.Point(343, 131);
            this.requireddateLabel.Name = "requireddateLabel";
            this.requireddateLabel.Size = new System.Drawing.Size(76, 13);
            this.requireddateLabel.TabIndex = 8;
            this.requireddateLabel.Text = "Required Date";
            // 
            // requireddateDatepicker
            // 
            this.requireddateDatepicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.requireddateDatepicker.Enabled = false;
            this.requireddateDatepicker.Location = new System.Drawing.Point(445, 127);
            this.requireddateDatepicker.Name = "requireddateDatepicker";
            this.requireddateDatepicker.Size = new System.Drawing.Size(200, 20);
            this.requireddateDatepicker.TabIndex = 9;
            // 
            // wosummaryLabel
            // 
            this.wosummaryLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wosummaryLabel.AutoSize = true;
            this.wosummaryLabel.Location = new System.Drawing.Point(3, 200);
            this.wosummaryLabel.Name = "wosummaryLabel";
            this.wosummaryLabel.Size = new System.Drawing.Size(70, 13);
            this.wosummaryLabel.TabIndex = 10;
            this.wosummaryLabel.Text = "Wo Summary";
            // 
            // wosummaryTextbox
            // 
            this.wosummaryTextbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wosummaryTextbox.Location = new System.Drawing.Point(105, 168);
            this.wosummaryTextbox.Multiline = true;
            this.wosummaryTextbox.Name = "wosummaryTextbox";
            this.wosummaryTextbox.Size = new System.Drawing.Size(200, 77);
            this.wosummaryTextbox.TabIndex = 11;
            // 
            // wodescriptionLabel
            // 
            this.wodescriptionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wodescriptionLabel.AutoSize = true;
            this.wodescriptionLabel.Location = new System.Drawing.Point(343, 200);
            this.wodescriptionLabel.Name = "wodescriptionLabel";
            this.wodescriptionLabel.Size = new System.Drawing.Size(89, 13);
            this.wodescriptionLabel.TabIndex = 12;
            this.wodescriptionLabel.Text = "* WO Description";
            // 
            // wodescriptionTextbox
            // 
            this.wodescriptionTextbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wodescriptionTextbox.Location = new System.Drawing.Point(445, 168);
            this.wodescriptionTextbox.Multiline = true;
            this.wodescriptionTextbox.Name = "wodescriptionTextbox";
            this.wodescriptionTextbox.Size = new System.Drawing.Size(200, 77);
            this.wodescriptionTextbox.TabIndex = 13;
            // 
            // requiredLabel
            // 
            this.requiredLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.requiredLabel.AutoSize = true;
            this.requiredLabel.Location = new System.Drawing.Point(3, 264);
            this.requiredLabel.Name = "requiredLabel";
            this.requiredLabel.Size = new System.Drawing.Size(57, 13);
            this.requiredLabel.TabIndex = 14;
            this.requiredLabel.Text = "* Required";
            // 
            // WorkOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 399);
            this.Controls.Add(this.formLayoutPanel);
            this.Name = "WorkOrderForm";
            this.Text = "WorkOrderForm";
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.Controls.SetChildIndex(this.formLayoutPanel, 0);
            this.formLayoutPanel.ResumeLayout(false);
            this.formLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel formLayoutPanel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameTextbox;
        private System.Windows.Forms.Label prtnameLabel;
        private System.Windows.Forms.ComboBox prtnameCombobox;
        private System.Windows.Forms.Label wourgencyLabel;
        private System.Windows.Forms.ComboBox wourgencyCombobox;
        private System.Windows.Forms.Label wotypeLabel;
        private System.Windows.Forms.TextBox wotypeTextbox;
        private System.Windows.Forms.Label requireddateLabel;
        private System.Windows.Forms.DateTimePicker requireddateDatepicker;
        private System.Windows.Forms.Label wosummaryLabel;
        private System.Windows.Forms.TextBox wosummaryTextbox;
        private System.Windows.Forms.Label wodescriptionLabel;
        private System.Windows.Forms.TextBox wodescriptionTextbox;
        private System.Windows.Forms.Label requiredLabel;
    }
}
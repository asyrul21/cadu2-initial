﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms
{
    public partial class TollPriceForm : CollapsibleForm
    {
        protected GTollPrice _tollPrice;

        public int? SelectedVehicleType
        {
            get { return (cbVehicleType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GVehicleType>)cbVehicleType.SelectedItem).Value.Code; }
        }

        public TollPriceForm()
            : this(null)
        {
        }

        public TollPriceForm(GTollPrice tollPrice)
        {
            InitializeComponent();
            _tollPrice = tollPrice;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Toll Price Form");

            ComboBoxUtils.PopulateVehicleType(cbVehicleType);

            txtId.Text = _tollPrice.OID.ToString();
            txtTollRouteID.Text = _tollPrice.TollRouteID.ToString();
            cbVehicleType.Text = _tollPrice.VehicleTypeValue;
            numPrice.Value = (_tollPrice.Price.HasValue) ? _tollPrice.Price.Value : 0;

            txtDateCreated.Text = _tollPrice.DateCreated;
            txtDateUpdated.Text = _tollPrice.DateUpdated;
            txtCreator.Text = _tollPrice.CreatedBy;
            txtUpdater.Text = _tollPrice.UpdatedBy;
        }

        public void SetValues()
        {
            _tollPrice.VehicleType = SelectedVehicleType;
            _tollPrice.Price = numPrice.Value;
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Vehicle Type
            bool isTypeValid = TollPriceValidator.CheckVehicleType(SelectedVehicleType);
            LabelColor(lblVehicleType, isTypeValid);
            pass &= isTypeValid;

            // Out Price
            bool isPriceValid = TollPriceValidator.CheckPrice(numPrice.Value);
            LabelColor(lblPrice, isPriceValid);
            pass &= isPriceValid;

            // check if route already exists
            RepositoryFactory repo = new RepositoryFactory();
            Query<GTollPrice> query = new Query<GTollPrice>();
            query.Obj.TollRouteID = _tollPrice.TollRouteID;
            query.Obj.VehicleType = SelectedVehicleType;

            bool isOk = repo.Count(query) == 0;

            pass &= isOk;

            if (!isOk)
                throw new QualityControlException("Vehicle type already exists.");

            return pass;
        }

    }
}

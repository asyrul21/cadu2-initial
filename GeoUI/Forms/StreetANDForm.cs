﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;

namespace Geomatic.UI.Forms
{
    public partial class StreetANDForm : CollapsibleForm
    {
        #region Properties

        public int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public int? SelectedStatus
        {
            get { return (cbStatus.SelectedItem == null) ? (int?)null : ((GStreetStatus)cbStatus.SelectedItem).Code; }
        }

        public int? SelectedNetworkClass
        {
            get { return (cbNetworkClass.SelectedItem == null) ? (int?)null : ((GStreetNetworkClass)cbNetworkClass.SelectedItem).Code; }
        }

        public int? SelectedClass
        {
            get { return (cbClass.SelectedItem == null) ? (int?)null : ((GStreetClass)cbClass.SelectedItem).Code; }
        }

        public int? SelectedCategory
        {
            get { return (cbCategory.SelectedItem == null) ? (int?)null : ((GStreetCategory)cbCategory.SelectedItem).Code; }
        }

        public int? SelectedFilterLevel
        {
            get { return (cbFilterLevel.SelectedItem == null) ? (int?)null : ((GStreetFilterLevel)cbFilterLevel.SelectedItem).Code; }
        }

        public int? SelectedDesign
        {
            get { return (cbDesign.SelectedItem == null) ? (int?)null : ((GStreetDesign)cbDesign.SelectedItem).Code; }
        }

        public int? SelectedDirection
        {
            get { return (cbDirection.SelectedItem == null) ? (int?)null : ((GStreetDirection)cbDirection.SelectedItem).Code; }
        }

        public int? SelectedTollType
        {
            get { return (cbTollType.SelectedItem == null) ? (int?)null : ((GStreetTollType)cbTollType.SelectedItem).Code; }
        }

        public int? SelectedDivider
        {
            get { return (cbDivider.SelectedItem == null) ? (int?)null : ((GStreetDivider)cbDivider.SelectedItem).Code; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((GStreetType)cbType.SelectedItem).Code; }
        }

        public string InsertedName
        {
            get { return cbName.Text; }
        }

        public string InsertedName2
        {
            get { return cbName2.Text; }
        }

        public string InsertedSection
        {
            get { return txtSection.Text; }
        }

        public string InsertedPostCode
        {
            get { return txtPostcode.Text; }
        }

        public string InsertedCity
        {
            get { return txtCity.Text; }
        }

        public string InsertedSubCity
        {
            get { return txtSubCity.Text; }
        }

        public string InsertedState
        {
            get { return txtState.Text; }
        }

        public int? InsertedSpeedLimit
        {
            get { return numSpeedLimit.Value; }
        }

        public double? InsertedWeight
        {
            get { return numWeight.Value; }
        }

        public int? InsertedElevation
        {
            get { return numElevation.Value; }
        }

        public double? InsertedLength
        {
            get { return numLength.Value; }
        }

        public double? InsertedWidth
        {
            get { return numWidth.Value; }
        }

        public double? InsertedHeight
        {
            get { return numHeight.Value; }
        }

        public int? InsertedLanes
        {
            get { return numLanes.Value; }
        }

        public int? Bicycle
        {
            get { return (!chkBicycle.Checked) ? 1 : 0; }
        }

        public int? Bus
        {
            get { return (!chkBus.Checked) ? 1 : 0; }
        }

        public int? Car
        {
            get { return (!chkCar.Checked) ? 1 : 0; }
        }

        public int? Delivery
        {
            get { return (!chkDelivery.Checked) ? 1 : 0; }
        }

        public int? Emergency
        {
            get { return (!chkEmergency.Checked) ? 1 : 0; }
        }

        public int? Pedestrian
        {
            get { return (!chkPedestrian.Checked) ? 1 : 0; }
        }

        public int? Truck
        {
            get { return (!chkTruck.Checked) ? 1 : 0; }
        }

        //asyrul
        public bool ShowText
        {
            get { return ckShowText.Checked; }
        }

        #endregion

        protected GStreetAND _streetAND;

        public StreetANDForm()
            : this(null)
        {
        }

        public StreetANDForm(GStreetAND street)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus,
                lblNetworkClass, cbNetworkClass,
                lblClass, cbClass,
                lblCategory, cbCategory,
                lblDivider, cbDivider);
            _commandPool.Register(Command.EditableItem,
                lblConstructionStatus, cbConstructionStatus,
                lblStatus, cbStatus,
                lblNaviStatus, cbNaviStatus,
                lblNetworkClass, cbNetworkClass,
                lblClass, cbClass,
                lblCategory, cbCategory,
                lblFilterLevel, cbFilterLevel,
                lblDesign, cbDesign,
                lblDirection, cbDirection,
                lblTollType, cbTollType,
                lblSource, cbSource,
                lblType, cbType,
                lblName, cbName,
                lblName2, cbName2,
                lblSection, txtSection,
                lblPostcode, txtPostcode,
                lblCity, txtCity,
                lblSubCity, txtSubCity,
                lblState, txtState,
                lblSpeedLimit, numSpeedLimit,
                lblMaxWeight, numWeight,
                lblElevation, numElevation,
                lblLength, numLength,
                lblWidth, numWidth,
                lblHeight, numHeight,
                lblLanes, numLanes,
                btnPickLocation,
                chkBicycle,
                chkBus,
                chkCar,
                chkDelivery,
                chkEmergency,
                chkPedestrian,
                chkTruck,
                btnApply);

            _streetAND = street;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateStreetStatus(cbStatus);
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateStreetNetworkClass(cbNetworkClass);
            ComboBoxUtils.PopulateStreetClass(cbClass);
            ComboBoxUtils.PopulateStreetCategory(cbCategory);
            ComboBoxUtils.PopulateStreetFilterLevel(cbFilterLevel);
            ComboBoxUtils.PopulateStreetDesign(cbDesign);
            ComboBoxUtils.PopulateStreetDirection(cbDirection);
            ComboBoxUtils.PopulateStreetTollType(cbTollType);
            ComboBoxUtils.PopulateStreetDivider(cbDivider);
            ComboBoxUtils.PopulateStreetType(cbType);

            txtId.Text = _streetAND.OriId.ToString();

            txtFromId.Text = _streetAND.FromNodeId.ToString();
            txtToId.Text = _streetAND.ToNodeId.ToString();

            cbConstructionStatus.Text = _streetAND.ConstructionStatusValue;
            cbNaviStatus.Text = _streetAND.NavigationStatusValue;
            cbStatus.Text = _streetAND.StatusValue;
            cbNetworkClass.Text = _streetAND.NetworkClassValue;
            cbClass.Text = _streetAND.ClassValue;
            cbCategory.Text = _streetAND.CategoryValue;
            cbFilterLevel.Text = _streetAND.FilterLevelValue;
            cbDesign.Text = _streetAND.DesignValue;
            cbDirection.Text = _streetAND.DirectionValue;
            cbTollType.Text = _streetAND.TollTypeValue;
            cbDivider.Text = _streetAND.DividerValue;
            cbSource.Text = _streetAND.SourceValue;

            cbType.Text = _streetAND.TypeValue;
            cbName.Text = _streetAND.Name;
            cbName2.Text = _streetAND.Name2;
            txtSection.Text = _streetAND.Section;
            txtPostcode.Text = _streetAND.Postcode;
            txtCity.Text = _streetAND.City;
            txtSubCity.Text = _streetAND.SubCity;
            txtState.Text = _streetAND.State;

            numSpeedLimit.Value = (_streetAND.SpeedLimit.HasValue) ? _streetAND.SpeedLimit.Value : 0;
            numWeight.Value = (_streetAND.Weight.HasValue) ? _streetAND.Weight.Value : 0;
            numElevation.Value = (_streetAND.Elevation.HasValue) ? _streetAND.Elevation.Value : 0;
            numLength.Value = (_streetAND.Length.HasValue) ? _streetAND.Length.Value : 0;
            numWidth.Value = (_streetAND.Width.HasValue) ? _streetAND.Width.Value : 0;
            numHeight.Value = (_streetAND.Height.HasValue) ? _streetAND.Height.Value : 0;
            numLanes.Value = (_streetAND.NumOfLanes.HasValue) ? _streetAND.NumOfLanes.Value : 1;

            chkBicycle.Checked = (_streetAND.Bicycle != 1);
            chkBus.Checked = (_streetAND.Bus != 1);
            chkCar.Checked = (_streetAND.Car != 1);
            chkDelivery.Checked = (_streetAND.Delivery != 1);
            chkEmergency.Checked = (_streetAND.Emergency != 1);
            chkPedestrian.Checked = (_streetAND.Pedestrian != 1);
            chkTruck.Checked = (_streetAND.Truck != 1);

            // added by noraini 15/5/2019
            if (Name == "AddStreetForm")
                ckShowText.Checked = true;
            else
            {
                if (_streetAND.HasText())
                    ckShowText.Checked = true;
                else
                    ckShowText.Checked = false;
            }
            // end

            txtCreator.Text = _streetAND.CreatedBy;
            txtDateCreated.Text = _streetAND.DateCreated;
            txtUpdater.Text = _streetAND.UpdatedBy;
            txtDateUpdated.Text = _streetAND.DateUpdated;
        }

        private void btnPickLocation_Click(object sender, EventArgs e)
        {
            using (ChooseLocationForm form = new ChooseLocationForm(_streetAND.SegmentName))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GLocation location = form.SelectedLocation;
                txtSection.Text = location.Section;
                txtCity.Text = location.City;
                txtState.Text = location.State;
            }
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Construction Status
            bool isConstructionStatusValid = StreetValidator.CheckConstructionStatus(SelectedConstructionStatus);
            LabelColor(lblConstructionStatus, isConstructionStatusValid);
            pass &= isConstructionStatusValid;

            // Status
            bool isStatusValid = StreetValidator.CheckStatus(SelectedStatus);
            LabelColor(lblStatus, isStatusValid);
            pass &= isStatusValid;

            // Filter Level
            bool isFilterLevelValid = StreetValidator.CheckFilterLevel(SelectedFilterLevel);
            LabelColor(lblFilterLevel, isFilterLevelValid);
            pass &= isFilterLevelValid;

            // Design
            bool isDesignValid = StreetValidator.CheckDesign(SelectedDesign);
            LabelColor(lblDesign, isDesignValid);
            pass &= isDesignValid;

            // Direction
            bool isDirectionValid = StreetValidator.CheckDirection(SelectedDirection);
            LabelColor(lblDirection, isDirectionValid);
            pass &= isDirectionValid;

            // Toll Type
            bool isTollTypeValid = StreetValidator.CheckTollType(SelectedTollType);
            LabelColor(lblTollType, isTollTypeValid);
            pass &= isTollTypeValid;

            // Divider
            bool isDividerValid = StreetValidator.CheckDivider(SelectedDivider);
            LabelColor(lblDivider, isDividerValid);
            pass &= isDividerValid;

            // Lane
            bool isLaneValid = StreetValidator.CheckLane(numLanes.Value);
            LabelColor(lblLanes, isLaneValid);
            pass &= isLaneValid;

            // Type
            bool isTypeValid = StreetValidator.CheckType(SelectedType);
            LabelColor(lblType, isTypeValid);
            //isTypeValid &= isTypeValid;
            pass &= isTypeValid;

            // Name
            bool isNameValid = StreetValidator.CheckName(SelectedStatus, cbName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;
            
            // Name2
            bool isName2Valid = StreetValidator.CheckName2(cbName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Postcode
            bool isPostcodeValid = StreetValidator.CheckPostcode(txtPostcode.Text); ;
            LabelColor(lblPostcode, isPostcodeValid);
            pass &= isPostcodeValid;

            // City
            bool isCityValid = StreetValidator.CheckCity(txtCity.Text);
            LabelColor(lblCity, isCityValid);
            pass &= isCityValid;

            // Sub City
            bool isSubCityValid = StreetValidator.CheckSubCity(txtSubCity.Text);
            LabelColor(lblSubCity, isSubCityValid);
            pass &= isSubCityValid;

            // State
            bool isStateValid = StreetValidator.CheckState(txtState.Text);
            LabelColor(lblState, isStateValid);
            pass &= isStateValid;

            return pass;
        }

        public void SetValues()
        {
            _streetAND.ConstructionStatus = SelectedConstructionStatus;
            _streetAND.NavigationStatus = SelectedNavigationStatus;
            _streetAND.Status = SelectedStatus;
            _streetAND.NetworkClass = SelectedNetworkClass;
            _streetAND.Class = SelectedClass;
            _streetAND.Category = SelectedCategory;
            _streetAND.FilterLevel = SelectedFilterLevel;
            _streetAND.Design = SelectedDesign;
            _streetAND.Direction = SelectedDirection;
            _streetAND.TollType = SelectedTollType;
            _streetAND.Divider = SelectedDivider;
            _streetAND.Source = SelectedSource;

            _streetAND.Type = SelectedType;
            _streetAND.Name = InsertedName;
            _streetAND.Name2 = InsertedName2;
            _streetAND.Section = InsertedSection;
            _streetAND.Postcode = InsertedPostCode;
            _streetAND.City = InsertedCity;
            _streetAND.SubCity = InsertedSubCity;
            _streetAND.State = InsertedState;

            _streetAND.SpeedLimit = InsertedSpeedLimit;
            _streetAND.Weight = InsertedWeight;
            _streetAND.Elevation = InsertedElevation;
            _streetAND.Length = InsertedLength;
            _streetAND.Width = InsertedWidth;
            _streetAND.Height = InsertedHeight;
            _streetAND.NumOfLanes = InsertedLanes;

            _streetAND.Bicycle = Bicycle;
            _streetAND.Bus = Bus;
            _streetAND.Car = Car;
            _streetAND.Delivery = Delivery;
            _streetAND.Emergency = Emergency;
            _streetAND.Pedestrian = Pedestrian;
            _streetAND.Truck = Truck;
        }
    }
}

﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System.Linq;

namespace Geomatic.UI.Forms
{
    public partial class VerificationForm : BaseForm
    {
        public SegmentName SegmentName { get; private set; }
        public int verifystatus = -1;
        public int revertstatus = -1;
        string str1 = "";
        string str2 = "";
        //private int FeatureANDId = 0;

        //string[] propertyAttName = { };
        //string[] propertyAttName = new string[]
        //{
        //    "OBJECT ID",
        //    "AREA_ID",
        //    "AND_STATUS",
        //    "HOUSE NO"
        //};

        public VerificationForm(string featureId, string featurename, string WASegmentName)
        {
            InitializeComponent();
            txtFeatureId.Text = featureId;
            txtFeatureName.Text = featurename;
            SegmentName = GetSegmentName(WASegmentName);
            Form_Load();
        }

        protected void Form_Load()
        {
            RefreshTitle("AND_Verification Form");

            listView1.Columns.Add("Field Name", 120);
            listView1.Columns.Add("Old (Original Data)", 120);
            listView1.Columns.Add("New (Update From AND)", 150);

            listView1.BeginUpdate();
            listView1.Sorting = SortOrder.None;
            listView1.Items.Clear();

            if (txtFeatureName.Text == StringAttributeName.Property)
                ListAttributeProperty();
            else if (txtFeatureName.Text == StringAttributeName.Building)
                ListAttributeBuilding();
            else if (txtFeatureName.Text == StringAttributeName.Landmark)
                ListAttributeLandmark();
            else if (txtFeatureName.Text == StringAttributeName.Street)
                ListAttributeStreet();
            else if (txtFeatureName.Text == StringAttributeName.Junction)
                ListAttributeJunction();
            else if (txtFeatureName.Text == StringAttributeName.BuildingGroup)
                ListAttributeBuildingGroup();

            if (listView1.Items.Count > 0)
            {
                listView1.Items[0].Selected = true;
                if (!listView1.Focused)
                {
                    listView1.Focus();
                }
            }
        }

        #region listAttribute Feature Belong to Selected Work Area Id
        private void ListAttributeProperty()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GProperty property = repo.GetById<GProperty>(id.Value);
                GPropertyAND propertyAND = property.GetPropertyANDId(true);

                //foreach (GPropertyAND propertyAND in property.GetPropertyAND())
                //{
                List<string> list = new List<string>();
                list.Add(property.OID.ToString());
                list.Add(property.AreaId.ToString());
                //list.Add(property.AndStatus.ToString());
                list.Add(GetnameAndStatus(property.AndStatus));
                string House = property.House ?? string.Empty;
                list.Add(House);
                //list.Add(property.House);
                string Lot = property.Lot ?? string.Empty;
                list.Add(Lot);
                //list.Add(property.Lot);
                list.Add(property.TypeValue.ToString());
                list.Add(property.NumberOfFloor.ToString());
                list.Add(property.YearInstall.ToString());
                list.Add(property.ConstructionStatusValue.ToString());
                list.Add(property.SourceValue.ToString());
                list.Add(property.StreetId.ToString());
                list.Add(property.DateCreated);
                list.Add(property.DateUpdated);
                list.Add(property.X.ToString());
                list.Add(property.Y.ToString());

                if (propertyAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(propertyAND.OriId.ToString());
                    listAND.Add(propertyAND.AreaId.ToString());
                    //listAND.Add(propertyAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(propertyAND.AndStatus));
                    string HouseAND = propertyAND.House ?? string.Empty;
                    listAND.Add(HouseAND);
                    //list.Add(propertyAND.House);
                    string LotAND = propertyAND.Lot ?? string.Empty;
                    listAND.Add(LotAND);
                    //list.Add(propertyAND.Lot);
                    listAND.Add(propertyAND.TypeValue.ToString());
                    listAND.Add(propertyAND.NumberOfFloor.ToString());
                    listAND.Add(propertyAND.YearInstall.ToString());
                    listAND.Add(propertyAND.ConstructionStatusValue.ToString());
                    listAND.Add(propertyAND.SourceValue.ToString());
                    listAND.Add(propertyAND.StreetId.ToString());
                    listAND.Add(propertyAND.DateCreated);
                    listAND.Add(propertyAND.DateUpdated);
                    listAND.Add(propertyAND.X.ToString());
                    listAND.Add(propertyAND.Y.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemProperty(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();

                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add(" ");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemProperty(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        private void ListAttributeBuilding()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GBuilding building = repo.GetById<GBuilding>(id.Value);
                GBuildingAND buildingAND = building.GetBuildingANDId(true);

                //foreach (GBuildingAND buildingAND in building.GetBuildingAND())
                //{
                List<string> list = new List<string>();
                list.Add(building.OID.ToString());
                list.Add(building.AreaId.ToString());
                //list.Add(building.AndStatus.ToString());
                list.Add(GetnameAndStatus(building.AndStatus));
                list.Add(building.Name);
                list.Add(building.Name2);
                list.Add(building.PropertyId.ToString());
                list.Add(building.NumberOfFloor.ToString());
                list.Add(building.Unit.ToString());
                list.Add(building.SourceValue.ToString());
                list.Add(building.GroupId.ToString());
                list.Add(building.X.ToString());
                list.Add(building.Y.ToString());

                if (buildingAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(buildingAND.OriId.ToString());
                    listAND.Add(buildingAND.AreaId.ToString());
                    //listAND.Add(buildingAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(buildingAND.AndStatus));
                    listAND.Add(buildingAND.Name);
                    listAND.Add(buildingAND.Name2);
                    listAND.Add(buildingAND.PropertyId.ToString());
                    listAND.Add(buildingAND.NumberOfFloor.ToString());
                    listAND.Add(buildingAND.Unit.ToString());
                    listAND.Add(buildingAND.SourceValue.ToString());
                    listAND.Add(buildingAND.GroupId.ToString());
                    listAND.Add(buildingAND.X.ToString());
                    listAND.Add(buildingAND.Y.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemBuilding(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();
                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add("");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemBuilding(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        private void ListAttributeLandmark()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GLandmark landmark = repo.GetById<GLandmark>(id.Value);
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId(true);

                //foreach (GLandmarkAND landmarkAND in landmark.GetLandmarkAND())
                //{
                List<string> list = new List<string>();
                list.Add(landmark.OID.ToString());
                list.Add(landmark.AreaId.ToString());
                //list.Add(landmark.AndStatus.ToString());
                list.Add(GetnameAndStatus(landmark.AndStatus));
                string Name = landmark.Name ?? string.Empty;
                list.Add(Name);
                string Name2 = landmark.Name2 ?? string.Empty;
                list.Add(Name2);
                list.Add(landmark.StreetId.ToString());
                list.Add(landmark.UpdateStatus.ToString());
                list.Add(landmark.NavigationStatusValue.ToString());
                list.Add(landmark.SourceValue.ToString());
                //list.Add(landmark.Code.ToString());
                string Code = landmark.Code ?? string.Empty;
                list.Add(Code);
                list.Add(landmark.X.ToString());
                list.Add(landmark.Y.ToString());

                if (landmarkAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(landmarkAND.OriId.ToString());
                    listAND.Add(landmarkAND.AreaId.ToString());
                    //listAND.Add(landmarkAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(landmarkAND.AndStatus));
                    Name = landmarkAND.Name ?? string.Empty;
                    listAND.Add(Name);
                    Name2 = landmarkAND.Name2 ?? string.Empty;
                    listAND.Add(Name2);
                    listAND.Add(landmarkAND.StreetId.ToString());
                    listAND.Add(landmarkAND.UpdateStatus.ToString());
                    listAND.Add(landmarkAND.NavigationStatusValue.ToString());
                    listAND.Add(landmarkAND.SourceValue.ToString());
                    String CodeAND = landmarkAND.Code ?? string.Empty;
                    listAND.Add(CodeAND);
                    listAND.Add(landmarkAND.X.ToString());
                    listAND.Add(landmarkAND.Y.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemLandmark(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();
                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add("");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemLandmark(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        private void ListAttributeStreet()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GStreet street = repo.GetById<GStreet>(id.Value);
                GStreetAND streetAND = street.GetStreetANDId(true);

                //foreach (GStreetAND streetAND in street.GetStreetAND())
                //{
                List<string> list = new List<string>();
                list.Add(street.OID.ToString());
                list.Add(street.AreaId.ToString());
                //list.Add(street.AndStatus.ToString());
                list.Add(GetnameAndStatus(street.AndStatus));
                String Name = street.Name ?? string.Empty;
                list.Add(Name);
                String Name2 = street.Name2 ?? string.Empty;
                list.Add(Name2);
                list.Add(street.TypeValue.ToString());
                String Section = street.Section ?? string.Empty;
                list.Add(Section);
                String City = street.City ?? string.Empty;
                list.Add(City);
                String State = street.State ?? string.Empty;
                list.Add(State.ToString());
                String Postcode = street.Postcode ?? string.Empty;
                list.Add(Postcode);
                list.Add(street.ConstructionStatusValue.ToString());
                list.Add(street.StatusValue.ToString());
                list.Add(street.NetworkClassValue.ToString());
                list.Add(street.Length.ToString());

                if (streetAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(streetAND.OriId.ToString());
                    listAND.Add(streetAND.AreaId.ToString());
                    //listAND.Add(streetAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(streetAND.AndStatus));
                    Name = streetAND.Name ?? string.Empty;
                    listAND.Add(Name);
                    Name2 = streetAND.Name2 ?? string.Empty;
                    listAND.Add(Name2);
                    listAND.Add(streetAND.TypeValue.ToString());
                    Section = streetAND.Section ?? string.Empty;
                    listAND.Add(Section);
                    City = streetAND.City ?? string.Empty;
                    listAND.Add(City);
                    State = streetAND.State ?? string.Empty;
                    listAND.Add(State);
                    Postcode = streetAND.Postcode ?? string.Empty;
                    listAND.Add(Postcode);
                    listAND.Add(streetAND.ConstructionStatusValue.ToString());
                    listAND.Add(streetAND.StatusValue.ToString());
                    listAND.Add(streetAND.NetworkClassValue.ToString());
                    if (string.IsNullOrEmpty(streetAND.Length.ToString()))
                    {
                        streetAND.Length = street.Length;
                    }
                    listAND.Add(streetAND.Length.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemStreet(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();
                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add("");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemStreet(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        private void ListAttributeJunction()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GJunction junction = repo.GetById<GJunction>(id.Value);
                GJunctionAND junctionAND = junction.GetJunctionANDId(true);

                //foreach (GJunctionAND junctionAND in junction.GetJunctionAND())
                //{
                List<string> list = new List<string>();
                list.Add(junction.OID.ToString());
                list.Add(junction.AreaId.ToString());
                //list.Add(junction.AndStatus.ToString());
                list.Add(GetnameAndStatus(junction.AndStatus));
                string Name = junction.Name ?? string.Empty;
                list.Add(Name);
                list.Add(junction.TypeValue.ToString());
                list.Add(junction.X.ToString());
                list.Add(junction.Y.ToString());

                if (junctionAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(junctionAND.OriId.ToString());
                    listAND.Add(junctionAND.AreaId.ToString());
                    //listAND.Add(junctionAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(junctionAND.AndStatus));
                    Name = junctionAND.Name ?? string.Empty;
                    listAND.Add(Name);
                    listAND.Add(junctionAND.TypeValue.ToString());
                    listAND.Add(junctionAND.X.ToString());
                    listAND.Add(junctionAND.Y.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemJunction(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();
                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add("");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemJunction(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        private void ListAttributeBuildingGroup()
        {
            int? id = StringUtils.CheckInt(txtFeatureId.Text);
            if (id.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(id.Value);
                GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId(true);

                List<string> list = new List<string>();
                list.Add(buildingGroup.OID.ToString());
                list.Add(buildingGroup.AreaId.ToString());
                //list.Add(buildingGroup.AndStatus.ToString());
                list.Add(GetnameAndStatus(buildingGroup.AndStatus));
                string Name = buildingGroup.Name ?? string.Empty;
                list.Add(Name);
                string Code = buildingGroup.Code ?? string.Empty;
                list.Add(Code);
                list.Add(buildingGroup.StreetId.ToString());
                list.Add(buildingGroup.NavigationStatusValue.ToString());
                list.Add(buildingGroup.SourceValue.ToString());
                list.Add(buildingGroup.NumUnit.ToString());
                list.Add(buildingGroup.ForecastNumUnit.ToString());
                string ForecastSource = buildingGroup.ForecastSource ?? string.Empty;
                list.Add(ForecastSource);
                //list.Add(buildingGroup.ForecastSource.ToString());
                list.Add(buildingGroup.X.ToString());
                list.Add(buildingGroup.Y.ToString());

                if (buildingGroupAND != null)
                {
                    List<string> listAND = new List<string>();
                    listAND.Add(buildingGroupAND.OriId.ToString());
                    listAND.Add(buildingGroupAND.AreaId.ToString());
                    //listAND.Add(buildingGroupAND.AndStatus.ToString());
                    listAND.Add(GetnameAndStatus(buildingGroupAND.AndStatus));
                    Name = buildingGroupAND.Name ?? string.Empty;
                    listAND.Add(Name);
                    Code = buildingGroupAND.Code ?? string.Empty;
                    listAND.Add(Code);
                    listAND.Add(buildingGroupAND.StreetId.ToString());
                    listAND.Add(buildingGroupAND.NavigationStatusValue.ToString());
                    listAND.Add(buildingGroupAND.SourceValue.ToString());
                    listAND.Add(buildingGroupAND.NumUnit.ToString());
                    listAND.Add(buildingGroupAND.ForecastNumUnit.ToString());
                    ForecastSource = buildingGroupAND.ForecastSource ?? string.Empty;
                    listAND.Add(ForecastSource);
                    //listAND.Add(buildingGroupAND.ForecastSource.ToString());
                    listAND.Add(buildingGroupAND.X.ToString());
                    listAND.Add(buildingGroupAND.Y.ToString());

                    for (int j = 0; j < list.Count; j++)
                    {
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemBuildingGroup(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = true;
                }
                else
                {
                    List<string> listAND = new List<string>();
                    for (int j = 0; j < list.Count; j++)
                    {
                        listAND.Add("");
                        List<ListViewItem> items = new List<ListViewItem>();
                        ListViewItem item = CreateItemBuildingGroup(j, listAND, list);
                        items.Add(item);
                        listView1.Items.AddRange(items.ToArray());
                        listView1.EndUpdate();
                    }
                    btnVerifyFeature.Enabled = false;
                }
            }
        }
        #endregion

        #region Create Fields Name for Selected AND Feature 
        private ListViewItem CreateItemProperty(int j, List<string> listAND, List<string> list)
        {
            List<ListViewItem> items = new List<ListViewItem>();
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.propertyAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);

            return item;
        }
        private ListViewItem CreateItemBuilding(int j, List<string> listAND, List<string> list)
        {
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.buildingAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);

            return item;
        }
        private ListViewItem CreateItemLandmark(int j, List<string> listAND, List<string> list)
        {
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.landmarkAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);
            return item;
        }
        private ListViewItem CreateItemStreet(int j, List<string> listAND, List<string> list)
        {
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.streetAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);
            return item;
        }
        private ListViewItem CreateItemJunction(int j, List<string> listAND, List<string> list)
        {
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.junctionAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);
            return item;

        }
        private ListViewItem CreateItemBuildingGroup(int j, List<string> listAND, List<string> list)
        {
            ListViewItem item = new ListViewItem();
            item.Text = VerifyAttName.buildingGroupAttName[j];
            item.SubItems.Add(list[j]);
            item.SubItems.Add(listAND[j]);

            return item;
        }
        #endregion

        private void BtnVerifyFeature_Click(object sender, EventArgs e)
        {
            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                          .SetDefaultButton(MessageBoxDefaultButton.Button1)
                          .SetCaption("Verify Feature AND")
                          .SetText("Are you sure you want to Proceed Verify id {0}?", txtFeatureId.Text);
                if (confirmBox.Show(this) != DialogResult.Yes)
                {
                    return;
                }
            }

            bool success = false;
            try
            {
                using (new WaitCursor())
                {
                    VerifyFeature();
                    if (verifystatus == 1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }
        private void BtnRevertFeature_Click(object sender, EventArgs e)
        {
            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                 .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                 .SetCaption("Revert Feature AND")
                                 .SetText("Are you sure you want to Proceed Revert id {0}?", txtFeatureId.Text);
                if (confirmBox.Show(this) != DialogResult.Yes)
                {
                    return;
                }
            }

            bool success = false;
            try
            {
                using (new WaitCursor())
                {
                    RevertFeature();
                    if (revertstatus == 1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        #region Verification Feature AND by Supervisor
        private void VerifyFeature()
        {
            try
            {
                if (txtFeatureName.Text == "Property")
                {
                    VerifyFeatureProperty();
                }
                else if (txtFeatureName.Text == "Landmark")
                {
                    VerifyFeatureLandmark();
                }
                else if (txtFeatureName.Text == "Junction")
                {
                    VerifyFeatureJunction();
                }
                else if (txtFeatureName.Text == "Building")
                {
                    VerifyFeatureBuilding();
                }
                else if (txtFeatureName.Text == "Street")
                {
                    VerifyFeatureStreet();
                }
                else if (txtFeatureName.Text == "BuildingGroup")
                {
                    VerifyFeatureBuildingGroup();
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
        }
        public void VerifyFeatureProperty()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GProperty property = repo.GetById<GProperty>(id.Value);
                    GPropertyAND propertyAND = property.GetPropertyANDId(true);

                    //foreach (GPropertyAND propertyAND1 in property.GetPropertyAND())
                    //{
                    //    FeatureANDId = propertyAND1.OID;
                    //}
                    //id = StringUtils.CheckInt(FeatureANDId.ToString());
                    //GPropertyAND propertyAND = repo.GetById<GPropertyAND>(id.Value);

                    repo.StartTransaction();

                    if (property.AndStatus == 2) //Delete
                    {
                        if (property.HasBuilding() || property.HasFloor() || property.HasPoi() || property.HasPhone())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Process with status to DELETE, Property has Poi or Floor or Building or Phone");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            foreach (GPropertyText text in property.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            foreach (GPropertyTextAND text in propertyAND.GetTextsAND())
                            {
                                repo.Delete(text);
                            }

                            repo.Delete(property);
                            repo.Delete(propertyAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (property.AndStatus == 1 || property.AndStatus == 3 || property.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "Before Verify ...";
                        //str2 = "";
                        //List<GPropertyText> texts = property.GetTexts().ToList();
                        //List<GPropertyTextAND> AND_texts = propertyAND.GetTextsAND().ToList();
                        //writeline(texts, AND_texts, str1, str2);
                        //// end

                        property.Lot = propertyAND.Lot;
                        property.ConstructionStatus = propertyAND.ConstructionStatus;
                        property.Type = propertyAND.Type;
                        property.NumberOfFloor = propertyAND.NumberOfFloor;
                        property.Lot = propertyAND.Lot;
                        property.House = propertyAND.House;
                        property.Source = propertyAND.Source;
                        property.YearInstall = propertyAND.YearInstall;
                        property.StreetId = propertyAND.StreetId;
                        property.Shape = propertyAND.Shape;
                        property.AndStatus = 0;
                        property.DateUpdated = propertyAND.DateUpdated;
                        property.UpdatedBy = propertyAND.UpdatedBy;

                        repo.UpdateExt(property, true);

                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "After Update Verify...";
                        //str2 = "To Delete AND ...";
                        //List<GPropertyText> Texts = property.GetTexts().ToList();
                        //List<GPropertyTextAND> AND_Texts = propertyAND.GetTextsAND().ToList();
                        //writeline(Texts, AND_Texts, str1, str2);
                        //// end

                        // to delete feature text and feature AND 
                        foreach (GPropertyTextAND text in propertyAND.GetTextsAND())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(propertyAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void VerifyFeatureLandmark()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GLandmark landmark = repo.GetById<GLandmark>(id.Value);
                    GLandmarkAND landmarkAND = landmark.GetLandmarkANDId(true);

                    //foreach (GLandmarkAND landmarkAND1 in landmark.GetLandmarkAND())
                    //{
                    //    FeatureANDId = landmarkAND1.OID;
                    //}
                    //id = StringUtils.CheckInt(FeatureANDId.ToString());
                    //GLandmarkAND landmarkAND = repo.GetById<GLandmarkAND>(id.Value);

                    repo.StartTransaction();

                    if (landmark.AndStatus == 2) //Delete
                    {
                        if (landmark.HasPoi())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Process with status to DELETE, Landmark has Poi");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            foreach (GLandmarkText text in landmark.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            foreach (GLandmarkTextAND text in landmarkAND.GetTextsAND())
                            {
                                repo.Delete(text);
                            }

                            repo.Delete(landmark);
                            repo.Delete(landmarkAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (landmark.AndStatus == 1 || landmark.AndStatus == 3 || landmark.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "Before Verify ...";
                        //str1 = "";
                        //List<GLandmarkText> texts = landmark.GetTexts().ToList();
                        //List<GLandmarkTextAND> AND_texts = landmarkAND.GetTextsAND().ToList();
                        //writeline(texts, AND_texts, str1, str2);
                        //// end

                        landmark.Name = landmarkAND.Name;
                        landmark.Name2 = landmarkAND.Name2;
                        landmark.Code = landmarkAND.Code;
                        landmark.Source = landmarkAND.Source;
                        landmark.Shape = landmarkAND.Shape;
                        landmark.AndStatus = 0;

                        repo.UpdateExt(landmark, true);

                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "After Update Verify...";
                        //str2 = "To Delete AND ...";
                        //List<GLandmarkText> Texts = landmark.GetTexts().ToList();
                        //List<GLandmarkTextAND> AND_Texts = landmarkAND.GetTextsAND().ToList();
                        //writeline(Texts, AND_Texts, str1, str2);
                        //// end

                        // to delete feature text and feature AND 
                        foreach (GLandmarkTextAND text in landmarkAND.GetTextsAND())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(landmarkAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        private void VerifyFeatureBuilding()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GBuilding building = repo.GetById<GBuilding>(id.Value);
                    GBuildingAND buildingAND = building.GetBuildingANDId(true);

                    repo.StartTransaction();

                    if (building.AndStatus == 2) //Delete
                    {
                        if (building.HasPoi() || building.HasGroup() || building.HasMultiStorey())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Process with status to DELETE, Building has MultiStorey/Poi/Group");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            foreach (GBuildingText text in building.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            foreach (GBuildingTextAND text in buildingAND.GetTextsAND())
                            {
                                repo.Delete(text);
                            }

                            repo.Delete(building);
                            repo.Delete(buildingAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (building.AndStatus == 1 || building.AndStatus == 3 || building.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        building.Name = buildingAND.Name;
                        building.Name2 = buildingAND.Name2;
                        building.Code = buildingAND.Code;
                        building.Source = buildingAND.Source;
                        building.PropertyId = buildingAND.PropertyId;
                        building.GroupId = buildingAND.GroupId;
                        building.Shape = buildingAND.Shape;
                        building.AndStatus = 0;
                        building.DateUpdated = buildingAND.DateUpdated;
                        building.UpdatedBy = buildingAND.UpdatedBy;
                        building.ForecastSource = buildingAND.ForecastSource;
                        building.ForecastNumUnit = buildingAND.ForecastNumUnit;

                        repo.UpdateExt(building, true);

                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "After Update Verify...";
                        //str2 = "To Delete AND ...";
                        //List<GBuildingText> Texts = building.GetTexts().ToList();
                        //List<GBuildingTextAND> AND_Texts = buildingAND.GetTextsAND().ToList();
                        //writeline(Texts, AND_Texts, str1, str2);
                        //// end

                        // to delete feature text and feature AND 
                        foreach (GBuildingTextAND text in buildingAND.GetTextsAND())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(buildingAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void VerifyFeatureJunction()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GJunction junction = repo.GetById<GJunction>(id.Value);
                    GJunctionAND junctionAND = junction.GetJunctionANDId(true);

                    repo.StartTransaction();
                    if (junction.AndStatus == 2) //Delete
                    {
                        if (junction.HasStreet() || junction.HasStreetRestriction() || junction.HasTollRoute())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Process with status to DELETE, Junction attach to street/Restriction/TollRoute");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            repo.Delete(junction);
                            repo.Delete(junctionAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (junction.AndStatus == 1 || junction.AndStatus == 3 || junction.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        junction.Name = junctionAND.Name;
                        junction.Type = junctionAND.Type;
                        junction.Source = junctionAND.Source;
                        junction.Shape = junctionAND.Shape;
                        junction.AndStatus = 0;
                        junction.DateUpdated = junctionAND.DateUpdated;
                        junction.UpdatedBy = junctionAND.UpdatedBy;

                        repo.UpdateExt(junction, true);
                        repo.Delete(junctionAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void VerifyFeatureStreet()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GStreet street = repo.GetById<GStreet>(id.Value);
                    GStreetAND streetAND = street.GetStreetANDId(true);

                    repo.StartTransaction();
                    if (street.AndStatus == 2) //Delete
                    {
                        if (street.HasBuildingGroup() || street.HasLandmark() || street.HasProperty() || street.HasRestriction())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Process with status to DELETE, Steeet has Property/Landmark etc");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            foreach (GStreetText text in street.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            foreach (GStreetTextAND text in streetAND.GetTextsAND())
                            {
                                repo.Delete(text);
                            }

                            repo.Delete(street);
                            repo.Delete(streetAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (street.AndStatus == 1 || street.AndStatus == 3 || street.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        street.FromNodeId = streetAND.FromNodeId;
                        street.ToNodeId = streetAND.ToNodeId;

                        street.ConstructionStatus = streetAND.ConstructionStatus;
                        street.NavigationStatus = streetAND.NavigationStatus;
                        street.Status = streetAND.Status;
                        street.NetworkClass = streetAND.NetworkClass;
                        street.Class = streetAND.Class;
                        street.Category = streetAND.Category;
                        street.FilterLevel = streetAND.FilterLevel;
                        street.Design = streetAND.Design;
                        street.Direction = streetAND.Direction;
                        street.TollType = streetAND.TollType;
                        street.Divider = streetAND.Divider;
                        street.Source = streetAND.Source;
                        street.Shape = streetAND.Shape;

                        street.Type = streetAND.Type;
                        street.Name = streetAND.Name;
                        street.Name2 = streetAND.Name2;
                        street.Section = streetAND.Section;
                        street.Postcode = streetAND.Postcode;
                        street.City = streetAND.City;
                        street.SubCity = streetAND.SubCity;
                        street.State = streetAND.State;
                        street.Length = streetAND.Length;
                        street.AndStatus = 0;
                        street.DateUpdated = streetAND.DateUpdated;
                        street.UpdatedBy = streetAND.UpdatedBy;

                        // noraini ali - Sept 2020 to cater data from cadu1
                        if (streetAND.NumOfLanes != null)
                        {
                            street.NumOfLanes = streetAND.NumOfLanes;
                        }
                        else
                            street.NumOfLanes = 1;

                        if (streetAND.Divider != null)
                        {
                            street.Divider = streetAND.Divider;
                        }
                        else
                            street.Divider = 1;
                        //end 

                        repo.UpdateExt(street, true);

                        //// noraini - Mar 2022 - to display text during verifying process
                        //str1 = "After Update Verify...";
                        //str2 = "To Delete AND ...";
                        //List<GStreetText> Texts = street.GetTexts().ToList();
                        //List<GStreetTextAND> AND_Texts = streetAND.GetTextsAND().ToList();
                        //writeline(Texts, AND_Texts, str1, str2);
                        //// end

                        // to delete feature text and feature AND 
                        foreach (GStreetTextAND text in streetAND.GetTextsAND())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(streetAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void VerifyFeatureBuildingGroup()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    int? id = StringUtils.CheckInt(txtFeatureId.Text);
                    GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(id.Value);
                    GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId(true);

                    repo.StartTransaction();
                    if (buildingGroup.AndStatus == 2) //Delete
                    {
                        if (buildingGroup.HasBuilding())
                        {
                            using (InfoMessageBox box1 = new InfoMessageBox())
                            {
                                box1.SetText("Unable to Verify Feature with status to DELETE, Building Group has Building");
                                box1.Show();
                            }
                            verifystatus = 2;
                            success = false;
                        }
                        else
                        {
                            foreach (GBuildingGroupText text in buildingGroup.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            foreach (GBuildingGroupTextAND text in buildingGroupAND.GetTextsAND())
                            {
                                repo.Delete(text);
                            }

                            repo.Delete(buildingGroup);
                            repo.Delete(buildingGroupAND);

                            verifystatus = 1;
                            success = true;
                        }
                    }
                    else if (buildingGroup.AndStatus == 1 || buildingGroup.AndStatus == 3 || buildingGroup.AndStatus == 4) // Update Graphic/Attribute only
                    {
                        buildingGroup.NavigationStatus = buildingGroupAND.NavigationStatus;
                        buildingGroup.Source = buildingGroupAND.Source;
                        buildingGroup.Code = buildingGroupAND.Code;
                        buildingGroup.Name = buildingGroupAND.Name;
                        buildingGroup.StreetId = buildingGroupAND.StreetId;
                        buildingGroup.Shape = buildingGroupAND.Shape;
                        buildingGroup.AndStatus = 0;
                        buildingGroup.DateUpdated = buildingGroupAND.DateUpdated;
                        buildingGroup.UpdatedBy = buildingGroupAND.UpdatedBy;
                        buildingGroup.BuldingNamePos = buildingGroupAND.BuldingNamePos;
                        buildingGroup.ForecastSource = buildingGroupAND.ForecastSource;
                        buildingGroup.ForecastNumUnit = buildingGroupAND.ForecastNumUnit;

                        repo.UpdateExt(buildingGroup, true);

                        // to delete feature text and feature AND 
                        foreach (GBuildingGroupTextAND text in buildingGroupAND.GetTextsAND())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(buildingGroupAND);

                        verifystatus = 1;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                verifystatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        #endregion

        #region Revert Feature AND by Supervisor
        private void RevertFeature()
        {
            try
            {
                if (txtFeatureName.Text == "Property")
                {
                    RevertFeatureProperty();
                }
                else if (txtFeatureName.Text == "Landmark")
                {
                    RevertFeatureLandmark();
                }
                else if (txtFeatureName.Text == "Junction")
                {
                    RevertFeatureJunction();
                }
                else if (txtFeatureName.Text == "Building")
                {
                    RevertFeatureBuilding();
                }
                else if (txtFeatureName.Text == "Street")
                {
                    RevertFeatureStreet();
                }
                else if (txtFeatureName.Text == "BuildingGroup")
                {
                    RevertFeatureBuildingGroup();
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
        }
        private void RevertFeatureProperty()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GProperty property = repo.GetById<GProperty>(id.Value);
                GPropertyAND propertyAND = property.GetPropertyANDId(true);

                repo.StartTransaction();
                if (property.AndStatus == 1) // New
                {
                    if (property.HasBuilding() || property.HasFloor() || property.HasPhone() || property.HasPoi())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Revert Process, Property has Poi or Floor or Building or Phone");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        foreach (GPropertyText text in property.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        foreach (GPropertyText text in propertyAND.GetTexts())
                        {
                            repo.Delete(text);
                        }

                        repo.Delete(property);
                        if (propertyAND != null)
                            repo.Delete(propertyAND);

                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (property.AndStatus == 2 || property.AndStatus == 3 || property.AndStatus == 4)
                {
                    if (property.AndStatus == 2)
                    {
                        property.UpdateStatus = 4;
                    }
                    property.AndStatus = 0;
                    repo.UpdateExt(property, true);

                    if (propertyAND != null)
                    {
                        foreach (GPropertyText text in propertyAND.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(propertyAND);
                    }

                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void RevertFeatureLandmark()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GLandmark landmark = repo.GetById<GLandmark>(id.Value);
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId(true);

                repo.StartTransaction();
                if (landmark.AndStatus == 1) // New
                {
                    if (landmark.HasPoi())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Revert Process, Landmark has Poi");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        foreach (GLandmarkText text in landmark.GetTexts())
                        {
                            repo.Delete(text);
                        }

                        repo.Delete(landmark);

                        if (landmarkAND != null)
                        {
                            foreach (GLandmarkText text in landmarkAND.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            repo.Delete(landmarkAND);
                        }

                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (landmark.AndStatus == 2 || landmark.AndStatus == 3 || landmark.AndStatus == 4)
                {
                    if (landmark.AndStatus == 2)
                    {
                        landmark.UpdateStatus = 4;
                    }
                    landmark.AndStatus = 0;
                    repo.UpdateExt(landmark, true);

                    if (landmarkAND != null)
                    {
                        foreach (GLandmarkText text in landmarkAND.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(landmarkAND);
                    }
                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void RevertFeatureBuilding()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GBuilding building = repo.GetById<GBuilding>(id.Value);
                GBuildingAND buildingAND = building.GetBuildingANDId(true);

                repo.StartTransaction();
                if (building.AndStatus == 1) // New
                {
                    if (building.HasPoi() || building.HasGroup() || building.HasMultiStorey())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Revert Process, Building has MultiStorey/Poi/Group");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        foreach (GBuildingText text in building.GetTexts())
                        {
                            repo.Delete(text);
                        }

                        repo.Delete(building);

                        if (buildingAND != null)
                        {
                            foreach (GBuildingText text in buildingAND.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            repo.Delete(buildingAND);
                        }

                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (building.AndStatus == 2 || building.AndStatus == 3 || building.AndStatus == 4)
                {
                    if (building.AndStatus == 2)
                    {
                        building.UpdateStatus = 4;
                    }
                    building.AndStatus = 0;
                    repo.UpdateExt(building, true);

                    if (buildingAND != null)
                    {
                        foreach (GBuildingText text in buildingAND.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(buildingAND);
                    }

                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void RevertFeatureJunction()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GJunction junction = repo.GetById<GJunction>(id.Value);
                GJunctionAND junctionAND = junction.GetJunctionANDId(true);

                repo.StartTransaction();
                if (junction.AndStatus == 1) // New
                {
                    if (junction.HasStreet() || junction.HasStreetRestriction()) // || junction.HasTollRoute())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Verify Process, Junction attach to street/Restriction");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        repo.Delete(junction);
                        if (junctionAND != null)
                        {
                            repo.Delete(junctionAND);
                        }

                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (junction.AndStatus == 2 || junction.AndStatus == 3 || junction.AndStatus == 4)
                {
                    if (junction.AndStatus == 2)
                    {
                        junction.UpdateStatus = 4;
                    }
                    junction.AndStatus = 0;
                    repo.UpdateExt(junction, true);
                    if (junctionAND != null)
                    {
                        repo.Delete(junctionAND);
                    }
                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void RevertFeatureStreet()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GStreet street = repo.GetById<GStreet>(id.Value);
                GStreetAND streetAND = street.GetStreetANDId(true);

                repo.StartTransaction();
                if (street.AndStatus == 1) // New
                {
                    if (street.HasBuildingGroup() || street.HasLandmark() || street.HasProperty() || street.HasRestriction())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Revert Process, Steeet has Property/Landmark etc");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        foreach (GStreetText text in street.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(street);

                        if (streetAND != null)
                        {
                            foreach (GStreetText text in streetAND.GetTexts())
                            {
                                repo.Delete(text);
                            }
                            repo.Delete(streetAND);
                        }

                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (street.AndStatus == 2 || street.AndStatus == 3 || street.AndStatus == 4)
                {
                    if (street.AndStatus == 2)
                    {
                        street.UpdateStatus = 4;
                    }
                    street.AndStatus = 0;
                    repo.UpdateExt(street, true);

                    if (streetAND != null)
                    {
                        foreach (GStreetText text in streetAND.GetTexts())
                        {
                            repo.Delete(text);
                        }
                        repo.Delete(streetAND);
                    }

                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        private void RevertFeatureBuildingGroup()
        {
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                int? id = StringUtils.CheckInt(txtFeatureId.Text);
                GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(id.Value);
                GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId(true);

                repo.StartTransaction();
                if (buildingGroup.AndStatus == 1) // New
                {
                    if (buildingGroup.HasBuilding())
                    {
                        using (InfoMessageBox box1 = new InfoMessageBox())
                        {
                            box1.SetText("Unable to Revert Feature, Building Group has Building");
                            box1.Show();
                        }
                        revertstatus = 2;
                        success = false;
                    }
                    else
                    {
                        repo.Delete(buildingGroup);
                        if (buildingGroupAND != null)
                        {
                            repo.Delete(buildingGroupAND);
                        }
                        revertstatus = 1;
                        success = true;
                    }
                }
                else if (buildingGroup.AndStatus == 2 || buildingGroup.AndStatus == 3 || buildingGroup.AndStatus == 4)
                {
                    if (buildingGroup.AndStatus == 2)
                    {
                        buildingGroup.UpdateStatus = 4;
                    }
                    buildingGroup.AndStatus = 0;
                    repo.UpdateExt(buildingGroup, true);
                    if (buildingGroupAND != null)
                    {
                        repo.Delete(buildingGroupAND);
                    }

                    revertstatus = 1;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
                revertstatus = 0;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
        #endregion

        private void BtnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        private SegmentName GetSegmentName(string WASegmentname)
        {
            if (WASegmentname == "AS")
                return SegmentName.AS;
            else if (WASegmentname == "JH")
                return SegmentName.JH;
            else if (WASegmentname == "JP")
                return SegmentName.JP;
            else if (WASegmentname == "KG")
                return SegmentName.KG;
            else if (WASegmentname == "KK")
                return SegmentName.KK;
            else if (WASegmentname == "KN")
                return SegmentName.KN;
            else if (WASegmentname == "KV")
                return SegmentName.KV;
            else if (WASegmentname == "MK")
                return SegmentName.MK;
            else if (WASegmentname == "PG")
                return SegmentName.PG;
            else if (WASegmentname == "TG")
                return SegmentName.TG;

            return SegmentName.None;
        }
        private string GetnameAndStatus(int? andStatus)
        {
            string AndStatusName = "";

            if (andStatus == 1)
            { AndStatusName = "New"; }
            else if (andStatus == 2)
            { AndStatusName = "Delete"; }
            else if (andStatus == 3)
            { AndStatusName = "Edit Graphic & Attribute"; }
            else if (andStatus == 4)
            { AndStatusName = "Edit Attribute Only"; }

            return AndStatusName;
        }
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            int? id = StringUtils.CheckInt(txtFeatureId.Text);

            if (txtFeatureName.Text == StringAttributeName.Property)
            {
                GProperty property = repo.GetById<GProperty>(id.Value);

            }
        }

        private void writeline_output(string str1, string str2, string str3, string str4)
        {
            Console.WriteLine(str1);
            Console.WriteLine("OID :" + str2);
            Console.WriteLine("Link :" + str3);
            Console.WriteLine("Text Value :" + str4);
        }

        private void writeline(List<GStreetText> texts, List<GStreetTextAND> AND_texts, string str1, string str2)
        {
            Console.WriteLine(str1);
            for (int count = 0; count < texts.Count; count++)
            {
                GStreetText text = texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            if (!string.IsNullOrEmpty(str2.Length.ToString()))
            { Console.WriteLine(""); }
            Console.WriteLine(str2);

            for (int count = 0; count < AND_texts.Count; count++)
            {
                GStreetTextAND text = AND_texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            Console.WriteLine("");
        }

        private void writeline(List<GPropertyText> texts, List<GPropertyTextAND> AND_texts, string str1, string str2)
        {
            Console.WriteLine(str1);
            for (int count = 0; count < texts.Count; count++)
            {
                GPropertyText text = texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            if (!string.IsNullOrEmpty(str2.Length.ToString()))
            { Console.WriteLine(""); }
            Console.WriteLine(str2);

            for (int count = 0; count < AND_texts.Count; count++)
            {
                GPropertyTextAND text = AND_texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            Console.WriteLine("");
        }

        private void writeline(List<GBuildingText> texts, List<GBuildingTextAND> AND_texts, string str1, string str2)
        {
            Console.WriteLine(str1);
            for (int count = 0; count < texts.Count; count++)
            {
                GBuildingText text = texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            if (!string.IsNullOrEmpty(str2.Length.ToString()))
            { Console.WriteLine(""); }
            Console.WriteLine(str2);

            for (int count = 0; count < AND_texts.Count; count++)
            {
                GBuildingTextAND text = AND_texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            Console.WriteLine("");
        }

        private void writeline(List<GLandmarkText> texts, List<GLandmarkTextAND> AND_texts, string str1, string str2)
        {
            Console.WriteLine(str1);
            for (int count = 0; count < texts.Count; count++)
            {
                GLandmarkText text = texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            if (!string.IsNullOrEmpty(str2.Length.ToString()))
            { Console.WriteLine(""); }
            Console.WriteLine(str2);

            for (int count = 0; count < AND_texts.Count; count++)
            {
                GLandmarkTextAND text = AND_texts[count];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            Console.WriteLine("");
        }

        private void writeline(List<GBuildingGroupText> texts, List<GBuildingGroupTextAND> AND_texts, string str1, string str2)
        {
            Console.WriteLine(str1);
            if (texts.Count > 0)
            {
                GBuildingGroupText text = texts[0];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            if (!string.IsNullOrEmpty(str2.Length.ToString()))
            { Console.WriteLine(""); }
            Console.WriteLine(str2);
        
            if (AND_texts.Count > 0)
            {
                GBuildingGroupTextAND text = AND_texts[0];
                writeline_output(text.TableName, text.OID.ToString(), text.LinkedFeatureID.ToString(), text.Text);
            }
            Console.WriteLine("");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Controls;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms
{
    public partial class BuildingForm : CollapsibleForm
    {
        #region Properties
        StringBuilder sb = new StringBuilder();
        public static string sendtextLotHse;

        public int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string InsertedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string InsertedName2
        {
            get { return txtName2.Text; }
        }

        public int InsertedNumOfFloor
        {
            get { return numOfFloor.Value; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public int InsertedTotalForecastUnit
        {
            get { return totalForecastUnit.Value; }
        }

        public string SelectedForecastSource
        {
            get { return (cbForecastSource.SelectedItem == null) ? string.Empty : ((GSource)cbForecastSource.SelectedItem).Code.ToString(); }
        }

        public string TotalUnit
        {
            get { return txtTotalApt.Text ?? "0"; }
        }

        #endregion

        protected GBuilding _building;

        public BuildingForm()
            : this(null)
        {
        }

        public BuildingForm(GBuilding building)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
              lblConstructionStatus, cbConstructionStatus,
              btnSetCode,
              lblCode, txtCode,
              lblName, txtName,
              lblName2, txtName2,
              lblSpace, txtSpace,
              lblUnit, cbUnit,
              lblNumOfFloor, numOfFloor,
              lblSource, cbSource,
              lblTotalForecastUnit, totalForecastUnit,
              lblForecastSource, cbForecastSource,
              btnApply
              );

            _building = building;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateForecastSource(cbForecastSource);
            ComboBoxUtils.PopulateUnit(cbUnit);

            controlMultiStorey.Columns.Add("Id");
            controlMultiStorey.Columns.Add("Floor");
            controlMultiStorey.Columns.Add("Apartment Unit");

            controlPoi.Columns.Add("Id");
            controlPoi.Columns.Add("Code");
            controlPoi.Columns.Add("Name");
            controlPoi.Columns.Add("Name2");
            controlPoi.Columns.Add("Navi_Status");  // added by noraini - Aug 2019
            controlPoi.Columns.Add("Description");
            controlPoi.Columns.Add("Url");
            
            // building
            txtId.Text = _building.OID.ToString();

            cbConstructionStatus.Text = _building.ConstructionStatusValue;
            cbNaviStatus.Text = _building.NavigationStatusValue;
            txtCode.Text = _building.Code;
            GCode1 code1 = _building.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _building.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _building.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            txtName.Text = _building.Name;
            txtName2.Text = _building.Name2;

            txtSpace.Text = _building.Space.ToString();
            cbUnit.Text = "SQUARE FEET";

            numOfFloor.Value = _building.NumberOfFloor.HasValue ? _building.NumberOfFloor.Value : 0;

            int forecastUnitCount = _building.ForecastNumUnit.HasValue ? _building.ForecastNumUnit.Value : 0;
            totalForecastUnit.Value = forecastUnitCount;

            cbSource.Text = _building.SourceValue;
            cbForecastSource.Text = _building.ForecastSourceValue;

            txtPropertyId.Text = _building.PropertyId.ToString();

            txtDateCreated.Text = _building.DateCreated;
            txtCreator.Text = _building.CreatedBy;
            txtDateUpdated.Text = _building.DateUpdated;
            txtUpdater.Text = _building.UpdatedBy;

            // property
            GProperty property = _building.GetProperty();
            if (property != null)
            {
                txtLot.Text = property.Lot;
                txtHouse.Text = property.House;

                // street
                GStreet street = property.GetStreet();
                if (street != null)
                {
                    GStreetAND streetAND = street.GetStreetANDId();
                    if (streetAND != null)
                    {
                        txtStreetType.Text = streetAND.TypeValue;
                        txtStreetName.Text = streetAND.Name;
                        txtStreetName2.Text = streetAND.Name2;
                        txtSection.Text = streetAND.Section;
                        txtPostcode.Text = streetAND.Postcode;
                        txtCity.Text = streetAND.City;
                        txtState.Text = streetAND.State;
                    }
                    else
                    {
                        txtStreetType.Text = street.TypeValue;
                        txtStreetName.Text = street.Name;
                        txtStreetName2.Text = street.Name2;
                        txtSection.Text = street.Section;
                        txtPostcode.Text = street.Postcode;
                        txtCity.Text = street.City;
                        txtState.Text = street.State;
                    }
                }
            }
            PopulateMultiStorey();
            PopulatePoi();
        }

        public void disablePOIEdit()
        {
            controlPoi.Enabled = false;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected void PopulateMultiStorey()
        {
            controlMultiStorey.BeginUpdate();
            controlMultiStorey.Sorting = SortOrder.None;
            controlMultiStorey.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GMultiStorey storey in _building.GetMultiStories())
            {
                ListViewItem item = new ListViewItem();
                item.Text = storey.OID.ToString();
                item.SubItems.Add(storey.Floor);
                item.SubItems.Add(storey.Apartment);
                item.Tag = storey;

                items.Add(item);
            }

            controlMultiStorey.Items.AddRange(items.ToArray());
            controlMultiStorey.FixColumnWidth();
            controlMultiStorey.EndUpdate();
            txtTotalApt.Text = controlMultiStorey.Items.Count.ToString(); // added by noraini
        }

        protected void PopulatePoi()
        {
            controlPoi.BeginUpdate();
            controlPoi.Sorting = SortOrder.None;
            controlPoi.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GPoi poi in _building.GetPois())
            {
                ListViewItem item = new ListViewItem();
                item.Text = poi.OID.ToString();
                item.SubItems.Add(poi.Code);
                item.SubItems.Add(poi.Name);
                item.SubItems.Add(poi.Name2);
                item.SubItems.Add(poi.NavigationStatusValue);  // added by noraini - Aug 2019
                item.SubItems.Add(poi.Description);
                item.SubItems.Add(poi.Url);
                item.Tag = poi;

                items.Add(item);
            }

            controlPoi.Items.AddRange(items.ToArray());
            controlPoi.FixColumnWidth();
            controlPoi.EndUpdate();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Construction Status
            bool isConstructionStatusValid = BuildingValidator.CheckConstructionStatus(SelectedConstructionStatus);
            LabelColor(lblConstructionStatus, isConstructionStatusValid);
            pass &= isConstructionStatusValid;

            // Name
            bool isNameValid = BuildingValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Name 2
            bool isName2Valid = BuildingValidator.CheckName2(txtName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Source
            bool isSourceValid = BuildingValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            return pass;
        }

        public void SetValues()
        {
            int forecastInt;
            _building.ConstructionStatus = SelectedConstructionStatus;
            _building.NavigationStatus = SelectedNavigationStatus;
            _building.Code = InsertedCode;
            _building.Name = InsertedName;
            _building.Name2 = InsertedName2;
            _building.NumberOfFloor = InsertedNumOfFloor;
            _building.Source = SelectedSource;
            _building.ForecastNumUnit = InsertedTotalForecastUnit;
            _building.ForecastSource = int.TryParse(SelectedForecastSource, out forecastInt) ? (int?)forecastInt : null;
            _building.Unit = Int32.Parse(TotalUnit);
        }
       
        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                GCode1 code1 = code3.GetCode1();
                GCode2 code2 = code3.GetCode2();
                categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
                categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
                categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            }
        }

        private void controlPoi_AddClick(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
            GPoi newPoi = repo.NewObj<GPoi>();
            newPoi.Init();
            newPoi.ParentId = _building.OID;
            newPoi.ParentType = (int)ParentClass.Building;
            newPoi.Shape = _building.Shape;
            newPoi = repo.Insert(newPoi, false);

            using (AddPoiForm form = new AddPoiForm(newPoi))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    repo.Delete(newPoi);
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(newPoi);

                    ListViewItem item = new ListViewItem();
                    item.Text = newPoi.OID.ToString();
                    item.SubItems.Add(newPoi.Code);
                    item.SubItems.Add(newPoi.Name);
                    item.SubItems.Add(newPoi.Name2);
                    item.SubItems.Add(newPoi.NavigationStatusValue);  // added by noraini - Aug 2019
                    item.SubItems.Add(newPoi.Description);
                    item.SubItems.Add(newPoi.Url);
                    item.Tag = newPoi;
                    controlPoi.Items.Add(item);
                    controlPoi.FixColumnWidth();
                }
            }
        }

        private void controlPoi_EditClick(object sender, EventArgs e)
        {
            if (controlPoi.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
            ListViewItem item = controlPoi.SelectedItems[0];
            GPoi poi = (GPoi)item.Tag;

            using (EditPoiForm form = new EditPoiForm(poi))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(poi);

                    item.SubItems.Clear();
                    item.Text = poi.OID.ToString();
                    item.SubItems.Add(poi.Code);
                    item.SubItems.Add(poi.Name);
                    item.SubItems.Add(poi.Name2);
                    item.SubItems.Add(poi.NavigationStatusValue); // added by noraini - Aug2019
                    item.SubItems.Add(poi.Description);
                    item.SubItems.Add(poi.Url);
                    item.Tag = poi;
                    controlPoi.FixColumnWidth();
                }
            }
        }

        private void controlPoi_DeleteClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlPoi.SelectedItems.Count == 0)
                {
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
                ListViewItem item = controlPoi.SelectedItems[0];
                GPoi poi = (GPoi)item.Tag;
                if (poi.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        using (InfoMessageBox box = new InfoMessageBox())
                        {
                            box.SetText("Navigation ready lock.");
                            box.Show(this);
                        }
                        return;
                    }
                }

                //added by asyrul
                if (!poi.CanDelete())
                {
                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                    confirmBox.SetCaption("POI Numbers Dissociation")
                              .SetText("This POI has phone numbers " +
                              "associated to it. Do you want to DISSOCIATE " +
                              "ALL phone numbers for this POI?");

                    if (confirmBox.Show() == DialogResult.Yes)
                    {
                        using (new WaitCursor())
                        {
                            poi.DiassociateAllNumbers();
                        }
                        MessageBox.Show("Done!");
                        return;
                    }
                    else
                    {
                        throw new QualityControlException("Unable to delete poi");
                    }
                }
                //added end

                repo.Delete(poi);
                item.Remove();
                controlPoi.FixColumnWidth();
            }
        }

        private void controlMultiStorey_AddClick(object sender, EventArgs e)
        {
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Add multistorey address button clicked");
            sb.Append("\n");

            // added by Noraini Ali - Feb2019
            if ((txtLot.Text != string.Empty) && (txtHouse.Text != string.Empty))
            {
                sendtextLotHse = txtHouse.Text;
            }
            else if ((txtLot.Text == string.Empty) && (txtHouse.Text != string.Empty))
            {
                sendtextLotHse = txtHouse.Text;
            }
            else if ((txtLot.Text != string.Empty) && (txtHouse.Text == string.Empty))
            {
                sendtextLotHse = txtLot.Text;
            }
            // end added by noraini

            // noraini ali - OKT 2021 - MultiStorey Address Format
            //using (AddMultiStoreyMultipleForm2 form2 = new AddMultiStoreyMultipleForm2())

            // noraini ali - Feb 2022 - Restructure code - MultiStorey Address Format
            using (AddStoreyForm form2 = new AddStoreyForm())
			{
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening add multistorey address format form completed");
                sb.Append("\n");

                if (form2.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button add multistorey address format form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
                {
                    int floorAmount = form2.SelectedNumOfFloors;
                    int aptAmount = form2.SelectedNumOfApt;
                    int startFloor = form2.SelectedStartFloor;
                    int startApt = form2.SelectedStartApt;
                    int endFloorLoop = startFloor + floorAmount;
                    int endAptLoop = startApt + aptAmount;

                    // get floor and Apt digit
                    int FloorDigitNum = form2.FloorDigit;
                    int AptDigitNum = form2.UnitDigit;

                    // allow user to keyin floor num as alphanumeric value 
                    // like 'G','LG' etc
                    bool textflrNum = false;
                    if (string.IsNullOrEmpty(form2.SelectedTextFloorNum))
                    {
                        textflrNum = true;
                    }

                    RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
                    GMultiStorey newMultiStorey = repo.NewObj<GMultiStorey>();
                    newMultiStorey.Init();
                    newMultiStorey.BuildingId = _building.OID;

                    for (int floor = startFloor; floor < endFloorLoop; floor++)
                    {
                        string floorNumString = "";

                        if (textflrNum)
                        {
                            floorNumString = floor.ToString();
                        }
                        else
                        {
                            floorNumString = form2.SelectedTextFloorNum;
                        }

                        newMultiStorey.Floor = string.Format("{0}", floorNumString);
                        int i = 0;

                        for (int apt = startApt; apt < endAptLoop; apt++)
                        {
                            string aptNumString = "";
                            if (apt != startApt) i++;

                            // string format 1
                            aptNumString = AddressFormat.GenerateAddressFormat(form2.SelectedFormat1, form2.Prefix1, form2.SelectedBlock, floor, apt, 
                                                        startApt, i, form2.ChkAptChar1, FloorDigitNum, AptDigitNum);
                            string aptNumStr = aptNumString;

                            // string format 2
                            aptNumString = AddressFormat.GenerateAddressFormat(form2.SelectedFormat2, form2.Prefix2, form2.SelectedBlock, floor, apt, 
                                                        startApt, i, form2.ChkAptChar2, FloorDigitNum, AptDigitNum);
                            aptNumStr = aptNumStr + aptNumString;

                            // string format 3
                            aptNumString = AddressFormat.GenerateAddressFormat(form2.SelectedFormat3, form2.Prefix3, form2.SelectedBlock, floor, apt, 
                                                    startApt, i, form2.ChkAptChar3, FloorDigitNum, AptDigitNum);
                            aptNumStr = aptNumStr + aptNumString;

                            aptNumString = "";
                            // string format 4
                            if (!string.IsNullOrEmpty(form2.Prefix4))
                                aptNumString = string.Format("{0}", form2.Prefix4);

                            aptNumStr = aptNumStr + aptNumString;

                            string aptString = "";
                            aptString = string.Format("{0}", aptNumStr);

                            newMultiStorey.Apartment = aptString;
                            bool noDuplicate = CheckDuplicateFormat(newMultiStorey.Apartment, newMultiStorey.Floor);

                            if (noDuplicate)
                            {
                                newMultiStorey = repo.Insert(newMultiStorey);
                                ListViewItem item = new ListViewItem();
                                item.Text = newMultiStorey.OID.ToString();
                                item.SubItems.Add(newMultiStorey.Floor);
                                item.SubItems.Add(newMultiStorey.Apartment);
                                item.Tag = newMultiStorey;
                                controlMultiStorey.Items.Add(item);
                                controlMultiStorey.FixColumnWidth();
                            }
                        }
                    }
                }
                txtTotalApt.Text = controlMultiStorey.Items.Count.ToString();
            }
        }

        private void controlMultiStorey_EditClick(object sender, EventArgs e)
        {
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Edit multistorey address button clicked");
            sb.Append("\n");

            if (controlMultiStorey.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
            ListViewItem item = controlMultiStorey.SelectedItems[0];
            GMultiStorey MultiStorey = (GMultiStorey)item.Tag;

            GMultiStorey multiStorey = repo.GetById<GMultiStorey>(MultiStorey.OID);

            using (EditMultiStoreyForm form = new EditMultiStoreyForm(multiStorey))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening edit multistorey address format form completed.");
                sb.Append("\n");

                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button edit multistorey address format form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(multiStorey);

                    item.SubItems.Clear();
                    item.Text = multiStorey.OID.ToString();
                    item.SubItems.Add(multiStorey.Floor);
                    item.SubItems.Add(multiStorey.Apartment);
                    item.Tag = multiStorey;
                    controlMultiStorey.FixColumnWidth();
                    controlMultiStorey.Refresh();
                }
            }
        }

        private void controlMultiStorey_DeleteClick(object sender, EventArgs e)
        {
            sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Delete multistorey button clicked");
            sb.Append("\n");

            using (new WaitCursor())
            {
                if (controlMultiStorey.SelectedItems.Count == 0)
                {
                    return;
                }

                // modified by Noraini - Feb2019
                RepositoryFactory repo = new RepositoryFactory(_building.SegmentName);
                foreach (ListViewItem item in controlMultiStorey.SelectedItems)
                {
                    GMultiStorey MultiStorey = (GMultiStorey)item.Tag;
                    GMultiStorey multiStorey = repo.GetById<GMultiStorey>(MultiStorey.OID);

                    repo.Delete(multiStorey);
                    item.Remove();
                    controlMultiStorey.FixColumnWidth();
                }
                txtTotalApt.Text = controlMultiStorey.Items.Count.ToString();

                // end modified

            }
        }

        private bool CheckDuplicateFormat(string Apt, string Floor)
        {
            bool noDuplicate = true;
            foreach (GMultiStorey MultiStoriesX in _building.GetMultiStories())
            {
                if (Apt == MultiStoriesX.Apartment && Floor == MultiStoriesX.Floor)
                {
                    MessageBox.Show("MultiStorey Format is duplicate", "MultiStorey Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    noDuplicate = false;
                    break;
                }
            }
            return noDuplicate;
        }

        public string MessageButtonClick()
        {
            return "\n" + sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Controls;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;

namespace Geomatic.UI.Forms
{
    public partial class MultiStoreyForm : CollapsibleForm
    {
        public string InsertedFloor
        {
            get { return txtFloorNum.Text; }
        }

        public string InsertedApt
        {
            get { return txtAptNum.Text; }
        }

        protected GMultiStorey _multiStorey;

        public MultiStoreyForm()
            : this(null)
        {
        }

        public MultiStoreyForm(GMultiStorey multiStorey)
        {
            InitializeComponent();
            _multiStorey = multiStorey;
        }

        protected override void Form_Load()
        {
            txtId.Text = _multiStorey.OID.ToString();

            txtFloorNum.Text = _multiStorey.Floor;
            txtAptNum.Text = _multiStorey.Apartment;

            txtDateUpdated.Text = _multiStorey.DateUpdated;
            txtUpdater.Text = _multiStorey.UpdatedBy;

            // noraini ali - OKt 2021 - Display Username & Date Infomation
            txtCreator.Text = _multiStorey.CreatedBy;
            txtDateCreated.Text = _multiStorey.DateCreated;

        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Floor
            bool isFloorValid = MultiStoreyValidator.CheckFloor(txtFloorNum.Text);
            LabelColor(lblFloorNumber, isFloorValid);
            pass &= isFloorValid;

            // Apt
            bool isAptValid = MultiStoreyValidator.CheckApt(txtAptNum.Text);
            LabelColor(lblAptNum, isAptValid);
            pass &= isAptValid;

            return pass;
        }

        public void SetValues()
        {
            _multiStorey.Apartment = InsertedApt;
            _multiStorey.Floor = InsertedFloor;
        }
    }
}

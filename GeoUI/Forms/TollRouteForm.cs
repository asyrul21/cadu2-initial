﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Validators;
using Geomatic.UI.Controls;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;

namespace Geomatic.UI.Forms
{
    public partial class TollRouteForm : CollapsibleForm
    {
        protected GTollRoute _tollRoute;
        protected int? _junctionType;

        public SegmentName? SelectedInTollSegment
        {
            get
            {
                return (cbInTollSegment.SelectedItem == null) ? (SegmentName?)null : ((ComboBoxItem<string, SegmentName>)cbInTollSegment.SelectedItem).Value;
            }
        }

        public SegmentName? SelectedOutTollSegment
        {
            get
            {
                return (cbOutTollSegment.SelectedItem == null) ? (SegmentName?)null : ((ComboBoxItem<string, SegmentName>)cbOutTollSegment.SelectedItem).Value;
            }
        }

        public int? SelectedTollType
        {
            get { return (cbTollType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GTollRouteType>)cbTollType.SelectedItem).Value.Code; }
        }

        public int? SelectedInTollID
        {
            get { return (string.IsNullOrEmpty(txtInTollID.Text)) ? (int?)null : Convert.ToInt32(txtInTollID.Text); }
        }

        public int? SelectedOutTollID
        {
            get { return (string.IsNullOrEmpty(txtOutTollID.Text)) ? (int?)null : Convert.ToInt32(txtOutTollID.Text); }
        }

        public TollRouteForm()
            : this(null, null)
        {
        }

        public TollRouteForm(GTollRoute tollRoute, int? junctionType)
        {
            InitializeComponent();
            _tollRoute = tollRoute;
            _junctionType = junctionType;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Toll Route Form");

            ComboBoxUtils.PopulateTollRouteType(cbTollType);
            ComboBoxUtils.PopulateSegment(cbInTollSegment);
            ComboBoxUtils.PopulateSegment(cbOutTollSegment);

            txtId.Text = _tollRoute.OID.ToString();

            cbTollType.Text = _tollRoute.TypeValue;
            cbInTollSegment.SelectedItem = SetInTollSegment(StringUtils.GetSegmentName(_tollRoute.InSegment));
            txtInTollID.Text = _tollRoute.InID.ToString();
            cbOutTollSegment.SelectedItem = SetOutTollSegment(StringUtils.GetSegmentName(_tollRoute.OutSegment));
            txtOutTollID.Text = _tollRoute.OutID.ToString();

            txtDateCreated.Text = _tollRoute.DateCreated;
            txtDateUpdated.Text = _tollRoute.DateUpdated;
            txtCreator.Text = _tollRoute.CreatedBy;
            txtUpdater.Text = _tollRoute.UpdatedBy;

            controlPrice.Columns.Add("ID");
            controlPrice.Columns.Add("Vehicle Type");
            controlPrice.Columns.Add("Price");
            controlPrice.Columns.Add("Created By");
            controlPrice.Columns.Add("Date Created");
            controlPrice.Columns.Add("Updated By");
            controlPrice.Columns.Add("Date Updated");

            PopulatePrice();

            EnableInOutToll(_junctionType);
        }

        private void PopulatePrice()
        {
            controlPrice.BeginUpdate();
            controlPrice.Sorting = SortOrder.None;
            controlPrice.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();
            List<GTollPrice> tollPrices;
            RepositoryFactory repo = new RepositoryFactory();
            Query<GTollPrice> query = new Query<GTollPrice>();
            query.Obj.TollRouteID = _tollRoute.OID;

            tollPrices = repo.Search<GTollPrice>(query).ToList();
            foreach (GTollPrice tollPrice in tollPrices)
            {
                ListViewItem item = CreateItem(tollPrice);
                items.Add(item);
            }
            controlPrice.Items.AddRange(items.ToArray());
            controlPrice.FixColumnWidth();
            controlPrice.EndUpdate();
        }

        private ListViewItem CreateItem(GTollPrice tollPrice)
        {
            ListViewItem item = new ListViewItem();
            item.Text = tollPrice.OID.ToString();
            item.SubItems.Add(tollPrice.VehicleTypeValue);
            item.SubItems.Add(tollPrice.Price.ToString());
            item.SubItems.Add(tollPrice.CreatedBy);
            item.SubItems.Add(tollPrice.DateCreated);
            item.SubItems.Add(tollPrice.UpdatedBy);
            item.SubItems.Add(tollPrice.DateUpdated);
            item.Tag = tollPrice;
            return item;
        }

        private ComboBoxItem<string, SegmentName> SetInTollSegment(SegmentName segmentName)
        {
            foreach (ComboBoxItem<string, SegmentName> item in cbInTollSegment.Items)
            {
                if (item.Value == segmentName)
                {
                    return item;
                }
            }
            return null;
        }

        private ComboBoxItem<string, SegmentName> SetOutTollSegment(SegmentName segmentName)
        {
            foreach (ComboBoxItem<string, SegmentName> item in cbOutTollSegment.Items)
            {
                if (item.Value == segmentName)
                {
                    return item;
                }
            }
            return null;
        }

        private void cbInTollSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbInTollSegment.SelectedItem == null)
                btnSetInTollID.Enabled = false;
            else
                btnSetInTollID.Enabled = true;
        }

        private void cbOutTollSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutTollSegment.SelectedItem == null)
                btnSetOutTollID.Enabled = false;
            else
                btnSetOutTollID.Enabled = true;
        }

        private void btnSetInTollID_Click(object sender, EventArgs e)
        {
            int inTollType = GJunctionType.IN_TOLL;
            txtInTollID.Text = SelectTollGate(SelectedInTollSegment, inTollType);
        }

        private void btnSetOutTollID_Click(object sender, EventArgs e)
        {
            int outTollType = GJunctionType.OUT_TOLL;
            txtOutTollID.Text = SelectTollGate(SelectedOutTollSegment, outTollType);
        }

        private string SelectTollGate(SegmentName? segmentName, int tollType)
        {
            if (!segmentName.HasValue)
            {
                return null;
            }

            List<GJunction> junctions;
            RepositoryFactory repo = new RepositoryFactory(segmentName.Value);
            Query<GJunction> query = new Query<GJunction>(segmentName.Value);
            query.AddClause(() => query.Obj.Type, string.Format("IN({0}, {1})", GJunctionType.TOLL_GATE, tollType));
            junctions = repo.Search<GJunction>(query).ToList();

            using (ChooseFeatureForm chooseForm = ChooseFormFactory.CreateForm(junctions))
            {
                if (chooseForm.ShowDialog(this) != DialogResult.OK)
                {
                    return null;
                }
                GJunction junction = (GJunction)chooseForm.SelectedFeature;

                return junction.OID.ToString();
            }
        }

        private void cbTollType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (SelectedTollType)
            {
                case 0:
                    EnableInToll(false);
                    ClearOutToll();
                    break;
                case 1:
                    EnableOutToll(true);
                    break;
                default:
                    EnableOutToll(false);
                    break;

            }
        }

        private void EnableInToll(Boolean boolean)
        {
            cbInTollSegment.Enabled = boolean;
            btnSetInTollID.Enabled = false;
        }

        private void EnableOutToll(Boolean boolean)
        {
            cbOutTollSegment.Enabled = boolean;
            btnSetOutTollID.Enabled = false;
        }

        private void ClearOutToll()
        {
            cbOutTollSegment.Enabled = false;
            cbOutTollSegment.SelectedItem = null;
            btnSetOutTollID.Enabled = false;
            txtOutTollID.Text = null;
        }

        private void EnableInOutToll(int? junctionType)
        {
            switch (junctionType)
            {
                case GJunctionType.IN_TOLL:
                    EnableInToll(false);
                    EnableOutToll(false);
                    cbTollType_SelectedIndexChanged(null, null);
                    cbOutTollSegment_SelectedIndexChanged(null, null);
                    break;
                case GJunctionType.OUT_TOLL:
                    EnableInToll(true);
                    EnableOutToll(false);
                    cbInTollSegment_SelectedIndexChanged(null, null);
                    cbTollType.Enabled = false;
                    break;
                case GJunctionType.TOLL_GATE:
                    EnableInToll(true);
                    EnableOutToll(true);
                    cbInTollSegment_SelectedIndexChanged(null, null);
                    cbTollType_SelectedIndexChanged(null, null);
                    cbOutTollSegment_SelectedIndexChanged(null, null);
                    break;
                default:
                    throw new QualityControlException("Invalid Junction type for toll routes");
            }
        }

        private void controlPrice_AddClick(object sender, EventArgs e)
        {
            RepositoryFactory repo = new RepositoryFactory(_tollRoute.SegmentName);

            GTollPrice newTollPrice = repo.NewObj<GTollPrice>();
            newTollPrice.TollRouteID = _tollRoute.OID;
            newTollPrice = repo.Insert(newTollPrice, false);
            newTollPrice.Init();

            using (AddTollPriceForm form = new AddTollPriceForm(newTollPrice))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    repo.Delete(newTollPrice);
                    return;
                }

                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(newTollPrice);

                    ListViewItem item = new ListViewItem();
                    item.Text = newTollPrice.OID.ToString();
                    item.SubItems.Add(newTollPrice.VehicleTypeValue);
                    item.SubItems.Add(newTollPrice.Price.ToString());
                    item.SubItems.Add(newTollPrice.CreatedBy);
                    item.SubItems.Add(newTollPrice.DateCreated);
                    item.SubItems.Add(newTollPrice.UpdatedBy);
                    item.SubItems.Add(newTollPrice.DateUpdated);
                    item.Tag = newTollPrice;
                    controlPrice.Items.Add(item);
                    controlPrice.FixColumnWidth();
                }
            }

        }

        public void SetValues()
        {
            _tollRoute.Type = SelectedTollType;
            _tollRoute.InSegment = SelectedInTollSegment.ToString();
            _tollRoute.InID = SelectedInTollID;
            _tollRoute.OutSegment = SelectedOutTollSegment.ToString();
            _tollRoute.OutID = SelectedOutTollID;
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Toll Type
            bool isTypeValid = TollRouteValidator.CheckType(SelectedTollType);
            LabelColor(lblTollType, isTypeValid);
            pass &= isTypeValid;

            // In Toll Segment
            bool isInTollSegmentValid = TollRouteValidator.CheckInTollSegment(cbInTollSegment.Text);
            LabelColor(lblInTollSegment, isInTollSegmentValid);
            pass &= isInTollSegmentValid;

            // In Toll ID
            bool isInTollIDValid = TollRouteValidator.CheckInTollID(SelectedInTollID);
            LabelColor(lblInTollID, isInTollIDValid);
            pass &= isInTollIDValid;

            // Out Toll Segment
            bool isOutTollSegmentValid = TollRouteValidator.CheckOutTollSegment(cbOutTollSegment.Text, SelectedTollType);
            LabelColor(lblOutTollSegment, isOutTollSegmentValid);
            pass &= isOutTollSegmentValid;

            // Out Toll ID
            bool isOutTollIDValid = TollRouteValidator.CheckOutTollID(SelectedOutTollID, SelectedTollType);
            LabelColor(lblOutTollID, isOutTollIDValid);
            pass &= isOutTollIDValid;

            return pass;
        }

        private void controlPrice_EditClick(object sender, EventArgs e)
        {
            if (controlPrice.SelectedItems.Count == 0)
            {
                return;
            }

            RepositoryFactory repo = new RepositoryFactory(_tollRoute.SegmentName);
            ListViewItem item = controlPrice.SelectedItems[0];
            GTollPrice tollPrice = (GTollPrice)item.Tag;

            using (EditTollPriceForm form = new EditTollPriceForm(tollPrice))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    form.SetValues();
                    repo.Update(tollPrice);

                    item.SubItems.Clear();
                    item.Text = tollPrice.OID.ToString();
                    item.SubItems.Add(tollPrice.VehicleTypeValue);
                    item.SubItems.Add(tollPrice.Price.ToString());
                    item.SubItems.Add(tollPrice.CreatedBy);
                    item.SubItems.Add(tollPrice.DateCreated);
                    item.SubItems.Add(tollPrice.UpdatedBy);
                    item.SubItems.Add(tollPrice.DateUpdated);
                    item.Tag = tollPrice;
                    controlPrice.FixColumnWidth();
                }
            }
        }

        private void controlPrice_DeleteClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlPrice.SelectedItems.Count == 0)
                {
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory(_tollRoute.SegmentName);
                ListViewItem item = controlPrice.SelectedItems[0];
                GTollPrice tollPrice = (GTollPrice)item.Tag;
                repo.Delete(tollPrice);
                item.Remove();
                controlPrice.FixColumnWidth();
            }
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

    }
}

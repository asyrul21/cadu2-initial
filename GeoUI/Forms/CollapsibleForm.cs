﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Forms
{
    public partial class CollapsibleForm : BaseForm2
    {
        private SegmentName segmentName;

        public CollapsibleForm()
        {
            InitializeComponent();
        }

        public CollapsibleForm(SegmentName segmentName)
        {
            this.segmentName = segmentName;
        }

        protected override void Form_Load()
        {
            AcceptButton = btnApply;
        }

        private void CollapsibleForm_Shown(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            OnCollapsedChanged();
        }

        protected void OnCollapsedChanged()
        {
            Height += bottomPanel.Location.Y + bottomPanel.Height - ClientSize.Height;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
                //else
                //{
                //    DialogResult = DialogResult.None;
                //}
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual bool ValidateValues()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.Toolbox
{
    internal class ToolboxFactory
    {
        public static IToolbox Create<T>() where T : IToolbox
        {
            return Activator.CreateInstance<T>();
        }
    }
}

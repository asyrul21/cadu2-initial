﻿namespace Geomatic.UI.Forms.Toolbox
{
    partial class GoToToolbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoToToolbox));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.btnZoomTo = new System.Windows.Forms.ToolStripButton();
            this.btnPanTo = new System.Windows.Forms.ToolStripButton();
            this.separator20 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGoToGoogleMap = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtX = new Geomatic.UI.Controls.TextBoxExt();
            this.lblY = new System.Windows.Forms.Label();
            this.txtLon = new Geomatic.UI.Controls.TextBoxExt();
            this.lblLon = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.txtY = new Geomatic.UI.Controls.TextBoxExt();
            this.txtLat = new Geomatic.UI.Controls.TextBoxExt();
            this.lblLat = new System.Windows.Forms.Label();
            this.btnClearMarker = new System.Windows.Forms.ToolStripButton();
            this.toolStrip.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnZoomTo,
            this.btnPanTo,
            this.btnClearMarker,
            this.separator20,
            this.btnGoToGoogleMap});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(284, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip";
            // 
            // btnZoomTo
            // 
            this.btnZoomTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnZoomTo.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomTo.Image")));
            this.btnZoomTo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnZoomTo.Name = "btnZoomTo";
            this.btnZoomTo.Size = new System.Drawing.Size(23, 22);
            this.btnZoomTo.Text = "Zoom To";
            this.btnZoomTo.Click += new System.EventHandler(this.btnZoomTo_Click);
            // 
            // btnPanTo
            // 
            this.btnPanTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPanTo.Image = ((System.Drawing.Image)(resources.GetObject("btnPanTo.Image")));
            this.btnPanTo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPanTo.Name = "btnPanTo";
            this.btnPanTo.Size = new System.Drawing.Size(23, 22);
            this.btnPanTo.Text = "Pan To";
            this.btnPanTo.Click += new System.EventHandler(this.btnPanTo_Click);
            // 
            // separator20
            // 
            this.separator20.Name = "separator20";
            this.separator20.Size = new System.Drawing.Size(6, 25);
            // 
            // btnGoToGoogleMap
            // 
            this.btnGoToGoogleMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnGoToGoogleMap.Image = ((System.Drawing.Image)(resources.GetObject("btnGoToGoogleMap.Image")));
            this.btnGoToGoogleMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGoToGoogleMap.Name = "btnGoToGoogleMap";
            this.btnGoToGoogleMap.Size = new System.Drawing.Size(23, 22);
            this.btnGoToGoogleMap.Text = "Go To Google Map";
            this.btnGoToGoogleMap.Click += new System.EventHandler(this.btnGoToGoogleMap_Click);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtX, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblY, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.txtLon, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.lblLon, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblX, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtY, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.txtLat, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblLat, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(284, 56);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // txtX
            // 
            this.txtX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtX.Location = new System.Drawing.Point(38, 4);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(101, 20);
            this.txtX.TabIndex = 1;
            this.txtX.AfterPaste += new Geomatic.UI.Controls.TextBoxExt.PasteEventHandler(this.txtX_AfterPaste);
            this.txtX.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtX_KeyUp);
            // 
            // lblY
            // 
            this.lblY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(145, 7);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(29, 13);
            this.lblY.TabIndex = 2;
            this.lblY.Text = "Y:";
            this.lblY.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLon
            // 
            this.txtLon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLon.Location = new System.Drawing.Point(180, 32);
            this.txtLon.Name = "txtLon";
            this.txtLon.Size = new System.Drawing.Size(101, 20);
            this.txtLon.TabIndex = 7;
            this.txtLon.AfterPaste += new Geomatic.UI.Controls.TextBoxExt.PasteEventHandler(this.txtLon_AfterPaste);
            this.txtLon.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtLon_KeyUp);
            // 
            // lblLon
            // 
            this.lblLon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLon.AutoSize = true;
            this.lblLon.Location = new System.Drawing.Point(145, 35);
            this.lblLon.Name = "lblLon";
            this.lblLon.Size = new System.Drawing.Size(29, 13);
            this.lblLon.TabIndex = 6;
            this.lblLon.Text = "Lon:";
            this.lblLon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblX
            // 
            this.lblX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(3, 7);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(29, 13);
            this.lblX.TabIndex = 0;
            this.lblX.Text = "X:";
            this.lblX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtY
            // 
            this.txtY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtY.Location = new System.Drawing.Point(180, 4);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(101, 20);
            this.txtY.TabIndex = 3;
            this.txtY.AfterPaste += new Geomatic.UI.Controls.TextBoxExt.PasteEventHandler(this.txtY_AfterPaste);
            this.txtY.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtY_KeyUp);
            // 
            // txtLat
            // 
            this.txtLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLat.Location = new System.Drawing.Point(38, 32);
            this.txtLat.Name = "txtLat";
            this.txtLat.Size = new System.Drawing.Size(101, 20);
            this.txtLat.TabIndex = 5;
            this.txtLat.AfterPaste += new Geomatic.UI.Controls.TextBoxExt.PasteEventHandler(this.txtLat_AfterPaste);
            this.txtLat.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtLat_KeyUp);
            // 
            // lblLat
            // 
            this.lblLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLat.AutoSize = true;
            this.lblLat.Location = new System.Drawing.Point(3, 35);
            this.lblLat.Name = "lblLat";
            this.lblLat.Size = new System.Drawing.Size(29, 13);
            this.lblLat.TabIndex = 4;
            this.lblLat.Text = "Lat:";
            this.lblLat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnClearMarker
            // 
            this.btnClearMarker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClearMarker.Image = ((System.Drawing.Image)(resources.GetObject("btnClearMarker.Image")));
            this.btnClearMarker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearMarker.Name = "btnClearMarker";
            this.btnClearMarker.Size = new System.Drawing.Size(23, 22);
            this.btnClearMarker.Text = "Clear Marker";
            this.btnClearMarker.Click += new System.EventHandler(this.btnClearMarker_Click);
            // 
            // GoToToolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 81);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.toolStrip);
            this.Name = "GoToToolbox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GoToToolbox";
            this.Load += new System.EventHandler(this.GoToToolbox_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton btnZoomTo;
        private System.Windows.Forms.ToolStripButton btnPanTo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblLat;
        private System.Windows.Forms.Label lblLon;
        private System.Windows.Forms.Label lblY;
        private Controls.TextBoxExt txtX;
        private Controls.TextBoxExt txtY;
        private Controls.TextBoxExt txtLon;
        private Controls.TextBoxExt txtLat;
        private System.Windows.Forms.ToolStripButton btnGoToGoogleMap;
        private System.Windows.Forms.ToolStripSeparator separator20;
        private System.Windows.Forms.ToolStripButton btnClearMarker;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;

namespace Geomatic.UI.Forms.Toolbox
{
    public partial class Toolbox : BaseForm, IToolbox
    {
        public delegate void FormHidEventHandler(object sender, EventArgs e);
        public event FormHidEventHandler Hid;
        private bool _shown;
        private bool _hideOnClose;
        protected SegmentName? Segment;

        public Toolbox()
        {
            InitializeComponent();
            _hideOnClose = true;
        }

        [Browsable(true), Category("Misc"), DefaultValue(true)]
        public bool HideOnClose
        {
            set { _hideOnClose = value; }
            get { return _hideOnClose; }
        }

        public new void Show()
        {
            base.Show();
            if (_shown)
            {
                OnShown(EventArgs.Empty);
            }
            _shown = true;
            if (!Focused)
            {
                Focus();
            }
        }

        public new void Show(IWin32Window owner)
        {
            if (_shown)
            {
                Show();
            }
            else
            {
                base.Show(owner);
                if (_shown)
                {
                    OnShown(EventArgs.Empty);
                }
                _shown = true;
                if (!Focused)
                {
                    Focus();
                }
            }
        }

        public new void Hide()
        {
            base.Hide();
            OnHid();
        }

        private void Toolbox_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.ApplicationExitCall ||
                e.CloseReason == CloseReason.FormOwnerClosing ||
                e.CloseReason == CloseReason.MdiFormClosing ||
                e.CloseReason == CloseReason.TaskManagerClosing ||
                e.CloseReason == CloseReason.WindowsShutDown)
            {
                return;
            }

            if (_hideOnClose)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void OnHid()
        {
            if (Hid != null)
            {
                Hid(this, EventArgs.Empty);
            }
        }

        public void OnSegmentChanged(SegmentName segmentName)
        {
            Segment = segmentName;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;

namespace Geomatic.UI.Forms.Toolbox
{
    public interface IToolbox
    {
        void OnSegmentChanged(SegmentName segmentName);
        void Show();
        void Show(IWin32Window owner);
        void Hide();
    }
}

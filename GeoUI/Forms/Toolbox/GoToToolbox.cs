﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.CRS;
using Geomatic.Core;
using System.Diagnostics;

namespace Geomatic.UI.Forms.Toolbox
{
    public partial class GoToToolbox : Toolbox
    {
        private const string URL_FORMAT = @"http://www.google.com.my/maps/place/{0}+{1}/@{2},{3},19z";

        public EventHandler<GoToEventArgs> ZoomTo;
        public EventHandler<GoToEventArgs> PanTo;
        public EventHandler ClearMarker;

        private Area Area
        {
            get
            {
                if (!Segment.HasValue)
                {
                    return Area.UNDECIDED;
                }
                return (Segment == SegmentName.AS
                    || Segment == SegmentName.JH
                    || Segment == SegmentName.JP
                    || Segment == SegmentName.KN
                    || Segment == SegmentName.KV
                    || Segment == SegmentName.MK
                    || Segment == SegmentName.PG
                    || Segment == SegmentName.TG) ? Area.WEST :
                    (Segment == SegmentName.KG
                    || Segment == SegmentName.KK) ? Area.EAST :
                    Area.UNDECIDED;
            }
        }

        public GoToToolbox()
        {
            InitializeComponent();
        }

        private void GoToToolbox_Load(object sender, EventArgs e)
        {
            RefreshTitle("Go To");
        }

        private void btnZoomTo_Click(object sender, EventArgs e)
        {
            double x;
            double y;
            if (double.TryParse(txtX.Text, out x) && double.TryParse(txtY.Text, out y))
            {
                OnZoomTo(new GoToEventArgs(x, y));
            }
            else
            {
                double lon;
                double lat;
                if (Area == Area.UNDECIDED)
                {
                    return;
                }
                if (double.TryParse(txtLon.Text, out lon) && double.TryParse(txtLat.Text, out lat))
                {
                    Point3D point = Geomatic.Core.CRS.Convert.LatLongToRso(lon, lat, Area);
                    OnZoomTo(new GoToEventArgs(point.X, point.Y));
                }
            }
        }

        private void btnPanTo_Click(object sender, EventArgs e)
        {
            double x;
            double y;
            if (double.TryParse(txtX.Text, out x) && double.TryParse(txtY.Text, out y))
            {
                OnPanTo(new GoToEventArgs(x, y));
            }
            else
            {
                double lon;
                double lat;
                if (Area == Area.UNDECIDED)
                {
                    return;
                }
                if (double.TryParse(txtLon.Text, out lon) && double.TryParse(txtLat.Text, out lat))
                {
                    Point3D point = Geomatic.Core.CRS.Convert.LatLongToRso(lon, lat, Area);
                    OnPanTo(new GoToEventArgs(point.X, point.Y));
                }
            }
        }

        private void btnClearMarker_Click(object sender, EventArgs e)
        {
            OnClearMarker();
            txtLat.Text = "";
            txtLon.Text = "";
            txtX.Text = "";
            txtY.Text = "";
        }

        private void btnGoToGoogleMap_Click(object sender, EventArgs e)
        {
            if (Area == Area.UNDECIDED)
            {
                return;
            }

            double x;
            double y;
            if (double.TryParse(txtX.Text, out x) && double.TryParse(txtY.Text, out y))
            {
                GeoPoint point = Geomatic.Core.CRS.Convert.RsoToLatLong(x, y, Area);
                Process.Start(string.Format(URL_FORMAT, point.Latitude, point.Longitude, point.Latitude, point.Longitude));
            }
            else
            {
                double lon;
                double lat;
                if (double.TryParse(txtLon.Text, out lon) && double.TryParse(txtLat.Text, out lat))
                {
                    Process.Start(string.Format(URL_FORMAT, lat, lon, lat, lon));
                }
            }
        }

        private void OnZoomTo(GoToEventArgs e)
        {
            if (ZoomTo != null)
                ZoomTo(this, e);
        }

        private void OnPanTo(GoToEventArgs e)
        {
            if (PanTo != null)
                PanTo(this, e);
        }

        private void OnClearMarker()
        {
            if (ClearMarker != null)
                ClearMarker(this, EventArgs.Empty);
        }

        private void txtX_KeyUp(object sender, KeyEventArgs e)
        {
            OnXChanged();
        }

        private void txtX_AfterPaste(object sender, EventArgs e)
        {
            OnXChanged();
        }

        private void OnXChanged()
        {
            if (Area == Area.UNDECIDED)
            {
                return;
            }
            if (txtX.Text == string.Empty)
            {
                return;
            }
            OnXyChanged();
        }

        private void txtY_KeyUp(object sender, KeyEventArgs e)
        {
            OnYChanged();
        }

        private void txtY_AfterPaste(object sender, EventArgs e)
        {
            OnYChanged();
        }

        private void OnYChanged()
        {
            if (Area == Area.UNDECIDED)
            {
                return;
            }
            if (txtY.Text == string.Empty)
            {
                return;
            }
            OnXyChanged();
        }

        private void OnXyChanged()
        {
            double x;
            double y;
            if (double.TryParse(txtX.Text, out x) && double.TryParse(txtY.Text, out y))
            {
                GeoPoint point = Geomatic.Core.CRS.Convert.RsoToLatLong(x, y, Area);
                txtLon.Text = point.Longitude.ToString("0.0000");
                txtLat.Text = point.Latitude.ToString("0.0000");
            }
        }

        private void txtLon_KeyUp(object sender, KeyEventArgs e)
        {
            OnLonChanged();
        }

        private void txtLon_AfterPaste(object sender, EventArgs e)
        {
            OnLonChanged();
        }

        private void OnLonChanged()
        {
            if (Area == Area.UNDECIDED)
            {
                return;
            }
            if (txtLon.Text == string.Empty)
            {
                return;
            }
            OnLonLatChanged();
        }

        private void txtLat_KeyUp(object sender, KeyEventArgs e)
        {
            OnLatChanged();
        }

        private void txtLat_AfterPaste(object sender, EventArgs e)
        {
            OnLatChanged();
        }

        private void OnLatChanged()
        {
            if (Area == Area.UNDECIDED)
            {
                return;
            }
            if (txtLat.Text == string.Empty)
            {
                return;
            }
            OnLonLatChanged();
        }

        private void OnLonLatChanged()
        {
            double lon;
            double lat;
            if (double.TryParse(txtLon.Text, out lon) && double.TryParse(txtLat.Text, out lat))
            {
                Point3D point = Geomatic.Core.CRS.Convert.LatLongToRso(lon, lat, Area);
                txtX.Text = point.X.ToString("0.0000");
                txtY.Text = point.Y.ToString("0.0000");
            }
        }
    }
}

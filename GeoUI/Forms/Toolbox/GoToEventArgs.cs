﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Forms.Toolbox
{
    public class GoToEventArgs : EventArgs
    {
        public readonly double X;
        public readonly double Y;

        public GoToEventArgs(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core;

namespace Geomatic.UI.Forms.Toolbox
{
    public class ToolboxManager : IDisposable
    {
        private Dictionary<Type, IToolbox> _toolboxes;

        public ToolboxManager()
        {
            _toolboxes = new Dictionary<Type, IToolbox>();
        }

        public IToolbox GetView<T>() where T : IToolbox
        {
            string key = typeof(T).ToString();
            IToolbox toolbox;
            if (_toolboxes.ContainsKey(typeof(T)))
            {
                _toolboxes.TryGetValue(typeof(T), out toolbox);
            }
            else
            {
                toolbox = ToolboxFactory.Create<T>();
                _toolboxes.Add(typeof(T), toolbox);
            }
            return toolbox;
        }

        public IEnumerable<IToolbox> GetToolboxes()
        {
            return _toolboxes.Values;
        }

        public void OnSegmentChanged(SegmentName segmentName)
        {
            foreach (IToolbox toolbox in GetToolboxes())
            {
                toolbox.OnSegmentChanged(segmentName);
            }
        }

        public void Dispose()
        {
            _toolboxes.Clear();
        }
    }
}

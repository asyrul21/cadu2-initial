﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Edit;
using Geomatic.Core;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms
{
    public partial class UserGroupForm : BaseForm
    {
        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public IEnumerable<GFunction> FunctionsForAssociate
        {
            get
            {
                foreach (TreeNode node in tvFunction.Nodes)
                {
                    foreach (TreeNode subNode in node.Nodes)
                    {
                        if (subNode.Checked)
                        {
                            yield return (GFunction)subNode.Tag;
                        }
                    }
                }
            }
        }

        public IEnumerable<GFunction> FunctionsForDissociate
        {
            get
            {
                foreach (TreeNode node in tvFunction.Nodes)
                {
                    foreach (TreeNode subNode in node.Nodes)
                    {
                        if (!subNode.Checked)
                        {
                            yield return (GFunction)subNode.Tag;
                        }
                    }
                }
            }
        }

        public IEnumerable<GUser> UsersForAssociate
        {
            get
            {
                foreach (ListViewItem item in controlUsers.Items)
                {
                    yield return (GUser)item.Tag;
                }
            }
        }

        public List<GUser> UsersForDissociate { get; protected set; }

        public UserGroupForm()
        {
            InitializeComponent();
            UsersForDissociate = new List<GUser>();
        }

        private void UserGroupForm_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            using (new WaitCursor())
            {
                AcceptButton = btnApply;
                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {
            controlUsers.Columns.Add("Name");
            tvFunction.BeginUpdate();
            PopulateFunctions();
            tvFunction.EndUpdate();
        }

        protected virtual void PopulateFunctions()
        {
            foreach (GFunction function in GFunction.GetRoot())
            {
                TreeNode newNode = new TreeNode(function.Name);
                newNode.Tag = function;
                int index = tvFunction.Nodes.Add(newNode);

                foreach (GFunction subFunction in function.GetChild())
                {
                    TreeNode newSubNode = new TreeNode(subFunction.Name);
                    newSubNode.Tag = subFunction;
                    tvFunction.Nodes[index].Nodes.Add(newSubNode);
                }
            }
        }

        private void controlUsers_AssociateClick(object sender, EventArgs e)
        {
            using (ChooseUserForm chooseForm = new ChooseUserForm())
            {
                if (chooseForm.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GUser user = chooseForm.SelectedUser;

                if (!UsersForAssociate.Contains(user))
                {
                    ListViewItem item = CreateUserItem(user);
                    controlUsers.Items.Add(item);
                }

                if (UsersForDissociate.Contains(user))
                {
                    UsersForDissociate.Remove(user);
                }
            }
        }

        private void controlUsers_DissociateClick(object sender, EventArgs e)
        {
            if (controlUsers.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = controlUsers.SelectedItems[0];
            GUser user = (GUser)item.Tag;

            if (!UsersForDissociate.Contains(user))
            {
                UsersForDissociate.Add(user);
            }
            item.Remove();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual bool ValidateValues()
        {
            bool pass = true;

            // Name
            bool isNameValid = true;
            isNameValid &= !StringUtils.HasExtraSpaces(txtName.Text); ;
            isNameValid &= !string.IsNullOrEmpty(txtName.Text);
            pass &= isNameValid;
            lblName.ForeColor = isNameValid ? Color.Black : Color.Red;

            return pass;
        }

        protected ListViewItem CreateUserItem(GUser user)
        {
            ListViewItem item = new ListViewItem();
            item.Text = user.Name;
            item.Tag = user;
            return item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Forms
{
    public partial class StreetRestrictionForm : CollapsibleForm
    {
        #region Properties

        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        #endregion

        protected GStreetRestriction _streetRestriction;

        public StreetRestrictionForm()
            : this(null)
        {
        }

        public StreetRestrictionForm(GStreetRestriction streetRestriction)
        {
            InitializeComponent();
            _streetRestriction = streetRestriction;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);

            cbNaviStatus.Text = _streetRestriction.NavigationStatusValue;
            cbSource.Text = _streetRestriction.SourceValue;

            GStreet startStreet = _streetRestriction.GetStartStreet();
            txtStreetType.Text = (startStreet == null) ? string.Empty : startStreet.TypeValue;
            txtStreetName.Text = (startStreet == null) ? string.Empty : startStreet.Name;

            txtJunctionId.Text = _streetRestriction.JunctionId.ToString();
            txtStartId.Text = _streetRestriction.StartId.ToString();
            txtEndId1.Text = _streetRestriction.EndId1.ToString();
            txtEndId2.Text = _streetRestriction.EndId2.ToString();
            txtEndId3.Text = _streetRestriction.EndId3.ToString();
            txtEndId4.Text = _streetRestriction.EndId4.ToString();
            txtEndId5.Text = _streetRestriction.EndId5.ToString();
            txtEndId6.Text = _streetRestriction.EndId6.ToString();
            txtEndId7.Text = _streetRestriction.EndId7.ToString();
            txtEndId8.Text = _streetRestriction.EndId8.ToString();
            txtEndId9.Text = _streetRestriction.EndId9.ToString();
            txtEndId10.Text = _streetRestriction.EndId10.ToString();
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            return pass;
        }

        public void SetValues()
        {
            _streetRestriction.NavigationStatus = SelectedNavigationStatus;
            _streetRestriction.Source = SelectedSource;
        }
    }
}

﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms
{
	public partial class AddressFormatForm : CollapsibleForm
	{
		#region properties

		int FloorDigitNum;
		int UnitDigitNum;

		public int SelectedNumOfFloors
		{
			get { return numOfFloors.Value; }
		}
		public int SelectedNumOfApt
		{
			get { return numOfApts.Value; }
		}
		public int SelectedStartFloor
		{
			get { return numStartFloor.Value; }
		}
		public int SelectedStartApt
		{
			get { return numStartApt.Value; }
		}
		public string SelectedBlock
		{
			get { return txtBlock.Text; }
		}
		public string Prefix1
		{
			get { return txtPrefix1.Text; }
		}
		public string Prefix2
		{
			get { return txtPrefix2.Text; }
		}
		public string Prefix3
		{
			get { return txtPrefix3.Text; }
		}
		public string Prefix4
		{
			get { return txtPrefix4.Text; }
		}
		public string SelectedFormat1
		{
			get { return cbFormat1.Text; }
		}
		public string SelectedFormat2
		{
			get { return cbFormat2.Text; }
		}
		public string SelectedFormat3
		{
			get { return cbFormat3.Text; }
		}
		public string SelectedTextFloorNum
		{
			get { return txtFloorsNum.Text; }
		}
		public bool ChkAptChar1
		{
			get { return chkUnitChar1.Checked; }
		}
		public bool ChkAptChar2
		{
			get { return chkUnitChar2.Checked; }
		}
		public bool ChkAptChar3
		{
			get { return chkUnitChar3.Checked; }
		}
		public bool ChkFlrDigit
		{
			get { return chkFlrDigit.Checked; }
		}
		public bool ChkUnitDigit
		{
			get { return chkUnitDigit.Checked; }
		}
		public int FloorDigit
		{
			get { return FloorDigitNum; }
		}
		public int UnitDigit
		{
			get { return UnitDigitNum; }
		}
		#endregion


		public AddressFormatForm()
        {
            InitializeComponent();
		}

		protected override void Form_Load()
		{
			RefreshTitle("Address Format Form");
		}

		private void btnApply_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (ValidateValues())
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

		protected void RefreshFloorTitle()
		{
			groupBox1.Text = "Floor Info";
			lblNumOfApts.Text = "Num Of Unit :";
			lblAptStartNo.Text = "Unit (Start Num) :";
			txtBlock.Text = "";
			groupBox2.Text = "Format - Floor Address";
			label2.Text = "Unit Address";
			cbFormat1.Text = "FLR";
			cbFormat2.Text = "UNIT";
			txtPrefix3.Text = "";
			cbFormat3.Text = "";
		}

		private void txtPrefix1_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbFormat1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtPrefix2_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void cbFormat2_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtPrefix3_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void cbFormat3_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void txtPrefix4_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void txtBlock_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numStartApt_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numStartFloor_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numOfFloors_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }
        private void numOfApts_ValueChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

		protected override bool ValidateValues()
        {
            bool pass = true;

            bool isFloorValid = true;
            isFloorValid &= SelectedNumOfFloors.ToString() != "";
            pass &= isFloorValid;

            bool isAptValid = true;
            isAptValid &= SelectedNumOfApt.ToString() != "";
            pass &= isAptValid;

            lblNumOfFloors.ForeColor = isFloorValid ? Color.Black : Color.Red;
            lblNumOfApts.ForeColor = isAptValid ? Color.Black : Color.Red;

            // to check if alphabet value is enough to generete for every unit
            bool isAptValidApha1 = true;
            string strPrefix = AddressFormat.unitAlpha(Prefix1);

            if (ChkAptChar1 && strPrefix.Length == 1)
            {
                char letter = strPrefix[0];
                int totalLetter = ((int)letter) + (SelectedNumOfApt - 1);
                isAptValidApha1 &= SelectedNumOfApt < 91;
                pass &= isAptValidApha1;

                lblNumOfApts.ForeColor = isAptValidApha1 ? Color.Black : Color.Red;
                lblPrefix1.ForeColor = isAptValidApha1 ? Color.Black : Color.Red;
            }

            bool isAptValidApha2 = true;
            strPrefix = AddressFormat.unitAlpha(Prefix2);
            if (ChkAptChar2 && strPrefix.Length == 1)
            {
                char letter = strPrefix[0];
                int totalLetter = ((int)letter) + (SelectedNumOfApt - 1);
                isAptValidApha2 &= totalLetter < 91;
                pass &= isAptValidApha2;

                lblNumOfApts.ForeColor = isAptValidApha2 ? Color.Black : Color.Red;
                lblPrefix2.ForeColor = isAptValidApha2 ? Color.Black : Color.Red;
            }

            bool isAptValidApha3 = true;
            strPrefix = AddressFormat.unitAlpha(Prefix3);
            if (ChkAptChar3 && strPrefix.Length == 1)
            {
                char letter = strPrefix[0];
                int totalLetter = ((int)letter) + (SelectedNumOfApt-1);
                isAptValidApha3 &= totalLetter < 91;
                pass &= isAptValidApha3;

                lblNumOfApts.ForeColor = isAptValidApha3 ? Color.Black : Color.Red;
                lblPrefix3.ForeColor = isAptValidApha3 ? Color.Black : Color.Red;
            }

            return pass;
        }

        private void PopulatePreview()
        {
            RefreshEnableChkCharBox();
            InitialPrefix();

            string preview = "";

            string strFormat1 = "";
            string strFormat2 = "";
            string strFormat3 = "";
            string strFormat4 = "";

            // display floor preview
            previewfloor();

			// generate digit format
			int endFloorLoop = SelectedStartFloor + SelectedNumOfFloors;
			int endUnitLoop = SelectedStartApt + SelectedNumOfApt;

		    FloorDigitNum = AddressFormat.DigitLen(chkFlrDigit.Checked, endFloorLoop);
		    UnitDigitNum = AddressFormat.DigitLen(chkUnitDigit.Checked, endUnitLoop);

            // Display Address preview
            // String Format 1
            strFormat1 = AddressFormat.GenerateAddressFormat(SelectedFormat1, Prefix1, SelectedBlock, SelectedStartFloor, SelectedStartApt,
                                        SelectedStartApt, 0, ChkAptChar1, FloorDigitNum, UnitDigitNum);
            preview = string.Format("{0}", strFormat1);

            // string format 2
            strFormat2 = AddressFormat.GenerateAddressFormat(SelectedFormat2, Prefix2, SelectedBlock, SelectedStartFloor, SelectedStartApt,
                                        SelectedStartApt, 0, ChkAptChar2, FloorDigitNum, UnitDigitNum);
            preview = string.Format("{0}{1}", strFormat1, strFormat2);

            // string format 3
            strFormat3 = AddressFormat.GenerateAddressFormat(SelectedFormat3, Prefix3, SelectedBlock, SelectedStartFloor, SelectedStartApt,
                                    SelectedStartApt, 0, ChkAptChar3, FloorDigitNum, UnitDigitNum);
            preview = string.Format("{0}{1}{2}", strFormat1, strFormat2, strFormat3);

            strFormat4 = string.Format("{0}{1}", strFormat4, Prefix4);
            preview = string.Format("{0}{1}{2}{3}", strFormat1, strFormat2, strFormat3, strFormat4);

            lblPreviewText.Text = preview;
        }

        private void previewfloor()
        {
            lblFlrNoPreview.Text = numStartFloor.Value.ToString();
            if (!string.IsNullOrEmpty(txtFloorsNum.Text))
            {
                lblFlrNoPreview.Text = txtFloorsNum.Text;
            } 
        }

        private void InitialPrefix()
        {
            if (string.IsNullOrEmpty(cbFormat1.Text))
            {
                txtPrefix1.Text = "";
            }
            if (string.IsNullOrEmpty(cbFormat2.Text))
            {
                txtPrefix2.Text = "";
            }
            if (string.IsNullOrEmpty(cbFormat3.Text))
            {
                txtPrefix3.Text = "";
            }
        }

        private void chkUnitChar1_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitChar2_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitChar3_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkFlrDigit_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void chkUnitDigit_CheckedChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void RefreshEnableChkCharBox()
        {
            chkUnitChar1.Enabled = false;
            chkUnitChar2.Enabled = false;
            chkUnitChar3.Enabled = false;

            if (cbFormat1.Text == "UNIT")
                chkUnitChar1.Enabled = true;

            if (cbFormat2.Text == "UNIT")
                chkUnitChar2.Enabled = true;

            if (cbFormat3.Text == "UNIT")
                chkUnitChar3.Enabled = true;
        }

        private void txtFloorsNum_TextChanged(object sender, EventArgs e)
        {
            // display floor preview
            previewfloor();
        }
    }
}

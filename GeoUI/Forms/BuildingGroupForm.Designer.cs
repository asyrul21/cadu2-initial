﻿namespace Geomatic.UI.Forms
{
    partial class BuildingGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.relationLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lblStreetId = new System.Windows.Forms.Label();
            this.txtStreetId = new System.Windows.Forms.TextBox();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pageBuilding = new System.Windows.Forms.TabPage();
            this.controlBuilding = new Geomatic.UI.Controls.ReferenceControl();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblSection = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnSetCode = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.cbMirror = new System.Windows.Forms.CheckBox();
            this.lblMirror = new System.Windows.Forms.Label();
            this.lblForecastSource = new System.Windows.Forms.Label();
            this.cbForecastSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblForecastUnit = new System.Windows.Forms.Label();
            this.totalForecastUnit = new Geomatic.UI.Controls.IntUpDown();
            this.lblTotalNumUnit = new System.Windows.Forms.Label();
            this.txtTotalUnit = new System.Windows.Forms.TextBox();
            this.categoryControl = new Geomatic.UI.Controls.CodeControl();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.relationLayout.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pageBuilding.SuspendLayout();
            this.addressGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalForecastUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 670);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel2);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 600);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 12;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(554, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.relationLayout);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 556);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(560, 44);
            this.relationGroup.TabIndex = 11;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // relationLayout
            // 
            this.relationLayout.ColumnCount = 4;
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.relationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.relationLayout.Controls.Add(this.lblStreetId, 0, 0);
            this.relationLayout.Controls.Add(this.txtStreetId, 1, 0);
            this.relationLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.relationLayout.Location = new System.Drawing.Point(3, 16);
            this.relationLayout.Name = "relationLayout";
            this.relationLayout.RowCount = 1;
            this.relationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.relationLayout.Size = new System.Drawing.Size(554, 25);
            this.relationLayout.TabIndex = 0;
            // 
            // lblStreetId
            // 
            this.lblStreetId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetId.AutoSize = true;
            this.lblStreetId.Location = new System.Drawing.Point(57, 6);
            this.lblStreetId.Name = "lblStreetId";
            this.lblStreetId.Size = new System.Drawing.Size(50, 13);
            this.lblStreetId.TabIndex = 0;
            this.lblStreetId.Text = "Street Id:";
            // 
            // txtStreetId
            // 
            this.txtStreetId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetId.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetId.Location = new System.Drawing.Point(113, 3);
            this.txtStreetId.Name = "txtStreetId";
            this.txtStreetId.ReadOnly = true;
            this.txtStreetId.Size = new System.Drawing.Size(161, 20);
            this.txtStreetId.TabIndex = 1;
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabAdditional);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 359);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(560, 197);
            this.additionalGroup.TabIndex = 10;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Controls.Add(this.pageBuilding);
            this.tabAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAdditional.Location = new System.Drawing.Point(3, 16);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(554, 178);
            this.tabAdditional.TabIndex = 0;
            // 
            // pageBuilding
            // 
            this.pageBuilding.Controls.Add(this.controlBuilding);
            this.pageBuilding.Location = new System.Drawing.Point(4, 22);
            this.pageBuilding.Name = "pageBuilding";
            this.pageBuilding.Padding = new System.Windows.Forms.Padding(3);
            this.pageBuilding.Size = new System.Drawing.Size(546, 152);
            this.pageBuilding.TabIndex = 0;
            this.pageBuilding.Text = "Building";
            this.pageBuilding.UseVisualStyleBackColor = true;
            // 
            // controlBuilding
            // 
            this.controlBuilding.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBuilding.Location = new System.Drawing.Point(3, 3);
            this.controlBuilding.MultiSelect = true;
            this.controlBuilding.Name = "controlBuilding";
            this.controlBuilding.Size = new System.Drawing.Size(540, 146);
            this.controlBuilding.TabIndex = 0;
            this.controlBuilding.AssociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlBuilding_AssociateClick1);
            this.controlBuilding.DissociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlBuilding_DissociateClick1);
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.tableLayoutPanel3);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = false;
            this.addressGroup.Location = new System.Drawing.Point(12, 239);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(560, 120);
            this.addressGroup.TabIndex = 9;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName2, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtSection, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblSection, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblCity, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtCity, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblPostcode, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblState, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtPostcode, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtState, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetType, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetType, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(554, 101);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(309, 31);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 13);
            this.lblStreetName2.TabIndex = 4;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(390, 28);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName2.TabIndex = 5;
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 53);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(161, 20);
            this.txtSection.TabIndex = 7;
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 56);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 6;
            this.lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 81);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 10;
            this.lblCity.Text = "City:";
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 78);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(161, 20);
            this.txtCity.TabIndex = 11;
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(329, 56);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 8;
            this.lblPostcode.Text = "Postcode:";
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(349, 81);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 12;
            this.lblState.Text = "State:";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(390, 53);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(161, 20);
            this.txtPostcode.TabIndex = 9;
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(390, 78);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(161, 20);
            this.txtState.TabIndex = 13;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(42, 6);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 13);
            this.lblStreetType.TabIndex = 0;
            this.lblStreetType.Text = "Street Type:";
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 3);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(161, 20);
            this.txtStreetType.TabIndex = 1;
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(315, 6);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 2;
            this.lblStreetName.Text = "Street Name:";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(390, 3);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName.TabIndex = 3;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.txtName, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblCode, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.btnSetCode, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblName, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lblNaviStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.cbMirror, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lblMirror, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblForecastSource, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.cbForecastSource, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblForecastUnit, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.totalForecastUnit, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblTotalNumUnit, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.txtTotalUnit, 1, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 97);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 142);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(398, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(159, 21);
            this.cbNaviStatus.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(124, 60);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(158, 20);
            this.txtName.TabIndex = 6;
            // 
            // lblCode
            // 
            this.lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(83, 35);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(35, 13);
            this.lblCode.TabIndex = 2;
            this.lblCode.Text = "Code:";
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.Location = new System.Drawing.Point(124, 32);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(158, 20);
            this.txtCode.TabIndex = 3;
            // 
            // btnSetCode
            // 
            this.btnSetCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSetCode.Location = new System.Drawing.Point(288, 32);
            this.btnSetCode.Name = "btnSetCode";
            this.btnSetCode.Size = new System.Drawing.Size(32, 20);
            this.btnSetCode.TabIndex = 4;
            this.btnSetCode.Text = "...";
            this.btnSetCode.UseVisualStyleBackColor = true;
            this.btnSetCode.Click += new System.EventHandler(this.btnSetCode_Click);
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(80, 63);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name:";
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(298, 7);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 0;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(398, 59);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(159, 21);
            this.cbSource.TabIndex = 8;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(348, 63);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 7;
            this.lblSource.Text = "Source:";
            // 
            // cbMirror
            // 
            this.cbMirror.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cbMirror.AutoSize = true;
            this.cbMirror.Location = new System.Drawing.Point(63, 5);
            this.cbMirror.Name = "cbMirror";
            this.cbMirror.Size = new System.Drawing.Size(55, 17);
            this.cbMirror.TabIndex = 9;
            this.cbMirror.Text = "Mirror:";
            this.cbMirror.UseVisualStyleBackColor = true;
            this.cbMirror.CheckedChanged += new System.EventHandler(this.cbMirror_CheckedChanged);
            // 
            // lblMirror
            // 
            this.lblMirror.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMirror.AutoSize = true;
            this.lblMirror.Location = new System.Drawing.Point(124, 0);
            this.lblMirror.Name = "lblMirror";
            this.lblMirror.Size = new System.Drawing.Size(158, 28);
            this.lblMirror.TabIndex = 10;
            this.lblMirror.Text = "SUNWAY SUTERA CONDOMINIUM BLOCK A ";
            this.lblMirror.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblForecastSource
            // 
            this.lblForecastSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblForecastSource.AutoSize = true;
            this.lblForecastSource.Location = new System.Drawing.Point(301, 91);
            this.lblForecastSource.Name = "lblForecastSource";
            this.lblForecastSource.Size = new System.Drawing.Size(91, 13);
            this.lblForecastSource.TabIndex = 12;
            this.lblForecastSource.Text = "Forecast Source :";
            // 
            // cbForecastSource
            // 
            this.cbForecastSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbForecastSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbForecastSource.FormattingEnabled = true;
            this.cbForecastSource.Location = new System.Drawing.Point(398, 87);
            this.cbForecastSource.Name = "cbForecastSource";
            this.cbForecastSource.Size = new System.Drawing.Size(159, 21);
            this.cbForecastSource.TabIndex = 17;
            // 
            // lblForecastUnit
            // 
            this.lblForecastUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblForecastUnit.AutoSize = true;
            this.lblForecastUnit.Location = new System.Drawing.Point(17, 91);
            this.lblForecastUnit.Name = "lblForecastUnit";
            this.lblForecastUnit.Size = new System.Drawing.Size(101, 13);
            this.lblForecastUnit.TabIndex = 13;
            this.lblForecastUnit.Text = "Forecast Num Unit :";
            // 
            // totalForecastUnit
            // 
            this.totalForecastUnit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.totalForecastUnit.Location = new System.Drawing.Point(124, 88);
            this.totalForecastUnit.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.totalForecastUnit.Name = "totalForecastUnit";
            this.totalForecastUnit.Size = new System.Drawing.Size(120, 20);
            this.totalForecastUnit.TabIndex = 18;
            this.totalForecastUnit.Value = 0;
            // 
            // lblTotalNumUnit
            // 
            this.lblTotalNumUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTotalNumUnit.AutoSize = true;
            this.lblTotalNumUnit.Location = new System.Drawing.Point(8, 120);
            this.lblTotalNumUnit.Name = "lblTotalNumUnit";
            this.lblTotalNumUnit.Size = new System.Drawing.Size(110, 13);
            this.lblTotalNumUnit.TabIndex = 14;
            this.lblTotalNumUnit.Text = "Total Floor Num Unit :";
            // 
            // txtTotalUnit
            // 
            this.txtTotalUnit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTotalUnit.BackColor = System.Drawing.SystemColors.Control;
            this.txtTotalUnit.Location = new System.Drawing.Point(124, 117);
            this.txtTotalUnit.Name = "txtTotalUnit";
            this.txtTotalUnit.ReadOnly = true;
            this.txtTotalUnit.Size = new System.Drawing.Size(100, 20);
            this.txtTotalUnit.TabIndex = 16;
            // 
            // categoryControl
            // 
            this.categoryControl.Code1Text = "";
            this.categoryControl.Code2Text = "";
            this.categoryControl.Code3Text = "";
            this.categoryControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.categoryControl.Location = new System.Drawing.Point(12, 51);
            this.categoryControl.Name = "categoryControl";
            this.categoryControl.Size = new System.Drawing.Size(560, 46);
            this.categoryControl.TabIndex = 7;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // BuildingGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 653);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.categoryControl);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "BuildingGroupForm";
            this.Text = "BuildingGroupForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.categoryControl, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.relationLayout.ResumeLayout(false);
            this.relationLayout.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pageBuilding.ResumeLayout(false);
            this.addressGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.totalForecastUnit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel relationLayout;
        protected System.Windows.Forms.Label lblStreetId;
        protected System.Windows.Forms.TextBox txtStreetId;
        protected Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pageBuilding;
        protected Controls.ReferenceControl controlBuilding;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected Controls.UpperComboBox cbNaviStatus;
        protected System.Windows.Forms.TextBox txtName;
        protected System.Windows.Forms.Label lblCode;
        protected System.Windows.Forms.TextBox txtCode;
        protected System.Windows.Forms.Button btnSetCode;
        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.Label lblSource;
        protected Controls.CodeControl categoryControl;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        private System.Windows.Forms.CheckBox cbMirror;
        private System.Windows.Forms.Label lblMirror;
        private System.Windows.Forms.Label lblForecastSource;
        private System.Windows.Forms.Label lblForecastUnit;
        private System.Windows.Forms.Label lblTotalNumUnit;
        private System.Windows.Forms.TextBox txtTotalUnit;
        protected Controls.UpperComboBox cbForecastSource;
        private Controls.IntUpDown totalForecastUnit;
    }
}
﻿
using Geomatic.Core;
using Geomatic.Core.Logs;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Search;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using Geomatic.Core.Rows;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Oracle;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;

namespace Geomatic.UI.Forms
{
    public partial class VerifyCadu3WebForm : SearchForm
    {
        int? _CMSNo = 0;
        List<int> remarkStatusSuccess = new List<int>();

        private int StatusValue
        {
            get
            {
                if (cbStatus.Text == "ADD")
                    return 1;
                else if (cbStatus.Text == "EDIT")
                    return 3;
                else if (cbStatus.Text == "DELETE")
                    return 2;
                else
                    return 0;
            }
        }

        public VerifyCadu3WebForm(SegmentName SegmentName)
           : base(SegmentName)
        {
            InitializeComponent();
            RefreshTitle("Supervisor Verification Web Feature Form");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            // header result column
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("POI Code");
            lvResult.Columns.Add("Place Name");
            lvResult.Columns.Add("Status");
            lvResult.Columns.Add("Modified Date");
            lvResult.Columns.Add("Modified User");
            lvResult.Columns.Add("Remark");
            lvResult.Columns.Add("CMS No");

            // populate combo box list
            ComboBoxUtils.PopulateWebUserName(cbUserName, SegmentName);
            ComboBoxUtils.PopulateWebStatus(cbStatus);
        }

        private void PopulatePreview()
        {
            btnSearch.Enabled = true;
            lvResult.Items.Clear();
        }

        private void cbUserName_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();
        }

        private void cbStatus_TextChanged(object sender, EventArgs e)
        {
            PopulatePreview();  
        }

        private void VerifyWebFeatureForm_Shown(object sender, EventArgs e)
        {}

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }
        #region display single attribute
        private void lvResult_DoubleClick(object sender, EventArgs e)
        {
            DisplayAttribute();
        }

        private void DisplayAttribute()
        {
            int selectedIndex = lvResult.FocusedItem.Index;
            ListViewItem selecteditem = lvResult.Items[selectedIndex];
            string FeatureID = selecteditem.Text;
            string StatusVerify= selecteditem.SubItems[6].Text;

            using (VerificationWebForm form = new VerificationWebForm(FeatureID, StatusVerify, SegmentName))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    if (lvResult.Items.Count > 0)
                    {
                        lvResult.Items[selectedIndex].Selected = true;
                        ListViewItem item = lvResult.SelectedItems[0];
                        UpdateSelectedItem(item, form.remark);
                    }
                }
            }
        }
        #endregion

        #region populate WEB POI
        protected ListViewItem CreateItem(GPoiWeb poi, string log)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add((poi.Code == null) ? string.Empty : poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.StatusType);
            item.SubItems.Add(poi.DateUpdated);
            item.SubItems.Add(poi.UpdatedBy);
            item.SubItems.Add(log);
            item.SubItems.Add((string.IsNullOrEmpty(poi.CMSNo.ToString())) ? string.Empty : poi.CMSNo.ToString());
            item.Tag = poi;
            return item;
        }

        protected override void DoSearch()
        {
            populateList();
        }

        private void populateList()
        {
            string _userName = StringUtils.CheckNull(cbUserName.Text);
            string _statusType = StringUtils.CheckNull(cbStatus.Text);

            lvResult.BeginUpdate();
            lvResult.Sorting = SortOrder.None;
            lvResult.Items.Clear();

            try
            {
                List<ListViewItem> items = new List<ListViewItem>();

                foreach (GPoiWeb Webpoi in GPoiWeb.GetAll(false, SegmentName))
                {
                    if (Webpoi.OID > 0)
                    {
                        if (!string.IsNullOrEmpty(_userName) && (string.IsNullOrEmpty(_statusType)))
                        {
                            if (Webpoi.UpdatedBy == _userName)
                            {
                                ListViewItem item = CreateItem(Webpoi, GetRemarkStatus(Webpoi.OID));
                                items.Add(item);

                            }
                        }
                        else if (string.IsNullOrEmpty(_userName) && (!string.IsNullOrEmpty(_statusType)))
                        {
                            if (Webpoi.Status == StatusValue)
                            {
                                ListViewItem item = CreateItem(Webpoi, GetRemarkStatus(Webpoi.OID));
                                items.Add(item);
                            }
                        }
                        else if (!string.IsNullOrEmpty(_userName) && (!string.IsNullOrEmpty(_statusType)))
                        {
                            if (Webpoi.Status == StatusValue && Webpoi.UpdatedBy == _userName)
                            {
                                ListViewItem item = CreateItem(Webpoi, GetRemarkStatus(Webpoi.OID));
                                items.Add(item);
                            }
                        }
                        else
                        {
                            ListViewItem item = CreateItem(Webpoi, GetRemarkStatus(Webpoi.OID));
                            items.Add(item);
                        }
                    }
                }

                lvResult.Items.AddRange(items.ToArray());
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }
        #endregion

        #region Verify all result list
        private void btnVerifyAll_Click(object sender, EventArgs e)
        {
            // ask user to confirm proceeed all this data
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Confirmation to Verify ALL.")
                      .SetText("Are you sure want to verify ALL this result list ? \n OR you can click NO and Double-click result to verify single data");

            if (confirmBox.Show() != DialogResult.Yes)
            {
                return;
            }

            int featureCount = lvResult.Items.Count;

            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
            {
                form.setMinimun(0);
                form.setMaximum(featureCount);
                form.Show();

                string remark;
                int progressBar = 0;

                foreach (ListViewItem item in lvResult.Items)
                {
                    GPoiWeb poi = (GPoiWeb)item.Tag;
                    remark = item.SubItems[6].Text;
                    bool remarkstatus = remark.Contains("To Verify");
                    if (remarkstatus)
                    {
                        remark = Verify(poi.OID);
                        ListViewItem subitem = lvResult.Items[item.Index];
                        lvResult.Items[item.Index].Selected = true;
                        UpdateSelectedItem(subitem, remark);
                    }
                    progressBar++;
                    form.setValue(progressBar);
                }
            }
        }

        private void UpdateSelectedItem(ListViewItem item, string log)
        {
            GPoiWeb poi = (GPoiWeb)item.Tag;
            item.SubItems.Clear();
            item.Text = poi.OID.ToString();
            item.SubItems.Add((poi.Code == null) ? string.Empty : poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.StatusType);
            item.SubItems.Add(poi.DateUpdated);
            item.SubItems.Add(poi.UpdatedBy);
            item.SubItems.Add(log);
            item.SubItems.Add((string.IsNullOrEmpty(poi.CMSNo.ToString())) ? string.Empty : poi.CMSNo.ToString());
            if (log.Contains("Success"))
                remarkStatusSuccess.Add(poi.OID);
        }

        public string Verify(int? featureId)
        {
            string remark = "";
            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    GPoiWeb poiWeb = repo.GetById<GPoiWeb>(featureId.Value);
                    GPoi poi = repo.GetById<GPoi>(poiWeb.OriId.Value);

                    repo.StartTransaction();

                    switch (poiWeb.StatusType)
                    {
                        case "ADD":
                            // check data validation
                            if (ValidateWebData(poiWeb))
                            {
                                // to create adm poi, use building id point
                                GPoi newPoi = repo.NewObj<GPoi>();
                                newPoi.Init();
                                newPoi.CopyFromWEB(poiWeb);
                                newPoi.CopyFromWEB(poiWeb, true);
                                newPoi = repo.Insert(newPoi, false);

                                remark = "Success Verify";
                                success = true;
                            }
                            else
                            {
                                success = false;
                                remark = "Fail Verify";
                            }
                            break;

                        case "EDIT":
                            // check data validation
                            if (ValidateWebData(poiWeb))
                            {
                                // insert data in LOG Table
                                CreateLogADMPoi(repo, poi);

                                // copy web data to adm data
                                poi.CopyFromWEB(poiWeb);
                                repo.Update(poi, false);

                                remark = "Success Verify";
                                success = true;
                            }
                            else
                            {
                                remark = "Fail Verify";
                                success = false;
                            }
                            break;

                        case "DELETE":
                            // insert data in LOG Table
                            CreateLogADMPoi(repo, poi);
                            poi.CopyFromWEB(poiWeb, false, true);
                            remark = "Success Verify";
                            success = true;
                            break;

                        default:
                            throw new Exception(string.Format("Unknown status. {0}", poiWeb.StatusType));
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
            return remark;
        }

        public void CreateLogADMPoi(RepositoryFactory repo, GPoi poi)
        {
            GPoiLog poiLog = repo.NewObj<GPoiLog>();
            poiLog.CopyFrom(poi);
            repo.Insert(poiLog, true);
        }

        public virtual bool ValidateWebData(GPoiWeb poi)
        {
            bool pass = true;

            bool isNameValid = true;
            isNameValid &= !string.IsNullOrEmpty(poi.Name);
            pass &= isNameValid;

            bool isCodeValid = true;
            isCodeValid &= !string.IsNullOrEmpty(poi.Code);
            pass &= isCodeValid;

            bool isParentIdValid = true;
            isParentIdValid &= (!string.IsNullOrEmpty(poi.ParentId.ToString()) && poi.ParentId > 0);
            pass &= isParentIdValid;

            bool isCMSNoValid = true;
            isCMSNoValid &= (!string.IsNullOrEmpty(poi.CMSNo.ToString()) && poi.CMSNo > 0);
            pass &= isCMSNoValid;

            return pass;
        }
        #endregion

        #region process to clear WEB data
        private void btnUpdWEB_Click(object sender, EventArgs e)
        {
            if (remarkStatusSuccess.Count == 0)
                return;

            bool success = false;
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();

                    foreach (ListViewItem item in lvResult.Items)
                    {
                        ListViewItem subitem = lvResult.Items[item.Index];
                        lvResult.Items[item.Index].Selected = true;

                        GPoiWeb poiWeb = (GPoiWeb)item.Tag;
                        string operation = item.SubItems[3].Text;
                        string remark = item.SubItems[6].Text;
                        bool remarkSuccsesVerify = remark.Contains("Success Verify");

                        if (remarkSuccsesVerify)
                        {
                            this._CMSNo = poiWeb.CMSNo;

                            switch (operation)
                            {
                                case "ADD":
                                    repo.Delete(poiWeb);
                                    success = true;
                                    break;

                                case "EDIT":
                                    repo.Delete(poiWeb);
                                    success = true;
                                    break;

                                case "DELETE":
                                    GPoi poi = repo.GetById<GPoi>(poiWeb.OriId.Value);
                                    repo.Delete(poiWeb);
                                    repo.Delete(poi);

                                    success = true;
                                    break;
                            }

                            // UpdateTMOM status
                            UpdateTMOMStatus(this._CMSNo, 2); // TMOM status type 'Completed"
                            this._CMSNo = 0;
                        }
                        else 
                        {
                            bool remarkSuccsesRevert = remark.Contains("Success Revert");
                            if (remarkSuccsesRevert)
                            {
                                repo.Delete(poiWeb);
                                success = true;

                                // UpdateTMOM status
                                UpdateTMOMStatus(this._CMSNo, 3); // TMOM status type 'Rejected"
                                this._CMSNo = 0;
                            }
                        }
                        // remove OID from list - process done delete Web data
                        remarkStatusSuccess.Remove(poiWeb.OID);
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box1 = MessageBoxFactory.Create(ex))
                {
                    box1.Show();
                }
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }

            // Save the current state
            using (new WaitCursor())
            {
                Session.Current.Cadu.Save(true);
            }
            populateList();
        }

        // update TMOM Status using connect oracle database
        public void UpdateTMOMStatus(int? CMSNo, int tmomStatusType)
        {
            TMOMUpdStatus tmomstatus = new TMOMUpdStatus(SegmentName);
            tmomstatus.UpdateStatus(CMSNo, tmomStatusType);
            //MessageBox.Show("Success update TMOM status CMS no : " + CMSNo.ToString() + "-" + (tmomStatusType == 2 ? "Completed" : "Rejected"));
        }
        #endregion

        private string GetRemarkStatus(int OID)
        {
            if (remarkStatusSuccess.Contains(OID))
                return "Success Verify";
            else
                return "To Verify";
        }
    }
}

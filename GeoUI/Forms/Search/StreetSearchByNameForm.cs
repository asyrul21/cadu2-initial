﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class StreetSearchByNameForm : SearchByNameForm
    {
        public StreetSearchByNameForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Street by Names");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("Sub City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Network Class");
            lvResult.Columns.Add("Class");
            lvResult.Columns.Add("Category");
            lvResult.Columns.Add("Filter Level");
            lvResult.Columns.Add("Speed Limit");
            lvResult.Columns.Add("Direction");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
           
        }

        private void StreetSearchForm_Shown(object sender, EventArgs e)
        {     
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }
 
        protected override void DoSearch()
        {
            //MessageBox.Show("Name field: " + cbName.Text);
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (!chkSearchAND.Checked)
            {
                Query<GStreet> query = new Query<GStreet>(SegmentName);

                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[" + name + @"]'");
                }

                //query.Obj.Name = name;
                query.Obj.Name2 = name2;

                foreach (GStreet street in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }

                lvResult.Items.AddRange(items.ToArray());
            }
            else
            {
                Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);

                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    //query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[" + name + @"]'");
                }

                //query.Obj.Name = name;
                query.Obj.Name2 = name2;

                foreach (GStreetAND streetAND in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(streetAND);
                    items.Add(item);
                }

                lvResult.Items.AddRange(items.ToArray());
            }
            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(street.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.SubCity);
            item.SubItems.Add(street.State);
            item.SubItems.Add(StringUtils.TrimSpaces(street.StatusValue));
            item.SubItems.Add(street.NavigationStatusValue);
            item.SubItems.Add(StringUtils.TrimSpaces(street.NetworkClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.ClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.CategoryValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.FilterLevelValue));
            item.SubItems.Add((street.SpeedLimit.HasValue) ? street.SpeedLimit.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(street.DirectionValue));
            item.SubItems.Add(street.CreatedBy);
            item.SubItems.Add(street.DateCreated);
            item.SubItems.Add(street.UpdatedBy);
            item.SubItems.Add(street.DateUpdated);
            item.Tag = street;
            return item;
        }

        private void btnSearchDropDown_Click(object sender, EventArgs e)
        {
            string inputname = StringUtils.CheckNull(txtInputName.Text);

            if (!string.IsNullOrEmpty(inputname))
            {
                List<ListViewItem> items = new List<ListViewItem>();
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                Query<GStreet> queryName = new Query<GStreet>(SegmentName);
                queryName.AddClause(() => queryName.Obj.Name, @"LIKE q'[%" + inputname + @"%]'");
                ComboBoxUtils.PopulateInputStreetName(cbName, inputname, repo, queryName, Limit);

                Query<GStreet> querName2 = new Query<GStreet>(SegmentName);
                querName2.AddClause(() => querName2.Obj.Name2, @"LIKE q'[%" + inputname + @"%]'");
                ComboBoxUtils.PopulateInputStreetName2(cbName2, inputname, repo, querName2, Limit);
            }
            else
            {
                cbName.Text = "";
                cbName2.Text = "";
                cbName.Items.Clear();
                cbName2.Items.Clear();
                using (ErrorMessageBox box = new ErrorMessageBox())
                {
                    box.SetText("Please input street name, input name cannot be empty.").Show();
                } 
            }
        }
    }
}

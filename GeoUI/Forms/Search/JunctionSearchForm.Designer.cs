﻿namespace Geomatic.UI.Forms.Search
{
    partial class JunctionSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblId;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            lblType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblId = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 74);
            this.lvResult.Size = new System.Drawing.Size(610, 347);
            // 
            // lblType
            // 
            lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblType.AutoSize = true;
            lblType.Location = new System.Drawing.Point(83, 35);
            lblType.Name = "lblType";
            lblType.Size = new System.Drawing.Size(34, 13);
            lblType.TabIndex = 2;
            lblType.Text = "Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(384, 35);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(38, 13);
            lblName.TabIndex = 4;
            lblName.Text = "Name:";
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbName, 3, 1);
            this.tableLayoutPanel1.Controls.Add(lblName, 2, 1);
            this.tableLayoutPanel1.Controls.Add(lblType, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbType, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(610, 56);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(428, 31);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(179, 21);
            this.cbName.TabIndex = 5;
            this.cbName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(123, 31);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(179, 21);
            this.cbType.TabIndex = 3;
            this.cbType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // JunctionSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "JunctionSearchForm";
            this.Text = "JunctionSearchForm";
            this.Shown += new System.EventHandler(this.JunctionSearchForm_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Geomatic.UI.Controls.UpperComboBox cbType;
        private Geomatic.UI.Controls.UpperComboBox cbName;
        private System.Windows.Forms.TextBox txtId;

    }
}
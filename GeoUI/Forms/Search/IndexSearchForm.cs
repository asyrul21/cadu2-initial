﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms.Search
{
    public partial class IndexSearchForm : SearchFeatureForm
    {
        public IndexSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Index");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Index");
        }

        private void IndexSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> id = StringUtils.CommaIntSplit(txtId.Text);
            string index = StringUtils.CheckNull(txtIndex.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (id.Count() > 0)
            {
                //MessageBox.Show("Search by ID!");
                foreach (GRegion region in repo.GetByIds<GRegion>(id, true))
                {
                    ListViewItem item = CreateItem(region);
                    items.Add(item);
                }
            }
            else
            {
                //MessageBox.Show("Search by Index!");
                Query<GRegion> query = new Query<GRegion>(SegmentName);
                query.Obj.index = index;
                query.MatchType = MatchType.Contains;

                foreach (GRegion region in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(region);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GRegion region)
        {
            ListViewItem item = new ListViewItem();
            item.Text = region.OID.ToString();
            item.SubItems.Add(region.index);
            item.Tag = region;
            return item;
        }
    }
}

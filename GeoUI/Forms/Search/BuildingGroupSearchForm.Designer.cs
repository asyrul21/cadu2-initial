﻿namespace Geomatic.UI.Forms.Search
{
    partial class BuildingGroupSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblCode;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblStreetType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblNaviStatus;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbStreetType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.btnSelectCode = new System.Windows.Forms.Button();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            lblCode = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lblId = new System.Windows.Forms.Label();
            lblStreetType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblNaviStatus = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 249);
            this.lvResult.Size = new System.Drawing.Size(610, 172);
            // 
            // lblCode
            // 
            lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCode.AutoSize = true;
            lblCode.Location = new System.Drawing.Point(82, 35);
            lblCode.Name = "lblCode";
            lblCode.Size = new System.Drawing.Size(35, 13);
            lblCode.TabIndex = 4;
            lblCode.Text = "Code:";
            // 
            // label2
            // 
            label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(378, 91);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(44, 13);
            label2.TabIndex = 10;
            label2.Text = "Name2:";
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(79, 91);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(38, 13);
            label1.TabIndex = 8;
            label1.Text = "Name:";
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblStreetType
            // 
            lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetType.AutoSize = true;
            lblStreetType.Location = new System.Drawing.Point(52, 119);
            lblStreetType.Name = "lblStreetType";
            lblStreetType.Size = new System.Drawing.Size(65, 13);
            lblStreetType.TabIndex = 18;
            lblStreetType.Text = "Street Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(48, 147);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(69, 13);
            lblName.TabIndex = 20;
            lblName.Text = "Street Name:";
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(347, 147);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(75, 13);
            lblName2.TabIndex = 22;
            lblName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 175);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 24;
            lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 207);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 28;
            lblCity.Text = "City:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 175);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 26;
            lblPostcode.Text = "Postcode:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 207);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 30;
            lblState.Text = "State:";
            // 
            // lblNaviStatus
            // 
            lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNaviStatus.AutoSize = true;
            lblNaviStatus.Location = new System.Drawing.Point(328, 63);
            lblNaviStatus.Name = "lblNaviStatus";
            lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            lblNaviStatus.TabIndex = 6;
            lblNaviStatus.Text = "Navigation Status:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.cbNaviStatus, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel1.Controls.Add(lblCode, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(lblStreetType, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetType, 1, 4);
            this.tableLayoutPanel1.Controls.Add(lblName, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetName, 1, 5);
            this.tableLayoutPanel1.Controls.Add(lblName2, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetName2, 3, 5);
            this.tableLayoutPanel1.Controls.Add(lblSection, 0, 6);
            this.tableLayoutPanel1.Controls.Add(lblCity, 0, 7);
            this.tableLayoutPanel1.Controls.Add(lblPostcode, 2, 6);
            this.tableLayoutPanel1.Controls.Add(lblState, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnSelectCode, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbName2, 3, 3);
            this.tableLayoutPanel1.Controls.Add(label2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbName, 1, 3);
            this.tableLayoutPanel1.Controls.Add(label1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(lblNaviStatus, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbSection, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbPostcode, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbCity, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.cbState, 3, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(610, 231);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbNaviStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(428, 59);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(179, 21);
            this.cbNaviStatus.TabIndex = 7;
            this.cbNaviStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCode.Location = new System.Drawing.Point(123, 32);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(179, 20);
            this.txtCode.TabIndex = 5;
            this.txtCode.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Location = new System.Drawing.Point(123, 115);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(179, 21);
            this.cbStreetType.TabIndex = 19;
            this.cbStreetType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName
            // 
            this.cbStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName.FormattingEnabled = true;
            this.cbStreetName.Location = new System.Drawing.Point(123, 143);
            this.cbStreetName.Name = "cbStreetName";
            this.cbStreetName.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName.TabIndex = 21;
            this.cbStreetName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName2
            // 
            this.cbStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName2.FormattingEnabled = true;
            this.cbStreetName2.Location = new System.Drawing.Point(428, 143);
            this.cbStreetName2.Name = "cbStreetName2";
            this.cbStreetName2.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName2.TabIndex = 23;
            this.cbStreetName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // btnSelectCode
            // 
            this.btnSelectCode.Location = new System.Drawing.Point(308, 31);
            this.btnSelectCode.Name = "btnSelectCode";
            this.btnSelectCode.Size = new System.Drawing.Size(32, 22);
            this.btnSelectCode.TabIndex = 32;
            this.btnSelectCode.Text = "...";
            this.btnSelectCode.UseVisualStyleBackColor = true;
            this.btnSelectCode.Click += new System.EventHandler(this.btnSelectCode_Click);
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Sorted = true;
            this.cbName2.Location = new System.Drawing.Point(428, 87);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(179, 21);
            this.cbName2.TabIndex = 11;
            this.cbName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName.FormattingEnabled = true;
            this.cbName.Sorted = true;
            this.cbName.Location = new System.Drawing.Point(123, 87);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(179, 21);
            this.cbName.TabIndex = 9;
            this.cbName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 171);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 33;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 171);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 34;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 203);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 35;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 203);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 36;
            // 
            // BuildingGroupSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "BuildingGroupSearchForm";
            this.Text = "BuildingGroupSearchForm";
            this.Shown += new System.EventHandler(this.BuildingGroupSearchForm_Shown);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtCode;
        private Geomatic.UI.Controls.UpperComboBox cbName2;
        private Geomatic.UI.Controls.UpperComboBox cbName;
        private System.Windows.Forms.TextBox txtId;
        private Geomatic.UI.Controls.UpperComboBox cbStreetType;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName2;
        private System.Windows.Forms.Button btnSelectCode;
        private Geomatic.UI.Controls.UpperComboBox cbNaviStatus;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbState;
    }
}
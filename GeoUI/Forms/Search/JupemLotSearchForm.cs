﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class JupemLotSearchForm : SearchFeatureForm
    {
        public JupemLotSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Jupem Lot");
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            GetSegmentName();
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Segment Name");
            lvResult.Columns.Add("Exc Abb");
            lvResult.Columns.Add("Lot Num");
            lvResult.Columns.Add("Boundary Type");
            lvResult.Columns.Add("Boundary Name");
            lvResult.Columns.Add("State Code");
        }

        private void IdSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {   txtId.Focus(); }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        private void txtExcAbb_TextChanged(object sender, EventArgs e)
        {
            txtExcAbb.Text = txtExcAbb.Text.ToUpper();
        }

        protected override void DoSearch()
        {
            int? id = StringUtils.CheckInt(txtId.Text);
            string segmentname = StringUtils.CheckNull(txtSegmentName.Text);
            string excabb = StringUtils.TrimSpaces(txtExcAbb.Text);
            int? lotNum = StringUtils.CheckInt(txtLotNum.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (id.HasValue)
            {
                GJupem jupemLot = repo.GetById<GJupem>(id.Value);
                ListViewItem item = CreateItem(jupemLot);
                items.Add(item);
            }
            else
            {                
                Query<GJupem> query = new Query<GJupem>(SegmentName);

                if (!string.IsNullOrEmpty(excabb))
                {
                    query.AddClause(() => query.Obj.exc_abb, @"LIKE '" + excabb + @"%'");
                }
                if (lotNum.HasValue)
                {
                    query.AddClause(() => query.Obj.lot_num, @"LIKE '" + lotNum.ToString() + @"%'");
                }

                query.Obj.segment_name = segmentname; 

                foreach (GJupem jupemLot in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(jupemLot);
                    items.Add(item);
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GJupem jupemLot)
        {
            ListViewItem item = new ListViewItem();
            item.Text = jupemLot.OID.ToString();
            item.SubItems.Add(jupemLot.segment_name.ToString());
            item.SubItems.Add(jupemLot.exc_abb.ToString());
            item.SubItems.Add(jupemLot.lot_num.ToString());
            item.SubItems.Add(jupemLot.bnd_type.ToString());
            item.SubItems.Add(jupemLot.bnd_name.ToString());
            item.SubItems.Add(jupemLot.state_code.ToString());
            item.Tag = jupemLot;
            return item;
        }

        private void GetSegmentName()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    txtSegmentName.Text = "AS";
                    break;
                case SegmentName.JH:
                    txtSegmentName.Text = "JH";
                    break;
                case SegmentName.JP:
                    txtSegmentName.Text = "JP";
                    break;
                case SegmentName.KG:
                    txtSegmentName.Text = "KG";
                    break;
                case SegmentName.KK:
                    txtSegmentName.Text = "KK";
                    break;
                case SegmentName.KN:
                    txtSegmentName.Text = "KN";
                    break;
                case SegmentName.KV:
                    txtSegmentName.Text = "KV";
                    break;
                case SegmentName.MK:
                    txtSegmentName.Text = "MK";
                    break;
                case SegmentName.PG:
                    txtSegmentName.Text = "PG";
                    break;
                case SegmentName.TG:
                    txtSegmentName.Text = "TG";
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment."));
            }
        }

    }
}

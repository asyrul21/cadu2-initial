﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class LandmarkSearchForm : SearchFeatureForm
    {
        private int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        private int? SelectedStreetType
        {
            get { return (cbStreetType.SelectedItem == null) ? (int?)null : ((GStreetType)cbStreetType.SelectedItem).Code; }
        }

        public LandmarkSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Landmark");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateStreetType(cbStreetType);

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateASLandmarkName(cbName);
                    ComboBoxUtils.PopulateASLandmarkName2(cbName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateJHLandmarkName(cbName);
                    ComboBoxUtils.PopulateJHLandmarkName2(cbName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateJPLandmarkName(cbName);
                    ComboBoxUtils.PopulateJPLandmarkName2(cbName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateKGLandmarkName(cbName);
                    ComboBoxUtils.PopulateKGLandmarkName2(cbName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateKKLandmarkName(cbName);
                    ComboBoxUtils.PopulateKKLandmarkName2(cbName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateKNLandmarkName(cbName);
                    ComboBoxUtils.PopulateKNLandmarkName2(cbName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    //ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateKVLandmarkName(cbName);
                    ComboBoxUtils.PopulateKVLandmarkName2(cbName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateMKLandmarkName(cbName);
                    ComboBoxUtils.PopulateMKLandmarkName2(cbName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulatePGLandmarkName(cbName);
                    ComboBoxUtils.PopulatePGLandmarkName2(cbName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);

                    //landmark
                    ComboBoxUtils.PopulateTGLandmarkName(cbName);
                    ComboBoxUtils.PopulateTGLandmarkName2(cbName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void LandmarkSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string code = StringUtils.CheckNull(txtCode.Text);
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);
            string streetName2 = StringUtils.CheckNull(cbStreetName2.Text);

            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GLandmark landmark in repo.GetByIds<GLandmark>(ids, true))
                {
                    ListViewItem item = CreateItem(landmark);
                    items.Add(item);
                }
            }
            else
            {
                Query<GLandmark> query = new Query<GLandmark>(SegmentName);
                query.Obj.NavigationStatus = SelectedNavigationStatus;
                query.Obj.Code = code;
                query.Obj.Name = name;
                query.Obj.Name2 = name2;
                query.Obj.StreetType = SelectedStreetType;
                query.Obj.StreetName = streetName;
                query.Obj.StreetName2 = streetName2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.State = state;

                foreach (GLandmark landmark in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(landmark);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(landmark.Code);
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.StreetTypeValue));
            item.SubItems.Add(landmark.StreetName);
            item.SubItems.Add(landmark.StreetName2);
            item.SubItems.Add(landmark.Section);
            item.SubItems.Add(landmark.Postcode);
            item.SubItems.Add(landmark.City);
            item.SubItems.Add(landmark.State);
            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }

        private void btnSelectCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code = form.SelectedCode;
                txtCode.Text = code.GetCodes();
            }
        }
    }
}

﻿namespace Geomatic.UI.Forms.Search
{
    partial class PropertySearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblConstructionStatus;
            System.Windows.Forms.Label lblStreetType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblType;
            System.Windows.Forms.Label lblLot;
            System.Windows.Forms.Label lblHouse;
            System.Windows.Forms.Label lblYearInstall;
            System.Windows.Forms.Label lblNumOfFloor;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtNumOfFloor = new System.Windows.Forms.TextBox();
            this.txtYearInstall = new System.Windows.Forms.TextBox();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            lblName2 = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblId = new System.Windows.Forms.Label();
            lblConstructionStatus = new System.Windows.Forms.Label();
            lblStreetType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblType = new System.Windows.Forms.Label();
            lblLot = new System.Windows.Forms.Label();
            lblHouse = new System.Windows.Forms.Label();
            lblYearInstall = new System.Windows.Forms.Label();
            lblNumOfFloor = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 242);
            this.lvResult.Size = new System.Drawing.Size(610, 179);
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(347, 147);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(75, 13);
            lblName2.TabIndex = 18;
            lblName2.Text = "Street Name2:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 203);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 26;
            lblState.Text = "State:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 175);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 22;
            lblPostcode.Text = "Postcode:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 203);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 24;
            lblCity.Text = "City:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 175);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 20;
            lblSection.Text = "Section:";
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblConstructionStatus
            // 
            lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblConstructionStatus.AutoSize = true;
            lblConstructionStatus.Location = new System.Drawing.Point(320, 7);
            lblConstructionStatus.Name = "lblConstructionStatus";
            lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            lblConstructionStatus.TabIndex = 2;
            lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblStreetType
            // 
            lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetType.AutoSize = true;
            lblStreetType.Location = new System.Drawing.Point(52, 119);
            lblStreetType.Name = "lblStreetType";
            lblStreetType.Size = new System.Drawing.Size(65, 13);
            lblStreetType.TabIndex = 14;
            lblStreetType.Text = "Street Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(353, 119);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(69, 13);
            lblName.TabIndex = 16;
            lblName.Text = "Street Name:";
            // 
            // lblType
            // 
            lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblType.AutoSize = true;
            lblType.Location = new System.Drawing.Point(83, 35);
            lblType.Name = "lblType";
            lblType.Size = new System.Drawing.Size(34, 13);
            lblType.TabIndex = 4;
            lblType.Text = "Type:";
            // 
            // lblLot
            // 
            lblLot.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblLot.AutoSize = true;
            lblLot.Location = new System.Drawing.Point(92, 63);
            lblLot.Name = "lblLot";
            lblLot.Size = new System.Drawing.Size(25, 13);
            lblLot.TabIndex = 6;
            lblLot.Text = "Lot:";
            // 
            // lblHouse
            // 
            lblHouse.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblHouse.AutoSize = true;
            lblHouse.Location = new System.Drawing.Point(381, 63);
            lblHouse.Name = "lblHouse";
            lblHouse.Size = new System.Drawing.Size(41, 13);
            lblHouse.TabIndex = 8;
            lblHouse.Text = "House:";
            // 
            // lblYearInstall
            // 
            lblYearInstall.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblYearInstall.AutoSize = true;
            lblYearInstall.Location = new System.Drawing.Point(55, 91);
            lblYearInstall.Name = "lblYearInstall";
            lblYearInstall.Size = new System.Drawing.Size(62, 13);
            lblYearInstall.TabIndex = 10;
            lblYearInstall.Text = "Year Install:";
            // 
            // lblNumOfFloor
            // 
            lblNumOfFloor.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNumOfFloor.AutoSize = true;
            lblNumOfFloor.Location = new System.Drawing.Point(337, 91);
            lblNumOfFloor.Name = "lblNumOfFloor";
            lblNumOfFloor.Size = new System.Drawing.Size(85, 13);
            lblNumOfFloor.TabIndex = 12;
            lblNumOfFloor.Text = "Number of Floor:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtNumOfFloor, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.txtYearInstall, 1, 3);
            this.tableLayoutPanel.Controls.Add(lblNumOfFloor, 2, 3);
            this.tableLayoutPanel.Controls.Add(lblYearInstall, 0, 3);
            this.tableLayoutPanel.Controls.Add(lblHouse, 2, 2);
            this.tableLayoutPanel.Controls.Add(lblLot, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.txtHouse, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtLot, 1, 2);
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(lblConstructionStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cbConstructionStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(lblCity, 0, 7);
            this.tableLayoutPanel.Controls.Add(lblSection, 0, 6);
            this.tableLayoutPanel.Controls.Add(lblState, 2, 7);
            this.tableLayoutPanel.Controls.Add(lblPostcode, 2, 6);
            this.tableLayoutPanel.Controls.Add(lblName2, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.cbStreetName2, 3, 5);
            this.tableLayoutPanel.Controls.Add(lblName, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.cbStreetName, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.cbStreetType, 1, 4);
            this.tableLayoutPanel.Controls.Add(lblStreetType, 0, 4);
            this.tableLayoutPanel.Controls.Add(lblType, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbType, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.cbSection, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.cbCity, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.cbPostcode, 3, 6);
            this.tableLayoutPanel.Controls.Add(this.cbState, 3, 7);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 8;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(610, 224);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // txtNumOfFloor
            // 
            this.txtNumOfFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumOfFloor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumOfFloor.Location = new System.Drawing.Point(428, 88);
            this.txtNumOfFloor.Name = "txtNumOfFloor";
            this.txtNumOfFloor.Size = new System.Drawing.Size(179, 20);
            this.txtNumOfFloor.TabIndex = 13;
            this.txtNumOfFloor.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtYearInstall
            // 
            this.txtYearInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtYearInstall.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYearInstall.Location = new System.Drawing.Point(123, 88);
            this.txtYearInstall.Name = "txtYearInstall";
            this.txtYearInstall.Size = new System.Drawing.Size(179, 20);
            this.txtYearInstall.TabIndex = 11;
            this.txtYearInstall.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(428, 60);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(179, 20);
            this.txtHouse.TabIndex = 9;
            this.txtHouse.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(123, 60);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(179, 20);
            this.txtLot.TabIndex = 7;
            this.txtLot.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbConstructionStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(428, 3);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(179, 21);
            this.cbConstructionStatus.TabIndex = 3;
            this.cbConstructionStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName2
            // 
            this.cbStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName2.FormattingEnabled = true;
            this.cbStreetName2.Location = new System.Drawing.Point(428, 143);
            this.cbStreetName2.Name = "cbStreetName2";
            this.cbStreetName2.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName2.TabIndex = 19;
            this.cbStreetName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName
            // 
            this.cbStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName.FormattingEnabled = true;
            this.cbStreetName.Location = new System.Drawing.Point(428, 115);
            this.cbStreetName.Name = "cbStreetName";
            this.cbStreetName.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName.TabIndex = 17;
            this.cbStreetName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Location = new System.Drawing.Point(123, 115);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(179, 21);
            this.cbStreetType.TabIndex = 15;
            this.cbStreetType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(123, 31);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(179, 21);
            this.cbType.TabIndex = 5;
            this.cbType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 171);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 27;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 199);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 28;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 171);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 29;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 199);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 30;
            // 
            // PropertySearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "PropertySearchForm";
            this.Text = "PropertySearchForm";
            this.Shown += new System.EventHandler(this.PropertySearchForm_Shown);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox txtId;
        private Geomatic.UI.Controls.UpperComboBox cbConstructionStatus;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName2;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName;
        private Geomatic.UI.Controls.UpperComboBox cbStreetType;
        private System.Windows.Forms.TextBox txtHouse;
        private System.Windows.Forms.TextBox txtLot;
        private System.Windows.Forms.TextBox txtNumOfFloor;
        private System.Windows.Forms.TextBox txtYearInstall;
        private Geomatic.UI.Controls.UpperComboBox cbType;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbState;
    }
}
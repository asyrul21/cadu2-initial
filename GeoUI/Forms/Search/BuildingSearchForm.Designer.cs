﻿namespace Geomatic.UI.Forms.Search
{
    partial class BuildingSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblConstructionStatus;
            System.Windows.Forms.Label lblNaviStatus;
            System.Windows.Forms.Label lblStreetType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblPropertyType;
            System.Windows.Forms.Label lblLot;
            System.Windows.Forms.Label lblHouse;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label lblCode;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbStreetType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPropertyType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            lblId = new System.Windows.Forms.Label();
            lblConstructionStatus = new System.Windows.Forms.Label();
            lblNaviStatus = new System.Windows.Forms.Label();
            lblStreetType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblPropertyType = new System.Windows.Forms.Label();
            lblLot = new System.Windows.Forms.Label();
            lblHouse = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            lblCode = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 270);
            this.lvResult.Size = new System.Drawing.Size(610, 151);
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblConstructionStatus
            // 
            lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblConstructionStatus.AutoSize = true;
            lblConstructionStatus.Location = new System.Drawing.Point(320, 7);
            lblConstructionStatus.Name = "lblConstructionStatus";
            lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            lblConstructionStatus.TabIndex = 2;
            lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblNaviStatus
            // 
            lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNaviStatus.AutoSize = true;
            lblNaviStatus.Location = new System.Drawing.Point(328, 35);
            lblNaviStatus.Name = "lblNaviStatus";
            lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            lblNaviStatus.TabIndex = 6;
            lblNaviStatus.Text = "Navigation Status:";
            // 
            // lblStreetType
            // 
            lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetType.AutoSize = true;
            lblStreetType.Location = new System.Drawing.Point(52, 147);
            lblStreetType.Name = "lblStreetType";
            lblStreetType.Size = new System.Drawing.Size(65, 13);
            lblStreetType.TabIndex = 18;
            lblStreetType.Text = "Street Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(353, 147);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(69, 13);
            lblName.TabIndex = 20;
            lblName.Text = "Street Name:";
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(347, 175);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(75, 13);
            lblName2.TabIndex = 22;
            lblName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 203);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 24;
            lblSection.Text = "Section:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 203);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 26;
            lblPostcode.Text = "Postcode:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 231);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 28;
            lblCity.Text = "City:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 231);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 30;
            lblState.Text = "State:";
            // 
            // lblPropertyType
            // 
            lblPropertyType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPropertyType.AutoSize = true;
            lblPropertyType.Location = new System.Drawing.Point(41, 91);
            lblPropertyType.Name = "lblPropertyType";
            lblPropertyType.Size = new System.Drawing.Size(76, 13);
            lblPropertyType.TabIndex = 12;
            lblPropertyType.Text = "Property Type:";
            // 
            // lblLot
            // 
            lblLot.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblLot.AutoSize = true;
            lblLot.Location = new System.Drawing.Point(92, 119);
            lblLot.Name = "lblLot";
            lblLot.Size = new System.Drawing.Size(25, 13);
            lblLot.TabIndex = 14;
            lblLot.Text = "Lot:";
            // 
            // lblHouse
            // 
            lblHouse.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblHouse.AutoSize = true;
            lblHouse.Location = new System.Drawing.Point(381, 119);
            lblHouse.Name = "lblHouse";
            lblHouse.Size = new System.Drawing.Size(41, 13);
            lblHouse.TabIndex = 16;
            lblHouse.Text = "House:";
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(79, 63);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(38, 13);
            label1.TabIndex = 8;
            label1.Text = "Name:";
            // 
            // label2
            // 
            label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(378, 63);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(44, 13);
            label2.TabIndex = 10;
            label2.Text = "Name2:";
            // 
            // lblCode
            // 
            lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCode.AutoSize = true;
            lblCode.Location = new System.Drawing.Point(82, 35);
            lblCode.Name = "lblCode";
            lblCode.Size = new System.Drawing.Size(35, 13);
            lblCode.TabIndex = 4;
            lblCode.Text = "Code:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel1.Controls.Add(lblCode, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbName2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(label2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtHouse, 3, 4);
            this.tableLayoutPanel1.Controls.Add(lblHouse, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtLot, 1, 4);
            this.tableLayoutPanel1.Controls.Add(lblNaviStatus, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbConstructionStatus, 3, 0);
            this.tableLayoutPanel1.Controls.Add(lblConstructionStatus, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(lblCity, 0, 8);
            this.tableLayoutPanel1.Controls.Add(lblSection, 0, 7);
            this.tableLayoutPanel1.Controls.Add(lblStreetType, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetType, 1, 5);
            this.tableLayoutPanel1.Controls.Add(lblState, 2, 8);
            this.tableLayoutPanel1.Controls.Add(lblPostcode, 2, 7);
            this.tableLayoutPanel1.Controls.Add(lblName2, 2, 6);
            this.tableLayoutPanel1.Controls.Add(lblName, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetName2, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetName, 3, 5);
            this.tableLayoutPanel1.Controls.Add(lblPropertyType, 0, 3);
            this.tableLayoutPanel1.Controls.Add(lblLot, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbPropertyType, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbNaviStatus, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbSection, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.cbPostcode, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.cbCity, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.cbState, 3, 8);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(610, 252);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCode.Location = new System.Drawing.Point(123, 32);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(179, 20);
            this.txtCode.TabIndex = 5;
            this.txtCode.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Location = new System.Drawing.Point(428, 59);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(179, 21);
            this.cbName2.TabIndex = 11;
            this.cbName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName.FormattingEnabled = true;
            this.cbName.Sorted = true;
            this.cbName.Location = new System.Drawing.Point(123, 59);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(179, 21);
            this.cbName.TabIndex = 9;
            this.cbName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(428, 116);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(179, 20);
            this.txtHouse.TabIndex = 17;
            this.txtHouse.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(123, 116);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(179, 20);
            this.txtLot.TabIndex = 15;
            this.txtLot.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbConstructionStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(428, 3);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(179, 21);
            this.cbConstructionStatus.TabIndex = 3;
            this.cbConstructionStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Location = new System.Drawing.Point(123, 143);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(179, 21);
            this.cbStreetType.TabIndex = 19;
            this.cbStreetType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName2
            // 
            this.cbStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName2.FormattingEnabled = true;
            this.cbStreetName2.Location = new System.Drawing.Point(428, 171);
            this.cbStreetName2.Name = "cbStreetName2";
            this.cbStreetName2.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName2.TabIndex = 23;
            this.cbStreetName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName
            // 
            this.cbStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName.FormattingEnabled = true;
            this.cbStreetName.Location = new System.Drawing.Point(428, 143);
            this.cbStreetName.Name = "cbStreetName";
            this.cbStreetName.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName.TabIndex = 21;
            this.cbStreetName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbPropertyType
            // 
            this.cbPropertyType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPropertyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPropertyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPropertyType.FormattingEnabled = true;
            this.cbPropertyType.Location = new System.Drawing.Point(123, 87);
            this.cbPropertyType.Name = "cbPropertyType";
            this.cbPropertyType.Size = new System.Drawing.Size(179, 21);
            this.cbPropertyType.TabIndex = 13;
            this.cbPropertyType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbNaviStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(428, 31);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(179, 21);
            this.cbNaviStatus.TabIndex = 7;
            this.cbNaviStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 199);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 31;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 199);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 32;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 227);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 33;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 227);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 34;
            // 
            // BuildingSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "BuildingSearchForm";
            this.Text = "BuildingSearchForm";
            this.Shown += new System.EventHandler(this.BuildingSearchForm_Shown);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtId;
        private Geomatic.UI.Controls.UpperComboBox cbConstructionStatus;
        private Geomatic.UI.Controls.UpperComboBox cbStreetType;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName;
        private Geomatic.UI.Controls.UpperComboBox cbStreetName2;
        private System.Windows.Forms.TextBox txtLot;
        private System.Windows.Forms.TextBox txtHouse;
        private Geomatic.UI.Controls.UpperComboBox cbName;
        private Geomatic.UI.Controls.UpperComboBox cbName2;
        private System.Windows.Forms.TextBox txtCode;
        private Geomatic.UI.Controls.UpperComboBox cbPropertyType;
        private Geomatic.UI.Controls.UpperComboBox cbNaviStatus;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbState;
    }
}
﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class JunctionSearchForm : SearchFeatureForm
    {
        private int? SelectedJunctionType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((GJunctionType)cbType.SelectedItem).Code; }
        }

        public JunctionSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Junction");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateJunctionType(cbType);

            //added by asyrul

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASJunctionName(cbName);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHJunctionName(cbName);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPJunctionName(cbName);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGJunctionName(cbName);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKJunctionName(cbName);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNJunctionName(cbName);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVJunctionName(cbName);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKJunctionName(cbName);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGJunctionName(cbName);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGJunctionName(cbName);
                    break;

                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void JunctionSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string name = StringUtils.CheckNull(cbName.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GJunction junction in repo.GetByIds<GJunction>(ids, true))
                {
                    ListViewItem item = CreateItem(junction);
                    items.Add(item);
                }
            }
            else
            {
                Query<GJunction> query = new Query<GJunction>(SegmentName);
                query.Obj.Name = name;
                query.Obj.Type = SelectedJunctionType;

                foreach (GJunction junction in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(junction);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GJunction junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }
    }
}

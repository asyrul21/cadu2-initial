﻿using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GFloor = Geomatic.Core.Features.JoinedFeatures.GFloor;

namespace Geomatic.UI.Forms.Search
{
    public partial class FloorSearchForm : SearchFeatureForm
    {
        private int? SelectedPropertyType
        {
            get { return (cbPropertyType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbPropertyType.SelectedItem).Value.Code; }
        }

        private int? SelectedStreetType
        {
            get { return (cbStreetType.SelectedItem == null) ? (int?)null : ((GStreetType)cbStreetType.SelectedItem).Code; }
        }

        public FloorSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Floor");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Number");
            lvResult.Columns.Add("Unit No");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulatePropertyType(cbPropertyType);
            ComboBoxUtils.PopulateStreetType(cbStreetType);

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    //ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void FloorSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string number = StringUtils.CheckNull(txtNumber.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);
            string streetName2 = StringUtils.CheckNull(cbStreetName2.Text);

            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null; ;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GFloor floor in repo.GetByIds<GFloor>(ids, true))
                {
                    ListViewItem item = CreateItem(floor);
                    items.Add(item);
                }
            }
            else
            {
                Query<GFloor> query = new Query<GFloor>(SegmentName);
                query.Obj.Number = number;
                query.Obj.PropertyType = SelectedPropertyType;
                query.Obj.StreetType = SelectedStreetType;
                query.Obj.StreetName = streetName;
                query.Obj.StreetName2 = streetName2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.State = state;
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");

                foreach (GFloor floor in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(floor);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GFloor floor)
        {
            ListViewItem item = new ListViewItem();
            item.Text = floor.OID.ToString();
            item.SubItems.Add(floor.Number);
            item.SubItems.Add(floor.NumUnit);
            item.SubItems.Add(StringUtils.TrimSpaces(floor.StreetTypeValue));
            item.SubItems.Add(floor.StreetName);
            item.SubItems.Add(floor.StreetName2);
            item.SubItems.Add(floor.Section);
            item.SubItems.Add(floor.Postcode);
            item.SubItems.Add(floor.City);
            item.SubItems.Add(floor.State);
            item.SubItems.Add(floor.CreatedBy);
            item.SubItems.Add(floor.DateCreated);
            item.SubItems.Add(floor.UpdatedBy);
            item.SubItems.Add(floor.DateUpdated);
            item.Tag = floor;
            return item;
        }
    }
}

﻿namespace Geomatic.UI.Forms.Search
{
    partial class LandmarkSearchByIdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkPOI = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chkPOI
            // 
            this.chkPOI.AutoSize = true;
            this.chkPOI.Location = new System.Drawing.Point(330, 429);
            this.chkPOI.Name = "chkPOI";
            this.chkPOI.Size = new System.Drawing.Size(44, 17);
            this.chkPOI.TabIndex = 15;
            this.chkPOI.Text = "POI";
            this.chkPOI.UseVisualStyleBackColor = true;
            this.chkPOI.CheckedChanged += new System.EventHandler(this.ChkPOI_CheckedChanged);
            // 
            // LandmarkSearchByIdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 462);
            this.Controls.Add(this.chkPOI);
            this.Name = "LandmarkSearchByIdForm";
            this.Text = "LandmarkSearchByIdForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.Controls.SetChildIndex(this.chkPOI, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkPOI;
    }
}
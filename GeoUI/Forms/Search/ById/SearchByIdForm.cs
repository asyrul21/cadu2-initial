﻿using Geomatic.Core;
using Geomatic.Core.Sessions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class SearchByIdForm : SearchFeatureForm
    {
        public SearchByIdForm()
            : this(SegmentName.None)
        {
        }

        public SearchByIdForm(SegmentName segmentName) : base(segmentName)
        {
            InitializeComponent();
        }

        private void SearchByIdForm_Load(object sender, EventArgs e)
        {

        }
    }
}

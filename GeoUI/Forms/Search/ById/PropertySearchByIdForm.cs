﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GFloor1 = Geomatic.Core.Rows.GFloor;

namespace Geomatic.UI.Forms.Search
{
    public partial class PropertySearchByIdForm : SearchByIdForm
    {
        public PropertySearchByIdForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Property By Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            PropertyResultTitle();
        }

        private void PropertySearchByIdForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkPOI.Checked && !chkMultiFloor.Checked)
                {
                    if (!chkSearchAND.Checked)
                    {
                        foreach (GProperty property in repo.GetByIds<GProperty>(ids, true))
                        {
                            ListViewItem item = CreateItem(property);
                            items.Add(item);
                        }
                    }
                    else
                    {
                        // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                        foreach (GProperty propertys in repo.GetByIds<GProperty>(ids, true))
                        {
                            //GProperty property = repo.GetById<GProperty>(propertys.OID);
                            Geomatic.Core.Features.GPropertyAND propertyAND = propertys.GetPropertyANDId();
                            if (propertyAND != null)
                            {
                                GPropertyAND PropertyAND = repo.GetById<GPropertyAND>(propertyAND.OID);

                                ListViewItem item = CreateItem(PropertyAND);
                                items.Add(item);
                            }
                        }
                    }
                }
                else
                {
                    if (chkPOI.Checked)
                    {
                        // added by noraini ali - Sept 2019
                        int id = Int32.Parse(txtId.Text);
                        Query<GPoi> query = new Query<GPoi>(SegmentName);
                        query.Obj.ParentId = id;

                        foreach (GPoi poi in repo.Search(query, true, Limit))
                        {
                            ListViewItem item = CreateItem(poi);
                            items.Add(item);
                        }
                    }
                    else
                    {
                        // noraini ali - Jun 2020 - Display list multi floor and can save list in Excel
                        int id = Int32.Parse(txtId.Text);
                        //Query<GFloor> query = new Query<GFloor>(SegmentName);
                        //query.Obj.PropertyId = id;
                        //
                        //foreach (GFloor floor in repo.Search(query, true, Limit))
                        //{
                        //    ListViewItem item = CreateItem(floor);
                        //    items.Add(item);
                        //}

                        GProperty _property = repo.GetById<GProperty>(id);
                        foreach (GFloor1 floor in _property.GetFloors(true))
                        {
                            ListViewItem item = CreateItem(floor);
                            items.Add(item);
                        }
                    }
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GProperty property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(property.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add(property.Lot);
            item.SubItems.Add(property.House);
            item.SubItems.Add((property.YearInstall.HasValue) ? property.YearInstall.Value.ToString() : string.Empty);
            item.SubItems.Add((property.NumberOfFloor.HasValue) ? property.NumberOfFloor.Value.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(property.StreetTypeValue));
            item.SubItems.Add(property.StreetName);
            item.SubItems.Add(property.StreetName2);
            item.SubItems.Add(property.Section);
            item.SubItems.Add(property.Postcode);
            item.SubItems.Add(property.City);
            item.SubItems.Add(property.State);
            item.SubItems.Add(property.CreatedBy);
            item.SubItems.Add(property.DateCreated);
            item.SubItems.Add(property.UpdatedBy);
            item.SubItems.Add(property.DateUpdated);
            item.Tag = property;
            return item;
        }

        private ListViewItem CreateItem(GPropertyAND property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(property.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add(property.Lot);
            item.SubItems.Add(property.House);
            item.SubItems.Add((property.YearInstall.HasValue) ? property.YearInstall.Value.ToString() : string.Empty);
            item.SubItems.Add((property.NumberOfFloor.HasValue) ? property.NumberOfFloor.Value.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(property.StreetTypeValue));
            item.SubItems.Add(property.StreetName);
            item.SubItems.Add(property.StreetName2);
            item.SubItems.Add(property.Section);
            item.SubItems.Add(property.Postcode);
            item.SubItems.Add(property.City);
            item.SubItems.Add(property.State);
            item.SubItems.Add(property.CreatedBy);
            item.SubItems.Add(property.DateCreated);
            item.SubItems.Add(property.UpdatedBy);
            item.SubItems.Add(property.DateUpdated);
            item.Tag = property;
            return item;
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add(poi.NavigationStatusValue);
            item.SubItems.Add(poi.Description);
            item.SubItems.Add(poi.Url);
            item.Tag = poi;
            return item;
        }

        private void ChkPOI_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPOI.Checked)
            {
                chkMultiFloor.Checked = false;

                lvResult.Clear();
                lvResult.Columns.Add("Id");
                lvResult.Columns.Add("Code");
                lvResult.Columns.Add("Name");
                lvResult.Columns.Add("Name2");
                lvResult.Columns.Add("Navi_Status");
                lvResult.Columns.Add("Description");
                lvResult.Columns.Add("Url");
            }
            else
            {
                PropertyResultTitle();
            }
        }

        private void ChkMultiFloor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMultiFloor.Checked)
            {
                chkPOI.Checked = false;

                lvResult.Clear();
                lvResult.Columns.Add("Id");
                lvResult.Columns.Add("Floor Number");
                lvResult.Columns.Add("Unit Number");
                //lvResult.Columns.Add("Street Type");
                //lvResult.Columns.Add("Street Name");
                //lvResult.Columns.Add("Street Name2");
                //lvResult.Columns.Add("Section");
                //lvResult.Columns.Add("Postcode");
                //lvResult.Columns.Add("City");
                //lvResult.Columns.Add("State");
                lvResult.Columns.Add("Created By");
                lvResult.Columns.Add("Date Created");
                lvResult.Columns.Add("Updated By");
                lvResult.Columns.Add("Date Updated");
                lvResult.Columns.Add("Status");
            }
            else
            {
                PropertyResultTitle();
            }
        }

        private ListViewItem CreateItem(GFloor floor)
        {
            ListViewItem item = new ListViewItem();
            item.Text = floor.OID.ToString();
            item.SubItems.Add(floor.Number);
            item.SubItems.Add(floor.NumUnit);
            item.SubItems.Add(StringUtils.TrimSpaces(floor.StreetTypeValue));
            item.SubItems.Add(floor.StreetName);
            item.SubItems.Add(floor.StreetName2);
            item.SubItems.Add(floor.Section);
            item.SubItems.Add(floor.Postcode);
            item.SubItems.Add(floor.City);
            item.SubItems.Add(floor.State);
            item.SubItems.Add(floor.CreatedBy);
            item.SubItems.Add(floor.DateCreated);
            item.SubItems.Add(floor.UpdatedBy);
            item.SubItems.Add(floor.DateUpdated);
            item.Tag = floor;
            return item;
        }

        private ListViewItem CreateItem(GFloor1 floor)
        {
            ListViewItem item = new ListViewItem();
            item.Text = floor.OID.ToString();
            item.SubItems.Add(floor.Number);
            item.SubItems.Add(floor.NumUnit);
            item.SubItems.Add(floor.CreatedBy);
            item.SubItems.Add(floor.DateCreated);
            item.SubItems.Add(floor.UpdatedBy);
            item.SubItems.Add(floor.DateUpdated);
            item.SubItems.Add(GetnameAndStatus(floor.UpdateStatus));
            item.Tag = floor;
            return item;
        }

        private void PropertyResultTitle()
        {
            lvResult.Clear();
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Year Install");
            lvResult.Columns.Add("Number of Floor");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private string GetnameAndStatus(int? Status)
        {
            string StatusName = "";

            if (Status == 1)
            { StatusName = "NEW"; }
            else if (Status == 2)
            { StatusName = "DELETE"; }
            else if (Status == 3)
            { StatusName = "EDIT GRAPHIC & ATT"; }
            else if (Status == 4)
            { StatusName = "EDIT ATT ONLY"; }

            return StatusName;
        }
    }
}

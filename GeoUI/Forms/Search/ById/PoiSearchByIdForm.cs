﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class PoiSearchByIdForm : SearchByIdForm
    {
        public PoiSearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search POI by id");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Url");
            lvResult.Columns.Add("Description");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private void PoiSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
       
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GPoi poi in repo.GetByIds<GPoi>(ids, true))
                {
                    ListViewItem item = CreateItem(poi);
                    items.Add(item);
                }
            }
          
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add(poi.Url);
            item.SubItems.Add(poi.Description);
            item.SubItems.Add(StringUtils.TrimSpaces(poi.StreetTypeValue));
            item.SubItems.Add(poi.StreetName);
            item.SubItems.Add(poi.StreetName2);
            item.SubItems.Add(poi.Section);
            item.SubItems.Add(poi.Postcode);
            item.SubItems.Add(poi.City);
            item.SubItems.Add(poi.State);
            item.SubItems.Add(poi.CreatedBy);
            item.SubItems.Add(poi.DateCreated);
            item.SubItems.Add(poi.UpdatedBy);
            item.SubItems.Add(poi.DateUpdated);
            item.Tag = poi;
            return item;
        }
    }
}


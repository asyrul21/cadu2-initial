﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class JunctionSearchByIdForm : SearchByIdForm
    {
        public JunctionSearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Junction by Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private void JunctionSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkSearchAND.Checked)
                {
                    foreach (GJunction junction in repo.GetByIds<GJunction>(ids, true))
                    {
                        ListViewItem item = CreateItem(junction);
                        items.Add(item);
                    }
                }
                else
                {
                    // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                    foreach (GJunction junctions in repo.GetByIds<GJunction>(ids, true))
                    {
                        Geomatic.Core.Features.GJunctionAND junctionAND = junctions.GetJunctionANDId();
                        if (junctionAND != null)
                        {
                            GJunctionAND JunctionAND = repo.GetById<GJunctionAND>(junctionAND.OID);
                            ListViewItem item = CreateItem(JunctionAND);
                            items.Add(item);
                        }
                    }
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GJunction junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }

        private ListViewItem CreateItem(GJunctionAND junction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = junction.OID.ToString();
            item.SubItems.Add(junction.Name);
            item.SubItems.Add(StringUtils.TrimSpaces(junction.TypeValue));
            item.SubItems.Add(junction.CreatedBy);
            item.SubItems.Add(junction.DateCreated);
            item.SubItems.Add(junction.UpdatedBy);
            item.SubItems.Add(junction.DateUpdated);
            item.Tag = junction;
            return item;
        }
    }
}

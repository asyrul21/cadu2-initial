﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;
using GMultiStorey = Geomatic.Core.Features.JoinedFeatures.GMultiStorey;

namespace Geomatic.UI.Forms.Search
{
    public partial class MultistoreySearchByIdForm : SearchByIdForm
    {
        public MultistoreySearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Multistorey by ID");
        }


        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Floor");
            lvResult.Columns.Add("Apartment");
            lvResult.Columns.Add("Building Name");
            lvResult.Columns.Add("Building Name2");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
            lvResult.Columns.Add("Status");
        }

        private void MultiStoreySearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
         
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GMultiStorey multiStorey in repo.GetByIds<GMultiStorey>(ids, true))
                {
                    ListViewItem item = CreateItem(multiStorey);
                    items.Add(item);
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GMultiStorey multiStorey)
        {
            ListViewItem item = new ListViewItem();
            item.Text = multiStorey.OID.ToString();
            item.SubItems.Add(multiStorey.Floor);
            item.SubItems.Add(multiStorey.Apartment);
            item.SubItems.Add(multiStorey.BuildingName);
            item.SubItems.Add(multiStorey.BuildingName2);
            item.SubItems.Add(StringUtils.TrimSpaces(multiStorey.StreetTypeValue));
            item.SubItems.Add(multiStorey.StreetName);
            item.SubItems.Add(multiStorey.StreetName2);
            item.SubItems.Add(multiStorey.Section);
            item.SubItems.Add(multiStorey.Postcode);
            item.SubItems.Add(multiStorey.City);
            item.SubItems.Add(multiStorey.State);
            item.SubItems.Add(multiStorey.UpdatedBy);
            item.SubItems.Add(multiStorey.DateUpdated);
            item.SubItems.Add(GetnameAndStatus(multiStorey.UpdateStatus));
            item.Tag = multiStorey;
            return item;
        }

        private string GetnameAndStatus(int? Status)
        {
            string StatusName = "";

            if (Status == 1)
            { StatusName = "NEW"; }
            else if (Status == 2)
            { StatusName = "DELETE"; }
            else if (Status == 3)
            { StatusName = "EDIT GRAPHIC & ATT"; }
            else if (Status == 4)
            { StatusName = "EDIT ATT ONLY"; }

            return StatusName;
        }
    }
}

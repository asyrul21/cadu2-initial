﻿using Geomatic.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class SearchByNameForm : SearchFeatureForm
    {
        public SearchByNameForm()
           : this(SegmentName.None)
        {
        }

        public SearchByNameForm(SegmentName segmentName) : base(segmentName)
        {
            InitializeComponent();
        }

        private void SearchByIdForm_Load(object sender, EventArgs e)
        {

        }

        private void lvResult_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

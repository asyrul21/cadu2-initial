﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class PoiSearchByNameForm : SearchByNameForm
    {
        public PoiSearchByNameForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search POI by names");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Url");
            lvResult.Columns.Add("Description");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASPoiName(cbName);
                    ComboBoxUtils.PopulateASPoiName2(cbName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHPoiName(cbName);
                    ComboBoxUtils.PopulateJHPoiName2(cbName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPPoiName(cbName);
                    ComboBoxUtils.PopulateJPPoiName2(cbName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGPoiName(cbName);
                    ComboBoxUtils.PopulateKGPoiName2(cbName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKPoiName(cbName);
                    ComboBoxUtils.PopulateKKPoiName2(cbName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNPoiName(cbName);
                    ComboBoxUtils.PopulateKNPoiName2(cbName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVPoiName(cbName);
                    ComboBoxUtils.PopulateKVPoiName2(cbName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKPoiName(cbName);
                    ComboBoxUtils.PopulateMKPoiName2(cbName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGPoiName(cbName);
                    ComboBoxUtils.PopulatePGPoiName2(cbName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGPoiName(cbName);
                    ComboBoxUtils.PopulateTGPoiName2(cbName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void PoiSearchForm_Shown(object sender, EventArgs e)
        {
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);
           
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
          
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.Name = name;
            query.Obj.Name2 = name2;

            foreach (GPoi poi in repo.Search(query, true, Limit))
            {
                ListViewItem item = CreateItem(poi);
                items.Add(item);
            }
          
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add(poi.Url);
            item.SubItems.Add(poi.Description);
            item.SubItems.Add(StringUtils.TrimSpaces(poi.StreetTypeValue));
            item.SubItems.Add(poi.StreetName);
            item.SubItems.Add(poi.StreetName2);
            item.SubItems.Add(poi.Section);
            item.SubItems.Add(poi.Postcode);
            item.SubItems.Add(poi.City);
            item.SubItems.Add(poi.State);
            item.SubItems.Add(poi.CreatedBy);
            item.SubItems.Add(poi.DateCreated);
            item.SubItems.Add(poi.UpdatedBy);
            item.SubItems.Add(poi.DateUpdated);
            item.Tag = poi;
            return item;
        }
    }
}


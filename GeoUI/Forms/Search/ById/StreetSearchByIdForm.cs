﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class StreetSearchByIdForm : SearchByIdForm
    {
        public StreetSearchByIdForm(SegmentName segmentName)
           : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Street By Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("Sub City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Network Class");
            lvResult.Columns.Add("Class");
            lvResult.Columns.Add("Category");
            lvResult.Columns.Add("Filter Level");
            lvResult.Columns.Add("Speed Limit");
            lvResult.Columns.Add("Direction");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private void StreetSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkSearchAND.Checked)
                {
                    foreach (GStreet street in repo.GetByIds<GStreet>(ids))
                    {
                        ListViewItem item = CreateItem(street);
                        items.Add(item);
                    }
                }
                else
                {
                    // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                    foreach (GStreet streets in repo.GetByIds<GStreet>(ids, true))
                    {
                        Geomatic.Core.Features.GStreetAND streetAND = streets.GetStreetANDId();
                        //foreach (Geomatic.Core.Features.GStreetAND streetAND in streets.GetStreetsANDId())
                        if (streetAND != null)
                        {
                            GStreetAND StreetAND = repo.GetById<GStreetAND>(streetAND.OID);
                            ListViewItem item = CreateItem(StreetAND);
                            items.Add(item);
                        }
                    }
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(street.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.SubCity);
            item.SubItems.Add(street.State);
            item.SubItems.Add(StringUtils.TrimSpaces(street.StatusValue));
            item.SubItems.Add(street.NavigationStatusValue);
            item.SubItems.Add(StringUtils.TrimSpaces(street.NetworkClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.ClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.CategoryValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.FilterLevelValue));
            item.SubItems.Add((street.SpeedLimit.HasValue) ? street.SpeedLimit.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(street.DirectionValue));
            item.SubItems.Add(street.CreatedBy);
            item.SubItems.Add(street.DateCreated);
            item.SubItems.Add(street.UpdatedBy);
            item.SubItems.Add(street.DateUpdated);
            item.Tag = street;
            return item;
        }
    }

}


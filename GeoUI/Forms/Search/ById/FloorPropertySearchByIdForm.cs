﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GFloor = Geomatic.Core.Features.JoinedFeatures.GFloor;

namespace Geomatic.UI.Forms.Search
{
    public partial class FloorPropertySearchByIdForm : SearchByIdForm
    {
        public FloorPropertySearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Floor By Id");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Floor Number");
            lvResult.Columns.Add("Unit Number");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
            lvResult.Columns.Add("Status");
        }

        private void FloorSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
 
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GFloor floor in repo.GetByIds<GFloor>(ids, true))
                {
                    ListViewItem item = CreateItem(floor);
                    items.Add(item);
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GFloor floor)
        {
            ListViewItem item = new ListViewItem();
            item.Text = floor.OID.ToString();
            item.SubItems.Add(floor.Number);
            item.SubItems.Add(floor.NumUnit);
            item.SubItems.Add(StringUtils.TrimSpaces(floor.StreetTypeValue));
            item.SubItems.Add(floor.StreetName);
            item.SubItems.Add(floor.StreetName2);
            item.SubItems.Add(floor.Section);
            item.SubItems.Add(floor.Postcode);
            item.SubItems.Add(floor.City);
            item.SubItems.Add(floor.State);
            item.SubItems.Add(floor.CreatedBy);
            item.SubItems.Add(floor.DateCreated);
            item.SubItems.Add(floor.UpdatedBy);
            item.SubItems.Add(floor.DateUpdated);
            item.SubItems.Add(GetnameAndStatus(floor.UpdateStatus));
            item.Tag = floor;
            return item;
        }

        private string GetnameAndStatus(int? Status)
        {
            string StatusName = "";

            if (Status == 1)
            { StatusName = "NEW"; }
            else if (Status == 2)
            { StatusName = "DELETE"; }
            else if (Status == 3)
            { StatusName = "EDIT GRAPHIC & ATT"; }
            else if (Status == 4)
            { StatusName = "EDIT ATT ONLY"; }

            return StatusName;
        }
    }
}

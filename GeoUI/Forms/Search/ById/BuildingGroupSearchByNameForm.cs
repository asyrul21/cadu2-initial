﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class BuildingGroupSearchByNameForm : SearchByNameForm
    {
        public BuildingGroupSearchByNameForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Building Group By Names");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            cbName2.Enabled = false;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASBuildingGroupName(cbName);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHBuildingGroupName(cbName);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPBuildingGroupName(cbName);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGBuildingGroupName(cbName);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKBuildingGroupName(cbName);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNBuildingGroupName(cbName);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVBuildingGroupName(cbName);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKBuildingGroupName(cbName);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGBuildingGroupName(cbName);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGBuildingGroupName(cbName);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void BuildingGroupSearchForm_Shown(object sender, EventArgs e)
        {
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            string name = StringUtils.CheckNull(cbName.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (!chkSearchAND.Checked)
            {
                Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                //query.Obj.Name = name;

                foreach (GBuildingGroup buildingGroup in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(buildingGroup);
                    items.Add(item);
                }
                lvResult.Items.AddRange(items.ToArray());
            }
            else
            {
                Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);

                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }

                foreach (GBuildingGroupAND buildingGroupAND in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(buildingGroupAND);
                    items.Add(item);
                }
                lvResult.Items.AddRange(items.ToArray());
            }

            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GBuildingGroup buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add(buildingGroup.Code);
            item.SubItems.Add(buildingGroup.Name);
            item.SubItems.Add((buildingGroup.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(buildingGroup.StreetTypeValue));
            item.SubItems.Add(buildingGroup.StreetName);
            item.SubItems.Add(buildingGroup.StreetName2);
            item.SubItems.Add(buildingGroup.Section);
            item.SubItems.Add(buildingGroup.Postcode);
            item.SubItems.Add(buildingGroup.City);
            item.SubItems.Add(buildingGroup.State);
            item.SubItems.Add(buildingGroup.CreatedBy);
            item.SubItems.Add(buildingGroup.DateCreated);
            item.SubItems.Add(buildingGroup.UpdatedBy);
            item.SubItems.Add(buildingGroup.DateUpdated);
            item.Tag = buildingGroup;
            return item;
        }

        private ListViewItem CreateItem(GBuildingGroupAND buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add(buildingGroup.Code);
            item.SubItems.Add(buildingGroup.Name);
            item.SubItems.Add((buildingGroup.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(buildingGroup.StreetTypeValue));
            item.SubItems.Add(buildingGroup.StreetName);
            item.SubItems.Add(buildingGroup.StreetName2);
            item.SubItems.Add(buildingGroup.Section);
            item.SubItems.Add(buildingGroup.Postcode);
            item.SubItems.Add(buildingGroup.City);
            item.SubItems.Add(buildingGroup.State);
            item.SubItems.Add(buildingGroup.CreatedBy);
            item.SubItems.Add(buildingGroup.DateCreated);
            item.SubItems.Add(buildingGroup.UpdatedBy);
            item.SubItems.Add(buildingGroup.DateUpdated);
            item.Tag = buildingGroup;
            return item;
        }
    }
}

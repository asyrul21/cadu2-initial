﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
//using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class LandmarkSearchByIdForm : SearchByIdForm
    {
        public LandmarkSearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Landmark by Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            LandmarkResultTitle();
        }

        private void LandmarkSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
        
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkPOI.Checked)
                {
                    if (!chkSearchAND.Checked)
                    {
                        foreach (GLandmark landmark in repo.GetByIds<GLandmark>(ids, true))
                        {
                            ListViewItem item = CreateItem(landmark);
                            items.Add(item);
                        }
                    }
                    else
                    {
                        // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                        foreach (GLandmark landmarks in repo.GetByIds<GLandmark>(ids, true))
                        {
                            Geomatic.Core.Features.GLandmarkAND landmarkAND = landmarks.GetLandmarkANDId();
                            if (landmarkAND != null)
                            {
                                GLandmarkAND LandmarkAND = repo.GetById<GLandmarkAND>(landmarkAND.OID);
                                ListViewItem item = CreateItem(LandmarkAND);
                                items.Add(item);
                            }
                        }
                    }
                }
                else
                {
                    // added by noraini ali - Sept 2019
                    int id = Int32.Parse(txtId.Text);
                    Query<GPoi> query = new Query<GPoi>(SegmentName);
                    query.Obj.ParentId = id;

                    foreach (GPoi poi in repo.Search(query, true, Limit))
                    {
                        ListViewItem item = CreateItem(poi);
                        items.Add(item);
                    }
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(landmark.Code);
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.StreetTypeValue));
            item.SubItems.Add(landmark.StreetName);
            item.SubItems.Add(landmark.StreetName2);
            item.SubItems.Add(landmark.Section);
            item.SubItems.Add(landmark.Postcode);
            item.SubItems.Add(landmark.City);
            item.SubItems.Add(landmark.State);
            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }

        private ListViewItem CreateItem(GLandmarkAND landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(landmark.Code);
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.StreetTypeValue));
            item.SubItems.Add(landmark.StreetName);
            item.SubItems.Add(landmark.StreetName2);
            item.SubItems.Add(landmark.Section);
            item.SubItems.Add(landmark.Postcode);
            item.SubItems.Add(landmark.City);
            item.SubItems.Add(landmark.State);
            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add(poi.NavigationStatusValue);
            item.SubItems.Add(poi.Description);
            item.SubItems.Add(poi.Url);
            item.Tag = poi;
            return item;
        }

        private void ChkPOI_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPOI.Checked)
            {
                lvResult.Clear();
                lvResult.Columns.Add("Id");
                lvResult.Columns.Add("Code");
                lvResult.Columns.Add("Name");
                lvResult.Columns.Add("Name2");
                lvResult.Columns.Add("Navi_Status");
                lvResult.Columns.Add("Description");
                lvResult.Columns.Add("Url");
            }
            else
            {
                LandmarkResultTitle();
            }
        }

        private void LandmarkResultTitle()
        {
            lvResult.Clear();
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }
    }
}

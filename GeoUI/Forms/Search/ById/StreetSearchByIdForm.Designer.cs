﻿namespace Geomatic.UI.Forms.Search
{
    partial class StreetSearchByIdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(470, 427);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(389, 427);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(713, 427);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(551, 427);
            // 
            // lvResult
            // 
            this.lvResult.Size = new System.Drawing.Size(776, 347);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(632, 427);
            // 
            // StreetSearchByIdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 462);
            this.Name = "StreetSearchByIdForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
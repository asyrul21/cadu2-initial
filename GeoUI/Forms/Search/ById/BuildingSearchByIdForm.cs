﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMultiStorey1 = Geomatic.Core.Rows.GMultiStorey;

namespace Geomatic.UI.Forms.Search
{
    public partial class BuildingSearchByIdForm : SearchByIdForm
    {

        public BuildingSearchByIdForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Building by Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            BuildingResultTitle();
        }

        private void BuildingSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkPOI.Checked && !chkMultiStorey.Checked)
                {
                    if (!chkSearchAND.Checked)
                    {
                        foreach (GBuilding building in repo.GetByIds<GBuilding>(ids, true))
                        {
                            ListViewItem item = CreateItem(building);
                            items.Add(item);
                        }
                    }
                    else
                    {
                        // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                        foreach (GBuilding buildings in repo.GetByIds<GBuilding>(ids, true))
                        {
                            Geomatic.Core.Features.GBuildingAND buildingAND = buildings.GetBuildingANDId();
                            if (buildingAND != null)
                            { 
                                GBuildingAND BuildingAND = repo.GetById<GBuildingAND>(buildingAND.OID);
                                ListViewItem item = CreateItem(BuildingAND);
                                items.Add(item);
                            }
                        }
                    }
                }
                else
                {
                    if (chkPOI.Checked)
                    {
                        // added by noraini ali - Sept 2019
                        int id = Int32.Parse(txtId.Text);
                        Query<GPoi> query = new Query<GPoi>(SegmentName);
                        query.Obj.ParentId = id;

                        foreach (GPoi poi in repo.Search(query, true, Limit))
                        {
                            ListViewItem item = CreateItem(poi);
                            items.Add(item);
                        }
                    }
                    else
                    {
                        // noraini ali - Jun 2020 - Display list for Multistorey and can save list in Excel
                        int id = Int32.Parse(txtId.Text);
                        //Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
                        //query.Obj.BuildingId = id;
                        //
                        //foreach (GMultiStorey multistorey in repo.Search(query, true, Limit))
                        //{
                        //    ListViewItem item = CreateItem(multistorey);
                        //    items.Add(item);
                        //}

                        GBuilding _building = repo.GetById<GBuilding>(id);
                        foreach (GMultiStorey1 multistorey in _building.GetMultiStories(true))
                        {
                            ListViewItem item = CreateItem(multistorey);
                            items.Add(item);
                        }
                    }
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add((building.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(building.BuildGrpName);
            item.SubItems.Add(StringUtils.TrimSpaces(building.PropertyTypeValue));
            item.SubItems.Add(building.Lot);
            item.SubItems.Add(building.House);
            item.SubItems.Add(StringUtils.TrimSpaces(building.StreetTypeValue));
            item.SubItems.Add(building.StreetName);
            item.SubItems.Add(building.StreetName2);
            item.SubItems.Add(building.Section);
            item.SubItems.Add(building.Postcode);
            item.SubItems.Add(building.City);
            item.SubItems.Add(building.State);
            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private ListViewItem CreateItem(GBuildingAND building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add((building.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(building.BuildGrpName);
            item.SubItems.Add(StringUtils.TrimSpaces(building.PropertyTypeValue));
            item.SubItems.Add(building.Lot);
            item.SubItems.Add(building.House);
            item.SubItems.Add(StringUtils.TrimSpaces(building.StreetTypeValue));
            item.SubItems.Add(building.StreetName);
            item.SubItems.Add(building.StreetName2);
            item.SubItems.Add(building.Section);
            item.SubItems.Add(building.Postcode);
            item.SubItems.Add(building.City);
            item.SubItems.Add(building.State);
            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private ListViewItem CreateItem(GPoi poi)
        {
            ListViewItem item = new ListViewItem();
            item.Text = poi.OID.ToString();
            item.SubItems.Add(poi.Code);
            item.SubItems.Add(poi.Name);
            item.SubItems.Add(poi.Name2);
            item.SubItems.Add(poi.NavigationStatusValue);
            item.SubItems.Add(poi.Description);
            item.SubItems.Add(poi.Url);
            item.Tag = poi;
            return item;
        }
        private ListViewItem CreateItem(GMultiStorey1 multiStorey)
        {
            ListViewItem item = new ListViewItem();
            item.Text = multiStorey.OID.ToString();
            item.SubItems.Add(multiStorey.Floor);
            item.SubItems.Add(multiStorey.Apartment);
            item.SubItems.Add(multiStorey.BuildingId.ToString());
            item.SubItems.Add(multiStorey.CreatedBy);
            item.SubItems.Add(multiStorey.DateCreated);
            item.SubItems.Add(multiStorey.UpdatedBy);
            item.SubItems.Add(multiStorey.DateUpdated);
            item.SubItems.Add(GetnameAndStatus(multiStorey.UpdateStatus));
            item.Tag = multiStorey;
            return item;
        }

        private ListViewItem CreateItem(GMultiStorey multiStorey)
        {
            ListViewItem item = new ListViewItem();
            item.Text = multiStorey.OID.ToString();
            item.SubItems.Add(multiStorey.Floor);
            item.SubItems.Add(multiStorey.Apartment);
            item.SubItems.Add(multiStorey.BuildingName);
            item.SubItems.Add(multiStorey.BuildingName2);
            item.SubItems.Add(StringUtils.TrimSpaces(multiStorey.StreetTypeValue));
            item.SubItems.Add(multiStorey.StreetName);
            item.SubItems.Add(multiStorey.StreetName2);
            item.SubItems.Add(multiStorey.Section);
            item.SubItems.Add(multiStorey.Postcode);
            item.SubItems.Add(multiStorey.City);
            item.SubItems.Add(multiStorey.State);
            item.SubItems.Add(multiStorey.UpdatedBy);
            item.SubItems.Add(multiStorey.DateUpdated);
            item.Tag = multiStorey;
            return item;
        }

        // added by noraini - Aug 2019
        private void ChkPOI_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPOI.Checked)
            {
                chkMultiStorey.Checked = false;

                lvResult.Clear();
                lvResult.Columns.Add("Id");
                lvResult.Columns.Add("Code");
                lvResult.Columns.Add("Name");
                lvResult.Columns.Add("Name2");
                lvResult.Columns.Add("Navi_Status");
                lvResult.Columns.Add("Description");
                lvResult.Columns.Add("Url");
            }
            else
            {
                BuildingResultTitle();
            }
        }

        private void ChkMultiStorey_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMultiStorey.Checked)
            {
                chkPOI.Checked = false;

                lvResult.Clear();
                lvResult.Columns.Add("Id");
                lvResult.Columns.Add("Floor");
                lvResult.Columns.Add("Apartment");
                lvResult.Columns.Add("Building Id");
                //lvResult.Columns.Add("Building Name2");
                //lvResult.Columns.Add("Street Type");
                //lvResult.Columns.Add("Street Name");
                //lvResult.Columns.Add("Street Name2");
                //lvResult.Columns.Add("Section");
                //lvResult.Columns.Add("Postcode");
                //lvResult.Columns.Add("City");
                //lvResult.Columns.Add("State");
                lvResult.Columns.Add("Created By");
                lvResult.Columns.Add("Date Created");
                lvResult.Columns.Add("Updated By");
                lvResult.Columns.Add("Date Updated");
                lvResult.Columns.Add("Status");
            }
            else
            {
                BuildingResultTitle();
            }
        }

        private void BuildingResultTitle()
        {
            lvResult.Clear();
            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("B.Group Name");
            lvResult.Columns.Add("Property Type");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private string GetnameAndStatus(int? Status)
        {
            string StatusName = "";

            if (Status == 1)
            { StatusName = "NEW"; }
            else if (Status == 2)
            { StatusName = "DELETE"; }
            else if (Status == 3)
            { StatusName = "EDIT GRAPHIC & ATT"; }
            else if (Status == 4)
            { StatusName = "EDIT ATT ONLY"; }

            return StatusName;
        }
    }
}

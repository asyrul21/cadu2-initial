﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class BuildingSearchByNameForm : SearchByNameForm
    {
        public BuildingSearchByNameForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Building By Names");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("B.Group Name");
            lvResult.Columns.Add("Property Type");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASBuildingName(cbName);
                    ComboBoxUtils.PopulateASBuildingName2(cbName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHBuildingName(cbName);
                    ComboBoxUtils.PopulateJHBuildingName2(cbName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPBuildingName(cbName);
                    ComboBoxUtils.PopulateJPBuildingName2(cbName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGBuildingName(cbName);
                    ComboBoxUtils.PopulateKGBuildingName2(cbName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKBuildingName(cbName);
                    ComboBoxUtils.PopulateKKBuildingName2(cbName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNBuildingName(cbName);
                    ComboBoxUtils.PopulateKNBuildingName2(cbName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVBuildingName(cbName);
                    ComboBoxUtils.PopulateKVBuildingName2(cbName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKBuildingName(cbName);
                    ComboBoxUtils.PopulateMKBuildingName2(cbName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGBuildingName(cbName);
                    ComboBoxUtils.PopulatePGBuildingName2(cbName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGBuildingName(cbName);
                    ComboBoxUtils.PopulateTGBuildingName2(cbName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void BuildingSearchForm_Shown(object sender, EventArgs e)
        {
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (!chkSearchAND.Checked)
            {
                Query<GBuilding> query = new Query<GBuilding>(SegmentName);
                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                query.Obj.Name2 = name2;

                foreach (GBuilding building in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(building);
                    items.Add(item);
                }

                lvResult.Items.AddRange(items.ToArray());
            }
            else
            {
                Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                query.Obj.Name2 = name2;

                foreach (GBuildingAND buildingAND in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(buildingAND);
                    items.Add(item);
                }
                
                lvResult.Items.AddRange(items.ToArray());
            }

            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add((building.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(building.BuildGrpName);
            item.SubItems.Add(StringUtils.TrimSpaces(building.PropertyTypeValue));
            item.SubItems.Add(building.Lot);
            item.SubItems.Add(building.House);
            item.SubItems.Add(StringUtils.TrimSpaces(building.StreetTypeValue));
            item.SubItems.Add(building.StreetName);
            item.SubItems.Add(building.StreetName2);
            item.SubItems.Add(building.Section);
            item.SubItems.Add(building.Postcode);
            item.SubItems.Add(building.City);
            item.SubItems.Add(building.State);
            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private ListViewItem CreateItem(GBuildingAND building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add((building.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(building.BuildGrpName);
            item.SubItems.Add(StringUtils.TrimSpaces(building.PropertyTypeValue));
            item.SubItems.Add(building.Lot);
            item.SubItems.Add(building.House);
            item.SubItems.Add(StringUtils.TrimSpaces(building.StreetTypeValue));
            item.SubItems.Add(building.StreetName);
            item.SubItems.Add(building.StreetName2);
            item.SubItems.Add(building.Section);
            item.SubItems.Add(building.Postcode);
            item.SubItems.Add(building.City);
            item.SubItems.Add(building.State);
            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }
    }
}

﻿namespace Geomatic.UI.Forms.Search
{
    partial class SearchByNameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblName2;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.chkSearchAND = new System.Windows.Forms.CheckBox();
            lblName = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(470, 427);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(389, 427);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(713, 427);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(551, 427);
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(10, 83);
            this.lvResult.Size = new System.Drawing.Size(776, 338);
            this.lvResult.SelectedIndexChanged += new System.EventHandler(this.lvResult_SelectedIndexChanged);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(632, 427);
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(54, 8);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(38, 13);
            lblName.TabIndex = 0;
            lblName.Text = "Name:";
            lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(349, 8);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(47, 13);
            lblName2.TabIndex = 2;
            lblName2.Text = "Name 2:";
            lblName2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel.Controls.Add(this.cbName, 0, 0);
            this.tableLayoutPanel.Controls.Add(lblName, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.cbName2, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.chkSearchAND, 4, 0);
            this.tableLayoutPanel.Controls.Add(lblName2, 2, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(10, 38);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(770, 30);
            this.tableLayoutPanel.TabIndex = 11;
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(98, 4);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(236, 21);
            this.cbName.Sorted = true;
            this.cbName.TabIndex = 24;
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Location = new System.Drawing.Point(402, 4);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(236, 21);
            this.cbName2.TabIndex = 25;
            // 
            // chkSearchAND
            // 
            this.chkSearchAND.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSearchAND.AutoSize = true;
            this.chkSearchAND.Location = new System.Drawing.Point(644, 6);
            this.chkSearchAND.Name = "chkSearchAND";
            this.chkSearchAND.Size = new System.Drawing.Size(123, 17);
            this.chkSearchAND.TabIndex = 26;
            this.chkSearchAND.Text = "AND";
            this.chkSearchAND.UseVisualStyleBackColor = true;
            this.chkSearchAND.Visible = false;
            // 
            // SearchByNameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "SearchByNameForm";
            this.Text = "SearchByNameForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected Controls.UpperComboBox cbName;
        protected Controls.UpperComboBox cbName2;
        public System.Windows.Forms.CheckBox chkSearchAND;
    }
}
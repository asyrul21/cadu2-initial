﻿using Geomatic.Core;
//using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.UI.Forms.Search
{
    public partial class SearchWorkAreaForm : SearchByIdForm
    {

        public SearchWorkAreaForm(SegmentName segmentName)
           : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Work Area By Id");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            // Added By Noraini Ali Mac 2020 - CADU 2 AND
            if (Session.User.GetGroup().Name == "SUPERVISOR")
            { ComboBoxUtils.PopulateWorkAreaId(workareaId); }
            else { ComboBoxUtils.PopulateWorkAreaByUser(workareaId); }
            // end   

            lvResult.Columns.Add("WA Id");
            lvResult.Columns.Add("Segment");
            lvResult.Columns.Add("Work Order No");
            lvResult.Columns.Add("User Id");
            lvResult.Columns.Add("Complete Flag");
            lvResult.Columns.Add("Description");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Date Updated");
        }

         private void SearchWorkAreaForm_Shown(object sender, EventArgs e)
         {
             if (!workareaId.Focused)
                 workareaId.Focus();
         }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {               
            int? id = StringUtils.CheckInt(workareaId.Text);
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (id.HasValue)
            {
                GWorkArea work_area = repo.GetById<GWorkArea>(id.Value);
                if (work_area != null)
                {
                    ListViewItem item = CreateItem(work_area);
                    items.Add(item);
                }
                else
                {
                    if (Session.User.GetGroup().Name == "SUPERVISOR" && CountDanglingFeature(id)>0)
                    {
                        using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                        {
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                            .SetDefaultButton(MessageBoxDefaultButton.Button2)
                            .SetCaption("No Work Area Found!")
                            .SetText("But found {0} dangling feature without work area. \nUnassociate all feature with following Area ID?", CountDanglingFeature(id));

                            if (confirmBox.Show() == DialogResult.Yes)
                            {
                                RemoveDanglingFeature(id); //remove feature with non existance area id.
                            }
                        }
                    }
                }

               
            }
            else
            {
                Query<GWorkArea> query = new Query<GWorkArea>();

                // Added by Noraini Ali Mac 2020 - CADU 2 AND 
                if (Session.User.GetGroup().Name == "SUPERVISOR")
                {
                    //query.Obj.flag = "1";
                }
                else
                {
                    //query.Obj.user_id = Session.User.Name.ToString();
                    // noraini ali - Mei 2021 -  query upper and lower case username.
                    string LowerUsername = Session.User.Name.ToLower();
                    string UpperUsername = Session.User.Name.ToUpper();
                    query.AddClause(() => query.Obj.user_id, "IN (" + "'" + LowerUsername + "','" + UpperUsername + "')");
                }
                // End

                foreach (GWorkArea work_area in repo.Search(query, true))
                {
                    ListViewItem item = CreateItem(work_area);
                    items.Add(item);
                }
            } 
            lvResult.Items.AddRange(items.ToArray());

            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GWorkArea work_area)
        {
            ListViewItem item = new ListViewItem();
            item.Text = work_area.OID.ToString();
            item.SubItems.Add(work_area.WorkAreaSegmentName);
            item.SubItems.Add(work_area.Wo_no);
            item.SubItems.Add(work_area.user_id);
            if (work_area.flag == null)
            {
                item.SubItems.Add("New");
            }
            else if (work_area.flag == "0")
            {
                item.SubItems.Add("Open");
            }
            else if (work_area.flag == "1")
            { 
                item.SubItems.Add("Closed");
            }
            else if (work_area.flag == "2")
            {
                item.SubItems.Add("Verifying");
            }
            item.SubItems.Add(work_area.Wo_desc);
            // noraini ali -  display normal format for Date Created & Updated
            string dd = work_area.DateCreated1.ToString();
            DateTime NormalFormat = DisplayNormalDateTimeFormat(dd);
            item.SubItems.Add(NormalFormat.ToString("yyyyMMdd"));

            dd = work_area.DateUpdated1.ToString();
            NormalFormat = DisplayNormalDateTimeFormat(dd);
            item.SubItems.Add(NormalFormat.ToString("yyyyMMdd"));

            //item.SubItems.Add(String.Format("{0:d}", work_area.DateCreated1));
            //item.SubItems.Add(String.Format("{0:d}", work_area.DateUpdated1));

            item.Tag = work_area;
            return item;
        }

        private DateTime DisplayNormalDateTimeFormat(string dd)
        {
            DateTime NormalFormatDate;
            string[] formats = {"MM/dd/yyyy hh:mm:ss tt", "MM/dd/yyyy hh:mm:ss", "MM/dd/yyyy h:mm:ss", "MM/dd/yyyy h:mm:ss tt",
                                "MM/d/yyyy hh:mm:ss tt", "MM/d/yyyy hh:mm:ss", "MM/d/yyyy h:mm:ss", "MM/d/yyyy h:mm:ss tt",
                                "dd/MM/yyyy hh:mm:ss tt", "dd/MM/yyyy hh:mm:ss", "dd/MM/yyyy h:mm:ss", "dd/MM/yyyy h:mm:ss tt",
                                "MM/dd/yy hh:mm:ss tt", "MM/dd/yy hh:mm:ss", "MM/dd/yy h:mm:ss", "MM/dd/yy h:mm:ss tt",
                                "MM/d/yyyy hh:mm:ss tt", "MM/d/yy hh:mm:ss", "MM/d/yy h:mm:ss", "MM/d/yy h:mm:ss tt",
                                "dd/MM/yy hh:mm:ss tt", "dd/MM/yy hh:mm:ss", "dd/MM/yy h:mm:ss", "dd/MM/yy h:mm:ss tt",
                                "dd/MM/yyyy HH:mm:ss", "MM/dd/yyyy HH:mm:ss", "d/d/yyyy HH:mm:ss", "dd/MM/yyy HH:mm:ss",
                                "M/dd/yyyy hh:mm:ss tt", "M/dd/yyyy hh:mm:ss", "M/dd/yyyy h:mm:ss", "M/dd/yyyy h:mm:ss tt",
                                "d/MM/yyyy hh:mm:ss tt", "d/MM/yyyy hh:mm:ss", "d/MM/yyyy h:mm:ss", "d/MM/yyyy h:mm:ss tt",
                                "d/M/yyyy hh:mm:ss tt", "d/M/yyyy hh:mm:ss", "d/M/yyyy h:mm:ss", "d/M/yyyy h:mm:ss tt",
                                "M/d/yyyy hh:mm:ss tt", "M/d/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", "M/d/yyyy h:mm:ss tt",
                                "M/dd/yy hh:mm:ss tt", "M/dd/yy hh:mm:ss", "M/dd/yy h:mm:ss", "M/dd/yy h:mm:ss tt",
                                "d/MM/yy hh:mm:ss tt", "d/MM/yy hh:mm:ss", "d/MM/yy h:mm:ss", "d/MM/yy h:mm:ss tt",
                                "d/M/yy hh:mm:ss tt", "d/M/yy hh:mm:ss", "d/M/yy h:mm:ss", "d/M/yy h:mm:ss tt",
                                "dd-MMM-yyyy hh:mm:ss tt", "dd-MMM-yyyy hh:mm:ss", "dd-MMM-yyyy h:mm:ss tt",
                                "dd-MMM-yyyy hh:mm tt", "dd-MMM-yyyy hh:mm", "dd-MMM-yyyy h:mm tt",
                                "dd-MMM-yy hh:mm:ss tt", "dd-MMM-yy hh:mm:ss", "dd-MMM-yy h:mm:ss tt",
                                "dd-MMM-yy hh:mm tt", "dd-MMM-yy hh:mm", "dd-MMM-yy h:mm tt",
                                "dd-MM-yyyy", "yyyyMMdd", "MMddyyyy", "ddMMyyyy", "yyMMdd", "MMddyy", "ddMMyy",
                                "yyyyddMM","yyyy-dd-MM","yyyy-MM-dd", "yyyyMMdd" };

            DateTime.TryParseExact(dd, formats,
            System.Globalization.CultureInfo.InvariantCulture,
            System.Globalization.DateTimeStyles.None, out NormalFormatDate);
            return NormalFormatDate;
        }

        private int CountDanglingFeature(int? AreaId)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            //Search features with the area ID
            Query<GBuilding> queryBuilding = new Query<GBuilding>(SegmentName);
            queryBuilding.AddClause(() => queryBuilding.Obj.AreaId, "IN (" + AreaId + ")");

            Query<GLandmark> queryLandmark = new Query<GLandmark>(SegmentName);
            queryLandmark.AddClause(() => queryLandmark.Obj.AreaId, "IN (" + AreaId + ")");

            Query<GBuildingGroup> queryBuildingGroup = new Query<GBuildingGroup>(SegmentName);
            queryBuildingGroup.AddClause(() => queryBuildingGroup.Obj.AreaId, "IN (" + AreaId + ")");

            Query<GProperty> queryProperty = new Query<GProperty>(SegmentName);
            queryProperty.AddClause(() => queryProperty.Obj.AreaId, "IN (" + AreaId + ")");

            Query<GJunction> queryJunction = new Query<GJunction>(SegmentName);
            queryJunction.AddClause(() => queryJunction.Obj.AreaId, "IN (" + AreaId + ")");

            Query<GStreet> queryStreet = new Query<GStreet>(SegmentName);
            queryStreet.AddClause(() => queryStreet.Obj.AreaId, "IN (" + AreaId + ")");

            int totalCount = repo.Count(queryBuilding) + repo.Count(queryLandmark) + repo.Count(queryBuildingGroup) + repo.Count(queryProperty) + repo.Count(queryJunction) + repo.Count(queryStreet); 
            return totalCount;

        }

        private void RemoveDanglingFeature(int? AreaId)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        //Search features with the area ID
                        Query<GBuilding> queryBuilding = new Query<GBuilding>(SegmentName);
                        queryBuilding.AddClause(() => queryBuilding.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GBuilding>  buildings = repo.Search(queryBuilding);

                        Query<GLandmark> queryLandmark = new Query<GLandmark>(SegmentName);
                        queryLandmark.AddClause(() => queryLandmark.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GLandmark> landmarks = repo.Search(queryLandmark);

                        Query<GBuildingGroup> queryBuildingGroup = new Query<GBuildingGroup>(SegmentName);
                        queryBuildingGroup.AddClause(() => queryBuildingGroup.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GBuildingGroup> buildingGroups = repo.Search(queryBuildingGroup);

                        Query<GProperty> queryProperty = new Query<GProperty>(SegmentName);
                        queryProperty.AddClause(() => queryProperty.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GProperty> properties = repo.Search(queryProperty);

                        Query<GJunction> queryJunction = new Query<GJunction>(SegmentName);
                        queryJunction.AddClause(() => queryJunction.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GJunction> junctions = repo.Search(queryJunction);

                        Query<GStreet> queryStreet = new Query<GStreet>(SegmentName);
                        queryStreet.AddClause(() => queryStreet.Obj.AreaId, "IN (" + AreaId + ")");
                        IEnumerable<GStreet> streets = repo.Search(queryStreet);

                        int totalCount = CountDanglingFeature(AreaId);
                        int currentCount = 0;

                        using (ProgressForm form = new ProgressForm())
                        {
                            form.setMinimun(0);
                            form.setMaximum(totalCount);
                            form.Show();

                            //Remove the area ID from feature
                            foreach (GBuilding item in buildings)
                            {
                                form.Text = "Updating Building";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }

                            foreach (GLandmark item in landmarks)
                            {
                                form.Text = "Updating Landmark";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }

                            foreach (GBuildingGroup item in buildingGroups)
                            {
                                form.Text = "Updating Building Group";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }

                            foreach (GProperty item in properties)
                            {
                                form.Text = "Updating Property";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }

                            foreach (GJunction item in junctions)
                            {
                                form.Text = "Updating Junction";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }

                            foreach (GStreet item in streets)
                            {
                                form.Text = "Updating Street";
                                item.AreaId = null;
                                repo.UpdateRemainStatus(item, false);
                                currentCount = currentCount + 1;
                                form.setValue(currentCount);
                            }
                        }
                    });
                }
                
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                repo.AbortTransaction();
            }
        }
    }
}

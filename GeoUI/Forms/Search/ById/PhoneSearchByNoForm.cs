﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class PhoneSearchByNoForm : SearchByIdForm
    {
        public PhoneSearchByNoForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Phone by No");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Phone Number");
            lvResult.Columns.Add("Property ID");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Apartment");
            lvResult.Columns.Add("Floor");
            lvResult.Columns.Add("Building");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            //btnShow.Visible = false;
            //btnShowAll.Visible = false;
        }

        private void PoiSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtPhoneNo.Focused)
            {
                txtPhoneNo.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            string number = StringUtils.CheckNull(txtPhoneNo.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.Number = number;

            foreach (GPhone phone in repo.Search(query, true, Limit))
            {
                ListViewItem item = CreateItem(phone);
                items.Add(item);
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GPhone phone)
        {
            ListViewItem item = new ListViewItem();
            item.Text = phone.Number;
            item.SubItems.Add(phone.PropertyId.ToString());
            item.SubItems.Add(phone.Lot);
            item.SubItems.Add(phone.House);
            item.SubItems.Add(phone.Apartment);
            item.SubItems.Add(phone.Floor);
            item.SubItems.Add(phone.Building);
            item.SubItems.Add(phone.StreetType);
            item.SubItems.Add(phone.StreetName);
            item.SubItems.Add(phone.Section);
            item.SubItems.Add(phone.Postcode);
            item.SubItems.Add(phone.City);
            item.SubItems.Add(phone.State);
            item.SubItems.Add(phone.CreatedBy);
            item.SubItems.Add(phone.DateCreated);
            item.SubItems.Add(phone.UpdatedBy);
            item.SubItems.Add(phone.DateUpdated);
            item.Tag = phone;
            return item;
        }

        // modified by asyrul
        protected override void BuildCollection(out List<IGFeature> collection)
        {
            collection = new List<IGFeature>();

            foreach (ListViewItem item in lvResult.SelectedItems)
            { 
                // added by noraini - 21/8/2019
                if (item.SubItems[1].Text == "0")
                    MessageBox.Show("No Geocode for this Phone Number");
                else
                // end
                { 
                    IGFeature feature = ((GPhone)item.Tag).GetProperty();
                    collection.Add(feature);
                }
            }
        }

        protected override void BuildAllCollection(out List<IGFeature> collection)
        {
            collection = new List<IGFeature>();

            foreach (ListViewItem item in lvResult.SelectedItems)
            {
                if (item.SubItems[1].Text != "0") // added by noraini - 21/8/2019
                {
                    IGFeature feature = ((GPhone)item.Tag).GetProperty();
                    collection.Add(feature);
                }
            }
        }
        // modified end
    }
}

﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class BuildingGroupSearchByIdForm : SearchByIdForm
    {
        public BuildingGroupSearchByIdForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Building Group by Id");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");
        }

        private void BuildingGroupSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                if (!chkSearchAND.Checked)
                {
                    foreach (GBuildingGroup buildingGroup in repo.GetByIds<GBuildingGroup>(ids, true))
                    {
                        ListViewItem item = CreateItem(buildingGroup);
                        items.Add(item);
                    }
                }
                else
                {
                    // noraini ali - Jul 2020 - Search AND by keyin ADM Id 
                    foreach (GBuildingGroup buildingGroups in repo.GetByIds<GBuildingGroup>(ids, true))
                    {
                        Geomatic.Core.Features.GBuildingGroupAND buildingGroupAND = buildingGroups.GetBuildingGroupANDId();
                        if (buildingGroupAND != null)
                        {
                            GBuildingGroupAND BuildingGroupAND = repo.GetById<GBuildingGroupAND>(buildingGroupAND.OID);
                            ListViewItem item = CreateItem(BuildingGroupAND);
                            items.Add(item);
                        }
                    }
                }
            }
            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GBuildingGroup buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add(buildingGroup.Code);
            item.SubItems.Add(buildingGroup.Name);
            item.SubItems.Add((buildingGroup.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(buildingGroup.StreetTypeValue));
            item.SubItems.Add(buildingGroup.StreetName);
            item.SubItems.Add(buildingGroup.StreetName2);
            item.SubItems.Add(buildingGroup.Section);
            item.SubItems.Add(buildingGroup.Postcode);
            item.SubItems.Add(buildingGroup.City);
            item.SubItems.Add(buildingGroup.State);
            item.SubItems.Add(buildingGroup.CreatedBy);
            item.SubItems.Add(buildingGroup.DateCreated);
            item.SubItems.Add(buildingGroup.UpdatedBy);
            item.SubItems.Add(buildingGroup.DateUpdated);
            item.Tag = buildingGroup;
            return item;
        }

        private ListViewItem CreateItem(GBuildingGroupAND buildingGroup)
        {
            ListViewItem item = new ListViewItem();
            item.Text = buildingGroup.OID.ToString();
            item.SubItems.Add(buildingGroup.Code);
            item.SubItems.Add(buildingGroup.Name);
            item.SubItems.Add((buildingGroup.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(buildingGroup.StreetTypeValue));
            item.SubItems.Add(buildingGroup.StreetName);
            item.SubItems.Add(buildingGroup.StreetName2);
            item.SubItems.Add(buildingGroup.Section);
            item.SubItems.Add(buildingGroup.Postcode);
            item.SubItems.Add(buildingGroup.City);
            item.SubItems.Add(buildingGroup.State);
            item.SubItems.Add(buildingGroup.CreatedBy);
            item.SubItems.Add(buildingGroup.DateCreated);
            item.SubItems.Add(buildingGroup.UpdatedBy);
            item.SubItems.Add(buildingGroup.DateUpdated);
            item.Tag = buildingGroup;
            return item;
        }

    }
}

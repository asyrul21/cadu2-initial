﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class LandmarkSearchByNameForm : SearchByNameForm
    {
        public LandmarkSearchByNameForm(SegmentName segmentName)
            :base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Landmark by Name");

            // noraini ali - June 2020 - CADU2 AND - Add button to search AND Feature
            if (Session.User.GetGroup().Name == "SUPERVISOR" || Session.User.GetGroup().Name == "AND")
            {
                chkSearchAND.Visible = true;
            }
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASLandmarkName(cbName);
                    ComboBoxUtils.PopulateASLandmarkName2(cbName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHLandmarkName(cbName);
                    ComboBoxUtils.PopulateJHLandmarkName2(cbName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPLandmarkName(cbName);
                    ComboBoxUtils.PopulateJPLandmarkName2(cbName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGLandmarkName(cbName);
                    ComboBoxUtils.PopulateKGLandmarkName2(cbName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKLandmarkName(cbName);
                    ComboBoxUtils.PopulateKKLandmarkName2(cbName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNLandmarkName(cbName);
                    ComboBoxUtils.PopulateKNLandmarkName2(cbName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVLandmarkName(cbName);
                    ComboBoxUtils.PopulateKVLandmarkName2(cbName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKLandmarkName(cbName);
                    ComboBoxUtils.PopulateMKLandmarkName2(cbName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGLandmarkName(cbName);
                    ComboBoxUtils.PopulatePGLandmarkName2(cbName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGLandmarkName(cbName);
                    ComboBoxUtils.PopulateTGLandmarkName2(cbName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void LandmarkSearchForm_Shown(object sender, EventArgs e)
        {
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);
           
            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (!chkSearchAND.Checked)
            {
                Query<GLandmark> query = new Query<GLandmark>(SegmentName);

                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                //query.Obj.Name = name;
                query.Obj.Name2 = name2;

                foreach (GLandmark landmark in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(landmark);
                    items.Add(item);
                }

                lvResult.Items.AddRange(items.ToArray());
            }
            else
            {
                Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);

                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                //query.Obj.Name = name;
                query.Obj.Name2 = name2;

                foreach (GLandmarkAND landmarkAND in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(landmarkAND);
                    items.Add(item);
                }

                lvResult.Items.AddRange(items.ToArray());
            }

            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GLandmark landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(landmark.Code);
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.StreetTypeValue));
            item.SubItems.Add(landmark.StreetName);
            item.SubItems.Add(landmark.StreetName2);
            item.SubItems.Add(landmark.Section);
            item.SubItems.Add(landmark.Postcode);
            item.SubItems.Add(landmark.City);
            item.SubItems.Add(landmark.State);
            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }

        private ListViewItem CreateItem(GLandmarkAND landmark)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmark.OID.ToString();
            item.SubItems.Add(landmark.Code);
            item.SubItems.Add(landmark.Name);
            item.SubItems.Add(landmark.Name2);
            item.SubItems.Add((landmark.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(StringUtils.TrimSpaces(landmark.StreetTypeValue));
            item.SubItems.Add(landmark.StreetName);
            item.SubItems.Add(landmark.StreetName2);
            item.SubItems.Add(landmark.Section);
            item.SubItems.Add(landmark.Postcode);
            item.SubItems.Add(landmark.City);
            item.SubItems.Add(landmark.State);
            item.SubItems.Add(landmark.CreatedBy);
            item.SubItems.Add(landmark.DateCreated);
            item.SubItems.Add(landmark.UpdatedBy);
            item.SubItems.Add(landmark.DateUpdated);
            item.Tag = landmark;
            return item;
        }
    }
}

﻿namespace Geomatic.UI.Forms.Search
{
    partial class SearchWorkAreaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblId = new System.Windows.Forms.Label();
            this.workareaId = new Geomatic.UI.Controls.UpperComboBox();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Enabled = false;
            this.btnShowAll.Location = new System.Drawing.Point(317, 427);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(236, 427);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(560, 427);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(398, 427);
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 58);
            this.lvResult.Size = new System.Drawing.Size(623, 339);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(479, 427);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.36434F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.63566F));
            this.tableLayoutPanel2.Controls.Add(this.lblId, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.workareaId, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(15, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(516, 28);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // lblId
            // 
            this.lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(88, 7);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(76, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "Work Area Id :";
            // 
            // workareaId
            // 
            this.workareaId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.workareaId.DropDownWidth = 236;
            this.workareaId.FormattingEnabled = true;
            this.workareaId.Location = new System.Drawing.Point(170, 3);
            this.workareaId.Name = "workareaId";
            this.workareaId.Size = new System.Drawing.Size(343, 21);
            this.workareaId.TabIndex = 1;
            this.workareaId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // SearchWorkAreaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 462);
            this.Controls.Add(this.tableLayoutPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SearchWorkAreaForm";
            this.Text = "SearchWorkAreaForm";
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel2, 0);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblId;
        private Controls.UpperComboBox workareaId;
    }
}
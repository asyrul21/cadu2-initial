﻿namespace Geomatic.UI.Forms.Search
{
    partial class PhoneSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblPropertyId;
            System.Windows.Forms.Label lblPhoneNo;
            System.Windows.Forms.Label lblStreetType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblBuilding;
            System.Windows.Forms.Label lblLot;
            System.Windows.Forms.Label lblHouse;
            System.Windows.Forms.Label lblApartment;
            System.Windows.Forms.Label lblFloor;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtFloor = new System.Windows.Forms.TextBox();
            this.txtApartment = new System.Windows.Forms.TextBox();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.cbBuilding = new Geomatic.UI.Controls.UpperComboBox();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.cbStreetName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNumber = new Geomatic.UI.Controls.UpperComboBox();
            this.txtPropertyId = new System.Windows.Forms.TextBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            lblPropertyId = new System.Windows.Forms.Label();
            lblPhoneNo = new System.Windows.Forms.Label();
            lblStreetType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblBuilding = new System.Windows.Forms.Label();
            lblLot = new System.Windows.Forms.Label();
            lblHouse = new System.Windows.Forms.Label();
            lblApartment = new System.Windows.Forms.Label();
            lblFloor = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 216);
            this.lvResult.Size = new System.Drawing.Size(610, 205);
            // 
            // lblPropertyId
            // 
            lblPropertyId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPropertyId.AutoSize = true;
            lblPropertyId.Location = new System.Drawing.Point(361, 7);
            lblPropertyId.Name = "lblPropertyId";
            lblPropertyId.Size = new System.Drawing.Size(61, 13);
            lblPropertyId.TabIndex = 2;
            lblPropertyId.Text = "Property Id:";
            // 
            // lblPhoneNo
            // 
            lblPhoneNo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPhoneNo.AutoSize = true;
            lblPhoneNo.Location = new System.Drawing.Point(36, 7);
            lblPhoneNo.Name = "lblPhoneNo";
            lblPhoneNo.Size = new System.Drawing.Size(81, 13);
            lblPhoneNo.TabIndex = 0;
            lblPhoneNo.Text = "Phone Number:";
            // 
            // lblStreetType
            // 
            lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetType.AutoSize = true;
            lblStreetType.Location = new System.Drawing.Point(52, 119);
            lblStreetType.Name = "lblStreetType";
            lblStreetType.Size = new System.Drawing.Size(65, 13);
            lblStreetType.TabIndex = 14;
            lblStreetType.Text = "Street Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(353, 119);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(69, 13);
            lblName.TabIndex = 16;
            lblName.Text = "Street Name:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 147);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 20;
            lblPostcode.Text = "Postcode:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 176);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 24;
            lblState.Text = "State:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 147);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 18;
            lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 176);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 22;
            lblCity.Text = "City:";
            // 
            // lblBuilding
            // 
            lblBuilding.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblBuilding.AutoSize = true;
            lblBuilding.Location = new System.Drawing.Point(70, 91);
            lblBuilding.Name = "lblBuilding";
            lblBuilding.Size = new System.Drawing.Size(47, 13);
            lblBuilding.TabIndex = 12;
            lblBuilding.Text = "Building:";
            // 
            // lblLot
            // 
            lblLot.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblLot.AutoSize = true;
            lblLot.Location = new System.Drawing.Point(92, 35);
            lblLot.Name = "lblLot";
            lblLot.Size = new System.Drawing.Size(25, 13);
            lblLot.TabIndex = 4;
            lblLot.Text = "Lot:";
            // 
            // lblHouse
            // 
            lblHouse.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblHouse.AutoSize = true;
            lblHouse.Location = new System.Drawing.Point(381, 35);
            lblHouse.Name = "lblHouse";
            lblHouse.Size = new System.Drawing.Size(41, 13);
            lblHouse.TabIndex = 6;
            lblHouse.Text = "House:";
            // 
            // lblApartment
            // 
            lblApartment.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblApartment.AutoSize = true;
            lblApartment.Location = new System.Drawing.Point(59, 63);
            lblApartment.Name = "lblApartment";
            lblApartment.Size = new System.Drawing.Size(58, 13);
            lblApartment.TabIndex = 8;
            lblApartment.Text = "Apartment:";
            // 
            // lblFloor
            // 
            lblFloor.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblFloor.AutoSize = true;
            lblFloor.Location = new System.Drawing.Point(389, 63);
            lblFloor.Name = "lblFloor";
            lblFloor.Size = new System.Drawing.Size(33, 13);
            lblFloor.TabIndex = 10;
            lblFloor.Text = "Floor:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(lblFloor, 2, 2);
            this.tableLayoutPanel1.Controls.Add(lblApartment, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtFloor, 3, 2);
            this.tableLayoutPanel1.Controls.Add(lblHouse, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtApartment, 1, 2);
            this.tableLayoutPanel1.Controls.Add(lblLot, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtHouse, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbBuilding, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtLot, 1, 1);
            this.tableLayoutPanel1.Controls.Add(lblBuilding, 0, 3);
            this.tableLayoutPanel1.Controls.Add(lblCity, 0, 6);
            this.tableLayoutPanel1.Controls.Add(lblState, 2, 6);
            this.tableLayoutPanel1.Controls.Add(lblSection, 0, 5);
            this.tableLayoutPanel1.Controls.Add(lblPostcode, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetName, 3, 4);
            this.tableLayoutPanel1.Controls.Add(lblName, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbStreetType, 1, 4);
            this.tableLayoutPanel1.Controls.Add(lblStreetType, 0, 4);
            this.tableLayoutPanel1.Controls.Add(lblPhoneNo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbNumber, 1, 0);
            this.tableLayoutPanel1.Controls.Add(lblPropertyId, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtPropertyId, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbPostcode, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbSection, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbState, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbCity, 1, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(610, 198);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // txtFloor
            // 
            this.txtFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloor.Location = new System.Drawing.Point(428, 60);
            this.txtFloor.Name = "txtFloor";
            this.txtFloor.Size = new System.Drawing.Size(179, 20);
            this.txtFloor.TabIndex = 11;
            this.txtFloor.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtApartment
            // 
            this.txtApartment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApartment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApartment.Location = new System.Drawing.Point(123, 60);
            this.txtApartment.Name = "txtApartment";
            this.txtApartment.Size = new System.Drawing.Size(179, 20);
            this.txtApartment.TabIndex = 9;
            this.txtApartment.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(428, 32);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(179, 20);
            this.txtHouse.TabIndex = 7;
            this.txtHouse.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbBuilding
            // 
            this.cbBuilding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBuilding.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbBuilding.FormattingEnabled = true;
            this.cbBuilding.Location = new System.Drawing.Point(123, 87);
            this.cbBuilding.Name = "cbBuilding";
            this.cbBuilding.Size = new System.Drawing.Size(179, 21);
            this.cbBuilding.TabIndex = 13;
            this.cbBuilding.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(123, 32);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(179, 20);
            this.txtLot.TabIndex = 5;
            this.txtLot.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName
            // 
            this.cbStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName.FormattingEnabled = true;
            this.cbStreetName.Location = new System.Drawing.Point(428, 115);
            this.cbStreetName.Name = "cbStreetName";
            this.cbStreetName.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName.TabIndex = 17;
            this.cbStreetName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Location = new System.Drawing.Point(123, 115);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(179, 21);
            this.cbStreetType.TabIndex = 15;
            this.cbStreetType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbNumber
            // 
            this.cbNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbNumber.FormattingEnabled = true;
            this.cbNumber.Location = new System.Drawing.Point(123, 3);
            this.cbNumber.MaxLength = 12;
            this.cbNumber.Name = "cbNumber";
            this.cbNumber.Size = new System.Drawing.Size(179, 21);
            this.cbNumber.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cbNumber, "First 3 Digit - Area Code, Next 8 Digit - Phone No\r\nExample : 00312345678 @ 00401" +
        "234567 @ 08200123456");
            this.cbNumber.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtPropertyId
            // 
            this.txtPropertyId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPropertyId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPropertyId.Location = new System.Drawing.Point(428, 4);
            this.txtPropertyId.Name = "txtPropertyId";
            this.txtPropertyId.Size = new System.Drawing.Size(179, 20);
            this.txtPropertyId.TabIndex = 3;
            this.txtPropertyId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 143);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 25;
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 143);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 26;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 172);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 27;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 172);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 28;
            // 
            // PhoneSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PhoneSearchForm";
            this.Text = "PhoneSearchForm";
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtPropertyId;
        private Geomatic.UI.Controls.UpperComboBox cbNumber;
        private Controls.UpperComboBox cbStreetName;
        private Controls.UpperComboBox cbStreetType;
        private Controls.UpperComboBox cbBuilding;
        private System.Windows.Forms.TextBox txtFloor;
        private System.Windows.Forms.TextBox txtApartment;
        private System.Windows.Forms.TextBox txtHouse;
        private System.Windows.Forms.TextBox txtLot;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbState;
        private Controls.UpperComboBox cbCity;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Search
{
    public class ShowMapEventArgs : EventArgs
    {
        public List<IGFeature> Collection { private set; get; }

        public ShowMapEventArgs(List<IGFeature> collection)
        {
            Collection = collection;
        }
    }
}

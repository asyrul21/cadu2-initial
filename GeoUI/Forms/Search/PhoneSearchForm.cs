﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class PhoneSearchForm : SearchForm
    {
        public PhoneSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Phone");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Phone Number");
            lvResult.Columns.Add("Property ID");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Apartment");
            lvResult.Columns.Add("Floor");
            lvResult.Columns.Add("Building");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);
            ComboBoxUtils.PopulateStreetType(cbStreetType);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void PoiSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtPropertyId.Focused)
            {
                txtPropertyId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            int? propertyId = StringUtils.CheckInt(txtPropertyId.Text);
            string number = StringUtils.CheckNull(cbNumber.Text);
            string lot = StringUtils.CheckNull(txtLot.Text);
            string house = StringUtils.CheckNull(txtHouse.Text);
            string apartment = StringUtils.CheckNull(txtApartment.Text);
            string floor = StringUtils.CheckNull(txtFloor.Text);
            string building = StringUtils.CheckNull(cbBuilding.Text);
            string streetType = StringUtils.CheckNull(cbStreetType.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);

            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.PropertyId = propertyId;
            query.Obj.Number = number;
            query.Obj.Lot = lot;
            query.Obj.House = house;
            query.Obj.Apartment = apartment;
            query.Obj.Floor = floor;
            query.Obj.Building = building;
            query.Obj.StreetType = streetType;
            query.Obj.StreetName = streetName;
            query.Obj.Section = section;
            query.Obj.Postcode = postcode;
            query.Obj.City = city;
            query.Obj.State = state;

            foreach (GPhone phone in repo.Search(query, true, Limit))
            {
                ListViewItem item = CreateItem(phone);
                items.Add(item);
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GPhone phone)
        {
            ListViewItem item = new ListViewItem();
            item.Text = phone.Number;
            item.SubItems.Add(phone.PropertyId.ToString());
            item.SubItems.Add(phone.Lot);
            item.SubItems.Add(phone.House);
            item.SubItems.Add(phone.Apartment);
            item.SubItems.Add(phone.Floor);
            item.SubItems.Add(phone.Building);
            item.SubItems.Add(phone.StreetType);
            item.SubItems.Add(phone.StreetName);
            item.SubItems.Add(phone.Section);
            item.SubItems.Add(phone.Postcode);
            item.SubItems.Add(phone.City);
            item.SubItems.Add(phone.State);
            item.SubItems.Add(phone.CreatedBy);
            item.SubItems.Add(phone.DateCreated);
            item.SubItems.Add(phone.UpdatedBy);
            item.SubItems.Add(phone.DateUpdated);
            item.Tag = phone;
            return item;
        }
    }
}

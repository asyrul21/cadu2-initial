﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class PropertySearchForm : SearchFeatureForm
    {
        private int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        private int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbType.SelectedItem).Value.Code; }
        }

        private int? SelectedStreetType
        {
            get { return (cbStreetType.SelectedItem == null) ? (int?)null : ((GStreetType)cbStreetType.SelectedItem).Code; }
        }

        public PropertySearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Property");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Year Install");
            lvResult.Columns.Add("Number of Floor");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulatePropertyType(cbType);
            ComboBoxUtils.PopulateStreetType(cbStreetType);

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void PropertySearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string lot = StringUtils.CheckNull(txtLot.Text);
            string house = StringUtils.CheckNull(txtHouse.Text);
            int? yearInstall = StringUtils.CheckInt(txtYearInstall.Text);
            int? numOfFloor = StringUtils.CheckInt(txtNumOfFloor.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);
            string streetName2 = StringUtils.CheckNull(cbStreetName2.Text);

            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GProperty property in repo.GetByIds<GProperty>(ids, true))
                {
                    ListViewItem item = CreateItem(property);
                    items.Add(item);
                }
            }
            else
            {
                Query<GProperty> query = new Query<GProperty>(SegmentName);
                query.Obj.ConstructionStatus = SelectedConstructionStatus;
                query.Obj.Type = SelectedType;
                query.Obj.Lot = lot;
                query.Obj.House = house;
                query.Obj.YearInstall = yearInstall;
                query.Obj.NumberOfFloor = numOfFloor;
                query.Obj.StreetType = SelectedStreetType;
                query.Obj.StreetName = streetName;
                query.Obj.StreetName2 = streetName2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.State = state;

                foreach (GProperty property in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(property);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GProperty property)
        {
            ListViewItem item = new ListViewItem();
            item.Text = property.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(property.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(property.TypeValue));
            item.SubItems.Add(property.Lot);
            item.SubItems.Add(property.House);
            item.SubItems.Add((property.YearInstall.HasValue) ? property.YearInstall.Value.ToString() : string.Empty);
            item.SubItems.Add((property.NumberOfFloor.HasValue) ? property.NumberOfFloor.Value.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(property.StreetTypeValue));
            item.SubItems.Add(property.StreetName);
            item.SubItems.Add(property.StreetName2);
            item.SubItems.Add(property.Section);
            item.SubItems.Add(property.Postcode);
            item.SubItems.Add(property.City);
            item.SubItems.Add(property.State);
            item.SubItems.Add(property.CreatedBy);
            item.SubItems.Add(property.DateCreated);
            item.SubItems.Add(property.UpdatedBy);
            item.SubItems.Add(property.DateUpdated);
            item.Tag = property;
            return item;
        }
    }
}

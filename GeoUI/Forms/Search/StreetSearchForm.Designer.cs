﻿namespace Geomatic.UI.Forms.Search
{
    partial class StreetSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblConstructionStatus;
            System.Windows.Forms.Label lblStatus;
            System.Windows.Forms.Label lblNetworkClass;
            System.Windows.Forms.Label lblClass;
            System.Windows.Forms.Label lblCategory;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblType;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblNaviStatus;
            System.Windows.Forms.Label lblFilterLevel;
            System.Windows.Forms.Label lblSpeedLimit;
            System.Windows.Forms.Label lblDirection;
            System.Windows.Forms.Label lblSubCity;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbSubCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbFilterLevel = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNetworkClass = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCategory = new Geomatic.UI.Controls.UpperComboBox();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbClass = new Geomatic.UI.Controls.UpperComboBox();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbDirection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.txtSpeedLimit = new System.Windows.Forms.TextBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            lblId = new System.Windows.Forms.Label();
            lblConstructionStatus = new System.Windows.Forms.Label();
            lblStatus = new System.Windows.Forms.Label();
            lblNetworkClass = new System.Windows.Forms.Label();
            lblClass = new System.Windows.Forms.Label();
            lblCategory = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblType = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            lblNaviStatus = new System.Windows.Forms.Label();
            lblFilterLevel = new System.Windows.Forms.Label();
            lblSpeedLimit = new System.Windows.Forms.Label();
            lblDirection = new System.Windows.Forms.Label();
            lblSubCity = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 298);
            this.lvResult.Size = new System.Drawing.Size(610, 123);
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblConstructionStatus
            // 
            lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblConstructionStatus.AutoSize = true;
            lblConstructionStatus.Location = new System.Drawing.Point(320, 7);
            lblConstructionStatus.Name = "lblConstructionStatus";
            lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            lblConstructionStatus.TabIndex = 2;
            lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblStatus
            // 
            lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStatus.AutoSize = true;
            lblStatus.Location = new System.Drawing.Point(77, 35);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(40, 13);
            lblStatus.TabIndex = 4;
            lblStatus.Text = "Status:";
            // 
            // lblNetworkClass
            // 
            lblNetworkClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNetworkClass.AutoSize = true;
            lblNetworkClass.Location = new System.Drawing.Point(39, 63);
            lblNetworkClass.Name = "lblNetworkClass";
            lblNetworkClass.Size = new System.Drawing.Size(78, 13);
            lblNetworkClass.TabIndex = 8;
            lblNetworkClass.Text = "Network Class:";
            // 
            // lblClass
            // 
            lblClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblClass.AutoSize = true;
            lblClass.Location = new System.Drawing.Point(387, 63);
            lblClass.Name = "lblClass";
            lblClass.Size = new System.Drawing.Size(35, 13);
            lblClass.TabIndex = 10;
            lblClass.Text = "Class:";
            // 
            // lblCategory
            // 
            lblCategory.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCategory.AutoSize = true;
            lblCategory.Location = new System.Drawing.Point(65, 91);
            lblCategory.Name = "lblCategory";
            lblCategory.Size = new System.Drawing.Size(52, 13);
            lblCategory.TabIndex = 12;
            lblCategory.Text = "Category:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(384, 147);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(38, 13);
            lblName.TabIndex = 22;
            lblName.Text = "Name:";
            // 
            // lblType
            // 
            lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblType.AutoSize = true;
            lblType.Location = new System.Drawing.Point(83, 147);
            lblType.Name = "lblType";
            lblType.Size = new System.Drawing.Size(34, 13);
            lblType.TabIndex = 20;
            lblType.Text = "Type:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 203);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 26;
            lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 231);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 30;
            lblCity.Text = "City:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 203);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 28;
            lblPostcode.Text = "Postcode:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 259);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 34;
            lblState.Text = "State:";
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(378, 175);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(44, 13);
            lblName2.TabIndex = 24;
            lblName2.Text = "Name2:";
            // 
            // lblNaviStatus
            // 
            lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNaviStatus.AutoSize = true;
            lblNaviStatus.Location = new System.Drawing.Point(328, 35);
            lblNaviStatus.Name = "lblNaviStatus";
            lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            lblNaviStatus.TabIndex = 6;
            lblNaviStatus.Text = "Navigation Status:";
            // 
            // lblFilterLevel
            // 
            lblFilterLevel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblFilterLevel.AutoSize = true;
            lblFilterLevel.Location = new System.Drawing.Point(361, 91);
            lblFilterLevel.Name = "lblFilterLevel";
            lblFilterLevel.Size = new System.Drawing.Size(61, 13);
            lblFilterLevel.TabIndex = 14;
            lblFilterLevel.Text = "Filter Level:";
            // 
            // lblSpeedLimit
            // 
            lblSpeedLimit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSpeedLimit.AutoSize = true;
            lblSpeedLimit.Location = new System.Drawing.Point(357, 119);
            lblSpeedLimit.Name = "lblSpeedLimit";
            lblSpeedLimit.Size = new System.Drawing.Size(65, 13);
            lblSpeedLimit.TabIndex = 18;
            lblSpeedLimit.Text = "Speed Limit:";
            // 
            // lblDirection
            // 
            lblDirection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblDirection.AutoSize = true;
            lblDirection.Location = new System.Drawing.Point(65, 119);
            lblDirection.Name = "lblDirection";
            lblDirection.Size = new System.Drawing.Size(52, 13);
            lblDirection.TabIndex = 16;
            lblDirection.Text = "Direction:";
            // 
            // lblSubCity
            // 
            lblSubCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSubCity.AutoSize = true;
            lblSubCity.Location = new System.Drawing.Point(68, 259);
            lblSubCity.Name = "lblSubCity";
            lblSubCity.Size = new System.Drawing.Size(49, 13);
            lblSubCity.TabIndex = 32;
            lblSubCity.Text = "Sub City:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.cbSubCity, 0, 9);
            this.tableLayoutPanel.Controls.Add(lblSubCity, 0, 9);
            this.tableLayoutPanel.Controls.Add(this.cbFilterLevel, 3, 3);
            this.tableLayoutPanel.Controls.Add(lblFilterLevel, 2, 3);
            this.tableLayoutPanel.Controls.Add(lblNaviStatus, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 1);
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(lblConstructionStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(lblStatus, 0, 1);
            this.tableLayoutPanel.Controls.Add(lblNetworkClass, 0, 2);
            this.tableLayoutPanel.Controls.Add(lblClass, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.cbStatus, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.cbNetworkClass, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.cbCategory, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.cbConstructionStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.cbClass, 3, 2);
            this.tableLayoutPanel.Controls.Add(lblCategory, 0, 3);
            this.tableLayoutPanel.Controls.Add(lblType, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.cbType, 1, 5);
            this.tableLayoutPanel.Controls.Add(lblDirection, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.cbDirection, 1, 4);
            this.tableLayoutPanel.Controls.Add(lblPostcode, 2, 7);
            this.tableLayoutPanel.Controls.Add(lblName2, 2, 6);
            this.tableLayoutPanel.Controls.Add(this.cbName2, 3, 6);
            this.tableLayoutPanel.Controls.Add(this.cbName, 3, 5);
            this.tableLayoutPanel.Controls.Add(lblName, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.txtSpeedLimit, 3, 4);
            this.tableLayoutPanel.Controls.Add(lblSpeedLimit, 2, 4);
            this.tableLayoutPanel.Controls.Add(lblCity, 0, 8);
            this.tableLayoutPanel.Controls.Add(lblSection, 0, 7);
            this.tableLayoutPanel.Controls.Add(lblState, 2, 9);
            this.tableLayoutPanel.Controls.Add(this.cbSection, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.cbPostcode, 3, 7);
            this.tableLayoutPanel.Controls.Add(this.cbCity, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.cbState, 3, 9);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(610, 280);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // cbSubCity
            // 
            this.cbSubCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSubCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSubCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSubCity.FormattingEnabled = true;
            this.cbSubCity.Location = new System.Drawing.Point(123, 255);
            this.cbSubCity.Name = "cbSubCity";
            this.cbSubCity.Size = new System.Drawing.Size(179, 21);
            this.cbSubCity.TabIndex = 39;
            // 
            // cbFilterLevel
            // 
            this.cbFilterLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilterLevel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbFilterLevel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFilterLevel.FormattingEnabled = true;
            this.cbFilterLevel.Location = new System.Drawing.Point(428, 87);
            this.cbFilterLevel.Name = "cbFilterLevel";
            this.cbFilterLevel.Size = new System.Drawing.Size(179, 21);
            this.cbFilterLevel.TabIndex = 15;
            this.cbFilterLevel.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbNaviStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(428, 31);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(179, 21);
            this.cbNaviStatus.TabIndex = 7;
            this.cbNaviStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStatus
            // 
            this.cbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(123, 31);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(179, 21);
            this.cbStatus.TabIndex = 5;
            this.cbStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbNetworkClass
            // 
            this.cbNetworkClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNetworkClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbNetworkClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNetworkClass.FormattingEnabled = true;
            this.cbNetworkClass.Location = new System.Drawing.Point(123, 59);
            this.cbNetworkClass.Name = "cbNetworkClass";
            this.cbNetworkClass.Size = new System.Drawing.Size(179, 21);
            this.cbNetworkClass.TabIndex = 9;
            this.cbNetworkClass.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(123, 87);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(179, 21);
            this.cbCategory.TabIndex = 13;
            this.cbCategory.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbConstructionStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(428, 3);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(179, 21);
            this.cbConstructionStatus.TabIndex = 3;
            this.cbConstructionStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbClass
            // 
            this.cbClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(428, 59);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(179, 21);
            this.cbClass.TabIndex = 11;
            this.cbClass.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(123, 143);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(179, 21);
            this.cbType.TabIndex = 21;
            this.cbType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbDirection
            // 
            this.cbDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDirection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbDirection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbDirection.FormattingEnabled = true;
            this.cbDirection.Location = new System.Drawing.Point(123, 115);
            this.cbDirection.Name = "cbDirection";
            this.cbDirection.Size = new System.Drawing.Size(179, 21);
            this.cbDirection.TabIndex = 17;
            this.cbDirection.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Location = new System.Drawing.Point(428, 171);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(179, 21);
            this.cbName2.TabIndex = 25;
            this.cbName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(428, 143);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(179, 21);
            this.cbName.TabIndex = 23;
            this.cbName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtSpeedLimit
            // 
            this.txtSpeedLimit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSpeedLimit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpeedLimit.Location = new System.Drawing.Point(428, 116);
            this.txtSpeedLimit.Name = "txtSpeedLimit";
            this.txtSpeedLimit.Size = new System.Drawing.Size(179, 20);
            this.txtSpeedLimit.TabIndex = 19;
            this.txtSpeedLimit.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 199);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 35;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 199);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 36;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 227);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 37;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 255);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 38;
            // 
            // StreetSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "StreetSearchForm";
            this.Text = "StreetSearchForm";
            this.Shown += new System.EventHandler(this.StreetSearchForm_Shown);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox txtId;
        private Geomatic.UI.Controls.UpperComboBox cbStatus;
        private Geomatic.UI.Controls.UpperComboBox cbNetworkClass;
        private Geomatic.UI.Controls.UpperComboBox cbCategory;
        private Geomatic.UI.Controls.UpperComboBox cbType;
        private Geomatic.UI.Controls.UpperComboBox cbConstructionStatus;
        private Geomatic.UI.Controls.UpperComboBox cbClass;
        private Geomatic.UI.Controls.UpperComboBox cbName;
        private Geomatic.UI.Controls.UpperComboBox cbName2;
        private Geomatic.UI.Controls.UpperComboBox cbNaviStatus;
        private Geomatic.UI.Controls.UpperComboBox cbFilterLevel;
        private System.Windows.Forms.TextBox txtSpeedLimit;
        private Geomatic.UI.Controls.UpperComboBox cbDirection;
        private Controls.UpperComboBox cbSubCity;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbState;
    }
}
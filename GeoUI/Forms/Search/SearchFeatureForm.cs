﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Features;

namespace Geomatic.UI.Forms.Search
{
    public partial class SearchFeatureForm : SearchForm
    {
        public event EventHandler<ShowMapEventArgs> ShowMap;
        public event EventHandler<ShowMapEventArgs> ShowMap2;

        public SearchFeatureForm()
            : this(SegmentName.None)
        {
        }

        public SearchFeatureForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
        }

        private void lvResult_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnShow;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                return;
            }

            List<IGFeature> collection;

            using (new WaitCursor())
            {
                BuildCollection(out collection);
            }

            OnShowMap(new ShowMapEventArgs(collection));
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            if (lvResult.Items.Count == 0)
            {
                return;
            }

            List<IGFeature> collection;

            using (new WaitCursor())
            {
                BuildAllCollection(out collection);
            }

            OnShowMap(new ShowMapEventArgs(collection));
        }

        protected virtual void BuildCollection(out List<IGFeature> collection)
        {
            collection = new List<IGFeature>();

            foreach (ListViewItem item in lvResult.SelectedItems)
            {
                IGFeature feature = (IGFeature)item.Tag;
                collection.Add(feature);
            }
        }

        protected virtual void BuildAllCollection(out List<IGFeature> collection)
        {
            collection = new List<IGFeature>();

            foreach (ListViewItem item in lvResult.Items)
            {
                IGFeature feature = (IGFeature)item.Tag;
                collection.Add(feature);
            }
        }

        protected void OnShowMap(ShowMapEventArgs e)
        {
            if (ShowMap != null)
            {
                ShowMap(this, e);
                
                Close();
            }
        }

        protected void OnShowMap(ShowMapEventArgs e, bool status)
        {
            if (ShowMap2 != null)
            {
                ShowMap2(this, e);
            }
        }
    }
}

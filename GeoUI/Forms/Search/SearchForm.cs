﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;
using System.Diagnostics;
using System.IO;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Forms.Search
{
    public partial class SearchForm : BaseForm
    {
        protected int? Limit
        {
            get
            {
                return StringUtils.CheckInt(txtLimit.Text);
            }
        }

        protected SegmentName SegmentName { set; get; }

        public SearchForm()
            : this(SegmentName.None)
        {
        }

        public SearchForm(SegmentName segmentName)
        {
            InitializeComponent();
            SegmentName = segmentName;
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            if (DesignMode) { return; }
            using (new WaitCursor())
            {
                Form_Load();
            }
        }

        protected virtual void Form_Load()
        {
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            using (new WaitCursor())
            {
                string fileName = saveFileDialog.FileName;

                if (!File.Exists(fileName))
                {
                    FileStream fileStream = File.Create(fileName);
                    fileStream.Dispose();
                }

                using (StreamWriter streamWriter = new StreamWriter(fileName, false))
                {
                    List<string> columns = new List<string>();
                    foreach (ColumnHeader columnHeader in lvResult.Columns)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("\"");
                        sb.Append(columnHeader.Text);
                        sb.Append("\"");
                        columns.Add(sb.ToString());
                    }
                    streamWriter.WriteLine(string.Join(",", columns.ToArray()));

                    foreach (ListViewItem item in lvResult.Items)
                    {
                        List<string> subItems = new List<string>();
                        foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("\"");
                            sb.Append(subItem.Text);
                            sb.Append("\"");
                            subItems.Add(sb.ToString());
                        }
                        streamWriter.WriteLine(string.Join(",", subItems.ToArray()));
                    }
                }
            }
        }
    
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    lvResult.Sorting = SortOrder.None;
                    lvResult.BeginUpdate();
                    lvResult.Items.Clear();
                    DoSearch();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (lvResult.Items.Count > 0)
                {
                    lvResult.Items[0].Selected = true;
                    if (!lvResult.Focused)
                    {
                        lvResult.Focus();
                    }
                }
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected virtual void DoSearch()
        {
            throw new NotImplementedException("Method not implemented.");
        }
    }
}

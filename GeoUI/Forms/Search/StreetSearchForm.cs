﻿using Geomatic.Core;
using Geomatic.Core.Features;
//using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class StreetSearchForm : SearchFeatureForm
    {
        private int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        private int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        private int? SelectedStatus
        {
            get { return (cbStatus.SelectedItem == null) ? (int?)null : ((GStreetStatus)cbStatus.SelectedItem).Code; }
        }

        private int? SelectedNetworkClass
        {
            get { return (cbNetworkClass.SelectedItem == null) ? (int?)null : ((GStreetNetworkClass)cbNetworkClass.SelectedItem).Code; }
        }

        private int? SelectedClass
        {
            get { return (cbClass.SelectedItem == null) ? (int?)null : ((GStreetClass)cbClass.SelectedItem).Code; }
        }

        private int? SelectedCategory
        {
            get { return (cbCategory.SelectedItem == null) ? (int?)null : ((GStreetCategory)cbCategory.SelectedItem).Code; }
        }

        private int? SelectedFilterLevel
        {
            get { return (cbFilterLevel.SelectedItem == null) ? (int?)null : ((GStreetFilterLevel)cbFilterLevel.SelectedItem).Code; }
        }

        private int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((GStreetType)cbType.SelectedItem).Code; }
        }

        private int? SelectedDirection
        {
            get { return (cbDirection.SelectedItem == null) ? (int?)null : ((GStreetDirection)cbDirection.SelectedItem).Code; }
        }

        public StreetSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Street");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Type");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("Sub City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Network Class");
            lvResult.Columns.Add("Class");
            lvResult.Columns.Add("Category");
            lvResult.Columns.Add("Filter Level");
            lvResult.Columns.Add("Speed Limit");
            lvResult.Columns.Add("Direction");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateStreetStatus(cbStatus);
            ComboBoxUtils.PopulateStreetNetworkClass(cbNetworkClass);
            ComboBoxUtils.PopulateStreetClass(cbClass);
            ComboBoxUtils.PopulateStreetCategory(cbCategory);
            ComboBoxUtils.PopulateStreetFilterLevel(cbFilterLevel);
            ComboBoxUtils.PopulateStreetType(cbType);
            ComboBoxUtils.PopulateStreetDirection(cbDirection);
            ComboBoxUtils.PopulateState(cbState);

    //added by asyrul
            //if (SegmentName == SegmentName.KV)
            //{
            //    ComboBoxUtils.PopulateKVStreetName(cbName);
            //    ComboBoxUtils.PopulateKVStreetName2(cbName2);
            //}
            switch (SegmentName)
            {              
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbName);
                    ComboBoxUtils.PopulateASStreetName2(cbName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);
                    ComboBoxUtils.PopulateASSubCity(cbSubCity);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbName);
                    ComboBoxUtils.PopulateJHStreetName2(cbName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    ComboBoxUtils.PopulateJHSubCity(cbSubCity);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbName);
                    ComboBoxUtils.PopulateJPStreetName2(cbName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    ComboBoxUtils.PopulateJPSubCity(cbSubCity);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbName);
                    ComboBoxUtils.PopulateKGStreetName2(cbName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    ComboBoxUtils.PopulateKGSubCity(cbSubCity);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbName);
                    ComboBoxUtils.PopulateKKStreetName2(cbName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    ComboBoxUtils.PopulateKKSubCity(cbSubCity);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbName);
                    ComboBoxUtils.PopulateKNStreetName2(cbName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    ComboBoxUtils.PopulateKNSubCity(cbSubCity);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbName);
                    ComboBoxUtils.PopulateKVStreetName2(cbName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    ComboBoxUtils.PopulateKVSubCity(cbSubCity);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbName);
                    ComboBoxUtils.PopulateMKStreetName2(cbName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    ComboBoxUtils.PopulateMKSubCity(cbSubCity);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbName);
                    ComboBoxUtils.PopulatePGStreetName2(cbName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    ComboBoxUtils.PopulatePGSubCity(cbSubCity);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbName);
                    ComboBoxUtils.PopulateTGStreetName2(cbName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    ComboBoxUtils.PopulateTGSubCity(cbSubCity);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            // not used anymore
            //cbName.SelectedIndexChanged += new System.EventHandler(cbName_SelectedIndexChanged);
            //cbName.TextChanged += new System.EventHandler(cbName2_reset);

        }

        // not used
        // meant to filter name2 based on name1
        //private void cbName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    cbName2.Items.Clear();
        //    ComboBoxUtils.RepopulateStreetName2(cbName2, cbName.Text);
        //}

        //private void cbName2_reset(object sender, EventArgs e)
        //{
        //    if (cbName.Text == "")
        //    {
        //        cbName2.Items.Clear();
        //        ComboBoxUtils.PopulateStreetName2(cbName2);
        //    }
        //}
    //added end
        private void StreetSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            //MessageBox.Show("Name field: " + cbName.Text);
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            int? speedLimit = StringUtils.CheckInt(txtSpeedLimit.Text);
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);

            //altered by asyrul - text boxes are replaced with a dropdown combo box
            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string subCity = StringUtils.CheckNull(txtSubCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);
            
            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            string subCity = StringUtils.CheckNull(cbSubCity.Text);
            //string state = StringUtils.CheckNull(cbState.Selecte);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            //altered end

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GStreet street in repo.GetByIds<GStreet>(ids))
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }
            }
            else
            {
                Query<GStreet> query = new Query<GStreet>(SegmentName);
                query.Obj.ConstructionStatus = SelectedConstructionStatus;
                query.Obj.Status = SelectedStatus;
                query.Obj.NavigationStatus = SelectedNavigationStatus;
                query.Obj.NetworkClass = SelectedNetworkClass;
                query.Obj.Class = SelectedClass;
                query.Obj.Category = SelectedCategory;
                query.Obj.FilterLevel = SelectedFilterLevel;
                query.Obj.Direction = SelectedDirection;
                query.Obj.SpeedLimit = speedLimit;
                query.Obj.Type = SelectedType;
                query.Obj.Name = name;
                query.Obj.Name2 = name2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.SubCity = subCity;
                query.Obj.State = state;

                //added by asyrul
                //query.Obj.
                //added end


                foreach (GStreet street in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GStreet street)
        {
            ListViewItem item = new ListViewItem();
            item.Text = street.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(street.ConstructionStatusValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add(street.Name);
            item.SubItems.Add(street.Name2);
            item.SubItems.Add(street.Section);
            item.SubItems.Add(street.Postcode);
            item.SubItems.Add(street.City);
            item.SubItems.Add(street.SubCity);
            item.SubItems.Add(street.State);
            item.SubItems.Add(StringUtils.TrimSpaces(street.StatusValue));
            item.SubItems.Add(street.NavigationStatusValue);
            item.SubItems.Add(StringUtils.TrimSpaces(street.NetworkClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.ClassValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.CategoryValue));
            item.SubItems.Add(StringUtils.TrimSpaces(street.FilterLevelValue));
            item.SubItems.Add((street.SpeedLimit.HasValue) ? street.SpeedLimit.ToString() : string.Empty);
            item.SubItems.Add(StringUtils.TrimSpaces(street.DirectionValue));
            item.SubItems.Add(street.CreatedBy);
            item.SubItems.Add(street.DateCreated);
            item.SubItems.Add(street.UpdatedBy);
            item.SubItems.Add(street.DateUpdated);
            item.Tag = street;
            return item;
        }

    }
}

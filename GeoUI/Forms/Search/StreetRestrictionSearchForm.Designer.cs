﻿namespace Geomatic.UI.Forms.Search
{
    partial class StreetRestrictionSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblNaviStatus;
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblStatus;
            System.Windows.Forms.Label lblJunctionId;
            System.Windows.Forms.Label lblStartId;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtEndId1 = new System.Windows.Forms.TextBox();
            this.txtStartId = new System.Windows.Forms.TextBox();
            this.txtJunctionId = new System.Windows.Forms.TextBox();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.txtEndId2 = new System.Windows.Forms.TextBox();
            this.txtEndId3 = new System.Windows.Forms.TextBox();
            this.txtEndId4 = new System.Windows.Forms.TextBox();
            this.txtEndId5 = new System.Windows.Forms.TextBox();
            this.txtEndId6 = new System.Windows.Forms.TextBox();
            this.txtEndId7 = new System.Windows.Forms.TextBox();
            this.txtEndId8 = new System.Windows.Forms.TextBox();
            this.txtEndId9 = new System.Windows.Forms.TextBox();
            this.txtEndId10 = new System.Windows.Forms.TextBox();
            lblNaviStatus = new System.Windows.Forms.Label();
            lblId = new System.Windows.Forms.Label();
            lblStatus = new System.Windows.Forms.Label();
            lblJunctionId = new System.Windows.Forms.Label();
            lblStartId = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 242);
            this.lvResult.Size = new System.Drawing.Size(610, 179);
            // 
            // lblNaviStatus
            // 
            lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNaviStatus.AutoSize = true;
            lblNaviStatus.Location = new System.Drawing.Point(328, 35);
            lblNaviStatus.Name = "lblNaviStatus";
            lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            lblNaviStatus.TabIndex = 4;
            lblNaviStatus.Text = "Navigation Status:";
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblStatus
            // 
            lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStatus.AutoSize = true;
            lblStatus.Location = new System.Drawing.Point(77, 35);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(40, 13);
            lblStatus.TabIndex = 2;
            lblStatus.Text = "Status:";
            // 
            // lblJunctionId
            // 
            lblJunctionId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblJunctionId.AutoSize = true;
            lblJunctionId.Location = new System.Drawing.Point(55, 63);
            lblJunctionId.Name = "lblJunctionId";
            lblJunctionId.Size = new System.Drawing.Size(62, 13);
            lblJunctionId.TabIndex = 6;
            lblJunctionId.Text = "Junction Id:";
            // 
            // lblStartId
            // 
            lblStartId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStartId.AutoSize = true;
            lblStartId.Location = new System.Drawing.Point(378, 63);
            lblStartId.Name = "lblStartId";
            lblStartId.Size = new System.Drawing.Size(44, 13);
            lblStartId.TabIndex = 8;
            lblStartId.Text = "Start Id:";
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(67, 91);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(50, 13);
            label1.TabIndex = 10;
            label1.Text = "End Id 1:";
            // 
            // label2
            // 
            label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(372, 91);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(50, 13);
            label2.TabIndex = 12;
            label2.Text = "End Id 2:";
            // 
            // label3
            // 
            label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(67, 119);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(50, 13);
            label3.TabIndex = 14;
            label3.Text = "End Id 3:";
            // 
            // label4
            // 
            label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(372, 119);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(50, 13);
            label4.TabIndex = 16;
            label4.Text = "End Id 4:";
            // 
            // label5
            // 
            label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(67, 147);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(50, 13);
            label5.TabIndex = 18;
            label5.Text = "End Id 5:";
            // 
            // label6
            // 
            label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(372, 147);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(50, 13);
            label6.TabIndex = 20;
            label6.Text = "End Id 6:";
            // 
            // label7
            // 
            label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(67, 175);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(50, 13);
            label7.TabIndex = 22;
            label7.Text = "End Id 7:";
            // 
            // label8
            // 
            label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(372, 175);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(50, 13);
            label8.TabIndex = 24;
            label8.Text = "End Id 8:";
            // 
            // label9
            // 
            label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(67, 203);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(50, 13);
            label9.TabIndex = 26;
            label9.Text = "End Id 9:";
            // 
            // label10
            // 
            label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(366, 203);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(56, 13);
            label10.TabIndex = 28;
            label10.Text = "End Id 10:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtEndId1, 1, 3);
            this.tableLayoutPanel.Controls.Add(label8, 2, 6);
            this.tableLayoutPanel.Controls.Add(label5, 0, 5);
            this.tableLayoutPanel.Controls.Add(label3, 0, 4);
            this.tableLayoutPanel.Controls.Add(label2, 2, 3);
            this.tableLayoutPanel.Controls.Add(label1, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.txtStartId, 3, 2);
            this.tableLayoutPanel.Controls.Add(lblStartId, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtJunctionId, 1, 2);
            this.tableLayoutPanel.Controls.Add(lblJunctionId, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 1);
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(lblNaviStatus, 2, 1);
            this.tableLayoutPanel.Controls.Add(lblStatus, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.cbStatus, 1, 1);
            this.tableLayoutPanel.Controls.Add(label4, 2, 4);
            this.tableLayoutPanel.Controls.Add(label6, 2, 5);
            this.tableLayoutPanel.Controls.Add(label7, 0, 6);
            this.tableLayoutPanel.Controls.Add(label9, 0, 7);
            this.tableLayoutPanel.Controls.Add(label10, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.txtEndId2, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.txtEndId3, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.txtEndId4, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.txtEndId5, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.txtEndId6, 3, 5);
            this.tableLayoutPanel.Controls.Add(this.txtEndId7, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.txtEndId8, 3, 6);
            this.tableLayoutPanel.Controls.Add(this.txtEndId9, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.txtEndId10, 3, 7);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 8;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(610, 224);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // txtEndId1
            // 
            this.txtEndId1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId1.Location = new System.Drawing.Point(123, 88);
            this.txtEndId1.Name = "txtEndId1";
            this.txtEndId1.Size = new System.Drawing.Size(179, 20);
            this.txtEndId1.TabIndex = 11;
            this.txtEndId1.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtStartId
            // 
            this.txtStartId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStartId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStartId.Location = new System.Drawing.Point(428, 60);
            this.txtStartId.Name = "txtStartId";
            this.txtStartId.Size = new System.Drawing.Size(179, 20);
            this.txtStartId.TabIndex = 9;
            this.txtStartId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtJunctionId
            // 
            this.txtJunctionId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtJunctionId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtJunctionId.Location = new System.Drawing.Point(123, 60);
            this.txtJunctionId.Name = "txtJunctionId";
            this.txtJunctionId.Size = new System.Drawing.Size(179, 20);
            this.txtJunctionId.TabIndex = 7;
            this.txtJunctionId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbNaviStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(428, 31);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(179, 21);
            this.cbNaviStatus.TabIndex = 5;
            this.cbNaviStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStatus
            // 
            this.cbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(123, 31);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(179, 21);
            this.cbStatus.TabIndex = 3;
            this.cbStatus.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId2
            // 
            this.txtEndId2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId2.Location = new System.Drawing.Point(428, 88);
            this.txtEndId2.Name = "txtEndId2";
            this.txtEndId2.Size = new System.Drawing.Size(179, 20);
            this.txtEndId2.TabIndex = 13;
            this.txtEndId2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId3
            // 
            this.txtEndId3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId3.Location = new System.Drawing.Point(123, 116);
            this.txtEndId3.Name = "txtEndId3";
            this.txtEndId3.Size = new System.Drawing.Size(179, 20);
            this.txtEndId3.TabIndex = 15;
            this.txtEndId3.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId4
            // 
            this.txtEndId4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId4.Location = new System.Drawing.Point(428, 116);
            this.txtEndId4.Name = "txtEndId4";
            this.txtEndId4.Size = new System.Drawing.Size(179, 20);
            this.txtEndId4.TabIndex = 17;
            this.txtEndId4.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId5
            // 
            this.txtEndId5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId5.Location = new System.Drawing.Point(123, 144);
            this.txtEndId5.Name = "txtEndId5";
            this.txtEndId5.Size = new System.Drawing.Size(179, 20);
            this.txtEndId5.TabIndex = 19;
            this.txtEndId5.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId6
            // 
            this.txtEndId6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId6.Location = new System.Drawing.Point(428, 144);
            this.txtEndId6.Name = "txtEndId6";
            this.txtEndId6.Size = new System.Drawing.Size(179, 20);
            this.txtEndId6.TabIndex = 21;
            this.txtEndId6.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId7
            // 
            this.txtEndId7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId7.Location = new System.Drawing.Point(123, 172);
            this.txtEndId7.Name = "txtEndId7";
            this.txtEndId7.Size = new System.Drawing.Size(179, 20);
            this.txtEndId7.TabIndex = 23;
            this.txtEndId7.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId8
            // 
            this.txtEndId8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId8.Location = new System.Drawing.Point(428, 172);
            this.txtEndId8.Name = "txtEndId8";
            this.txtEndId8.Size = new System.Drawing.Size(179, 20);
            this.txtEndId8.TabIndex = 25;
            this.txtEndId8.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId9
            // 
            this.txtEndId9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId9.Location = new System.Drawing.Point(123, 200);
            this.txtEndId9.Name = "txtEndId9";
            this.txtEndId9.Size = new System.Drawing.Size(179, 20);
            this.txtEndId9.TabIndex = 27;
            this.txtEndId9.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtEndId10
            // 
            this.txtEndId10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndId10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtEndId10.Location = new System.Drawing.Point(428, 200);
            this.txtEndId10.Name = "txtEndId10";
            this.txtEndId10.Size = new System.Drawing.Size(179, 20);
            this.txtEndId10.TabIndex = 29;
            this.txtEndId10.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // StreetRestrictionSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "StreetRestrictionSearchForm";
            this.Text = "StreetRestrictionSearchForm";
            this.Shown += new System.EventHandler(this.StreetRestrictionSearchForm_Shown);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private Geomatic.UI.Controls.UpperComboBox cbNaviStatus;
        private System.Windows.Forms.TextBox txtId;
        private Geomatic.UI.Controls.UpperComboBox cbStatus;
        private System.Windows.Forms.TextBox txtJunctionId;
        private System.Windows.Forms.TextBox txtStartId;
        private System.Windows.Forms.TextBox txtEndId1;
        private System.Windows.Forms.TextBox txtEndId2;
        private System.Windows.Forms.TextBox txtEndId3;
        private System.Windows.Forms.TextBox txtEndId4;
        private System.Windows.Forms.TextBox txtEndId5;
        private System.Windows.Forms.TextBox txtEndId6;
        private System.Windows.Forms.TextBox txtEndId7;
        private System.Windows.Forms.TextBox txtEndId8;
        private System.Windows.Forms.TextBox txtEndId9;
        private System.Windows.Forms.TextBox txtEndId10;
    }
}
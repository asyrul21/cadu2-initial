﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class LandmarkBoundarySearchForm : SearchFeatureForm
    {
        public LandmarkBoundarySearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Landmark Boundary");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
        }

        private void LandmarkBoundarySearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GLandmarkBoundary landmarkBoundary in repo.GetByIds<GLandmarkBoundary>(ids, true))
                {
                    ListViewItem item = CreateItem(landmarkBoundary);
                    items.Add(item);
                }
            }
            else
            {
                Query<GLandmarkBoundary> query = new Query<GLandmarkBoundary>(SegmentName);

                foreach (GLandmarkBoundary landmarkBoundary in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(landmarkBoundary);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GLandmarkBoundary landmarkBoundary)
        {
            ListViewItem item = new ListViewItem();
            item.Text = landmarkBoundary.OID.ToString();
            item.Tag = landmarkBoundary;
            return item;
        }
    }
}

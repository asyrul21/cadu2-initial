﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.UI.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using GMultiStorey = Geomatic.Core.Features.JoinedFeatures.GMultiStorey;
using IGFeature = Geomatic.Core.Features.IGFeature;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms.Search
{
    public partial class MultiStoreySearchForm : SearchFeatureForm
    {
        private int? SelectedStreetType
        {
            get { return (cbStreetType.SelectedItem == null) ? (int?)null : ((GStreetType)cbStreetType.SelectedItem).Code; }
        }

        public MultiStoreySearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search MultiStorey");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Floor");
            lvResult.Columns.Add("Apartment");
            lvResult.Columns.Add("Building Name");
            lvResult.Columns.Add("Building Name2");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateStreetType(cbStreetType);

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateASBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateASBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateJHBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateJHBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateJPBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateJPBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKGBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateKGBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKKBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateKKBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKNBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateKNBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    //ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKVBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateKVBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateMKBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateMKBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulatePGBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulatePGBuildingName2(cbBuildingName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateTGBuildingName(cbBuildingName);
                    ComboBoxUtils.PopulateTGBuildingName2(cbBuildingName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void MultiStoreySearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string floor = StringUtils.CheckNull(txtFloor.Text);
            string apartment = StringUtils.CheckNull(txtApartment.Text);
            string buildingName = StringUtils.CheckNull(cbBuildingName.Text);
            string buildingName2 = StringUtils.CheckNull(cbBuildingName2.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);
            string streetName2 = StringUtils.CheckNull(cbStreetName2.Text);

            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GMultiStorey multiStorey in repo.GetByIds<GMultiStorey>(ids, true))
                {
                    ListViewItem item = CreateItem(multiStorey);
                    items.Add(item);
                }
            }
            else
            {
                Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
                query.Obj.Floor = floor;
                query.Obj.Apartment = apartment;
                query.Obj.BuildingName = buildingName;
                query.Obj.BuildingName2 = buildingName2;
                query.Obj.StreetType = SelectedStreetType;
                query.Obj.StreetName = streetName;
                query.Obj.StreetName2 = streetName2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.State = state;
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");

                foreach (GMultiStorey multiStorey in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(multiStorey);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GMultiStorey multiStorey)
        {
            ListViewItem item = new ListViewItem();
            item.Text = multiStorey.OID.ToString();
            item.SubItems.Add(multiStorey.Floor);
            item.SubItems.Add(multiStorey.Apartment);
            item.SubItems.Add(multiStorey.BuildingName);
            item.SubItems.Add(multiStorey.BuildingName2);
            item.SubItems.Add(StringUtils.TrimSpaces(multiStorey.StreetTypeValue));
            item.SubItems.Add(multiStorey.StreetName);
            item.SubItems.Add(multiStorey.StreetName2);
            item.SubItems.Add(multiStorey.Section);
            item.SubItems.Add(multiStorey.Postcode);
            item.SubItems.Add(multiStorey.City);
            item.SubItems.Add(multiStorey.State);
            item.SubItems.Add(multiStorey.UpdatedBy);
            item.SubItems.Add(multiStorey.DateUpdated);
            item.Tag = multiStorey;
            return item;
        }
    }
}

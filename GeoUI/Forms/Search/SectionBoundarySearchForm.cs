﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class SectionBoundarySearchForm : SearchFeatureForm
    {
        public SectionBoundarySearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Section Boundary");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASCity(cbCity);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHCity(cbCity);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPCity(cbCity);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGCity(cbCity);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKCity(cbCity);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNCity(cbCity);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVCity(cbCity);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKCity(cbCity);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGCity(cbCity);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGCity(cbCity);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void SectionBoundarySearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string name = StringUtils.CheckNull(txtName.Text);

            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GSectionBoundary sectionBoundary in repo.GetByIds<GSectionBoundary>(ids, true))
                {
                    ListViewItem item = CreateItem(sectionBoundary);
                    items.Add(item);
                }
            }
            else
            {
                Query<GSectionBoundary> query = new Query<GSectionBoundary>(SegmentName);
                query.Obj.Name = name;
                query.Obj.City = city;
                query.Obj.State = state;

                foreach (GSectionBoundary sectionBoundary in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(sectionBoundary);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
        }

        private ListViewItem CreateItem(GSectionBoundary sectionBoundary)
        {
            ListViewItem item = new ListViewItem();
            item.Text = sectionBoundary.OID.ToString();
            item.SubItems.Add(sectionBoundary.Name);
            item.SubItems.Add(sectionBoundary.City);
            item.SubItems.Add(sectionBoundary.State);
            item.SubItems.Add(sectionBoundary.CreatedBy);
            item.SubItems.Add(sectionBoundary.DateCreated);
            item.SubItems.Add(sectionBoundary.UpdatedBy);
            item.SubItems.Add(sectionBoundary.DateUpdated);
            item.Tag = sectionBoundary;
            return item;
        }
    }
}

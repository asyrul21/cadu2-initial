﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class BuildingSearchForm : SearchFeatureForm
    {
        private int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        private int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        private int? SelectedPropertyType
        {
            get { return (cbPropertyType.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, GPropertyType>)cbPropertyType.SelectedItem).Value.Code; }
        }

        private int? SelectedStreetType
        {
            get { return (cbStreetType.SelectedItem == null) ? (int?)null : ((GStreetType)cbStreetType.SelectedItem).Code; }
        }

        public BuildingSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Building");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Construction Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Code");
            lvResult.Columns.Add("Name");
            lvResult.Columns.Add("Name2");
            lvResult.Columns.Add("Property Type");
            lvResult.Columns.Add("Lot");
            lvResult.Columns.Add("House");
            lvResult.Columns.Add("Street Type");
            lvResult.Columns.Add("Street Name");
            lvResult.Columns.Add("Street Name2");
            lvResult.Columns.Add("Section");
            lvResult.Columns.Add("Postcode");
            lvResult.Columns.Add("City");
            lvResult.Columns.Add("State");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");
            lvResult.Columns.Add("Updated By");
            lvResult.Columns.Add("Date Updated");

            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulatePropertyType(cbPropertyType);
            ComboBoxUtils.PopulateStreetType(cbStreetType);

            //added by asyrul
            ComboBoxUtils.PopulateState(cbState);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    ComboBoxUtils.PopulateASStreetName(cbStreetName);
                    ComboBoxUtils.PopulateASStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateASSection(cbSection);
                    ComboBoxUtils.PopulateASPostcode(cbPostcode);
                    ComboBoxUtils.PopulateASCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateASBuildingName(cbName);
                    ComboBoxUtils.PopulateASBuildingName2(cbName2);
                    break;
                case SegmentName.JH:
                    ComboBoxUtils.PopulateJHStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJHStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJHSection(cbSection);
                    ComboBoxUtils.PopulateJHPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJHCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateJHBuildingName(cbName);
                    ComboBoxUtils.PopulateJHBuildingName2(cbName2);
                    break;
                case SegmentName.JP:
                    ComboBoxUtils.PopulateJPStreetName(cbStreetName);
                    ComboBoxUtils.PopulateJPStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateJPSection(cbSection);
                    ComboBoxUtils.PopulateJPPostcode(cbPostcode);
                    ComboBoxUtils.PopulateJPCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateJPBuildingName(cbName);
                    ComboBoxUtils.PopulateJPBuildingName2(cbName2);
                    break;
                case SegmentName.KG:
                    ComboBoxUtils.PopulateKGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKGSection(cbSection);
                    ComboBoxUtils.PopulateKGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKGBuildingName(cbName);
                    ComboBoxUtils.PopulateKGBuildingName2(cbName2);
                    break;
                case SegmentName.KK:
                    ComboBoxUtils.PopulateKKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKKSection(cbSection);
                    ComboBoxUtils.PopulateKKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKKCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKKBuildingName(cbName);
                    ComboBoxUtils.PopulateKKBuildingName2(cbName2);
                    break;
                case SegmentName.KN:
                    ComboBoxUtils.PopulateKNStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKNStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKNSection(cbSection);
                    ComboBoxUtils.PopulateKNPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKNCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKNBuildingName(cbName);
                    ComboBoxUtils.PopulateKNBuildingName2(cbName2);
                    break;
                case SegmentName.KV:
                    ComboBoxUtils.PopulateKVStreetName(cbStreetName);
                    ComboBoxUtils.PopulateKVStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateKVSection(cbSection);
                    //ComboBoxUtils.PopulateKVPostcode(cbPostcode);
                    ComboBoxUtils.PopulateKVCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateKVBuildingName(cbName);
                    ComboBoxUtils.PopulateKVBuildingName2(cbName2);
                    break;
                case SegmentName.MK:
                    ComboBoxUtils.PopulateMKStreetName(cbStreetName);
                    ComboBoxUtils.PopulateMKStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateMKSection(cbSection);
                    ComboBoxUtils.PopulateMKPostcode(cbPostcode);
                    ComboBoxUtils.PopulateMKCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateMKBuildingName(cbName);
                    ComboBoxUtils.PopulateMKBuildingName2(cbName2);
                    break;
                case SegmentName.PG:
                    ComboBoxUtils.PopulatePGStreetName(cbStreetName);
                    ComboBoxUtils.PopulatePGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulatePGSection(cbSection);
                    ComboBoxUtils.PopulatePGPostcode(cbPostcode);
                    ComboBoxUtils.PopulatePGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulatePGBuildingName(cbName);
                    ComboBoxUtils.PopulatePGBuildingName2(cbName2);
                    break;
                case SegmentName.TG:
                    ComboBoxUtils.PopulateTGStreetName(cbStreetName);
                    ComboBoxUtils.PopulateTGStreetName2(cbStreetName2);
                    ComboBoxUtils.PopulateTGSection(cbSection);
                    ComboBoxUtils.PopulateTGPostcode(cbPostcode);
                    ComboBoxUtils.PopulateTGCity(cbCity);

                    // building
                    ComboBoxUtils.PopulateTGBuildingName(cbName);
                    ComboBoxUtils.PopulateTGBuildingName2(cbName2);
                    break;
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //added end
        }

        private void BuildingSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            string code = StringUtils.CheckNull(txtCode.Text);
            string name = StringUtils.CheckNull(cbName.Text);
            string name2 = StringUtils.CheckNull(cbName2.Text);
            string lot = StringUtils.CheckNull(txtLot.Text);
            string house = StringUtils.CheckNull(txtHouse.Text);
            string streetName = StringUtils.CheckNull(cbStreetName.Text);
            string streetName2 = StringUtils.CheckNull(cbStreetName2.Text);


            //string section = StringUtils.CheckNull(txtSection.Text);
            //string postcode = StringUtils.CheckNull(txtPostcode.Text);
            //string city = StringUtils.CheckNull(txtCity.Text);
            //string state = StringUtils.CheckNull(txtState.Text);

            string section = StringUtils.CheckNull(cbSection.Text);
            string postcode = StringUtils.CheckNull(cbPostcode.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            //string state = StringUtils.CheckNull(cbState.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GBuilding building in repo.GetByIds<GBuilding>(ids, true))
                {
                    ListViewItem item = CreateItem(building);
                    items.Add(item);
                }
            }
            else
            {
                Query<GBuilding> query = new Query<GBuilding>(SegmentName);
                query.Obj.ConstructionStatus = SelectedConstructionStatus;
                query.Obj.NavigationStatus = SelectedNavigationStatus;
                query.Obj.Code = code;
                if (!string.IsNullOrEmpty(name))
                {
                    //query.AddClause(() => query.Obj.Name, @"LIKE '%" + name + @"%'");
                    query.AddClause(() => query.Obj.Name, @"LIKE q'[%" + name + @"%]'");
                }
                query.Obj.Name2 = name2;
                query.Obj.PropertyType = SelectedPropertyType;
                query.Obj.Lot = lot;
                query.Obj.House = house;
                query.Obj.StreetType = SelectedStreetType;
                query.Obj.StreetName = streetName;
                query.Obj.StreetName2 = streetName2;
                query.Obj.Section = section;
                query.Obj.Postcode = postcode;
                query.Obj.City = city;
                query.Obj.State = state;

                foreach (GBuilding building in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(building);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add((building.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add(StringUtils.TrimSpaces(building.PropertyTypeValue));
            item.SubItems.Add(building.Lot);
            item.SubItems.Add(building.House);
            item.SubItems.Add(StringUtils.TrimSpaces(building.StreetTypeValue));
            item.SubItems.Add(building.StreetName);
            item.SubItems.Add(building.StreetName2);
            item.SubItems.Add(building.Section);
            item.SubItems.Add(building.Postcode);
            item.SubItems.Add(building.City);
            item.SubItems.Add(building.State);
            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }
    }
}

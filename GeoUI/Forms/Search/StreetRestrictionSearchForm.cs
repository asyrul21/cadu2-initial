﻿using Geomatic.Core;
using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Search
{
    public partial class StreetRestrictionSearchForm : SearchFeatureForm
    {
        private int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public StreetRestrictionSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            RefreshTitle("Search Street Restriction");
        }

        protected override void Form_Load()
        {
            base.Form_Load();

            lvResult.Columns.Add("Id");
            lvResult.Columns.Add("Status");
            lvResult.Columns.Add("Navigation Status");
            lvResult.Columns.Add("Junction Id");
            lvResult.Columns.Add("Start Id");
            lvResult.Columns.Add("End Id 1");
            lvResult.Columns.Add("End Id 2");
            lvResult.Columns.Add("End Id 3");
            lvResult.Columns.Add("End Id 4");
            lvResult.Columns.Add("End Id 5");
            lvResult.Columns.Add("End Id 6");
            lvResult.Columns.Add("End Id 7");
            lvResult.Columns.Add("End Id 8");
            lvResult.Columns.Add("End Id 9");
            lvResult.Columns.Add("End Id 10");
            lvResult.Columns.Add("Created By");
            lvResult.Columns.Add("Date Created");

            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            //ComboBoxUtils.PopulateStreetStatus(cbStatus);
        }

        private void StreetRestrictionSearchForm_Shown(object sender, EventArgs e)
        {
            if (!txtId.Focused)
            {
                txtId.Focus();
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }

        protected override void DoSearch()
        {
            IEnumerable<int> ids = StringUtils.CommaIntSplit(txtId.Text);
            int? junctionId = StringUtils.CheckInt(txtJunctionId.Text);
            int? startId = StringUtils.CheckInt(txtStartId.Text);
            int? endId1 = StringUtils.CheckInt(txtEndId1.Text);
            int? endId2 = StringUtils.CheckInt(txtEndId2.Text);
            int? endId3 = StringUtils.CheckInt(txtEndId3.Text);
            int? endId4 = StringUtils.CheckInt(txtEndId4.Text);
            int? endId5 = StringUtils.CheckInt(txtEndId5.Text);
            int? endId6 = StringUtils.CheckInt(txtEndId6.Text);
            int? endId7 = StringUtils.CheckInt(txtEndId7.Text);
            int? endId8 = StringUtils.CheckInt(txtEndId8.Text);
            int? endId9 = StringUtils.CheckInt(txtEndId9.Text);
            int? endId10 = StringUtils.CheckInt(txtEndId10.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (ids.Count() > 0)
            {
                foreach (GStreetRestriction street in repo.GetByIds<GStreetRestriction>(ids, true))
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }
            }
            else
            {
                Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
                //query.Object.Status = SelectedStatus;
                query.Obj.NavigationStatus = SelectedNavigationStatus;
                query.Obj.JunctionId = junctionId;
                query.Obj.StartId = startId;
                query.Obj.EndId1 = endId1;
                query.Obj.EndId2 = endId2;
                query.Obj.EndId3 = endId3;
                query.Obj.EndId4 = endId4;
                query.Obj.EndId5 = endId5;
                query.Obj.EndId6 = endId6;
                query.Obj.EndId7 = endId7;
                query.Obj.EndId8 = endId8;
                query.Obj.EndId9 = endId9;
                query.Obj.EndId10 = endId10;

                foreach (GStreetRestriction street in repo.Search(query, true, Limit))
                {
                    ListViewItem item = CreateItem(street);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());

            // noraini ali - Feb 2021
            // update lblResult
            lblResult.Text = lvResult.Items.Count.ToString();
        }

        private ListViewItem CreateItem(GStreetRestriction restriction)
        {
            ListViewItem item = new ListViewItem();
            item.Text = restriction.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(restriction.StatusValue));
            item.SubItems.Add((restriction.IsNavigationReady) ? "Ready" : "Not Ready");
            item.SubItems.Add(restriction.JunctionId.HasValue ? restriction.JunctionId.ToString() : string.Empty);
            item.SubItems.Add(restriction.StartId.HasValue ? restriction.StartId.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId1.HasValue ? restriction.EndId1.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId2.HasValue ? restriction.EndId2.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId3.HasValue ? restriction.EndId3.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId4.HasValue ? restriction.EndId4.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId5.HasValue ? restriction.EndId5.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId6.HasValue ? restriction.EndId6.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId7.HasValue ? restriction.EndId7.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId8.HasValue ? restriction.EndId8.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId9.HasValue ? restriction.EndId9.ToString() : string.Empty);
            item.SubItems.Add(restriction.EndId10.HasValue ? restriction.EndId10.ToString() : string.Empty);
            item.SubItems.Add(restriction.CreatedBy);
            item.SubItems.Add(restriction.DateCreated);
            item.Tag = restriction;
            return item;
        }
    }
}

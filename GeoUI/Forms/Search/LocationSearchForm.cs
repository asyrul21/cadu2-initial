﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms.Search
{
    public partial class LocationSearchForm : SearchForm
    {
        protected bool HideDeleted { set; get; }

        private string SelectedStateCode
        {
            get { return null; }
            //get { return (cbState.SelectedItem == null) ? null : ((SegmentState)cbState.SelectedItem).StateCode; }
        }

        public LocationSearchForm()
        {
            InitializeComponent();
            HideDeleted = true;
        }

        public LocationSearchForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            HideDeleted = true;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    lvResult.Sorting = SortOrder.None;
                    lvResult.BeginUpdate();
                    lvResult.Items.Clear();
                    DoSearch();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (lvResult.Items.Count > 0)
                {
                    lvResult.Items[0].Selected = true;
                    if (!lvResult.Focused)
                    {
                        lvResult.Focus();
                    }
                }
                lblResult.Text = lvResult.Items.Count.ToString();
                lvResult.FixColumnWidth();
                lvResult.EndUpdate();
            }
        }

        protected override void DoSearch()
        {
            //int testInt;
            //int? id = (int.TryParse(txtId.Text, out testInt)) ? testInt : (int?)null;
            //string sectionText = cbSection.Text;
            //string cityText = cbCity.Text;
            //string stateText = cbState.Text;

            //List<ListViewItem> items = new List<ListViewItem>();
            //RepositoryFactory repo = new RepositoryFactory(_segmentName);
            //if (id.HasValue)
            //{
            //    GSection section = repo.GetById<GSection>(id.Value);
            //    if (section != null)
            //    {
            //        ListViewItem item = CreateItem(section);
            //        items.Add(item);
            //    }
            //}
            //else
            //{
            //    Query<GSection> query = new Query<GSection>(_segmentName);
            //    query.MatchType = MatchType.Contains;
            //    query.Object.Section = (string.IsNullOrEmpty(sectionText)) ? null : sectionText;
            //    query.Object.City = (string.IsNullOrEmpty(cityText)) ? null : cityText;
            //    if (SelectedStateCode != "-") query.Object.State = SelectedStateCode;
            //    if (HideDeleted) query.Object.DelFlag = 1;

            //    foreach (GSection section in repo.Search<GSection>(query, true))
            //    {
            //        ListViewItem item = CreateItem(section);
            //        items.Add(item);
            //    }
            //}

            //lvResult.Items.AddRange(items.ToArray());
        }

        protected ListViewItem CreateItem(GLocation location)
        {
            ListViewItem item = new ListViewItem();
            item.Text = location.OID.ToString();
            item.SubItems.Add(location.Section);
            item.SubItems.Add(location.City);
            item.SubItems.Add(location.State);
            item.SubItems.Add(location.OriginalSection);
            item.SubItems.Add(location.OriginalCity);
            item.SubItems.Add(location.OriginalState);
            item.SubItems.Add(location.DelFlag == 1 ? "Yes" : "No");
            item.SubItems.Add(location.NewFlag == 1 ? "Yes" : "No");
            item.SubItems.Add(location.CreatedBy);
            item.SubItems.Add(location.DateCreated);
            item.SubItems.Add(location.UpdatedBy);
            item.SubItems.Add(location.DateUpdated);
            if (location.DelFlag == 1)
            {
                item.BackColor = Color.Red;
                item.ForeColor = Color.White;
            }
            else
            {
                if (location.NewFlag == 1)
                {
                    item.BackColor = Color.Green;
                    item.ForeColor = Color.White;
                }
            }
            item.Tag = location;
            return item;
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnSearch;
        }
    }
}

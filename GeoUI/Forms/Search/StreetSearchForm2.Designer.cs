﻿namespace Geomatic.UI.Forms.Search
{
    partial class StreetSearchForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblSubCity;
            System.Windows.Forms.Label lblState;
            System.Windows.Forms.Label lblStatus;
            System.Windows.Forms.Label lblNaviStatus;
            System.Windows.Forms.Label lblNetworkClass;
            System.Windows.Forms.Label lblClass;
            System.Windows.Forms.Label lblCategory;
            System.Windows.Forms.Label lblFilterLevel;
            System.Windows.Forms.Label lblDirection;
            System.Windows.Forms.Label lblSpeedLimit;
            System.Windows.Forms.Label lblType;
            System.Windows.Forms.Label lblConstructionStatus;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbName = new Geomatic.UI.Controls.UpperComboBox();
            this.cbName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSubCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.txtSpeedLimit = new System.Windows.Forms.TextBox();
            this.cbFilterLevel = new Geomatic.UI.Controls.UpperComboBox();
            this.cbClass = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbNetworkClass = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCategory = new Geomatic.UI.Controls.UpperComboBox();
            this.cbDirection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbConstructionStatus = new Geomatic.UI.Controls.UpperComboBox();
            lblId = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblSubCity = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            lblStatus = new System.Windows.Forms.Label();
            lblNaviStatus = new System.Windows.Forms.Label();
            lblNetworkClass = new System.Windows.Forms.Label();
            lblClass = new System.Windows.Forms.Label();
            lblCategory = new System.Windows.Forms.Label();
            lblFilterLevel = new System.Windows.Forms.Label();
            lblDirection = new System.Windows.Forms.Label();
            lblSpeedLimit = new System.Windows.Forms.Label();
            lblType = new System.Windows.Forms.Label();
            lblConstructionStatus = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(338, 427);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(257, 427);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(581, 427);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(419, 427);
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 298);
            this.lvResult.Size = new System.Drawing.Size(644, 123);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(500, 427);
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(79, 35);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(38, 13);
            lblName.TabIndex = 23;
            lblName.Text = "Name:";
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(393, 35);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(44, 13);
            lblName2.TabIndex = 25;
            lblName2.Text = "Name2:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 63);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 27;
            lblSection.Text = "Section:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(382, 63);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 37;
            lblPostcode.Text = "Postcode:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 91);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 39;
            lblCity.Text = "City:";
            // 
            // lblSubCity
            // 
            lblSubCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSubCity.AutoSize = true;
            lblSubCity.Location = new System.Drawing.Point(388, 91);
            lblSubCity.Name = "lblSubCity";
            lblSubCity.Size = new System.Drawing.Size(49, 13);
            lblSubCity.TabIndex = 41;
            lblSubCity.Text = "Sub City:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(402, 119);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 43;
            lblState.Text = "State:";
            // 
            // lblStatus
            // 
            lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStatus.AutoSize = true;
            lblStatus.Location = new System.Drawing.Point(77, 147);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(40, 13);
            lblStatus.TabIndex = 45;
            lblStatus.Text = "Status:";
            // 
            // lblNaviStatus
            // 
            lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNaviStatus.AutoSize = true;
            lblNaviStatus.Location = new System.Drawing.Point(343, 175);
            lblNaviStatus.Name = "lblNaviStatus";
            lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            lblNaviStatus.TabIndex = 47;
            lblNaviStatus.Text = "Navigation Status:";
            // 
            // lblNetworkClass
            // 
            lblNetworkClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNetworkClass.AutoSize = true;
            lblNetworkClass.Location = new System.Drawing.Point(39, 175);
            lblNetworkClass.Name = "lblNetworkClass";
            lblNetworkClass.Size = new System.Drawing.Size(78, 13);
            lblNetworkClass.TabIndex = 49;
            lblNetworkClass.Text = "Network Class:";
            // 
            // lblClass
            // 
            lblClass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblClass.AutoSize = true;
            lblClass.Location = new System.Drawing.Point(402, 203);
            lblClass.Name = "lblClass";
            lblClass.Size = new System.Drawing.Size(35, 13);
            lblClass.TabIndex = 51;
            lblClass.Text = "Class:";
            // 
            // lblCategory
            // 
            lblCategory.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCategory.AutoSize = true;
            lblCategory.Location = new System.Drawing.Point(65, 203);
            lblCategory.Name = "lblCategory";
            lblCategory.Size = new System.Drawing.Size(52, 13);
            lblCategory.TabIndex = 53;
            lblCategory.Text = "Category:";
            // 
            // lblFilterLevel
            // 
            lblFilterLevel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblFilterLevel.AutoSize = true;
            lblFilterLevel.Location = new System.Drawing.Point(376, 231);
            lblFilterLevel.Name = "lblFilterLevel";
            lblFilterLevel.Size = new System.Drawing.Size(61, 13);
            lblFilterLevel.TabIndex = 55;
            lblFilterLevel.Text = "Filter Level:";
            // 
            // lblDirection
            // 
            lblDirection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblDirection.AutoSize = true;
            lblDirection.Location = new System.Drawing.Point(65, 231);
            lblDirection.Name = "lblDirection";
            lblDirection.Size = new System.Drawing.Size(52, 13);
            lblDirection.TabIndex = 57;
            lblDirection.Text = "Direction:";
            // 
            // lblSpeedLimit
            // 
            lblSpeedLimit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSpeedLimit.AutoSize = true;
            lblSpeedLimit.Location = new System.Drawing.Point(372, 259);
            lblSpeedLimit.Name = "lblSpeedLimit";
            lblSpeedLimit.Size = new System.Drawing.Size(65, 13);
            lblSpeedLimit.TabIndex = 59;
            lblSpeedLimit.Text = "Speed Limit:";
            // 
            // lblType
            // 
            lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblType.AutoSize = true;
            lblType.Location = new System.Drawing.Point(403, 7);
            lblType.Name = "lblType";
            lblType.Size = new System.Drawing.Size(34, 13);
            lblType.TabIndex = 61;
            lblType.Text = "Type:";
            // 
            // lblConstructionStatus
            // 
            lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblConstructionStatus.AutoSize = true;
            lblConstructionStatus.Location = new System.Drawing.Point(335, 147);
            lblConstructionStatus.Name = "lblConstructionStatus";
            lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            lblConstructionStatus.TabIndex = 63;
            lblConstructionStatus.Text = "Construction Status:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(lblName, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbName, 1, 1);
            this.tableLayoutPanel.Controls.Add(lblName2, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.cbName2, 3, 1);
            this.tableLayoutPanel.Controls.Add(lblSection, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.cbSection, 1, 2);
            this.tableLayoutPanel.Controls.Add(lblPostcode, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.cbPostcode, 3, 2);
            this.tableLayoutPanel.Controls.Add(lblCity, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.cbCity, 1, 3);
            this.tableLayoutPanel.Controls.Add(lblSubCity, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.cbSubCity, 3, 3);
            this.tableLayoutPanel.Controls.Add(lblState, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.cbState, 3, 4);
            this.tableLayoutPanel.Controls.Add(lblType, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cbType, 3, 0);
            this.tableLayoutPanel.Controls.Add(lblSpeedLimit, 2, 9);
            this.tableLayoutPanel.Controls.Add(this.txtSpeedLimit, 3, 9);
            this.tableLayoutPanel.Controls.Add(lblFilterLevel, 2, 8);
            this.tableLayoutPanel.Controls.Add(this.cbFilterLevel, 3, 8);
            this.tableLayoutPanel.Controls.Add(lblClass, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.cbClass, 3, 7);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 6);
            this.tableLayoutPanel.Controls.Add(lblNaviStatus, 2, 6);
            this.tableLayoutPanel.Controls.Add(lblStatus, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.cbStatus, 1, 5);
            this.tableLayoutPanel.Controls.Add(lblNetworkClass, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.cbNetworkClass, 1, 6);
            this.tableLayoutPanel.Controls.Add(lblCategory, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.cbCategory, 1, 7);
            this.tableLayoutPanel.Controls.Add(lblDirection, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.cbDirection, 1, 8);
            this.tableLayoutPanel.Controls.Add(lblConstructionStatus, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.cbConstructionStatus, 3, 5);
            this.tableLayoutPanel.Location = new System.Drawing.Point(15, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 10;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(641, 280);
            this.tableLayoutPanel.TabIndex = 10;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(194, 20);
            this.txtId.TabIndex = 1;
            // 
            // cbName
            // 
            this.cbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName.FormattingEnabled = true;
            this.cbName.Location = new System.Drawing.Point(123, 31);
            this.cbName.Name = "cbName";
            this.cbName.Size = new System.Drawing.Size(194, 21);
            this.cbName.Sorted = true;
            this.cbName.TabIndex = 24;
            // 
            // cbName2
            // 
            this.cbName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbName2.FormattingEnabled = true;
            this.cbName2.Location = new System.Drawing.Point(443, 31);
            this.cbName2.Name = "cbName2";
            this.cbName2.Size = new System.Drawing.Size(195, 21);
            this.cbName2.Sorted = true;
            this.cbName2.TabIndex = 26;
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 59);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(194, 21);
            this.cbSection.Sorted = true;
            this.cbSection.TabIndex = 36;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(443, 59);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(195, 21);
            this.cbPostcode.TabIndex = 38;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 87);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(194, 21);
            this.cbCity.Sorted = true;
            this.cbCity.TabIndex = 40;
            // 
            // cbSubCity
            // 
            this.cbSubCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSubCity.FormattingEnabled = true;
            this.cbSubCity.Location = new System.Drawing.Point(443, 87);
            this.cbSubCity.Name = "cbSubCity";
            this.cbSubCity.Size = new System.Drawing.Size(195, 21);
            this.cbSubCity.TabIndex = 42;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(443, 115);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(195, 21);
            this.cbState.TabIndex = 44;
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(443, 3);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(195, 21);
            this.cbType.Sorted = true;
            this.cbType.TabIndex = 62;
            // 
            // txtSpeedLimit
            // 
            this.txtSpeedLimit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSpeedLimit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpeedLimit.Location = new System.Drawing.Point(443, 256);
            this.txtSpeedLimit.Name = "txtSpeedLimit";
            this.txtSpeedLimit.Size = new System.Drawing.Size(195, 20);
            this.txtSpeedLimit.TabIndex = 60;
            // 
            // cbFilterLevel
            // 
            this.cbFilterLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilterLevel.FormattingEnabled = true;
            this.cbFilterLevel.Location = new System.Drawing.Point(443, 227);
            this.cbFilterLevel.Name = "cbFilterLevel";
            this.cbFilterLevel.Size = new System.Drawing.Size(195, 21);
            this.cbFilterLevel.TabIndex = 56;
            // 
            // cbClass
            // 
            this.cbClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(443, 199);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(195, 21);
            this.cbClass.TabIndex = 52;
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(443, 171);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(195, 21);
            this.cbNaviStatus.TabIndex = 48;
            // 
            // cbStatus
            // 
            this.cbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(123, 143);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(194, 21);
            this.cbStatus.TabIndex = 46;
            // 
            // cbNetworkClass
            // 
            this.cbNetworkClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNetworkClass.FormattingEnabled = true;
            this.cbNetworkClass.Location = new System.Drawing.Point(123, 171);
            this.cbNetworkClass.Name = "cbNetworkClass";
            this.cbNetworkClass.Size = new System.Drawing.Size(194, 21);
            this.cbNetworkClass.TabIndex = 50;
            // 
            // cbCategory
            // 
            this.cbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(123, 199);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(194, 21);
            this.cbCategory.TabIndex = 54;
            // 
            // cbDirection
            // 
            this.cbDirection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDirection.FormattingEnabled = true;
            this.cbDirection.Location = new System.Drawing.Point(123, 227);
            this.cbDirection.Name = "cbDirection";
            this.cbDirection.Size = new System.Drawing.Size(194, 21);
            this.cbDirection.TabIndex = 58;
            // 
            // cbConstructionStatus
            // 
            this.cbConstructionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstructionStatus.FormattingEnabled = true;
            this.cbConstructionStatus.Location = new System.Drawing.Point(443, 143);
            this.cbConstructionStatus.Name = "cbConstructionStatus";
            this.cbConstructionStatus.Size = new System.Drawing.Size(195, 21);
            this.cbConstructionStatus.TabIndex = 64;
            // 
            // StreetSearchForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "StreetSearchForm2";
            this.Text = "StreetSearchForm2";
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox txtId;
        private Controls.UpperComboBox cbName;
        private Controls.UpperComboBox cbName2;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbSubCity;
        private Controls.UpperComboBox cbState;
        private Controls.UpperComboBox cbStatus;
        private Controls.UpperComboBox cbNaviStatus;
        private Controls.UpperComboBox cbNetworkClass;
        private Controls.UpperComboBox cbClass;
        private Controls.UpperComboBox cbCategory;
        private Controls.UpperComboBox cbFilterLevel;
        private Controls.UpperComboBox cbDirection;
        private System.Windows.Forms.TextBox txtSpeedLimit;
        private Controls.UpperComboBox cbType;
        private Controls.UpperComboBox cbConstructionStatus;
    }
}
﻿namespace Geomatic.UI.Forms.Search
{
    partial class FloorSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblPropertyType;
            System.Windows.Forms.Label lblStreetType;
            System.Windows.Forms.Label lblName;
            System.Windows.Forms.Label lblNumber;
            System.Windows.Forms.Label lblName2;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblPostcode;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblState;
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.cbStreetName2 = new Geomatic.UI.Controls.UpperComboBox();
            this.cbStreetName = new Geomatic.UI.Controls.UpperComboBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cbStreetType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPropertyType = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbPostcode = new Geomatic.UI.Controls.UpperComboBox();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            lblId = new System.Windows.Forms.Label();
            lblPropertyType = new System.Windows.Forms.Label();
            lblStreetType = new System.Windows.Forms.Label();
            lblName = new System.Windows.Forms.Label();
            lblNumber = new System.Windows.Forms.Label();
            lblName2 = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblPostcode = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 186);
            this.lvResult.Size = new System.Drawing.Size(610, 235);
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 7);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblPropertyType
            // 
            lblPropertyType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPropertyType.AutoSize = true;
            lblPropertyType.Location = new System.Drawing.Point(346, 35);
            lblPropertyType.Name = "lblPropertyType";
            lblPropertyType.Size = new System.Drawing.Size(76, 13);
            lblPropertyType.TabIndex = 4;
            lblPropertyType.Text = "Property Type:";
            // 
            // lblStreetType
            // 
            lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetType.AutoSize = true;
            lblStreetType.Location = new System.Drawing.Point(52, 63);
            lblStreetType.Name = "lblStreetType";
            lblStreetType.Size = new System.Drawing.Size(65, 13);
            lblStreetType.TabIndex = 6;
            lblStreetType.Text = "Street Type:";
            // 
            // lblName
            // 
            lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName.AutoSize = true;
            lblName.Location = new System.Drawing.Point(353, 63);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(69, 13);
            lblName.TabIndex = 8;
            lblName.Text = "Street Name:";
            // 
            // lblNumber
            // 
            lblNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblNumber.AutoSize = true;
            lblNumber.Location = new System.Drawing.Point(70, 35);
            lblNumber.Name = "lblNumber";
            lblNumber.Size = new System.Drawing.Size(47, 13);
            lblNumber.TabIndex = 2;
            lblNumber.Text = "Number:";
            // 
            // lblName2
            // 
            lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblName2.AutoSize = true;
            lblName2.Location = new System.Drawing.Point(347, 91);
            lblName2.Name = "lblName2";
            lblName2.Size = new System.Drawing.Size(75, 13);
            lblName2.TabIndex = 10;
            lblName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 119);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 12;
            lblSection.Text = "Section:";
            // 
            // lblPostcode
            // 
            lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblPostcode.AutoSize = true;
            lblPostcode.Location = new System.Drawing.Point(367, 119);
            lblPostcode.Name = "lblPostcode";
            lblPostcode.Size = new System.Drawing.Size(55, 13);
            lblPostcode.TabIndex = 14;
            lblPostcode.Text = "Postcode:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(90, 147);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 16;
            lblCity.Text = "City:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(387, 147);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 18;
            lblState.Text = "State:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtNumber, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.cbStreetName2, 3, 3);
            this.tableLayoutPanel.Controls.Add(lblNumber, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbStreetName, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(lblStreetType, 0, 2);
            this.tableLayoutPanel.Controls.Add(lblName, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.cbStreetType, 1, 2);
            this.tableLayoutPanel.Controls.Add(lblName2, 2, 3);
            this.tableLayoutPanel.Controls.Add(lblSection, 0, 4);
            this.tableLayoutPanel.Controls.Add(lblPostcode, 2, 4);
            this.tableLayoutPanel.Controls.Add(lblCity, 0, 5);
            this.tableLayoutPanel.Controls.Add(lblState, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.cbPropertyType, 3, 1);
            this.tableLayoutPanel.Controls.Add(lblPropertyType, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.cbSection, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.cbPostcode, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.cbCity, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.cbState, 3, 5);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(610, 168);
            this.tableLayoutPanel.TabIndex = 10;
            // 
            // txtNumber
            // 
            this.txtNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumber.Location = new System.Drawing.Point(123, 32);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(179, 20);
            this.txtNumber.TabIndex = 3;
            this.txtNumber.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName2
            // 
            this.cbStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName2.FormattingEnabled = true;
            this.cbStreetName2.Location = new System.Drawing.Point(428, 87);
            this.cbStreetName2.Name = "cbStreetName2";
            this.cbStreetName2.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName2.TabIndex = 11;
            this.cbStreetName2.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetName
            // 
            this.cbStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbStreetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStreetName.FormattingEnabled = true;
            this.cbStreetName.Location = new System.Drawing.Point(428, 59);
            this.cbStreetName.Name = "cbStreetName";
            this.cbStreetName.Size = new System.Drawing.Size(179, 21);
            this.cbStreetName.TabIndex = 9;
            this.cbStreetName.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(179, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbStreetType
            // 
            this.cbStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbStreetType.FormattingEnabled = true;
            this.cbStreetType.Location = new System.Drawing.Point(123, 59);
            this.cbStreetType.Name = "cbStreetType";
            this.cbStreetType.Size = new System.Drawing.Size(179, 21);
            this.cbStreetType.TabIndex = 7;
            this.cbStreetType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbPropertyType
            // 
            this.cbPropertyType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPropertyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPropertyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPropertyType.FormattingEnabled = true;
            this.cbPropertyType.Location = new System.Drawing.Point(428, 31);
            this.cbPropertyType.Name = "cbPropertyType";
            this.cbPropertyType.Size = new System.Drawing.Size(179, 21);
            this.cbPropertyType.TabIndex = 5;
            this.cbPropertyType.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 115);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(179, 21);
            this.cbSection.TabIndex = 19;
            // 
            // cbPostcode
            // 
            this.cbPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPostcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbPostcode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPostcode.FormattingEnabled = true;
            this.cbPostcode.Location = new System.Drawing.Point(428, 115);
            this.cbPostcode.Name = "cbPostcode";
            this.cbPostcode.Size = new System.Drawing.Size(179, 21);
            this.cbPostcode.TabIndex = 20;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(123, 143);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(179, 21);
            this.cbCity.TabIndex = 21;
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(428, 143);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(179, 21);
            this.cbState.TabIndex = 22;
            // 
            // FloorSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "FloorSearchForm";
            this.Text = "FloorSearchForm";
            this.Shown += new System.EventHandler(this.FloorSearchForm_Shown);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnShow, 0);
            this.Controls.SetChildIndex(this.btnShowAll, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TextBox txtId;
        private Controls.UpperComboBox cbPropertyType;
        private Controls.UpperComboBox cbStreetName;
        private Controls.UpperComboBox cbStreetType;
        private System.Windows.Forms.TextBox txtNumber;
        private Controls.UpperComboBox cbStreetName2;
        private Controls.UpperComboBox cbSection;
        private Controls.UpperComboBox cbPostcode;
        private Controls.UpperComboBox cbCity;
        private Controls.UpperComboBox cbState;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Commands;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;

namespace Geomatic.UI.Forms
{
    public partial class StreetForm : CollapsibleForm
    {
        #region Properties

        public int? SelectedConstructionStatus
        {
            get { return (cbConstructionStatus.SelectedItem == null) ? (int?)null : ((GConstructionStatus)cbConstructionStatus.SelectedItem).Code; }
        }

        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public int? SelectedStatus
        {
            get { return (cbStatus.SelectedItem == null) ? (int?)null : ((GStreetStatus)cbStatus.SelectedItem).Code; }
        }

        public int? SelectedNetworkClass
        {
            get { return (cbNetworkClass.SelectedItem == null) ? (int?)null : ((GStreetNetworkClass)cbNetworkClass.SelectedItem).Code; }
        }

        public int? SelectedClass
        {
            get { return (cbClass.SelectedItem == null) ? (int?)null : ((GStreetClass)cbClass.SelectedItem).Code; }
        }

        public int? SelectedCategory
        {
            get { return (cbCategory.SelectedItem == null) ? (int?)null : ((GStreetCategory)cbCategory.SelectedItem).Code; }
        }

        public int? SelectedFilterLevel
        {
            get { return (cbFilterLevel.SelectedItem == null) ? (int?)null : ((GStreetFilterLevel)cbFilterLevel.SelectedItem).Code; }
        }

        public int? SelectedDesign
        {
            get { return (cbDesign.SelectedItem == null) ? (int?)null : ((GStreetDesign)cbDesign.SelectedItem).Code; }
        }

        public int? SelectedDirection
        {
            get { return (cbDirection.SelectedItem == null) ? (int?)null : ((GStreetDirection)cbDirection.SelectedItem).Code; }
        }

        public int? SelectedTollType
        {
            get { return (cbTollType.SelectedItem == null) ? (int?)null : ((GStreetTollType)cbTollType.SelectedItem).Code; }
        }

        public int? SelectedDivider
        {
            get { return (cbDivider.SelectedItem == null) ? (int?)null : ((GStreetDivider)cbDivider.SelectedItem).Code; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public int? SelectedType
        {
            get { return (cbType.SelectedItem == null) ? (int?)null : ((GStreetType)cbType.SelectedItem).Code; }
        }

        public string InsertedName
        {
            get { return cbName.Text; }
        }

        public string InsertedName2
        {
            get { return cbName2.Text; }
        }

        public string InsertedSection
        {
            get { return txtSection.Text; }
        }

        public string InsertedPostCode
        {
            get { return txtPostcode.Text; }
        }

        public string InsertedCity
        {
            get { return txtCity.Text; }
        }

        public string InsertedSubCity
        {
            get { return txtSubCity.Text; }
        }

        public string InsertedState
        {
            get { return txtState.Text; }
        }

        public int? InsertedSpeedLimit
        {
            get { return numSpeedLimit.Value; }
        }

        public double? InsertedWeight
        {
            get { return numWeight.Value; }
        }

        public int? InsertedElevation
        {
            get { return numElevation.Value; }
        }

        public double? InsertedLength
        {
            get { return numLength.Value; }
        }

        public double? InsertedWidth
        {
            get { return numWidth.Value; }
        }

        public double? InsertedHeight
        {
            get { return numHeight.Value; }
        }

        public int? InsertedLanes
        {
            get { return numLanes.Value; }
        }

        public int? Bicycle
        {
            get { return (!chkBicycle.Checked) ? 1 : 0; }
        }

        public int? Bus
        {
            get { return (!chkBus.Checked) ? 1 : 0; }
        }

        public int? Car
        {
            get { return (!chkCar.Checked) ? 1 : 0; }
        }

        public int? Delivery
        {
            get { return (!chkDelivery.Checked) ? 1 : 0; }
        }

        public int? Emergency
        {
            get { return (!chkEmergency.Checked) ? 1 : 0; }
        }

        public int? Pedestrian
        {
            get { return (!chkPedestrian.Checked) ? 1 : 0; }
        }

        public int? Truck
        {
            get { return (!chkTruck.Checked) ? 1 : 0; }
        }

        //asyrul
        public bool ShowText
        {
            get { return ckShowText.Checked; }
        }

        #endregion

        protected GStreet _street;

        public StreetForm()
            : this(null)
        {
        }

        public StreetForm(GStreet street)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus,
                lblNetworkClass, cbNetworkClass,
                lblClass, cbClass,
                lblCategory, cbCategory,
                lblDivider, cbDivider);
            _commandPool.Register(Command.EditableItem,
                lblConstructionStatus, cbConstructionStatus,
                lblStatus, cbStatus,
                lblNaviStatus, cbNaviStatus,
                lblNetworkClass, cbNetworkClass,
                lblClass, cbClass,
                lblCategory, cbCategory,
                lblFilterLevel, cbFilterLevel,
                lblDesign, cbDesign,
                lblDirection, cbDirection,
                lblTollType, cbTollType,
                lblSource, cbSource,
                lblType, cbType,
                lblName, cbName,
                lblName2, cbName2,
                lblSection, txtSection,
                lblPostcode, txtPostcode,
                lblCity, txtCity,
                lblSubCity, txtSubCity,
                lblState, txtState,
                lblSpeedLimit, numSpeedLimit,
                lblMaxWeight, numWeight,
                lblElevation, numElevation,
                lblLength, numLength,
                lblWidth, numWidth,
                lblHeight, numHeight,
                lblLanes, numLanes,
                btnPickLocation,
                chkBicycle,
                chkBus,
                chkCar,
                chkDelivery,
                chkEmergency,
                chkPedestrian,
                chkTruck,
                btnApply);

            _street = street;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateConstructionStatus(cbConstructionStatus);
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateStreetStatus(cbStatus);
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateStreetNetworkClass(cbNetworkClass);
            ComboBoxUtils.PopulateStreetClass(cbClass);
            ComboBoxUtils.PopulateStreetCategory(cbCategory);
            ComboBoxUtils.PopulateStreetFilterLevel(cbFilterLevel);
            ComboBoxUtils.PopulateStreetDesign(cbDesign);
            ComboBoxUtils.PopulateStreetDirection(cbDirection);
            ComboBoxUtils.PopulateStreetTollType(cbTollType);
            ComboBoxUtils.PopulateStreetDivider(cbDivider);
            ComboBoxUtils.PopulateStreetType(cbType);

            txtId.Text = _street.OID.ToString();

            txtFromId.Text = _street.FromNodeId.ToString();
            txtToId.Text = _street.ToNodeId.ToString();

            cbConstructionStatus.Text = _street.ConstructionStatusValue;
            cbNaviStatus.Text = _street.NavigationStatusValue;
            cbStatus.Text = _street.StatusValue;
            cbNetworkClass.Text = _street.NetworkClassValue;
            cbClass.Text = _street.ClassValue;
            cbCategory.Text = _street.CategoryValue;
            cbFilterLevel.Text = _street.FilterLevelValue;
            cbDesign.Text = _street.DesignValue;
            cbDirection.Text = _street.DirectionValue;
            cbTollType.Text = _street.TollTypeValue;
            cbDivider.Text = _street.DividerValue;
            cbSource.Text = _street.SourceValue;

            cbType.Text = _street.TypeValue;
            cbName.Text = _street.Name;
            cbName2.Text = _street.Name2;
            txtSection.Text = _street.Section;
            txtPostcode.Text = _street.Postcode;
            txtCity.Text = _street.City;
            txtSubCity.Text = _street.SubCity;
            txtState.Text = _street.State;

            numSpeedLimit.Value = (_street.SpeedLimit.HasValue) ? _street.SpeedLimit.Value : 0;
            numWeight.Value = (_street.Weight.HasValue) ? _street.Weight.Value : 0;
            numElevation.Value = (_street.Elevation.HasValue) ? _street.Elevation.Value : 0;
            numLength.Value = (_street.Length.HasValue) ? _street.Length.Value : 0;
            numWidth.Value = (_street.Width.HasValue) ? _street.Width.Value : 0;
            numHeight.Value = (_street.Height.HasValue) ? _street.Height.Value : 0;
            numLanes.Value = (_street.NumOfLanes.HasValue) ? _street.NumOfLanes.Value : 1;

            chkBicycle.Checked = (_street.Bicycle != 1);
            chkBus.Checked = (_street.Bus != 1);
            chkCar.Checked = (_street.Car != 1);
            chkDelivery.Checked = (_street.Delivery != 1);
            chkEmergency.Checked = (_street.Emergency != 1);
            chkPedestrian.Checked = (_street.Pedestrian != 1);
            chkTruck.Checked = (_street.Truck != 1);

            // added by noraini 15/5/2019
            if (Name == "AddStreetForm")
                ckShowText.Checked = true;
            else
            {
                if (Name == "EditStreetForm2")  // added by noraini - feb 2021
                {
                    ckShowText.Enabled = false;
                }
                else
                {
                    if (_street.HasText())
                        ckShowText.Checked = true;
                    else
                        ckShowText.Checked = false;
                }
            }
            // end

            txtCreator.Text = _street.CreatedBy;
            txtDateCreated.Text = _street.DateCreated;
            txtUpdater.Text = _street.UpdatedBy;
            txtDateUpdated.Text = _street.DateUpdated;
        }

        private void btnPickLocation_Click(object sender, EventArgs e)
        {
            using (ChooseLocationForm form = new ChooseLocationForm(_street.SegmentName))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GLocation location = form.SelectedLocation;
                txtSection.Text = location.Section;
                txtCity.Text = location.City;
                txtState.Text = location.State;
            }
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Construction Status
            bool isConstructionStatusValid = StreetValidator.CheckConstructionStatus(SelectedConstructionStatus);
            LabelColor(lblConstructionStatus, isConstructionStatusValid);
            pass &= isConstructionStatusValid;

            // Status
            bool isStatusValid = StreetValidator.CheckStatus(SelectedStatus);
            LabelColor(lblStatus, isStatusValid);
            pass &= isStatusValid;

            // Filter Level
            bool isFilterLevelValid = StreetValidator.CheckFilterLevel(SelectedFilterLevel);
            LabelColor(lblFilterLevel, isFilterLevelValid);
            pass &= isFilterLevelValid;

            // Design
            bool isDesignValid = StreetValidator.CheckDesign(SelectedDesign);
            LabelColor(lblDesign, isDesignValid);
            pass &= isDesignValid;

            // Direction
            bool isDirectionValid = StreetValidator.CheckDirection(SelectedDirection);
            LabelColor(lblDirection, isDirectionValid);
            pass &= isDirectionValid;

            // Toll Type
            bool isTollTypeValid = StreetValidator.CheckTollType(SelectedTollType);
            LabelColor(lblTollType, isTollTypeValid);
            pass &= isTollTypeValid;

            // Divider
            bool isDividerValid = StreetValidator.CheckDivider(SelectedDivider);
            LabelColor(lblDivider, isDividerValid);
            pass &= isDividerValid;

            // Lane
            bool isLaneValid = StreetValidator.CheckLane(numLanes.Value);
            LabelColor(lblLanes, isLaneValid);
            pass &= isLaneValid;

            // Type
            bool isTypeValid = StreetValidator.CheckType(SelectedType);
            LabelColor(lblType, isTypeValid);
            //isTypeValid &= isTypeValid;
            pass &= isTypeValid;

            // Name
            bool isNameValid = StreetValidator.CheckName(SelectedStatus, cbName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Name2
            bool isName2Valid = StreetValidator.CheckName2(cbName2.Text);
            LabelColor(lblName2, isName2Valid);
            pass &= isName2Valid;

            // Postcode
            bool isPostcodeValid = StreetValidator.CheckPostcode(txtPostcode.Text); ;
            LabelColor(lblPostcode, isPostcodeValid);
            pass &= isPostcodeValid;

            // City
            bool isCityValid = StreetValidator.CheckCity(txtCity.Text);
            LabelColor(lblCity, isCityValid);
            pass &= isCityValid;

            // Sub City
            bool isSubCityValid = StreetValidator.CheckSubCity(txtSubCity.Text);
            LabelColor(lblSubCity, isSubCityValid);
            pass &= isSubCityValid;

            // State
            bool isStateValid = StreetValidator.CheckState(txtState.Text);
            LabelColor(lblState, isStateValid);
            pass &= isStateValid;

            return pass;
        }

        public void SetValues()
        {
            _street.ConstructionStatus = SelectedConstructionStatus;
            _street.NavigationStatus = SelectedNavigationStatus;
            _street.Status = SelectedStatus;
            _street.NetworkClass = SelectedNetworkClass;
            _street.Class = SelectedClass;
            _street.Category = SelectedCategory;
            _street.FilterLevel = SelectedFilterLevel;
            _street.Design = SelectedDesign;
            _street.Direction = SelectedDirection;
            _street.TollType = SelectedTollType;
            _street.Divider = SelectedDivider;
            _street.Source = SelectedSource;

            _street.Type = SelectedType;
            _street.Name = InsertedName;
            _street.Name2 = InsertedName2;
            _street.Section = InsertedSection;
            _street.Postcode = InsertedPostCode;
            _street.City = InsertedCity;
            _street.SubCity = InsertedSubCity;
            _street.State = InsertedState;

            _street.SpeedLimit = InsertedSpeedLimit;
            _street.Weight = InsertedWeight;
            _street.Elevation = InsertedElevation;
            _street.Length = InsertedLength;
            _street.Width = InsertedWidth;
            _street.Height = InsertedHeight;
            _street.NumOfLanes = InsertedLanes;

            _street.Bicycle = Bicycle;
            _street.Bus = Bus;
            _street.Car = Car;
            _street.Delivery = Delivery;
            _street.Emergency = Emergency;
            _street.Pedestrian = Pedestrian;
            _street.Truck = Truck;
        }
    }
}

﻿using System;

namespace Geomatic.UI.Forms
{
    partial class VerifyCadu3WebForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbUserName = new System.Windows.Forms.ComboBox();
            this.btnVerifyAll = new System.Windows.Forms.Button();
            this.btnUpdWEB = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(541, 422);
            this.btnCancel.Text = "Close";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(382, 422);
            // 
            // lvResult
            // 
            this.lvResult.Location = new System.Drawing.Point(12, 96);
            this.lvResult.Size = new System.Drawing.Size(610, 315);
            this.lvResult.DoubleClick += new System.EventHandler(this.lvResult_DoubleClick);
            // 
            // lblResult
            // 
            this.lblResult.Location = new System.Drawing.Point(151, 423);
            // 
            // lblRecord
            // 
            this.lblRecord.Location = new System.Drawing.Point(100, 423);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Enabled = false;
            this.btnSaveAs.Location = new System.Drawing.Point(462, 422);
            // 
            // txtLimit
            // 
            this.txtLimit.Location = new System.Drawing.Point(59, 420);
            // 
            // lblLimit
            // 
            this.lblLimit.Location = new System.Drawing.Point(9, 422);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.tableLayoutPanel.Controls.Add(this.cbStatus, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbUserName, 1, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(15, 14);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.14286F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.14286F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(607, 66);
            this.tableLayoutPanel.TabIndex = 15;
            // 
            // cbStatus
            // 
            this.cbStatus.AllowDrop = true;
            this.cbStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(197, 39);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(179, 21);
            this.cbStatus.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "User Name :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Status :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUserName
            // 
            this.cbUserName.AllowDrop = true;
            this.cbUserName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbUserName.FormattingEnabled = true;
            this.cbUserName.Location = new System.Drawing.Point(197, 6);
            this.cbUserName.Name = "cbUserName";
            this.cbUserName.Size = new System.Drawing.Size(179, 21);
            this.cbUserName.TabIndex = 21;
            // 
            // btnVerifyAll
            // 
            this.btnVerifyAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerifyAll.AutoSize = true;
            this.btnVerifyAll.Image = global::Geomatic.UI.Properties.Resources.apply;
            this.btnVerifyAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVerifyAll.Location = new System.Drawing.Point(300, 422);
            this.btnVerifyAll.Name = "btnVerifyAll";
            this.btnVerifyAll.Size = new System.Drawing.Size(75, 23);
            this.btnVerifyAll.TabIndex = 19;
            this.btnVerifyAll.Text = "Verify All";
            this.btnVerifyAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVerifyAll.UseVisualStyleBackColor = true;
            this.btnVerifyAll.Click += new System.EventHandler(this.btnVerifyAll_Click);
            // 
            // btnUpdWEB
            // 
            this.btnUpdWEB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdWEB.Image = global::Geomatic.UI.Properties.Resources.delete;
            this.btnUpdWEB.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdWEB.Location = new System.Drawing.Point(212, 422);
            this.btnUpdWEB.Name = "btnUpdWEB";
            this.btnUpdWEB.Size = new System.Drawing.Size(80, 23);
            this.btnUpdWEB.TabIndex = 20;
            this.btnUpdWEB.Text = "Clear Web";
            this.btnUpdWEB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdWEB.UseVisualStyleBackColor = true;
            this.btnUpdWEB.Click += new System.EventHandler(this.btnUpdWEB_Click);
            // 
            // VerifyCadu3WebForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(634, 462);
            this.Controls.Add(this.btnUpdWEB);
            this.Controls.Add(this.btnVerifyAll);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "VerifyCadu3WebForm";
            this.Text = "Verification CADU3 WEB";
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.btnVerifyAll, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.lblResult, 0);
            this.Controls.SetChildIndex(this.lblRecord, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnSaveAs, 0);
            this.Controls.SetChildIndex(this.txtLimit, 0);
            this.Controls.SetChildIndex(this.lblLimit, 0);
            this.Controls.SetChildIndex(this.btnUpdWEB, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbUserName;
        private System.Windows.Forms.Button btnVerifyAll;
        private System.Windows.Forms.Button btnUpdWEB;
    }
}
﻿namespace Geomatic.UI.Forms
{
    partial class LocationForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblId;
            System.Windows.Forms.Label lblSection;
            System.Windows.Forms.Label lblCity;
            System.Windows.Forms.Label lblState;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LocationForm2));
            this.txtId = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cbCity = new Geomatic.UI.Controls.UpperComboBox();
            this.cbSection = new Geomatic.UI.Controls.UpperComboBox();
            this.cbState = new Geomatic.UI.Controls.UpperComboBox();
            this.lblRecord = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lvResult = new Geomatic.UI.Controls.SortableListView();
            this.btnCancel = new System.Windows.Forms.Button();
            lblId = new System.Windows.Forms.Label();
            lblSection = new System.Windows.Forms.Label();
            lblCity = new System.Windows.Forms.Label();
            lblState = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblId
            // 
            lblId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblId.AutoSize = true;
            lblId.Location = new System.Drawing.Point(98, 8);
            lblId.Name = "lblId";
            lblId.Size = new System.Drawing.Size(19, 13);
            lblId.TabIndex = 0;
            lblId.Text = "Id:";
            // 
            // lblSection
            // 
            lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblSection.AutoSize = true;
            lblSection.Location = new System.Drawing.Point(71, 39);
            lblSection.Name = "lblSection";
            lblSection.Size = new System.Drawing.Size(46, 13);
            lblSection.TabIndex = 4;
            lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblCity.AutoSize = true;
            lblCity.Location = new System.Drawing.Point(370, 8);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(27, 13);
            lblCity.TabIndex = 2;
            lblCity.Text = "City:";
            // 
            // lblState
            // 
            lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblState.AutoSize = true;
            lblState.Location = new System.Drawing.Point(362, 39);
            lblState.Name = "lblState";
            lblState.Size = new System.Drawing.Size(35, 13);
            lblState.TabIndex = 6;
            lblState.Text = "State:";
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Location = new System.Drawing.Point(123, 5);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(154, 20);
            this.txtId.TabIndex = 1;
            this.txtId.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.cbCity, 3, 0);
            this.tableLayoutPanel.Controls.Add(lblId, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel.Controls.Add(lblSection, 0, 1);
            this.tableLayoutPanel.Controls.Add(lblCity, 2, 0);
            this.tableLayoutPanel.Controls.Add(lblState, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.cbSection, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.cbState, 3, 1);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 61);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // cbCity
            // 
            this.cbCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(403, 4);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(154, 21);
            this.cbCity.TabIndex = 3;
            this.cbCity.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbSection
            // 
            this.cbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            //this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbSection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSection.FormattingEnabled = true;
            this.cbSection.Location = new System.Drawing.Point(123, 35);
            this.cbSection.Name = "cbSection";
            this.cbSection.Size = new System.Drawing.Size(154, 21);
            this.cbSection.TabIndex = 5;
            this.cbSection.Enter += new System.EventHandler(this.OnParameter_Enter);
            // 
            // cbState
            // 
            this.cbState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            //this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbState.FormattingEnabled = true;
            this.cbState.Location = new System.Drawing.Point(403, 35);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(154, 21);
            this.cbState.TabIndex = 7;
            // 
            // lblRecord
            // 
            this.lblRecord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRecord.AutoSize = true;
            this.lblRecord.Location = new System.Drawing.Point(12, 432);
            this.lblRecord.Name = "lblRecord";
            this.lblRecord.Size = new System.Drawing.Size(45, 13);
            this.lblRecord.TabIndex = 2;
            this.lblRecord.Text = "Record:";
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(63, 432);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(13, 13);
            this.lblResult.TabIndex = 1;
            this.lblResult.Text = "0";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(416, 427);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lvResult
            // 
            this.lvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResult.FullRowSelect = true;
            this.lvResult.HideSelection = false;
            this.lvResult.Location = new System.Drawing.Point(12, 84);
            this.lvResult.Name = "lvResult";
            this.lvResult.Size = new System.Drawing.Size(560, 337);
            this.lvResult.TabIndex = 0;
            this.lvResult.UseCompatibleStateImageBehavior = false;
            this.lvResult.View = System.Windows.Forms.View.Details;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(497, 427);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // LocationForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.lvResult);
            this.Controls.Add(this.lblRecord);
            this.Controls.Add(this.lblResult);
            this.Name = "LocationForm2";
            this.Text = "LocationForm";
            this.Load += new System.EventHandler(this.LocationForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Geomatic.UI.Controls.SortableListView lvResult;
        protected System.Windows.Forms.Label lblRecord;
        protected System.Windows.Forms.Label lblResult;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Button btnCancel;
        protected Geomatic.UI.Controls.UpperComboBox cbSection;
        protected Geomatic.UI.Controls.UpperComboBox cbCity;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Button btnSearch;
        protected Geomatic.UI.Controls.UpperComboBox cbState;
    }
}
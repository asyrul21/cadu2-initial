﻿using Geomatic.Core;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Search;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms
{
    public partial class ManageLocationForm : LocationForm2
    {
        public ManageLocationForm()
            : this(SegmentName.None)
        {
        }

        public ManageLocationForm(SegmentName segmentName)
            : base(segmentName)
        {
            InitializeComponent();
            _hideDeleted = false;
        }

        protected override void Form_Load()
        {
            RefreshTitle("Manage Location");
            base.Form_Load();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (AddLocationForm form = new AddLocationForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    GLocation newLocation = repo.NewObj<GLocation>();

                    Query<GLocation> query = new Query<GLocation>();
					//query.Obj.Section = form.InsertedSection;
					query.AddClause(() => query.Obj.Section, @"LIKE q'[%" + form.InsertedSection + @"]'");
					query.Obj.City = form.InsertedCity;
                    query.Obj.State = form.SelectedState;

                    if (repo.Count(query) > 0)
                    {
                        throw new QualityControlException("Location already exists");
                    }

                    newLocation.Section = form.InsertedSection;
                    newLocation.City = form.InsertedCity;
                    newLocation.State = form.SelectedState;
                    newLocation.NewFlag = 1;
                    newLocation.DelFlag = 0;   // noraini ali 10/03/2021 - set delflag = 0, fixed search pick location
                    //added by asyrul
                    //newLocation.DelFlag = form.IsCheckedForDeletion;
                    //added end

                    //Console.WriteLine(newLocation);
                    newLocation = repo.Insert(newLocation);

                    ListViewItem item = new ListViewItem();
                    item.Text = newLocation.OID.ToString();
                    item.SubItems.Add(newLocation.Section);
                    item.SubItems.Add(newLocation.City);
                    item.SubItems.Add(newLocation.State);
                    item.SubItems.Add(newLocation.OriginalSection);
                    item.SubItems.Add(newLocation.OriginalCity);
                    item.SubItems.Add(newLocation.OriginalState);
                    item.SubItems.Add((newLocation.DelFlag == 1) ? "Yes" : "No");
                    item.SubItems.Add((newLocation.NewFlag == 1) ? "Yes" : "No");
                    item.SubItems.Add(newLocation.CreatedBy);
                    item.SubItems.Add(newLocation.DateCreated);
                    item.SubItems.Add(newLocation.UpdatedBy);
                    item.SubItems.Add(newLocation.DateUpdated);
                    if (newLocation.DelFlag == 1)
                    {
                        item.BackColor = Color.Red;
                        item.ForeColor = Color.White;
                    }
                    else
                    {
                        item.BackColor = Color.Green;
                        item.ForeColor = Color.White;
                    }
                    item.Tag = newLocation;

                    lvResult.Items.Add(item);                   
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = lvResult.SelectedItems[0];
            GLocation location = (GLocation)item.Tag;

            using (EditLocationForm form = new EditLocationForm(location))
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();

                    location.Section = form.InsertedSection;
                    location.City = form.InsertedCity;
                    location.State = form.SelectedState;
                    location.NewFlag = 1;
                    location.DelFlag = form.IsCheckedForDeletion; //added by noraini
                    // todo

                    repo.Update(location);

                    item.SubItems.Clear();
                    item.Text = location.OID.ToString();
                    item.SubItems.Add(location.Section);
                    item.SubItems.Add(location.City);
                    item.SubItems.Add(location.State);
                    item.SubItems.Add(location.OriginalSection);
                    item.SubItems.Add(location.OriginalCity);
                    item.SubItems.Add(location.OriginalState);
                    item.SubItems.Add((location.DelFlag == 1) ? "Yes" : "No");
                    item.SubItems.Add((location.NewFlag == 1) ? "Yes" : "No");
                    item.SubItems.Add(location.CreatedBy);
                    item.SubItems.Add(location.DateCreated);
                    item.SubItems.Add(location.UpdatedBy);
                    item.SubItems.Add(location.DateUpdated);
                    if (location.DelFlag == 1)
                    {
                        item.BackColor = Color.Red;
                        item.ForeColor = Color.White;
                    }
                    else
                    { 
                        if (location.NewFlag == 1)
                        {
                            item.BackColor = Color.Green;
                            item.ForeColor = Color.White;
                        }
                    }
                    item.Tag = location;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvResult.SelectedItems.Count == 0)
            {
                return;
            }

            ListViewItem item = lvResult.SelectedItems[0];
            GLocation location = (GLocation)item.Tag;

            using (QuestionMessageBox box = new QuestionMessageBox())
            {
                box.SetText("Please Make sure this Section not belong to any Street, Are you sure you want to delete section: {0} city: {1} state: {2}? ", location.Section, location.City, location.State);
                if (box.Show(this) != DialogResult.Yes)
                {
                    return;
                }
                using (new WaitCursor())
                {
                    RepositoryFactory repo = new RepositoryFactory();

                    repo.Delete(location);
                    item.Remove();
                }
            }
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            using (new WaitCursor())
            {
                string fileName = saveFileDialog1.FileName;

                if (!File.Exists(fileName))
                {
                    FileStream fileStream = File.Create(fileName);
                    fileStream.Dispose();
                }

                using (StreamWriter streamWriter = new StreamWriter(fileName, false))
                {
                    List<string> columns = new List<string>();
                    foreach (ColumnHeader columnHeader in lvResult.Columns)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("\"");
                        sb.Append(columnHeader.Text);
                        sb.Append("\"");
                        columns.Add(sb.ToString());
                    }
                    streamWriter.WriteLine(string.Join(",", columns.ToArray()));

                    foreach (ListViewItem item in lvResult.Items)
                    {
                        List<string> subItems = new List<string>();
                        foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("\"");
                            sb.Append(subItem.Text);
                            sb.Append("\"");
                            subItems.Add(sb.ToString());
                        }
                        streamWriter.WriteLine(string.Join(",", subItems.ToArray()));
                    }
                }
            }
        }

        // added by noraini ali - Feb 2021
        protected override void DoSearch()
        {
            int? id = StringUtils.CheckInt(txtId.Text);
            string section = StringUtils.CheckNull(cbSection.Text);
            string city = StringUtils.CheckNull(cbCity.Text);
            string state = (cbState.SelectedItem != null) ? ((ComboBoxItem<string, string>)cbState.SelectedItem).Value : null;

            string filterflag = StringUtils.CheckNull(cbflagfilter.Text);

            List<ListViewItem> items = new List<ListViewItem>();
            RepositoryFactory repo = new RepositoryFactory();
            if (id.HasValue)
            {
                GLocation location = repo.GetById<GLocation>(id.Value);
                if (location != null)
                {
                    ListViewItem item = CreateItem(location);
                    items.Add(item);
                }
            }
            else
            {
                Query<GLocation> query = new Query<GLocation>();
                query.MatchType = MatchType.Contains;

                if (!string.IsNullOrEmpty(section))
                {
                    query.AddClause(() => query.Obj.Section, @"LIKE q'[%" + section + @"%]'");
                }
                else
                {
                    query.Obj.Section = section;
                }

                // noraini - feb 2022 - update search location by segment
                string stateCode = "";
                if (string.IsNullOrEmpty(cbState.Text))
                {
                    stateCode = "'" + GetStateCode(_segmentName, cbState.Items[0].ToString()) + "'";
                    int index = cbState.Items.Count;
                    if (index > 1)
                    {
                        for (int i = 1; i < index; i++)
                        {
                            stateCode = stateCode + ",'" + GetStateCode(_segmentName, cbState.Items[i].ToString()) + "'"; ;
                        }
                    }
                }
                else
                {
                    stateCode = "'" + GetStateCode(_segmentName, cbState.Text.ToString()) + "'";
                }
                query.AddClause(() => query.Obj.State, "IN (" + stateCode + ")");
                // end 

                query.Obj.City = city;
                //query.Obj.State = SelectedState;
                query.Obj.State = state;

                if (filterflag == "Deleted")
                    query.Obj.DelFlag = 1;
                else if (filterflag == "New/Updated")
                    query.Obj.DelFlag = 0;

                foreach (GLocation location in repo.Search(query, true))
                {
                    ListViewItem item = CreateItem(location);
                    items.Add(item);
                }
            }

            lvResult.Items.AddRange(items.ToArray());
            lblResult.Text = lvResult.Items.Count.ToString();
        }

    }
}

﻿namespace Geomatic.UI.Forms
{
    partial class VerifyWorkAreaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblWorkAreaId = new System.Windows.Forms.Label();
            this.txtWorkAreaId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtWAStatus = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtWAOwner = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWONo = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.chkProperty = new System.Windows.Forms.CheckBox();
            this.chkLandmark = new System.Windows.Forms.CheckBox();
            this.chkJunction = new System.Windows.Forms.CheckBox();
            this.chkStreet = new System.Windows.Forms.CheckBox();
            this.chkBuilding = new System.Windows.Forms.CheckBox();
            this.chkBuildingGroup = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblResult = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReview = new System.Windows.Forms.Button();
            this.btnSaveVerify = new System.Windows.Forms.Button();
            this.lvResult = new Geomatic.UI.Controls.SortableListView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.None;
            this.bottomPanel.Location = new System.Drawing.Point(26, 481);
            this.bottomPanel.Size = new System.Drawing.Size(494, 36);
            this.bottomPanel.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.77551F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.22449F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 183F));
            this.tableLayoutPanel1.Controls.Add(this.lblWorkAreaId, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtWorkAreaId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtWAStatus, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtWAOwner, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtWONo, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(18, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(507, 52);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // lblWorkAreaId
            // 
            this.lblWorkAreaId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblWorkAreaId.AutoSize = true;
            this.lblWorkAreaId.Location = new System.Drawing.Point(5, 5);
            this.lblWorkAreaId.Name = "lblWorkAreaId";
            this.lblWorkAreaId.Size = new System.Drawing.Size(76, 13);
            this.lblWorkAreaId.TabIndex = 0;
            this.lblWorkAreaId.Text = "Work Area Id :";
            this.lblWorkAreaId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWorkAreaId
            // 
            this.txtWorkAreaId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWorkAreaId.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtWorkAreaId.Enabled = false;
            this.txtWorkAreaId.Location = new System.Drawing.Point(87, 3);
            this.txtWorkAreaId.Name = "txtWorkAreaId";
            this.txtWorkAreaId.Size = new System.Drawing.Size(128, 20);
            this.txtWorkAreaId.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(240, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Complete Flag :";
            // 
            // txtWAStatus
            // 
            this.txtWAStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWAStatus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtWAStatus.Enabled = false;
            this.txtWAStatus.Location = new System.Drawing.Point(326, 3);
            this.txtWAStatus.Name = "txtWAStatus";
            this.txtWAStatus.ReadOnly = true;
            this.txtWAStatus.Size = new System.Drawing.Size(178, 20);
            this.txtWAStatus.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "User Id :";
            // 
            // txtWAOwner
            // 
            this.txtWAOwner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWAOwner.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtWAOwner.Enabled = false;
            this.txtWAOwner.Location = new System.Drawing.Point(326, 28);
            this.txtWAOwner.Name = "txtWAOwner";
            this.txtWAOwner.ReadOnly = true;
            this.txtWAOwner.Size = new System.Drawing.Size(178, 20);
            this.txtWAOwner.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Work Order No:";
            // 
            // txtWONo
            // 
            this.txtWONo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWONo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtWONo.Enabled = false;
            this.txtWONo.Location = new System.Drawing.Point(87, 28);
            this.txtWONo.Name = "txtWONo";
            this.txtWONo.ReadOnly = true;
            this.txtWONo.Size = new System.Drawing.Size(128, 20);
            this.txtWONo.TabIndex = 7;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.24561F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.75439F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205F));
            this.tableLayoutPanel2.Controls.Add(this.chkProperty, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkLandmark, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.chkJunction, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkStreet, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.chkBuilding, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkBuildingGroup, 2, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(20, 78);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(421, 48);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // chkProperty
            // 
            this.chkProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkProperty.AutoSize = true;
            this.chkProperty.Checked = true;
            this.chkProperty.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProperty.Location = new System.Drawing.Point(3, 3);
            this.chkProperty.Name = "chkProperty";
            this.chkProperty.Size = new System.Drawing.Size(98, 18);
            this.chkProperty.TabIndex = 0;
            this.chkProperty.Text = "Property";
            this.chkProperty.UseVisualStyleBackColor = true;
            // 
            // chkLandmark
            // 
            this.chkLandmark.AutoSize = true;
            this.chkLandmark.Checked = true;
            this.chkLandmark.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLandmark.Location = new System.Drawing.Point(3, 27);
            this.chkLandmark.Name = "chkLandmark";
            this.chkLandmark.Size = new System.Drawing.Size(73, 17);
            this.chkLandmark.TabIndex = 1;
            this.chkLandmark.Text = "Landmark";
            this.chkLandmark.UseVisualStyleBackColor = true;
            // 
            // chkJunction
            // 
            this.chkJunction.AutoSize = true;
            this.chkJunction.Checked = true;
            this.chkJunction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkJunction.Location = new System.Drawing.Point(107, 3);
            this.chkJunction.Name = "chkJunction";
            this.chkJunction.Size = new System.Drawing.Size(66, 17);
            this.chkJunction.TabIndex = 2;
            this.chkJunction.Text = "Junction";
            this.chkJunction.UseVisualStyleBackColor = true;
            // 
            // chkStreet
            // 
            this.chkStreet.AutoSize = true;
            this.chkStreet.Checked = true;
            this.chkStreet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStreet.Location = new System.Drawing.Point(107, 27);
            this.chkStreet.Name = "chkStreet";
            this.chkStreet.Size = new System.Drawing.Size(54, 17);
            this.chkStreet.TabIndex = 3;
            this.chkStreet.Text = "Street";
            this.chkStreet.UseVisualStyleBackColor = true;
            // 
            // chkBuilding
            // 
            this.chkBuilding.AutoSize = true;
            this.chkBuilding.Checked = true;
            this.chkBuilding.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuilding.Location = new System.Drawing.Point(218, 3);
            this.chkBuilding.Name = "chkBuilding";
            this.chkBuilding.Size = new System.Drawing.Size(63, 17);
            this.chkBuilding.TabIndex = 4;
            this.chkBuilding.Text = "Building";
            this.chkBuilding.UseVisualStyleBackColor = true;
            // 
            // chkBuildingGroup
            // 
            this.chkBuildingGroup.AutoSize = true;
            this.chkBuildingGroup.Checked = true;
            this.chkBuildingGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuildingGroup.Location = new System.Drawing.Point(218, 27);
            this.chkBuildingGroup.Name = "chkBuildingGroup";
            this.chkBuildingGroup.Size = new System.Drawing.Size(95, 17);
            this.chkBuildingGroup.TabIndex = 5;
            this.chkBuildingGroup.Text = "Building Group";
            this.chkBuildingGroup.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.77114F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.22886F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel3.Controls.Add(this.lblResult, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnClose, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnReview, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnSaveVerify, 2, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(18, 482);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(507, 32);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(58, 9);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(60, 13);
            this.lblResult.TabIndex = 3;
            this.lblResult.Text = "lblResult";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Record :";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(425, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 26);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnReview
            // 
            this.btnReview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnReview.Image = global::Geomatic.UI.Properties.Resources.apply;
            this.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReview.Location = new System.Drawing.Point(303, 3);
            this.btnReview.Name = "btnReview";
            this.btnReview.Size = new System.Drawing.Size(116, 26);
            this.btnReview.TabIndex = 1;
            this.btnReview.Text = "Display Attribute";
            this.btnReview.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReview.UseVisualStyleBackColor = true;
            this.btnReview.Click += new System.EventHandler(this.BtnReview_Click);
            // 
            // btnSaveVerify
            // 
            this.btnSaveVerify.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSaveVerify.Location = new System.Drawing.Point(218, 4);
            this.btnSaveVerify.Name = "btnSaveVerify";
            this.btnSaveVerify.Size = new System.Drawing.Size(75, 23);
            this.btnSaveVerify.TabIndex = 6;
            this.btnSaveVerify.Text = "Save As";
            this.btnSaveVerify.UseVisualStyleBackColor = true;
            this.btnSaveVerify.Click += new System.EventHandler(this.BtnSaveVerify_Click);
            // 
            // lvResult
            // 
            this.lvResult.FullRowSelect = true;
            this.lvResult.GridLines = true;
            this.lvResult.HideSelection = false;
            this.lvResult.Location = new System.Drawing.Point(18, 136);
            this.lvResult.MultiSelect = false;
            this.lvResult.Name = "lvResult";
            this.lvResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lvResult.Size = new System.Drawing.Size(507, 339);
            this.lvResult.TabIndex = 0;
            this.lvResult.UseCompatibleStateImageBehavior = false;
            this.lvResult.View = System.Windows.Forms.View.Details;
            this.lvResult.SelectedIndexChanged += new System.EventHandler(this.lvResult_SelectedIndexChanged);
            this.lvResult.Click += new System.EventHandler(this.LvResult_Click);
            this.lvResult.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LvResult_MouseDoubleClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(446, 76);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(80, 26);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "csv";
            this.saveFileDialog.Filter = "Comma Delimited|*.csv";
            this.saveFileDialog.Title = "Save As";
            // 
            // VerifyWorkAreaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 519);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lvResult);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "VerifyWorkAreaForm";
            this.Text = "Form1";
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.lvResult, 0);
            this.Controls.SetChildIndex(this.btnRefresh, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel3, 0);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblWorkAreaId;
        private System.Windows.Forms.TextBox txtWorkAreaId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox chkProperty;
        private System.Windows.Forms.CheckBox chkLandmark;
        private System.Windows.Forms.CheckBox chkJunction;
        private System.Windows.Forms.CheckBox chkStreet;
        private System.Windows.Forms.CheckBox chkBuilding;
        private System.Windows.Forms.CheckBox chkBuildingGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnReview;
        protected Controls.SortableListView lvResult;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWAStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtWAOwner;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtWONo;
        private System.Windows.Forms.Button btnSaveVerify;
        protected System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}
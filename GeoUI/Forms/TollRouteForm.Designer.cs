﻿namespace Geomatic.UI.Forms
{
    partial class TollRouteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInTollID = new System.Windows.Forms.Label();
            this.lblInTollSegment = new System.Windows.Forms.Label();
            this.lblTollType = new System.Windows.Forms.Label();
            this.lblOutTollSegment = new System.Windows.Forms.Label();
            this.lblOutTollID = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.btnSetOutTollID = new System.Windows.Forms.Button();
            this.cbInTollSegment = new Geomatic.UI.Controls.UpperComboBox();
            this.cbTollType = new Geomatic.UI.Controls.UpperComboBox();
            this.txtOutTollID = new System.Windows.Forms.TextBox();
            this.txtInTollID = new System.Windows.Forms.TextBox();
            this.cbOutTollSegment = new Geomatic.UI.Controls.UpperComboBox();
            this.btnSetInTollID = new System.Windows.Forms.Button();
            this.lblId = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabPrice = new System.Windows.Forms.TabControl();
            this.pagePrice = new System.Windows.Forms.TabPage();
            this.controlPrice = new Geomatic.UI.Controls.ChildControl();
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabPrice.SuspendLayout();
            this.pagePrice.SuspendLayout();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 506);
            this.bottomPanel.Size = new System.Drawing.Size(582, 37);
            // 
            // lblInTollID
            // 
            this.lblInTollID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblInTollID.AutoSize = true;
            this.lblInTollID.Location = new System.Drawing.Point(342, 35);
            this.lblInTollID.Name = "lblInTollID";
            this.lblInTollID.Size = new System.Drawing.Size(56, 13);
            this.lblInTollID.TabIndex = 4;
            this.lblInTollID.Text = "In Toll ID :";
            // 
            // lblInTollSegment
            // 
            this.lblInTollSegment.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblInTollSegment.AutoSize = true;
            this.lblInTollSegment.Location = new System.Drawing.Point(13, 35);
            this.lblInTollSegment.Name = "lblInTollSegment";
            this.lblInTollSegment.Size = new System.Drawing.Size(84, 13);
            this.lblInTollSegment.TabIndex = 2;
            this.lblInTollSegment.Text = "In Toll Segment:";
            // 
            // lblTollType
            // 
            this.lblTollType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTollType.AutoSize = true;
            this.lblTollType.Location = new System.Drawing.Point(43, 7);
            this.lblTollType.Name = "lblTollType";
            this.lblTollType.Size = new System.Drawing.Size(54, 13);
            this.lblTollType.TabIndex = 0;
            this.lblTollType.Text = "Toll Type:";
            // 
            // lblOutTollSegment
            // 
            this.lblOutTollSegment.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblOutTollSegment.AutoSize = true;
            this.lblOutTollSegment.Location = new System.Drawing.Point(5, 64);
            this.lblOutTollSegment.Name = "lblOutTollSegment";
            this.lblOutTollSegment.Size = new System.Drawing.Size(92, 13);
            this.lblOutTollSegment.TabIndex = 7;
            this.lblOutTollSegment.Text = "Out Toll Segment:";
            // 
            // lblOutTollID
            // 
            this.lblOutTollID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblOutTollID.AutoSize = true;
            this.lblOutTollID.Location = new System.Drawing.Point(334, 64);
            this.lblOutTollID.Name = "lblOutTollID";
            this.lblOutTollID.Size = new System.Drawing.Size(64, 13);
            this.lblOutTollID.TabIndex = 9;
            this.lblOutTollID.Text = "Out Toll ID :";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel.Controls.Add(this.btnSetOutTollID, 4, 2);
            this.tableLayoutPanel.Controls.Add(this.cbInTollSegment, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblInTollSegment, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lblTollType, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.cbTollType, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblOutTollID, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtOutTollID, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.lblInTollID, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblOutTollSegment, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.txtInTollID, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.cbOutTollSegment, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.btnSetInTollID, 4, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 43);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(582, 85);
            this.tableLayoutPanel.TabIndex = 6;
            // 
            // btnSetOutTollID
            // 
            this.btnSetOutTollID.Location = new System.Drawing.Point(542, 59);
            this.btnSetOutTollID.Name = "btnSetOutTollID";
            this.btnSetOutTollID.Size = new System.Drawing.Size(26, 21);
            this.btnSetOutTollID.TabIndex = 11;
            this.btnSetOutTollID.Text = "...";
            this.btnSetOutTollID.UseVisualStyleBackColor = true;
            this.btnSetOutTollID.Click += new System.EventHandler(this.btnSetOutTollID_Click);
            // 
            // cbInTollSegment
            // 
            this.cbInTollSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInTollSegment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInTollSegment.FormattingEnabled = true;
            this.cbInTollSegment.Location = new System.Drawing.Point(103, 31);
            this.cbInTollSegment.Name = "cbInTollSegment";
            this.cbInTollSegment.Size = new System.Drawing.Size(201, 21);
            this.cbInTollSegment.TabIndex = 3;
            this.cbInTollSegment.SelectedIndexChanged += new System.EventHandler(this.cbInTollSegment_SelectedIndexChanged);
            // 
            // cbTollType
            // 
            this.cbTollType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTollType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.cbTollType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTollType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTollType.FormattingEnabled = true;
            this.cbTollType.Location = new System.Drawing.Point(103, 3);
            this.cbTollType.Name = "cbTollType";
            this.cbTollType.Size = new System.Drawing.Size(201, 21);
            this.cbTollType.TabIndex = 1;
            this.cbTollType.SelectedIndexChanged += new System.EventHandler(this.cbTollType_SelectedIndexChanged);
            // 
            // txtOutTollID
            // 
            this.txtOutTollID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutTollID.Location = new System.Drawing.Point(404, 59);
            this.txtOutTollID.Name = "txtOutTollID";
            this.txtOutTollID.ReadOnly = true;
            this.txtOutTollID.Size = new System.Drawing.Size(132, 20);
            this.txtOutTollID.TabIndex = 10;
            // 
            // txtInTollID
            // 
            this.txtInTollID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInTollID.Location = new System.Drawing.Point(404, 31);
            this.txtInTollID.Name = "txtInTollID";
            this.txtInTollID.ReadOnly = true;
            this.txtInTollID.Size = new System.Drawing.Size(132, 20);
            this.txtInTollID.TabIndex = 5;
            // 
            // cbOutTollSegment
            // 
            this.cbOutTollSegment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbOutTollSegment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOutTollSegment.FormattingEnabled = true;
            this.cbOutTollSegment.Location = new System.Drawing.Point(103, 60);
            this.cbOutTollSegment.Name = "cbOutTollSegment";
            this.cbOutTollSegment.Size = new System.Drawing.Size(201, 21);
            this.cbOutTollSegment.TabIndex = 8;
            this.cbOutTollSegment.SelectedIndexChanged += new System.EventHandler(this.cbOutTollSegment_SelectedIndexChanged);
            // 
            // btnSetInTollID
            // 
            this.btnSetInTollID.Location = new System.Drawing.Point(542, 31);
            this.btnSetInTollID.Name = "btnSetInTollID";
            this.btnSetInTollID.Size = new System.Drawing.Size(26, 21);
            this.btnSetInTollID.TabIndex = 6;
            this.btnSetInTollID.Text = "...";
            this.btnSetInTollID.UseVisualStyleBackColor = true;
            this.btnSetInTollID.Click += new System.EventHandler(this.btnSetInTollID_Click);
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 23);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(582, 20);
            this.txtId.TabIndex = 5;
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabPrice);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 128);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(582, 308);
            this.additionalGroup.TabIndex = 7;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabPrice
            // 
            this.tabPrice.Controls.Add(this.pagePrice);
            this.tabPrice.Location = new System.Drawing.Point(6, 19);
            this.tabPrice.Name = "tabPrice";
            this.tabPrice.SelectedIndex = 0;
            this.tabPrice.Size = new System.Drawing.Size(579, 283);
            this.tabPrice.TabIndex = 0;
            // 
            // pagePrice
            // 
            this.pagePrice.Controls.Add(this.controlPrice);
            this.pagePrice.Location = new System.Drawing.Point(4, 22);
            this.pagePrice.Name = "pagePrice";
            this.pagePrice.Padding = new System.Windows.Forms.Padding(3);
            this.pagePrice.Size = new System.Drawing.Size(571, 257);
            this.pagePrice.TabIndex = 0;
            this.pagePrice.Text = "Price";
            this.pagePrice.UseVisualStyleBackColor = true;
            // 
            // controlPrice
            // 
            this.controlPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlPrice.Location = new System.Drawing.Point(0, 0);
            this.controlPrice.Name = "controlPrice";
            this.controlPrice.Size = new System.Drawing.Size(571, 257);
            this.controlPrice.TabIndex = 0;
            this.controlPrice.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPrice_AddClick);
            this.controlPrice.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPrice_EditClick);
            this.controlPrice.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPrice_DeleteClick);
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel2);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 436);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(582, 70);
            this.updateGroup.TabIndex = 8;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(576, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(401, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(172, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(401, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(172, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(172, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(318, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(172, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(322, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // TollRouteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 543);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblId);
            this.Name = "TollRouteForm";
            this.Text = "TollRouteForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabPrice.ResumeLayout(false);
            this.pagePrice.ResumeLayout(false);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.UpperComboBox cbInTollSegment;
        private Controls.UpperComboBox cbTollType;
        private System.Windows.Forms.TabControl tabPrice;
        private System.Windows.Forms.TabPage pagePrice;
        private Controls.UpperComboBox cbOutTollSegment;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private Controls.CollapsibleGroupBox updateGroup;
        private Controls.ChildControl controlPrice;
        private Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.Label lblId;
        protected System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtInTollID;
        private System.Windows.Forms.TextBox txtOutTollID;
        private System.Windows.Forms.Button btnSetOutTollID;
        private System.Windows.Forms.Button btnSetInTollID;
        protected System.Windows.Forms.Label lblInTollID;
        protected System.Windows.Forms.Label lblInTollSegment;
        protected System.Windows.Forms.Label lblTollType;
        protected System.Windows.Forms.Label lblOutTollSegment;
        protected System.Windows.Forms.Label lblOutTollID;

    }
}
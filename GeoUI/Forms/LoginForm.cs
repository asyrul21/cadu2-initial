﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.UI;
using Geomatic.UI.Properties;

namespace Geomatic.UI.Forms
{
    public partial class LoginForm : BaseForm
    {
        public LoginForm()
        {
            InitializeComponent();
            RefreshTitle("{0} - v{1}", AppInfo.Title, AppInfo.Version);
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            txtUsername.Text = Settings.Default.LastUser.Trim();
        }

        private void LoginForm_Shown(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text))
                txtUsername.Focus();
            else
                txtPassword.Focus();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Session.Current.OpenCadu(StringVersionName.DEFAULT, true);

                string username = txtUsername.Text.Trim();
                string password = txtPassword.Text;

                if (string.IsNullOrEmpty(username))
                {
                    MessageBox.Show("Username is required", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrEmpty(password))
                {
                    MessageBox.Show("Password is required", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                RepositoryFactory repo = new RepositoryFactory();

                Query<GUser> query = new Query<GUser>();
                query.Obj.Name = username;
                query.Obj.Password = password;

                IEnumerable<GUser> users = repo.Search(query);

                if (users.Count() == 0)
                {
                    MessageBox.Show("Wrong username/password", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                else
                {
                    Settings.Default.LastUser = username;
                    Settings.Default.Save();
                    Session.User = users.First();
                    DialogResult = DialogResult.OK;
                }
            }
        }

        private void OnParameter_Enter(object sender, EventArgs e)
        {
            AcceptButton = btnLogin;
        }
    }
}

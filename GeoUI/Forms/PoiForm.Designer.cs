﻿namespace Geomatic.UI.Forms
{
    partial class PoiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtParentId = new System.Windows.Forms.TextBox();
            this.lblParentId = new System.Windows.Forms.Label();
            this.lblParentType = new System.Windows.Forms.Label();
            this.txtParentType = new System.Windows.Forms.TextBox();
            this.lblStreetId = new System.Windows.Forms.Label();
            this.txtStreetId = new System.Windows.Forms.TextBox();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pagePhone = new System.Windows.Forms.TabPage();
            this.controlPhone = new Geomatic.UI.Controls.ReferenceControl();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtFloor = new System.Windows.Forms.TextBox();
            this.btnSetFloor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblSection = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtAbbreviation = new System.Windows.Forms.TextBox();
            this.lblAbbreviation = new System.Windows.Forms.Label();
            this.lblFilterLevel = new System.Windows.Forms.Label();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnSetCode = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.lblUrl = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.numFilterLevel = new Geomatic.UI.Controls.IntUpDown();
            this.cbNaviStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.lblNaviStatus = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkFamous = new System.Windows.Forms.CheckBox();
            this.chkDisplayText = new System.Windows.Forms.CheckBox();
            this.categoryControl = new Geomatic.UI.Controls.CodeControl();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pagePhone.SuspendLayout();
            this.addressGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFilterLevel)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 745);
            this.bottomPanel.Size = new System.Drawing.Size(560, 37);
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel2);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 675);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(560, 70);
            this.updateGroup.TabIndex = 12;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(554, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(390, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(161, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(390, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(161, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(161, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(307, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(161, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(311, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.tableLayoutPanel4);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 606);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(560, 69);
            this.relationGroup.TabIndex = 11;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.txtParentId, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblParentId, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblParentType, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtParentType, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblStreetId, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtStreetId, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(554, 50);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtParentId
            // 
            this.txtParentId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParentId.BackColor = System.Drawing.SystemColors.Control;
            this.txtParentId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtParentId.Location = new System.Drawing.Point(113, 3);
            this.txtParentId.Name = "txtParentId";
            this.txtParentId.ReadOnly = true;
            this.txtParentId.Size = new System.Drawing.Size(161, 20);
            this.txtParentId.TabIndex = 1;
            // 
            // lblParentId
            // 
            this.lblParentId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblParentId.AutoSize = true;
            this.lblParentId.Location = new System.Drawing.Point(54, 6);
            this.lblParentId.Name = "lblParentId";
            this.lblParentId.Size = new System.Drawing.Size(53, 13);
            this.lblParentId.TabIndex = 0;
            this.lblParentId.Text = "Parent Id:";
            // 
            // lblParentType
            // 
            this.lblParentType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblParentType.AutoSize = true;
            this.lblParentType.Location = new System.Drawing.Point(316, 6);
            this.lblParentType.Name = "lblParentType";
            this.lblParentType.Size = new System.Drawing.Size(68, 13);
            this.lblParentType.TabIndex = 2;
            this.lblParentType.Text = "Parent Type:";
            // 
            // txtParentType
            // 
            this.txtParentType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParentType.BackColor = System.Drawing.SystemColors.Control;
            this.txtParentType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtParentType.Location = new System.Drawing.Point(390, 3);
            this.txtParentType.Name = "txtParentType";
            this.txtParentType.ReadOnly = true;
            this.txtParentType.Size = new System.Drawing.Size(161, 20);
            this.txtParentType.TabIndex = 3;
            // 
            // lblStreetId
            // 
            this.lblStreetId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetId.AutoSize = true;
            this.lblStreetId.Location = new System.Drawing.Point(57, 31);
            this.lblStreetId.Name = "lblStreetId";
            this.lblStreetId.Size = new System.Drawing.Size(50, 13);
            this.lblStreetId.TabIndex = 4;
            this.lblStreetId.Text = "Street Id:";
            // 
            // txtStreetId
            // 
            this.txtStreetId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetId.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetId.Location = new System.Drawing.Point(113, 28);
            this.txtStreetId.Name = "txtStreetId";
            this.txtStreetId.ReadOnly = true;
            this.txtStreetId.Size = new System.Drawing.Size(161, 20);
            this.txtStreetId.TabIndex = 5;
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabAdditional);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 409);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(560, 197);
            this.additionalGroup.TabIndex = 10;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Controls.Add(this.pagePhone);
            this.tabAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAdditional.Location = new System.Drawing.Point(3, 16);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(554, 178);
            this.tabAdditional.TabIndex = 0;
            // 
            // pagePhone
            // 
            this.pagePhone.Controls.Add(this.controlPhone);
            this.pagePhone.Location = new System.Drawing.Point(4, 22);
            this.pagePhone.Name = "pagePhone";
            this.pagePhone.Padding = new System.Windows.Forms.Padding(3);
            this.pagePhone.Size = new System.Drawing.Size(546, 152);
            this.pagePhone.TabIndex = 0;
            this.pagePhone.Text = "Phone Number";
            this.pagePhone.UseVisualStyleBackColor = true;
            // 
            // controlPhone
            // 
            this.controlPhone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPhone.Location = new System.Drawing.Point(3, 3);
            this.controlPhone.Name = "controlPhone";
            this.controlPhone.Size = new System.Drawing.Size(540, 146);
            this.controlPhone.TabIndex = 0;
            this.controlPhone.AssociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlPhone_AssociateClick);
            this.controlPhone.DissociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlPhone_DissociateClick);
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.tableLayoutPanel3);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = false;
            this.addressGroup.Location = new System.Drawing.Point(12, 259);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(560, 150);
            this.addressGroup.TabIndex = 9;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtFloor, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnSetFloor, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName2, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtSection, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblSection, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblCity, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtCity, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblPostcode, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblState, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtPostcode, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtState, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetType, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetType, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName, 3, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(554, 130);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtFloor
            // 
            this.txtFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloor.Location = new System.Drawing.Point(113, 3);
            this.txtFloor.Name = "txtFloor";
            this.txtFloor.ReadOnly = true;
            this.txtFloor.Size = new System.Drawing.Size(161, 20);
            this.txtFloor.TabIndex = 1;
            // 
            // btnSetFloor
            // 
            this.btnSetFloor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSetFloor.Location = new System.Drawing.Point(280, 3);
            this.btnSetFloor.Name = "btnSetFloor";
            this.btnSetFloor.Size = new System.Drawing.Size(32, 19);
            this.btnSetFloor.TabIndex = 2;
            this.btnSetFloor.Text = "...";
            this.btnSetFloor.UseVisualStyleBackColor = true;
            this.btnSetFloor.Click += new System.EventHandler(this.btnSetFloor_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Floor:";
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(309, 58);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 13);
            this.lblStreetName2.TabIndex = 7;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(390, 55);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName2.TabIndex = 8;
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 81);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(161, 20);
            this.txtSection.TabIndex = 10;
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 84);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 9;
            this.lblSection.Text = "Section:";
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 110);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 13;
            this.lblCity.Text = "City:";
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 107);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(161, 20);
            this.txtCity.TabIndex = 14;
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(329, 84);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 11;
            this.lblPostcode.Text = "Postcode:";
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(349, 110);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 15;
            this.lblState.Text = "State:";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(390, 81);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(161, 20);
            this.txtPostcode.TabIndex = 12;
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(390, 107);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(161, 20);
            this.txtState.TabIndex = 16;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(42, 32);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 13);
            this.lblStreetType.TabIndex = 3;
            this.lblStreetType.Text = "Street Type:";
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 29);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(161, 20);
            this.txtStreetType.TabIndex = 4;
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(315, 32);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 5;
            this.lblStreetName.Text = "Street Name:";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(390, 29);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(161, 20);
            this.txtStreetName.TabIndex = 6;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.txtAbbreviation, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblAbbreviation, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.lblFilterLevel, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.txtName2, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 5);
            this.tableLayoutPanel.Controls.Add(this.txtName, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblCode, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.btnSetCode, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblName, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lblName2, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtUrl, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.lblUrl, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.numFilterLevel, 3, 4);
            this.tableLayoutPanel.Controls.Add(this.cbNaviStatus, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.lblNaviStatus, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.txtDescription, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblDescription, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 5);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 103);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(560, 156);
            this.tableLayoutPanel.TabIndex = 8;
            // 
            // txtAbbreviation
            // 
            this.txtAbbreviation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAbbreviation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAbbreviation.Location = new System.Drawing.Point(113, 78);
            this.txtAbbreviation.Name = "txtAbbreviation";
            this.txtAbbreviation.Size = new System.Drawing.Size(164, 20);
            this.txtAbbreviation.TabIndex = 10;
            // 
            // lblAbbreviation
            // 
            this.lblAbbreviation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAbbreviation.AutoSize = true;
            this.lblAbbreviation.Location = new System.Drawing.Point(38, 81);
            this.lblAbbreviation.Name = "lblAbbreviation";
            this.lblAbbreviation.Size = new System.Drawing.Size(69, 13);
            this.lblAbbreviation.TabIndex = 9;
            this.lblAbbreviation.Text = "Abbreviation:";
            // 
            // lblFilterLevel
            // 
            this.lblFilterLevel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFilterLevel.AutoSize = true;
            this.lblFilterLevel.Location = new System.Drawing.Point(326, 106);
            this.lblFilterLevel.Name = "lblFilterLevel";
            this.lblFilterLevel.Size = new System.Drawing.Size(61, 13);
            this.lblFilterLevel.TabIndex = 15;
            this.lblFilterLevel.Text = "Filter Level:";
            // 
            // txtName2
            // 
            this.txtName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName2.Location = new System.Drawing.Point(393, 53);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(164, 20);
            this.txtName2.TabIndex = 8;
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(393, 130);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(164, 21);
            this.cbSource.TabIndex = 19;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(113, 53);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(164, 20);
            this.txtName.TabIndex = 6;
            // 
            // lblCode
            // 
            this.lblCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(72, 31);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(35, 13);
            this.lblCode.TabIndex = 2;
            this.lblCode.Text = "Code:";
            // 
            // txtCode
            // 
            this.txtCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode.Location = new System.Drawing.Point(113, 28);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(164, 20);
            this.txtCode.TabIndex = 3;
            // 
            // btnSetCode
            // 
            this.btnSetCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSetCode.Location = new System.Drawing.Point(283, 28);
            this.btnSetCode.Name = "btnSetCode";
            this.btnSetCode.Size = new System.Drawing.Size(32, 19);
            this.btnSetCode.TabIndex = 4;
            this.btnSetCode.Text = "...";
            this.btnSetCode.UseVisualStyleBackColor = true;
            this.btnSetCode.Click += new System.EventHandler(this.btnSetCode_Click);
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(69, 56);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(343, 56);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(44, 13);
            this.lblName2.TabIndex = 7;
            this.lblName2.Text = "Name2:";
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUrl.Location = new System.Drawing.Point(113, 103);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(164, 20);
            this.txtUrl.TabIndex = 14;
            // 
            // lblUrl
            // 
            this.lblUrl.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUrl.AutoSize = true;
            this.lblUrl.Location = new System.Drawing.Point(84, 106);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.Size = new System.Drawing.Size(23, 13);
            this.lblUrl.TabIndex = 13;
            this.lblUrl.Text = "Url:";
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(343, 134);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 18;
            this.lblSource.Text = "Source:";
            // 
            // numFilterLevel
            // 
            this.numFilterLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numFilterLevel.Location = new System.Drawing.Point(393, 103);
            this.numFilterLevel.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numFilterLevel.Name = "numFilterLevel";
            this.numFilterLevel.Size = new System.Drawing.Size(164, 20);
            this.numFilterLevel.TabIndex = 16;
            this.numFilterLevel.Value = 0;
            // 
            // cbNaviStatus
            // 
            this.cbNaviStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNaviStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNaviStatus.FormattingEnabled = true;
            this.cbNaviStatus.Location = new System.Drawing.Point(393, 3);
            this.cbNaviStatus.Name = "cbNaviStatus";
            this.cbNaviStatus.Size = new System.Drawing.Size(164, 21);
            this.cbNaviStatus.TabIndex = 1;
            // 
            // lblNaviStatus
            // 
            this.lblNaviStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNaviStatus.AutoSize = true;
            this.lblNaviStatus.Location = new System.Drawing.Point(293, 6);
            this.lblNaviStatus.Name = "lblNaviStatus";
            this.lblNaviStatus.Size = new System.Drawing.Size(94, 13);
            this.lblNaviStatus.TabIndex = 0;
            this.lblNaviStatus.Text = "Navigation Status:";
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescription.Location = new System.Drawing.Point(393, 78);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(164, 20);
            this.txtDescription.TabIndex = 12;
            // 
            // lblDescription
            // 
            this.lblDescription.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(324, 81);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 11;
            this.lblDescription.Text = "Description:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.chkFamous, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkDisplayText, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 128);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(274, 25);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // chkFamous
            // 
            this.chkFamous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFamous.AutoSize = true;
            this.chkFamous.Location = new System.Drawing.Point(3, 4);
            this.chkFamous.Name = "chkFamous";
            this.chkFamous.Size = new System.Drawing.Size(131, 17);
            this.chkFamous.TabIndex = 0;
            this.chkFamous.Text = "Famous";
            this.chkFamous.UseVisualStyleBackColor = true;
            // 
            // chkDisplayText
            // 
            this.chkDisplayText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDisplayText.AutoSize = true;
            this.chkDisplayText.Location = new System.Drawing.Point(140, 4);
            this.chkDisplayText.Name = "chkDisplayText";
            this.chkDisplayText.Size = new System.Drawing.Size(131, 17);
            this.chkDisplayText.TabIndex = 1;
            this.chkDisplayText.Text = "Display Text";
            this.chkDisplayText.UseVisualStyleBackColor = true;
            // 
            // categoryControl
            // 
            this.categoryControl.Code1Text = "";
            this.categoryControl.Code2Text = "";
            this.categoryControl.Code3Text = "";
            this.categoryControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.categoryControl.Location = new System.Drawing.Point(12, 51);
            this.categoryControl.Name = "categoryControl";
            this.categoryControl.Size = new System.Drawing.Size(560, 52);
            this.categoryControl.TabIndex = 7;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(560, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // PoiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 750);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.categoryControl);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "PoiForm";
            this.Text = "PoiForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.categoryControl, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pagePhone.ResumeLayout(false);
            this.addressGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFilterLevel)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        protected System.Windows.Forms.TextBox txtParentId;
        protected System.Windows.Forms.Label lblParentId;
        protected System.Windows.Forms.Label lblParentType;
        protected System.Windows.Forms.TextBox txtParentType;
        protected System.Windows.Forms.Label lblStreetId;
        protected System.Windows.Forms.TextBox txtStreetId;
        protected Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pagePhone;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Label lblFilterLevel;
        protected System.Windows.Forms.TextBox txtName2;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.TextBox txtName;
        protected System.Windows.Forms.Label lblCode;
        protected System.Windows.Forms.TextBox txtCode;
        protected System.Windows.Forms.Button btnSetCode;
        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.Label lblDescription;
        protected System.Windows.Forms.TextBox txtDescription;
        protected System.Windows.Forms.Label lblName2;
        protected System.Windows.Forms.TextBox txtUrl;
        protected System.Windows.Forms.Label lblUrl;
        protected System.Windows.Forms.Label lblSource;
        protected System.Windows.Forms.CheckBox chkDisplayText;
        protected Controls.IntUpDown numFilterLevel;
        protected Controls.UpperComboBox cbNaviStatus;
        protected System.Windows.Forms.Label lblNaviStatus;
        protected Controls.CodeControl categoryControl;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        protected System.Windows.Forms.Label label2;
        protected Controls.ReferenceControl controlPhone;
        protected System.Windows.Forms.Button btnSetFloor;
        protected System.Windows.Forms.TextBox txtFloor;
        protected System.Windows.Forms.TextBox txtAbbreviation;
        protected System.Windows.Forms.Label lblAbbreviation;
        protected System.Windows.Forms.CheckBox chkFamous;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
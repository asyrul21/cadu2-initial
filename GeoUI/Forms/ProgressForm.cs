﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms
{
    public partial class ProgressForm : BaseForm
    {
        public ProgressForm()
        {
            InitializeComponent();
        }

        int maxValue;

        public void setMinimun(int minimum)
        {
            LoadingProgressBar.Minimum = minimum;
        }
        public void setMaximum(int maximum)
        {
            LoadingProgressBar.Maximum = maximum;
            maxValue = maximum;
        }
        public void setValue(int value)
        {
            LoadingProgressBar.Value = value;
            setText(value);
        }
        public void setText(int value)
        {
            labelText.Text = $"{value}/{maxValue}";
            Refresh();
        }
    }
}

﻿namespace Geomatic.UI.Forms.Edit
{
    partial class EditStreetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // updateGroup
            // 
            this.updateGroup.IsCollapsed = true;
            this.updateGroup.Location = new System.Drawing.Point(12, 424);
            this.updateGroup.Size = new System.Drawing.Size(560, 20);
            // 
            // relationGroup
            // 
            this.relationGroup.Location = new System.Drawing.Point(12, 380);
            // 
            // usageGroup
            // 
            this.usageGroup.IsCollapsed = true;
            this.usageGroup.Size = new System.Drawing.Size(560, 20);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 444);
            // 
            // EditStreetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 658);
            this.Name = "EditStreetForm";
            this.Text = "EditStreetForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
﻿namespace Geomatic.UI.Forms.Edit
{
    partial class EditPropertyMultipleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // txtId
            // 
            this.txtId.Size = new System.Drawing.Size(559, 20);
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(559, 8);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Size = new System.Drawing.Size(559, 37);
            // 
            // EditPropertyMultipleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 181);
            this.Name = "EditPropertyMultipleForm";
            this.Text = "EditPropertyMultipleForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
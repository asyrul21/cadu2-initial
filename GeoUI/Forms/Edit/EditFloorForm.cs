﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;
using static Geomatic.UI.Controls.CollapsibleGroupBox;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditFloorForm : EditFloor
    {
        public EditFloorForm(GFloor floor)
            : base(floor)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Floor Address");

            //updateGroup.IsCollapsed = true;

        }

    }
}

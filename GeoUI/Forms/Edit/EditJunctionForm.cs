﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditJunctionForm : JunctionForm
    {
        public EditJunctionForm(GJunction junction)
            : base(junction)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Junction");
            if(Session.User.GetGroup().Name == "AND")
            {
                // can review only if feature is not in working area
                if (string.IsNullOrEmpty(_junction.AreaId.ToString()) || Int32.Parse(_junction.AreaId.ToString()) == 0)
                {
                    btnApply.Enabled = false;
                }
                else if (_junction.AreaId > 0)
                // can review only if feature is not owner work area
                {
                    // get Work area to check owner work area
                    RepositoryFactory repo = new RepositoryFactory();
                    GWorkArea workarea = repo.GetById<GWorkArea>(_junction.AreaId.Value);
                    if (workarea != null)
                    {
                        if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                        {
                            btnApply.Enabled = false;
                        }
                    }
                }
            }    
        }
    }
}

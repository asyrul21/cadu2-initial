﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Add;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;
using Geomatic.Core;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditLandmarkANDForm : LandmarkANDForm
    {
        public EditLandmarkANDForm(GLandmarkAND landmark)
            : base(landmark)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Landmark AND");
            disablePOIEdit();

            if (_landmarkAND.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                }
            }

            // noraini ali - NOV 2020
            if (_landmarkAND.AreaId > 0)
            {
                // form allow to review data only because working area not owner or work area flag is under verifiying
                RepositoryFactory repo = new RepositoryFactory();
                GWorkArea workarea = repo.GetById<GWorkArea>(_landmarkAND.AreaId.Value);
                if (workarea != null)
                {
                    if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                    {
                        _commandPool.Disable(Command.EditableItem);
                    }
                }
            }
        }
    }
}

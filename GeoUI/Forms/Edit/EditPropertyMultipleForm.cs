﻿using System.Linq;
using System.Collections.Generic;
using Geomatic.UI.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.Core.Rows;
using System.Windows.Forms;
using System;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditPropertyMultipleForm : PropertyMultiForm
    {
        protected List<GProperty> _properties;

        public EditPropertyMultipleForm()
            : this(null)
        {
        }

        public EditPropertyMultipleForm(List<GProperty> properties)
        {
            InitializeComponent();
            _properties = properties;
            RefreshTitle("Edit Multiple Properties");
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateConstructionStatus(cbConstStatus);
            ComboBoxUtils.PopulatePropertyType(cbType);
            ComboBoxUtils.PopulateYear(cbYearInstall);

            SetConstructionStatus();
            SetType();
            SetYear();
            SetSource();
        }

        private void SetConstructionStatus()
        {
            bool isSame = true;
            int? value = _properties[0].ConstructionStatus;
            foreach (GProperty property in _properties)
            {
                isSame &= value == property.ConstructionStatus;
            }
            if (isSame)
            {
                cbConstStatus.Text = _properties[0].ConstructionStatusValue;
            }
        }

        private void SetType()
        {
            bool isSame = true;
            int? value = _properties[0].Type;
            foreach (GProperty property in _properties)
            {
                isSame &= value == property.Type;
            }
            if (isSame)
            {
                cbType.Text = _properties[0].TypeAbbreviationValue + " - " + _properties[0].TypeValue;
            }
        }

        private void SetYear()
        {
            bool isSame = true;
            int? value = _properties[0].YearInstall;
            foreach (GProperty property in _properties)
            {
                isSame &= value == property.YearInstall;
            }
            if (isSame)
            {
                cbYearInstall.Text = _properties[0].YearInstall.ToString();
            }
        }

        private void SetSource()
        {
            bool isSame = true;
            string value = _properties[0].Source;
            foreach (GProperty property in _properties)
            {
                isSame &= value == property.Source;
            }
            if (isSame)
            {
                cbSource.Text = _properties[0].SourceValue;
            }
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Type
            bool isTypeValid = true;
            bool isTypeValid1 = true;
            string propertyMessage = "";
            foreach (GProperty property in _properties)
            {
                isTypeValid &= PropertyValidator.CheckType(SelectedType, property.HasBuilding());

                // get floor address exist and group prop type
                List<GFloor> floors = property.GetFloors().ToList();
                if (!GetGroupPropType("MFA") && floors.Count > 0)
                {
                    isTypeValid1 = false;
                    propertyMessage = propertyMessage + property.OID + "--" + property.TypeAbbreviationValue + "--- floor address exist\n";
                }

                // get building exist
                if (property.HasBuilding())
                {
                    GBuilding building = property.Getbuilding();
                    if (!GetGroupPropType("MSA") && (string.IsNullOrEmpty(building.AndStatus.ToString()) || building.AndStatus == 0))
                    {
                        isTypeValid1 = false;
                        propertyMessage = propertyMessage + property.OID + "--" + property.TypeAbbreviationValue + "--- building exist\n";
                    }
                }
            }

            if (!isTypeValid1)
            {
                string propertyMessage1 = "";
                propertyMessage1 = $"List Property :\n-   OID  -- Type --- Remark:\n";
                propertyMessage1 = propertyMessage1 + propertyMessage;
                propertyMessage1 = propertyMessage1 + "\n Please change within group property type or \n Delete floor address/building";

                MessageBox.Show(propertyMessage1, "Edit Multi Property",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                isTypeValid = isTypeValid1;
            }
            LabelColor(lblType, isTypeValid);
            pass &= isTypeValid;


            return pass;
        }

        public override void SetValues()
        {
            foreach (GProperty property in _properties)
            {
                property.ConstructionStatus = SelectedConstructionStatus;
                property.Type = SelectedType;
                property.Source = SelectedSource.ToString();
                property.YearInstall = SelectedYearInstall;

                // noraini ali - Dec 2021
                // to cater edit multiple number house/lot.
                if (NumIncrement > 0)
                {
                    if (property.House != null)
                    {
                        if (Int32.TryParse(property.House, out int numValue))
                        {
                            if (numValue > 0)
                            {
                                int housenum = numValue + NumIncrement;
                                property.House = housenum.ToString();
                            }
                        }
                    }
                    else
                    {
                        if (Int32.TryParse(property.Lot, out int numValue))
                        {
                            if (numValue > 0)
                            {
                                int lotnum = numValue + NumIncrement;
                                property.Lot = lotnum.ToString();
                            }
                        }
                    }
                }
            }
        }
    }
}

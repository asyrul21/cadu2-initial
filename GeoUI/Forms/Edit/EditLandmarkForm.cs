﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Add;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;
using Geomatic.Core;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditLandmarkForm : LandmarkForm
    {
        public EditLandmarkForm(GLandmark landmark)
            : base(landmark)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Landmark");
            if (Session.User.GetGroup().Name == "AND")
            {
                disablePOIEdit();

                // can review only if feature is not in working area
                if (string.IsNullOrEmpty(_landmark.AreaId.ToString()) || Int32.Parse(_landmark.AreaId.ToString()) == 0)
                {
                    _commandPool.Disable(Command.EditableItem);
                }
                else
                {
                    if (_landmark.AreaId > 0)
                    {
                        // form allow to review data only because working area not owner or work area flag is under verifiying
                        RepositoryFactory repo = new RepositoryFactory();
                        GWorkArea workarea = repo.GetById<GWorkArea>(_landmark.AreaId.Value);
                        if (workarea != null)
                        {
                            if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                            {
                                _commandPool.Disable(Command.EditableItem);
                            }
                        }
                    }
                }
            }

            if (_landmark.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                }
            }
        }
    }
}

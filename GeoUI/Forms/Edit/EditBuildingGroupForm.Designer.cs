﻿namespace Geomatic.UI.Forms.Edit
{
    partial class EditBuildingGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pageBuilding.SuspendLayout();
            this.SuspendLayout();
            // 
            // EditBuildingGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 653);
            this.Name = "EditBuildingGroupForm";
            this.Text = "EditBuildingGroupForm";
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pageBuilding.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
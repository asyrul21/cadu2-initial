﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core;
using Geomatic.Core.Search;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditPropertyANDForm : PropertyANDForm
    {
        public EditPropertyANDForm(GPropertyAND property)
            : base(property)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Property AND");
            disablePOIEdit();

            if (_propertyAND.HasBuildingAND())
            {
                controlFloor.Enabled = false;
                controlPoi.Enabled = false;
            }
            else
            {
                if (_propertyAND.HasBuilding())
                {
                    GBuilding building = _propertyAND.Getbuilding();
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        controlFloor.Enabled = false;
                        controlPoi.Enabled = false;
                    }
                }
            }

            if (_propertyAND.AreaId > 0)
            {
                // form allow to review data only because working area not owner or work area flag is under verifiying
                RepositoryFactory repo = new RepositoryFactory();
                GWorkArea workarea = repo.GetById<GWorkArea>(_propertyAND.AreaId.Value);
                if (workarea != null)
                {
                    if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                    {
                        _commandPool.Disable(Command.EditableItem);
                        controlFloor.Enabled = false;
                        controlPoi.Enabled = false;
                    }
                }
            } 
        }
    }
}

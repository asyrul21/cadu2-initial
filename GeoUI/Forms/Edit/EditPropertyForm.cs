﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core;
using Geomatic.Core.Search;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditPropertyForm : PropertyForm
    {
        public EditPropertyForm(GProperty property)
            : base(property)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Property");
            if (Session.User.GetGroup().Name == "AND")
            {
                disablePOIEdit();

                // form allow to review data only because feature is not in working area
                if (string.IsNullOrEmpty(_property.AreaId.ToString()) || Int32.Parse(_property.AreaId.ToString()) == 0)
                {
                    _commandPool.Disable(Command.EditableItem);
                    controlFloor.Enabled = false;
                    controlPoi.Enabled = false;
                }
                else
                {
                    // form allow to review data only because working area not owner or work area flag is under verifiying
                    RepositoryFactory repo = new RepositoryFactory();
                    GWorkArea workarea = repo.GetById<GWorkArea>(_property.AreaId.Value);
                    if (workarea != null) // noraini - cater for dangling feature
                    {
                        if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                        {
                            _commandPool.Disable(Command.EditableItem);
                            controlFloor.Enabled = false;
                            controlPoi.Enabled = false;
                        }
                    }
                }
            }
          
            if (_property.HasBuilding())
            {
                controlFloor.Enabled = false;
                controlPoi.Enabled = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;
using Geomatic.Core.Rows;
using Geomatic.UI.Commands;
using Geomatic.Core.Repositories;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditStreetForm : StreetForm
    {
        public EditStreetForm(GStreet street)
            : base(street)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Street");

            if (_street.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                    usageGroup.IsCollapsed = true;
                }
            }

            if (Session.User.GetGroup().Name == "AND")
            {
                usageGroup.IsCollapsed = true;
                cbConstructionStatus.Enabled = false;
                cbNetworkClass.Enabled = false;
                cbFilterLevel.Enabled = false;
                cbTollType.Enabled = false;
                cbDivider.Enabled = false;
                cbNaviStatus.Enabled = false;
                cbCategory.Enabled = false;
                cbClass.Enabled = false;
                cbDesign.Enabled = false;
                cbDirection.Enabled = false;

                if (string.IsNullOrEmpty(_street.AreaId.ToString()) || Int32.Parse(_street.AreaId.ToString()) == 0)
                {
                    btnApply.Enabled = false;
                }
                else if (_street.AreaId > 0)
                // can review only if feature is not owner work area
                {
                    // get Work area to check owner work area
                    RepositoryFactory repo = new RepositoryFactory();
                    GWorkArea workarea = repo.GetById<GWorkArea>(_street.AreaId.Value);
                    if (workarea != null)
                    {
                        if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                        {
                            btnApply.Enabled = false;
                        }
                    }
                }
            }    
        }
    }
}

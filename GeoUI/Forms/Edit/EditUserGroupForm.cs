﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.Core.Repositories;
using Geomatic.Core;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditUserGroupForm : UserGroupForm
    {
        protected GUserGroup _userGroup;

        public EditUserGroupForm(GUserGroup userGroup)
        {
            InitializeComponent();
            _userGroup = userGroup;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit User Group");

            PopulateAttribute();            
            PopulateUsers();
        }

        protected override void PopulateFunctions()
        {
            List<GFunction> userFunctions = _userGroup.GetFunctions().ToList();

            foreach (GFunction function in GFunction.GetRoot())
            {
                TreeNode newNode = new TreeNode(function.Name);
                newNode.Tag = function;
                int index = tvFunction.Nodes.Add(newNode);

                foreach (GFunction subFunction in function.GetChild())
                {
                    TreeNode newSubNode = new TreeNode(subFunction.Name);
                    newSubNode.Tag = subFunction;
                    newSubNode.Checked = userFunctions.Contains(subFunction);
                    tvFunction.Nodes[index].Nodes.Add(newSubNode);
                }
            }
        }

        private void PopulateUsers()
        {
            controlUsers.BeginUpdate();
            controlUsers.Sorting = SortOrder.None;
            controlUsers.Items.Clear();
            List<ListViewItem> items = new List<ListViewItem>();
            foreach (GUser user in _userGroup.GetUsers())
            {
                ListViewItem item = CreateUserItem(user);
                items.Add(item);
            }
            controlUsers.Items.AddRange(items.ToArray());
            controlUsers.FixColumnWidth();
            controlUsers.EndUpdate();
        }

        private void PopulateAttribute()
        {
            txtName.Text = _userGroup.Name;
        }
    }
}

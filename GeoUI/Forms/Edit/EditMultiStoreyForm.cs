﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditMultiStoreyForm : MultiStoreyForm
    {
        public EditMultiStoreyForm(GMultiStorey multiStorey)
            : base(multiStorey)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit MultiStorey Address");
        }
    }
}

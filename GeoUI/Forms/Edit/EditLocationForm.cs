﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using GState = Geomatic.Core.Features.GState;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditLocationForm : LocationForm
    {
        protected GLocation _location;

        public EditLocationForm(GLocation location)            
        {
            InitializeComponent();
            _location = location;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Location");

            PopulateAttribute();
            
        }

        private void PopulateAttribute()
        {
            txtId.Text = _location.OID.ToString();
            cbSection.Text = _location.Section;
            cbCity.Text = _location.City;
            //cbState.Text = _location.State;
            cbState.Text = GetStateName(); // added by noraini
            chkDeleted.Checked = _location.DelFlag == 1;
            txtDateCreated.Text = _location.DateCreated;
            txtDateUpdated.Text = _location.DateUpdated;
            txtCreator.Text = _location.CreatedBy;
            txtUpdater.Text = _location.UpdatedBy;
        }

        // added by noraini - Aug 2019
        private string GetStateName()
        {
            string StateName = "";
            string value = _location.State;
            foreach (GState state in GState.GetAll(false))
            {
                if (value == state.Code)
                {
                    StateName = state.Name;
                    return StateName;
                }
            }
            return StateName;
        }
        //end
    }
}

﻿using Geomatic.Core.Features;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditSectionBoundaryForm : SectionBoundaryForm
    {
        public EditSectionBoundaryForm(GSectionBoundary sectionBoundary)
            : base(sectionBoundary)
        {
            InitializeComponent();
            RefreshTitle("Edit Section Boundary");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditUserForm : UserForm
    {
        protected GUser _user;

        public EditUserForm(GUser user)
        {
            InitializeComponent();
            _user = user;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit User");

            PopulateAttribute();
        }

        private void PopulateAttribute()
        {
            txtUsername.Text = _user.Name;
            txtPassword.Text = _user.Password;
            GUserGroup userGroup = _user.GetGroup();
            if (userGroup != null)
            {
                txtGroup.Text = userGroup.Name;
            }
        }

        protected override bool ValidateValues()
        {
            bool pass = base.ValidateValues();

            if (!pass)
            {
                return false;
            }

            // check if name already exists
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUser> query = new Query<GUser>();
            query.Obj.Name = txtUsername.Text;

            bool isOk = true;

            foreach (GUser user in repo.Search(query))
            {
                isOk &= user.Equals(_user);
            }

            lblUsername.ForeColor = isOk ? Color.Black : Color.Red;

            pass &= isOk;

            return pass;
        }
    }
}

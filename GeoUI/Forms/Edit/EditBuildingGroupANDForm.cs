﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditBuildingGroupANDForm : BuildingGroupANDForm
    {
        public EditBuildingGroupANDForm(GBuildingGroupAND buildingGroup)
            : base(buildingGroup)
        {
            InitializeComponent();
            _buildingGroupAND = buildingGroup;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Building Group AND");

            if (_buildingGroupAND.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                }
            }

            if (_buildingGroupAND.AreaId > 0)
            {
                // form allow to review data only because working area not owner or work area flag is under verifiying
                RepositoryFactory repo = new RepositoryFactory();
                GWorkArea workarea = repo.GetById<GWorkArea>(_buildingGroupAND.AreaId.Value);
                if (workarea != null)
                {
                    if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                    {
                        _commandPool.Disable(Command.EditableItem);
                        //controlBuilding.Enabled = false;
                    }
                }
            }
            addressGroup.IsCollapsed = true;
        }
    }
}

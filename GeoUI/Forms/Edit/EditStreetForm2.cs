﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;
using Geomatic.Core.Rows;
using Geomatic.UI.Commands;
using Geomatic.Core.Repositories;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditStreetForm2 : StreetForm
    {
        public EditStreetForm2(GStreet street)
            : base(street)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Street Section by Supervisor");

            generalLayout.Enabled = false;
            usageLayout.Enabled = false;
            usageLayout2.Enabled = false;
            relationGroup.Enabled = false;
            updateGroup.Enabled = false;

            cbType.Enabled = false;
            cbName.Enabled = false;
            cbName2.Enabled = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditBuildingForm : BuildingForm
    {
        public EditBuildingForm(GBuilding building)
            : base(building)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Building");
            if (Session.User.GetGroup().Name == "AND")
            {
                disablePOIEdit();

                // can review only if feature is not in working area
                if (string.IsNullOrEmpty(_building.AreaId.ToString()) || Int32.Parse(_building.AreaId.ToString()) == 0)
                {
                    _commandPool.Disable(Command.EditableItem);
                    controlMultiStorey.Enabled = false;
                }
                else
                {
                    if (_building.AreaId > 0)
                    {
                        // form allow to review data only because working area not owner or work area flag is under verifiying
                        RepositoryFactory repo = new RepositoryFactory();
                        GWorkArea workarea = repo.GetById<GWorkArea>(_building.AreaId.Value);
                        if (workarea != null)
                        {
                            if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                            {
                                _commandPool.Disable(Command.EditableItem);
                                controlMultiStorey.Enabled = false;
                            }
                        }
                    }
                }
            }
            
            if (_building.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                }
            }

            addressGroup.IsCollapsed = true;
        }
    }
}

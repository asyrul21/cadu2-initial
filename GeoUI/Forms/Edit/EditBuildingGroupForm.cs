﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditBuildingGroupForm : BuildingGroupForm
    {
        public EditBuildingGroupForm(GBuildingGroup buildingGroup)
            : base(buildingGroup)
        {
            InitializeComponent();
            _buildingGroup = buildingGroup;
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Building Group");

            // noraini ali - OKT 2020
            if (Session.User.GetGroup().Name == "AND")
            {
                // can review only if feature is not in working area
                if (string.IsNullOrEmpty(_buildingGroup.AreaId.ToString()) || Int32.Parse(_buildingGroup.AreaId.ToString()) == 0)
                {
                    _commandPool.Disable(Command.EditableItem);
                    //controlBuilding.Enabled = false;
                }
                else
                {
                    if (_buildingGroup.AreaId > 0)
                    {
                        // form allow to review data only because working area not owner or work area flag is under verifiying
                        RepositoryFactory repo = new RepositoryFactory();
                        GWorkArea workarea = repo.GetById<GWorkArea>(_buildingGroup.AreaId.Value);
                        if (workarea != null)
                        {
                            if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                            {
                                _commandPool.Disable(Command.EditableItem);
                                controlBuilding.Enabled = false;
                            }
                        }
                    }
                }
            }

            if (_buildingGroup.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);

                    // noraini ali - OKT 2020
                    if (Session.User.GetGroup().Name == "MLI_UPDATE")
                    {
                        controlBuilding.Enabled = false;
                    }
                }
            }
            addressGroup.IsCollapsed = true;
        }
    }
}

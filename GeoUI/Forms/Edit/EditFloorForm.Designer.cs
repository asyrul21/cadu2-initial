﻿namespace Geomatic.UI.Forms.Edit
{
    partial class EditFloorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // updateGroup
            // 
            this.updateGroup.Size = new System.Drawing.Size(582, 70);
            // 
            // txtId
            // 
            this.txtId.Size = new System.Drawing.Size(582, 20);
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(582, 8);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Size = new System.Drawing.Size(582, 37);
            // 
            // EditFloorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 257);
            this.Name = "EditFloorForm";
            this.Text = "EditFloorForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
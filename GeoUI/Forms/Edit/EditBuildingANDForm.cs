﻿using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;

namespace Geomatic.UI.Forms.Edit
{
    public partial class EditBuildingANDForm : BuildingANDForm
    {
        public EditBuildingANDForm(GBuildingAND building)
            : base(building)
        {
            InitializeComponent();
        }

        protected override void Form_Load()
        {
            base.Form_Load();
            RefreshTitle("Edit Building AND");
            disablePOIEdit();

            if (_buildingAND.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    _commandPool.Disable(Command.EditableItem);
                }
            }

            // noraini ali - NOV 2020
            if (_buildingAND.AreaId > 0)
            {
                // form allow to review data only because working area not owner or work area flag is under verifiying
                RepositoryFactory repo = new RepositoryFactory();
                GWorkArea workarea = repo.GetById<GWorkArea>(_buildingAND.AreaId.Value);
                if (workarea != null)
                {
                    if (workarea.user_id.ToUpper() != Session.User.Name || workarea.flag == "2")
                    {
                        _commandPool.Disable(Command.EditableItem);
                    }
                }
            }

            addressGroup.IsCollapsed = true;
        }
    }
}

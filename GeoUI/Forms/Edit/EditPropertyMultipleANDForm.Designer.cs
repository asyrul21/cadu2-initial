﻿namespace Geomatic.UI.Forms.Edit
{
    partial class EditPropertyMultipleANDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // txtId
            // 
            this.txtId.Size = new System.Drawing.Size(595, 20);
            // 
            // panel3
            // 
            this.panel3.Size = new System.Drawing.Size(595, 8);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Size = new System.Drawing.Size(595, 37);
            // 
            // EditPropertyMultipleANDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 186);
            this.Name = "EditPropertyMultipleANDForm";
            this.Text = "EditPropertyMultipleANDForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
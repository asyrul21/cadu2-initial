﻿namespace Geomatic.UI.Forms
{
    partial class PropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblStreetId;
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.lblCreator = new System.Windows.Forms.Label();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.relationGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtStreetId = new System.Windows.Forms.TextBox();
            this.additionalGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pagePoi = new System.Windows.Forms.TabPage();
            this.controlPoi = new Geomatic.UI.Controls.ChildControl();
            this.pageFloor = new System.Windows.Forms.TabPage();
            this.controlFloor = new Geomatic.UI.Controls.ChildControl();
            this.addressGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtStreetType = new System.Windows.Forms.TextBox();
            this.lblStreetType = new System.Windows.Forms.Label();
            this.lblStreetName = new System.Windows.Forms.Label();
            this.lblStreetName2 = new System.Windows.Forms.Label();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtSection = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.txtStreetName2 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblType = new System.Windows.Forms.Label();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.lblConstructionStatus = new System.Windows.Forms.Label();
            this.lblLot = new System.Windows.Forms.Label();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.lblHouse = new System.Windows.Forms.Label();
            this.txtHouse = new System.Windows.Forms.TextBox();
            this.cbConstStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.lblYearInstall = new System.Windows.Forms.Label();
            this.lblSource = new System.Windows.Forms.Label();
            this.cbYearInstall = new System.Windows.Forms.ComboBox();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblNumOfFloors = new System.Windows.Forms.Label();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.txtTotUnit = new System.Windows.Forms.TextBox();
            this.lblTotUnit = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            lblStreetId = new System.Windows.Forms.Label();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.additionalGroup.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pagePoi.SuspendLayout();
            this.pageFloor.SuspendLayout();
            this.addressGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 582);
            this.bottomPanel.Size = new System.Drawing.Size(550, 37);
            // 
            // lblStreetId
            // 
            lblStreetId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lblStreetId.AutoSize = true;
            lblStreetId.Location = new System.Drawing.Point(57, 6);
            lblStreetId.Name = "lblStreetId";
            lblStreetId.Size = new System.Drawing.Size(50, 13);
            lblStreetId.TabIndex = 0;
            lblStreetId.Text = "Street Id:";
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel2);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 512);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(550, 70);
            this.updateGroup.TabIndex = 11;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(544, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(385, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(156, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(46, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(385, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(156, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(113, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(156, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(42, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(302, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(113, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(156, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(306, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.tableLayoutPanel4);
            this.relationGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.relationGroup.IsCollapsed = false;
            this.relationGroup.Location = new System.Drawing.Point(12, 468);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(550, 44);
            this.relationGroup.TabIndex = 10;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relation";
            this.relationGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.txtStreetId, 1, 0);
            this.tableLayoutPanel4.Controls.Add(lblStreetId, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(544, 25);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtStreetId
            // 
            this.txtStreetId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetId.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetId.Location = new System.Drawing.Point(113, 3);
            this.txtStreetId.Name = "txtStreetId";
            this.txtStreetId.ReadOnly = true;
            this.txtStreetId.Size = new System.Drawing.Size(156, 20);
            this.txtStreetId.TabIndex = 1;
            // 
            // additionalGroup
            // 
            this.additionalGroup.Controls.Add(this.tabAdditional);
            this.additionalGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.additionalGroup.IsCollapsed = false;
            this.additionalGroup.Location = new System.Drawing.Point(12, 271);
            this.additionalGroup.Name = "additionalGroup";
            this.additionalGroup.Size = new System.Drawing.Size(550, 197);
            this.additionalGroup.TabIndex = 9;
            this.additionalGroup.TabStop = false;
            this.additionalGroup.Text = "Additional Properties";
            this.additionalGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Controls.Add(this.pagePoi);
            this.tabAdditional.Controls.Add(this.pageFloor);
            this.tabAdditional.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAdditional.Location = new System.Drawing.Point(3, 16);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(544, 178);
            this.tabAdditional.TabIndex = 3;
            // 
            // pagePoi
            // 
            this.pagePoi.Controls.Add(this.controlPoi);
            this.pagePoi.Location = new System.Drawing.Point(4, 22);
            this.pagePoi.Name = "pagePoi";
            this.pagePoi.Padding = new System.Windows.Forms.Padding(3);
            this.pagePoi.Size = new System.Drawing.Size(536, 152);
            this.pagePoi.TabIndex = 1;
            this.pagePoi.Text = "POI";
            this.pagePoi.UseVisualStyleBackColor = true;
            // 
            // controlPoi
            // 
            this.controlPoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPoi.Location = new System.Drawing.Point(3, 3);
            this.controlPoi.Name = "controlPoi";
            this.controlPoi.Size = new System.Drawing.Size(530, 146);
            this.controlPoi.TabIndex = 0;
            this.controlPoi.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_AddClick);
            this.controlPoi.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_EditClick);
            this.controlPoi.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlPoi_DeleteClick);
            // 
            // pageFloor
            // 
            this.pageFloor.Controls.Add(this.controlFloor);
            this.pageFloor.Location = new System.Drawing.Point(4, 22);
            this.pageFloor.Name = "pageFloor";
            this.pageFloor.Padding = new System.Windows.Forms.Padding(3);
            this.pageFloor.Size = new System.Drawing.Size(536, 152);
            this.pageFloor.TabIndex = 0;
            this.pageFloor.Text = "Floor Address";
            this.pageFloor.UseVisualStyleBackColor = true;
            // 
            // controlFloor
            // 
            this.controlFloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlFloor.Location = new System.Drawing.Point(3, 3);
            this.controlFloor.Name = "controlFloor";
            this.controlFloor.Size = new System.Drawing.Size(530, 146);
            this.controlFloor.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.controlFloor.TabIndex = 0;
            this.controlFloor.AddClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlFloor_AddClick);
            this.controlFloor.EditClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlFloor_EditClick);
            this.controlFloor.DeleteClick += new Geomatic.UI.Controls.ChildControl.ClickEventHandler(this.controlFloor_DeleteClick);
            // 
            // addressGroup
            // 
            this.addressGroup.Controls.Add(this.tableLayoutPanel3);
            this.addressGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.addressGroup.IsCollapsed = false;
            this.addressGroup.Location = new System.Drawing.Point(12, 151);
            this.addressGroup.Name = "addressGroup";
            this.addressGroup.Size = new System.Drawing.Size(550, 120);
            this.addressGroup.TabIndex = 8;
            this.addressGroup.TabStop = false;
            this.addressGroup.Text = "Address";
            this.addressGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtPostcode, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetType, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetType, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblStreetName2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblSection, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtSection, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblCity, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtState, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtCity, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblState, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblPostcode, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtStreetName2, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(544, 101);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtPostcode
            // 
            this.txtPostcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPostcode.Location = new System.Drawing.Point(385, 53);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(156, 20);
            this.txtPostcode.TabIndex = 9;
            // 
            // txtStreetName
            // 
            this.txtStreetName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName.Location = new System.Drawing.Point(385, 3);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.ReadOnly = true;
            this.txtStreetName.Size = new System.Drawing.Size(156, 20);
            this.txtStreetName.TabIndex = 3;
            // 
            // txtStreetType
            // 
            this.txtStreetType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetType.Location = new System.Drawing.Point(113, 3);
            this.txtStreetType.Name = "txtStreetType";
            this.txtStreetType.ReadOnly = true;
            this.txtStreetType.Size = new System.Drawing.Size(156, 20);
            this.txtStreetType.TabIndex = 1;
            // 
            // lblStreetType
            // 
            this.lblStreetType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetType.AutoSize = true;
            this.lblStreetType.Location = new System.Drawing.Point(42, 6);
            this.lblStreetType.Name = "lblStreetType";
            this.lblStreetType.Size = new System.Drawing.Size(65, 13);
            this.lblStreetType.TabIndex = 0;
            this.lblStreetType.Text = "Street Type:";
            // 
            // lblStreetName
            // 
            this.lblStreetName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(310, 6);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(69, 13);
            this.lblStreetName.TabIndex = 2;
            this.lblStreetName.Text = "Street Name:";
            // 
            // lblStreetName2
            // 
            this.lblStreetName2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStreetName2.AutoSize = true;
            this.lblStreetName2.Location = new System.Drawing.Point(304, 31);
            this.lblStreetName2.Name = "lblStreetName2";
            this.lblStreetName2.Size = new System.Drawing.Size(75, 13);
            this.lblStreetName2.TabIndex = 4;
            this.lblStreetName2.Text = "Street Name2:";
            // 
            // lblSection
            // 
            this.lblSection.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSection.AutoSize = true;
            this.lblSection.Location = new System.Drawing.Point(61, 56);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(46, 13);
            this.lblSection.TabIndex = 6;
            this.lblSection.Text = "Section:";
            // 
            // txtSection
            // 
            this.txtSection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSection.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSection.Location = new System.Drawing.Point(113, 53);
            this.txtSection.Name = "txtSection";
            this.txtSection.ReadOnly = true;
            this.txtSection.Size = new System.Drawing.Size(156, 20);
            this.txtSection.TabIndex = 7;
            // 
            // lblCity
            // 
            this.lblCity.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(80, 81);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 10;
            this.lblCity.Text = "City:";
            // 
            // txtState
            // 
            this.txtState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(385, 78);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(156, 20);
            this.txtState.TabIndex = 13;
            // 
            // txtCity
            // 
            this.txtCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(113, 78);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(156, 20);
            this.txtCity.TabIndex = 11;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(344, 81);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 12;
            this.lblState.Text = "State:";
            // 
            // lblPostcode
            // 
            this.lblPostcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(324, 56);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(55, 13);
            this.lblPostcode.TabIndex = 8;
            this.lblPostcode.Text = "Postcode:";
            // 
            // txtStreetName2
            // 
            this.txtStreetName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStreetName2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStreetName2.Location = new System.Drawing.Point(385, 28);
            this.txtStreetName2.Name = "txtStreetName2";
            this.txtStreetName2.ReadOnly = true;
            this.txtStreetName2.Size = new System.Drawing.Size(156, 20);
            this.txtStreetName2.TabIndex = 5;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblType, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbType, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblConstructionStatus, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lblLot, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.txtLot, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblHouse, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtHouse, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.cbConstStatus, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblYearInstall, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.cbYearInstall, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.lblNumOfFloors, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.numOfFloors, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.txtTotUnit, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.lblTotUnit, 2, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 51);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(550, 100);
            this.tableLayoutPanel.TabIndex = 7;
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(73, 31);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type:";
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(113, 28);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(159, 21);
            this.cbType.TabIndex = 3;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // lblConstructionStatus
            // 
            this.lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblConstructionStatus.AutoSize = true;
            this.lblConstructionStatus.Location = new System.Drawing.Point(5, 6);
            this.lblConstructionStatus.Name = "lblConstructionStatus";
            this.lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            this.lblConstructionStatus.TabIndex = 0;
            this.lblConstructionStatus.Text = "Construction Status:";
            // 
            // lblLot
            // 
            this.lblLot.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLot.AutoSize = true;
            this.lblLot.Location = new System.Drawing.Point(82, 56);
            this.lblLot.Name = "lblLot";
            this.lblLot.Size = new System.Drawing.Size(25, 13);
            this.lblLot.TabIndex = 6;
            this.lblLot.Text = "Lot:";
            // 
            // txtLot
            // 
            this.txtLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLot.BackColor = System.Drawing.Color.White;
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Location = new System.Drawing.Point(113, 53);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(159, 20);
            this.txtLot.TabIndex = 7;
            // 
            // lblHouse
            // 
            this.lblHouse.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHouse.AutoSize = true;
            this.lblHouse.Location = new System.Drawing.Point(341, 56);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(41, 13);
            this.lblHouse.TabIndex = 8;
            this.lblHouse.Text = "House:";
            // 
            // txtHouse
            // 
            this.txtHouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHouse.BackColor = System.Drawing.Color.White;
            this.txtHouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHouse.Location = new System.Drawing.Point(388, 53);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(159, 20);
            this.txtHouse.TabIndex = 9;
            // 
            // cbConstStatus
            // 
            this.cbConstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConstStatus.FormattingEnabled = true;
            this.cbConstStatus.Location = new System.Drawing.Point(113, 3);
            this.cbConstStatus.Name = "cbConstStatus";
            this.cbConstStatus.Size = new System.Drawing.Size(159, 21);
            this.cbConstStatus.TabIndex = 1;
            // 
            // lblYearInstall
            // 
            this.lblYearInstall.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblYearInstall.AutoSize = true;
            this.lblYearInstall.Location = new System.Drawing.Point(45, 81);
            this.lblYearInstall.Name = "lblYearInstall";
            this.lblYearInstall.Size = new System.Drawing.Size(62, 13);
            this.lblYearInstall.TabIndex = 10;
            this.lblYearInstall.Text = "Year Install:";
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(338, 81);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 12;
            this.lblSource.Text = "Source:";
            // 
            // cbYearInstall
            // 
            this.cbYearInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbYearInstall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYearInstall.FormattingEnabled = true;
            this.cbYearInstall.Location = new System.Drawing.Point(113, 78);
            this.cbYearInstall.Name = "cbYearInstall";
            this.cbYearInstall.Size = new System.Drawing.Size(159, 21);
            this.cbYearInstall.TabIndex = 11;
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(388, 78);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(159, 21);
            this.cbSource.TabIndex = 13;
            // 
            // lblNumOfFloors
            // 
            this.lblNumOfFloors.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNumOfFloors.AutoSize = true;
            this.lblNumOfFloors.Location = new System.Drawing.Point(304, 6);
            this.lblNumOfFloors.Name = "lblNumOfFloors";
            this.lblNumOfFloors.Size = new System.Drawing.Size(78, 13);
            this.lblNumOfFloors.TabIndex = 4;
            this.lblNumOfFloors.Text = "Num of Floors :";
            this.lblNumOfFloors.Visible = false;
            // 
            // numOfFloors
            // 
            this.numOfFloors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloors.Enabled = false;
            this.numOfFloors.Location = new System.Drawing.Point(388, 3);
            this.numOfFloors.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.ReadOnly = true;
            this.numOfFloors.Size = new System.Drawing.Size(159, 20);
            this.numOfFloors.TabIndex = 5;
            this.numOfFloors.Value = 1;
            this.numOfFloors.Visible = false;
            // 
            // txtTotUnit
            // 
            this.txtTotUnit.Location = new System.Drawing.Point(388, 28);
            this.txtTotUnit.Name = "txtTotUnit";
            this.txtTotUnit.Size = new System.Drawing.Size(100, 20);
            this.txtTotUnit.TabIndex = 14;
            // 
            // lblTotUnit
            // 
            this.lblTotUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTotUnit.AutoSize = true;
            this.lblTotUnit.Location = new System.Drawing.Point(323, 31);
            this.lblTotUnit.Name = "lblTotUnit";
            this.lblTotUnit.Size = new System.Drawing.Size(59, 13);
            this.lblTotUnit.TabIndex = 15;
            this.lblTotUnit.Text = "Total Unit :";
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(550, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(550, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(49, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Objectid:";
            // 
            // PropertyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 619);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.relationGroup);
            this.Controls.Add(this.additionalGroup);
            this.Controls.Add(this.addressGroup);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "PropertyForm";
            this.Text = "PropertyForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.addressGroup, 0);
            this.Controls.SetChildIndex(this.additionalGroup, 0);
            this.Controls.SetChildIndex(this.relationGroup, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.additionalGroup.ResumeLayout(false);
            this.tabAdditional.ResumeLayout(false);
            this.pagePoi.ResumeLayout(false);
            this.pageFloor.ResumeLayout(false);
            this.addressGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected Controls.CollapsibleGroupBox relationGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        protected System.Windows.Forms.TextBox txtStreetId;
        protected Controls.CollapsibleGroupBox additionalGroup;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pageFloor;
        protected Controls.ChildControl controlFloor;
        protected System.Windows.Forms.TabPage pagePoi;
        protected Controls.ChildControl controlPoi;
        protected Controls.CollapsibleGroupBox addressGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.TextBox txtPostcode;
        protected System.Windows.Forms.TextBox txtStreetName;
        protected System.Windows.Forms.TextBox txtStreetType;
        protected System.Windows.Forms.Label lblStreetType;
        protected System.Windows.Forms.Label lblStreetName;
        protected System.Windows.Forms.Label lblStreetName2;
        protected System.Windows.Forms.Label lblSection;
        protected System.Windows.Forms.TextBox txtSection;
        protected System.Windows.Forms.Label lblCity;
        protected System.Windows.Forms.TextBox txtState;
        protected System.Windows.Forms.TextBox txtCity;
        protected System.Windows.Forms.Label lblState;
        protected System.Windows.Forms.Label lblPostcode;
        protected System.Windows.Forms.TextBox txtStreetName2;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Label lblType;
        protected Controls.UpperComboBox cbType;
        protected System.Windows.Forms.Label lblNumOfFloors;
        protected Controls.IntUpDown numOfFloors;
        protected System.Windows.Forms.Label lblConstructionStatus;
        protected System.Windows.Forms.Label lblLot;
        protected System.Windows.Forms.TextBox txtLot;
        protected System.Windows.Forms.Label lblHouse;
        protected System.Windows.Forms.TextBox txtHouse;
        protected Controls.UpperComboBox cbConstStatus;
        protected System.Windows.Forms.Label lblYearInstall;
        protected System.Windows.Forms.Label lblSource;
        protected System.Windows.Forms.ComboBox cbYearInstall;
        protected Controls.UpperComboBox cbSource;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtTotUnit;
        private System.Windows.Forms.Label lblTotUnit;
    }
}
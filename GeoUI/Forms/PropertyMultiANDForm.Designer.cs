﻿namespace Geomatic.UI.Forms
{
    partial class PropertyMultiANDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblType = new System.Windows.Forms.Label();
            this.cbType = new Geomatic.UI.Controls.UpperComboBox();
            this.lblConstructionStatus = new System.Windows.Forms.Label();
            this.cbConstStatus = new Geomatic.UI.Controls.UpperComboBox();
            this.cbYearInstall = new System.Windows.Forms.ComboBox();
            this.lblYearInstall = new System.Windows.Forms.Label();
            this.cbSource = new Geomatic.UI.Controls.UpperComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblTotUnit = new System.Windows.Forms.Label();
            this.txtTotUnit = new System.Windows.Forms.TextBox();
            this.lblNumOfFloors = new System.Windows.Forms.Label();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.numIncrement = new Geomatic.UI.Controls.IntUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblId = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).BeginInit();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 151);
            this.bottomPanel.Size = new System.Drawing.Size(550, 37);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblType, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cbType, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblConstructionStatus, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.cbConstStatus, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.cbYearInstall, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblYearInstall, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.cbSource, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.lblSource, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblTotUnit, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.txtTotUnit, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.lblNumOfFloors, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.numOfFloors, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.numIncrement, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 51);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(550, 100);
            this.tableLayoutPanel.TabIndex = 7;
            // 
            // lblType
            // 
            this.lblType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(73, 32);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 2;
            this.lblType.Text = "Type:";
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(113, 29);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(159, 21);
            this.cbType.TabIndex = 3;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // lblConstructionStatus
            // 
            this.lblConstructionStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblConstructionStatus.AutoSize = true;
            this.lblConstructionStatus.Location = new System.Drawing.Point(5, 6);
            this.lblConstructionStatus.Name = "lblConstructionStatus";
            this.lblConstructionStatus.Size = new System.Drawing.Size(102, 13);
            this.lblConstructionStatus.TabIndex = 0;
            this.lblConstructionStatus.Text = "Construction Status:";
            // 
            // cbConstStatus
            // 
            this.cbConstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConstStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConstStatus.FormattingEnabled = true;
            this.cbConstStatus.Location = new System.Drawing.Point(113, 3);
            this.cbConstStatus.Name = "cbConstStatus";
            this.cbConstStatus.Size = new System.Drawing.Size(159, 21);
            this.cbConstStatus.TabIndex = 1;
            // 
            // cbYearInstall
            // 
            this.cbYearInstall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbYearInstall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYearInstall.FormattingEnabled = true;
            this.cbYearInstall.Location = new System.Drawing.Point(113, 55);
            this.cbYearInstall.Name = "cbYearInstall";
            this.cbYearInstall.Size = new System.Drawing.Size(159, 21);
            this.cbYearInstall.TabIndex = 11;
            // 
            // lblYearInstall
            // 
            this.lblYearInstall.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblYearInstall.AutoSize = true;
            this.lblYearInstall.Location = new System.Drawing.Point(45, 58);
            this.lblYearInstall.Name = "lblYearInstall";
            this.lblYearInstall.Size = new System.Drawing.Size(62, 13);
            this.lblYearInstall.TabIndex = 10;
            this.lblYearInstall.Text = "Year Install:";
            // 
            // cbSource
            // 
            this.cbSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSource.FormattingEnabled = true;
            this.cbSource.Location = new System.Drawing.Point(388, 29);
            this.cbSource.Name = "cbSource";
            this.cbSource.Size = new System.Drawing.Size(159, 21);
            this.cbSource.TabIndex = 13;
            // 
            // lblSource
            // 
            this.lblSource.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(338, 32);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 12;
            this.lblSource.Text = "Source:";
            // 
            // lblTotUnit
            // 
            this.lblTotUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTotUnit.AutoSize = true;
            this.lblTotUnit.Location = new System.Drawing.Point(323, 58);
            this.lblTotUnit.Name = "lblTotUnit";
            this.lblTotUnit.Size = new System.Drawing.Size(59, 13);
            this.lblTotUnit.TabIndex = 15;
            this.lblTotUnit.Text = "Total Unit :";
            // 
            // txtTotUnit
            // 
            this.txtTotUnit.Enabled = false;
            this.txtTotUnit.Location = new System.Drawing.Point(388, 55);
            this.txtTotUnit.Name = "txtTotUnit";
            this.txtTotUnit.Size = new System.Drawing.Size(100, 20);
            this.txtTotUnit.TabIndex = 14;
            // 
            // lblNumOfFloors
            // 
            this.lblNumOfFloors.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNumOfFloors.AutoSize = true;
            this.lblNumOfFloors.Location = new System.Drawing.Point(304, 82);
            this.lblNumOfFloors.Name = "lblNumOfFloors";
            this.lblNumOfFloors.Size = new System.Drawing.Size(78, 13);
            this.lblNumOfFloors.TabIndex = 4;
            this.lblNumOfFloors.Text = "Num of Floors :";
            this.lblNumOfFloors.Visible = false;
            // 
            // numOfFloors
            // 
            this.numOfFloors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloors.Enabled = false;
            this.numOfFloors.Location = new System.Drawing.Point(388, 81);
            this.numOfFloors.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.ReadOnly = true;
            this.numOfFloors.Size = new System.Drawing.Size(159, 20);
            this.numOfFloors.TabIndex = 5;
            this.numOfFloors.Value = 1;
            this.numOfFloors.Visible = false;
            // 
            // numIncrement
            // 
            this.numIncrement.BackColor = System.Drawing.SystemColors.Control;
            this.numIncrement.Location = new System.Drawing.Point(388, 3);
            this.numIncrement.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numIncrement.Name = "numIncrement";
            this.numIncrement.Size = new System.Drawing.Size(120, 20);
            this.numIncrement.TabIndex = 16;
            this.numIncrement.Value = 0;
            this.numIncrement.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(293, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 26);
            this.label1.TabIndex = 17;
            this.label1.Text = "Increment House/Lot Num :";
            this.label1.Visible = false;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.Control;
            this.txtId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtId.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtId.Location = new System.Drawing.Point(12, 31);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(550, 20);
            this.txtId.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Enabled = false;
            this.panel3.Location = new System.Drawing.Point(12, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(550, 8);
            this.panel3.TabIndex = 5;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblId.Location = new System.Drawing.Point(12, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(57, 13);
            this.lblId.TabIndex = 4;
            this.lblId.Text = "Original Id:";
            // 
            // PropertyMultiANDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 619);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblId);
            this.Name = "PropertyMultiANDForm";
            this.Text = "PropertyForm";
            this.Controls.SetChildIndex(this.lblId, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.txtId, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Label lblType;
        protected Controls.UpperComboBox cbType;
        protected System.Windows.Forms.Label lblNumOfFloors;
        protected System.Windows.Forms.Label lblConstructionStatus;
        protected Controls.UpperComboBox cbConstStatus;
        protected System.Windows.Forms.TextBox txtId;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Label lblId;
        protected System.Windows.Forms.Label lblYearInstall;
        protected System.Windows.Forms.Label lblSource;
        protected System.Windows.Forms.ComboBox cbYearInstall;
        protected Controls.UpperComboBox cbSource;
        protected Controls.IntUpDown numOfFloors;
        private System.Windows.Forms.TextBox txtTotUnit;
        private System.Windows.Forms.Label lblTotUnit;
        private Controls.IntUpDown numIncrement;
        private System.Windows.Forms.Label label1;
    }
}
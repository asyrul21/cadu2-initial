﻿namespace Geomatic.UI.Forms
{
    partial class FloorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.updateGroup = new Geomatic.UI.Controls.CollapsibleGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateUpdated = new System.Windows.Forms.TextBox();
            this.txtDateCreated = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.lblUpdater = new System.Windows.Forms.Label();
            this.lblDateUpdated = new System.Windows.Forms.Label();
            this.txtCreator = new System.Windows.Forms.TextBox();
            this.lblDateCreated = new System.Windows.Forms.Label();
            this.lblCreator = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblLot = new System.Windows.Forms.Label();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.numOfFloors = new Geomatic.UI.Controls.IntUpDown();
            this.numStartFloor = new Geomatic.UI.Controls.IntUpDown();
            this.lblNoOfUnit = new System.Windows.Forms.Label();
            this.numOfUnit = new Geomatic.UI.Controls.IntUpDown();
            this.lblNoOfFloors = new System.Windows.Forms.Label();
            this.lblStartFloor = new System.Windows.Forms.Label();
            this.txtFloorPrefix = new Geomatic.UI.Controls.TextBoxExt();
            this.txtFloorPostfix = new Geomatic.UI.Controls.TextBoxExt();
            this.txtUnitPostfix = new Geomatic.UI.Controls.TextBoxExt();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUnitPrefix = new Geomatic.UI.Controls.TextBoxExt();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFloorNumFormat = new System.Windows.Forms.Label();
            this.cbFloorNumFormat = new System.Windows.Forms.ComboBox();
            this.cbUnitNoFormat = new System.Windows.Forms.ComboBox();
            this.lblUnitNoFormat = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPreviewText = new System.Windows.Forms.Label();
            this.lblPreview = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.updateGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfUnit)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(12, 323);
            this.bottomPanel.Size = new System.Drawing.Size(471, 37);
            // 
            // groupBox
            // 
            this.groupBox.Location = new System.Drawing.Point(0, 0);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(200, 100);
            this.groupBox.TabIndex = 0;
            this.groupBox.TabStop = false;
            // 
            // updateGroup
            // 
            this.updateGroup.Controls.Add(this.tableLayoutPanel3);
            this.updateGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateGroup.IsCollapsed = false;
            this.updateGroup.Location = new System.Drawing.Point(12, 253);
            this.updateGroup.Name = "updateGroup";
            this.updateGroup.Size = new System.Drawing.Size(471, 70);
            this.updateGroup.TabIndex = 8;
            this.updateGroup.TabStop = false;
            this.updateGroup.Text = "Updates";
            this.updateGroup.OnCollapsedChanged += new Geomatic.UI.Controls.CollapsibleGroupBox.CollapseChangedEventHandler(this.group_OnCollapsedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtDateUpdated, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtDateCreated, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtUpdater, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblUpdater, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblDateUpdated, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtCreator, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblDateCreated, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblCreator, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(465, 51);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtDateUpdated
            // 
            this.txtDateUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateUpdated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateUpdated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateUpdated.Location = new System.Drawing.Point(316, 28);
            this.txtDateUpdated.Name = "txtDateUpdated";
            this.txtDateUpdated.ReadOnly = true;
            this.txtDateUpdated.Size = new System.Drawing.Size(146, 20);
            this.txtDateUpdated.TabIndex = 7;
            // 
            // txtDateCreated
            // 
            this.txtDateCreated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateCreated.BackColor = System.Drawing.SystemColors.Control;
            this.txtDateCreated.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDateCreated.Location = new System.Drawing.Point(316, 3);
            this.txtDateCreated.Name = "txtDateCreated";
            this.txtDateCreated.ReadOnly = true;
            this.txtDateCreated.Size = new System.Drawing.Size(146, 20);
            this.txtDateCreated.TabIndex = 3;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdater.BackColor = System.Drawing.SystemColors.Control;
            this.txtUpdater.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUpdater.Location = new System.Drawing.Point(78, 28);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.ReadOnly = true;
            this.txtUpdater.Size = new System.Drawing.Size(146, 20);
            this.txtUpdater.TabIndex = 5;
            // 
            // lblUpdater
            // 
            this.lblUpdater.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUpdater.AutoSize = true;
            this.lblUpdater.Location = new System.Drawing.Point(7, 31);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(65, 13);
            this.lblUpdater.TabIndex = 4;
            this.lblUpdater.Text = "Updated by:";
            // 
            // lblDateUpdated
            // 
            this.lblDateUpdated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateUpdated.AutoSize = true;
            this.lblDateUpdated.Location = new System.Drawing.Point(233, 31);
            this.lblDateUpdated.Name = "lblDateUpdated";
            this.lblDateUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblDateUpdated.TabIndex = 6;
            this.lblDateUpdated.Text = "Date Updated:";
            // 
            // txtCreator
            // 
            this.txtCreator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreator.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreator.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCreator.Location = new System.Drawing.Point(78, 3);
            this.txtCreator.Name = "txtCreator";
            this.txtCreator.ReadOnly = true;
            this.txtCreator.Size = new System.Drawing.Size(146, 20);
            this.txtCreator.TabIndex = 1;
            // 
            // lblDateCreated
            // 
            this.lblDateCreated.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDateCreated.AutoSize = true;
            this.lblDateCreated.Location = new System.Drawing.Point(237, 6);
            this.lblDateCreated.Name = "lblDateCreated";
            this.lblDateCreated.Size = new System.Drawing.Size(73, 13);
            this.lblDateCreated.TabIndex = 2;
            this.lblDateCreated.Text = "Date Created:";
            // 
            // lblCreator
            // 
            this.lblCreator.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCreator.AutoSize = true;
            this.lblCreator.Location = new System.Drawing.Point(11, 6);
            this.lblCreator.Name = "lblCreator";
            this.lblCreator.Size = new System.Drawing.Size(61, 13);
            this.lblCreator.TabIndex = 0;
            this.lblCreator.Text = "Created by:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.77778F));
            this.tableLayoutPanel.Controls.Add(this.lblLot, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.txtLot, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.numOfFloors, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.numStartFloor, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.lblNoOfUnit, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.numOfUnit, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblNoOfFloors, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lblStartFloor, 0, 2);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 10);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(471, 134);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // lblLot
            // 
            this.lblLot.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLot.AutoSize = true;
            this.lblLot.Location = new System.Drawing.Point(9, 2);
            this.lblLot.Name = "lblLot";
            this.lblLot.Size = new System.Drawing.Size(92, 26);
            this.lblLot.TabIndex = 4;
            this.lblLot.Text = "House Num / Lot Num:";
            // 
            // txtLot
            // 
            this.txtLot.BackColor = System.Drawing.SystemColors.Control;
            this.txtLot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLot.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtLot.Location = new System.Drawing.Point(107, 3);
            this.txtLot.Name = "txtLot";
            this.txtLot.ReadOnly = true;
            this.txtLot.Size = new System.Drawing.Size(361, 20);
            this.txtLot.TabIndex = 1;
            // 
            // numOfFloors
            // 
            this.numOfFloors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfFloors.Location = new System.Drawing.Point(107, 35);
            this.numOfFloors.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOfFloors.Name = "numOfFloors";
            this.numOfFloors.Size = new System.Drawing.Size(361, 20);
            this.numOfFloors.TabIndex = 5;
            this.numOfFloors.Value = 1;
            // 
            // numStartFloor
            // 
            this.numStartFloor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numStartFloor.Location = new System.Drawing.Point(107, 65);
            this.numStartFloor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartFloor.Name = "numStartFloor";
            this.numStartFloor.Size = new System.Drawing.Size(361, 20);
            this.numStartFloor.TabIndex = 7;
            this.numStartFloor.Value = 1;
            // 
            // lblNoOfUnit
            // 
            this.lblNoOfUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNoOfUnit.AutoSize = true;
            this.lblNoOfUnit.Location = new System.Drawing.Point(18, 105);
            this.lblNoOfUnit.Name = "lblNoOfUnit";
            this.lblNoOfUnit.Size = new System.Drawing.Size(83, 13);
            this.lblNoOfUnit.TabIndex = 8;
            this.lblNoOfUnit.Text = "Number of Units";
            // 
            // numOfUnit
            // 
            this.numOfUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numOfUnit.Location = new System.Drawing.Point(107, 102);
            this.numOfUnit.Name = "numOfUnit";
            this.numOfUnit.Size = new System.Drawing.Size(361, 20);
            this.numOfUnit.TabIndex = 9;
            this.numOfUnit.Value = 1;
            this.numOfUnit.ValueChanged += new System.EventHandler(this.numOfUnit_ValueChanged);
            // 
            // lblNoOfFloors
            // 
            this.lblNoOfFloors.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblNoOfFloors.AutoSize = true;
            this.lblNoOfFloors.Location = new System.Drawing.Point(14, 38);
            this.lblNoOfFloors.Name = "lblNoOfFloors";
            this.lblNoOfFloors.Size = new System.Drawing.Size(87, 13);
            this.lblNoOfFloors.TabIndex = 4;
            this.lblNoOfFloors.Text = "Number of Floors";
            // 
            // lblStartFloor
            // 
            this.lblStartFloor.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblStartFloor.AutoSize = true;
            this.lblStartFloor.Location = new System.Drawing.Point(26, 68);
            this.lblStartFloor.Name = "lblStartFloor";
            this.lblStartFloor.Size = new System.Drawing.Size(75, 13);
            this.lblStartFloor.TabIndex = 6;
            this.lblStartFloor.Text = "Start Floor No.";
            // 
            // txtFloorPrefix
            // 
            this.txtFloorPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloorPrefix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloorPrefix.Location = new System.Drawing.Point(252, 23);
            this.txtFloorPrefix.Name = "txtFloorPrefix";
            this.txtFloorPrefix.Size = new System.Drawing.Size(103, 20);
            this.txtFloorPrefix.TabIndex = 10;
            this.txtFloorPrefix.TextChanged += new System.EventHandler(this.txtFloorPrefix_TextChanged);
            // 
            // txtFloorPostfix
            // 
            this.txtFloorPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFloorPostfix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFloorPostfix.Location = new System.Drawing.Point(361, 23);
            this.txtFloorPostfix.Name = "txtFloorPostfix";
            this.txtFloorPostfix.Size = new System.Drawing.Size(107, 20);
            this.txtFloorPostfix.TabIndex = 13;
            this.txtFloorPostfix.TextChanged += new System.EventHandler(this.txtFloorPostfix_TextChanged);
            // 
            // txtUnitPostfix
            // 
            this.txtUnitPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUnitPostfix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnitPostfix.Location = new System.Drawing.Point(361, 55);
            this.txtUnitPostfix.Name = "txtUnitPostfix";
            this.txtUnitPostfix.Size = new System.Drawing.Size(107, 20);
            this.txtUnitPostfix.TabIndex = 15;
            this.txtUnitPostfix.TextChanged += new System.EventHandler(this.txtUnitPostfix_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(361, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Postfix";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtUnitPrefix
            // 
            this.txtUnitPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUnitPrefix.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnitPrefix.Location = new System.Drawing.Point(252, 55);
            this.txtUnitPrefix.Name = "txtUnitPrefix";
            this.txtUnitPrefix.Size = new System.Drawing.Size(103, 20);
            this.txtUnitPrefix.TabIndex = 14;
            this.txtUnitPrefix.TextChanged += new System.EventHandler(this.txtUnitPrefix_TextChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.38462F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.61538F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel2.Controls.Add(this.lblFloorNumFormat, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbFloorNumFormat, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cbUnitNoFormat, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblUnitNoFormat, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtFloorPrefix, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtFloorPostfix, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtUnitPrefix, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtUnitPostfix, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPreviewText, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblPreview, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 144);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(471, 109);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lblFloorNumFormat
            // 
            this.lblFloorNumFormat.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblFloorNumFormat.AutoSize = true;
            this.lblFloorNumFormat.Location = new System.Drawing.Point(50, 26);
            this.lblFloorNumFormat.Name = "lblFloorNumFormat";
            this.lblFloorNumFormat.Size = new System.Drawing.Size(47, 13);
            this.lblFloorNumFormat.TabIndex = 19;
            this.lblFloorNumFormat.Text = "Floor No";
            // 
            // cbFloorNumFormat
            // 
            this.cbFloorNumFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFloorNumFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFloorNumFormat.FormattingEnabled = true;
            this.cbFloorNumFormat.Items.AddRange(new object[] {
            "1",
            "01",
            "001",
            "0001"});
            this.cbFloorNumFormat.Location = new System.Drawing.Point(103, 22);
            this.cbFloorNumFormat.Name = "cbFloorNumFormat";
            this.cbFloorNumFormat.Size = new System.Drawing.Size(143, 21);
            this.cbFloorNumFormat.TabIndex = 18;
            this.cbFloorNumFormat.SelectedIndexChanged += new System.EventHandler(this.cbFloorNumFormat_SelectedIndexChanged);
            // 
            // cbUnitNoFormat
            // 
            this.cbUnitNoFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUnitNoFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUnitNoFormat.FormattingEnabled = true;
            this.cbUnitNoFormat.Items.AddRange(new object[] {
            "1",
            "01",
            "001",
            "0001"});
            this.cbUnitNoFormat.Location = new System.Drawing.Point(103, 54);
            this.cbUnitNoFormat.Name = "cbUnitNoFormat";
            this.cbUnitNoFormat.Size = new System.Drawing.Size(143, 21);
            this.cbUnitNoFormat.TabIndex = 20;
            this.cbUnitNoFormat.SelectedIndexChanged += new System.EventHandler(this.cbUnitNoFormat_SelectedIndexChanged);
            // 
            // lblUnitNoFormat
            // 
            this.lblUnitNoFormat.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblUnitNoFormat.AutoSize = true;
            this.lblUnitNoFormat.Location = new System.Drawing.Point(54, 58);
            this.lblUnitNoFormat.Name = "lblUnitNoFormat";
            this.lblUnitNoFormat.Size = new System.Drawing.Size(43, 13);
            this.lblUnitNoFormat.TabIndex = 21;
            this.lblUnitNoFormat.Text = "Unit No";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(252, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Prefix";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPreviewText
            // 
            this.lblPreviewText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreviewText.AutoSize = true;
            this.lblPreviewText.Location = new System.Drawing.Point(103, 93);
            this.lblPreviewText.Name = "lblPreviewText";
            this.lblPreviewText.Size = new System.Drawing.Size(143, 13);
            this.lblPreviewText.TabIndex = 22;
            this.lblPreviewText.Text = "[ Preview ]";
            this.lblPreviewText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPreview
            // 
            this.lblPreview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblPreview.AutoSize = true;
            this.lblPreview.Location = new System.Drawing.Point(49, 93);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(48, 13);
            this.lblPreview.TabIndex = 23;
            this.lblPreview.Text = "Preview:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoEllipsis = true;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Format";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNumber
            // 
            this.lblNumber.Location = new System.Drawing.Point(0, 0);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(100, 23);
            this.lblNumber.TabIndex = 0;
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(0, 0);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(100, 20);
            this.txtNumber.TabIndex = 0;
            // 
            // FloorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 391);
            this.Controls.Add(this.updateGroup);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "FloorForm";
            this.Text = "FloorForm";
            this.Controls.SetChildIndex(this.tableLayoutPanel, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanel2, 0);
            this.Controls.SetChildIndex(this.updateGroup, 0);
            this.Controls.SetChildIndex(this.bottomPanel, 0);
            this.updateGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfFloors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfUnit)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox;
        protected Controls.CollapsibleGroupBox updateGroup;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.TextBox txtDateUpdated;
        protected System.Windows.Forms.Label lblCreator;
        protected System.Windows.Forms.TextBox txtDateCreated;
        protected System.Windows.Forms.TextBox txtUpdater;
        protected System.Windows.Forms.Label lblUpdater;
        protected System.Windows.Forms.Label lblDateUpdated;
        protected System.Windows.Forms.TextBox txtCreator;
        protected System.Windows.Forms.Label lblDateCreated;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected System.Windows.Forms.Label lblNumber;
        protected System.Windows.Forms.TextBox txtNumber;
        protected System.Windows.Forms.Label lblNoOfFloors;
        private Geomatic.UI.Controls.IntUpDown numOfFloors;
        private System.Windows.Forms.Label lblStartFloor;
        private Geomatic.UI.Controls.IntUpDown numStartFloor;
        private System.Windows.Forms.Label lblNoOfUnit;
        private Geomatic.UI.Controls.IntUpDown numOfUnit;
        private System.Windows.Forms.Label lblFloorNumFormat;
        protected System.Windows.Forms.ComboBox cbFloorNumFormat;
        private System.Windows.Forms.Label lblUnitNoFormat;
        protected System.Windows.Forms.ComboBox cbUnitNoFormat;
        private System.Windows.Forms.Label lblPreview;
        private System.Windows.Forms.Label lblPreviewText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Controls.TextBoxExt txtFloorPrefix;
        private Controls.TextBoxExt txtFloorPostfix;
        private Controls.TextBoxExt txtUnitPrefix;
        private Controls.TextBoxExt txtUnitPostfix;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLot;
        private System.Windows.Forms.TextBox txtLot;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Validators;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities;
using Geomatic.UI.Controls;
using Geomatic.UI.Commands;
using ESRI.ArcGIS.Carto;
using Geomatic.UI.Forms.Documents;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms
{
    public partial class BuildingGroupForm : CollapsibleForm
    {
        public int? SelectedNavigationStatus
        {
            get { return (cbNaviStatus.SelectedItem == null) ? (int?)null : ((ComboBoxItem<string, int>)cbNaviStatus.SelectedItem).Value; }
        }

        public string InsertedCode
        {
            get { return txtCode.Text; }
        }

        public string InsertedName
        {
            get { return txtName.Text; }
        }

        public string SelectedSource
        {
            get { return (cbSource.SelectedItem == null) ? string.Empty : ((GSource)cbSource.SelectedItem).Code.ToString(); }
        }

        public string SelectedForecastSource
        {
            get { return (cbForecastSource.SelectedItem == null) ? string.Empty : ((GSource)cbForecastSource.SelectedItem).Code.ToString(); }
        }

        public int TotalForecastUnit
        {
            get { return totalForecastUnit.Value; }
        }

        public string TotalUnit
        {
            get { return txtTotalUnit.Text ?? "0"; }
        }

        protected IEnumerable<IGFeature> _selectedBuildings;
        protected GBuildingGroup _buildingGroup;

        public BuildingGroupForm()
            : this(null)
        {
        }

        public BuildingGroupForm(GBuildingGroup buildingGroup)
        {
            InitializeComponent();
            _commandPool.Register(Command.NavigationItem,
                lblNaviStatus, cbNaviStatus);
            _commandPool.Register(Command.EditableItem,
              btnSetCode,
              lblCode, txtCode,
              lblName, txtName,
              lblSource, cbSource,
              lblForecastSource, cbForecastSource,
              lblForecastUnit, totalForecastUnit,
              lblTotalNumUnit, txtTotalUnit,
              btnApply
              );

            _buildingGroup = buildingGroup;
        }

        protected override void Form_Load()
        {
            ComboBoxUtils.PopulateNavigationStatus(cbNaviStatus);
            ComboBoxUtils.PopulateSource(cbSource);
            ComboBoxUtils.PopulateForecastSource(cbForecastSource);

            controlBuilding.Columns.Add("Id");
            controlBuilding.Columns.Add("Construction Status");
            controlBuilding.Columns.Add("Code");
            controlBuilding.Columns.Add("Name");
            controlBuilding.Columns.Add("Name2");
            controlBuilding.Columns.Add("Floor Num Unit ");
            controlBuilding.Columns.Add("Lot");
            controlBuilding.Columns.Add("House");
            controlBuilding.Columns.Add("Street Type");
            controlBuilding.Columns.Add("Street Name");
            controlBuilding.Columns.Add("Street Name2");
            controlBuilding.Columns.Add("Section");
            controlBuilding.Columns.Add("Postcode");
            controlBuilding.Columns.Add("City");
            controlBuilding.Columns.Add("State");
            controlBuilding.Columns.Add("Created By");
            controlBuilding.Columns.Add("Date Created");
            controlBuilding.Columns.Add("Updated By");
            controlBuilding.Columns.Add("Date Updated");

            txtId.Text = _buildingGroup.OID.ToString();

            cbNaviStatus.Text = _buildingGroup.NavigationStatusValue;
            txtName.Text = _buildingGroup.Name;
            txtCode.Text = _buildingGroup.Code;
            cbSource.Text = _buildingGroup.SourceValue;
            GCode1 code1 = _buildingGroup.GetCode1();
            categoryControl.Code1Text = code1 == null ? string.Empty : code1.Description;
            GCode2 code2 = _buildingGroup.GetCode2();
            categoryControl.Code2Text = code2 == null ? string.Empty : code2.Description;
            GCode3 code3 = _buildingGroup.GetCode3();
            categoryControl.Code3Text = code3 == null ? string.Empty : code3.Description;
            txtDateCreated.Text = _buildingGroup.DateCreated;
            txtDateUpdated.Text = _buildingGroup.DateUpdated;
            txtCreator.Text = _buildingGroup.CreatedBy;
            txtUpdater.Text = _buildingGroup.UpdatedBy;
            cbForecastSource.Text = _buildingGroup.ForecastSourceValue;
            
            int UnitCount = _buildingGroup.NumUnit.HasValue ? _buildingGroup.NumUnit.Value : 0;
            txtTotalUnit.Text = UnitCount.ToString();

            int forecastUnitCount = _buildingGroup.ForecastNumUnit.HasValue ? _buildingGroup.ForecastNumUnit.Value : 0;
            totalForecastUnit.Value = forecastUnitCount;

            if (_buildingGroup.BuldingNamePos == 1)
            {
                cbMirror.Checked = true;
            }
            else
            {
                cbMirror.Checked = false;
            }

            GStreet street = _buildingGroup.GetStreet();

            if (street != null)
            {
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null)
                {
                    txtStreetId.Text = streetAND.OriId.ToString();
                    txtStreetType.Text = streetAND.TypeValue;
                    txtStreetName.Text = streetAND.Name;
                    txtStreetName2.Text = streetAND.Name2;
                    txtSection.Text = streetAND.Section;
                    txtPostcode.Text = streetAND.Postcode;
                    txtCity.Text = streetAND.City;
                    txtState.Text = streetAND.State;
                }
                else
                {
                    txtStreetId.Text = street.OID.ToString();
                    txtStreetType.Text = street.TypeValue;
                    txtStreetName.Text = street.Name;
                    txtStreetName2.Text = street.Name2;
                    txtSection.Text = street.Section;
                    txtPostcode.Text = street.Postcode;
                    txtCity.Text = street.City;
                    txtState.Text = street.State;
                }
            }
            PopulateBuildings();
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Code
            bool isCodeValid = BuildingGroupValidator.CheckCode(txtCode.Text);
            LabelColor(lblCode, isCodeValid);
            pass = isCodeValid;

            // Name
            bool isNameValid = BuildingGroupValidator.CheckName(txtName.Text);
            LabelColor(lblName, isNameValid);
            pass &= isNameValid;

            // Source
            bool isSourceValid = BuildingGroupValidator.CheckSource(SelectedSource);
            LabelColor(lblSource, isSourceValid);
            pass &= isSourceValid;

            // forcast Source
            bool isForecastSourceValid = BuildingGroupValidator.CheckSource(SelectedForecastSource);
            LabelColor(lblForecastSource, isForecastSourceValid);
            pass &= isForecastSourceValid;

            return pass;
        }

        public void SetValues()
        {
            _buildingGroup.NavigationStatus = SelectedNavigationStatus;
            _buildingGroup.Code = InsertedCode;
            _buildingGroup.Name = InsertedName;
            _buildingGroup.Source = SelectedSource;
            _buildingGroup.ForecastSource = SelectedForecastSource;
            _buildingGroup.ForecastNumUnit = TotalForecastUnit;
            _buildingGroup.NumUnit = Int32.Parse(TotalUnit);
        }

        protected void PopulateBuildings()
        {
            int totolFloorUnit = 0;

            controlBuilding.BeginUpdate();
            controlBuilding.Sorting = SortOrder.None;
            controlBuilding.Items.Clear();

            List<ListViewItem> items = new List<ListViewItem>();

            foreach (GBuilding building in _buildingGroup.GetBuildings())
            {
                ListViewItem item = CreateItem(building);
                items.Add(item);
                if (!string.IsNullOrEmpty(building.Unit.ToString()))
                {
                    totolFloorUnit += building.Unit.Value;
                }
            }

            controlBuilding.Items.AddRange(items.ToArray());
            controlBuilding.FixColumnWidth();
            controlBuilding.EndUpdate();

            txtTotalUnit.Text = totolFloorUnit.ToString();
        }

        private void btnSetCode_Click(object sender, EventArgs e)
        {
            using (ChoosePoiCodeForm form = new ChoosePoiCodeForm())
            {
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }
                GCode3 code3 = form.SelectedCode;
                txtCode.Text = code3.GetCodes();

                GCode1 code1 = code3.GetCode1();
                if (code1 != null)
                {
                    categoryControl.Code1Text = code1.Description;
                }
                GCode2 code2 = code3.GetCode2();
                if (code2 != null)
                {
                    categoryControl.Code2Text = code2.Description;
                }
                if (code3 != null)
                {
                    categoryControl.Code3Text = code3.Description;
                }
            }
        }

        protected ListViewItem CreateItem(GBuilding building)
        {
            ListViewItem item = new ListViewItem();
            item.Text = building.OID.ToString();
            item.SubItems.Add(StringUtils.TrimSpaces(building.ConstructionStatusValue));
            item.SubItems.Add(building.Code);
            item.SubItems.Add(building.Name);
            item.SubItems.Add(building.Name2);
            item.SubItems.Add((building.Unit == null) ? "0" : building.Unit.ToString());

            GProperty property = building.GetProperty();
            item.SubItems.Add((property == null) ? string.Empty : property.Lot);
            item.SubItems.Add((property == null) ? string.Empty : property.House);

            GStreet street = building.GetStreet();
            item.SubItems.Add((street == null) ? string.Empty : StringUtils.TrimSpaces(street.TypeValue));
            item.SubItems.Add((street == null) ? string.Empty : street.Name);
            item.SubItems.Add((street == null) ? string.Empty : street.Name2);
            item.SubItems.Add((street == null) ? string.Empty : street.Section);
            item.SubItems.Add((street == null) ? string.Empty : street.Postcode);
            item.SubItems.Add((street == null) ? string.Empty : street.City);
            item.SubItems.Add((street == null) ? string.Empty : street.State);

            item.SubItems.Add(building.CreatedBy);
            item.SubItems.Add(building.DateCreated);
            item.SubItems.Add(building.UpdatedBy);
            item.SubItems.Add(building.DateUpdated);
            item.Tag = building;
            return item;
        }

        private void controlBuilding_AssociateClick1(object sender, EventArgs e)
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Please use function: Building> Associate Building Group");
                box.Show();
            }

            return;
        }

        private void controlBuilding_DissociateClick1(object sender, EventArgs e)
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Please use function: Building> Dissociate Building Group");
                box.Show();
            }

            return;
        }

        private void controlBuilding_AssociateClick(object sender, EventArgs e)
        { 
           // original
        
           using (ChooseBuildingByDistanceForm chooseForm = new ChooseBuildingByDistanceForm(_buildingGroup))
           {
        
               if (chooseForm.ShowDialog(this) != DialogResult.OK)
               {
                   return;
               }
               using (new WaitCursor())
               {
                   RepositoryFactory repo = new RepositoryFactory(_buildingGroup.SegmentName);
                   foreach (IGFeature feature in chooseForm.SelectedFeatures)
                   {
                       GBuilding building = (GBuilding)feature;
                       building.GroupId = _buildingGroup.OID;
                       repo.Update(building);
        
                       ListViewItem item = CreateItem(building);
                       controlBuilding.Items.Add(item);
                       controlBuilding.FixColumnWidth();
                    }
               }
           }
        }

        private void controlBuilding_DissociateClick(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (controlBuilding.SelectedItems.Count == 0)
                {
                    return;
                }
                RepositoryFactory repo = new RepositoryFactory(_buildingGroup.SegmentName);
                ListViewItem item = controlBuilding.SelectedItems[0];
                GBuilding building = (GBuilding)item.Tag;
                building.GroupId = 0;
                repo.Update(building);
                item.Remove();
                controlBuilding.FixColumnWidth();
            }
        }

        private void cbMirror_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMirror.Checked)
            {
                lblMirror.Text = "BLOCK A SUNWAY SUTERA CONDOMINIUM";
                _buildingGroup.BuldingNamePos = 1;
            }
            else
            {
                lblMirror.Text = "SUNWAY SUTERA CONDOMINIUM BLOCK A";
                _buildingGroup.BuldingNamePos = null;
            }
        }
    }
}

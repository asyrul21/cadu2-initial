using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class InfoMessageBox : MessageBoxBuilder
    {
        public InfoMessageBox()
        {
            SetCaption("Information");
            SetIcon(MessageBoxIcon.Information);
            SetButtons(MessageBoxButtons.OK);
            SetDefaultButton(MessageBoxDefaultButton.Button1);     
        }
    }
}

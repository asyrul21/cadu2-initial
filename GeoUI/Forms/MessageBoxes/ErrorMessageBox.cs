using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class ErrorMessageBox : MessageBoxBuilder
    {
        public ErrorMessageBox()
        {
            SetCaption("Error");
            SetIcon(MessageBoxIcon.Error);
            SetButtons(MessageBoxButtons.OK);
            SetDefaultButton(MessageBoxDefaultButton.Button1);            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class WarningMessageBox : MessageBoxBuilder
    {
        public WarningMessageBox()
        {
            SetCaption("Warning");
            SetIcon(MessageBoxIcon.Warning);
            SetButtons(MessageBoxButtons.OK);
            SetDefaultButton(MessageBoxDefaultButton.Button1);    
        }
    }
}

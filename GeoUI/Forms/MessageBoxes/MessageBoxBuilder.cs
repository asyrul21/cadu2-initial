using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class MessageBoxBuilder : IDisposable
    {
        protected string _text;
        protected string _caption;
        protected MessageBoxButtons _buttons;
        protected MessageBoxIcon _icon;
        protected MessageBoxDefaultButton _defaultButton;

        public MessageBoxBuilder()
        {
            _icon = MessageBoxIcon.None;
        }

        public MessageBoxBuilder SetText(string format, params object[] args)
        {
            return SetText(string.Format(format, args));
        }

        public MessageBoxBuilder SetText(string text)
        {
            _text = text;
            return this;
        }

        public MessageBoxBuilder SetCaption(string format, params object[] args)
        {
            return SetCaption(string.Format(format, args));
        }

        public MessageBoxBuilder SetCaption(string caption)
        {
            _caption = caption;
            return this;
        }

        public MessageBoxBuilder SetButtons(MessageBoxButtons buttons)
        {
            _buttons = buttons;
            return this;
        }

        public MessageBoxBuilder SetIcon(MessageBoxIcon icon)
        {
            _icon = icon;
            return this;
        }

        public MessageBoxBuilder SetDefaultButton(MessageBoxDefaultButton defaultButton)
        {
            _defaultButton = defaultButton;
            return this;
        }

        public DialogResult Show()
        {
            return MessageBox.Show(_text, _caption, _buttons, _icon, _defaultButton);
        }

        public DialogResult Show(IWin32Window owner)
        {
            return MessageBox.Show(owner, _text, _caption, _buttons, _icon, _defaultButton);
        }

        public virtual void Dispose()
        {
        }
    }
}

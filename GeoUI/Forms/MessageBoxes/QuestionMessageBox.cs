using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class QuestionMessageBox : MessageBoxBuilder
    {
        public QuestionMessageBox()
        {
            SetCaption("Question");
            SetIcon(MessageBoxIcon.Question);
            SetButtons(MessageBoxButtons.YesNoCancel);
            SetDefaultButton(MessageBoxDefaultButton.Button1);     
        }
    }
}

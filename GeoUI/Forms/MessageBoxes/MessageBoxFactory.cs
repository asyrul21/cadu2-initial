﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Geomatic.Core.Exceptions;

namespace Geomatic.UI.Forms.MessageBoxes
{
    public class MessageBoxFactory
    {
        public static MessageBoxBuilder Create(Exception ex)
        {
            MessageBoxBuilder messageBox;
            if (ex.GetType() == typeof(WarningException))
            {
                messageBox = new WarningMessageBox();
            }
            else if (ex.GetType() == typeof(NetworkException))
            {
                messageBox = new WarningMessageBox();
            }
            else if (ex.GetType() == typeof(NavigationControlException))
            {
                messageBox = new WarningMessageBox();
            }
            else if (ex.GetType() == typeof(ProcessCancelledException))
            {
                messageBox = new WarningMessageBox();
            }
            else if (ex.GetType() == typeof(QualityControlException))
            {
                messageBox = new WarningMessageBox();
            }
            else
            {
                messageBox = new ErrorMessageBox();
            }
            messageBox.SetText(ex.Message);
            return messageBox;
        }
    }
}

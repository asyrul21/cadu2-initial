﻿namespace Geomatic.UI.Forms
{
    partial class UserGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabAdditional = new System.Windows.Forms.TabControl();
            this.pageFunctions = new System.Windows.Forms.TabPage();
            this.pageUsers = new System.Windows.Forms.TabPage();
            this.tvFunction = new Geomatic.UI.Controls.CheckChildTreeView();
            this.controlUsers = new Geomatic.UI.Controls.ReferenceControl();
            this.tableLayoutPanel.SuspendLayout();
            this.tabAdditional.SuspendLayout();
            this.pageFunctions.SuspendLayout();
            this.pageUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(69, 8);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Controls.Add(this.txtName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblName, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(360, 30);
            this.tableLayoutPanel.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(113, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(244, 20);
            this.txtName.TabIndex = 1;
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Image = global::Geomatic.UI.Properties.Resources.apply;
            this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApply.Location = new System.Drawing.Point(216, 477);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "Apply";
            this.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = global::Geomatic.UI.Properties.Resources.cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(297, 477);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabAdditional
            // 
            this.tabAdditional.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabAdditional.Controls.Add(this.pageFunctions);
            this.tabAdditional.Controls.Add(this.pageUsers);
            this.tabAdditional.Location = new System.Drawing.Point(12, 48);
            this.tabAdditional.Name = "tabAdditional";
            this.tabAdditional.SelectedIndex = 0;
            this.tabAdditional.Size = new System.Drawing.Size(360, 423);
            this.tabAdditional.TabIndex = 3;
            // 
            // pageFunctions
            // 
            this.pageFunctions.Controls.Add(this.tvFunction);
            this.pageFunctions.Location = new System.Drawing.Point(4, 22);
            this.pageFunctions.Name = "pageFunctions";
            this.pageFunctions.Padding = new System.Windows.Forms.Padding(3);
            this.pageFunctions.Size = new System.Drawing.Size(352, 397);
            this.pageFunctions.TabIndex = 1;
            this.pageFunctions.Text = "Function";
            this.pageFunctions.UseVisualStyleBackColor = true;
            // 
            // pageUsers
            // 
            this.pageUsers.Controls.Add(this.controlUsers);
            this.pageUsers.Location = new System.Drawing.Point(4, 22);
            this.pageUsers.Name = "pageUsers";
            this.pageUsers.Padding = new System.Windows.Forms.Padding(3);
            this.pageUsers.Size = new System.Drawing.Size(352, 397);
            this.pageUsers.TabIndex = 0;
            this.pageUsers.Text = "User";
            this.pageUsers.UseVisualStyleBackColor = true;
            // 
            // tvFunction
            // 
            this.tvFunction.CheckBoxes = true;
            this.tvFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFunction.Location = new System.Drawing.Point(3, 3);
            this.tvFunction.Name = "tvFunction";
            this.tvFunction.Size = new System.Drawing.Size(346, 391);
            this.tvFunction.TabIndex = 0;
            // 
            // controlUsers
            // 
            this.controlUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlUsers.Location = new System.Drawing.Point(3, 3);
            this.controlUsers.Name = "controlUsers";
            this.controlUsers.Size = new System.Drawing.Size(346, 391);
            this.controlUsers.TabIndex = 0;
            this.controlUsers.AssociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlUsers_AssociateClick);
            this.controlUsers.DissociateClick += new Geomatic.UI.Controls.ReferenceControl.ClickEventHandler(this.controlUsers_DissociateClick);
            // 
            // UserGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 512);
            this.Controls.Add(this.tabAdditional);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "UserGroupForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UserGroupForm";
            this.Load += new System.EventHandler(this.UserGroupForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.tabAdditional.ResumeLayout(false);
            this.pageFunctions.ResumeLayout(false);
            this.pageUsers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblName;
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.Button btnApply;
        protected System.Windows.Forms.Button btnCancel;
        protected System.Windows.Forms.TabControl tabAdditional;
        protected System.Windows.Forms.TabPage pageUsers;
        protected System.Windows.Forms.TabPage pageFunctions;
        protected Geomatic.UI.Controls.ReferenceControl controlUsers;
        protected System.Windows.Forms.TextBox txtName;
        protected Geomatic.UI.Controls.CheckChildTreeView tvFunction;

    }
}
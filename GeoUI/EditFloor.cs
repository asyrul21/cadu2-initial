﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Geomatic.Core.Validators;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Forms
{
    public partial class EditFloor : CollapsibleForm
    {
        public string InsertedFloor
        {
            get { return txtNumber.Text; }
        }

        public string InsertedNumUnit
        {
            get { return txtUnitNumber.Text; }
        }

        protected GFloor _floor;

        public EditFloor()
            : this(null)
        {
        }

        public EditFloor(GFloor floor)
        {
            InitializeComponent();
            _floor = floor;
        }

        protected override void Form_Load()
        {
            txtId.Text = _floor.OID.ToString();

            txtNumber.Text = _floor.Number;
            txtUnitNumber.Text = _floor.NumUnit;

            txtCreator.Text = _floor.CreatedBy;
            txtDateCreated.Text = _floor.DateCreated;
            txtDateUpdated.Text = _floor.DateUpdated;
            txtUpdater.Text = _floor.UpdatedBy;
        }

        private void group_OnCollapsedChanged(object sender, CollapseChangedEventArgs e)
        {
            OnCollapsedChanged();
        }

        protected override bool ValidateValues()
        {
            bool pass = true;

            // Number
            bool isNumberValid = FloorValidator.CheckNumber(txtNumber.Text);
            LabelColor(lblNumber, isNumberValid);
            pass &= isNumberValid;

            // Unit Number
            bool isUnitNumberValid = FloorValidator.CheckUnitNumber(txtUnitNumber.Text);
            LabelColor(lblUnitNumber, isUnitNumberValid);
            pass &= isUnitNumberValid;

            return pass;
        }

        public void SetValues()
        {
            _floor.Number = InsertedFloor;
            _floor.NumUnit = InsertedNumUnit;
        }

    }
}

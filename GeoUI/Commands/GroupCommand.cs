﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Commands
{
    public class GroupCommand : ICommand
    {
        protected List<ICommand> _commands;

        public IEnumerable<ICommand> Commands
        {
            get
            {
                foreach (ICommand command in _commands)
                {
                    yield return command;
                }
            }
        }

        public GroupCommand()
            :this(null)
        {
        }

        public GroupCommand(params object[] objects)
        {
            _commands = new List<ICommand>();
            if (objects != null)
            {
                foreach (object obj in objects)
                {
                    Add(CommandFactory.Create(obj));
                }
            }
        }

        public bool Contains(ICommand command)
        {
            return _commands.Contains(command);
        }

        public void Add(ICommand command)
        {
            _commands.Add(command);
        }

        public bool Checked
        {
            get
            {
                bool isChecked = true;
                foreach (ICommand command in _commands)
                {
                    isChecked &= command.Checked;
                }
                return isChecked;                
            }
            set
            {
                foreach (ICommand command in _commands)
                {
                    command.Checked = value;
                }                
            }
        }

        public bool Enabled
        {
            set
            {
                foreach (ICommand command in _commands)
                {
                    command.Enabled = value;
                }
            }
            get
            {
                bool isEnabled = true;
                foreach (ICommand command in _commands)
                {
                    isEnabled &= command.Enabled;
                }
                return isEnabled;
            }
        }

        public bool Visible
        {
            set
            {
                foreach (ICommand command in _commands)
                {
                    command.Visible = true;
                }
            }
            get
            {
                bool isVisible = true;
                foreach (ICommand command in _commands)
                {
                    isVisible &= command.Visible;
                }
                return isVisible;
            }
        }
    }
}

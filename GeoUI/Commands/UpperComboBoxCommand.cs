﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Commands
{
    public class UpperComboBoxCommand : ICommand
    {
        private Geomatic.UI.Controls.UpperComboBox _item;

        public UpperComboBoxCommand(object obj)
        {
            _item = (UpperComboBox)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for UpperComboBox.");
            }
            set
            {
                throw new Exception("Invalid check operation for UpperComboBox.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

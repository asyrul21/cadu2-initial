﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Geomatic.UI.Forms;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Commands
{
    public class CommandFactory
    {
        public static ICommand Create(object obj)
        {
            if (obj.GetType() == typeof(RibbonPanel))
            {
                return new RibbonPanelCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonButton))
            {
                return new RibbonButtonCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonOrbOptionButton))
            {
                return new RibbonOrbOptionButtonCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonTextBox))
            {
                return new RibbonTextBoxCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonOrbMenuItem))
            {
                return new RibbonOrbMenuItemCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonDescriptionMenuItem))
            {
                return new RibbonDescriptionMenuItemCommand(obj);
            }
            if (obj.GetType() == typeof(RibbonItemGroup))
            {
                return new RibbonItemGroupCommand(obj);
            }
            if (obj.GetType() == typeof(Button))
            {
                return new ButtonCommand(obj);
            }
            if (obj.GetType() == typeof(Label))
            {
                return new LabelCommand(obj);
            }
            if (obj.GetType() == typeof(TextBox))
            {
                return new TextBoxCommand(obj);
            }
            if (obj.GetType() == typeof(CheckBox))
            {
                return new CheckBoxCommand(obj);
            }
            if (obj.GetType() == typeof(ComboBox))
            {
                return new ComboBoxCommand(obj);
            }
            if (obj.GetType() == typeof(UpperComboBox))
            {
                return new UpperComboBoxCommand(obj);
            }
            if (obj.GetType() == typeof(IntUpDown))
            {
                return new IntUpDownCommand(obj);
            }
            if (obj.GetType() == typeof(DoubleUpDown))
            {
                return new DoubleUpDownCommand(obj);
            }
            if (obj.GetType() == typeof(ToolStripMenuItem))
            {
                return new ToolStripMenuItemCommand(obj);
            }
            throw new Exception("Unknown command object.");
        }
    }
}

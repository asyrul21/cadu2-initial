﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Commands
{
    public class IntUpDownCommand : ICommand
    {
        private Geomatic.UI.Controls.IntUpDown _item;

        public IntUpDownCommand(object obj)
        {
            _item = (IntUpDown)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for IntUpDown.");
            }
            set
            {
                throw new Exception("Invalid check operation for IntUpDown.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

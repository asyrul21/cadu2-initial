﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Commands
{
    public class Command
    {
        public const string NavigationItem = "NavigationItem";
        public const string EditableItem = "EditableItem";
    }
}

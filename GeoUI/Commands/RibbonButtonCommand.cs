﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class RibbonButtonCommand : ICommand
    {
        private RibbonButton _item;

        public RibbonButtonCommand(object obj)
        {
            _item = (RibbonButton)obj;
        }

        public bool Checked
        {
            get
            {
                return _item.Checked;
            }
            set
            {
                _item.Checked = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

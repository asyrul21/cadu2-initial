﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class RibbonItemGroupCommand : ICommand
    {
        private RibbonItemGroup _item;

        public RibbonItemGroupCommand(object obj)
        {
            _item = (RibbonItemGroup)obj;
        }

        public bool Checked
        {
            get
            {
                return _item.Checked;
            }
            set
            {
                _item.Checked = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

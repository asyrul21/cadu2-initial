﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class RibbonPanelCommand : ICommand
    {
        private RibbonPanel _item;

        public RibbonPanelCommand(object obj)
        {
            _item = (RibbonPanel)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for RibbonPanel.");
            }
            set
            {
                throw new Exception("Invalid check operation for RibbonPanel.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

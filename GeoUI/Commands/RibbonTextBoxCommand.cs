﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class RibbonTextBoxCommand : ICommand
    {
        private RibbonTextBox _item;

        public RibbonTextBoxCommand(object obj)
        {
            _item = (RibbonTextBox)obj;
        }

        public bool Checked
        {
            get
            {
                return _item.Checked;
            }
            set
            {
                _item.Checked = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

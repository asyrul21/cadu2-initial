﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class ButtonCommand : ICommand
    {
        private Button _item;

        public ButtonCommand(object obj)
        {
            _item = (Button)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for Button.");
            }
            set
            {
                throw new Exception("Invalid check operation for Button.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

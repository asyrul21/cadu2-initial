﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class CommandPool
    {
        protected Dictionary<string, ICommand> _commands;
        protected Dictionary<string, RadioCommand> _radioCommands;

        public CommandPool()
        {
            _commands = new Dictionary<string, ICommand>();
            _radioCommands = new Dictionary<string, RadioCommand>();
        }

        public void Register(string commandName, params object[] objects)
        {
            GroupCommand groupCommand = new GroupCommand(objects);
            _commands.Add(commandName, groupCommand);
        }

        public void RegisterGroup(string groupCommandName, params string[] commandNames)
        {
            GroupCommand groupCommand = new GroupCommand();
            foreach (string commandName in commandNames)
            {
                ICommand command = GetCommand(commandName);
                groupCommand.Add(command);
            }
            _commands.Add(groupCommandName, groupCommand);
        }

        public void RegisterRadio(string radioCommandName, params string[] commandNames)
        {
            RadioCommand radioCommand = new RadioCommand();
            foreach (string commandName in commandNames)
            {
                ICommand command = GetCommand(commandName);
                radioCommand.Add(command);
            }
            _radioCommands.Add(radioCommandName, radioCommand);
            RegisterGroup(radioCommandName, commandNames);
        }

        public bool Registered(string commandName)
        {
            return _commands.ContainsKey(commandName);
        }

        protected ICommand GetCommand(string commandName)
        {
            if (!_commands.ContainsKey(commandName))
            {
                return null;
            }
            return _commands[commandName];
        }

        public void Enable(string commandName)
        {
            ICommand command = GetCommand(commandName);
            command.Enabled = true;
        }

        public void Disable(string commandName)
        {
            ICommand command = GetCommand(commandName);
            command.Enabled = false;
        }

        public void Check(string commandName)
        {
            ICommand command = GetCommand(commandName);
            foreach (RadioCommand radioCommand in _radioCommands.Values)
            {
                if (radioCommand.Contains(command))
                {
                    radioCommand.Check(command);
                    return;
                }
            }
            command.Checked = true;
        }

        public void UnCheck(string commandName)
        {
            ICommand command = GetCommand(commandName);
            command.Checked = false;
        }
    }
}

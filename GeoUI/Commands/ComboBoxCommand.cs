﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class ComboBoxCommand : ICommand
    {
        private ComboBox _item;

        public ComboBoxCommand(object obj)
        {
            _item = (ComboBox)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for ComboBox.");
            }
            set
            {
                throw new Exception("Invalid check operation for ComboBox.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

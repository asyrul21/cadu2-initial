﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.Controls;

namespace Geomatic.UI.Commands
{
    public class DoubleUpDownCommand : ICommand
    {
        private DoubleUpDown _item;

        public DoubleUpDownCommand(object obj)
        {
            _item = (DoubleUpDown)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for DoubleUpDown.");
            }
            set
            {
                throw new Exception("Invalid check operation for DoubleUpDown.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

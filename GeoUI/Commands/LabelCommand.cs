﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class LabelCommand : ICommand
    {
        private Label _item;

        public LabelCommand(object obj)
        {
            _item = (Label)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for Label.");
            }
            set
            {
                throw new Exception("Invalid check operation for Label.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

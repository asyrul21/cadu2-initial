﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Commands
{
    public class TextBoxCommand : ICommand
    {
        private TextBox _item;

        public TextBoxCommand(object obj)
        {
            _item = (TextBox)obj;
        }

        public bool Checked
        {
            get
            {
                throw new Exception("Invalid check operation for TextBox.");
            }
            set
            {
                throw new Exception("Invalid check operation for TextBox.");
            }
        }

        public bool Enabled
        {
            get
            {
                return _item.Enabled;
            }
            set
            {
                _item.Enabled = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _item.Visible;
            }
            set
            {
                _item.Visible = value;
            }
        }
    }
}

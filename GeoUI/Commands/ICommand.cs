﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geomatic.UI.Commands
{
    public interface ICommand
    {
        bool Checked { set; get; }
        bool Enabled { set; get; }
        bool Visible { set; get; }
    }
}

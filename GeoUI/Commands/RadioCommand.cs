﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Commands
{
    public class RadioCommand : GroupCommand
    {
        public void Check(ICommand targetCommand)
        {
            foreach (ICommand command in _commands)
            {
                command.Checked = object.Equals(command, targetCommand);
            }
        }

        public void UnCheckAll()
        {
            foreach (ICommand command in _commands)
            {
                command.Checked = false;
            }
        }
    }
}

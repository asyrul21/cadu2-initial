﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Geomatic.UI.WomsSR {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://tm.com.my/", ConfigurationName="WomsSR.WomsWSSoap")]
    public interface WomsWSSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tm.com.my/CreateWorkOrder", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string CreateWorkOrder(string username, string prtname, string wo_urgency, string wo_type, string lnd_type, string rqr_date, string wo_smry, string wo_desc, string wo_crt_date, string wo_send_date, string wo_status, string lndindex1, string lndindex2, string lndindex3, string lndindex4);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tm.com.my/CreateANDWorkOrder2", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string CreateANDWorkOrder2(
                    string username, 
                    string prtname, 
                    string wo_urgency, 
                    string wo_type, 
                    string lnd_type, 
                    string rqr_date, 
                    string wo_smry, 
                    string wo_desc, 
                    string wo_crt_date, 
                    string wo_send_date, 
                    string lndindex1, 
                    string lndindex2, 
                    string lndindex3, 
                    string lndindex4, 
                    string sprvname, 
                    string sprv_acpt_rjt_date, 
                    string sprv_notes, 
                    string diginame, 
                    string di_date_asgn, 
                    string di_exp_comp_date, 
                    string di_comp_date, 
                    string digi_notes, 
                    string qaqcname, 
                    string q_date_asgn, 
                    string q_exp_comp_date);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tm.com.my/CreateANDWorkOrder", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string CreateANDWorkOrder(
                    string username, 
                    string prtname, 
                    string wo_urgency, 
                    string wo_type, 
                    string lnd_type, 
                    string rqr_date, 
                    string wo_smry, 
                    string wo_desc, 
                    string wo_crt_date, 
                    string wo_send_date, 
                    string lndindex1, 
                    string lndindex2, 
                    string lndindex3, 
                    string lndindex4, 
                    string diginame, 
                    string q_date_asgn, 
                    string q_exp_comp_date);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WomsWSSoapChannel : Geomatic.UI.WomsSR.WomsWSSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WomsWSSoapClient : System.ServiceModel.ClientBase<Geomatic.UI.WomsSR.WomsWSSoap>, Geomatic.UI.WomsSR.WomsWSSoap {
        
        public WomsWSSoapClient() {
        }
        
        public WomsWSSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WomsWSSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WomsWSSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WomsWSSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string CreateWorkOrder(string username, string prtname, string wo_urgency, string wo_type, string lnd_type, string rqr_date, string wo_smry, string wo_desc, string wo_crt_date, string wo_send_date, string wo_status, string lndindex1, string lndindex2, string lndindex3, string lndindex4) {
            return base.Channel.CreateWorkOrder(username, prtname, wo_urgency, wo_type, lnd_type, rqr_date, wo_smry, wo_desc, wo_crt_date, wo_send_date, wo_status, lndindex1, lndindex2, lndindex3, lndindex4);
        }
        
        public string CreateANDWorkOrder2(
                    string username, 
                    string prtname, 
                    string wo_urgency, 
                    string wo_type, 
                    string lnd_type, 
                    string rqr_date, 
                    string wo_smry, 
                    string wo_desc, 
                    string wo_crt_date, 
                    string wo_send_date, 
                    string lndindex1, 
                    string lndindex2, 
                    string lndindex3, 
                    string lndindex4, 
                    string sprvname, 
                    string sprv_acpt_rjt_date, 
                    string sprv_notes, 
                    string diginame, 
                    string di_date_asgn, 
                    string di_exp_comp_date, 
                    string di_comp_date, 
                    string digi_notes, 
                    string qaqcname, 
                    string q_date_asgn, 
                    string q_exp_comp_date) {
            return base.Channel.CreateANDWorkOrder2(username, prtname, wo_urgency, wo_type, lnd_type, rqr_date, wo_smry, wo_desc, wo_crt_date, wo_send_date, lndindex1, lndindex2, lndindex3, lndindex4, sprvname, sprv_acpt_rjt_date, sprv_notes, diginame, di_date_asgn, di_exp_comp_date, di_comp_date, digi_notes, qaqcname, q_date_asgn, q_exp_comp_date);
        }
        
        public string CreateANDWorkOrder(
                    string username, 
                    string prtname, 
                    string wo_urgency, 
                    string wo_type, 
                    string lnd_type, 
                    string rqr_date, 
                    string wo_smry, 
                    string wo_desc, 
                    string wo_crt_date, 
                    string wo_send_date, 
                    string lndindex1, 
                    string lndindex2, 
                    string lndindex3, 
                    string lndindex4, 
                    string diginame, 
                    string q_date_asgn, 
                    string q_exp_comp_date) {
            return base.Channel.CreateANDWorkOrder(username, prtname, wo_urgency, wo_type, lnd_type, rqr_date, wo_smry, wo_desc, wo_crt_date, wo_send_date, lndindex1, lndindex2, lndindex3, lndindex4, diginame, q_date_asgn, q_exp_comp_date);
        }
    }
}

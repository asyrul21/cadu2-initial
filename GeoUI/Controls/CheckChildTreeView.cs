﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public class CheckChildTreeView : TreeView
    {
        public CheckChildTreeView()
            : base()
        {
            this.CheckBoxes = true;
        }

        protected override void WndProc(ref Message m)
        {
            // Filter WM_LBUTTONDBLCLK
            if (m.Msg != 0x203)
            {
                base.WndProc(ref m);
            }
        }

        protected override void OnAfterCheck(TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                this.BeginUpdate();
                if (e.Node.Nodes.Count > 0)
                {
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
                TreeNode parentNode = e.Node.Parent as TreeNode;
                if (parentNode != null)
                {
                    bool isChecked = true;
                    foreach (TreeNode node in parentNode.Nodes)
                    {
                        isChecked &= node.Checked;
                    }
                    parentNode.Checked = isChecked;
                }
                this.EndUpdate();
            }
            base.OnAfterCheck(e);
        }

        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }
    }
}

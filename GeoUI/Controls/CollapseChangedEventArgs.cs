﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI.Controls
{
    public class CollapseChangedEventArgs : EventArgs
    {
        public bool IsCollapsed { private set; get; }
        public int DeltaHeight { private set; get; }

        public CollapseChangedEventArgs(bool isCollapsed, int deltaHeight)
        {
            IsCollapsed = isCollapsed;
            DeltaHeight = deltaHeight;
        }
    }
}

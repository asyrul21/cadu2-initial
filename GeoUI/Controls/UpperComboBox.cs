﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public partial class UpperComboBox : ComboBox
    {
        private const int CBS_UPPERCASE = 0x2000;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.Style |= CBS_UPPERCASE;
                return createParams;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.VisualStyles;

namespace Geomatic.UI.Controls
{
    public partial class CollapsibleGroupBox : GroupBox
    {
        public delegate void CollapseChangedEventHandler(object sender, CollapseChangedEventArgs e);
        public event CollapseChangedEventHandler OnCollapsedChanged;

        private bool _isCollapsed = false;
        private Rectangle _buttonRect = new Rectangle(6, 2, 11, 11);
        private int _actualHeight = 0;
        private int _collapseHeight = 20;
        private List<Control> _visibleCtrls = new List<Control>();
        private SolidBrush _drawBrush = null;
        private StringFormat format = new StringFormat();

        private SolidBrush DrawBrush
        {
            get
            {
                if (_drawBrush == null)
                    _drawBrush = new SolidBrush(this.ForeColor);
                _drawBrush.Color = this.ForeColor;
                return _drawBrush;
            }
        }

        private int CollapseHeight
        {
            get { return _collapseHeight; }
        }

        private int ActualHeight
        {
            get { return _actualHeight; }
            set { _actualHeight = value; }
        }

        public int DeltaHeight
        {
            get { return (IsCollapsed) ? -(ActualHeight - CollapseHeight) : (ActualHeight - CollapseHeight); }
        }

        private Rectangle ButtonRect
        {
            get { return _buttonRect; }
        }

        public bool IsCollapsed
        {
            get { return _isCollapsed; }
            set
            {
                _isCollapsed = value;
                if (!value)
                {
                    Height = ActualHeight;
                }
                else
                    Height = CollapseHeight;

                foreach (Control c in _visibleCtrls)
                {
                    c.Visible = !value;
                }
                Invalidate();
            }
        }

        public CollapsibleGroupBox()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (ActualHeight == 0)
                ActualHeight = Height;
            DrawGroupBox(e.Graphics);
            DrawButton(e.Graphics);
        }

        void DrawGroupBox(Graphics g)
        {
            Rectangle bounds = new Rectangle(ClientRectangle.X, ClientRectangle.Y + 6, ClientRectangle.Width, ClientRectangle.Height - 6);
            GroupBoxRenderer.DrawGroupBox(g, bounds, Enabled ? GroupBoxState.Normal : GroupBoxState.Disabled);

            int textX = ButtonRect.Left + ButtonRect.Width + 4;
            int textSize = (int)g.MeasureString(Text, this.Font).Width;
            textSize = textSize < 1 ? 1 : textSize;

            g.DrawLine(SystemPens.Control, textX - 1, bounds.Top, textX + textSize + 1, bounds.Top);
            g.DrawString(Text, this.Font, DrawBrush, textX, bounds.Top - 6, format);
        }

        private void DrawButton(Graphics g)
        {
            if (IsCollapsed)
                g.DrawImage(Properties.Resources.expand, ButtonRect);
            else
                g.DrawImage(Properties.Resources.collapse, ButtonRect);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (ButtonRect.Contains(e.Location))
            {
                CollapsedChanged();
            }
            base.OnMouseUp(e);
        }

        private void CollapsedChanged()
        {
            IsCollapsed = !IsCollapsed;
            if (OnCollapsedChanged != null)
                OnCollapsedChanged(this, new CollapseChangedEventArgs(IsCollapsed, DeltaHeight));
        }

        protected override void OnResize(EventArgs e)
        {
            if (ActualHeight == 0)
                ActualHeight = Height;
            base.OnResize(e);
        }

        protected override void OnLayout(LayoutEventArgs e)
        {
            if (_visibleCtrls.Count == 0)
            {
                foreach (Control c in Controls)
                {
                    if (c.Visible)
                        _visibleCtrls.Add(c);
                }
            }
            base.OnLayout(e);
        }
    }
}

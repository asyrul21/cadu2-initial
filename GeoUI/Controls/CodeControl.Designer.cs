﻿namespace Geomatic.UI.Controls
{
    partial class CodeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.txtCode3 = new System.Windows.Forms.TextBox();
            this.txtCode2 = new System.Windows.Forms.TextBox();
            this.txtCode1 = new System.Windows.Forms.TextBox();
            this.lblCode3 = new System.Windows.Forms.Label();
            this.lblCode2 = new System.Windows.Forms.Label();
            this.lblCode1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.Controls.Add(this.txtCode3, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.txtCode2, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.txtCode1, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.lblCode3, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.lblCode2, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblCode1, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(365, 52);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // txtCode3
            // 
            this.txtCode3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode3.Location = new System.Drawing.Point(245, 29);
            this.txtCode3.Name = "txtCode3";
            this.txtCode3.ReadOnly = true;
            this.txtCode3.Size = new System.Drawing.Size(117, 20);
            this.txtCode3.TabIndex = 5;
            // 
            // txtCode2
            // 
            this.txtCode2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode2.Location = new System.Drawing.Point(124, 29);
            this.txtCode2.Name = "txtCode2";
            this.txtCode2.ReadOnly = true;
            this.txtCode2.Size = new System.Drawing.Size(115, 20);
            this.txtCode2.TabIndex = 4;
            // 
            // txtCode1
            // 
            this.txtCode1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCode1.Location = new System.Drawing.Point(3, 29);
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = true;
            this.txtCode1.Size = new System.Drawing.Size(115, 20);
            this.txtCode1.TabIndex = 3;
            // 
            // lblCode3
            // 
            this.lblCode3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCode3.AutoSize = true;
            this.lblCode3.Location = new System.Drawing.Point(281, 6);
            this.lblCode3.Name = "lblCode3";
            this.lblCode3.Size = new System.Drawing.Size(44, 13);
            this.lblCode3.TabIndex = 2;
            this.lblCode3.Text = "Code 3:";
            this.lblCode3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCode2
            // 
            this.lblCode2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCode2.AutoSize = true;
            this.lblCode2.Location = new System.Drawing.Point(159, 6);
            this.lblCode2.Name = "lblCode2";
            this.lblCode2.Size = new System.Drawing.Size(44, 13);
            this.lblCode2.TabIndex = 1;
            this.lblCode2.Text = "Code 2:";
            this.lblCode2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCode1
            // 
            this.lblCode1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCode1.AutoSize = true;
            this.lblCode1.Location = new System.Drawing.Point(38, 6);
            this.lblCode1.Name = "lblCode1";
            this.lblCode1.Size = new System.Drawing.Size(44, 13);
            this.lblCode1.TabIndex = 0;
            this.lblCode1.Text = "Code 1:";
            this.lblCode1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CodeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "CodeControl";
            this.Size = new System.Drawing.Size(365, 52);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        protected System.Windows.Forms.TextBox txtCode3;
        protected System.Windows.Forms.TextBox txtCode2;
        protected System.Windows.Forms.TextBox txtCode1;
        protected System.Windows.Forms.Label lblCode3;
        protected System.Windows.Forms.Label lblCode2;
        protected System.Windows.Forms.Label lblCode1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public class IntUpDown : NumericUpDown
    {
        public new int Value
        {
            set 
            {
                base.Value = Convert.ToDecimal(value);
            } 
            get
            {
                return Convert.ToInt32(base.Value);
            }
        }
    }
}

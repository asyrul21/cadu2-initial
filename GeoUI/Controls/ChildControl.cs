﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Collections;

namespace Geomatic.UI.Controls
{
    public partial class ChildControl : UserControl
    {
        public delegate void ClickEventHandler(object sender, EventArgs e);
        public event ClickEventHandler AddClick;
        public event ClickEventHandler EditClick;
        public event ClickEventHandler DeleteClick;

        [Browsable(false)]
        public ListView ListView { get { return this.lvResult; } }

        [Browsable(false)]
        public System.Windows.Forms.ListView.SelectedListViewItemCollection SelectedItems
        {
            get
            {
                return this.lvResult.SelectedItems;
            }
        }

        [Browsable(true), Category("ListView")]
        public System.Windows.Forms.ListView.ColumnHeaderCollection Columns
        {
            get
            {
                return this.ListView.Columns;
            }
        }

        [Browsable(true), Category("ListView")]
        public System.Windows.Forms.ListView.ListViewItemCollection Items
        {
            get
            {
                return this.ListView.Items;
            }
        }

        [Browsable(true), Category("ListView"), DefaultValue(View.Details)]
        public View View
        {
            set
            {
                this.ListView.View = value;
            }
            get
            {
                return this.ListView.View;
            }
        }

        [Browsable(true), Category("ListView"), DefaultValue(false)]
        public bool MultiSelect
        {
            set
            {
                this.ListView.MultiSelect = value;
            }
            get
            {
                return this.ListView.MultiSelect;
            }
        }

        [Browsable(true), Category("ListView"), DefaultValue(true)]
        public bool FullRowSelect
        {
            set
            {
                this.ListView.FullRowSelect = value;
            }
            get
            {
                return this.ListView.FullRowSelect;
            }
        }

        [Browsable(true), Category("ListView"), DefaultValue(false)]
        public bool HideSelection
        {
            set
            {
                this.ListView.HideSelection = value;
            }
            get
            {
                return this.ListView.HideSelection;
            }
        }

        [Browsable(true), Category("ListView"), DefaultValue(SortOrder.None)]
        public SortOrder Sorting
        {
            set
            {
                this.ListView.Sorting = value;
            }
            get
            {
                return this.ListView.Sorting;
            }
        }

        public ChildControl()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (AddClick != null)
                AddClick(this, e);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (EditClick != null)
                EditClick(this, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (DeleteClick != null)
                DeleteClick(this, e);
        }

        public void BeginUpdate()
        {
            this.lvResult.BeginUpdate();
        }

        public void EndUpdate()
        {
            this.lvResult.EndUpdate();
        }

        public void FixColumnWidth()
        {
            foreach (ColumnHeader column in this.Columns)
            {
                column.Width = -2;
            }
        }
    }
}

﻿using System;

namespace Geomatic.UI.Controls
{
    partial class ReferenceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvResult = new Geomatic.UI.Controls.SortableListView();
            this.btnDissociate = new System.Windows.Forms.Button();
            this.btnAssociate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvResult
            // 
            this.lvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResult.FullRowSelect = true;
            this.lvResult.HideSelection = false;
            this.lvResult.Location = new System.Drawing.Point(3, 3);
            this.lvResult.MultiSelect = false;
            this.lvResult.Name = "lvResult";
            this.lvResult.Size = new System.Drawing.Size(344, 215);
            this.lvResult.TabIndex = 0;
            this.lvResult.UseCompatibleStateImageBehavior = false;
            this.lvResult.View = System.Windows.Forms.View.Details;
            //this.lvResult.SelectedIndexChanged += new System.EventHandler(this.lvResult_SelectedIndexChanged);
            // 
            // btnDissociate
            // 
            this.btnDissociate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDissociate.Image = global::Geomatic.UI.Properties.Resources.delete;
            this.btnDissociate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDissociate.Location = new System.Drawing.Point(262, 224);
            this.btnDissociate.Name = "btnDissociate";
            this.btnDissociate.Size = new System.Drawing.Size(85, 23);
            this.btnDissociate.TabIndex = 3;
            this.btnDissociate.Text = "Dissociate";
            this.btnDissociate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDissociate.UseVisualStyleBackColor = true;
            this.btnDissociate.Click += new System.EventHandler(this.btnDissociate_Click);
            // 
            // btnAssociate
            // 
            this.btnAssociate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAssociate.Image = global::Geomatic.UI.Properties.Resources.add;
            this.btnAssociate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAssociate.Location = new System.Drawing.Point(171, 224);
            this.btnAssociate.Name = "btnAssociate";
            this.btnAssociate.Size = new System.Drawing.Size(85, 23);
            this.btnAssociate.TabIndex = 1;
            this.btnAssociate.Text = "Associate";
            this.btnAssociate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAssociate.UseVisualStyleBackColor = true;
            this.btnAssociate.Click += new System.EventHandler(this.btnAssociate_Click);
            // 
            // ReferenceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvResult);
            this.Controls.Add(this.btnDissociate);
            this.Controls.Add(this.btnAssociate);
            this.Name = "ReferenceControl";
            this.Size = new System.Drawing.Size(350, 250);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAssociate;
        private System.Windows.Forms.Button btnDissociate;
        private Geomatic.UI.Controls.SortableListView lvResult;


    }
}

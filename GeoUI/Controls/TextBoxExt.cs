﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public class TextBoxExt : TextBox
    {
        public delegate void PasteEventHandler(object sender, EventArgs e);
        public event PasteEventHandler AfterPaste;

        private const int WM_CUT = 0x0300;
        private const int WM_COPY = 0x0301;
        private const int WM_PASTE = 0x0302;

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_PASTE)
            {
                OnPaste();
            }
        }

        private void OnPaste()
        {
            if (AfterPaste != null)
                AfterPaste(this, EventArgs.Empty);
        }
    }
}

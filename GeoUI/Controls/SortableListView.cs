﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public class SortableListView : ListView
    {
        private ColumnSorter _sorter;

        public SortableListView()
        {
            _sorter = new ColumnSorter();
            this.ListViewItemSorter = _sorter;
        }

        public void FixColumnWidth()
        {
            foreach (ColumnHeader column in this.Columns)
            {
                column.Width = -2;
            }
        }

        public new void BeginUpdate()
        {
            this.ListViewItemSorter = null;
            base.BeginUpdate();
        }

        public new void EndUpdate()
        {
            this.ListViewItemSorter = _sorter;
            base.EndUpdate();
        }

        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == _sorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (_sorter.Order == SortOrder.Ascending)
                {
                    _sorter.Order = SortOrder.Descending;
                }
                else
                {
                    _sorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _sorter.SortColumn = e.Column;
                _sorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            base.BeginUpdate();
            this.Sort();
            base.EndUpdate();
            base.OnColumnClick(e);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions;
using Geomatic.Core;

namespace Geomatic.UI.Controls
{
    public class SegmentButton : RibbonButton
    {
        public SegmentName? SegmentName { set; get; }


        public SegmentButton()
            : base()
        {
        }

        public SegmentButton(Image smallImage)
            : base(smallImage)
        {
        }

        public SegmentButton(string text)
            : base(text)
        {
        }
    }
}

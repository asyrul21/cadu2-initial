﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public class DoubleUpDown : NumericUpDown
    {
        public DoubleUpDown()
        {
            DecimalPlaces = 2;
        }

        public new double Value
        {
            set
            {
                base.Value = Convert.ToDecimal(value);
            }
            get
            {
                return Convert.ToDouble(base.Value);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI.Controls
{
    public partial class CodeControl : UserControl
    {
        public CodeControl()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        public string Code1Text
        {
            set
            {
                txtCode1.Text = value;
            }
            get
            {
                return txtCode1.Text;
            }
        }

        [Browsable(true)]
        public string Code2Text
        {
            set
            {
                txtCode2.Text = value;
            }
            get
            {
                return txtCode2.Text;
            }
        }

        [Browsable(true)]
        public string Code3Text
        {
            set
            {
                txtCode3.Text = value;
            }
            get
            {
                return txtCode3.Text;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.UI
{
    public class ComboBoxItem<T, K> 
    {
        private T _key;
        private K _value;

        public T Key {
            get
            {
                return _key;
            }
        }

        public K Value
        {
            get
            {
                return _value;
            }
        }

        public ComboBoxItem(T key, K value)
        {
            this._key = key;
            this._value = value;
        }

        public override string ToString()
        {
            return _key.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Geomatic.UI.Utilities
{
	public class AddressFormat
	{
		public static int DigitLen(bool chkbox, int totalNum)
		{
			int DigitLen = 1;
			if (!chkbox)
			{
				totalNum = totalNum - 1;
				DigitLen = totalNum.ToString().Length;
			}
			return DigitLen;
		}

		// default tick checkbox is genereate the Single digit for floor/unit -> 1,2..10..99..100
		// untick to generate floor/unit by total num digit -> if total num digit 3 like 100 -> 001,002..010..099..100
		public static string GetDigitFormat(int Digit, int startNum)
		{
			string NumString = startNum.ToString();
			switch (Digit)
			{
				default:
				case 1: break;
				case 2: NumString = startNum.ToString("00"); break;
				case 3: NumString = startNum.ToString("000"); break;
				case 4: NumString = startNum.ToString("0000"); break;
			}
			return NumString;
		}

		public static string unitAlpha(string Prefix)
		{
			Regex rgx = new Regex("[^A-Z]");
			string strPrefix = Prefix;
			strPrefix = rgx.Replace(strPrefix, "");
			if (!string.IsNullOrEmpty(strPrefix))
				return strPrefix;

			return strPrefix;
		}

		public static bool chkPrefixWithAlphabet(string Prefix)
		{
			Regex rgx = new Regex("[^A-Z]");
			string strPrefix = Prefix;
			strPrefix = rgx.Replace(strPrefix, "");
			if (!string.IsNullOrEmpty(strPrefix))
				return true;

			return false;
		}

        public static string GenerateAddressFormat(string strFormat, string prefix, string BlockName, int floor, int apt, int startApt, int i, bool ChkAptChar, 
                int FloorDigitNum, int AptDigitNum)
        {
            string AptAddressFormat = "";
            string strPrefix = "";

            if (strFormat == "BLOCK")
            {
                if (!string.IsNullOrEmpty(prefix))
                    strPrefix = string.Format("{0}", prefix);

                AptAddressFormat = string.Format("{0}{1}", strPrefix, BlockName);
            }
            else if (strFormat == "FLR")
            {
                if (!string.IsNullOrEmpty(prefix))
                    strPrefix = string.Format("{0}", prefix);

                AptAddressFormat = string.Format("{0}{1}", strPrefix, AddressFormat.GetDigitFormat(FloorDigitNum, floor));
            }
            else if (strFormat == "UNIT")
            {
                // set Apt unit number as alphabet 'A->Z"
                if (ChkAptChar)
                {
                    strPrefix = AddressFormat.unitAlpha(prefix);

                    if (strPrefix.Length == 1)
                    {
                        char letter = strPrefix[0];

                        if (apt == startApt)
                        {
                            AptAddressFormat = string.Format("{0}", prefix);
                        }
                        else
                        {
                            char nextChar = (char)(((int)letter) + i);
                            AptAddressFormat = prefix.Replace(letter.ToString(), nextChar.ToString());
                        }
                    }
                    else
                    {
                        AptAddressFormat = string.Format("{0}{1}", prefix, AddressFormat.GetDigitFormat(AptDigitNum, apt));
                    }
                }
                else
                {
                    AptAddressFormat = string.Format("{0}{1}", prefix, AddressFormat.GetDigitFormat(AptDigitNum, apt));
                }
            }
            return AptAddressFormat;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;

namespace Geomatic.UI.Utilities.Elements
{
    public abstract class ElementCreator
    {
        public abstract IElement Create();
    }
}

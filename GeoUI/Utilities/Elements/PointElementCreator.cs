﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.Utilities.Elements
{
    public class PointElementCreator : ElementCreator
    {
        protected IRgbColor _color;
        protected IRgbColor _outlineColor;
        protected esriSimpleMarkerStyle _style;

        public PointElementCreator(IRgbColor color, IRgbColor outlineColor, esriSimpleMarkerStyle style)
        {
            _color = color;
            _outlineColor = outlineColor;
            _style = style;
        }

        public override IElement Create()
        {
            // Marker symbols
            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = _color;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = _outlineColor;
            simpleMarkerSymbol.Size = 6;
            simpleMarkerSymbol.Style = _style;
            simpleMarkerSymbol.OutlineSize = 2;

            IMarkerElement markerElement = new MarkerElementClass();
            markerElement.Symbol = simpleMarkerSymbol;
            return (IElement)markerElement;
        }
    }
}

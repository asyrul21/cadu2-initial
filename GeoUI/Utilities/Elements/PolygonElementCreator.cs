﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;

namespace Geomatic.UI.Utilities.Elements
{
    public class PolygonElementCreator : ElementCreator
    {
        protected IRgbColor _color;
        protected esriSimpleFillStyle _style;

        public PolygonElementCreator(IRgbColor color, esriSimpleFillStyle style)
        {
            _color = color;
            _style = style;
        }

        public override IElement Create()
        {
            // Polygon elements
            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Width = 2;
            simpleLineSymbol.Color = _color;

            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = _color;
            simpleFillSymbol.Style = _style;
            simpleFillSymbol.Outline = simpleLineSymbol;

            IFillShapeElement fillShapeElement = new PolygonElementClass();
            fillShapeElement.Symbol = simpleFillSymbol;
            return (IElement)fillShapeElement;
        }
    }
}

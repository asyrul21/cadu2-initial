﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using System.Drawing;

namespace Geomatic.UI.Utilities.Elements
{
    public class ElementFactory
    {
        public static IElement CreateElement(esriGeometryType type)
        {
            IRgbColor color = ColorUtils.Get(Color.Black);
            IRgbColor outlineColor = ColorUtils.Get(Color.Black);
            return CreateElement(type, color, outlineColor);
        }

        public static IElement CreateElement(IGeometry geometry, IRgbColor color, IRgbColor outlineColor)
        {
            return CreateElement(geometry.GeometryType, color, outlineColor);
        }

        public static IElement CreateElement(esriGeometryType type, IRgbColor color, IRgbColor outlineColor)
        {
            ElementCreator creator;
            switch (type)
            {
                case esriGeometryType.esriGeometryPoint:
                    creator = new PointElementCreator(color, outlineColor, esriSimpleMarkerStyle.esriSMSCircle);
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    creator = new PolylineElementCreator(color, esriSimpleLineStyle.esriSLSSolid);
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    creator = new PolygonElementCreator(color, esriSimpleFillStyle.esriSFSHollow);
                    break;
                case esriGeometryType.esriGeometryCircularArc:
                    creator = new CircularElementCreator(color, esriSimpleFillStyle.esriSFSHollow);
                    break;
                default:
                    throw new Exception(string.Format("Unknown geometry type {0}", type));
            }
            return creator.Create();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;

namespace Geomatic.UI.Utilities.Elements
{
    public class PolylineElementCreator : ElementCreator
    {
        protected IRgbColor _color;
        protected esriSimpleLineStyle _style;

        public PolylineElementCreator(IRgbColor color, esriSimpleLineStyle style)
        {
            _color = color;
            _style = style;
        }

        public override IElement Create()
        {
            // Line elements
            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Color = _color;
            simpleLineSymbol.Style = _style;
            simpleLineSymbol.Width = 2;

            ILineElement lineElement = new LineElementClass();
            lineElement.Symbol = simpleLineSymbol;
            return (IElement)lineElement;
        }
    }
}

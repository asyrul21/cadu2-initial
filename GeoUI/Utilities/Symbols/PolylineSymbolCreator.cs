﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using System.Drawing;

namespace Geomatic.UI.Utilities.Symbols
{
    public class PolylineSymbolCreator : SymbolCreator
    {
        protected IRgbColor _color;
        protected esriSimpleLineStyle _style;
        protected double _width;

        public PolylineSymbolCreator()
            : this(ColorUtils.Random(), esriSimpleLineStyle.esriSLSSolid, 1)
        {
        }

        public PolylineSymbolCreator(IRgbColor color, esriSimpleLineStyle style, double width)
        {
            // noraini ali - June 2020 - update line color & width for gps line
            _color = ColorUtils.Get(Color.Black);
            _style = style;
            _width = 2.3; // currently street width (2.0), juhaida want extra thick than street
        }

        public override ISymbol Create()
        {
            // Line
            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Color = _color;
            simpleLineSymbol.Style = _style;
            simpleLineSymbol.Width = _width;

            return simpleLineSymbol as ISymbol;
        }
    }
}

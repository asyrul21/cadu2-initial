﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using System.Drawing;

namespace Geomatic.UI.Utilities.Symbols
{
    public class PointSymbolCreator : SymbolCreator
    {
        protected IRgbColor _color;
        protected IRgbColor _outlineColor;
        protected esriSimpleMarkerStyle _style;
        protected double _size;

        public PointSymbolCreator()
            : this(ColorUtils.Random(), ColorUtils.Get(Color.Black), esriSimpleMarkerStyle.esriSMSCircle, 4)
        {
        }

        public PointSymbolCreator(IRgbColor color, IRgbColor outlineColor, esriSimpleMarkerStyle style, double size)
        {
            _color = color;
            _outlineColor = outlineColor;
            _style = style;
            _size = size;
        }

        public override ISymbol Create()
        {
            // Marker
            ISimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbolClass();
            simpleMarkerSymbol.Color = _color;
            simpleMarkerSymbol.Outline = true;
            simpleMarkerSymbol.OutlineColor = _outlineColor;
            simpleMarkerSymbol.Size = _size;
            simpleMarkerSymbol.Style = _style;

            return simpleMarkerSymbol as ISymbol;
        }
    }
}

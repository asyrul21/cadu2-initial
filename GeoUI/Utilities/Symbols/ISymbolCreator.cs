﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;

namespace Geomatic.UI.Utilities.Symbols
{
    public interface ISymbolCreator
    {
        ISymbol Create();
    }
}

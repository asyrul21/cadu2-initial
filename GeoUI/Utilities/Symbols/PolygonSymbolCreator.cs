﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using System.Drawing;

namespace Geomatic.UI.Utilities.Symbols
{
    public class PolygonSymbolCreator : SymbolCreator
    {
        protected IRgbColor _color;
        protected IRgbColor _outlineColor;
        protected esriSimpleFillStyle _style;
        protected esriSimpleLineStyle _outlineStyle;
        protected double _outlineWidth;

        public PolygonSymbolCreator()
            : this(ColorUtils.Random(), ColorUtils.Get(Color.Black), esriSimpleFillStyle.esriSFSSolid, esriSimpleLineStyle.esriSLSSolid, 0.4)
        {
        }

        public PolygonSymbolCreator(IRgbColor color, IRgbColor outlineColor, esriSimpleFillStyle style, esriSimpleLineStyle outlineStyle, double outlineWidth)
        {
            _color = color;
            _outlineColor = outlineColor;
            _style = style;
            _outlineStyle = outlineStyle;
            _outlineWidth = outlineWidth;
        }

        public override ISymbol Create()
        {
            // Line
            ISimpleLineSymbol simpleLineSymbol = new SimpleLineSymbolClass();
            simpleLineSymbol.Color = _outlineColor;
            simpleLineSymbol.Style = _outlineStyle;
            simpleLineSymbol.Width = _outlineWidth;

            // Polygon
            ISimpleFillSymbol simpleFillSymbol = new SimpleFillSymbolClass();
            simpleFillSymbol.Color = _color;
            simpleFillSymbol.Style = _style;
            simpleFillSymbol.Outline = simpleLineSymbol;

            return simpleFillSymbol as ISymbol;
        }
    }
}

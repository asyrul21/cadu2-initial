﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;

namespace Geomatic.UI.Utilities.Symbols
{
    public abstract class SymbolCreator : ISymbolCreator
    {
        public abstract ISymbol Create();
    }
}

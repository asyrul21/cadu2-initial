﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.UI.Utilities.Symbols
{
    public class SymbolFactory
    {
        public static ISymbol CreateSymbol(esriGeometryType type)
        {
            ISymbolCreator creator;
            switch (type)
            {
                case esriGeometryType.esriGeometryPoint:
                    creator = new PointSymbolCreator();
                    break;
                case esriGeometryType.esriGeometryPolyline:
                    creator = new PolylineSymbolCreator();
                    break;
                case esriGeometryType.esriGeometryPolygon:
                    creator = new PolygonSymbolCreator();
                    break;
                case esriGeometryType.esriGeometryCircularArc:
                    creator = new CircularSymbolCreator();
                    break;
                default:
                    throw new Exception(string.Format("Unknown geometry type {0}", type));
            }
            return creator.Create();
        }
    }
}

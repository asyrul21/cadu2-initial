﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;

namespace Geomatic.UI.Utilities
{
    public static class GroupLayerExt
    {
        public static int GetLayerCount(this IGroupLayer groupLayer)
        {
            ICompositeLayer compositeLayer = groupLayer as ICompositeLayer;
            return (compositeLayer == null) ? 0 : compositeLayer.Count;
        }

        public static IEnumerable<ILayer> GetLayers(this IGroupLayer groupLayer)
        {
            ICompositeLayer compositeLayer = groupLayer as ICompositeLayer;
            if (compositeLayer == null)
            {
                yield break;
            }
            for (int count = 0; count < compositeLayer.Count; count++)
            {
                yield return compositeLayer.get_Layer(count);
            }
        }
    }
}

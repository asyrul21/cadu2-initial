﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using System.Drawing;

namespace Geomatic.UI.Utilities
{
    public static class ColorUtils
    {
        public static IRgbColor Select;
        private static List<IRgbColor> _colors;

        public static IRgbColor Get(Color color)
        {
            return Get(color.R, color.G, color.B);
        }

        public static IRgbColor Get(int red, int green, int blue)
        {
            return new RgbColorClass() { Red = red, Green = green, Blue = blue };
        }

        public static IRgbColor Random()
        {
            Random rnd = new Random();
            return _colors[rnd.Next(_colors.Count)];
        }

        static ColorUtils()
        {
            Select = Get(Color.Cyan);
            _colors = new List<IRgbColor>();
            _colors.Add(Get(Color.AliceBlue));
            _colors.Add(Get(Color.AntiqueWhite));
            _colors.Add(Get(Color.Aqua));
            _colors.Add(Get(Color.Aquamarine));
            _colors.Add(Get(Color.Azure));
            _colors.Add(Get(Color.Beige));
            _colors.Add(Get(Color.Bisque));
            _colors.Add(Get(Color.Black));
            _colors.Add(Get(Color.BlanchedAlmond));
            _colors.Add(Get(Color.Blue));
            _colors.Add(Get(Color.BlueViolet));
            _colors.Add(Get(Color.Brown));
            _colors.Add(Get(Color.BurlyWood));
            _colors.Add(Get(Color.CadetBlue));
            _colors.Add(Get(Color.Chartreuse));
            _colors.Add(Get(Color.Chocolate));
            _colors.Add(Get(Color.Coral));
            _colors.Add(Get(Color.CornflowerBlue));
            _colors.Add(Get(Color.Cornsilk));
            _colors.Add(Get(Color.Crimson));
            _colors.Add(Get(Color.Cyan));
            _colors.Add(Get(Color.DarkBlue));
            _colors.Add(Get(Color.DarkCyan));
            _colors.Add(Get(Color.DarkGoldenrod));
            _colors.Add(Get(Color.DarkGray));
            _colors.Add(Get(Color.DarkGreen));
            _colors.Add(Get(Color.DarkKhaki));
            _colors.Add(Get(Color.DarkMagenta));
            _colors.Add(Get(Color.DarkOliveGreen));
            _colors.Add(Get(Color.DarkOrange));
            _colors.Add(Get(Color.DarkOrchid));
            _colors.Add(Get(Color.DarkRed));
            _colors.Add(Get(Color.DarkSalmon));
            _colors.Add(Get(Color.DarkSeaGreen));
            _colors.Add(Get(Color.DarkSlateBlue));
            _colors.Add(Get(Color.DarkSlateGray));
            _colors.Add(Get(Color.DarkTurquoise));
            _colors.Add(Get(Color.DarkViolet));
            _colors.Add(Get(Color.DeepPink));
            _colors.Add(Get(Color.DeepSkyBlue));
            _colors.Add(Get(Color.DimGray));
            _colors.Add(Get(Color.DodgerBlue));
            _colors.Add(Get(Color.Firebrick));
            _colors.Add(Get(Color.FloralWhite));
            _colors.Add(Get(Color.ForestGreen));
            _colors.Add(Get(Color.Fuchsia));
            _colors.Add(Get(Color.Gainsboro));
            _colors.Add(Get(Color.GhostWhite));
            _colors.Add(Get(Color.Gold));
            _colors.Add(Get(Color.Goldenrod));
            _colors.Add(Get(Color.Gray));
            _colors.Add(Get(Color.Green));
            _colors.Add(Get(Color.GreenYellow));
            _colors.Add(Get(Color.Honeydew));
            _colors.Add(Get(Color.HotPink));
            _colors.Add(Get(Color.IndianRed));
            _colors.Add(Get(Color.Indigo));
            _colors.Add(Get(Color.Ivory));
            _colors.Add(Get(Color.Khaki));
            _colors.Add(Get(Color.Lavender));
            _colors.Add(Get(Color.LavenderBlush));
            _colors.Add(Get(Color.LawnGreen));
            _colors.Add(Get(Color.LemonChiffon));
            _colors.Add(Get(Color.LightBlue));
            _colors.Add(Get(Color.LightCoral));
            _colors.Add(Get(Color.LightCyan));
            _colors.Add(Get(Color.LightGoldenrodYellow));
            _colors.Add(Get(Color.LightGray));
            _colors.Add(Get(Color.LightGreen));
            _colors.Add(Get(Color.LightPink));
            _colors.Add(Get(Color.LightSalmon));
            _colors.Add(Get(Color.LightSeaGreen));
            _colors.Add(Get(Color.LightSkyBlue));
            _colors.Add(Get(Color.LightSlateGray));
            _colors.Add(Get(Color.LightSteelBlue));
            _colors.Add(Get(Color.LightYellow));
            _colors.Add(Get(Color.Lime));
            _colors.Add(Get(Color.LimeGreen));
            _colors.Add(Get(Color.Linen));
            _colors.Add(Get(Color.Magenta));
            _colors.Add(Get(Color.Maroon));
            _colors.Add(Get(Color.MediumAquamarine));
            _colors.Add(Get(Color.MediumBlue));
            _colors.Add(Get(Color.MediumOrchid));
            _colors.Add(Get(Color.MediumPurple));
            _colors.Add(Get(Color.MediumSeaGreen));
            _colors.Add(Get(Color.MediumSlateBlue));
            _colors.Add(Get(Color.MediumSpringGreen));
            _colors.Add(Get(Color.MediumTurquoise));
            _colors.Add(Get(Color.MediumVioletRed));
            _colors.Add(Get(Color.MidnightBlue));
            _colors.Add(Get(Color.MintCream));
            _colors.Add(Get(Color.MistyRose));
            _colors.Add(Get(Color.Moccasin));
            _colors.Add(Get(Color.NavajoWhite));
            _colors.Add(Get(Color.Navy));
            _colors.Add(Get(Color.OldLace));
            _colors.Add(Get(Color.Olive));
            _colors.Add(Get(Color.OliveDrab));
            _colors.Add(Get(Color.Orange));
            _colors.Add(Get(Color.OrangeRed));
            _colors.Add(Get(Color.Orchid));
            _colors.Add(Get(Color.PaleGoldenrod));
            _colors.Add(Get(Color.PaleGreen));
            _colors.Add(Get(Color.PaleTurquoise));
            _colors.Add(Get(Color.PaleVioletRed));
            _colors.Add(Get(Color.PapayaWhip));
            _colors.Add(Get(Color.PeachPuff));
            _colors.Add(Get(Color.Peru));
            _colors.Add(Get(Color.Pink));
            _colors.Add(Get(Color.Plum));
            _colors.Add(Get(Color.PowderBlue));
            _colors.Add(Get(Color.Purple));
            _colors.Add(Get(Color.Red));
            _colors.Add(Get(Color.RosyBrown));
            _colors.Add(Get(Color.RoyalBlue));
            _colors.Add(Get(Color.SaddleBrown));
            _colors.Add(Get(Color.Salmon));
            _colors.Add(Get(Color.SandyBrown));
            _colors.Add(Get(Color.SeaGreen));
            _colors.Add(Get(Color.SeaShell));
            _colors.Add(Get(Color.Sienna));
            _colors.Add(Get(Color.Silver));
            _colors.Add(Get(Color.SkyBlue));
            _colors.Add(Get(Color.SlateBlue));
            _colors.Add(Get(Color.SlateGray));
            _colors.Add(Get(Color.Snow));
            _colors.Add(Get(Color.SpringGreen));
            _colors.Add(Get(Color.SteelBlue));
            _colors.Add(Get(Color.Tan));
            _colors.Add(Get(Color.Teal));
            _colors.Add(Get(Color.Thistle));
            _colors.Add(Get(Color.Tomato));
            _colors.Add(Get(Color.Transparent));
            _colors.Add(Get(Color.Turquoise));
            _colors.Add(Get(Color.Violet));
            _colors.Add(Get(Color.Wheat));
            _colors.Add(Get(Color.White));
            _colors.Add(Get(Color.WhiteSmoke));
            _colors.Add(Get(Color.Yellow));
            _colors.Add(Get(Color.YellowGreen));
        }
    }
}

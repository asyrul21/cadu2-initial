﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;
using Earthworm.AO;

namespace Geomatic.UI.Utilities.GeometryTrackers
{
    public class LineTracker : GeometryTracker
    {
        public override IGeometry TrackNew(IActiveView activeView, ISymbol symbol, IPoint point)
        {
            IRubberBand2 rubberBand = new RubberLineClass();

            IGeometry geometry = rubberBand.TrackNew(activeView.ScreenDisplay, symbol);

            return geometry;
        }
    }
}
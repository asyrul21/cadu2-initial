﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;
using Earthworm.AO;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Utilities.GeometryTrackers
{
    public class CircleTracker : GeometryTracker
    {
        public override IGeometry TrackNew(IActiveView activeView, ISymbol symbol, IPoint point)
        {
            IRubberBand2 rubberBand = new RubberCircleClass();

            IGeometry geometry = rubberBand.TrackNew(activeView.ScreenDisplay, symbol);

            if (geometry == null)
            {
                return point.Buffer(MapUtils.ConvertPixelsToMapUnits(activeView, 8, 1));
            }

            if (geometry.IsEmpty || (geometry.Envelope.Width == 0 && geometry.Envelope.Height == 0))
            {
                return point.Buffer(MapUtils.ConvertPixelsToMapUnits(activeView, 8, 1));
            }

            ISegmentCollection segmentCollection = new PolygonClass();
            segmentCollection.AddSegment((ISegment)geometry, Type.Missing, Type.Missing);

            return segmentCollection as IGeometry;
        }
    }
}

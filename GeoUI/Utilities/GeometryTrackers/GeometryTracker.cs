﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;

namespace Geomatic.UI.Utilities.GeometryTrackers
{
    public abstract class GeometryTracker
    {
        public IGeometry TrackNew(IActiveView activeView, IPoint point)
        {
            return TrackNew(activeView, null, point);
        }

        public abstract IGeometry TrackNew(IActiveView activeView, ISymbol symbol, IPoint point);
    }
}

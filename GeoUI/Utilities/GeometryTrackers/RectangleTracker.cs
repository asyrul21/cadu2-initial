﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;
using Earthworm.AO;
using Geomatic.Core.Utilities;

namespace Geomatic.UI.Utilities.GeometryTrackers
{
    public class RectangleTracker : GeometryTracker
    {
        public override IGeometry TrackNew(IActiveView activeView, ISymbol symbol, IPoint point)
        {
            IRubberBand2 rubberBand = new RubberEnvelopeClass();

            IGeometry geometry = rubberBand.TrackNew(activeView.ScreenDisplay, symbol);

            IPolygon polygon = new PolygonClass();
            IPointCollection4 pointCollection = (IPointCollection4)polygon;

            if (geometry == null || geometry.IsEmpty || (geometry.Envelope.Width == 0 || geometry.Envelope.Height == 0))
            {
                geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(activeView, 8, 1));

                IPoint point1 = new PointClass();
                point1.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMax);
                pointCollection.AddPoint(point1);

                IPoint point2 = new PointClass();
                point2.PutCoords(geometry.Envelope.XMax, geometry.Envelope.YMax);
                pointCollection.AddPoint(point2);

                IPoint point3 = new PointClass();
                point3.PutCoords(geometry.Envelope.XMax, geometry.Envelope.YMin);
                pointCollection.AddPoint(point3);

                IPoint point4 = new PointClass();
                point4.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMin);
                pointCollection.AddPoint(point4);

                pointCollection.AddPoint(point1);

                return polygon;
            }
            else
            {
                IEnvelope envelope = (IEnvelope)geometry;

                IPoint point1 = new PointClass();
                point1.PutCoords(envelope.XMin, envelope.YMax);
                pointCollection.AddPoint(point1);

                IPoint point2 = new PointClass();
                point2.PutCoords(envelope.XMax, envelope.YMax);
                pointCollection.AddPoint(point2);

                IPoint point3 = new PointClass();
                point3.PutCoords(envelope.XMax, envelope.YMin);
                pointCollection.AddPoint(point3);

                IPoint point4 = new PointClass();
                point4.PutCoords(envelope.XMin, envelope.YMin);
                pointCollection.AddPoint(point4);

                pointCollection.AddPoint(point1);

                return polygon;
            }
        }
    }
}

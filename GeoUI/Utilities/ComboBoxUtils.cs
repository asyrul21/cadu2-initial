﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Rows.StreetConsiced;
using Geomatic.Core.Search;
using Geomatic.UI.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core;
using Geomatic.UI.Controls;
using Geomatic.Core.Features.Streets;

//added for optimisation
using System.Diagnostics;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Features.Jupem;

using GPoiWeb = Geomatic.Core.Rows.GPoiWeb;

namespace Geomatic.UI.Utilities
{
    public class ComboBoxUtils
    {
        // added by asyrul
        private IEnumerable<GLocation> getAllLocation(string state)
        {
            return GLocation.GetAllSectionByState(state, false);
        }

        // ==================
        // Data Attributes
        //
        #region AS
        // AS
        protected static IEnumerable<GAsStreetConcised> allASStreet
        {
            get
            {
                return GAsStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allASJunction
        {
            get
            {
                return GAsJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allASLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("AS", false);
            }
        }

        protected static IEnumerable<GBuilding> allASBuilding
        {
            get
            {
                return GAsBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allASBuildingGroup
        {
            get
            {
                return GAsBuildingGroup.GetAll();
                //return GBuildingGroup.GetAll(false, SegmentName.AS);
            }
        }

        protected static IEnumerable<GLandmark> allASLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.AS);
            }
        }

        protected static IEnumerable<GPoi> allASPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.AS);
            }
        }
        #endregion

        #region JH
        // JH
        protected static IEnumerable<GJhStreetConcised> allJHStreet
        {
            get
            {
                return GJhStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allJHJunction
        {
            get
            {
                return GJhJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allJHLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("JH", false);
            }
        }

        protected static IEnumerable<GBuilding> allJHBuilding
        {
            get
            {
                return GJhBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allJHBuildingGroup
        {
            get
            {
                //return GBuildingGroup.GetAll(false, SegmentName.JH);
                return GJhBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allJHLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.JH);
            }
        }

        protected static IEnumerable<GPoi> allJHPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.JH);
            }
        }
        #endregion

        #region JP
        // JP
        protected static IEnumerable<GJpStreetConcised> allJPStreet
        {
            get
            {
                return GJpStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allJPJunction
        {
            get
            {
                return GJpJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allJPLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("JP", false);
            }
        }

        protected static IEnumerable<GBuilding> allJPBuilding
        {
            get
            {
                return GJpBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allJPBuildingGroup
        {
            get
            {
                //return GBuildingGroup.GetAll(false, SegmentName.JP);
                return GJpBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allJPLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.JP);
            }
        }

        protected static IEnumerable<GPoi> allJPPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.JP);
            }
        }
        #endregion

        #region KG
        // KG
        protected static IEnumerable<GKgStreetConcised> allKGStreet
        {
            get
            {
                return GKgStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allKGJunction
        {
            get
            {
                return GKgJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allKGLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("KG", false);
            }
        }

        protected static IEnumerable<GBuilding> allKGBuilding
        {
            get
            {
                return GKgBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allKGBuildingGroup
        {
            get
            {
                return GKgBuildingGroup.GetAll();
                //return GBuildingGroup.GetAll(false, SegmentName.KG);
            }
        }

        protected static IEnumerable<GLandmark> allKGLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.KG);
            }
        }

        protected static IEnumerable<GPoi> allKGPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.KG);
            }
        }
        #endregion

        #region KK
        // KK
        protected static IEnumerable<GKkStreetConcised> allKKStreet
        {
            get
            {
                return GKkStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allKKJunction
        {
            get
            {
                return GKkJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allKKLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("KK", false);
            }
        }

        protected static IEnumerable<GBuilding> allKKBuilding
        {
            get
            {
                return GKkBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allKKBuildingGroup
        {
            get
            {
                return GKkBuildingGroup.GetAll();
                //return GBuildingGroup.GetAll(false, SegmentName.KK);
            }
        }

        protected static IEnumerable<GLandmark> allKKLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.KK);
            }
        }

        protected static IEnumerable<GPoi> allKKPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.KK);
            }
        }
        #endregion

        #region KN
        // KN
        protected static IEnumerable<GKnStreetConcised> allKNStreet
        {
            get
            {
                return GKnStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allKNJunction
        {
            get
            {
                return GKnJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allKNLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("KN", false);
            }
        }

        protected static IEnumerable<GBuilding> allKNBuilding
        {
            get
            {
                return GKnBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allKNBuildingGroup
        {
            get
            {
                //return GBuildingGroup.GetAll(false, SegmentName.KN);
                return GKnBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allKNLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.KN);
            }
        }

        protected static IEnumerable<GPoi> allKNPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.KN);
            }
        }
        #endregion

        #region KV
        // KV
        protected static IEnumerable<GKvStreetConcised> allKVStreet
        {
            get
            {
                //MessageBox.Show("This should run only once");
                //return GKvStreet.GetAll(false);
                return GKvStreetConcised.GetAll(false);
                //switch (SegmentName)
                //{
                //    case SegmentName.AS:
                //        return GAsStreetConcised.GetAll(false);
                //    case SegmentName.JH:
                //        return GJhStreetConcised.GetAll(false);
                //    case SegmentName.JP:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.KG:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.KK:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.KN:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.KV:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.MK:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.PG:
                //        return GKvStreetConcised.GetAll(false);
                //    case SegmentName.TG:
                //        return GKvStreetConcised.GetAll(false);
                //    default:
                //        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                //}
            }
        }

        protected static IEnumerable<GLocation> allKVLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("KV", false);
                //return GLocation.GetAllSectionByState(false); // verified
            }
        }

        protected static IEnumerable<GJunction> allKVJunction
        {
            get
            {
                return GKvJunction.GetAll();
            }
        }

        protected static IEnumerable<GBuilding> allKVBuilding
        {
            get
            {
                return GKvBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allKVBuildingGroup
        {
            get
            {
                return GKvBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allKVLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.KV);
            }
        }

        protected static IEnumerable<GPoi> allKVPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.KV);
            }
        }
        #endregion

        #region MK
        // MK
        protected static IEnumerable<GMkStreetConcised> allMKStreet
        {
            get
            {
                return GMkStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allMKJunction
        {
            get
            {
                return GMkJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allMKLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("MK", false);
            }
        }

        protected static IEnumerable<GBuilding> allMKBuilding
        {
            get
            {
                return GMkBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allMKBuildingGroup
        {
            get
            {
                return GMkBuildingGroup.GetAll();
                //return GBuildingGroup.GetAll(false, SegmentName.MK);
            }
        }

        protected static IEnumerable<GLandmark> allMKLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.MK);
            }
        }

        protected static IEnumerable<GPoi> allMKPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.MK);
            }
        }
        #endregion

        #region PG
        // PG
        protected static IEnumerable<GPgStreetConcised> allPGStreet
        {
            get
            {
                return GPgStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allPGJunction
        {
            get
            {
                return GPgJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allPGLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("PG", false);
            }
        }

        protected static IEnumerable<GBuilding> allPGBuilding
        {
            get
            {
                return GPgBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allPGBuildingGroup
        {
            get
            {
                //return GBuildingGroup.GetAll(false, SegmentName.PG);
                return GPgBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allPGLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.PG);
            }
        }

        protected static IEnumerable<GPoi> allPGPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.PG);
            }
        }
        #endregion

        #region TG
        // TG
        protected static IEnumerable<GTgStreetConcised> allTGStreet
        {
            get
            {
                return GTgStreetConcised.GetAll(false);

            }
        }

        protected static IEnumerable<GJunction> allTGJunction
        {
            get
            {
                return GTgJunction.GetAll();
            }
        }

        protected static IEnumerable<GLocation> allTGLocation
        {
            get
            {
                return GLocation.GetAllSectionByState("TG", false);
            }
        }

        protected static IEnumerable<GBuilding> allTGBuilding
        {
            get
            {
                return GTgBuilding.GetAll();
            }
        }

        protected static IEnumerable<GBuildingGroup> allTGBuildingGroup
        {
            get
            {
                //return GBuildingGroup.GetAll(false, SegmentName.TG);
                return GTgBuildingGroup.GetAll();
            }
        }

        protected static IEnumerable<GLandmark> allTGLandmark
        {
            get
            {
                return GLandmark.GetAll(false, SegmentName.TG);
            }
        }

        protected static IEnumerable<GPoi> allTGPoi
        {
            get
            {
                return GPoi.GetAll(false, SegmentName.TG);
            }
        }
        #endregion

        // ==================

        public static void PopulateConstructionStatus(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GConstructionStatus status in GConstructionStatus.GetAll(false))
            {
                cb.Items.Add(status);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateNavigationStatus(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            cb.Items.Add(new ComboBoxItem<string, int>("READY", 1));
            cb.Items.Add(new ComboBoxItem<string, int>("NOT READY", 0));
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        #region Populate Input Street Name

        public static void PopulateInputStreetName(ComboBox cb, string name, RepositoryFactory repo, Query<GStreet> query, int? Limit)

        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            List <string> _streetNameResult = new List<string>();
            List<string> _streetNameResultDistinct = new List<string>();
            foreach (GStreet street in repo.Search(query, true, Limit))
            {
                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    _streetNameResult.Add(street.Name);
                }
            }
            // to get distinct street name
            _streetNameResultDistinct = _streetNameResult.Distinct().ToList();
            foreach (string streetName in _streetNameResultDistinct)
            {             
               cb.Items.Add(streetName);               
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateInputStreetName2(ComboBox cb, string name, RepositoryFactory repo, Query<GStreet> query, int? Limit)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            List<string> _streetNameResult = new List<string>();
            List<string> _streetNameResultDistinct = new List<string>();
            foreach (GStreet street in repo.Search(query, true, Limit))
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " " && street.Name2 != "-")
                {
                    _streetNameResult.Add(street.Name2);
                }
            }
            // to get distinct street name
            _streetNameResultDistinct = _streetNameResult.Distinct().ToList();
            foreach (string streetName2 in _streetNameResultDistinct)
            {
                cb.Items.Add(streetName2);
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }
        #endregion

        //added by asyrul
        #region Population AS
        //
        // Populate AS Street Names, Section, City
        //
        public static void PopulateASStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GAsStreetConcised> filtered = allASStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GAsStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown           
        }

        public static void PopulateASStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GAsStreetConcised> filtered = allASStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GAsStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // building
        public static void PopulateASBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allASBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateASBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allASBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building Group

        public static void PopulateASBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allASBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulateASLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allASLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateASLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allASLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI
        public static void PopulateASPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allASPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateASPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allASPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateASStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GAsStreetConcised> filteredList = allASStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GAsStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // postcode
        public static void PopulateASPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GAsStreetConcised> filtered = allASStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GAsStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateASCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allASLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }

            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateASSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GAsStreetConcised> filtered = allASStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GAsStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateASSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allASLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateASState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "AS"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateASJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allASJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population JH
        //
        // Populate JH Street Names, Section, City
        //
        public static void PopulateJHStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJhStreetConcised> filtered = allJHStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJhStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJHStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJhStreetConcised> filtered = allJHStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GJhStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building
        public static void PopulateJHBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allJHBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJHBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allJHBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateJHBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allJHBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark
        public static void PopulateJHLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allJHLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJHLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allJHLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            // cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulateJHPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allJHPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJHPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allJHPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown           
        }

        public static void RepopulateJHStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GJhStreetConcised> filteredList = allJHStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GJhStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // Postcode
        public static void PopulateJHPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJhStreetConcised> filtered = allJHStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GJhStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateJHCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allJHLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateJHSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJhStreetConcised> filtered = allJHStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GJhStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateJHSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allJHLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateJHState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "JH"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateJHJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allJHJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population JP
        //
        // Populate JP Street Names, Section, City
        //
        public static void PopulateJPStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJpStreetConcised> filtered = allJPStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJpStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown

        }

        public static void PopulateJPStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJpStreetConcised> filtered = allJPStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GJpStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // building

        public static void PopulateJPBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allJPBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJPBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allJPBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateJPBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allJPBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark
        public static void PopulateJPLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allJPLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJPLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allJPLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI
        public static void PopulateJPPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allJPPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateJPPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allJPPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateJPStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GJpStreetConcised> filteredList = allJPStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GJpStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateJPPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJpStreetConcised> filtered = allJPStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GJpStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateJPCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allJPLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateJPSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJpStreetConcised> filtered = allJPStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GJpStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateJPSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allJPLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateJPState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "JP"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateJPJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allJPJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population KG
        //
        // Populate KG Street Names, Section, City
        //
        public static void PopulateKGStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKgStreetConcised> filtered = allKGStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GKgStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown          
        }

        public static void PopulateKGStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKgStreetConcised> filtered = allKGStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GKgStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // building
        public static void PopulateKGBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKGBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKGBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKGBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group
        public static void PopulateKGBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allKGBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark
        public static void PopulateKGLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKGLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKGLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKGLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI
        public static void PopulateKGPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKGPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKGPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKGPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateKGStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GKgStreetConcised> filteredList = allKGStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GKgStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateKGPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKgStreetConcised> filtered = allKGStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GKgStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateKGCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKGLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateKGSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKgStreetConcised> filtered = allKGStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GKgStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateKGSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKGLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateKGState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "KG"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateKGJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allKGJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population KK
        //
        // Populate KK Street Names, Section, City
        //
        public static void PopulateKKStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKkStreetConcised> filtered = allKKStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GKkStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown

        }

        public static void PopulateKKStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKkStreetConcised> filtered = allKKStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GKkStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building

        public static void PopulateKKBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKKBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKKBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKKBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateKKBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allKKBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark
        public static void PopulateKKLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKKLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKKLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKKLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI
        public static void PopulateKKPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKKPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKKPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKKPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateKKStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GKkStreetConcised> filteredList = allKKStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GKkStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateKKPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKkStreetConcised> filtered = allKKStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GKkStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateKKCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKKLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateKKSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKkStreetConcised> filtered = allKKStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GKkStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateKKSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKKLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateKKState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "KK"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateKKJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allKKJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population KN
        //
        // Populate KN Street Names, Section, City
        //
        public static void PopulateKNStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKnStreetConcised> filtered = allKNStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GKnStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown

        }

        public static void PopulateKNStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKnStreetConcised> filtered = allKNStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GKnStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building

        public static void PopulateKNBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKNBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; 
        }

        public static void PopulateKNBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKNBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateKNBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allKNBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulateKNLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKNLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKNLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKNLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulateKNPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKNPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKNPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKNPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateKNStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GKnStreetConcised> filteredList = allKNStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GKnStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateKNPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKnStreetConcised> filtered = allKNStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GKnStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateKNCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKNLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateKNSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKnStreetConcised> filtered = allKNStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GKnStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateKNSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allKNLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateKNState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "KN"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }


            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }


        // Junction
        public static void PopulateKNJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allKNJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population KV
        //
        // Populate KV Street Names, Section, City
        //      
        public static void PopulateKVStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            //foreach (GKvStreetName street in allStreet)
            //foreach (GKvStreet street in GKvStreet.GetAll(false))

            IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            //foreach (GKvStreetName street in allStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList())
            foreach (GKvStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        public static void PopulateKVStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GKvStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // building

        public static void PopulateKVBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKVBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKVBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allKVBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateKVBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allKVBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulateKVLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKVLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKVLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allKVLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulateKVPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKVPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateKVPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allKVPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateKVStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GKvStreetConcised> filteredList = allKVStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GKvStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateKVPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GKvStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateKVCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            //IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.City).Select(y => y.First()).ToList();
            //foreach (GKvStreetConcised street in filtered)
            //{
            //    if (street.City != null && street.City != "" && street.City != " "
            //        && street.City != "-" && street.City != "  ")
            //    {
            //        cb.Items.Add(street.City);
            //    }
            //}

            IEnumerable<GLocation> filtered = allKVLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateKVSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GKvStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateKVSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            //IEnumerable<GKvStreetConcised> filtered = allKVStreet.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            //foreach (GKvStreetConcised street in filtered)
            //{
            //    if (street.Section != null && street.Section != "" && street.Section != " "
            //        && street.Section != "-" && street.Section != "  ")
            //    {
            //        cb.Items.Add(street.Section);
            //    }
            //}

            IEnumerable<GLocation> filtered = allKVLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateKVState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "KV"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateKVJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allKVJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        #endregion

        #region Population MK
        //
        // Populate MK Street Names, Section, City
        //
        public static void PopulateMKStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GMkStreetConcised> filtered = allMKStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GMkStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            //cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown           
        }

        public static void PopulateMKStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GMkStreetConcised> filtered = allMKStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GMkStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        // building

        public static void PopulateMKBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allMKBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateMKBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allMKBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateMKBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allMKBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulateMKLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allMKLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateMKLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allMKLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulateMKPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allMKPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateMKPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allMKPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateMKStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GMkStreetConcised> filteredList = allMKStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GMkStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateMKPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GMkStreetConcised> filtered = allMKStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GMkStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateMKCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allMKLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateMKSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GMkStreetConcised> filtered = allMKStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GMkStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateMKSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allMKLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // State
        public static void PopulateMKState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "MK"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateMKJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allMKJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population PG
        //
        // Populate PG Street Names, Section, City
        //
        public static void PopulatePGStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPgStreetConcised> filtered = allPGStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPgStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown
        }

        public static void PopulatePGStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPgStreetConcised> filtered = allPGStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPgStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building

        public static void PopulatePGBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allPGBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulatePGBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allPGBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulatePGBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allPGBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulatePGLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allPGLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulatePGLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allPGLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();

            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulatePGPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allPGPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulatePGPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allPGPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulatePGStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GPgStreetConcised> filteredList = allPGStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GPgStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulatePGPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPgStreetConcised> filtered = allPGStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GPgStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulatePGCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allPGLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulatePGSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPgStreetConcised> filtered = allPGStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GPgStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulatePGSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allPGLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulatePGState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "PG"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulatePGJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allPGJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }
        #endregion

        #region Population TG
        //
        // Populate TG Street Names, Section, City
        //
        public static void PopulateTGStreetName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GTgStreetConcised> filtered = allTGStreet.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GTgStreetConcised street in filtered)
            {

                if (street.Name != null && street.Name != "" && street.Name != " " && street.Name != "-")
                {
                    cb.Items.Add(street.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode; // comment bcoz double dropdown          
        }

        public static void PopulateTGStreetName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GTgStreetConcised> filtered = allTGStreet.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GTgStreetConcised street in filtered)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building

        public static void PopulateTGBuildingName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allTGBuilding.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateTGBuildingName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuilding> filtered = allTGBuilding.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GBuilding building in filtered)
            {

                if (building.Name2 != null && building.Name2 != "" && building.Name2 != " " && building.Name2 != "-")
                {
                    cb.Items.Add(building.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // building group

        public static void PopulateTGBuildingGroupName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GBuildingGroup> filtered = allTGBuildingGroup.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GBuildingGroup building in filtered)
            {

                if (building.Name != null && building.Name != "" && building.Name != " " && building.Name != "-")
                {
                    cb.Items.Add(building.Name);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Landmark

        public static void PopulateTGLandmarkName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allTGLandmark.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name != null && landmark.Name != "" && landmark.Name != " " && landmark.Name != "-")
                {
                    cb.Items.Add(landmark.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateTGLandmarkName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLandmark> filtered = allTGLandmark.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GLandmark landmark in filtered)
            {

                if (landmark.Name2 != null && landmark.Name2 != "" && landmark.Name2 != " " && landmark.Name2 != "-")
                {
                    cb.Items.Add(landmark.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // POI

        public static void PopulateTGPoiName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allTGPoi.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name != null && poi.Name != "" && poi.Name != " " && poi.Name != "-")
                {
                    cb.Items.Add(poi.Name);
                }
            }

            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateTGPoiName2(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GPoi> filtered = allTGPoi.GroupBy(x => x.Name2).Select(y => y.First()).ToList();
            foreach (GPoi poi in filtered)
            {

                if (poi.Name2 != null && poi.Name2 != "" && poi.Name2 != " " && poi.Name2 != "-")
                {
                    cb.Items.Add(poi.Name2);
                }
            }
            cb.Sorted = true; //sorting is not needed to faster population // takes 5 seconds
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void RepopulateTGStreetName2(ComboBox cb, string filter)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            IEnumerable<GTgStreetConcised> filteredList = allTGStreet
                                                    .GroupBy(x => x.Name2)
                                                    .Select(y => y.First())
                                                    .Where(street => street.Name == filter)
                                                    .ToList();

            foreach (GTgStreetConcised street in filteredList)
            {
                if (street.Name2 != null && street.Name2 != "" && street.Name2 != " "
                    && street.Name2 != "-" && street.Name2 != "  ")
                {
                    cb.Items.Add(street.Name2);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Postcode
        public static void PopulateTGPostcode(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GTgStreetConcised> filtered = allTGStreet.GroupBy(x => x.Postcode).Select(y => y.First()).ToList();
            foreach (GTgStreetConcised street in filtered)
            {
                if (street.Postcode != null && street.Postcode != "" && street.Postcode != " "
                    && street.Postcode != "-" && street.Postcode != "  ")
                {
                    cb.Items.Add(street.Postcode);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // City
        public static void PopulateTGCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allTGLocation.GroupBy(x => x.City).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.City != null && location.City != "" && location.City != " "
                    && location.City != "-" && location.City != "  ")
                {
                    cb.Items.Add(location.City);
                }
            }
            //cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Sub City
        public static void PopulateTGSubCity(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GTgStreetConcised> filtered = allTGStreet.GroupBy(x => x.SubCity).Select(y => y.First()).ToList();
            foreach (GTgStreetConcised street in filtered)
            {
                if (street.SubCity != null && street.SubCity != "" && street.SubCity != " "
                    && street.SubCity != "-" && street.SubCity != "  ")
                {
                    cb.Items.Add(street.SubCity);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // Section
        public static void PopulateTGSection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GLocation> filtered = allTGLocation.GroupBy(x => x.Section).Select(y => y.First()).ToList();
            foreach (GLocation location in filtered)
            {
                if (location.Section != null && location.Section != "" && location.Section != " "
                    && location.Section != "-" && location.Section != "  ")
                {
                    cb.Items.Add(location.Section);
                }
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        // state
        public static void PopulateTGState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GState state in GState.GetAll(false, "TG"))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value
            }

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Junction
        public static void PopulateTGJunctionName(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            IEnumerable<GJunction> filtered = allTGJunction.GroupBy(x => x.Name).Select(y => y.First()).ToList();
            foreach (GJunction junction in filtered)
            {
                if (junction.Name != null && junction.Name != "" && junction.Name != " "
                    && junction.Name != "-" && junction.Name != "  ")
                {
                    cb.Items.Add(junction.Name);
                }
            }

            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        #endregion

        //added by asyrul end

        public static void PopulateStreetStatus(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetStatus status in GStreetStatus.GetAll(false))
            {
                cb.Items.Add(status);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetRestrictionStatus(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetStatus status in GStreetStatus.GetAll(false))
            {
                cb.Items.Add(status);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetNetworkClass(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetNetworkClass streetClass in GStreetNetworkClass.GetAll(false))
            {
                cb.Items.Add(streetClass);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetClass(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetClass streetClass in GStreetClass.GetAll(false))
            {
                cb.Items.Add(streetClass);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetCategory(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetCategory category in GStreetCategory.GetAll(false))
            {
                cb.Items.Add(category);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetFilterLevel(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetFilterLevel filterLevel in GStreetFilterLevel.GetAll(false))
            {
                cb.Items.Add(filterLevel);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetDesign(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetDesign design in GStreetDesign.GetAll(false))
            {
                cb.Items.Add(design);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetType type in GStreetType.GetAll(false))
            {
                cb.Items.Add(type);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetDirection(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetDirection direction in GStreetDirection.GetAll(false))
            {
                cb.Items.Add(direction);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetTollType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetTollType tollType in GStreetTollType.GetAll(false))
            {
                cb.Items.Add(tollType);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateStreetDivider(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GStreetDivider divider in GStreetDivider.GetAll(false))
            {
                cb.Items.Add(divider);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulatePropertyType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            foreach (GPropertyType type in GPropertyType.GetAll(false))
            {
                cb.Items.Add(new ComboBoxItem<string, GPropertyType>(StringUtils.TrimSpaces(type.Abbreviation) + " - " + StringUtils.TrimSpaces(type.Name), type));
            }
            cb.Sorted = true;
            cb.EndUpdate();
            //cb.AutoCompleteMode = mode;
        }

        public static void PopulateSource(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GSource source in GSource.GetAll(false))
            {
                cb.Items.Add(source);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateForecastSource(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GSource source in GSource.GetAll(false))
            {
                cb.Items.Add(source);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }


        public static void PopulateJunctionType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GJunctionType type in GJunctionType.GetAll(false))
            {
                cb.Items.Add(type);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateState(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GState state in GState.GetAll(false))
            {
                cb.Items.Add(new ComboBoxItem<string, string>(state.Name, state.Code)); //key, value

                //cb.Items.Add(state);

            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateUnit(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            cb.Items.Add(new ComboBoxItem<string, double>("SQUARE FEET", 1));
            cb.Items.Add(new ComboBoxItem<string, double>("SQUARE METER", 10.763911));
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateYear(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            for (int i = DateTime.Today.Year; i >= 1900; i--)
            {
                cb.Items.Add(i.ToString());
            }
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateSegment(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            cb.Items.Add(new ComboBoxItem<string, SegmentName>("AS - Kedah & Perlis   ", SegmentName.AS));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("JH - Johor   ", SegmentName.JH));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("JP - Perak   ", SegmentName.JP));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("KG - Sarawak   ", SegmentName.KG));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("KK - Sabah & Labuan   ", SegmentName.KK));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("KN - Pahang   ", SegmentName.KN));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("KV - Selangor & Kuala Lumpur   ", SegmentName.KV));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("MK - Melaka   ", SegmentName.MK));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("PG - Penang   ", SegmentName.PG));
            cb.Items.Add(new ComboBoxItem<string, SegmentName>("TG - Terengganu & Kelantan   ", SegmentName.TG));

            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateTollRouteType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GTollRouteType type in GTollRouteType.GetAll(false))
            {
                cb.Items.Add(new ComboBoxItem<string, GTollRouteType>(StringUtils.TrimSpaces(type.Name), type));
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateVehicleType(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GVehicleType type in GVehicleType.GetAll(false))
            {
                cb.Items.Add(new ComboBoxItem<string, GVehicleType>(StringUtils.TrimSpaces(type.Name), type));
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateWorkAreaId(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GWorkArea work_area in GWorkArea.GetAllCompleteAreaWithSegment())
            //foreach (GWorkArea work_area in GWorkArea.GetAll(true, "KV"))
            {
                cb.Items.Add(work_area.OID);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        // Added by Noraini Ali - CADU 2 - Work Area by User
        public static void PopulateWorkAreaByUser(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GWorkArea work_area in GWorkArea.GetAllCompleteAreaByUser())
            {
                cb.Items.Add(work_area.OID);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }
        // Added by Noraini Ali - CADU 2 AND - Aug 2020 
        public static void PopulateWorkArea(ComboBox cb, string segmentname)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();
            foreach (GWorkArea work_area in GWorkArea.GetCompletedWorkArea(segmentname))
            {
                cb.Items.Add(work_area.OID);
                //cb.Items.Add(new ComboBoxItem<string, string>(work_area.OID.ToString(), work_area.UpdatedBy));
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }
        // end

        // 
        public static void PopulateWebUserName(ComboBox cb, SegmentName segmentname)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            List<string> listUserName = new List<string>();
            List<string> listUserNameDistinct = new List<string>();

            foreach (GPoiWeb Webpoi in GPoiWeb.GetAll(false, segmentname))
            {
                if (Webpoi.UpdatedBy != null && Webpoi.UpdatedBy != "" && Webpoi.UpdatedBy != " " && Webpoi.UpdatedBy != "-")
                {
                    listUserName.Add(Webpoi.UpdatedBy);
                }
            }
            listUserNameDistinct = listUserName.Distinct().ToList();
            foreach (string username in listUserNameDistinct)
            {
                cb.Items.Add(username);
            }
            cb.Sorted = true;
            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }

        public static void PopulateWebStatus(ComboBox cb)
        {
            AutoCompleteMode mode = cb.AutoCompleteMode;
            cb.AutoCompleteMode = AutoCompleteMode.None;
            cb.BeginUpdate();
            cb.Items.Clear();

            cb.Items.Add("ADD");
            cb.Items.Add("EDIT");
            cb.Items.Add("DELETE");

            cb.EndUpdate();
            cb.AutoCompleteMode = mode;
        }
    }
}

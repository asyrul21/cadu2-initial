﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Geomatic.UI.Utilities
{
    public class VerifyAttName
    {
        public readonly static string[] propertyAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "HOUSE NO :",
            "LOT_NO :",
            "PROPERTY TYPE :",
            "QUANTITY :",
            "YEAR INSTALL :",
            "CONS STATUS :",
            "SOURCE :",
            "STREET ID :",
            "DATE CREATED :",
            "DATE UPDATED :",
             "X :",
             "Y :"
        };

        public readonly static string[] buildingAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "NAME :",
            "NAME 2 :",
            "PROPERTY ID :",
            "NUM FLOOR :",
            "FLOOR NUM UNIT :",
            "SOURCE :",
            "GROUP ID :",
             "X :",
             "Y :"
        };

        public readonly static string[] landmarkAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "NAME 1 :",
            "NAME 2 :",
            "STREET ID :",
            "UPDATE STATUS :",
            "NAVI STATUS :",
            "SOURCE :",
            "LANDMARK CODE :",
             "X :",
             "Y :"
        };

        public readonly static string[] streetAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "NAME 1 :",
            "NAME 2 :",
            "TYPE :",
            "SECTION NAME :",
            "CITY :",
            "STATE :",
            "POSTCODE :",
            "CONS STATUS :",
            "STREET STATUS :",
            "NETWORK CLASS :",
            "LENGTH :"
        };
        public readonly static string[] junctionAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "JUNCTION NAME :",
            "JUNCTION TYPE :",
             "X :",
             "Y :"
        };

        public readonly static string[] buildingGroupAttName = {
            "OBJECT ID :",
            "AREA ID :",
            "AND STATUS :",
            "NAME :",
            "CODE :",
            "STREET ID :",
            "NAVI STATUS :",
            "SOURCE :",
            "NUM UNIT :",
            "FORECAST UNIT :",
            "FORECAST SOURCE :",
             "X :",
             "Y :"
        };
    }
    public class StringAttributeName
    {
        public const string Property = "Property";
        public const string Building = "Building";
        public const string Street = "Street";
        public const string BuildingGroup = "BuildingGroup";
        public const string Junction = "Junction";
        public const string Landmark = "Landmark";
    }


}

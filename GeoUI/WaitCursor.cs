﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.UI
{
    public class WaitCursor : IDisposable
    {
        private Cursor _previousCursor;

        public WaitCursor()
        {
            _previousCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
        }

        public void Dispose()
        {
            Cursor.Current = _previousCursor;
        }
    }
}

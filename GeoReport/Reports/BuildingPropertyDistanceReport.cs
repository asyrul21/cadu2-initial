﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Earthworm.AO;
using Geomatic.Core.Logs;
using ESRI.ArcGIS.Geometry;
using System.Reflection;

namespace GeoReport.Reports
{
    class BuildingPropertyDistanceReport
    {
        public void Run(SegmentName segmentName, string filename)
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ILog log = new CsvLog(directory + "\\" + filename);

            List<string> columns = new List<string>();
            columns.Add("\"Building id\"");
            columns.Add("\"Property id\"");
            columns.Add("\"Distance\"");
            columns.Add("\"Building name\"");
            columns.Add("\"Building name2\"");
            columns.Add("\"Modified_user\"");
            columns.Add("\"Modified_date\"");

            log.Log(string.Join(",", columns.ToArray()));

            RepositoryFactory repo = new RepositoryFactory(segmentName);
            foreach (GBuilding building in repo.Search<GBuilding>())
            {
                GProperty property = building.GetProperty();
                if (property == null)
                {
                    continue;
                }
                double distance = building.Shape.DistanceTo(property.Shape);
                if (distance > 500)
                {
                    List<string> fields = new List<string>();
                    fields.Add(string.Format("\"{0}\"", building.OID));
                    fields.Add(string.Format("\"{0}\"", property.OID));
                    fields.Add(string.Format("\"{0}\"", distance));
                    fields.Add(string.Format("\"{0}\"", building.Name));
                    fields.Add(string.Format("\"{0}\"", building.Name2));
                    fields.Add(string.Format("\"{0}\"", building.UpdatedBy));
                    fields.Add(string.Format("\"{0}\"", building.DateUpdated));
                    log.Log(string.Join(",", fields.ToArray()));
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core;
using ESRI.ArcGIS.esriSystem;
using System.Windows.Forms;
using GeoReport.Reports;

namespace GeoReport
{
    class Program
    {
        private static LicenseInitializer m_AOLicenseInitializer = new LicenseInitializer();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //ESRI License Initializer generated code.
            if (!m_AOLicenseInitializer.InitializeApplication(new esriLicenseProductCode[] { esriLicenseProductCode.esriLicenseProductCodeEngineGeoDB },
            new esriLicenseExtensionCode[] { }))
            {
                MessageBox.Show(m_AOLicenseInitializer.LicenseMessage() +
                "\n\nThis application could not initialize with the correct ArcGIS license and will shutdown.",
                "ArcGIS License Failture");
                m_AOLicenseInitializer.ShutdownApplication();
                Application.Exit();
                return;
            }

            //new BuildingPropertyDistanceReport().Run(SegmentName.AS, "AS_BuildingPropertyDistance");
            //new BuildingPropertyDistanceReport().Run(SegmentName.JH, "JH_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.JP, "JP_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.KG, "KG_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.KK, "KK_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.KN, "KN_BuildingPropertyDistance");
            //new BuildingPropertyDistanceReport().Run(SegmentName.KV, "KV_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.MK, "MK_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.PG, "PG_BuildingPropertyDistance");
            new BuildingPropertyDistanceReport().Run(SegmentName.TG, "TG_BuildingPropertyDistance");


            //ESRI License Initializer generated code.
            //Do not make any call to ArcObjects after ShutDownApplication()
            m_AOLicenseInitializer.ShutdownApplication();
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using System.Reflection;
using Geomatic.Core.Logs;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities;

namespace GeoObjectUpdater
{
    public partial class PropertyTypeUpdaterForm : Form
    {
        public PropertyTypeUpdaterForm()
        {
            InitializeComponent();
        }

        private SegmentName SegmentName
        {
            get
            {
                return (SegmentName)cbSegment.SelectedItem;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            cbSegment.Items.Add(SegmentName.AS);
            cbSegment.Items.Add(SegmentName.JH);
            cbSegment.Items.Add(SegmentName.JP);
            cbSegment.Items.Add(SegmentName.KG);
            cbSegment.Items.Add(SegmentName.KK);
            cbSegment.Items.Add(SegmentName.KN);
            cbSegment.Items.Add(SegmentName.KV);
            cbSegment.Items.Add(SegmentName.MK);
            cbSegment.Items.Add(SegmentName.PG);
            cbSegment.Items.Add(SegmentName.TG);

            ComboBoxUtils.PopulatePropertyType(cbTypeFrom);
            ComboBoxUtils.PopulatePropertyType(cbTypeTo);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ILog propLog = new CsvLog(directory + "\\" + SegmentName + "_Property_Update_Log");

            List<string> columns = new List<string>();
            columns.Add("\"Property id\"");
            columns.Add("\"Type Code\"");
            columns.Add("\"Type Name\"");
            columns.Add("\"Lot\"");
            columns.Add("\"House No\"");

            propLog.Log(string.Join(",", columns.ToArray()));

            Query<GProperty> query = new Query<GProperty>(SegmentName);
            //query.AddClause(() => query.Object.Type, "IN (20, 21, 34, 36, 300, 301, 302, 313, 314)");
            query.Obj.Type = ((GPropertyType)cbTypeFrom.SelectedItem).Code;

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            repo.StartTransaction(() =>
            {
                foreach (GProperty property in repo.Search(query))
                {
                    List<string> fields = new List<string>();
                    fields.Add(string.Format("\"{0}\"", property.OID));
                    fields.Add(string.Format("\"{0}\"", property.Type));
                    fields.Add(string.Format("\"{0}\"", property.TypeAbbreviationValue));
                    fields.Add(string.Format("\"{0}\"", property.Lot));
                    fields.Add(string.Format("\"{0}\"", property.House));
                    propLog.Log(string.Join(",", fields.ToArray()));

                    //update to new property type
                    GPropertyType newType = (GPropertyType)cbTypeTo.SelectedItem;
                    property.Type = newType.Code;
                    repo.Update(property);
                }

            });

            //Session.Current[SegmentName].Save(true);
            MessageBox.Show("Completed");
        }
    }
}

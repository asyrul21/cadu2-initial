﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using System.Reflection;
using Geomatic.Core.Logs;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace GeoObjectUpdater
{
    public partial class MultiFloorFlagUpdaterForm : Form
    {
        public MultiFloorFlagUpdaterForm()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            foreach (string selectedSegment in chkSegment.CheckedItems)
            {
                switch (selectedSegment)
                {
                    case "AS":
                        ProcessForSegment(SegmentName.AS);
                        break;

                    case "JH":
                        ProcessForSegment(SegmentName.JH);
                        break;

                    case "JP":
                        ProcessForSegment(SegmentName.JP);
                        break;

                    case "KG":
                        ProcessForSegment(SegmentName.KG);
                        break;

                    case "KK":
                        ProcessForSegment(SegmentName.KK);
                        break;

                    case "KN":
                        ProcessForSegment(SegmentName.KN);
                        break;

                    case "MK":
                        ProcessForSegment(SegmentName.MK);
                        break;

                    case "PG":
                        ProcessForSegment(SegmentName.PG);
                        break;

                    case "TG":
                        ProcessForSegment(SegmentName.TG);
                        break;

                    case "KV":
                        ProcessForSegment(SegmentName.KV);
                        break;

                    default:
                        throw new Exception("unknown segment");
                }
            }

            MessageBox.Show("Completed");
        }

        private void ProcessForSegment(SegmentName segmentName)
        {
            RepositoryFactory repo = new RepositoryFactory(segmentName);

            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ILog floorLog = new CsvLog(directory + "\\" + segmentName + "_FloorFlag_Log");

            List<string> columns = new List<string>();
            columns.Add("\"FLOOR ID\"");
            columns.Add("\"FLOOR NUM\"");
            columns.Add("\"PROPERTY ID\"");
            columns.Add("\"STATUS\"");

            floorLog.Log(string.Join(",", columns.ToArray()));

            repo.StartTransaction(() =>
            {
                foreach (GFloor floor in repo.Search<GFloor>())
                {
                    List<string> fields = new List<string>();
                    fields.Add(string.Format("\"{0}\"", floor.OID));
                    fields.Add(string.Format("\"{0}\"", floor.Number));
                    fields.Add(string.Format("\"{0}\"", floor.PropertyId));
                    fields.Add(string.Format("\"{0}\"", floor.UpdateStatus));
                    floorLog.Log(string.Join(",", fields.ToArray()));

                    floor.UpdateStatus = 1;
                    repo.Update(floor);
                }
            });

            Session.Current.Cadu.Save(true);
        }
    }
}

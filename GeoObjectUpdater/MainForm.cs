﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using System.Reflection;
using Geomatic.Core.Logs;
using System.Text.RegularExpressions;
using Geomatic.Core.Utilities;

namespace GeoObjectUpdater
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private SegmentName SegmentName
        {
            get
            {
                return (SegmentName)cbSegment.SelectedItem;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            cbSegment.Items.Add(SegmentName.AS);
            cbSegment.Items.Add(SegmentName.JH);
            cbSegment.Items.Add(SegmentName.JP);
            cbSegment.Items.Add(SegmentName.KG);
            cbSegment.Items.Add(SegmentName.KK);
            cbSegment.Items.Add(SegmentName.KN);
            cbSegment.Items.Add(SegmentName.KV);
            cbSegment.Items.Add(SegmentName.MK);
            cbSegment.Items.Add(SegmentName.PG);
            cbSegment.Items.Add(SegmentName.TG);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string directory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ILog log = new CsvLog(directory + "\\" + SegmentName + "_Street_Log");

            List<string> columns = new List<string>();
            columns.Add("\"Id\"");
            columns.Add("\"Navigation Status\"");
            columns.Add("\"Street Class\"");
            columns.Add("\"Category\"");
            columns.Add("\"Road Class\"");
            columns.Add("\"Type\"");
            columns.Add("\"Name\"");
            columns.Add("\"Name2\"");
            columns.Add("\"Modified User\"");
            columns.Add("\"Modified Date\"");

            log.Log(string.Join(",", columns.ToArray()));

            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Obj.Class = 6;
            query.Obj.Category = 14;
            query.AddClause(() => query.Obj.NetworkClass, "<> -1");

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            repo.StartTransaction(() =>
            {
                foreach (GStreet poi in repo.Search(query))
                {
                    List<string> fields = new List<string>();
                    fields.Add(string.Format("\"{0}\"", poi.OID));
                    fields.Add(string.Format("\"{0}\"", poi.NavigationStatusValue));
                    fields.Add(string.Format("\"{0}\"", poi.ClassValue));
                    fields.Add(string.Format("\"{0}\"", poi.CategoryValue));
                    fields.Add(string.Format("\"{0}\"", poi.NetworkClassValue));
                    fields.Add(string.Format("\"{0}\"", StringUtils.TrimSpaces(poi.TypeValue)));
                    fields.Add(string.Format("\"{0}\"", poi.Name));
                    fields.Add(string.Format("\"{0}\"", poi.Name2));
                    fields.Add(string.Format("\"{0}\"", poi.UpdatedBy));
                    fields.Add(string.Format("\"{0}\"", poi.DateUpdated));
                    log.Log(string.Join(",", fields.ToArray()));

                    poi.NetworkClass = -1;
                    poi.UpdateStatus = 4;
                    poi.UpdatedBy = "Cleanup";
                    poi.DateUpdated = DateTime.Now.ToString("yyyyMMdd");

                    repo.Update(poi);
                }
            });

            Session.Current.Cadu.Save(true);
            MessageBox.Show("Completed");
        }

        private void cbSegment_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

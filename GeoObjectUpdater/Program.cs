﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Geomatic.Core;
using ESRI.ArcGIS.esriSystem;
using System.Threading;
using Geomatic.UI.Forms.MessageBoxes;

namespace GeoObjectUpdater
{
    static class Program
    {
        private static LicenseInitializer m_AOLicenseInitializer = new LicenseInitializer();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //ESRI License Initializer generated code.
            if (!m_AOLicenseInitializer.InitializeApplication(new esriLicenseProductCode[] { esriLicenseProductCode.esriLicenseProductCodeEngineGeoDB },
            new esriLicenseExtensionCode[] { }))
            {
                MessageBox.Show(m_AOLicenseInitializer.LicenseMessage() +
                "\n\nThis application could not initialize with the correct ArcGIS license and will shutdown.",
                "ArcGIS License Failture");
                m_AOLicenseInitializer.ShutdownApplication();
                Application.Exit();
                return;
            }

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

            //ESRI License Initializer generated code.
            //Do not make any call to ArcObjects after ShutDownApplication()
            m_AOLicenseInitializer.ShutdownApplication();
            Application.Exit();
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            using (MessageBoxBuilder box = MessageBoxFactory.Create(e.Exception))
            {
                box.Show();
            }
        }
    }
}

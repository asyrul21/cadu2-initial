﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Search
{
    public enum MatchType
    {
        ExactMatch = 0,
        Contains,
        StartsWith,
        EndWith
    }
}

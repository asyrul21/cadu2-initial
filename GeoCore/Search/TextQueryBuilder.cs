﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;

namespace Geomatic.Core.Search
{
    internal class TextQueryBuilder<T> : QueryBuilder<T> where T : IGObject
    {
        public TextQueryBuilder(SegmentName segmentName, MatchType matchType, bool isCaseSensitive, Operator queryOperator)
            : base(segmentName, matchType, isCaseSensitive, queryOperator)
        {
        }

        public override IQueryFilter QueryFilter
        {
            get
            {
                IQueryFilter queryFilter = base.QueryFilter;
                IGText text = (IGText)Obj;
                List<string> whereClauses = new List<string>();
                if (!string.IsNullOrEmpty(queryFilter.WhereClause))
                {
                    whereClauses.Add(queryFilter.WhereClause);
                }
                if (text.LinkedFeatureID.HasValue)
                {
                    whereClauses.Add(string.Format("FEATUREID = {0}", text.LinkedFeatureID.Value));
                }

                switch (Operator)
                {
                    case Operator.AND:
                        queryFilter.WhereClause = string.Join(" AND ", whereClauses.ToArray());
                        break;
                    case Operator.OR:
                        queryFilter.WhereClause = string.Join(" OR ", whereClauses.ToArray());
                        break;
                    default:
                        throw new Exception("Unknown operator");
                }
                return queryFilter;
            }
        }
    }
}

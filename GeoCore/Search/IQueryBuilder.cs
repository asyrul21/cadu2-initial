﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Search
{
    internal interface IQueryBuilder
    {
        MatchType MatchType { set; get; }
        bool IsCaseSensitive { set; get; }
        Operator Operator { set; get; }
        esriSpatialRelEnum SpatialRelation { set; get; }
        IGeometry Geometry { set; get; }
        IGObject Obj { get; }
        IQueryFilter QueryFilter { get; }
        IQueryFilter SpatialFilter { get; }
        void AddClause(string propertyName, string clause);
    }
}

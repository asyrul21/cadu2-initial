﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Earthworm;

namespace Geomatic.Core.Search
{
    public class MappedProperty
    {
        public PropertyInfo PropertyInfo { private set; get; }
        public MappedField MappedField { private set; get; }
        public object Value { private set; get; }

        public Type PropertyType
        {
            get
            {
                return PropertyInfo.PropertyType;
            }
        }

        public string FieldName
        {
            get
            {
                return MappedField.FieldName;
            }
        }

        public MappedProperty(PropertyInfo propertyInfo, MappedField mappedField, object value)
        {
            this.PropertyInfo = propertyInfo;
            this.MappedField = mappedField;
            this.Value = value;
        }
    }
}

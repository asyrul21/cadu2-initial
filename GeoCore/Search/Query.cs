﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using GJoinedStreet = Geomatic.Core.Features.JoinedFeatures.GStreet;
using GJoinedJunction = Geomatic.Core.Features.JoinedFeatures.GJunction;
using GJoinedProperty = Geomatic.Core.Features.JoinedFeatures.GProperty;
using GJoinedBuilding = Geomatic.Core.Features.JoinedFeatures.GBuilding;
using GJoinedLandmark = Geomatic.Core.Features.JoinedFeatures.GLandmark;
using GJoinedPoi = Geomatic.Core.Features.JoinedFeatures.GPoi;
using GJoinedStreetRestriction = Geomatic.Core.Features.JoinedFeatures.GStreetRestriction;
using GJoinedBuildingGroup = Geomatic.Core.Features.JoinedFeatures.GBuildingGroup;
using GJoinedFloor = Geomatic.Core.Features.JoinedFeatures.GFloor;
using GJoinedMultiStorey = Geomatic.Core.Features.JoinedFeatures.GMultiStorey;
using GJoinedRegion = Geomatic.Core.Features.JoinedFeatures.GRegion;
using Geomatic.Core.Features.DeletedFeatures;

using GJoinedLandmarkAND = Geomatic.Core.Features.JoinedFeatures.GLandmarkAND;
using GJoinedPropertyAND = Geomatic.Core.Features.JoinedFeatures.GPropertyAND;
using GJoinedBuildingAND = Geomatic.Core.Features.JoinedFeatures.GBuildingAND;
using GJoinedStreetAND = Geomatic.Core.Features.JoinedFeatures.GStreetAND;
using GJoinedJunctionAND = Geomatic.Core.Features.JoinedFeatures.GJunctionAND;
using GJoinedBuildingGroupAND = Geomatic.Core.Features.JoinedFeatures.GBuildingGroupAND;

namespace Geomatic.Core.Search
{
    public sealed class Query<T> where T : IGObject
    {
        private SegmentName SegmentName { set; get; }
        private IQueryBuilder _queryBuilder;
        private Dictionary<Type, Type> _library;

        public MatchType MatchType
        {
            set { _queryBuilder.MatchType = value; }
            get { return _queryBuilder.MatchType; }
        }

        public bool IsCaseSensitive
        {
            set { _queryBuilder.IsCaseSensitive = value; }
            get { return _queryBuilder.IsCaseSensitive; }
        }

        public Operator Operator
        {
            set { _queryBuilder.Operator = value; }
            get { return _queryBuilder.Operator; }
        }

        public esriSpatialRelEnum SpatialRelation
        {
            set { _queryBuilder.SpatialRelation = value; }
            get { return _queryBuilder.SpatialRelation; }
        }

        public IGeometry Geometry
        {
            set { _queryBuilder.Geometry = value; }
            get { return _queryBuilder.Geometry; }
        }

        public T Obj
        {
            get { return (T)_queryBuilder.Obj; }
        }

        public IQueryFilter QueryFilter
        {
            get { return _queryBuilder.QueryFilter; }
        }

        public IQueryFilter SpatialFilter
        {
            get { return _queryBuilder.SpatialFilter; }
        }

        public Query()
            : this(SegmentName.None)
        {
        }

        public Query(SegmentName segmentName)
            : this(segmentName, MatchType.ExactMatch, true, Operator.AND)
        {
        }

        public Query(SegmentName segmentName, MatchType matchType)
            : this(segmentName, matchType, true, Operator.AND)
        {
        }

        public Query(SegmentName segmentName, MatchType matchType, bool isCaseSensitive)
            : this(segmentName, matchType, isCaseSensitive, Operator.AND)
        {
        }

        public Query(SegmentName segmentName, MatchType matchType, bool isCaseSensitive, Operator queryOperator)
        {
            SegmentName = segmentName;

            _library = new Dictionary<Type, Type>();
            Register<GDeletedBuilding, QueryBuilder<GDeletedBuilding>>();
            Register<GDeletedJunction, QueryBuilder<GDeletedJunction>>();
            Register<GDeletedLandmark, QueryBuilder<GDeletedLandmark>>();
            Register<GDeletedPoi, QueryBuilder<GDeletedPoi>>();
            Register<GDeletedProperty, QueryBuilder<GDeletedProperty>>();
            Register<GDeletedStreet, QueryBuilder<GDeletedStreet>>();
            Register<GStreet, QueryBuilder<GStreet>>();
            Register<GJunction, QueryBuilder<GJunction>>();
            Register<GProperty, QueryBuilder<GProperty>>();
            Register<GBuilding, QueryBuilder<GBuilding>>();
            Register<GBuildingPolygon, QueryBuilder<GBuildingPolygon>>();
            Register<GLandmark, QueryBuilder<GLandmark>>();
            Register<GPoi, QueryBuilder<GPoi>>();
            Register<GStreetRestriction, QueryBuilder<GStreetRestriction>>();
            Register<GState, QueryBuilder<GState>>();
            Register<GFloor, QueryBuilder<GFloor>>();
            Register<GBuildingGroup, QueryBuilder<GBuildingGroup>>();
            Register<GMultiStorey, QueryBuilder<GMultiStorey>>();
            Register<GLandmarkBoundary, QueryBuilder<GLandmarkBoundary>>();
            Register<GStreetType, QueryBuilder<GStreetType>>();
            Register<GJunctionType, QueryBuilder<GJunctionType>>();
            Register<GPropertyType, QueryBuilder<GPropertyType>>();
            Register<GPropertyPermission, QueryBuilder<GPropertyPermission>>();
            Register<GLocation, QueryBuilder<GLocation>>();
            Register<GSectionBoundary, QueryBuilder<GSectionBoundary>>();
            Register<GStreetText, TextQueryBuilder<GStreetText>>();
            Register<GPropertyText, TextQueryBuilder<GPropertyText>>();
            Register<GBuildingText, TextQueryBuilder<GBuildingText>>();
            Register<GLandmarkText, TextQueryBuilder<GLandmarkText>>();
            Register<GCode1, QueryBuilder<GCode1>>();
            Register<GCode2, QueryBuilder<GCode2>>();
            Register<GCode3, QueryBuilder<GCode3>>();
            Register<GSource, QueryBuilder<GSource>>();
            Register<GPhone, QueryBuilder<GPhone>>();
            Register<GPoiPhone, QueryBuilder<GPoiPhone>>();
            Register<GUser, QueryBuilder<GUser>>();
            Register<GUserGroup, QueryBuilder<GUserGroup>>();
            Register<GUserGroupFunction, QueryBuilder<GUserGroupFunction>>();
            Register<GFunction, QueryBuilder<GFunction>>();
            Register<GConstructionStatus, QueryBuilder<GConstructionStatus>>();
            Register<GStreetStatus, QueryBuilder<GStreetStatus>>();
            Register<GStreetNetworkClass, QueryBuilder<GStreetNetworkClass>>();
            Register<GStreetClass, QueryBuilder<GStreetClass>>();
            Register<GStreetCategory, QueryBuilder<GStreetCategory>>();
            Register<GStreetFilterLevel, QueryBuilder<GStreetFilterLevel>>();
            Register<GStreetTollType, QueryBuilder<GStreetTollType>>();
            Register<GStreetDivider, QueryBuilder<GStreetDivider>>();
            Register<GStreetDesign, QueryBuilder<GStreetDesign>>();
            Register<GStreetDirection, QueryBuilder<GStreetDirection>>();
            Register<GJoinedStreet, QueryBuilder<GJoinedStreet>>();
            Register<GJoinedJunction, QueryBuilder<GJoinedJunction>>();
            Register<GJoinedProperty, QueryBuilder<GJoinedProperty>>();
            Register<GJoinedBuilding, QueryBuilder<GJoinedBuilding>>();
            Register<GJoinedLandmark, QueryBuilder<GJoinedLandmark>>();
            Register<GJoinedPoi, QueryBuilder<GJoinedPoi>>();
            Register<GJoinedStreetRestriction, QueryBuilder<GJoinedStreetRestriction>>();
            Register<GJoinedBuildingGroup, QueryBuilder<GJoinedBuildingGroup>>();
            Register<GJoinedFloor, QueryBuilder<GJoinedFloor>>();
            Register<GJoinedMultiStorey, QueryBuilder<GJoinedMultiStorey>>();
            Register<GTollRoute, QueryBuilder<GTollRoute>>();
            Register<GTollPrice, QueryBuilder<GTollPrice>>();
            Register<GTollRouteType, QueryBuilder<GTollRouteType>>();
            Register<GVehicleType, QueryBuilder<GVehicleType>>();

            //added by asyrul
            Register<GRegion, QueryBuilder<GRegion>>();
            Register<GJoinedRegion, QueryBuilder<GJoinedRegion>>();
            Register<GPhoneFeature, QueryBuilder<GPhoneFeature>>();
            //
            // added by noraini
            Register<GWorkArea, QueryBuilder<GWorkArea>>();
            Register<GBuildingAND, QueryBuilder<GBuildingAND>>();
            Register<GBuildingGroupAND, QueryBuilder<GBuildingGroupAND>>();
            Register<GPropertyAND, QueryBuilder<GPropertyAND>>();
            Register<GStreetAND, QueryBuilder<GStreetAND>>();
            Register<GJunctionAND, QueryBuilder<GJunctionAND>>();
            Register<GLandmarkAND, QueryBuilder<GLandmarkAND>>();
            Register<GLandmarkBoundaryAND, QueryBuilder<GLandmarkBoundaryAND>>();

            Register<GJoinedLandmarkAND, QueryBuilder<GJoinedLandmarkAND>>();
            Register<GJoinedPropertyAND, QueryBuilder<GJoinedPropertyAND>>();
            Register<GJoinedBuildingAND, QueryBuilder<GJoinedBuildingAND>>();
            Register<GJoinedStreetAND, QueryBuilder<GJoinedStreetAND>>();
            Register<GJoinedJunctionAND, QueryBuilder<GJoinedJunctionAND>>();
            Register<GJoinedBuildingGroupAND, QueryBuilder<GJoinedBuildingGroupAND>>();

            // added by noraini - Jupem Lot
            Register<GJupem, QueryBuilder<GJupem>>();

            // added by noraini - Building Group ADM Text (Mar 2022)
            Register<GBuildingGroupText, TextQueryBuilder<GBuildingGroupText>>();

            // added by noraini - Feature AND Text (Mar 2022)
            Register<GStreetTextAND, TextQueryBuilder<GStreetTextAND>>();
            Register<GPropertyTextAND, TextQueryBuilder<GPropertyTextAND>>();
            Register<GBuildingTextAND, TextQueryBuilder<GBuildingTextAND>>();
            Register<GLandmarkTextAND, TextQueryBuilder<GLandmarkTextAND>>();
            Register<GBuildingGroupTextAND, TextQueryBuilder<GBuildingGroupTextAND>>();

            // added by noraini - Feature WEB POI (Verification module for CADU3)
            Register<GPoiWeb, QueryBuilder<GPoiWeb>>();
            // end

            _queryBuilder = CreateQuery(segmentName, matchType, isCaseSensitive, queryOperator);
        }

        private void Register<TKey, TValue>()
            where TKey : IGObject
            where TValue : IQueryBuilder
        {
            if (_library.ContainsKey(typeof(TKey)))
            {
                throw new Exception(string.Format("Duplicate key detected. {0}", typeof(TKey)));
            }
            _library.Add(typeof(TKey), typeof(TValue));
        }

        private IQueryBuilder CreateQuery(SegmentName segmentName, MatchType matchType, bool isCaseSensitive, Operator queryOperator)
        {
            if (!_library.ContainsKey(typeof(T)))
            {
                throw new Exception(string.Format("Unknown persistence object type. {0}", typeof(T)));
            }

            Type TValue;
            _library.TryGetValue(typeof(T), out TValue);

            return (IQueryBuilder)Activator.CreateInstance(TValue, segmentName, matchType, isCaseSensitive, queryOperator);
        }

        public void AddClause<TValue>(Expression<Func<TValue>> expression, string clause)
        {
            string name = ((MemberExpression)expression.Body).Member.Name;
            _queryBuilder.AddClause(name, clause);
        }
    }
}

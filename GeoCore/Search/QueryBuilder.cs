﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Search
{
    internal class QueryBuilder<T> : IQueryBuilder where T : IGObject
    {
        protected SegmentName SegmentName { set; get; }
        public MatchType MatchType { set; get; }
        public bool IsCaseSensitive { set; get; }
        public Operator Operator { set; get; }
        public esriSpatialRelEnum SpatialRelation { set; get; }
        public IGeometry Geometry { set; get; }

        private IGObject _obj;
        private Dictionary<string, string> _clauses;

        protected Dictionary<string, string> Clauses
        {
            get
            {
                if (_clauses == null)
                {
                    _clauses = new Dictionary<string, string>();
                }
                return _clauses;
            }
        }

        public IGObject Obj
        {
            get
            {
                if (_obj == null)
                {
                    _obj = CreateObject();
                }
                return _obj;
            }
        }

        public virtual IQueryFilter QueryFilter
        {
            get
            {
                IQueryFilter queryFilter = new QueryFilter();
                queryFilter.WhereClause = WhereClause;
                return queryFilter;
            }
        }

        public virtual IQueryFilter SpatialFilter
        {
            get
            {
                ISpatialFilter spatialFilter = new SpatialFilter();
                spatialFilter.SpatialRel = SpatialRelation;
                spatialFilter.Geometry = Geometry;
                return spatialFilter;
            }
        }

        public QueryBuilder(SegmentName segmentName, MatchType matchType, bool isCaseSensitive, Operator queryOperator)
        {
            SegmentName = segmentName;
            MatchType = matchType;
            IsCaseSensitive = isCaseSensitive;
            Operator = queryOperator;
        }

        protected IGObject CreateObject()
        {
            return new RepositoryFactory(SegmentName).NewObj<T>();
        }

        protected IEnumerable<MappedProperty> GetMappedProperty()
        {
            PropertyInfo[] propertyInfos = Obj.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                foreach (object attribute in propertyInfo.GetCustomAttributes(true))
                {
                    if (attribute is MappedField)
                    {
                        MappedField mappedField = (MappedField)attribute;
                        object obj = propertyInfo.GetValue(Obj, null);
                        yield return new MappedProperty(propertyInfo, mappedField, obj);
                    }
                }
            }
        }

        protected virtual string WhereClause
        {
            get
            {
                List<string> fields = new List<string>();

                foreach (MappedProperty property in GetMappedProperty())
                {
                    if (property.Value == null || ((property.FieldName == "ORIGINAL_ID" && property.Value.ToString() == "0")))
                    {
                        continue;
                    }

                    switch (MatchType)
                    {
                        case MatchType.Contains:
                            fields.Add(string.Format("{0} LIKE '%{1}%'", (IsCaseSensitive) ?
                                                                          property.FieldName :
                                                                          string.Format("UPPER({0})", property.FieldName),
                                                                          (IsCaseSensitive) ?
                                                                          property.Value.ToString() :
                                                                          property.Value.ToString().ToUpper()));
                            break;
                        case MatchType.ExactMatch:
                            fields.Add((property.PropertyType == typeof(System.String)) ?
                                        string.Format("{0} = '{1}'", (IsCaseSensitive) ?
                                        property.FieldName :
                                        string.Format("UPPER({0})", property.FieldName),
                                        (IsCaseSensitive) ?
                                        property.Value.ToString() :
                                        property.Value.ToString().ToUpper()) :
                                        string.Format("{0} = {1}", property.FieldName,
                                        property.Value.ToString()));
                            break;
                        case MatchType.StartsWith:
                            fields.Add(string.Format("{0} LIKE '{1}%'", (IsCaseSensitive) ?
                                                                         property.FieldName :
                                                                         string.Format("UPPER({0})", property.FieldName),
                                                                         (IsCaseSensitive) ?
                                                                         property.Value.ToString() :
                                                                         property.Value.ToString().ToUpper()));
                            break;
                        case MatchType.EndWith:
                            fields.Add(string.Format("{0} LIKE '%{1}'", (IsCaseSensitive) ?
                                                                         property.FieldName :
                                                                         string.Format("UPPER({0})", property.FieldName),
                                                                         (IsCaseSensitive) ?
                                                                         property.Value.ToString() :
                                                                         property.Value.ToString().ToUpper()));
                            break;
                        default:
                            throw new Exception("Invalid match type.");
                    }
                }

                foreach (KeyValuePair<string, string> pair in Clauses)
                {
                    PropertyInfo propertyInfo = Obj.GetType().GetProperty(pair.Key);
                    foreach (object attribute in propertyInfo.GetCustomAttributes(true))
                    {
                        if (attribute is MappedField)
                        {
                            MappedField mappedField = (MappedField)attribute;
                            fields.Add(string.Format("{0} {1}", mappedField.FieldName, pair.Value));
                        }
                    }
                }

                switch (Operator)
                {
                    case Operator.AND:
                        return string.Join(" AND ", fields.ToArray());
                    case Operator.OR:
                        return string.Join(" OR ", fields.ToArray());
                    default:
                        throw new Exception("Unknown operator");
                }
            }
        }

        public void AddClause(string propertyName, string clause)
        {
            Clauses.Add(propertyName, clause);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Geomatic.Core.Config
{
    public class Connections : ConfigurationElementCollection
    {
        private const string ELEMENT_NAME = "connection";

        protected override string ElementName
        {
            get
            {
                return ELEMENT_NAME;
            }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Connection();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Connection)element).Name;
        }
    }
}

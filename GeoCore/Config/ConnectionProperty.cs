﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Geomatic.Core.Config
{
    public class ConnectionProperty : ConfigurationElement
    {
        private const string NAME = "name";
        private const string VALUE = "value";

        [ConfigurationProperty(NAME, IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return base[NAME] as string;
            }
        }

        [ConfigurationProperty(VALUE, IsRequired = true)]
        public string Value
        {
            get
            {
                return base[VALUE] as string;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Geomatic.Core.Config
{
    public class Connection : ConfigurationElement
    {
        private const string ELEMENT_NAME = "connectionProperties";
        private const string NAME = "name";
        private const string TYPE = "type";

        [ConfigurationProperty(NAME, IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return base[NAME] as string;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [ConfigurationProperty(TYPE, IsRequired = true)]
        public string Type
        {
            get
            {
                return base[TYPE] as string;
            }
        }

        /// <summary>
        /// Gets the connection properties.
        /// </summary>
        /// <value>
        /// The connection properties.
        /// </value>
        [ConfigurationProperty(ELEMENT_NAME)]
        public ConnectionProperties ConnectionProperties
        {
            get
            {
                return (ConnectionProperties)base[ELEMENT_NAME];
            }
        }
    }
}

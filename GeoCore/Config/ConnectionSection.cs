﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Geomatic.Core.Config
{
    public class ConnectionSection : ConfigurationSection
    {
        private const string SECTION_NAME = "connectionConfig";
        private const string ELEMENT_NAME = "connections";

        /// <summary>
        /// Gets the section.
        /// </summary>
        /// <returns>The Connection Section</returns>
        public static ConnectionSection GetSection()
        {
            return (ConnectionSection)ConfigurationManager.GetSection(SECTION_NAME);
        }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <value>
        /// The connections.
        /// </value>
        [ConfigurationProperty(ELEMENT_NAME)]
        public Connections Connections
        {
            get
            {
                return (Connections)base[ELEMENT_NAME];
            }
        }
    }
}

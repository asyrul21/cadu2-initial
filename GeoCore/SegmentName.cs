﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    /// <summary>
    /// To indicate which table
    /// </summary>
    public enum SegmentName
    {
        AS = 0,
        JH,
        JP,
        KG,
        KK,
        KN,
        KV,
        MK,
        PG,
        TG,
        None
    }
}

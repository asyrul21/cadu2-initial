﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class MultiStoreyStatusUpdater : RowStatusUpdater
    {
        // Noraini ali - OKT 2021
        public override void Delete(IGNepsObject nepsObj)
        {
             nepsObj.UpdateStatus = Status.DELETE;
        }
        // end
    }
}

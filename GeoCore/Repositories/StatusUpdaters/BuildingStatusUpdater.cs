﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class BuildingStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            GBuilding building = (GBuilding)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(building.SegmentName);
            GDeletedBuilding deletedBuilding = repo.NewObj<GDeletedBuilding>();
            deletedBuilding.DeleteId = building.OID;
            deletedBuilding.AreaId = building.AreaId;
            deletedBuilding.UpdateStatus = Status.DELETE;
            repo.Insert(deletedBuilding);
            nepsObj.UpdateStatus = Status.DELETE;
        }

        public virtual void CreateLog(IGNepsObject nepsObj)
        {
            // copy feature ADM to LOG table before update new 
            GBuilding obj = (GBuilding)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GBuildingLog buildingLog = repo.NewObj<GBuildingLog>();
            buildingLog.CopyFrom(obj);
            repo.Insert(buildingLog, true);
        }
    }
}

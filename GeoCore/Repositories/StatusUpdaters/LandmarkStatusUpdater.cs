﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class LandmarkStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            // copy feature ADM to table delete before delete the original data
            GLandmark landmark = (GLandmark)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(landmark.SegmentName);
            GDeletedLandmark deletedLandmark = repo.NewObj<GDeletedLandmark>();
            deletedLandmark.DeleteId = landmark.OID;
            deletedLandmark.AreaId = landmark.AreaId;
            deletedLandmark.UpdateStatus = Status.DELETE;
            repo.Insert(deletedLandmark);
            nepsObj.UpdateStatus = Status.DELETE;
        }

        public virtual void CreateLog(IGNepsObject nepsObj)
        {
            // copy feature ADM to LOG table
            GLandmark obj = (GLandmark)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GLandmarkLog LandmarkLog = repo.NewObj<GLandmarkLog>();
            LandmarkLog.CopyFrom(obj);
            repo.Insert(LandmarkLog, true);
        }
    }
}

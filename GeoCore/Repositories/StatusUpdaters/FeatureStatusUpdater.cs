﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class FeatureStatusUpdater : RowStatusUpdater
    {
        public virtual void UpdateGraphic(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE_GRAPHIC;
        }

        public virtual void UpdateGraphicByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE_GRAPHIC;
            nepsObj.AndStatus = Status.UPDATE_GRAPHIC;
        }
    }
}

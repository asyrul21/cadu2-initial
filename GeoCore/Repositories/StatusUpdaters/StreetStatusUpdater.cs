﻿using System;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class StreetStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            GStreet street = (GStreet)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(street.SegmentName);
            GDeletedStreet deletedStreet = repo.NewObj<GDeletedStreet>();
            deletedStreet.DeleteId = street.OID;
            deletedStreet.AreaId = street.AreaId;
            deletedStreet.UpdateStatus = Status.DELETE;
            repo.Insert(deletedStreet);
            nepsObj.UpdateStatus = Status.DELETE;
        }


        public virtual void CreateLog(IGNepsObject nepsObj)
        {
            // copy feature ADM to LOG table
            GStreet obj = (GStreet)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GStreetLog StreetLog = repo.NewObj<GStreetLog>();
            StreetLog.CopyFrom(obj);
            repo.Insert(StreetLog, true);
        }

    }
}

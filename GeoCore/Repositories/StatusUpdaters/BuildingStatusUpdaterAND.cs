﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class BuildingStatusUpdaterAND : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            GBuildingAND building = (GBuildingAND)nepsObj;
            switch (nepsObj.UpdateStatus)
            {
                case null:
                case 0:
                case Status.UPDATE_GRAPHIC:
                case Status.UPDATE:
                    RepositoryFactory repo = new RepositoryFactory(building.SegmentName);
                    GDeletedBuilding deletedBuilding = repo.NewObj<GDeletedBuilding>();
                    deletedBuilding.DeleteId = building.OID;
                    deletedBuilding.AreaId = building.AreaId;
                    deletedBuilding.UpdateStatus = Status.DELETE;
                    repo.Insert(deletedBuilding);
                    break;
                case Status.NEW:
                case Status.DELETE:
                    // no need do anything
                    break;
                default:
                    throw new Exception("Unknown update status.");
            }
        }
    }
}

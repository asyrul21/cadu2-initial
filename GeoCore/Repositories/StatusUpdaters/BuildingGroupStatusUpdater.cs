﻿using System;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class BuildingGroupStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            GBuildingGroup building = (GBuildingGroup)nepsObj;

            RepositoryFactory repo = new RepositoryFactory(building.SegmentName);
            GDeletedBuildingGroup deletedBuilding = repo.NewObj<GDeletedBuildingGroup>();
            deletedBuilding.DeleteId = building.OID;
            deletedBuilding.AreaId = building.AreaId;
            deletedBuilding.UpdateStatus = Status.DELETE;
            repo.Insert(deletedBuilding);
            nepsObj.UpdateStatus = Status.DELETE;
        }

        public virtual void CreateLog(IGNepsObject nepsObj)
        {
            // copy feature ADM to LOG table before update new 
            GBuildingGroup obj = (GBuildingGroup)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GBuildingGroupLog buildingGrpLog = repo.NewObj<GBuildingGroupLog>();
            buildingGrpLog.CopyFrom(obj);
            repo.Insert(buildingGrpLog, true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class PropertyStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            // copy feature ADM to table delete before delete the original data
            GProperty property = (GProperty)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(property.SegmentName);
            GDeletedProperty deletedProperty = repo.NewObj<GDeletedProperty>();
            deletedProperty.DeleteId = property.OID;
            deletedProperty.AreaId = property.AreaId;
            deletedProperty.UpdateStatus = Status.DELETE;
            repo.Insert(deletedProperty);
            nepsObj.UpdateStatus = Status.DELETE;
        }

        public virtual void CreateLog(IGObject Obj)
        {
            // copy feature ADM to LOG table 
            GProperty obj = (GProperty)Obj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GPropertyLog propLog = repo.NewObj<GPropertyLog>();
            propLog.CopyFrom(obj);
            repo.Insert(propLog, true);
        }

    }
}

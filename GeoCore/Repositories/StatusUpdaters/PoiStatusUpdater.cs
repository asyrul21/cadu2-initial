﻿using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class PoiStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.DELETE;
        }

        public virtual void CreateLog(IGNepsObject nepsObj)
        {
            // copy feature ADM to LOG table before delete
            GPoi obj = (GPoi)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(obj.SegmentName);
            GPoiLog PoiLog = repo.NewObj<GPoiLog>();
            PoiLog.CopyFrom(obj);
            repo.Insert(PoiLog, true);
        }
    }
}

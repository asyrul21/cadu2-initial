﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class JunctionStatusUpdater : FeatureStatusUpdater
    {
        public override void Delete(IGNepsObject nepsObj)
        {
            GJunction junction = (GJunction)nepsObj;
            RepositoryFactory repo = new RepositoryFactory(junction.SegmentName);
            GDeletedJunction deletedJunction = repo.NewObj<GDeletedJunction>();
            deletedJunction.DeleteId = junction.OID;
            deletedJunction.AreaId = junction.AreaId;
            deletedJunction.UpdateStatus = Status.DELETE;
            repo.Insert(deletedJunction);
        }
    }
}

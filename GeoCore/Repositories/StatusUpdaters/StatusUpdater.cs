﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.StatusUpdaters
{
    public class StatusUpdater
    {
        public virtual void Insert(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.NEW;
        }

        public virtual void Update(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE;
        }

        // noraini - Apr 2021 - to update AND_STATUS for feature ADM
        public virtual void InsertByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.NEW;
            nepsObj.AndStatus = Status.NEW;
        }

        public virtual void UpdateByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE;
            nepsObj.AndStatus = Status.UPDATE;
        }

        public virtual void UpdateDeleteByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.DELETE;
            nepsObj.AndStatus = Status.DELETE;
        }

        public virtual void Delete(IGNepsObject nepsObj)
        {
            throw new Exception("Not implemented.");
        }
    }
}

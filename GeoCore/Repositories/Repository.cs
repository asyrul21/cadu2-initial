﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Repositories
{
    /// <summary>
    /// The parent class of Repository, implementing IRepository interface
    /// </summary>
    internal abstract class Repository : IRepository
    {
        /// <summary>
        /// Gets or sets the name of the segment.
        /// </summary>
        /// <value>
        /// The name of the segment.
        /// </value>
        protected SegmentName SegmentName { private set; get; }

        /// <summary>
        /// Gets the name of the database table.
        /// </summary>
        /// <value>
        /// The name of the database table.
        /// </value>
        protected abstract string TableName { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="segmentName">Name of the segment.</param>
        public Repository(SegmentName segmentName)
        {
            SegmentName = segmentName;
        }

        /// <summary>
        /// Counts the specified query filter result.
        /// </summary>
        /// <param name="queryFilter">The query filter.</param>
        /// <returns></returns>
        public abstract int Count(IQueryFilter queryFilter);

        /// <summary>
        /// Search a <see cref="GObject"/> by OID.
        /// </summary>
        /// <param name="id">The OID.</param>
        /// <returns></returns>
        public abstract IGObject GetById(int id);

        /// <summary>
        /// Search by ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public abstract IEnumerable<IGObject> GetByIds(IEnumerable<int> ids);

        /// <summary>
        /// Searches the specified query filter.
        /// </summary>
        /// <param name="queryFilter">The query filter.</param>
        /// <returns></returns>
        public abstract IEnumerable<IGObject> Search(IQueryFilter queryFilter);

        /// <summary>
        /// Inserts the object into the database.
        /// </summary>
        /// <param name="obj">The object to be inserted.</param>
        /// <returns></returns>
        public abstract IGObject Insert(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void Update(IGObject obj);

        /// <summary>
        /// Deletes the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void Delete(IGObject obj);

        /// <summary>
        /// Initialize the GObject as an empty object
        /// </summary>
        /// <returns>
        /// The new object
        /// </returns>
        public abstract IGObject NewObj();

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateRemainStatus(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateDeleteByAND(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateByAND(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateGraphicByAND(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateGraphic(IGObject obj);

        /// <summary>
        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateInsertByAND(IGObject obj);

        /// Updates the specified object into the database.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        public abstract void UpdateRemainStatusByAND(IGObject obj);
    }
}

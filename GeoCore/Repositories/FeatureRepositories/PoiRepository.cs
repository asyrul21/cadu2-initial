﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Pois;
using Geomatic.Core.Repositories.StatusUpdaters;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class PoiRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPoi.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPoi.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPoi.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPoi.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPoi.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPoi.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPoi.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPoi.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPoi.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPoi.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PoiRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsPoi>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhPoi>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpPoi>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgPoi>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkPoi>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnPoi>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvPoi>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkPoi>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgPoi>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgPoi>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPoi>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPoi>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPoi>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPoi>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPoi>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPoi>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPoi>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPoi>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPoi>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPoi>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPoi>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPoi>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPoi>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPoi>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPoi>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPoi>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPoi>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPoi>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPoi>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPoi>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GPoi poi = (GPoi)obj;

            new PoiStatusUpdater().Insert(poi);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        poi = ((GAsPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        poi = ((GJhPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        poi = ((GJpPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        poi = ((GKgPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        poi = ((GKkPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        poi = ((GKnPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        poi = ((GKvPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        poi = ((GMkPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        poi = ((GPgPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        poi = ((GTgPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            return poi;
        }

        public override void Update(IGObject obj)
        {
            GPoi absObj = (GPoi)obj;

            new PoiStatusUpdater().UpdateGraphic(absObj);
            new PoiStatusUpdater().CreateLog(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoi poi = (GAsPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPoi poi = (GJhPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPoi poi = (GJpPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPoi poi = (GKgPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPoi poi = (GKkPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPoi poi = (GKnPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPoi poi = (GKvPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPoi poi = (GMkPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPoi poi = (GPgPoi)obj;
                        poi.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPoi poi = (GTgPoi)obj;
                        poi.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GPoi absObj = (GPoi)obj;

            // removed by asyrul
            if (!absObj.CanDelete())
            {
                throw new QualityControlException("Unable to delete poi");
            }
            //removed end

            new PoiStatusUpdater().Delete(absObj);
            new PoiStatusUpdater().CreateLog(absObj);  

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoi poi = (GAsPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPoi poi = (GJhPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPoi poi = (GJpPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPoi poi = (GKgPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPoi poi = (GKkPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPoi poi = (GKnPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPoi poi = (GKvPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPoi poi = (GMkPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPoi poi = (GPgPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPoi poi = (GTgPoi)obj;
                        poi.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPoi>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPoi>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPoi>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPoi>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPoi>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPoi>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPoi>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPoi>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPoi>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPoi>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Repositories.StatusUpdaters;
using Geomatic.Core.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class BuildingRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuilding>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuilding>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuilding>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuilding>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuilding>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuilding>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuilding>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuilding>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuilding>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuilding>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuilding>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuilding>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuilding>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuilding>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuilding>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuilding>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuilding>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuilding>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuilding>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuilding>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuilding>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuilding>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuilding>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuilding>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuilding>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuilding>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuilding>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuilding>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuilding>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuilding>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GBuilding building = (GBuilding)obj;

            // noraini - Apr 2021 - Set value STATUS & AND_STATUS - Intergration with NEPS
            if (Sessions.Session.User.GetGroup().Name != "AND")
            {
                new BuildingStatusUpdater().Insert(building);
            }
            // end

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        building = ((GAsBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        building = ((GJhBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        building = ((GJpBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        building = ((GKgBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        building = ((GKkBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        building = ((GKnBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        building = ((GKvBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        building = ((GMkBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        building = ((GPgBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        building = ((GTgBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            building.UpdateText();
            return building;
        }

        public override void Update(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;

            new FeatureStatusUpdater().Update(absObj);
            //new BuildingStatusUpdater().CreateLog(absObj); // Jan 23 - comment LOGGING only require for POI

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuilding building = (GAsBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuilding building = (GJhBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuilding building = (GJpBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuilding building = (GKgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuilding building = (GKkBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuilding building = (GKnBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuilding building = (GKvBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuilding building = (GMkBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuilding building = (GPgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuilding building = (GTgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;

            if (!absObj.CanDelete())
            {
                throw new QualityControlException("Unable to delete building 1");
            }

            new BuildingStatusUpdater().Delete(absObj);
            //new BuildingStatusUpdater().CreateLog(absObj); // Jan 23 - comment LOGGING only require for POI

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuilding building = (GAsBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuilding building = (GJhBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuilding building = (GJpBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuilding building = (GKgBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuilding building = (GKkBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuilding building = (GKnBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuilding building = (GKvBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuilding building = (GMkBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuilding building = (GPgBuilding)obj;
                        building.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuilding building = (GTgBuilding)obj;
                        building.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
        }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuilding>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuilding>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuilding>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuilding>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuilding>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuilding>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuilding>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuilding>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuilding>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuilding>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        public override void UpdateGraphic(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            new BuildingStatusUpdater().UpdateGraphic(absObj);
            //new BuildingStatusUpdater().CreateLog(absObj); // Jan 23 - comment LOGGING only require for POI
            UpdateDataSegment(obj);
        }

        //  insert new data with  STATUS & AND_STATUS - new status
        public override void UpdateInsertByAND(IGObject obj)
        {
            GBuilding building = (GBuilding)obj;
            new FeatureStatusUpdater().InsertByAND(building);
            UpdateDataSegment(obj);
        }

        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            //new BuildingStatusUpdater().CreateLog(absObj); // Jan 23 - comment LOGGING only require for POI
            UpdateDataSegment(obj);
        }

        // update data without create logging
        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - update status
        public override void UpdateByAND(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            new FeatureStatusUpdater().UpdateByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - delete status
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            new FeatureStatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - update graphic status
        public override void UpdateGraphicByAND(IGObject obj)
        {
            GBuilding absObj = (GBuilding)obj;
            new FeatureStatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        // to update data perSegment
        private void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuilding building = (GAsBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuilding building = (GJhBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuilding building = (GJpBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuilding building = (GKgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuilding building = (GKkBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuilding building = (GKnBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuilding building = (GKvBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuilding building = (GMkBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuilding building = (GPgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuilding building = (GTgBuilding)obj;
                        building.Update();
                        building.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

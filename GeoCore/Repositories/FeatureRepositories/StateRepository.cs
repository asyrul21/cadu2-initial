﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class StateRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get { return GState.TABLE_NAME; }
        }

        public StateRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return FeatureClass.FindItemByOID<GState>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return FeatureClass.Map<GState>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return FeatureClass.Map<GState>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GState state = (GState)obj;
            return state.InsertInto(FeatureClass);
        }

        public override void Update(IGObject obj)
        {
            GState state = (GState)obj;
            state.Update();
        }

        public override void Delete(IGObject obj)
        {
            GState state = (GState)obj;
            state.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GState>();
        }
    }
}

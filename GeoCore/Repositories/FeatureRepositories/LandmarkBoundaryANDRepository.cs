﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.LandmarkBoundariesAND;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class LandmarkBoundaryANDRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmarkBoundary.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmarkBoundary.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmarkBoundary.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmarkBoundary.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmarkBoundary.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmarkBoundary.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmarkBoundary.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmarkBoundary.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmarkBoundary.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmarkBoundary.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public LandmarkBoundaryANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsLandmarkBoundary>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhLandmarkBoundary>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpLandmarkBoundary>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgLandmarkBoundary>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkLandmarkBoundary>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnLandmarkBoundary>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvLandmarkBoundary>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkLandmarkBoundary>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgLandmarkBoundary>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgLandmarkBoundary>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmarkBoundary>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmarkBoundary>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmarkBoundary>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmarkBoundary>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmarkBoundary>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmarkBoundary>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmarkBoundary>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmarkBoundary>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmarkBoundary>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmarkBoundary>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmarkBoundary>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmarkBoundary>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmarkBoundary>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmarkBoundary>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmarkBoundary>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmarkBoundary>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmarkBoundary>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmarkBoundary>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmarkBoundary>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmarkBoundary>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkBoundary landmarkBoundary = (GAsLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkBoundary landmarkBoundary = (GJhLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkBoundary landmarkBoundary = (GJpLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkBoundary landmarkBoundary = (GKgLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkBoundary landmarkBoundary = (GKkLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkBoundary landmarkBoundary = (GKnLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkBoundary landmarkBoundary = (GKvLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkBoundary landmarkBoundary = (GMkLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkBoundary landmarkBoundary = (GPgLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkBoundary landmarkBoundary = (GTgLandmarkBoundary)obj;
                        return landmarkBoundary.InsertInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkBoundary landmarkBoundary = (GAsLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkBoundary landmarkBoundary = (GJhLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkBoundary landmarkBoundary = (GJpLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkBoundary landmarkBoundary = (GKgLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkBoundary landmarkBoundary = (GKkLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkBoundary landmarkBoundary = (GKnLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkBoundary landmarkBoundary = (GKvLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkBoundary landmarkBoundary = (GMkLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkBoundary landmarkBoundary = (GPgLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkBoundary landmarkBoundary = (GTgLandmarkBoundary)obj;
                        landmarkBoundary.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkBoundary landmarkBoundary = (GAsLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkBoundary landmarkBoundary = (GJhLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkBoundary landmarkBoundary = (GJpLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkBoundary landmarkBoundary = (GKgLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkBoundary landmarkBoundary = (GKkLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkBoundary landmarkBoundary = (GKnLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkBoundary landmarkBoundary = (GKvLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkBoundary landmarkBoundary = (GMkLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkBoundary landmarkBoundary = (GPgLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkBoundary landmarkBoundary = (GTgLandmarkBoundary)obj;
                        landmarkBoundary.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLandmarkBoundary>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLandmarkBoundary>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLandmarkBoundary>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLandmarkBoundary>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLandmarkBoundary>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLandmarkBoundary>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLandmarkBoundary>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLandmarkBoundary>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLandmarkBoundary>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLandmarkBoundary>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

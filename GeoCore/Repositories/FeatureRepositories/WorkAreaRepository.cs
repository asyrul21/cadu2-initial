﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using System;
using System.Collections.Generic;


namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class WorkAreaRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get { return GWorkArea.TABLE_NAME; }
        }

        public WorkAreaRepository()
            : this(SegmentName.None)        {
        }

        public WorkAreaRepository(SegmentName segmentName) : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return FeatureClass.FindItemByOID<GWorkArea>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return FeatureClass.Map<GWorkArea>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return FeatureClass.Map<GWorkArea>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GWorkArea workArea = (GWorkArea)obj;
            return workArea.InsertInto(FeatureClass);
        }

        public override void Update(IGObject obj)
        {
            GWorkArea workArea = (GWorkArea)obj;
            workArea.Update();
        }

        public override void Delete(IGObject obj)
        {
            GWorkArea workArea = (GWorkArea)obj;
            workArea.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GWorkArea>();
        }

        public override void UpdateGraphic(IGObject obj)
        {
            GWorkArea workArea = (GWorkArea)obj;
            workArea.Update();
        }
    }
}

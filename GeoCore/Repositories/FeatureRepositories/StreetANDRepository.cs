﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.StreetsAND;
using Geomatic.Core.Repositories.StatusUpdatersAND;
using Geomatic.Core.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class StreetANDRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public StreetANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsStreet>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhStreet>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpStreet>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgStreet>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkStreet>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnStreet>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvStreet>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkStreet>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgStreet>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgStreet>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreet>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreet>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreet>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreet>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreet>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreet>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreet>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreet>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreet>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreet>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreet>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreet>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreet>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreet>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreet>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreet>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreet>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreet>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreet>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreet>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetAND street = (GStreetAND)obj;

            new StatusUpdater().Insert(street);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        street = ((GAsStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        street = ((GJhStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        street = ((GJpStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        street = ((GKgStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        street = ((GKkStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        street = ((GKnStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        street = ((GKvStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        street = ((GMkStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        street = ((GPgStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        street = ((GTgStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            street.UpdateTextAND();
            return street;
        }

        public override void Update(IGObject obj)
        {
            GStreetAND absObj = (GStreetAND)obj;

            new StatusUpdater().Update(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreet street = (GAsStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreet street = (GJhStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreet street = (GJpStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreet street = (GKgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreet street = (GKkStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreet street = (GKnStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreet street = (GKvStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreet street = (GMkStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreet street = (GPgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreet street = (GTgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
                {
                    case SegmentName.AS:
                        {
                            GAsStreet street = (GAsStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.JH:
                        {
                            GJhStreet street = (GJhStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.JP:
                        {
                            GJpStreet street = (GJpStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.KG:
                        {
                            GKgStreet street = (GKgStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.KK:
                        {
                            GKkStreet street = (GKkStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.KN:
                        {
                            GKnStreet street = (GKnStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.KV:
                        {
                            GKvStreet street = (GKvStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.MK:
                        {
                            GMkStreet street = (GMkStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.PG:
                        {
                            GPgStreet street = (GPgStreet)obj;
                            street.Delete();
                            break;
                        }
                    case SegmentName.TG:
                        {
                            GTgStreet street = (GTgStreet)obj;
                            street.Delete();
                            break;
                        }
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreet>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreet>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreet>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreet>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreet>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreet>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreet>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreet>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreet>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreet>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GStreetAND absObj = (GStreetAND)obj;
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to delete
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GStreetAND absObj = (GStreetAND)obj;
            new StatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to update graphic value
        public override void UpdateGraphic(IGObject obj)
        {
            GStreetAND absObj = (GStreetAND)obj;
            new StatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        public void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreet street = (GAsStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreet street = (GJhStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreet street = (GJpStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreet street = (GKgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreet street = (GKkStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreet street = (GKnStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreet street = (GKvStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreet street = (GMkStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreet street = (GPgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreet street = (GTgStreet)obj;
                        street.Update();
                        street.UpdateTextAND();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.BuildingGroupsAND;
using Geomatic.Core.Repositories.StatusUpdatersAND;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class BuildingGroupANDRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingGroup.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingGroup.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingGroup.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingGroup.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingGroup.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingGroup.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingGroup.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingGroup.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingGroup.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingGroup.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingGroupANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuildingGroup>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuildingGroup>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuildingGroup>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuildingGroup>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuildingGroup>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuildingGroup>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuildingGroup>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuildingGroup>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuildingGroup>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuildingGroup>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingGroup>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingGroup>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingGroup>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingGroup>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingGroup>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingGroup>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingGroup>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingGroup>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingGroup>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingGroup>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingGroup>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingGroup>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingGroup>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingGroup>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingGroup>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingGroup>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingGroup>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingGroup>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingGroup>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingGroup>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GBuildingGroupAND buildingGrp = (GBuildingGroupAND)obj;

            new StatusUpdater().Insert(buildingGrp);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        //GAsBuildingGroup buildingGroup = (GAsBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GAsBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        //GJhBuildingGroup buildingGroup = (GJhBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GJhBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        //GJpBuildingGroup buildingGroup = (GJpBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GJpBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        //GKgBuildingGroup buildingGroup = (GKgBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GKgBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        //GKkBuildingGroup buildingGroup = (GKkBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GKkBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        //GKnBuildingGroup buildingGroup = (GKnBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GKnBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        //GKvBuildingGroup buildingGroup = (GKvBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GKvBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        //GMkBuildingGroup buildingGroup = (GMkBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GMkBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        //GPgBuildingGroup buildingGroup = (GPgBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GPgBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        //GTgBuildingGroup buildingGroup = (GTgBuildingGroup)obj;
                        //return buildingGroup.InsertInto(FeatureClass);
                        buildingGrp = ((GTgBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            buildingGrp.UpdateTextAND();
            return buildingGrp;
        }

        public override void Update(IGObject obj)
        {
            GBuildingGroupAND buildingGrp = (GBuildingGroupAND)obj;
            new StatusUpdater().Update(buildingGrp);
            UpdateDataSegment(obj);
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
                {
                    case SegmentName.AS:
                        {
                            GAsBuildingGroup buildingGroup = (GAsBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.JH:
                        {
                            GJhBuildingGroup buildingGroup = (GJhBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.JP:
                        {
                            GJpBuildingGroup buildingGroup = (GJpBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.KG:
                        {
                            GKgBuildingGroup buildingGroup = (GKgBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.KK:
                        {
                            GKkBuildingGroup buildingGroup = (GKkBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.KN:
                        {
                            GKnBuildingGroup buildingGroup = (GKnBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.KV:
                        {
                            GKvBuildingGroup buildingGroup = (GKvBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.MK:
                        {
                            GMkBuildingGroup buildingGroup = (GMkBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.PG:
                        {
                            GPgBuildingGroup buildingGroup = (GPgBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    case SegmentName.TG:
                        {
                            GTgBuildingGroup buildingGroup = (GTgBuildingGroup)obj;
                            buildingGroup.Delete();
                            break;
                        }
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingGroup>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingGroup>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingGroup>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingGroup>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingGroup>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingGroup>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingGroup>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingGroup>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingGroup>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingGroup>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GBuildingGroupAND absObj = (GBuildingGroupAND)obj;
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to delete
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GBuildingGroupAND absObj = (GBuildingGroupAND)obj;
            new StatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to update graphic value
        public override void UpdateGraphic(IGObject obj)
        {
            GBuildingGroupAND absObj = (GBuildingGroupAND)obj;
            new StatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        // to update data perSegment
        private void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroup buildingGroup = (GAsBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroup buildingGroup = (GJhBuildingGroup)obj;

                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroup buildingGroup = (GJpBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroup buildingGroup = (GKgBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroup buildingGroup = (GKkBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroup buildingGroup = (GKnBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroup buildingGroup = (GKvBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroup buildingGroup = (GMkBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroup buildingGroup = (GPgBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroup buildingGroup = (GTgBuildingGroup)obj;
                        buildingGroup.Update();
                        buildingGroup.UpdateTextAND();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Repositories.StatusUpdaters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class PropertyRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PropertyRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsProperty>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhProperty>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpProperty>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgProperty>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkProperty>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnProperty>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvProperty>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkProperty>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgProperty>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgProperty>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsProperty>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhProperty>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpProperty>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgProperty>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkProperty>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnProperty>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvProperty>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkProperty>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgProperty>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgProperty>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsProperty>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhProperty>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpProperty>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgProperty>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkProperty>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnProperty>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvProperty>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkProperty>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgProperty>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgProperty>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GProperty property = (GProperty)obj;

            new PropertyStatusUpdater().Insert(property);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        property = ((GAsProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        property = ((GJhProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        property = ((GJpProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        property = ((GKgProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        property = ((GKkProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        property = ((GKnProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        property = ((GKvProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        property = ((GMkProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        property = ((GPgProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        property = ((GTgProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            property.UpdateText();
            return property;
        }

        public override void Update(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;

            new PropertyStatusUpdater().Update(absObj);
            //new PropertyStatusUpdater().CreateLog(absObj); // Jan 23 - comment LOGGING only require for POI

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsProperty property = (GAsProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhProperty property = (GJhProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpProperty property = (GJpProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgProperty property = (GKgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkProperty property = (GKkProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnProperty property = (GKnProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvProperty property = (GKvProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkProperty property = (GMkProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgProperty property = (GPgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgProperty property = (GTgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;

            if (!absObj.CanDelete())
            {
                throw new QualityControlException("Unable to delete property");
            }

            new PropertyStatusUpdater().Delete(absObj);
            //new PropertyStatusUpdater().CreateLog(absObj);  // Jan 23 - comment LOGGING only require for POI

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsProperty street = (GAsProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhProperty street = (GJhProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpProperty street = (GJpProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgProperty street = (GKgProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkProperty street = (GKkProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnProperty street = (GKnProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvProperty street = (GKvProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkProperty street = (GMkProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgProperty street = (GPgProperty)obj;
                        street.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgProperty street = (GTgProperty)obj;
                        street.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsProperty>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhProperty>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpProperty>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgProperty>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkProperty>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnProperty>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvProperty>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkProperty>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgProperty>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgProperty>();
                default:
                    throw new Exception(string.Format("Unknown version {0}.", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        public override void UpdateGraphic(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            new PropertyStatusUpdater().UpdateGraphic(absObj);
            //new PropertyStatusUpdater().CreateLog(absObj);  // Jan 23 - comment LOGGING only require for POI

            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - insert status
        public override void UpdateInsertByAND(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            new FeatureStatusUpdater().InsertByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            //new PropertyStatusUpdater().CreateLog(absObj);  // Jan 23 - comment LOGGING only require for POI
            UpdateDataSegment(obj); 
        }

        // update STATUS & AND_STATUS - update status
        public override void UpdateByAND(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            new FeatureStatusUpdater().UpdateByAND(absObj);
            UpdateDataSegment(obj);          
        }

        // update STATUS & AND_STATUS - delete status
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            new FeatureStatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - update graphic status
        public override void UpdateGraphicByAND(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            new FeatureStatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update data with not create logging 
        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            GProperty absObj = (GProperty)obj;
            UpdateDataSegment(obj);
        }

        // to update data perSegment
        private void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsProperty property = (GAsProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhProperty property = (GJhProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpProperty property = (GJpProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgProperty property = (GKgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkProperty property = (GKkProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnProperty property = (GKnProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvProperty property = (GKvProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkProperty property = (GMkProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgProperty property = (GPgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgProperty property = (GTgProperty)obj;
                        property.Update();
                        property.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

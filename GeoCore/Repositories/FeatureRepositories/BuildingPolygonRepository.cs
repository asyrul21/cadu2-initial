﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.BuildingPolygons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class BuildingPolygonRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingPolygon.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingPolygon.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingPolygon.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingPolygon.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingPolygon.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingPolygon.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingPolygon.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingPolygon.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingPolygon.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingPolygon.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingPolygonRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuildingPolygon>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuildingPolygon>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuildingPolygon>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuildingPolygon>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuildingPolygon>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuildingPolygon>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuildingPolygon>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuildingPolygon>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuildingPolygon>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuildingPolygon>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingPolygon>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingPolygon>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingPolygon>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingPolygon>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingPolygon>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingPolygon>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingPolygon>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingPolygon>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingPolygon>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingPolygon>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingPolygon>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingPolygon>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingPolygon>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingPolygon>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingPolygon>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingPolygon>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingPolygon>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingPolygon>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingPolygon>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingPolygon>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GBuildingPolygon buildingPolygon = (GBuildingPolygon)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        buildingPolygon = ((GAsBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        buildingPolygon = ((GJhBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        buildingPolygon = ((GJpBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        buildingPolygon = ((GKgBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        buildingPolygon = ((GKkBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        buildingPolygon = ((GKnBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        buildingPolygon = ((GKvBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        buildingPolygon = ((GMkBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        buildingPolygon = ((GPgBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        buildingPolygon = ((GTgBuildingPolygon)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            return buildingPolygon;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingPolygon buildingPolygon = (GAsBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingPolygon buildingPolygon = (GJhBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingPolygon buildingPolygon = (GJpBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingPolygon buildingPolygon = (GKgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingPolygon buildingPolygon = (GKkBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingPolygon buildingPolygon = (GKnBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingPolygon buildingPolygon = (GKvBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingPolygon buildingPolygon = (GMkBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingPolygon buildingPolygon = (GPgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingPolygon buildingPolygon = (GTgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingPolygon buildingPolygon = (GAsBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingPolygon buildingPolygon = (GJhBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingPolygon buildingPolygon = (GJpBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingPolygon buildingPolygon = (GKgBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingPolygon buildingPolygon = (GKkBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingPolygon buildingPolygon = (GKnBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingPolygon buildingPolygon = (GKvBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingPolygon buildingPolygon = (GMkBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingPolygon buildingPolygon = (GPgBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingPolygon buildingPolygon = (GTgBuildingPolygon)obj;
                        buildingPolygon.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingPolygon>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingPolygon>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingPolygon>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingPolygon>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingPolygon>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingPolygon>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingPolygon>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingPolygon>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingPolygon>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingPolygon>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void UpdateGraphic(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingPolygon buildingPolygon = (GAsBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingPolygon buildingPolygon = (GJhBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingPolygon buildingPolygon = (GJpBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingPolygon buildingPolygon = (GKgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingPolygon buildingPolygon = (GKkBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingPolygon buildingPolygon = (GKnBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingPolygon buildingPolygon = (GKvBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingPolygon buildingPolygon = (GMkBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingPolygon buildingPolygon = (GPgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingPolygon buildingPolygon = (GTgBuildingPolygon)obj;
                        buildingPolygon.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

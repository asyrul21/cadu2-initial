﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.SectionBoundaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class SectionBoundaryRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsSectionBoundary.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhSectionBoundary.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpSectionBoundary.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgSectionBoundary.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkSectionBoundary.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnSectionBoundary.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvSectionBoundary.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkSectionBoundary.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgSectionBoundary.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgSectionBoundary.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public SectionBoundaryRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsSectionBoundary>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhSectionBoundary>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpSectionBoundary>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgSectionBoundary>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkSectionBoundary>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnSectionBoundary>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvSectionBoundary>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkSectionBoundary>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgSectionBoundary>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgSectionBoundary>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsSectionBoundary>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhSectionBoundary>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpSectionBoundary>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgSectionBoundary>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkSectionBoundary>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnSectionBoundary>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvSectionBoundary>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkSectionBoundary>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgSectionBoundary>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgSectionBoundary>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsSectionBoundary>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhSectionBoundary>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpSectionBoundary>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgSectionBoundary>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkSectionBoundary>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnSectionBoundary>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvSectionBoundary>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkSectionBoundary>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgSectionBoundary>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgSectionBoundary>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GSectionBoundary sectionBoundary;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        sectionBoundary = ((GAsSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        sectionBoundary = ((GJhSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        sectionBoundary = ((GJpSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        sectionBoundary = ((GKgSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        sectionBoundary = ((GKkSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        sectionBoundary = ((GKnSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        sectionBoundary = ((GKvSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        sectionBoundary = ((GMkSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        sectionBoundary = ((GPgSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        sectionBoundary = ((GTgSectionBoundary)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return sectionBoundary;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsSectionBoundary sectionBoundary = (GAsSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhSectionBoundary sectionBoundary = (GJhSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpSectionBoundary sectionBoundary = (GJpSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgSectionBoundary sectionBoundary = (GKgSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkSectionBoundary sectionBoundary = (GKkSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnSectionBoundary sectionBoundary = (GKnSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvSectionBoundary sectionBoundary = (GKvSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkSectionBoundary sectionBoundary = (GMkSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgSectionBoundary sectionBoundary = (GPgSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgSectionBoundary sectionBoundary = (GTgSectionBoundary)obj;
                        sectionBoundary.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GSectionBoundary absObj = (GSectionBoundary)obj;

            if (!absObj.CanDelete())
            {
                throw new QualityControlException("Unable to delete section boundary");
            }

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsSectionBoundary sectionBoundary = (GAsSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhSectionBoundary sectionBoundary = (GJhSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpSectionBoundary sectionBoundary = (GJpSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgSectionBoundary sectionBoundary = (GKgSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkSectionBoundary sectionBoundary = (GKkSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnSectionBoundary sectionBoundary = (GKnSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvSectionBoundary sectionBoundary = (GKvSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkSectionBoundary sectionBoundary = (GMkSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgSectionBoundary sectionBoundary = (GPgSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgSectionBoundary sectionBoundary = (GTgSectionBoundary)obj;
                        sectionBoundary.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsSectionBoundary>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhSectionBoundary>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpSectionBoundary>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgSectionBoundary>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkSectionBoundary>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnSectionBoundary>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvSectionBoundary>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkSectionBoundary>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgSectionBoundary>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgSectionBoundary>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

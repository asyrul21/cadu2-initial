﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Junctions;
using Geomatic.Core.Repositories.StatusUpdaters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class JunctionRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsJunction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhJunction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpJunction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgJunction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkJunction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnJunction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvJunction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkJunction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgJunction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgJunction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", SegmentName));
                }
            }
        }

        public JunctionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsJunction>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhJunction>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpJunction>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgJunction>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkJunction>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnJunction>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvJunction>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkJunction>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgJunction>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgJunction>(id);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJunction>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJunction>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJunction>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJunction>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJunction>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJunction>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJunction>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJunction>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJunction>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJunction>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJunction>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJunction>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJunction>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJunction>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJunction>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJunction>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJunction>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJunction>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJunction>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJunction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GJunction junction = (GJunction)obj;

            new FeatureStatusUpdater().Insert(junction);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        junction = ((GAsJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        junction = ((GJhJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        junction = ((GJpJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        junction = ((GKgJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        junction = ((GKkJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        junction = ((GKnJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        junction = ((GKvJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        junction = ((GMkJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        junction = ((GPgJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        junction = ((GTgJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            return junction;
        }

        public override void Update(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;

            new FeatureStatusUpdater().Update(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsJunction junction = (GAsJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhJunction junction = (GJhJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpJunction junction = (GJpJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgJunction junction = (GKgJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkJunction junction = (GKkJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnJunction junction = (GKnJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvJunction junction = (GKvJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkJunction junction = (GMkJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgJunction junction = (GPgJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgJunction junction = (GTgJunction)obj;
                        junction.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;

            if (!absObj.CanDelete())
            {
                throw new QualityControlException("Unable to delete junction");
            }

            new JunctionStatusUpdater().Delete(absObj);

            switch (SegmentName)
                {
                    case SegmentName.AS:
                        {
                            GAsJunction junction = (GAsJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.JH:
                        {
                            GJhJunction junction = (GJhJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.JP:
                        {
                            GJpJunction junction = (GJpJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.KG:
                        {
                            GKgJunction junction = (GKgJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.KK:
                        {
                            GKkJunction junction = (GKkJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.KN:
                        {
                            GKnJunction junction = (GKnJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.KV:
                        {
                            GKvJunction junction = (GKvJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.MK:
                        {
                            GMkJunction junction = (GMkJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.PG:
                        {
                            GPgJunction junction = (GPgJunction)obj;
                            junction.Delete();
                            break;
                        }
                    case SegmentName.TG:
                        {
                            GTgJunction junction = (GTgJunction)obj;
                            junction.Delete();
                            break;
                        }
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsJunction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhJunction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpJunction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgJunction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkJunction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnJunction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvJunction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkJunction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgJunction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgJunction>();
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        public override void UpdateGraphic(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            new JunctionStatusUpdater().UpdateGraphic(absObj);
            UpdateDataSegment(obj);
        }

        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            UpdateDataSegment(obj);
        }

        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            UpdateDataSegment(obj);
        }

        //  insert new data with  STATUS & AND_STATUS - new status
        public override void UpdateInsertByAND(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            new FeatureStatusUpdater().InsertByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - update status
        public override void UpdateByAND(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            new FeatureStatusUpdater().UpdateByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - delete status
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            new FeatureStatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS - update graphic status
        public override void UpdateGraphicByAND(IGObject obj)
        {
            GJunction absObj = (GJunction)obj;
            new FeatureStatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        // to update data perSegment
        private void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsJunction junction = (GAsJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhJunction junction = (GJhJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpJunction junction = (GJpJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgJunction junction = (GKgJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkJunction junction = (GKkJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnJunction junction = (GKnJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvJunction junction = (GKvJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkJunction junction = (GMkJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgJunction junction = (GPgJunction)obj;
                        junction.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgJunction junction = (GTgJunction)obj;
                        junction.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

    }
}

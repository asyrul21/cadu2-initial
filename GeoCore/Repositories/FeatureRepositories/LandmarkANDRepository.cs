﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.LandmarksAND;
using Geomatic.Core.Repositories.StatusUpdatersAND;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class LandmarkANDRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmark.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmark.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmark.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmark.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmark.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmark.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmark.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmark.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmark.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmark.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public LandmarkANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsLandmark>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhLandmark>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpLandmark>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgLandmark>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkLandmark>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnLandmark>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvLandmark>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkLandmark>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgLandmark>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgLandmark>(id);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmark>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmark>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmark>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmark>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmark>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmark>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmark>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmark>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmark>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmark>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmark>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmark>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmark>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmark>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmark>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmark>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmark>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmark>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmark>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmark>(queryFilter);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GLandmarkAND landmark = (GLandmarkAND)obj;

            new StatusUpdater().Insert(landmark);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        landmark = ((GAsLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        landmark = ((GJhLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        landmark = ((GJpLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        landmark = ((GKgLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        landmark = ((GKkLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        landmark = ((GKnLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        landmark = ((GKvLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        landmark = ((GMkLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        landmark = ((GPgLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        landmark = ((GTgLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            landmark.UpdateTextAND();
            return landmark;
        }

        public override void Update(IGObject obj)
        {
            GLandmarkAND absObj = (GLandmarkAND)obj;

            new StatusUpdater().Update(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmark landmark = (GAsLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmark landmark = (GJhLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmark landmark = (GJpLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmark landmark = (GKgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmark landmark = (GKkLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmark landmark = (GKnLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmark landmark = (GKvLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmark landmark = (GMkLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmark landmark = (GPgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmark landmark = (GTgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GLandmarkAND absObj = (GLandmarkAND)obj;

            switch (SegmentName)
                {
                    case SegmentName.AS:
                        {
                            GAsLandmark landmark = (GAsLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.JH:
                        {
                            GJhLandmark landmark = (GJhLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.JP:
                        {
                            GJpLandmark landmark = (GJpLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.KG:
                        {
                            GKgLandmark landmark = (GKgLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.KK:
                        {
                            GKkLandmark landmark = (GKkLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.KN:
                        {
                            GKnLandmark landmark = (GKnLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.KV:
                        {
                            GKvLandmark landmark = (GKvLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.MK:
                        {
                            GMkLandmark landmark = (GMkLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.PG:
                        {
                            GPgLandmark landmark = (GPgLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    case SegmentName.TG:
                        {
                            GTgLandmark landmark = (GTgLandmark)obj;
                            landmark.Delete();
                            break;
                        }
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLandmark>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLandmark>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLandmark>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLandmark>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLandmark>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLandmark>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLandmark>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLandmark>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLandmark>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLandmark>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        // noraini - Apr 2021 - function to cater STATUS & AND_STATUS for Integration to NPES
        // update data without STATUS & AND_STATUS
        public override void UpdateRemainStatus(IGObject obj)
        {
            GLandmarkAND absObj = (GLandmarkAND)obj;
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to delete
        public override void UpdateDeleteByAND(IGObject obj)
        {
            GLandmarkAND absObj = (GLandmarkAND)obj;
            new StatusUpdater().UpdateDeleteByAND(absObj);
            UpdateDataSegment(obj);
        }

        // update STATUS & AND_STATUS to update graphic value
        public override void UpdateGraphic(IGObject obj)
        {
            GLandmarkAND absObj = (GLandmarkAND)obj;
            new StatusUpdater().UpdateGraphicByAND(absObj);
            UpdateDataSegment(obj);
        }

        // to update data perSegment
        private void UpdateDataSegment(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmark landmark = (GAsLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmark landmark = (GJhLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmark landmark = (GJpLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmark landmark = (GKgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmark landmark = (GKkLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmark landmark = (GKnLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmark landmark = (GKvLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmark landmark = (GMkLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmark landmark = (GPgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmark landmark = (GTgLandmark)obj;
                        landmark.Update();
                        landmark.UpdateTextAND();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal abstract class CaduFeatureRepository : FeatureRepository
    {
        /// <summary>
        /// Esri Feature Class
        /// Default get by TableName
        /// </summary>
        protected override IFeatureClass FeatureClass
        {
            get
            {
                return Session.Current.Cadu.OpenFeatureClass(TableName);
            }
        }

        public CaduFeatureRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        //noraini - Apr 2021
        public override void UpdateGraphic(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateRemainStatus(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateInsertByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateDeleteByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateGraphicByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.StreetRestrictions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class StreetRestrictionRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreetRestriction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreetRestriction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreetRestriction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreetRestriction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreetRestriction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreetRestriction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreetRestriction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreetRestriction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreetRestriction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreetRestriction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public StreetRestrictionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsStreetRestriction>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhStreetRestriction>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpStreetRestriction>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgStreetRestriction>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkStreetRestriction>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnStreetRestriction>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvStreetRestriction>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkStreetRestriction>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgStreetRestriction>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgStreetRestriction>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreetRestriction>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreetRestriction>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreetRestriction>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreetRestriction>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreetRestriction>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreetRestriction>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreetRestriction>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreetRestriction>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreetRestriction>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreetRestriction>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreetRestriction>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreetRestriction>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreetRestriction>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreetRestriction>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreetRestriction>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreetRestriction>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreetRestriction>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreetRestriction>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreetRestriction>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreetRestriction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetRestriction restriction = (GAsStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhStreetRestriction restriction = (GJhStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpStreetRestriction restriction = (GJpStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgStreetRestriction restriction = (GKgStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkStreetRestriction restriction = (GKkStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnStreetRestriction restriction = (GKnStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvStreetRestriction restriction = (GKvStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkStreetRestriction restriction = (GMkStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgStreetRestriction restriction = (GPgStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgStreetRestriction restriction = (GTgStreetRestriction)obj;
                        return restriction.InsertInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetRestriction restriction = (GAsStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreetRestriction restriction = (GJhStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreetRestriction restriction = (GJpStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreetRestriction restriction = (GKgStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreetRestriction restriction = (GKkStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreetRestriction restriction = (GKnStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreetRestriction restriction = (GKvStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreetRestriction restriction = (GMkStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreetRestriction restriction = (GPgStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreetRestriction restriction = (GTgStreetRestriction)obj;
                        restriction.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetRestriction restriction = (GAsStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreetRestriction restriction = (GJhStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreetRestriction restriction = (GJpStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreetRestriction restriction = (GKgStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreetRestriction restriction = (GKkStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreetRestriction restriction = (GKnStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreetRestriction restriction = (GKvStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreetRestriction restriction = (GMkStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreetRestriction restriction = (GPgStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreetRestriction restriction = (GTgStreetRestriction)obj;
                        restriction.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreetRestriction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreetRestriction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreetRestriction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreetRestriction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreetRestriction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreetRestriction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreetRestriction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreetRestriction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreetRestriction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreetRestriction>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

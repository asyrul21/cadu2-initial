﻿using System;
using System.Collections.Generic;
using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.Jupem;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class JupemRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            //get table name based on segment
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsJupem.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhJupem.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpJupem.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgJupem.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkJupem.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnJupem.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvJupem.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkJupem.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgJupem.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgJupem.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }

            }
        }

        public JupemRepository(SegmentName segmentName)
           : base(segmentName)
        {
        }

        //get by Id
        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsJupem>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhJupem>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpJupem>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgJupem>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkJupem>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnJupem>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvJupem>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkJupem>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgJupem>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgJupem>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //getbyIds
        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJupem>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJupem>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJupem>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJupem>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJupem>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJupem>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJupem>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJupem>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJupem>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJupem>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        //search
        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJupem>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJupem>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJupem>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJupem>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJupem>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJupem>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJupem>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJupem>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJupem>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJupem>(queryFilter);

                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            return null;
        }
        public override void Update(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }
        public override void Delete(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        //newObj
        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsJupem>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhJupem>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpJupem>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkJupem>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgJupem>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvJupem>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnJupem>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkJupem>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgJupem>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgJupem>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
        
    }
}

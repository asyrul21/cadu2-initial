﻿// this file was created by asyrul
// to enable updating of TEL_NO table
// feature is cancelled

using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Phones;
using Geomatic.Core.Rows.Phones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class PropertyPhoneFeatureRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPhone.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPhone.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPhone.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPhone.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPhone.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPhone.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPhoneFeature.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPhone.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPhone.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPhone.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", SegmentName));
                }
            }
        }

        public PropertyPhoneFeatureRepository(SegmentName segmentName)
           : base(segmentName)
        { }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsPhone>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhPhone>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpPhone>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgPhone>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkPhone>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnPhone>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvPhoneFeature>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkPhone>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgPhone>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgPhone>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPhone>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPhone>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPhone>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPhone>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPhone>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPhone>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPhoneFeature>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPhone>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPhone>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPhone>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPhone>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPhone>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPhone>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPhone>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPhone>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPhone>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPhoneFeature>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPhone>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPhone>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPhone>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            throw new Exception("Not Implemented");
        }

        public override void Update(IGObject obj)
        {
            //throw new Exception("Readonly");
            // added by asyrul
            // to enable UNGEOCODE function, when deleting property
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPhone phone = (GAsPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPhone phone = (GJhPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPhone phone = (GJpPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPhone phone = (GKgPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPhone phone = (GKkPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPhone phone = (GKnPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPhoneFeature phone = (GKvPhoneFeature)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPhone phone = (GMkPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPhone phone = (GPgPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPhone phone = (GTgPhone)obj;
                        phone.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            // added end
        }

        public override void Delete(IGObject obj)
        {
            throw new Exception("Not Implemented");
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPhone>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPhone>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPhone>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPhone>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPhone>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPhone>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPhoneFeature>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPhone>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPhone>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPhone>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal abstract class FeatureRepository : Repository
    {
        /// <summary>
        /// Esri Feature Class
        /// </summary>
        protected abstract IFeatureClass FeatureClass { get; }

        public FeatureRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override int Count(IQueryFilter queryFilter)
        {
            return FeatureClass.FeatureCount(queryFilter);
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Landmarks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedLandmarkRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedLandmark.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedLandmark.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedLandmark.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedLandmark.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedLandmark.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedLandmark.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedLandmark.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedLandmark.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedLandmark.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedLandmark.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedLandmarkRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedLandmark>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedLandmark>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedLandmark>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedLandmark>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedLandmark>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedLandmark>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedLandmark>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedLandmark>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedLandmark>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedLandmark>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedLandmark>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedLandmark>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedLandmark>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedLandmark>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedLandmark>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedLandmark>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedLandmark>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedLandmark>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedLandmark>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedLandmark>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedLandmark>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedLandmark>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedLandmark>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedLandmark>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedLandmark>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedLandmark>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedLandmark>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedLandmark>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedLandmark>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedLandmark>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedLandmark deletedLandmark;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedLandmark = ((GAsDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedLandmark = ((GJhDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedLandmark = ((GJpDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedLandmark = ((GKgDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedLandmark = ((GKkDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedLandmark = ((GKnDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedLandmark = ((GKvDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedLandmark = ((GMkDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedLandmark = ((GPgDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedLandmark = ((GTgDeletedLandmark)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedLandmark;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedLandmark deletedLandmark = (GAsDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedLandmark deletedLandmark = (GJhDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedLandmark deletedLandmark = (GJpDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedLandmark deletedLandmark = (GKgDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedLandmark deletedLandmark = (GKkDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedLandmark deletedLandmark = (GKnDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedLandmark deletedLandmark = (GKvDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedLandmark deletedLandmark = (GMkDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedLandmark deletedLandmark = (GPgDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedLandmark deletedLandmark = (GTgDeletedLandmark)obj;
                        deletedLandmark.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedLandmark absObj = (GDeletedLandmark)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedLandmark deletedLandmark = (GAsDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedLandmark deletedLandmark = (GJhDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedLandmark deletedLandmark = (GJpDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedLandmark deletedLandmark = (GKgDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedLandmark deletedLandmark = (GKkDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedLandmark deletedLandmark = (GKnDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedLandmark deletedLandmark = (GKvDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedLandmark deletedLandmark = (GMkDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedLandmark deletedLandmark = (GPgDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedLandmark deletedLandmark = (GTgDeletedLandmark)obj;
                        deletedLandmark.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedLandmark>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedLandmark>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedLandmark>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedLandmark>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedLandmark>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedLandmark>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedLandmark>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedLandmark>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedLandmark>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedLandmark>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

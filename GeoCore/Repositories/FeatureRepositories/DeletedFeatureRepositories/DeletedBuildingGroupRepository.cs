﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.BuildingGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedBuildingGroupRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedBuildingGroup.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedBuildingGroup.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedBuildingGroupRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedBuildingGroup>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedBuildingGroup>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedBuildingGroup>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedBuildingGroup>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedBuildingGroup>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedBuildingGroup>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedBuildingGroup>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedBuildingGroup>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedBuildingGroup>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedBuildingGroup>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedBuildingGroup>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedBuildingGroup>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedBuildingGroup>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedBuildingGroup>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedBuildingGroup>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedBuildingGroup>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedBuildingGroup>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedBuildingGroup>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedBuildingGroup>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedBuildingGroup>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedBuildingGroup>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedBuildingGroup>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedBuildingGroup>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedBuildingGroup>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedBuildingGroup>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedBuildingGroup>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedBuildingGroup>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedBuildingGroup>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedBuildingGroup>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedBuildingGroup>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedBuildingGroup deletedBuildingGroup;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedBuildingGroup = ((GAsDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedBuildingGroup = ((GJhDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedBuildingGroup = ((GJpDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedBuildingGroup = ((GKgDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedBuildingGroup = ((GKkDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedBuildingGroup = ((GKnDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedBuildingGroup = ((GKvDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedBuildingGroup = ((GMkDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedBuildingGroup = ((GPgDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedBuildingGroup = ((GTgDeletedBuildingGroup)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedBuildingGroup;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedBuildingGroup deletedBuildingGroup = (GAsDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedBuildingGroup deletedBuildingGroup = (GJhDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedBuildingGroup deletedBuildingGroup = (GJpDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedBuildingGroup deletedBuildingGroup = (GKgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedBuildingGroup deletedBuildingGroup = (GKkDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedBuildingGroup deletedBuildingGroup = (GKnDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedBuildingGroup deletedBuildingGroup = (GKvDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedBuildingGroup deletedBuildingGroup = (GMkDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedBuildingGroup deletedBuildingGroup = (GPgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedBuildingGroup deletedBuildingGroup = (GTgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedBuildingGroup absObj = (GDeletedBuildingGroup)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedBuildingGroup deletedBuildingGroup = (GAsDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedBuildingGroup deletedBuildingGroup = (GJhDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedBuildingGroup deletedBuildingGroup = (GJpDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedBuildingGroup deletedBuildingGroup = (GKgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedBuildingGroup deletedBuildingGroup = (GKkDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedBuildingGroup deletedBuildingGroup = (GKnDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedBuildingGroup deletedBuildingGroup = (GKvDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedBuildingGroup deletedBuildingGroup = (GMkDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedBuildingGroup deletedBuildingGroup = (GPgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedBuildingGroup deletedBuildingGroup = (GTgDeletedBuildingGroup)obj;
                        deletedBuildingGroup.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedBuildingGroup>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedBuildingGroup>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedBuildingGroup>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedBuildingGroup>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedBuildingGroup>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedBuildingGroup>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedBuildingGroup>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedBuildingGroup>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedBuildingGroup>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedBuildingGroup>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

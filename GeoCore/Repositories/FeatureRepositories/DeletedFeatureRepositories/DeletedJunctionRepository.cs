﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Junctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedJunctionRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedJunction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedJunction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedJunction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedJunction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedJunction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedJunction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedJunction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedJunction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedJunction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedJunction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedJunctionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedJunction>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedJunction>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedJunction>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedJunction>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedJunction>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedJunction>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedJunction>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedJunction>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedJunction>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedJunction>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedJunction>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedJunction>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedJunction>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedJunction>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedJunction>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedJunction>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedJunction>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedJunction>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedJunction>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedJunction>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedJunction>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedJunction>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedJunction>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedJunction>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedJunction>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedJunction>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedJunction>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedJunction>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedJunction>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedJunction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedJunction deletedJunction;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedJunction = ((GAsDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedJunction = ((GJhDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedJunction = ((GJpDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedJunction = ((GKgDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedJunction = ((GKkDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedJunction = ((GKnDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedJunction = ((GKvDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedJunction = ((GMkDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedJunction = ((GPgDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedJunction = ((GTgDeletedJunction)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedJunction;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedJunction deletedJunction = (GAsDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedJunction deletedJunction = (GJhDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedJunction deletedJunction = (GJpDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedJunction deletedJunction = (GKgDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedJunction deletedJunction = (GKkDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedJunction deletedJunction = (GKnDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedJunction deletedJunction = (GKvDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedJunction deletedJunction = (GMkDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedJunction deletedJunction = (GPgDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedJunction deletedJunction = (GTgDeletedJunction)obj;
                        deletedJunction.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedJunction absObj = (GDeletedJunction)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedJunction deletedJunction = (GAsDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedJunction deletedJunction = (GJhDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedJunction deletedJunction = (GJpDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedJunction deletedJunction = (GKgDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedJunction deletedJunction = (GKkDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedJunction deletedJunction = (GKnDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedJunction deletedJunction = (GKvDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedJunction deletedJunction = (GMkDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedJunction deletedJunction = (GPgDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedJunction deletedJunction = (GTgDeletedJunction)obj;
                        deletedJunction.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedJunction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedJunction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedJunction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedJunction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedJunction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedJunction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedJunction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedJunction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedJunction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedJunction>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

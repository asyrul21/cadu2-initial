﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Streets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedStreetRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedStreetRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedStreet>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedStreet>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedStreet>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedStreet>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedStreet>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedStreet>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedStreet>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedStreet>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedStreet>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedStreet>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedStreet>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedStreet>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedStreet>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedStreet>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedStreet>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedStreet>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedStreet>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedStreet>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedStreet>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedStreet>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedStreet>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedStreet>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedStreet>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedStreet>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedStreet>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedStreet>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedStreet>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedStreet>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedStreet>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedStreet>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedStreet deletedStreet;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedStreet = ((GAsDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedStreet = ((GJhDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedStreet = ((GJpDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedStreet = ((GKgDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedStreet = ((GKkDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedStreet = ((GKnDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedStreet = ((GKvDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedStreet = ((GMkDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedStreet = ((GPgDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedStreet = ((GTgDeletedStreet)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedStreet;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedStreet deletedStreet = (GAsDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedStreet deletedStreet = (GJhDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedStreet deletedStreet = (GJpDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedStreet deletedStreet = (GKgDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedStreet deletedStreet = (GKkDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedStreet deletedStreet = (GKnDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedStreet deletedStreet = (GKvDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedStreet deletedStreet = (GMkDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedStreet deletedStreet = (GPgDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedStreet deletedStreet = (GTgDeletedStreet)obj;
                        deletedStreet.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedStreet deletedStreet = (GAsDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedStreet deletedStreet = (GJhDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedStreet deletedStreet = (GJpDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedStreet deletedStreet = (GKgDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedStreet deletedStreet = (GKkDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedStreet deletedStreet = (GKnDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedStreet deletedStreet = (GKvDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedStreet deletedStreet = (GMkDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedStreet deletedStreet = (GPgDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedStreet deletedStreet = (GTgDeletedStreet)obj;
                        deletedStreet.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedStreet>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedStreet>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedStreet>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedStreet>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedStreet>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedStreet>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedStreet>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedStreet>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedStreet>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedStreet>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

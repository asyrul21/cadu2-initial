﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedPropertyRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedPropertyRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedProperty>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedProperty>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedProperty>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedProperty>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedProperty>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedProperty>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedProperty>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedProperty>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedProperty>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedProperty>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedProperty>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedProperty>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedProperty>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedProperty>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedProperty>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedProperty>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedProperty>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedProperty>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedProperty>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedProperty>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedProperty>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedProperty>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedProperty>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedProperty>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedProperty>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedProperty>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedProperty>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedProperty>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedProperty>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedProperty>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedProperty deletedProperty;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedProperty = ((GAsDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedProperty = ((GJhDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedProperty = ((GJpDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedProperty = ((GKgDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedProperty = ((GKkDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedProperty = ((GKnDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedProperty = ((GKvDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedProperty = ((GMkDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedProperty = ((GPgDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedProperty = ((GTgDeletedProperty)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedProperty;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedProperty deletedProperty = (GAsDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedProperty deletedProperty = (GJhDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedProperty deletedProperty = (GJpDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedProperty deletedProperty = (GKgDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedProperty deletedProperty = (GKkDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedProperty deletedProperty = (GKnDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedProperty deletedProperty = (GKvDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedProperty deletedProperty = (GMkDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedProperty deletedProperty = (GPgDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedProperty deletedProperty = (GTgDeletedProperty)obj;
                        deletedProperty.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedProperty absObj = (GDeletedProperty)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedProperty deletedProperty = (GAsDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedProperty deletedProperty = (GJhDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedProperty deletedProperty = (GJpDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedProperty deletedProperty = (GKgDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedProperty deletedProperty = (GKkDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedProperty deletedProperty = (GKnDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedProperty deletedProperty = (GKvDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedProperty deletedProperty = (GMkDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedProperty deletedProperty = (GPgDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedProperty deletedProperty = (GTgDeletedProperty)obj;
                        deletedProperty.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedProperty>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedProperty>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedProperty>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedProperty>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedProperty>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedProperty>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedProperty>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedProperty>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedProperty>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedProperty>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

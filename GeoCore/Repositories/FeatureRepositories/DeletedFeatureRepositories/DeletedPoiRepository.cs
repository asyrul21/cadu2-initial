﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Pois;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedPoiRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedPoi.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedPoi.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedPoi.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedPoi.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedPoi.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedPoi.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedPoi.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedPoi.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedPoi.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedPoi.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedPoiRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedPoi>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedPoi>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedPoi>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedPoi>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedPoi>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedPoi>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedPoi>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedPoi>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedPoi>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedPoi>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedPoi>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedPoi>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedPoi>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedPoi>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedPoi>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedPoi>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedPoi>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedPoi>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedPoi>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedPoi>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedPoi>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedPoi>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedPoi>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedPoi>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedPoi>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedPoi>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedPoi>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedPoi>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedPoi>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedPoi>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedPoi deletedPoi;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedPoi = ((GAsDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedPoi = ((GJhDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedPoi = ((GJpDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedPoi = ((GKgDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedPoi = ((GKkDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedPoi = ((GKnDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedPoi = ((GKvDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedPoi = ((GMkDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedPoi = ((GPgDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedPoi = ((GTgDeletedPoi)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedPoi;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedPoi deletedPoi = (GAsDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedPoi deletedPoi = (GJhDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedPoi deletedPoi = (GJpDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedPoi deletedPoi = (GKgDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedPoi deletedPoi = (GKkDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedPoi deletedPoi = (GKnDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedPoi deletedPoi = (GKvDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedPoi deletedPoi = (GMkDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedPoi deletedPoi = (GPgDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedPoi deletedPoi = (GTgDeletedPoi)obj;
                        deletedPoi.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedPoi absObj = (GDeletedPoi)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedPoi deletedPoi = (GAsDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedPoi deletedPoi = (GJhDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedPoi deletedPoi = (GJpDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedPoi deletedPoi = (GKgDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedPoi deletedPoi = (GKkDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedPoi deletedPoi = (GKnDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedPoi deletedPoi = (GKvDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedPoi deletedPoi = (GMkDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedPoi deletedPoi = (GPgDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedPoi deletedPoi = (GTgDeletedPoi)obj;
                        deletedPoi.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedPoi>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedPoi>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedPoi>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedPoi>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedPoi>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedPoi>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedPoi>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedPoi>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedPoi>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedPoi>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

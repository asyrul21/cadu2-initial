﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Features.DeletedFeatures.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories
{
    internal class DeletedBuildingRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsDeletedBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhDeletedBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpDeletedBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgDeletedBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkDeletedBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnDeletedBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvDeletedBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkDeletedBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgDeletedBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgDeletedBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public DeletedBuildingRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsDeletedBuilding>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhDeletedBuilding>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpDeletedBuilding>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgDeletedBuilding>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkDeletedBuilding>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnDeletedBuilding>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvDeletedBuilding>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkDeletedBuilding>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgDeletedBuilding>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgDeletedBuilding>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedBuilding>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedBuilding>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedBuilding>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedBuilding>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedBuilding>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedBuilding>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedBuilding>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedBuilding>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedBuilding>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedBuilding>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsDeletedBuilding>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhDeletedBuilding>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpDeletedBuilding>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgDeletedBuilding>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkDeletedBuilding>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnDeletedBuilding>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvDeletedBuilding>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkDeletedBuilding>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgDeletedBuilding>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgDeletedBuilding>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GDeletedBuilding deletedBuilding;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        deletedBuilding = ((GAsDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JH:
                    {
                        deletedBuilding = ((GJhDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.JP:
                    {
                        deletedBuilding = ((GJpDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KG:
                    {
                        deletedBuilding = ((GKgDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KK:
                    {
                        deletedBuilding = ((GKkDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KN:
                    {
                        deletedBuilding = ((GKnDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.KV:
                    {
                        deletedBuilding = ((GKvDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.MK:
                    {
                        deletedBuilding = ((GMkDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.PG:
                    {
                        deletedBuilding = ((GPgDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                case SegmentName.TG:
                    {
                        deletedBuilding = ((GTgDeletedBuilding)obj).InsertInto(FeatureClass);
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            return deletedBuilding;
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedBuilding deletedBuilding = (GAsDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedBuilding deletedBuilding = (GJhDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedBuilding deletedBuilding = (GJpDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedBuilding deletedBuilding = (GKgDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedBuilding deletedBuilding = (GKkDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedBuilding deletedBuilding = (GKnDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedBuilding deletedBuilding = (GKvDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedBuilding deletedBuilding = (GMkDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedBuilding deletedBuilding = (GPgDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedBuilding deletedBuilding = (GTgDeletedBuilding)obj;
                        deletedBuilding.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GDeletedBuilding absObj = (GDeletedBuilding)obj;

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsDeletedBuilding deletedBuilding = (GAsDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhDeletedBuilding deletedBuilding = (GJhDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpDeletedBuilding deletedBuilding = (GJpDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgDeletedBuilding deletedBuilding = (GKgDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkDeletedBuilding deletedBuilding = (GKkDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnDeletedBuilding deletedBuilding = (GKnDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvDeletedBuilding deletedBuilding = (GKvDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkDeletedBuilding deletedBuilding = (GMkDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgDeletedBuilding deletedBuilding = (GPgDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgDeletedBuilding deletedBuilding = (GTgDeletedBuilding)obj;
                        deletedBuilding.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsDeletedBuilding>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhDeletedBuilding>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpDeletedBuilding>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgDeletedBuilding>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkDeletedBuilding>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnDeletedBuilding>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvDeletedBuilding>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkDeletedBuilding>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgDeletedBuilding>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgDeletedBuilding>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

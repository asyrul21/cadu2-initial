﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingTexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class BuildingTextRepository : TextRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingTextRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuildingText>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuildingText>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuildingText>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuildingText>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuildingText>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuildingText>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuildingText>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuildingText>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuildingText>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuildingText>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsBuildingText>(ids);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhBuildingText>(ids);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpBuildingText>(ids);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgBuildingText>(ids);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkBuildingText>(ids);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnBuildingText>(ids);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvBuildingText>(ids);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkBuildingText>(ids);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgBuildingText>(ids);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgBuildingText>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsBuildingText>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhBuildingText>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpBuildingText>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgBuildingText>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkBuildingText>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnBuildingText>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvBuildingText>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkBuildingText>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgBuildingText>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgBuildingText>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingText text = (GAsBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingText text = (GJhBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingText text = (GJpBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingText text = (GKgBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingText text = (GKkBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingText text = (GKnBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingText text = (GKvBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingText text = (GMkBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingText text = (GPgBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingText text = (GTgBuildingText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingText text = (GAsBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingText text = (GJhBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingText text = (GJpBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingText text = (GKgBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingText text = (GKkBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingText text = (GKnBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingText text = (GKvBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingText text = (GMkBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingText text = (GPgBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingText text = (GTgBuildingText)obj;
                        text.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingText text = (GAsBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingText text = (GJhBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingText text = (GJpBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingText text = (GKgBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingText text = (GKkBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingText text = (GKnBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingText text = (GKvBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingText text = (GMkBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingText text = (GPgBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingText text = (GTgBuildingText)obj;
                        text.DeleteText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

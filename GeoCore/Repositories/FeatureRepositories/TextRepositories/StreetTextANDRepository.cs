﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.StreetTextsAND;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class StreetTextANDRepository : TextRepository //StreetTextRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreetText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreetText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreetText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreetText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreetText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreetText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreetText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreetText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreetText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreetText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public StreetTextANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsStreetText>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhStreetText>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpStreetText>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgStreetText>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkStreetText>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnStreetText>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvStreetText>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkStreetText>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgStreetText>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgStreetText>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsStreetText>(ids);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhStreetText>(ids);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpStreetText>(ids);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgStreetText>(ids);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkStreetText>(ids);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnStreetText>(ids);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvStreetText>(ids);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkStreetText>(ids);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgStreetText>(ids);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgStreetText>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsStreetText>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhStreetText>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpStreetText>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgStreetText>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkStreetText>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnStreetText>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvStreetText>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkStreetText>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgStreetText>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgStreetText>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetText text = (GAsStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhStreetText text = (GJhStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpStreetText text = (GJpStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgStreetText text = (GKgStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkStreetText text = (GKkStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnStreetText text = (GKnStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvStreetText text = (GKvStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkStreetText text = (GMkStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgStreetText text = (GPgStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgStreetText text = (GTgStreetText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetText text = (GAsStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreetText text = (GJhStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreetText text = (GJpStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreetText text = (GKgStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreetText text = (GKkStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreetText text = (GKnStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreetText text = (GKvStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreetText text = (GMkStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreetText text = (GPgStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreetText text = (GTgStreetText)obj;
                        text.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreetText text = (GAsStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreetText text = (GJhStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreetText text = (GJpStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreetText text = (GKgStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreetText text = (GKkStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreetText text = (GKnStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreetText text = (GKvStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreetText text = (GMkStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreetText text = (GPgStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreetText text = (GTgStreetText)obj;
                        text.DeleteText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreetText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreetText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreetText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreetText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreetText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreetText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreetText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreetText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreetText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreetText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

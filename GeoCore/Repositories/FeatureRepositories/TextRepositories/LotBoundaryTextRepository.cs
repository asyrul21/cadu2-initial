﻿using System;
using System.Collections.Generic;
using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.LotBoundaryTexts;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class LotBoundaryTextRepository : TextRepository
    {
        protected override string TableName
        {
            //get table name based on segment
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLotBoundaryText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLotBoundaryText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLotBoundaryText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLotBoundaryText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLotBoundaryText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLotBoundaryText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLotBoundaryText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLotBoundaryText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLotBoundaryText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLotBoundaryText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }

            }
        }

        public LotBoundaryTextRepository(SegmentName segmentName)
           : base(segmentName)
        {
        }

        //get by Id
        public override IGObject GetById(int id)
        {
            throw new Exception("Not implemented.");
        }

        //getbyIds
        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            throw new Exception("Not implemented.");
        }

        //search
        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            throw new Exception("Not implemented.");
        }

        public override IGObject Insert(IGObject obj)
        {
            return null;
        }
        public override void Update(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }
        public override void Delete(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        //newObj
        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLotBoundaryText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLotBoundaryText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLotBoundaryText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLotBoundaryText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLotBoundaryText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLotBoundaryText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLotBoundaryText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLotBoundaryText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLotBoundaryText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLotBoundaryText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
        
    }
}

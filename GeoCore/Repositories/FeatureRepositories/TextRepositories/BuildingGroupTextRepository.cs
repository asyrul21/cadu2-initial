﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.BuildingGroupTexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class BuildingGroupTextRepository : TextRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingGroupText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingGroupText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingGroupText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingGroupText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingGroupText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingGroupText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingGroupText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingGroupText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingGroupText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingGroupText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingGroupTextRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuildingGroupText>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuildingGroupText>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuildingGroupText>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuildingGroupText>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuildingGroupText>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuildingGroupText>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuildingGroupText>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuildingGroupText>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuildingGroupText>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuildingGroupText>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsBuildingGroupText>(ids);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhBuildingGroupText>(ids);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpBuildingGroupText>(ids);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgBuildingGroupText>(ids);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkBuildingGroupText>(ids);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnBuildingGroupText>(ids);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvBuildingGroupText>(ids);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkBuildingGroupText>(ids);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgBuildingGroupText>(ids);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgBuildingGroupText>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsBuildingGroupText>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhBuildingGroupText>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpBuildingGroupText>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgBuildingGroupText>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkBuildingGroupText>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnBuildingGroupText>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvBuildingGroupText>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkBuildingGroupText>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgBuildingGroupText>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgBuildingGroupText>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroupText text = (GAsBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroupText text = (GJhBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroupText text = (GJpBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroupText text = (GKgBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroupText text = (GKkBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroupText text = (GKnBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroupText text = (GKvBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroupText text = (GMkBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroupText text = (GPgBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroupText text = (GTgBuildingGroupText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroupText text = (GAsBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroupText text = (GJhBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroupText text = (GJpBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroupText text = (GKgBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroupText text = (GKkBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroupText text = (GKnBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroupText text = (GKvBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroupText text = (GMkBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroupText text = (GPgBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroupText text = (GTgBuildingGroupText)obj;
                        text.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroupText text = (GAsBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroupText text = (GJhBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroupText text = (GJpBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroupText text = (GKgBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroupText text = (GKkBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroupText text = (GKnBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroupText text = (GKvBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroupText text = (GMkBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroupText text = (GPgBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroupText text = (GTgBuildingGroupText)obj;
                        text.DeleteText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingGroupText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingGroupText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingGroupText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingGroupText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingGroupText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingGroupText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingGroupText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingGroupText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingGroupText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingGroupText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

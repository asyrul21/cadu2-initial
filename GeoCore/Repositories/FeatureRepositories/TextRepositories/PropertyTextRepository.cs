﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.PropertyTexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class PropertyTextRepository : TextRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPropertyText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPropertyText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPropertyText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPropertyText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPropertyText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPropertyText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPropertyText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPropertyText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPropertyText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPropertyText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PropertyTextRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsPropertyText>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhPropertyText>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpPropertyText>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgPropertyText>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkPropertyText>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnPropertyText>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvPropertyText>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkPropertyText>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgPropertyText>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgPropertyText>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsPropertyText>(ids);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhPropertyText>(ids);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpPropertyText>(ids);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgPropertyText>(ids);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkPropertyText>(ids);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnPropertyText>(ids);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvPropertyText>(ids);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkPropertyText>(ids);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgPropertyText>(ids);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgPropertyText>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsPropertyText>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhPropertyText>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpPropertyText>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgPropertyText>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkPropertyText>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnPropertyText>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvPropertyText>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkPropertyText>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgPropertyText>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgPropertyText>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPropertyText text = (GAsPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhPropertyText text = (GJhPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpPropertyText text = (GJpPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgPropertyText text = (GKgPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkPropertyText text = (GKkPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnPropertyText text = (GKnPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvPropertyText text = (GKvPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkPropertyText text = (GMkPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgPropertyText text = (GPgPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgPropertyText text = (GTgPropertyText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPropertyText text = (GAsPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPropertyText text = (GJhPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPropertyText text = (GJpPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPropertyText text = (GKgPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPropertyText text = (GKkPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPropertyText text = (GKnPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPropertyText text = (GKvPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPropertyText text = (GMkPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPropertyText text = (GPgPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPropertyText text = (GTgPropertyText)obj;
                        text.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPropertyText text = (GAsPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPropertyText text = (GJhPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPropertyText text = (GJpPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPropertyText text = (GKgPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPropertyText text = (GKkPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPropertyText text = (GKnPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPropertyText text = (GKvPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPropertyText text = (GMkPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPropertyText text = (GPgPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPropertyText text = (GTgPropertyText)obj;
                        text.DeleteText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPropertyText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPropertyText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPropertyText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPropertyText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPropertyText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPropertyText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPropertyText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPropertyText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPropertyText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPropertyText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

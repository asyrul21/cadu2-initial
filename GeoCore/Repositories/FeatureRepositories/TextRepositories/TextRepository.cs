﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal abstract class TextRepository : CaduFeatureRepository
    {
        public TextRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }
    }
}

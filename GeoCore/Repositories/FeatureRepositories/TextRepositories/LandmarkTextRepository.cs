﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.LandmarkTexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;


namespace Geomatic.Core.Repositories.FeatureRepositories.TextRepositories
{
    internal class LandmarkTextRepository : TextRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmarkText.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmarkText.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmarkText.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmarkText.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmarkText.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmarkText.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmarkText.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmarkText.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmarkText.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmarkText.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public LandmarkTextRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsLandmarkText>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhLandmarkText>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpLandmarkText>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgLandmarkText>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkLandmarkText>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnLandmarkText>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvLandmarkText>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkLandmarkText>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgLandmarkText>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgLandmarkText>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsLandmarkText>(ids);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhLandmarkText>(ids);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpLandmarkText>(ids);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgLandmarkText>(ids);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkLandmarkText>(ids);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnLandmarkText>(ids);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvLandmarkText>(ids);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkLandmarkText>(ids);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgLandmarkText>(ids);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgLandmarkText>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.MapText<GAsLandmarkText>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.MapText<GJhLandmarkText>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.MapText<GJpLandmarkText>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.MapText<GKgLandmarkText>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.MapText<GKkLandmarkText>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.MapText<GKnLandmarkText>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.MapText<GKvLandmarkText>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.MapText<GMkLandmarkText>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.MapText<GPgLandmarkText>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.MapText<GTgLandmarkText>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkText text = (GAsLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkText text = (GJhLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkText text = (GJpLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkText text = (GKgLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkText text = (GKkLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkText text = (GKnLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkText text = (GKvLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkText text = (GMkLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkText text = (GPgLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkText text = (GTgLandmarkText)obj;
                        return text.InsertTextInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkText text = (GAsLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkText text = (GJhLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkText text = (GJpLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkText text = (GKgLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkText text = (GKkLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkText text = (GKnLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkText text = (GKvLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkText text = (GMkLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkText text = (GPgLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkText text = (GTgLandmarkText)obj;
                        text.UpdateText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmarkText text = (GAsLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmarkText text = (GJhLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmarkText text = (GJpLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmarkText text = (GKgLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmarkText text = (GKkLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmarkText text = (GKnLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmarkText text = (GKvLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmarkText text = (GMkLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmarkText text = (GPgLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmarkText text = (GTgLandmarkText)obj;
                        text.DeleteText();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLandmarkText>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLandmarkText>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLandmarkText>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLandmarkText>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLandmarkText>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLandmarkText>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLandmarkText>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLandmarkText>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLandmarkText>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLandmarkText>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

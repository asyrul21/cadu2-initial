﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.Regions;


namespace Geomatic.Core.Repositories.FeatureRepositories
{
    internal class RegionRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            //get table name based on segment
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsRegion.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhRegion.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpRegion.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgRegion.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkRegion.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnRegion.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvRegion.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkRegion.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgRegion.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgRegion.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }

            }
        }

        public RegionRepository(SegmentName segmentName)
           : base(segmentName)
        {
        }

        //get by Id
        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsRegion>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhRegion>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpRegion>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgRegion>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkRegion>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnRegion>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvRegion>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkRegion>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgRegion>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgRegion>(id);
                 default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //getbyIds
        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsRegion>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhRegion>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpRegion>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgRegion>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkRegion>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnRegion>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvRegion>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkRegion>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgRegion>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgRegion>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        //search
        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsRegion>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhRegion>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpRegion>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgRegion>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkRegion>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnRegion>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvRegion>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkRegion>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgRegion>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgRegion>(queryFilter);

                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //insert
        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsRegion region = (GAsRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.JH:
                    {
                        GJhRegion region = (GJhRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.JP:
                    {
                        GJpRegion region = (GJpRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.KK:
                    {
                        GKkRegion region = (GKkRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.KG:
                    {
                        GKgRegion region = (GKgRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.KV:
                    {
                        GKvRegion region = (GKvRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.MK:
                    {
                        GMkRegion region = (GMkRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.KN:
                    {
                        GKnRegion region = (GKnRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.PG:
                    {
                        GPgRegion region = (GPgRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                case SegmentName.TG:
                    {
                        GTgRegion region = (GTgRegion)obj;
                        return region.InsertInto(FeatureClass);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //update
        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsRegion region = (GAsRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhRegion region = (GJhRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpRegion region = (GJpRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkRegion region = (GKkRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnRegion region = (GKnRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgRegion region = (GKgRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvRegion region = (GKvRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkRegion region = (GMkRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgRegion region = (GPgRegion)obj;
                        region.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgRegion region = (GTgRegion)obj;
                        region.Update();
                        break;
                    }

                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //delete
        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsRegion region = (GAsRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhRegion region = (GJhRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpRegion region = (GJpRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkRegion region = (GKkRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgRegion region = (GKgRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnRegion region = (GKnRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvRegion region = (GKvRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkRegion region = (GMkRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgRegion region = (GTgRegion)obj;
                        region.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgRegion region = (GPgRegion)obj;
                        region.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //newObj
        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsRegion>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhRegion>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpRegion>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkRegion>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgRegion>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvRegion>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnRegion>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkRegion>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgRegion>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgRegion>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

    }
}

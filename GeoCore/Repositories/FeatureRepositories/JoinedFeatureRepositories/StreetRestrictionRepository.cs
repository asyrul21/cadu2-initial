﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.StreetRestrictions;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class StreetRestrictionRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreetRestriction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreetRestriction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreetRestriction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreetRestriction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreetRestriction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreetRestriction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreetRestriction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreetRestriction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreetRestriction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreetRestriction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(GSource.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.STATUS);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.SOURCE);
                subFields.Add("{0}.{1}", GSource.TABLE_NAME, GSource.NAME);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.JUNCTION_ID);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.START_ID);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID1);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID2);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID3);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID4);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID5);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID6);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID7);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID8);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID9);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.END_ID10);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GStreetRestriction.DATE_CREATED);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreetRestriction.SOURCE, GSource.TABLE_NAME, GSource.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public StreetRestrictionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsStreetRestriction>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhStreetRestriction>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpStreetRestriction>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgStreetRestriction>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkStreetRestriction>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnStreetRestriction>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvStreetRestriction>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkStreetRestriction>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgStreetRestriction>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgStreetRestriction>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreetRestriction>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreetRestriction>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreetRestriction>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreetRestriction>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreetRestriction>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreetRestriction>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreetRestriction>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreetRestriction>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreetRestriction>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreetRestriction>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreetRestriction>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreetRestriction>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreetRestriction>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreetRestriction>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreetRestriction>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreetRestriction>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreetRestriction>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreetRestriction>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreetRestriction>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreetRestriction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreetRestriction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreetRestriction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreetRestriction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreetRestriction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreetRestriction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreetRestriction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreetRestriction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreetRestriction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreetRestriction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreetRestriction>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

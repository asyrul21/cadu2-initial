﻿using ESRI.ArcGIS.Geodatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class AllPoiRepository : PoiRepository
    {
        protected override string Tables
        {
            get { throw new NotImplementedException(); }
        }

        protected override string SubFields
        {
            get { throw new NotImplementedException(); }
        }

        protected override string WhereClause
        {
            get { throw new NotImplementedException(); }
        }

        public AllPoiRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            IGObject obj;
            obj = new BuildingPoiRepository(SegmentName).GetById(id);
            if (obj != null)
            {
                return obj;
            }
            obj = new PropertyPoiRepository(SegmentName).GetById(id);
            if (obj != null)
            {
                return obj;
            }
            obj = new LandmarkPoiRepository(SegmentName).GetById(id);
            if (obj != null)
            {
                return obj;
            }
            return null;
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            foreach (IGObject obj in new BuildingPoiRepository(SegmentName).GetByIds(ids))
            {
                yield return obj;
            }
            foreach (IGObject obj in new PropertyPoiRepository(SegmentName).GetByIds(ids))
            {
                yield return obj;
            }
            foreach (IGObject obj in new LandmarkPoiRepository(SegmentName).GetByIds(ids))
            {
                yield return obj;
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            foreach (IGObject obj in new BuildingPoiRepository(SegmentName).Search(queryFilter))
            {
                yield return obj;
            }
            foreach (IGObject obj in new PropertyPoiRepository(SegmentName).Search(queryFilter))
            {
                yield return obj;
            }
            foreach (IGObject obj in new LandmarkPoiRepository(SegmentName).Search(queryFilter))
            {
                yield return obj;
            }
        }
    }
}

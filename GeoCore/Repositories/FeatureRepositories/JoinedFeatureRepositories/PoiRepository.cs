﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.JoinedFeatures.Pois;
using Geomatic.Core.Features.Landmarks;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal abstract class PoiRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPoi.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPoi.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPoi.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPoi.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPoi.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPoi.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPoi.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPoi.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPoi.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPoi.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string BuildingTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string PropertyTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string LandmarkTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmark.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmark.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmark.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmark.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmark.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmark.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmark.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmark.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmark.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmark.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PoiRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsPoi>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhPoi>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpPoi>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgPoi>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkPoi>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnPoi>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvPoi>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkPoi>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgPoi>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgPoi>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPoi>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPoi>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPoi>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPoi>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPoi>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPoi>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPoi>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPoi>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPoi>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPoi>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsPoi>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhPoi>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpPoi>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgPoi>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkPoi>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnPoi>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvPoi>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkPoi>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgPoi>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgPoi>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPoi>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPoi>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPoi>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPoi>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPoi>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPoi>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPoi>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPoi>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPoi>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPoi>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.Junctions;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class JunctionRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsJunction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhJunction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpJunction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgJunction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkJunction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnJunction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvJunction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkJunction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgJunction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgJunction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(GJunctionType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GJunction.NAME);
                subFields.Add("{0}.{1}", TableName, GJunction.TYPE);
                subFields.Add("{0}.{1}", TableName, GJunction.SOURCE);
                subFields.Add("{0}.{1}", TableName, GJunction.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GJunction.AREA_ID);
                subFields.Add("{0}.{1}", GJunctionType.TABLE_NAME, GJunctionType.NAME);
                subFields.Add("{0}.{1}", TableName, GJunction.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GJunction.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GJunction.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GJunction.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GJunction.AND_STATUS);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.{3}", TableName, GJunction.TYPE, GJunctionType.TABLE_NAME, GJunctionType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public JunctionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsJunction>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhJunction>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpJunction>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgJunction>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkJunction>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnJunction>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvJunction>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkJunction>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgJunction>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgJunction>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJunction>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJunction>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJunction>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJunction>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJunction>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJunction>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJunction>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJunction>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJunction>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJunction>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsJunction>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhJunction>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpJunction>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgJunction>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkJunction>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnJunction>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvJunction>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkJunction>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgJunction>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgJunction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsJunction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhJunction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpJunction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgJunction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkJunction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnJunction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvJunction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkJunction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgJunction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgJunction>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

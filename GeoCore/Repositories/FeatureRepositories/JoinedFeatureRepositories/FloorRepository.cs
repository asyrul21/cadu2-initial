﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.Floors;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GFloor = Geomatic.Core.Features.JoinedFeatures.GFloor;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GStreetType = Geomatic.Core.Rows.GStreetType;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class FloorRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsFloor.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhFloor.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpFloor.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgFloor.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkFloor.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnFloor.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvFloor.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkFloor.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgFloor.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgFloor.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string PropertyTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(PropertyTableName);
                tables.Add(StreetTableName);
                tables.Add(GPropertyType.TABLE_NAME);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GFloor.NUMBER);
                // added by asyrul
                subFields.Add("{0}.{1}", TableName, GFloor.UNIT_NUM);
                // added end
                subFields.Add("{0}.{1}", TableName, GFloor.PROPERTY_ID);
                subFields.Add("{0}.{1}", TableName, GFloor.UPDATE_STATUS);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.TYPE);
                subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.NAME);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.STREET_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GFloor.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GFloor.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GFloor.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GFloor.DATE_UPDATED);
                // added by noraini ali
                subFields.Add("{0}.{1}", TableName, GFloor.AND_STATUS);
                // added end
                subFields.Add("{0}.SHAPE", PropertyTableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GFloor.PROPERTY_ID, PropertyTableName);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", PropertyTableName, GProperty.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyTableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public FloorRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsFloor>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhFloor>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpFloor>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgFloor>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkFloor>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnFloor>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvFloor>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkFloor>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgFloor>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgFloor>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsFloor>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhFloor>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpFloor>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgFloor>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkFloor>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnFloor>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvFloor>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkFloor>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgFloor>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgFloor>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsFloor>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhFloor>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpFloor>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgFloor>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkFloor>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnFloor>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvFloor>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkFloor>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgFloor>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgFloor>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsFloor>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhFloor>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpFloor>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgFloor>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkFloor>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnFloor>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvFloor>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkFloor>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgFloor>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgFloor>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

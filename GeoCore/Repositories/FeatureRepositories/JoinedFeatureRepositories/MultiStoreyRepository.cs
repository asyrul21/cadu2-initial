﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.Buildings;
using Geomatic.Core.Features.JoinedFeatures.MultiStories;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMultiStorey = Geomatic.Core.Features.JoinedFeatures.GMultiStorey;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GStreetType = Geomatic.Core.Rows.GStreetType;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class MultiStoreyRepository : JoinedFeatureRepository
    {
         protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsMultiStorey.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhMultiStorey.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpMultiStorey.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgMultiStorey.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkMultiStorey.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnMultiStorey.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvMultiStorey.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkMultiStorey.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgMultiStorey.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgMultiStorey.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string BuildingTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string PropertyTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(BuildingTableName);
                tables.Add(PropertyTableName);
                tables.Add(StreetTableName);
                tables.Add(GPropertyType.TABLE_NAME);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.FLOOR);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.APARTMENT);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.BUILDING_ID);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.GDS_ID);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.NIS_ID);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.GR_ID);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.UPDATE_STATUS);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.NAME);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.NAME2);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.TYPE);
                subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.NAME);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.STREET_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GMultiStorey.DATE_CREATED);
                // added by noraini ali
                subFields.Add("{0}.{1}", TableName, GMultiStorey.AND_STATUS);
                // added end
                subFields.Add("{0}.SHAPE", BuildingTableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GMultiStorey.BUILDING_ID, BuildingTableName);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", BuildingTableName, GBuilding.PROPERTY_ID, PropertyTableName);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", PropertyTableName, GProperty.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyTableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public MultiStoreyRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsMultiStorey>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhMultiStorey>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpMultiStorey>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgMultiStorey>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkMultiStorey>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnMultiStorey>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvMultiStorey>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkMultiStorey>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgMultiStorey>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgMultiStorey>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsMultiStorey>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhMultiStorey>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpMultiStorey>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgMultiStorey>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkMultiStorey>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnMultiStorey>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvMultiStorey>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkMultiStorey>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgMultiStorey>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgMultiStorey>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsMultiStorey>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhMultiStorey>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpMultiStorey>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgMultiStorey>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkMultiStorey>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnMultiStorey>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvMultiStorey>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkMultiStorey>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgMultiStorey>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgMultiStorey>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsMultiStorey>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhMultiStorey>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpMultiStorey>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgMultiStorey>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkMultiStorey>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnMultiStorey>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvMultiStorey>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkMultiStorey>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgMultiStorey>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgMultiStorey>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.Regions;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class RegionRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                   
                    case SegmentName.KV:
                        return GKvRegion.TABLE_NAME;
                    
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GRegion.INDEX);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                //whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.CONS_STATUS, GConstructionStatus.TABLE_NAME, GConstructionStatus.CODE);
                whereClauses.Add("");
                return whereClauses.Join(" AND ");
            }
        }

        public RegionRepository(SegmentName segmentName)
        : base(segmentName)
        {
        }

        //get by Id
        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvRegion>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        //getbyIds
        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.KV:
                    return FeatureClass.Map<GKvRegion>(ids);
                default:
                    throw new Exception("Unknown version.");
            }
        }

        //search
        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.KV:
                    return FeatureClass.Map<GKvRegion>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvRegion>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.BuildingsAND;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features.BuildingGroups;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class BuildingANDRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string PropertyTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        // noraini ali - OKT 2021 
        protected string BuildGroupTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingGroup.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingGroup.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingGroup.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingGroup.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingGroup.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingGroup.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingGroup.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingGroup.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingGroup.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingGroup.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }
        // end

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(PropertyTableName);
                tables.Add(StreetTableName);
                tables.Add(BuildGroupTableName);
                tables.Add(GConstructionStatus.TABLE_NAME);
                tables.Add(GSource.TABLE_NAME);
                tables.Add(GPropertyType.TABLE_NAME);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.CONS_STATUS);
                subFields.Add("{0}.{1}", GConstructionStatus.TABLE_NAME, GConstructionStatus.NAME);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.SOURCE);
                subFields.Add("{0}.{1}", GSource.TABLE_NAME, GSource.NAME);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.CODE);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.NAME);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.NAME2);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.PROPERTY_ID);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.GROUP_ID);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.NUM_FLOOR);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.UNIT);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.SPACE);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.AREA_ID);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.TYPE);
                subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.NAME);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.LOT);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.HOUSE);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.YEAR_INSTALL);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.NUM_OF_FLOOR);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.STREET_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.AND_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.FORECAST_NUM_UNIT);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.FORECAST_SOURCE);
                subFields.Add("{0}.{1}", TableName, GBuildingAND.ORI_ID);
                subFields.Add("{0}.{1}", BuildGroupTableName, GBuildingGroup.NAME);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GBuildingAND.CONS_STATUS, GConstructionStatus.TABLE_NAME, GConstructionStatus.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GBuildingAND.SOURCE, GSource.TABLE_NAME, GSource.CODE);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GBuildingAND.PROPERTY_ID, PropertyTableName);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", PropertyTableName, GProperty.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyTableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GBuildingAND.GROUP_ID, BuildGroupTableName);
                return whereClauses.Join(" AND ");
            }
        }

        public BuildingANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuilding>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuilding>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuilding>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuilding>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuilding>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuilding>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuilding>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuilding>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuilding>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuilding>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuilding>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuilding>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuilding>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuilding>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuilding>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuilding>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuilding>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuilding>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuilding>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuilding>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuilding>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuilding>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuilding>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuilding>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuilding>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuilding>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuilding>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuilding>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuilding>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuilding>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuilding>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuilding>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuilding>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuilding>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuilding>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuilding>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuilding>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuilding>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuilding>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuilding>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

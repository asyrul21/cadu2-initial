﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    /// <summary>
    /// Readonly
    /// </summary>
    internal abstract class JoinedFeatureRepository : FeatureRepository
    {
        /// <summary>
        /// Table names
        /// </summary>
        protected abstract string Tables { get; }

        /// <summary>
        /// Fields 
        /// </summary>
        protected abstract string SubFields { get; }

        /// <summary>
        /// Join table relationship
        /// </summary>
        protected abstract string WhereClause { get; }

        protected virtual string PrimaryKey
        {
            get { return string.Format("{0}.OBJECTID", TableName); }
        }

        protected virtual IQueryDef QueryDef
        {
            get
            {
                IQueryDef queryDef = Session.Current.Cadu.CreateQueryDef();
                queryDef.Tables = Tables;
                queryDef.SubFields = SubFields;
                queryDef.WhereClause = WhereClause;
                return queryDef;
            }
        }

        protected virtual IQueryName2 QueryName
        {
            get
            {
                IQueryName2 queryName2 = (IQueryName2)new FeatureQueryNameClass();
                queryName2.QueryDef = QueryDef;
                queryName2.PrimaryKey = PrimaryKey;
                return queryName2;
            }
        }

        /// <summary>
        /// Esri Feature Class 
        /// Default get by QueryName
        /// </summary>
        protected override IFeatureClass FeatureClass
        {
            get
            {
                return Session.Current.Cadu.OpenFeatureClass(QueryName);
            }
        }

        public JoinedFeatureRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject Insert(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void Update(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void Delete(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        // noraini - Apr 2021
        public override void UpdateGraphic(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void UpdateRemainStatus(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void UpdateInsertByAND(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void UpdateByAND(IGObject obj)
        {
            throw new Exception("Readonly.");
        }

        public override void UpdateDeleteByAND(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void UpdateGraphicByAND(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            throw new Exception("Readonly");
        }
    }
}

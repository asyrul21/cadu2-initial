﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.BuildingGroups;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GStreetType = Geomatic.Core.Rows.GStreetType;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class BuildingGroupRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingGroup.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingGroup.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingGroup.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingGroup.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingGroup.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingGroup.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingGroup.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingGroup.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingGroup.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingGroup.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(StreetTableName);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.CODE);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.NAME);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.SOURCE);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.STREET_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.BUILD_NAME_POS);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.AREA_ID);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.AND_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.NUM_UNIT);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.FORECAST_NUM_UNIT);
                subFields.Add("{0}.{1}", TableName, GBuildingGroup.FORECAST_SOURCE);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GBuildingGroup.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public BuildingGroupRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsBuildingGroup>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhBuildingGroup>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpBuildingGroup>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgBuildingGroup>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkBuildingGroup>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnBuildingGroup>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvBuildingGroup>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkBuildingGroup>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgBuildingGroup>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgBuildingGroup>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingGroup>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingGroup>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingGroup>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingGroup>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingGroup>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingGroup>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingGroup>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingGroup>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingGroup>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingGroup>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsBuildingGroup>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhBuildingGroup>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpBuildingGroup>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgBuildingGroup>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkBuildingGroup>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnBuildingGroup>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvBuildingGroup>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkBuildingGroup>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgBuildingGroup>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgBuildingGroup>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingGroup>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingGroup>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingGroup>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingGroup>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingGroup>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingGroup>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingGroup>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingGroup>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingGroup>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingGroup>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GStreetType = Geomatic.Core.Rows.GStreetType;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class PropertyRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(StreetTableName);
                tables.Add(GConstructionStatus.TABLE_NAME);
                tables.Add(GPropertyType.TABLE_NAME);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GProperty.CONS_STATUS);
                subFields.Add("{0}.{1}", GConstructionStatus.TABLE_NAME, GConstructionStatus.NAME);
                subFields.Add("{0}.{1}", TableName, GProperty.TYPE);
                subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.NAME);
                subFields.Add("{0}.{1}", TableName, GProperty.LOT);
                subFields.Add("{0}.{1}", TableName, GProperty.HOUSE);
                subFields.Add("{0}.{1}", TableName, GProperty.YEAR_INSTALL);
                subFields.Add("{0}.{1}", TableName, GProperty.NUM_OF_FLOOR);
                subFields.Add("{0}.{1}", TableName, GProperty.STREET_ID);
                subFields.Add("{0}.{1}", TableName, GProperty.SOURCE);
                subFields.Add("{0}.{1}", TableName, GProperty.AREA_ID);
                subFields.Add("{0}.{1}", TableName, GProperty.UPDATE_STATUS);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GProperty.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GProperty.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GProperty.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GProperty.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GProperty.AND_STATUS);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GProperty.CONS_STATUS, GConstructionStatus.TABLE_NAME, GConstructionStatus.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GProperty.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public PropertyRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsProperty>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhProperty>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpProperty>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgProperty>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkProperty>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnProperty>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvProperty>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkProperty>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgProperty>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgProperty>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsProperty>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhProperty>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpProperty>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgProperty>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkProperty>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnProperty>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvProperty>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkProperty>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgProperty>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgProperty>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsProperty>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhProperty>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpProperty>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgProperty>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkProperty>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnProperty>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvProperty>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkProperty>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgProperty>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgProperty>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsProperty>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhProperty>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpProperty>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgProperty>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkProperty>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnProperty>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvProperty>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkProperty>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgProperty>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgProperty>();
                default:
                    throw new Exception(string.Format("Unknown version {0}.", SegmentName));
            }
        }
    }
}

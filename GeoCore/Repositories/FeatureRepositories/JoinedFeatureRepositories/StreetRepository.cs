﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.Streets;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GStreetCategory = Geomatic.Core.Rows.GStreetCategory;
using GStreetClass = Geomatic.Core.Rows.GStreetClass;
using GStreetDesign = Geomatic.Core.Rows.GStreetDesign;
using GStreetDirection = Geomatic.Core.Rows.GStreetDirection;
using GStreetFilterLevel = Geomatic.Core.Rows.GStreetFilterLevel;
using GStreetNetworkClass = Geomatic.Core.Rows.GStreetNetworkClass;
using GStreetStatus = Geomatic.Core.Rows.GStreetStatus;
using GStreetTollType = Geomatic.Core.Rows.GStreetTollType;
using GStreetType = Geomatic.Core.Rows.GStreetType;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class StreetRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(GConstructionStatus.TABLE_NAME);
                tables.Add(GStreetStatus.TABLE_NAME);
                tables.Add(GStreetNetworkClass.TABLE_NAME);
                tables.Add(GStreetClass.TABLE_NAME);
                tables.Add(GStreetCategory.TABLE_NAME);
                tables.Add(GStreetFilterLevel.TABLE_NAME);
                tables.Add(GStreetType.TABLE_NAME);
                tables.Add(GStreetDirection.TABLE_NAME);
                tables.Add(GStreetDesign.TABLE_NAME);
                tables.Add(GStreetTollType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GStreet.CONS_STATUS);
                subFields.Add("{0}.{1}", GConstructionStatus.TABLE_NAME, GConstructionStatus.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.STATUS);
                subFields.Add("{0}.{1}", GStreetStatus.TABLE_NAME, GStreetStatus.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.NETWORK_CLASS);
                subFields.Add("{0}.{1}", GStreetNetworkClass.TABLE_NAME, GStreetNetworkClass.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.CLASS);
                subFields.Add("{0}.{1}", GStreetClass.TABLE_NAME, GStreetClass.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.CATEGORY);
                subFields.Add("{0}.{1}", GStreetCategory.TABLE_NAME, GStreetCategory.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.FILTER_LEVEL);
                subFields.Add("{0}.{1}", GStreetFilterLevel.TABLE_NAME, GStreetFilterLevel.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", TableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", TableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", TableName, GStreet.CITY);
                subFields.Add("{0}.{1}", TableName, GStreet.SUB_CITY);
                subFields.Add("{0}.{1}", TableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GStreet.DIRECTION);
                subFields.Add("{0}.{1}", GStreetDirection.TABLE_NAME, GStreetDirection.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.DESIGN);
                subFields.Add("{0}.{1}", GStreetDesign.TABLE_NAME, GStreetDesign.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.TOLL_TYPE);
                subFields.Add("{0}.{1}", GStreetTollType.TABLE_NAME, GStreetTollType.NAME);
                subFields.Add("{0}.{1}", TableName, GStreet.DIVIDER);
                subFields.Add("{0}.{1}", TableName, GStreet.SPEED_LIMIT);
                subFields.Add("{0}.{1}", TableName, GStreet.WEIGHT);
                subFields.Add("{0}.{1}", TableName, GStreet.ELEVATION);
                subFields.Add("{0}.{1}", TableName, GStreet.LENGTH);
                subFields.Add("{0}.{1}", TableName, GStreet.WIDTH);
                subFields.Add("{0}.{1}", TableName, GStreet.HEIGHT);
                subFields.Add("{0}.{1}", TableName, GStreet.LANES);
                subFields.Add("{0}.{1}", TableName, GStreet.FROM_ID);
                subFields.Add("{0}.{1}", TableName, GStreet.TO_ID);
                subFields.Add("{0}.{1}", TableName, GStreet.BICYCLE);
                subFields.Add("{0}.{1}", TableName, GStreet.BUS);
                subFields.Add("{0}.{1}", TableName, GStreet.CAR);
                subFields.Add("{0}.{1}", TableName, GStreet.DELIVERY);
                subFields.Add("{0}.{1}", TableName, GStreet.EMERGENCY);
                subFields.Add("{0}.{1}", TableName, GStreet.PEDESTRIAN);
                subFields.Add("{0}.{1}", TableName, GStreet.TRUCK);
                subFields.Add("{0}.{1}", TableName, GStreet.AREA_ID);
                subFields.Add("{0}.{1}", TableName, GStreet.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GStreet.SOURCE);
                subFields.Add("{0}.{1}", TableName, GStreet.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GStreet.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GStreet.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GStreet.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GStreet.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GStreet.AND_STATUS);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.CONS_STATUS, GConstructionStatus.TABLE_NAME, GConstructionStatus.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.STATUS, GStreetStatus.TABLE_NAME, GStreetStatus.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.NETWORK_CLASS, GStreetNetworkClass.TABLE_NAME, GStreetNetworkClass.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.CLASS, GStreetClass.TABLE_NAME, GStreetClass.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.CATEGORY, GStreetCategory.TABLE_NAME, GStreetCategory.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.FILTER_LEVEL, GStreetFilterLevel.TABLE_NAME, GStreetFilterLevel.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.DIRECTION, GStreetDirection.TABLE_NAME, GStreetDirection.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.DESIGN, GStreetDesign.TABLE_NAME, GStreetDesign.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", TableName, GStreet.TOLL_TYPE, GStreetTollType.TABLE_NAME, GStreetTollType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public StreetRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsStreet>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhStreet>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpStreet>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgStreet>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkStreet>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnStreet>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvStreet>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkStreet>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgStreet>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgStreet>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreet>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreet>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreet>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreet>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreet>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreet>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreet>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreet>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreet>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreet>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsStreet>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhStreet>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpStreet>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgStreet>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkStreet>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnStreet>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvStreet>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkStreet>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgStreet>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgStreet>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreet>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreet>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreet>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreet>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreet>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreet>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreet>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreet>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreet>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreet>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

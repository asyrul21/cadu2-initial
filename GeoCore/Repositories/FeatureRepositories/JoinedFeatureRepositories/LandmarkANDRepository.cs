﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Features.JoinedFeatures.LandmarksAND;
using Geomatic.Core.Features.Properties;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class LandmarkANDRepository : JoinedFeatureRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmark.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmark.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmark.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmark.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmark.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmark.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmark.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmark.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmark.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmark.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected string StreetTableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(StreetTableName);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.CODE);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.NAME);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.NAME2);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.STREET_ID);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.SOURCE);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.AREA_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreet.STATE);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.DATE_UPDATED);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.AND_STATUS);
                subFields.Add("{0}.{1}", TableName, GLandmarkAND.ORI_ID);
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GLandmarkAND.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreet.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public LandmarkANDRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.FindItemByOID<GAsLandmark>(id);
                case SegmentName.JH:
                    return FeatureClass.FindItemByOID<GJhLandmark>(id);
                case SegmentName.JP:
                    return FeatureClass.FindItemByOID<GJpLandmark>(id);
                case SegmentName.KG:
                    return FeatureClass.FindItemByOID<GKgLandmark>(id);
                case SegmentName.KK:
                    return FeatureClass.FindItemByOID<GKkLandmark>(id);
                case SegmentName.KN:
                    return FeatureClass.FindItemByOID<GKnLandmark>(id);
                case SegmentName.KV:
                    return FeatureClass.FindItemByOID<GKvLandmark>(id);
                case SegmentName.MK:
                    return FeatureClass.FindItemByOID<GMkLandmark>(id);
                case SegmentName.PG:
                    return FeatureClass.FindItemByOID<GPgLandmark>(id);
                case SegmentName.TG:
                    return FeatureClass.FindItemByOID<GTgLandmark>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmark>(ids);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmark>(ids);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmark>(ids);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmark>(ids);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmark>(ids);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmark>(ids);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmark>(ids);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmark>(ids);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmark>(ids);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmark>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return FeatureClass.Map<GAsLandmark>(queryFilter);
                case SegmentName.JH:
                    return FeatureClass.Map<GJhLandmark>(queryFilter);
                case SegmentName.JP:
                    return FeatureClass.Map<GJpLandmark>(queryFilter);
                case SegmentName.KG:
                    return FeatureClass.Map<GKgLandmark>(queryFilter);
                case SegmentName.KK:
                    return FeatureClass.Map<GKkLandmark>(queryFilter);
                case SegmentName.KN:
                    return FeatureClass.Map<GKnLandmark>(queryFilter);
                case SegmentName.KV:
                    return FeatureClass.Map<GKvLandmark>(queryFilter);
                case SegmentName.MK:
                    return FeatureClass.Map<GMkLandmark>(queryFilter);
                case SegmentName.PG:
                    return FeatureClass.Map<GPgLandmark>(queryFilter);
                case SegmentName.TG:
                    return FeatureClass.Map<GTgLandmark>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLandmark>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLandmark>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLandmark>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLandmark>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLandmark>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLandmark>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLandmark>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLandmark>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLandmark>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLandmark>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

﻿using Geomatic.Core.Features.JoinedFeatures;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories
{
    internal class BuildingPoiRepository : PoiRepository
    {
        protected override string Tables
        {
            get
            {
                ListStringBuilder tables = new ListStringBuilder();
                tables.Add(TableName);
                tables.Add(BuildingTableName);
                tables.Add(PropertyTableName);
                tables.Add(GPropertyType.TABLE_NAME);
                tables.Add(StreetTableName);
                tables.Add(GStreetType.TABLE_NAME);
                return tables.Join(",");
            }
        }

        protected override string SubFields
        {
            get
            {
                ListStringBuilder subFields = new ListStringBuilder();
                subFields.Add("{0}.OBJECTID", TableName);
                subFields.Add("{0}.{1}", TableName, GPoi.CODE);
                subFields.Add("{0}.{1}", TableName, GPoi.NAME);
                subFields.Add("{0}.{1}", TableName, GPoi.NAME2);
                subFields.Add("{0}.{1}", TableName, GPoi.ABB);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_NAME);
                subFields.Add("{0}.{1}", TableName, GPoi.DESCRIPTION);
                subFields.Add("{0}.{1}", TableName, GPoi.URL);
                subFields.Add("{0}.{1}", TableName, GPoi.FAMOUS);
                subFields.Add("{0}.{1}", TableName, GPoi.DISPLAY_TEXT);
                subFields.Add("{0}.{1}", TableName, GPoi.FILTER_LEVEL);
                subFields.Add("{0}.{1}", TableName, GPoi.REF_ID);
                subFields.Add("{0}.{1}", TableName, GPoi.REF_TYPE);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_STATUS);
                subFields.Add("{0}.{1}", TableName, GPoi.SOURCE);
                subFields.Add("{0}.{1}", TableName, GPoi.UPDATE_STATUS);
                subFields.Add("{0}.{1}", TableName, GPoi.FLOOR_ID);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.CODE);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.NAME);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.NAME2);
                subFields.Add("{0}.{1}", BuildingTableName, GBuilding.PROPERTY_ID);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.TYPE);
                subFields.Add("{0}.{1}", GPropertyType.TABLE_NAME, GPropertyType.NAME);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.LOT);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.HOUSE);
                subFields.Add("{0}.{1}", PropertyTableName, GProperty.STREET_ID);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.TYPE);
                subFields.Add("{0}.{1}", GStreetType.TABLE_NAME, GStreetType.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.NAME);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.NAME2);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.SECTION);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.POSTCODE);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.CITY);
                subFields.Add("{0}.{1}", StreetTableName, GStreetAND.STATE);
                subFields.Add("{0}.{1}", TableName, GPoi.CREATED_BY);
                subFields.Add("{0}.{1}", TableName, GPoi.DATE_CREATED);
                subFields.Add("{0}.{1}", TableName, GPoi.UPDATED_BY);
                subFields.Add("{0}.{1}", TableName, GPoi.DATE_UPDATED);
                // added by noraini ali
                subFields.Add("{0}.{1}", TableName, GPoi.AND_STATUS);
                // added end
                // noraini ali Aug 22 - Verification module - CADU3
                subFields.Add("{0}.{1}", TableName, GPoi.TEL_NO);
                subFields.Add("{0}.{1}", TableName, GPoi.OTHER_TEL_NO);
                subFields.Add("{0}.{1}", TableName, GPoi.TEL_NO_OWNER);
                subFields.Add("{0}.{1}", TableName, GPoi.AREA_ID);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_ID);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_FC);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_MATCH);
                subFields.Add("{0}.{1}", TableName, GPoi.NAVI_MATCH_DATE);
                // added end
                subFields.Add("{0}.SHAPE", TableName);
                return subFields.Join(",");
            }
        }

        protected override string WhereClause
        {
            get
            {
                ListStringBuilder whereClauses = new ListStringBuilder();
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", TableName, GPoi.REF_ID, BuildingTableName);
                whereClauses.Add("{0}.{1} = 3", TableName, GPoi.REF_TYPE);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", BuildingTableName, GBuilding.PROPERTY_ID, PropertyTableName);
                whereClauses.Add("{0}.{1} = {2}.OBJECTID(+)", PropertyTableName, GProperty.STREET_ID, StreetTableName);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", PropertyTableName, GProperty.TYPE, GPropertyType.TABLE_NAME, GPropertyType.CODE);
                whereClauses.Add("{0}.{1} = {2}.{3}(+)", StreetTableName, GStreetAND.TYPE, GStreetType.TABLE_NAME, GStreetType.CODE);
                return whereClauses.Join(" AND ");
            }
        }

        public BuildingPoiRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }
    }
}

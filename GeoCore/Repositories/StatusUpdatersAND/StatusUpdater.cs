﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.StatusUpdatersAND
{
    public class StatusUpdater
    {
        public virtual void Insert(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.NEW;
            nepsObj.AndStatus = Status.NEW;
        }

        public virtual void Update(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE;
            nepsObj.AndStatus = Status.UPDATE;
        }

        public virtual void UpdateDeleteByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.DELETE;
            nepsObj.AndStatus = Status.DELETE;
        }

        public virtual void UpdateInsertByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.NEW;
            nepsObj.AndStatus = Status.NEW;
        }

        public virtual void UpdateGraphicByAND(IGNepsObject nepsObj)
        {
            nepsObj.UpdateStatus = Status.UPDATE_GRAPHIC;
            nepsObj.AndStatus = Status.UPDATE_GRAPHIC;
        }

        public virtual void Delete(IGNepsObject nepsObj)
        {
            throw new Exception("Not implemented.");
        }

    }
}

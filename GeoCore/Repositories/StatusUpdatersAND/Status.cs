﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.StatusUpdatersAND
{
    class Status
    {
        public const int NEW = 1;
        public const int DELETE = 2;
        public const int UPDATE_GRAPHIC = 3;
        public const int UPDATE = 4;
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.JunctionsLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class JunctionLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsJunction.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhJunction.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpJunction.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgJunction.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkJunction.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnJunction.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvJunction.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkJunction.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgJunction.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgJunction.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public JunctionLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsJunction>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhJunction>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpJunction>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgJunction>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkJunction>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnJunction>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvJunction>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkJunction>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgJunction>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgJunction>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsJunction>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhJunction>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpJunction>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgJunction>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkJunction>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnJunction>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvJunction>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkJunction>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgJunction>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgJunction>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsJunction>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhJunction>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpJunction>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgJunction>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkJunction>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnJunction>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvJunction>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkJunction>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgJunction>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgJunction>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsJunction Junction = (GAsJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhJunction Junction = (GJhJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpJunction Junction = (GJpJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgJunction Junction = (GKgJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkJunction Junction = (GKkJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnJunction Junction = (GKnJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvJunction Junction = (GKvJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkJunction Junction = (GMkJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgJunction Junction = (GPgJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgJunction Junction = (GTgJunction)obj;
                        return Junction.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsJunction>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhJunction>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpJunction>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgJunction>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkJunction>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnJunction>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvJunction>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkJunction>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgJunction>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgJunction>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsJunction Junction = (GAsJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhJunction Junction = (GJhJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpJunction Junction = (GJpJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgJunction Junction = (GKgJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkJunction Junction = (GKkJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnJunction Junction = (GKnJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvJunction Junction = (GKvJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkJunction Junction = (GMkJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgJunction Junction = (GPgJunction)obj;
                        Junction.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgJunction Junction = (GTgJunction)obj;
                        Junction.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
       
    }
}

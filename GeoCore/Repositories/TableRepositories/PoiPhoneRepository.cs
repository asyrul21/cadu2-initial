﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.PhoneRefs;
using Geomatic.Core.Rows.Phones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PoiPhoneRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPoiPhone.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPoiPhone.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPoiPhone.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPoiPhone.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPoiPhone.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPoiPhone.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPoiPhone.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPoiPhone.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPoiPhone.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPoiPhone.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PoiPhoneRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GAsPoiPhone>(id); //ignored by asyrul
                case SegmentName.MK:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GAsPoiPhone>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.JH:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.JP:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.KG:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.KK:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.KN:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.KV:
                    return Table.Map<GAsPoiPhone>(ids); //ignored by asyrul
                case SegmentName.MK:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.PG:
                    return Table.Map<GAsPoiPhone>(ids);
                case SegmentName.TG:
                    return Table.Map<GAsPoiPhone>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPoiPhone>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhPoiPhone>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpPoiPhone>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgPoiPhone>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkPoiPhone>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnPoiPhone>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvPoiPhone>(queryFilter); //changed by asyrul
                case SegmentName.MK:
                    return Table.Map<GMkPoiPhone>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgPoiPhone>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgPoiPhone>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoiPhone phone = (GAsPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhPoiPhone phone = (GJhPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpPoiPhone phone = (GJpPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgPoiPhone phone = (GKgPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkPoiPhone phone = (GKkPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnPoiPhone phone = (GKnPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvPoiPhone phone = (GKvPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkPoiPhone phone = (GMkPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgPoiPhone phone = (GPgPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgPoiPhone phone = (GTgPoiPhone)obj;
                        return phone.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoiPhone phone = (GAsPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPoiPhone phone = (GJhPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPoiPhone phone = (GJpPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPoiPhone phone = (GKgPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPoiPhone phone = (GKkPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPoiPhone phone = (GKnPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPoiPhone phone = (GKvPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPoiPhone phone = (GMkPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPoiPhone phone = (GPgPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPoiPhone phone = (GTgPoiPhone)obj;
                        phone.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoiPhone phone = (GAsPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPoiPhone phone = (GJhPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPoiPhone phone = (GJpPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPoiPhone phone = (GKgPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPoiPhone phone = (GKkPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPoiPhone phone = (GKnPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPoiPhone phone = (GKvPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPoiPhone phone = (GMkPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPoiPhone phone = (GPgPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPoiPhone phone = (GTgPoiPhone)obj;
                        phone.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }            
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPoiPhone>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPoiPhone>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPoiPhone>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPoiPhone>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPoiPhone>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPoiPhone>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPoiPhone>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPoiPhone>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPoiPhone>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPoiPhone>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

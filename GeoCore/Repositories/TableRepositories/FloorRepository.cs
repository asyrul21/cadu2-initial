﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories.StatusUpdaters;
using Geomatic.Core.Rows;
using Geomatic.Core.Rows.Floors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class FloorRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsFloor.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhFloor.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpFloor.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgFloor.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkFloor.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnFloor.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvFloor.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkFloor.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgFloor.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgFloor.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", SegmentName));
                }
            }
        }

        public FloorRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsFloor>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhFloor>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpFloor>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgFloor>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkFloor>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnFloor>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvFloor>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkFloor>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgFloor>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgFloor>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsFloor>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhFloor>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpFloor>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgFloor>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkFloor>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnFloor>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvFloor>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkFloor>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgFloor>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgFloor>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsFloor>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhFloor>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpFloor>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgFloor>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkFloor>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnFloor>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvFloor>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkFloor>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgFloor>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgFloor>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GFloor absObj = (GFloor)obj;

            new FloorStatusUpdater().Insert(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsFloor floor = (GAsFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhFloor floor = (GJhFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpFloor floor = (GJpFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgFloor floor = (GKgFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkFloor floor = (GKkFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnFloor floor = (GKnFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvFloor floor = (GKvFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkFloor floor = (GMkFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgFloor floor = (GPgFloor)obj;
                        return floor.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgFloor floor = (GTgFloor)obj;
                        return floor.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            GFloor absObj = (GFloor)obj;

            new FloorStatusUpdater().Update(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsFloor floor = (GAsFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhFloor floor = (GJhFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpFloor floor = (GJpFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgFloor floor = (GKgFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkFloor floor = (GKkFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnFloor floor = (GKnFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvFloor floor = (GKvFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkFloor floor = (GMkFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgFloor floor = (GPgFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgFloor floor = (GTgFloor)obj;
                        floor.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GFloor absObj = (GFloor)obj;

            new FloorStatusUpdater().Delete(absObj);
           
            //if (absObj.UpdateStatus == Status.DELETE)
            //{
            //    return;
            //}

            // Noraini Ali - NOV 2021 - Update Status value to status 'DELETE' only but not delete the records
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsFloor floor = (GAsFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhFloor floor = (GJhFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpFloor floor = (GJpFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgFloor floor = (GKgFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkFloor floor = (GKkFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnFloor floor = (GKnFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvFloor floor = (GKvFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkFloor floor = (GMkFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgFloor floor = (GPgFloor)obj;
                        floor.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgFloor floor = (GTgFloor)obj;
                        floor.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsFloor>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhFloor>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpFloor>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgFloor>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkFloor>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnFloor>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvFloor>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkFloor>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgFloor>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgFloor>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

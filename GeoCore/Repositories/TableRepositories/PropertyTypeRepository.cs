﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PropertyTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GPropertyType.TABLE_NAME; }
        }

        public PropertyTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GPropertyType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GPropertyType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GPropertyType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GPropertyType propType = (GPropertyType)obj;
            return propType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GPropertyType propType = (GPropertyType)obj;
            propType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GPropertyType propType = (GPropertyType)obj;
            propType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GPropertyType>();
        }
    }
}

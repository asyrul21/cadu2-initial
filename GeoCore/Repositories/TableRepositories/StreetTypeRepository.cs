﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetType.TABLE_NAME; }
        }

        public StreetTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetType streetType = (GStreetType)obj;
            return streetType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetType streetType = (GStreetType)obj;
            streetType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetType streetType = (GStreetType)obj;
            streetType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetType>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetNetworkClassRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetNetworkClass.TABLE_NAME; }
        }

        public StreetNetworkClassRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetNetworkClass>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetNetworkClass>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetNetworkClass>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetNetworkClass networkClass = (GStreetNetworkClass)obj;
            return networkClass.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetNetworkClass networkClass = (GStreetNetworkClass)obj;
            networkClass.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetNetworkClass networkClass = (GStreetNetworkClass)obj;
            networkClass.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetNetworkClass>();
        }
    }
}

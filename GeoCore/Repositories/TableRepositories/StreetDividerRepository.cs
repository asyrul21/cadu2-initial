﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetDividerRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetDivider.TABLE_NAME; }
        }

        public StreetDividerRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetDivider>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetDivider>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetDivider>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetDivider divider = (GStreetDivider)obj;
            return divider.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetDivider divider = (GStreetDivider)obj;
            divider.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetDivider divider = (GStreetDivider)obj;
            divider.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetDivider>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal abstract class TableRepository : Repository
    {
        /// <summary>
        /// Esri Table
        /// Default get by TableName
        /// </summary>
        protected virtual ITable Table
        {
            get
            {
                return Session.Current.Cadu.OpenTable(TableName);
            }
        }

        public TableRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override int Count(IQueryFilter queryFilter)
        {
            return Table.RowCount(queryFilter);
        }

        // noraini - Apr 2021
        public override void UpdateGraphic(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateRemainStatus(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateInsertByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateDeleteByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateGraphicByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

        public override void UpdateRemainStatusByAND(IGObject obj)
        {
            throw new Exception("Not implemented.");
        }

    }
}
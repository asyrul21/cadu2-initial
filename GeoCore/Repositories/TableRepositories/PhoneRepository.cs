﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.Phones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PhoneRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPhone.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPhone.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPhone.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPhone.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPhone.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPhone.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPhone.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPhone.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPhone.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPhone.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", SegmentName));
                }
            }
        }

        public PhoneRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsPhone>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhPhone>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpPhone>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgPhone>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkPhone>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnPhone>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvPhone>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkPhone>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgPhone>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgPhone>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPhone>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhPhone>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpPhone>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgPhone>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkPhone>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnPhone>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvPhone>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkPhone>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgPhone>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgPhone>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPhone>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhPhone>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpPhone>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgPhone>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkPhone>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnPhone>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvPhone>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkPhone>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgPhone>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgPhone>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void Update(IGObject obj)
        {
            //throw new Exception("Readonly");
            // added by asyrul
            // to enable UNGEOCODE function, when deleting property
            switch (SegmentName)
            {
                case SegmentName.KV:
                {
                    GKvPhone phone = (GKvPhone)obj;
                    phone.Update();
                    break;
                }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
            // added end
        }

        public override void Delete(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsPhone>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhPhone>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpPhone>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgPhone>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkPhone>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnPhone>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvPhone>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkPhone>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgPhone>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgPhone>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

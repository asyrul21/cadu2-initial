﻿//slow. can be deleted.
using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Repositories.FeatureRepositories;
using Geomatic.Core.Repositories.StatusUpdaters;
using Geomatic.Core.Rows;
using Geomatic.Core.Rows.StreetConsiced;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetConsicedRepository : TableRepository
    //internal class StreetNameRepository : CaduFeatureRepository
    {
        protected override string TableName
        {
            //get { return GKvStreet.TABLE_NAME; }
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public StreetConsicedRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsStreetConcised>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhStreetConcised>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpStreetConcised>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgStreetConcised>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkStreetConcised>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnStreetConcised>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvStreetConcised>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkStreetConcised>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgStreetConcised>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgStreetConcised>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //return Table.FindItemByOID<GKvStreetConcised>(id);
            //return FeatureClass.FindItemByOID<GKvStreetName>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsStreetConcised>(ids); // noraini
                case SegmentName.JH:
                    return Table.Map<GJhStreetConcised>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpStreetConcised>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgStreetConcised>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkStreetConcised>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnStreetConcised>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvStreetConcised>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkStreetConcised>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgStreetConcised>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgStreetConcised>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }


            //return Table.Map<GKvStreetConsiced>(ids);
            //return FeatureClass.Map<GKvStreetName>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsStreetConcised>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhStreetConcised>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpStreetConcised>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgStreetConcised>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkStreetConcised>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnStreetConcised>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvStreetConcised>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkStreetConcised>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgStreetConcised>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgStreetConcised>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }



            //return Table.Map<GKvStreetConcised>(queryFilter);
            //return FeatureClass.Map<GKvStreetName>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(IGObject obj)
        {
            throw new NotImplementedException();
        }

        public override void Delete(IGObject obj)
        {
            throw new NotImplementedException();
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreetConcised>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreetConcised>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreetConcised>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreetConcised>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreetConcised>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreetConcised>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreetConcised>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreetConcised>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreetConcised>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreetConcised>();
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }

            //return Activator.CreateInstance<GKvStreetConsiced>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.LandmarksLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class LandmarkLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsLandmark.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhLandmark.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpLandmark.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgLandmark.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkLandmark.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnLandmark.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvLandmark.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkLandmark.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgLandmark.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgLandmark.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public LandmarkLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsLandmark>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhLandmark>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpLandmark>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgLandmark>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkLandmark>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnLandmark>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvLandmark>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkLandmark>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgLandmark>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgLandmark>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsLandmark>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhLandmark>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpLandmark>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgLandmark>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkLandmark>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnLandmark>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvLandmark>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkLandmark>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgLandmark>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgLandmark>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsLandmark>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhLandmark>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpLandmark>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgLandmark>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkLandmark>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnLandmark>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvLandmark>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkLandmark>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgLandmark>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgLandmark>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmark Landmark = (GAsLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhLandmark Landmark = (GJhLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpLandmark Landmark = (GJpLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgLandmark Landmark = (GKgLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkLandmark Landmark = (GKkLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnLandmark Landmark = (GKnLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvLandmark Landmark = (GKvLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkLandmark Landmark = (GMkLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgLandmark Landmark = (GPgLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgLandmark Landmark = (GTgLandmark)obj;
                        return Landmark.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsLandmark>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhLandmark>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpLandmark>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgLandmark>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkLandmark>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnLandmark>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvLandmark>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkLandmark>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgLandmark>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgLandmark>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsLandmark Landmark = (GAsLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhLandmark Landmark = (GJhLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpLandmark Landmark = (GJpLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgLandmark Landmark = (GKgLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkLandmark Landmark = (GKkLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnLandmark Landmark = (GKnLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvLandmark Landmark = (GKvLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkLandmark Landmark = (GMkLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgLandmark Landmark = (GPgLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgLandmark Landmark = (GTgLandmark)obj;
                        Landmark.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
       
    }
}

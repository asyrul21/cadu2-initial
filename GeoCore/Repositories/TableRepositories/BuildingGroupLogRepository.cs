﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.BuildingGroupsLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class BuildingGroupLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuildingGroup.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuildingGroup.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuildingGroup.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuildingGroup.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuildingGroup.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuildingGroup.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuildingGroup.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuildingGroup.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuildingGroup.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuildingGroup.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingGroupLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsBuildingGroup>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhBuildingGroup>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpBuildingGroup>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgBuildingGroup>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkBuildingGroup>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnBuildingGroup>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvBuildingGroup>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkBuildingGroup>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgBuildingGroup>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgBuildingGroup>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsBuildingGroup>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhBuildingGroup>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpBuildingGroup>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgBuildingGroup>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkBuildingGroup>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnBuildingGroup>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvBuildingGroup>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkBuildingGroup>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgBuildingGroup>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgBuildingGroup>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsBuildingGroup>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhBuildingGroup>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpBuildingGroup>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgBuildingGroup>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkBuildingGroup>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnBuildingGroup>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvBuildingGroup>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkBuildingGroup>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgBuildingGroup>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgBuildingGroup>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroup BuildingGroup = (GAsBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroup BuildingGroup = (GJhBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroup BuildingGroup = (GJpBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroup BuildingGroup = (GKgBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroup BuildingGroup = (GKkBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroup BuildingGroup = (GKnBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroup BuildingGroup = (GKvBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroup BuildingGroup = (GMkBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroup BuildingGroup = (GPgBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroup BuildingGroup = (GTgBuildingGroup)obj;
                        return BuildingGroup.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuildingGroup>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuildingGroup>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuildingGroup>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuildingGroup>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuildingGroup>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuildingGroup>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuildingGroup>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuildingGroup>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuildingGroup>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuildingGroup>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuildingGroup BuildingGroup = (GAsBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuildingGroup BuildingGroup = (GJhBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuildingGroup BuildingGroup = (GJpBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuildingGroup BuildingGroup = (GKgBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuildingGroup BuildingGroup = (GKkBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuildingGroup BuildingGroup = (GKnBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuildingGroup BuildingGroup = (GKvBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuildingGroup BuildingGroup = (GMkBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuildingGroup BuildingGroup = (GPgBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuildingGroup BuildingGroup = (GTgBuildingGroup)obj;
                        BuildingGroup.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
       
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class UserGroupRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GUserGroup.TABLE_NAME; }
        }

        public UserGroupRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GUserGroup>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GUserGroup>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GUserGroup>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GUserGroup userGroup = (GUserGroup)obj;
            return userGroup.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GUserGroup userGroup = (GUserGroup)obj;
            userGroup.Update();
        }

        public override void Delete(IGObject obj)
        {
            GUserGroup userGroup = (GUserGroup)obj;
            userGroup.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GUserGroup>();
        }
    }
}

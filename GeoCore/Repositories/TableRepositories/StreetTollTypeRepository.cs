﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetTollTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetTollType.TABLE_NAME; }
        }

        public StreetTollTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetTollType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetTollType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetTollType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetTollType tollType = (GStreetTollType)obj;
            return tollType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetTollType tollType = (GStreetTollType)obj;
            tollType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetTollType tollType = (GStreetTollType)obj;
            tollType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetTollType>();
        }
    }
}

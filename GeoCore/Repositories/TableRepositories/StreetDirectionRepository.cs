﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetDirectionRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetDirection.TABLE_NAME; }
        }

        public StreetDirectionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetDirection>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetDirection>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetDirection>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetDirection direction = (GStreetDirection)obj;
            return direction.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetDirection direction = (GStreetDirection)obj;
            direction.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetDirection direction = (GStreetDirection)obj;
            direction.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetDirection>();
        }
    }
}

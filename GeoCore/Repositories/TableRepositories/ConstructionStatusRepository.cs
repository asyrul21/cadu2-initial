﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class ConstructionStatusRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GConstructionStatus.TABLE_NAME; }
        }

        public ConstructionStatusRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GConstructionStatus>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GConstructionStatus>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GConstructionStatus>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GConstructionStatus status = (GConstructionStatus)obj;
            return status.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GConstructionStatus status = (GConstructionStatus)obj;
            status.Update();
        }

        public override void Delete(IGObject obj)
        {
            GConstructionStatus status = (GConstructionStatus)obj;
            status.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GConstructionStatus>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class TollRouteTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GTollRouteType.TABLE_NAME; }
        }

        public TollRouteTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GTollRouteType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GTollRouteType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GTollRouteType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GTollRouteType tollRouteType = (GTollRouteType)obj;
            return tollRouteType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GTollRouteType tollRouteType = (GTollRouteType)obj;
            tollRouteType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GTollRouteType tollRouteType = (GTollRouteType)obj;
            tollRouteType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GTollRouteType>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class JunctionTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GJunctionType.TABLE_NAME; }
        }

        public JunctionTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GJunctionType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GJunctionType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GJunctionType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GJunctionType junctionType = (GJunctionType)obj;
            return junctionType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GJunctionType junctionType = (GJunctionType)obj;
            junctionType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GJunctionType junctionType = (GJunctionType)obj;
            junctionType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GJunctionType>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PoiCode2Repository : TableRepository
    {
        protected override string TableName
        {
            get { return GCode2.TABLE_NAME; }
        }

        public PoiCode2Repository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GCode2>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GCode2>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GCode2>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GCode2 code = (GCode2)obj;
            return code.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GCode2 code = (GCode2)obj;
            code.Update();
        }

        public override void Delete(IGObject obj)
        {
            GCode2 code = (GCode2)obj;
            code.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GCode2>();
        }
    }
}

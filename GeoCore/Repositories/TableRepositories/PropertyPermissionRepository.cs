﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PropertyPermissionRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GPropertyPermission.TABLE_NAME; }
        }

        public PropertyPermissionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GPropertyPermission>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GPropertyPermission>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GPropertyPermission>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GPropertyPermission propType = (GPropertyPermission)obj;
            return propType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GPropertyPermission propType = (GPropertyPermission)obj;
            propType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GPropertyPermission propType = (GPropertyPermission)obj;
            propType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GPropertyPermission>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class UserRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GUser.TABLE_NAME; }
        }

        public UserRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GUser>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GUser>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GUser>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GUser user = (GUser)obj;
            return user.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GUser user = (GUser)obj;
            user.Update();
        }

        public override void Delete(IGObject obj)
        {
            GUser user = (GUser)obj;
            user.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GUser>();
        }
    }
}

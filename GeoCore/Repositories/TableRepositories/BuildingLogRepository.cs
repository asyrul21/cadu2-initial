﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.BuildingsLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class BuildingLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsBuilding.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhBuilding.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpBuilding.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgBuilding.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkBuilding.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnBuilding.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvBuilding.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkBuilding.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgBuilding.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgBuilding.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public BuildingLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsBuilding>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhBuilding>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpBuilding>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgBuilding>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkBuilding>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnBuilding>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvBuilding>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkBuilding>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgBuilding>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgBuilding>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsBuilding>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhBuilding>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpBuilding>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgBuilding>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkBuilding>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnBuilding>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvBuilding>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkBuilding>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgBuilding>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgBuilding>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsBuilding>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhBuilding>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpBuilding>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgBuilding>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkBuilding>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnBuilding>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvBuilding>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkBuilding>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgBuilding>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgBuilding>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuilding Building = (GAsBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhBuilding Building = (GJhBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpBuilding Building = (GJpBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgBuilding Building = (GKgBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkBuilding Building = (GKkBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnBuilding Building = (GKnBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvBuilding Building = (GKvBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkBuilding Building = (GMkBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgBuilding Building = (GPgBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgBuilding Building = (GTgBuilding)obj;
                        return Building.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsBuilding>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhBuilding>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpBuilding>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgBuilding>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkBuilding>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnBuilding>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvBuilding>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkBuilding>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgBuilding>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgBuilding>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsBuilding Building = (GAsBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhBuilding Building = (GJhBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpBuilding Building = (GJpBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgBuilding Building = (GKgBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkBuilding Building = (GKkBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnBuilding Building = (GKnBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvBuilding Building = (GKvBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkBuilding Building = (GMkBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgBuilding Building = (GPgBuilding)obj;
                        Building.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgBuilding Building = (GTgBuilding)obj;
                        Building.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
       
    }
}

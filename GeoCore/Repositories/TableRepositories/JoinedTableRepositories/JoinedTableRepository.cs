﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;

namespace Geomatic.Core.Repositories.TableRepositories.JoinedTableRepositories
{
    /// <summary>
    /// Readonly
    /// </summary>
    internal abstract class JoinedTableRepository : TableRepository
    {
        /// <summary>
        /// Table names
        /// </summary>
        protected abstract string Tables { get; }

        /// <summary>
        /// Fields 
        /// </summary>
        protected abstract string SubFields { get; }

        /// <summary>
        /// Join table relationship
        /// </summary>
        protected abstract string WhereClause { get; }

        protected virtual string PrimaryKey
        {
            get { return string.Format("{0}.OBJECTID", TableName); }
        }

        protected virtual IQueryDef QueryDef
        {
            get
            {
                IQueryDef queryDef = Session.Current.Cadu.CreateQueryDef();
                queryDef.Tables = Tables;
                queryDef.SubFields = SubFields;
                queryDef.WhereClause = WhereClause;
                return queryDef;
            }
        }

        protected virtual IQueryName2 QueryName
        {
            get
            {
                IQueryName2 queryName2 = (IQueryName2)new TableQueryNameClass();
                queryName2.QueryDef = QueryDef;
                queryName2.PrimaryKey = PrimaryKey;
                return queryName2;
            }
        }

        /// <summary>
        /// Esri Table 
        /// Default get by QueryName
        /// </summary>
        protected override ITable Table
        {
            get
            {
                return Session.Current.Cadu.OpenTable(QueryName);
            }
        }

        public JoinedTableRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject Insert(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void Update(IGObject obj)
        {
            throw new Exception("Readonly");
        }

        public override void Delete(IGObject obj)
        {
            throw new Exception("Readonly");
        }
    }
}

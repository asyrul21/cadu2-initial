﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PoiCode3Repository : TableRepository
    {
        protected override string TableName
        {
            get { return GCode3.TABLE_NAME; }
        }

        public PoiCode3Repository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GCode3>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GCode3>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GCode3>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GCode3 code = (GCode3)obj;
            return code.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GCode3 code = (GCode3)obj;
            code.Update();
        }

        public override void Delete(IGObject obj)
        {
            GCode3 code = (GCode3)obj;
            code.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GCode3>();
        }
    }
}

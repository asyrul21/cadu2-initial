﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetClassRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetClass.TABLE_NAME; }
        }

        public StreetClassRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetClass>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetClass>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetClass>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetClass streetClass = (GStreetClass)obj;
            return streetClass.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetClass streetClass = (GStreetClass)obj;
            streetClass.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetClass streetClass = (GStreetClass)obj;
            streetClass.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetClass>();
        }
    }
}

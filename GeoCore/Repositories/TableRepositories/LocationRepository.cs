﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class LocationRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GLocation.TABLE_NAME; }
        }

        public LocationRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GLocation>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GLocation>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GLocation>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GLocation location = (GLocation)obj;
            return location.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GLocation location = (GLocation)obj;
            location.Update();
        }

        public override void Delete(IGObject obj)
        {
            GLocation location = (GLocation)obj;
            location.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GLocation>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetCategoryRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetCategory.TABLE_NAME; }
        }

        public StreetCategoryRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetCategory>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetCategory>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetCategory>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetCategory category = (GStreetCategory)obj;
            return category.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetCategory category = (GStreetCategory)obj;
            category.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetCategory category = (GStreetCategory)obj;
            category.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetCategory>();
        }
    }
}

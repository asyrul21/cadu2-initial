﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PhoneProviderRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GPhoneProvider.TABLE_NAME; }
        }

        public PhoneProviderRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GPhoneProvider>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GPhoneProvider>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GPhoneProvider>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GPhoneProvider provider = (GPhoneProvider)obj;
            return provider.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GPhoneProvider provider = (GPhoneProvider)obj;
            provider.Update();
        }

        public override void Delete(IGObject obj)
        {
            GPhoneProvider provider = (GPhoneProvider)obj;
            provider.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GPhoneProvider>();
        }
    }
}

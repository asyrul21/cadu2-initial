﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.PropertiesLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PropertyLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsProperty.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhProperty.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpProperty.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgProperty.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkProperty.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnProperty.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvProperty.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkProperty.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgProperty.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgProperty.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PropertyLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }
        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsProperty>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhProperty>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpProperty>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgProperty>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkProperty>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnProperty>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvProperty>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkProperty>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgProperty>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgProperty>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsProperty>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhProperty>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpProperty>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgProperty>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkProperty>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnProperty>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvProperty>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkProperty>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgProperty>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgProperty>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsProperty>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhProperty>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpProperty>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgProperty>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkProperty>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnProperty>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvProperty>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkProperty>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgProperty>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgProperty>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsProperty Property = (GAsProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhProperty Property = (GJhProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpProperty Property = (GJpProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgProperty Property = (GKgProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkProperty Property = (GKkProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnProperty Property = (GKnProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvProperty Property = (GKvProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkProperty Property = (GMkProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgProperty Property = (GPgProperty)obj;
                        return Property.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgProperty Property = (GTgProperty)obj;
                        return Property.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsProperty>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhProperty>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpProperty>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgProperty>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkProperty>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnProperty>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvProperty>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkProperty>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgProperty>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgProperty>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj){ }
    }
}

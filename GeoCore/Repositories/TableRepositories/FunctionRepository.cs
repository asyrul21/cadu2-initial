﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class FunctionRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GFunction.TABLE_NAME; }
        }

        public FunctionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GFunction>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GFunction>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GFunction>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GFunction function = (GFunction)obj;
            return function.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GFunction type = (GFunction)obj;
            type.Update();
        }

        public override void Delete(IGObject obj)
        {
            GFunction type = (GFunction)obj;
            type.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GFunction>();
        }
    }
}

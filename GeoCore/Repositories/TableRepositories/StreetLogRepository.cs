﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows.StreetsLog;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetLogRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsStreet.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhStreet.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpStreet.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgStreet.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkStreet.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnStreet.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvStreet.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkStreet.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgStreet.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgStreet.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public StreetLogRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsStreet>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhStreet>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpStreet>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgStreet>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkStreet>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnStreet>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvStreet>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkStreet>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgStreet>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgStreet>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsStreet>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhStreet>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpStreet>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgStreet>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkStreet>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnStreet>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvStreet>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkStreet>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgStreet>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgStreet>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsStreet>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhStreet>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpStreet>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgStreet>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkStreet>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnStreet>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvStreet>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkStreet>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgStreet>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgStreet>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreet Street = (GAsStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhStreet Street = (GJhStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpStreet Street = (GJpStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgStreet Street = (GKgStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkStreet Street = (GKkStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnStreet Street = (GKnStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvStreet Street = (GKvStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkStreet Street = (GMkStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgStreet Street = (GPgStreet)obj;
                        return Street.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgStreet Street = (GTgStreet)obj;
                        return Street.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj){ }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsStreet>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhStreet>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpStreet>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgStreet>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkStreet>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnStreet>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvStreet>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkStreet>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgStreet>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgStreet>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsStreet Street = (GAsStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhStreet Street = (GJhStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpStreet Street = (GJpStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgStreet Street = (GKgStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkStreet Street = (GKkStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnStreet Street = (GKnStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvStreet Street = (GKvStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkStreet Street = (GMkStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgStreet Street = (GPgStreet)obj;
                        Street.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgStreet Street = (GTgStreet)obj;
                        Street.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
       
    }
}

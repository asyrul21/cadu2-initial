﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class VehicleTypeRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GVehicleType.TABLE_NAME; }
        }

        public VehicleTypeRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GVehicleType>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GVehicleType>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GVehicleType>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GVehicleType vehicleType = (GVehicleType)obj;
            return vehicleType.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GVehicleType vehicleType = (GVehicleType)obj;
            vehicleType.Update();
        }

        public override void Delete(IGObject obj)
        {
            GVehicleType vehicleType = (GVehicleType)obj;
            vehicleType.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GVehicleType>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories.StatusUpdaters;
using Geomatic.Core.Rows;
using Geomatic.Core.Rows.MultiStories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class MultiStoreyRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsMultiStorey.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhMultiStorey.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpMultiStorey.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgMultiStorey.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkMultiStorey.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnMultiStorey.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvMultiStorey.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkMultiStorey.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgMultiStorey.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgMultiStorey.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", SegmentName));
                }
            }
        }

        public MultiStoreyRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsMultiStorey>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhMultiStorey>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpMultiStorey>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgMultiStorey>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkMultiStorey>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnMultiStorey>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvMultiStorey>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkMultiStorey>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgMultiStorey>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgMultiStorey>(id);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsMultiStorey>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhMultiStorey>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpMultiStorey>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgMultiStorey>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkMultiStorey>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnMultiStorey>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvMultiStorey>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkMultiStorey>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgMultiStorey>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgMultiStorey>(ids);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsMultiStorey>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhMultiStorey>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpMultiStorey>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgMultiStorey>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkMultiStorey>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnMultiStorey>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvMultiStorey>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkMultiStorey>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgMultiStorey>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgMultiStorey>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown version. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj)
        {
            GMultiStorey absObj = (GMultiStorey)obj;

            new MultiStoreyStatusUpdater().Insert(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsMultiStorey storey = (GAsMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.JH:
                    {
                        GJhMultiStorey storey = (GJhMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.JP:
                    {
                        GJpMultiStorey storey = (GJpMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.KG:
                    {
                        GKgMultiStorey storey = (GKgMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.KK:
                    {
                        GKkMultiStorey storey = (GKkMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.KN:
                    {
                        GKnMultiStorey storey = (GKnMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.KV:
                    {
                        GKvMultiStorey storey = (GKvMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.MK:
                    {
                        GMkMultiStorey storey = (GMkMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.PG:
                    {
                        GPgMultiStorey storey = (GPgMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                case SegmentName.TG:
                    {
                        GTgMultiStorey storey = (GTgMultiStorey)obj;
                        return storey.InsertInto(Table);
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Update(IGObject obj)
        {
            GMultiStorey absObj = (GMultiStorey)obj;

            new MultiStoreyStatusUpdater().Update(absObj);

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsMultiStorey storey = (GAsMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhMultiStorey storey = (GJhMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpMultiStorey storey = (GJpMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgMultiStorey storey = (GKgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkMultiStorey storey = (GKkMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnMultiStorey storey = (GKnMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvMultiStorey storey = (GKvMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkMultiStorey storey = (GMkMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgMultiStorey storey = (GPgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgMultiStorey storey = (GTgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override void Delete(IGObject obj)
        {
            GMultiStorey absObj = (GMultiStorey)obj;

            new MultiStoreyStatusUpdater().Delete(absObj);

            //if (absObj.UpdateStatus == Status.DELETE)
            //{
            //    return;
            //}

            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsMultiStorey storey = (GAsMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhMultiStorey storey = (GJhMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpMultiStorey storey = (GJpMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgMultiStorey storey = (GKgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkMultiStorey storey = (GKkMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnMultiStorey storey = (GKnMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvMultiStorey storey = (GKvMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkMultiStorey storey = (GMkMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgMultiStorey storey = (GPgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgMultiStorey storey = (GTgMultiStorey)obj;
                        storey.Update();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject NewObj()
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Activator.CreateInstance<GAsMultiStorey>();
                case SegmentName.JH:
                    return Activator.CreateInstance<GJhMultiStorey>();
                case SegmentName.JP:
                    return Activator.CreateInstance<GJpMultiStorey>();
                case SegmentName.KG:
                    return Activator.CreateInstance<GKgMultiStorey>();
                case SegmentName.KK:
                    return Activator.CreateInstance<GKkMultiStorey>();
                case SegmentName.KN:
                    return Activator.CreateInstance<GKnMultiStorey>();
                case SegmentName.KV:
                    return Activator.CreateInstance<GKvMultiStorey>();
                case SegmentName.MK:
                    return Activator.CreateInstance<GMkMultiStorey>();
                case SegmentName.PG:
                    return Activator.CreateInstance<GPgMultiStorey>();
                case SegmentName.TG:
                    return Activator.CreateInstance<GTgMultiStorey>();
                default:
                    throw new Exception(string.Format("Unknown segment {0}.", SegmentName));
            }
        }
    }
}

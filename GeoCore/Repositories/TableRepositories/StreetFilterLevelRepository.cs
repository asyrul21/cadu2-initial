﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetFilterLevelRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetFilterLevel.TABLE_NAME; }
        }

        public StreetFilterLevelRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetFilterLevel>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetFilterLevel>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetFilterLevel>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetFilterLevel filterLevel = (GStreetFilterLevel)obj;
            return filterLevel.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetFilterLevel filterLevel = (GStreetFilterLevel)obj;
            filterLevel.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetFilterLevel filterLevel = (GStreetFilterLevel)obj;
            filterLevel.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetFilterLevel>();
        }
    }
}

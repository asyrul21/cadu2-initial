﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class UserGroupFunctionRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GUserGroupFunction.TABLE_NAME; }
        }

        public UserGroupFunctionRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GUserGroupFunction>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GUserGroupFunction>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GUserGroupFunction>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GUserGroupFunction userGroupFunction = (GUserGroupFunction)obj;
            return userGroupFunction.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GUserGroupFunction userGroupFunction = (GUserGroupFunction)obj;
            userGroupFunction.Update();
        }

        public override void Delete(IGObject obj)
        {
            GUserGroupFunction userGroupFunction = (GUserGroupFunction)obj;
            userGroupFunction.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GUserGroupFunction>();
        }
    }
}

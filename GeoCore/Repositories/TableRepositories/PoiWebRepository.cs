﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories.TableRepositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Rows.PoisWeb;
using System;
using System.Collections.Generic;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PoiWebRepository : TableRepository
    {
        protected override string TableName
        {
            get
            {
                switch (SegmentName)
                {
                    case SegmentName.AS:
                        return GAsPoi.TABLE_NAME;
                    case SegmentName.JH:
                        return GJhPoi.TABLE_NAME;
                    case SegmentName.JP:
                        return GJpPoi.TABLE_NAME;
                    case SegmentName.KG:
                        return GKgPoi.TABLE_NAME;
                    case SegmentName.KK:
                        return GKkPoi.TABLE_NAME;
                    case SegmentName.KN:
                        return GKnPoi.TABLE_NAME;
                    case SegmentName.KV:
                        return GKvPoi.TABLE_NAME;
                    case SegmentName.MK:
                        return GMkPoi.TABLE_NAME;
                    case SegmentName.PG:
                        return GPgPoi.TABLE_NAME;
                    case SegmentName.TG:
                        return GTgPoi.TABLE_NAME;
                    default:
                        throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
                }
            }
        }

        public PoiWebRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.FindItemByOID<GAsPoi>(id);
                case SegmentName.JH:
                    return Table.FindItemByOID<GJhPoi>(id);
                case SegmentName.JP:
                    return Table.FindItemByOID<GJpPoi>(id);
                case SegmentName.KG:
                    return Table.FindItemByOID<GKgPoi>(id);
                case SegmentName.KK:
                    return Table.FindItemByOID<GKkPoi>(id);
                case SegmentName.KN:
                    return Table.FindItemByOID<GKnPoi>(id);
                case SegmentName.KV:
                    return Table.FindItemByOID<GKvPoi>(id);
                case SegmentName.MK:
                    return Table.FindItemByOID<GMkPoi>(id);
                case SegmentName.PG:
                    return Table.FindItemByOID<GPgPoi>(id);
                case SegmentName.TG:
                    return Table.FindItemByOID<GTgPoi>(id);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPoi>(ids);
                case SegmentName.JH:
                    return Table.Map<GJhPoi>(ids);
                case SegmentName.JP:
                    return Table.Map<GJpPoi>(ids);
                case SegmentName.KG:
                    return Table.Map<GKgPoi>(ids);
                case SegmentName.KK:
                    return Table.Map<GKkPoi>(ids);
                case SegmentName.KN:
                    return Table.Map<GKnPoi>(ids);
                case SegmentName.KV:
                    return Table.Map<GKvPoi>(ids);
                case SegmentName.MK:
                    return Table.Map<GMkPoi>(ids);
                case SegmentName.PG:
                    return Table.Map<GPgPoi>(ids);
                case SegmentName.TG:
                    return Table.Map<GTgPoi>(ids);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    return Table.Map<GAsPoi>(queryFilter);
                case SegmentName.JH:
                    return Table.Map<GJhPoi>(queryFilter);
                case SegmentName.JP:
                    return Table.Map<GJpPoi>(queryFilter);
                case SegmentName.KG:
                    return Table.Map<GKgPoi>(queryFilter);
                case SegmentName.KK:
                    return Table.Map<GKkPoi>(queryFilter);
                case SegmentName.KN:
                    return Table.Map<GKnPoi>(queryFilter);
                case SegmentName.KV:
                    return Table.Map<GKvPoi>(queryFilter);
                case SegmentName.MK:
                    return Table.Map<GMkPoi>(queryFilter);
                case SegmentName.PG:
                    return Table.Map<GPgPoi>(queryFilter);
                case SegmentName.TG:
                    return Table.Map<GTgPoi>(queryFilter);
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }

        public override IGObject Insert(IGObject obj) { throw new Exception("Not implemented."); }

        public override void Update(IGObject obj) { throw new Exception("Not implemented."); }

        public override IGObject NewObj() { throw new Exception("Not implemented."); }

        public override void Delete(IGObject obj)
        {
            switch (SegmentName)
            {
                case SegmentName.AS:
                    {
                        GAsPoi poi = (GAsPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.JH:
                    {
                        GJhPoi poi = (GJhPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.JP:
                    {
                        GJpPoi poi = (GJpPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KG:
                    {
                        GKgPoi poi = (GKgPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KK:
                    {
                        GKkPoi poi = (GKkPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KN:
                    {
                        GKnPoi poi = (GKnPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.KV:
                    {
                        GKvPoi poi = (GKvPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.MK:
                    {
                        GMkPoi poi = (GMkPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.PG:
                    {
                        GPgPoi poi = (GPgPoi)obj;
                        poi.Delete();
                        break;
                    }
                case SegmentName.TG:
                    {
                        GTgPoi poi = (GTgPoi)obj;
                        poi.Delete();
                        break;
                    }
                default:
                    throw new Exception(string.Format("Unknown segment. {0}", SegmentName));
            }
        }
    }
}

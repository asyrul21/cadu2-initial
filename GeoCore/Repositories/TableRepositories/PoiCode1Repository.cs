﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class PoiCode1Repository : TableRepository
    {
        protected override string TableName
        {
            get { return GCode1.TABLE_NAME; }
        }

        public PoiCode1Repository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GCode1>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GCode1>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GCode1>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GCode1 code = (GCode1)obj;
            return code.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GCode1 code = (GCode1)obj;
            code.Update();
        }

        public override void Delete(IGObject obj)
        {
            GCode1 code = (GCode1)obj;
            code.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GCode1>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class TollRouteRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GTollRoute.TABLE_NAME; }
        }

        public TollRouteRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GTollRoute>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GTollRoute>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GTollRoute>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GTollRoute tollRoute = (GTollRoute)obj;
            return tollRoute.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GTollRoute tollRoute = (GTollRoute)obj;
            tollRoute.Update();
        }

        public override void Delete(IGObject obj)
        {
            GTollRoute tollRoute = (GTollRoute)obj;
            tollRoute.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GTollRoute>();
        }
    }
}

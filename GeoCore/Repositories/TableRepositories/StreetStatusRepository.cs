﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class StreetStatusRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GStreetStatus.TABLE_NAME; }
        }

        public StreetStatusRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GStreetStatus>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GStreetStatus>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GStreetStatus>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GStreetStatus status = (GStreetStatus)obj;
            return status.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GStreetStatus status = (GStreetStatus)obj;
            status.Update();
        }

        public override void Delete(IGObject obj)
        {
            GStreetStatus status = (GStreetStatus)obj;
            status.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GStreetStatus>();
        }
    }
}

﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class SourceRepository : TableRepository
    {
        protected override string TableName
        {
            get { return GSource.TABLE_NAME; }
        }

        public SourceRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GSource>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GSource>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GSource>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GSource source = (GSource)obj;
            return source.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GSource source = (GSource)obj;
            source.Update();
        }

        public override void Delete(IGObject obj)
        {
            GSource source = (GSource)obj;
            source.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GSource>();
        }
    }
}

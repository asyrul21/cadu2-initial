﻿using Earthworm;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Repositories.TableRepositories
{
    internal class TollPriceRepository : TableRepository
    {
         protected override string TableName
        {
            get { return GTollPrice.TABLE_NAME; }
        }

         public TollPriceRepository(SegmentName segmentName)
            : base(segmentName)
        {
        }

        public override IGObject GetById(int id)
        {
            return Table.FindItemByOID<GTollPrice>(id);
        }

        public override IEnumerable<IGObject> GetByIds(IEnumerable<int> ids)
        {
            return Table.Map<GTollPrice>(ids);
        }

        public override IEnumerable<IGObject> Search(IQueryFilter queryFilter)
        {
            return Table.Map<GTollPrice>(queryFilter);
        }

        public override IGObject Insert(IGObject obj)
        {
            GTollPrice tollPrice = (GTollPrice)obj;
            return tollPrice.InsertInto(Table);
        }

        public override void Update(IGObject obj)
        {
            GTollPrice tollPrice = (GTollPrice)obj;
            tollPrice.Update();
        }

        public override void Delete(IGObject obj)
        {
            GTollPrice tollPrice = (GTollPrice)obj;
            tollPrice.Delete();
        }

        public override IGObject NewObj()
        {
            return Activator.CreateInstance<GTollPrice>();
        }
    }
}

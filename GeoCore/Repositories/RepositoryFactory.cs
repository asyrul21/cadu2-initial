﻿using ESRI.ArcGIS.ADF;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Features.DeletedFeatures;
using Geomatic.Core.Repositories.FeatureRepositories;
using Geomatic.Core.Repositories.FeatureRepositories.DeletedFeatureRepositories;
using Geomatic.Core.Repositories.FeatureRepositories.TextRepositories;
using Geomatic.Core.Repositories.TableRepositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using GJoinedBuilding = Geomatic.Core.Features.JoinedFeatures.GBuilding;
using GJoinedBuildingGroup = Geomatic.Core.Features.JoinedFeatures.GBuildingGroup;
using GJoinedFloor = Geomatic.Core.Features.JoinedFeatures.GFloor;
using GJoinedJunction = Geomatic.Core.Features.JoinedFeatures.GJunction;
using GJoinedLandmark = Geomatic.Core.Features.JoinedFeatures.GLandmark;
using GJoinedMultiStorey = Geomatic.Core.Features.JoinedFeatures.GMultiStorey;
using GJoinedPoi = Geomatic.Core.Features.JoinedFeatures.GPoi;
using GJoinedProperty = Geomatic.Core.Features.JoinedFeatures.GProperty;
using GJoinedStreet = Geomatic.Core.Features.JoinedFeatures.GStreet;
using GJoinedStreetRestriction = Geomatic.Core.Features.JoinedFeatures.GStreetRestriction;
using JoinedBuildingGroupRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.BuildingGroupRepository;
using JoinedBuildingRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.BuildingRepository;
using JoinedFloorRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.FloorRepository;
using JoinedJunctionRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.JunctionRepository;
using JoinedLandmarkRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.LandmarkRepository;
using JoinedMultiStoreyRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.MultiStoreyRepository;
using JoinedPoiRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.AllPoiRepository;
using JoinedPropertyRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.PropertyRepository;
using JoinedStreetRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.StreetRepository;
using JoinedStreetRestrictionRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.StreetRestrictionRepository;

using GJoinedRegion = Geomatic.Core.Features.JoinedFeatures.GRegion;
using JoinedRegionRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.RegionRepository;
using Geomatic.Core.Rows.StreetConsiced;

// added by Noraini Ali - CADU 2 AND Feb 2020
using GJoinedPropertyAND = Geomatic.Core.Features.JoinedFeatures.GPropertyAND;
using JoinedPropertyANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.PropertyANDRepository;

using GJoinedLandmarkAND = Geomatic.Core.Features.JoinedFeatures.GLandmarkAND;
using JoinedLandmarkANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.LandmarkANDRepository;

using GJoinedBuildingAND = Geomatic.Core.Features.JoinedFeatures.GBuildingAND;
using JoinedBuildingANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.BuildingANDRepository;

using GJoinedStreetAND = Geomatic.Core.Features.JoinedFeatures.GStreetAND;
using JoinedStreetANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.StreetANDRepository;

using GJoinedJunctionAND = Geomatic.Core.Features.JoinedFeatures.GJunctionAND;
using JoinedJunctionANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.JunctionANDRepository;

using GJoinedBuildingGroupAND = Geomatic.Core.Features.JoinedFeatures.GBuildingGroupAND;
using JoinedBuildingGroupANDRepository = Geomatic.Core.Repositories.FeatureRepositories.JoinedFeatureRepositories.BuildingGroupANDRepository;

namespace Geomatic.Core.Repositories
{
    /// <summary>
    /// Repository Factory
    /// </summary>
    public sealed class RepositoryFactory
    {
        /// <summary>
        /// An array storing list of feature & its repository
        /// </summary>
        private Dictionary<Type, Type> _library;

        /// <summary>
        /// Gets or sets the name of the segment.
        /// </summary>
        /// <value>
        /// The name of the segment.
        /// </value>
        public SegmentName SegmentName { private set; get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryFactory"/> class using default segment.
        /// </summary>
        public RepositoryFactory()
            : this(SegmentName.None)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryFactory"/> class.
        /// </summary>
        /// <param name="segmentName">Name of the segment.</param>
        public RepositoryFactory(SegmentName segmentName)
        {
            SegmentName = segmentName;

            _library = new Dictionary<Type, Type>();
            Register<GBuilding, BuildingRepository>();
            Register<GBuildingGroup, BuildingGroupRepository>();
            Register<GBuildingPolygon, BuildingPolygonRepository>();
            Register<GFloor, FloorRepository>();
            Register<GJunction, JunctionRepository>();
            Register<GJunctionType, JunctionTypeRepository>();
            Register<GLandmark, LandmarkRepository>();
            Register<GLandmarkBoundary, LandmarkBoundaryRepository>();
            Register<GMultiStorey, MultiStoreyRepository>();
            // modified by asyrul
            Register<GPhone, PhoneRepository>();
            Register<GPhoneFeature, PropertyPhoneFeatureRepository>();
            // modified end
            Register<GPoiPhone, PoiPhoneRepository>();
            Register<GProperty, PropertyRepository>();
            Register<GPropertyType, PropertyTypeRepository>();
            Register<GPropertyPermission, PropertyPermissionRepository>();
            Register<GLocation, LocationRepository>();
            Register<GSectionBoundary, SectionBoundaryRepository>();
            Register<GSource, SourceRepository>();
            Register<GStreet, StreetRepository>();
            Register<GDeletedBuilding, DeletedBuildingRepository>();
            Register<GDeletedLandmark, DeletedLandmarkRepository>();
            Register<GDeletedProperty, DeletedPropertyRepository>();
            Register<GDeletedPoi, DeletedPoiRepository>();
            Register<GDeletedJunction, DeletedJunctionRepository>();
            Register<GDeletedStreet, DeletedStreetRepository>();
            Register<GDeletedBuildingGroup, DeletedBuildingGroupRepository>();
            Register<GPoi, PoiRepository>();
            Register<GStreetRestriction, StreetRestrictionRepository>();
            Register<GJoinedStreet, JoinedStreetRepository>();
            Register<GJoinedProperty, JoinedPropertyRepository>();
            Register<GJoinedBuilding, JoinedBuildingRepository>();
            Register<GJoinedBuildingGroup, JoinedBuildingGroupRepository>();
            Register<GJoinedLandmark, JoinedLandmarkRepository>();
            Register<GJoinedJunction, JoinedJunctionRepository>();
            Register<GJoinedPoi, JoinedPoiRepository>();
            Register<GJoinedStreetRestriction, JoinedStreetRestrictionRepository>();
            Register<GJoinedFloor, JoinedFloorRepository>();
            Register<GJoinedMultiStorey, JoinedMultiStoreyRepository>();
            Register<GUser, UserRepository>();
            Register<GUserGroup, UserGroupRepository>();
            Register<GUserGroupFunction, UserGroupFunctionRepository>();
            Register<GFunction, FunctionRepository>();
            Register<GStreetType, StreetTypeRepository>();
            Register<GStreetStatus, StreetStatusRepository>();
            Register<GStreetFilterLevel, StreetFilterLevelRepository>();
            Register<GStreetDesign, StreetDesignRepository>();
            Register<GStreetDirection, StreetDirectionRepository>();
            Register<GConstructionStatus, ConstructionStatusRepository>();
            Register<GStreetNetworkClass, StreetNetworkClassRepository>();
            Register<GStreetClass, StreetClassRepository>();
            Register<GStreetCategory, StreetCategoryRepository>();
            Register<GStreetTollType, StreetTollTypeRepository>();
            Register<GStreetDivider, StreetDividerRepository>();
            Register<GStreetText, StreetTextRepository>();
            Register<GPropertyText, PropertyTextRepository>();
            Register<GBuildingText, BuildingTextRepository>();
            Register<GLandmarkText, LandmarkTextRepository>();
            Register<GCode1, PoiCode1Repository>();
            Register<GCode2, PoiCode2Repository>();
            Register<GCode3, PoiCode3Repository>();
            Register<GState, StateRepository>();
            Register<GTollRouteType, TollRouteTypeRepository>();
            Register<GTollRoute, TollRouteRepository>();
            Register<GTollPrice, TollPriceRepository>();
            Register<GVehicleType, VehicleTypeRepository>();

            //added by asyrul
            Register<GRegion, RegionRepository>();
            Register<GAsStreetConcised, StreetConsicedRepository>();
            Register<GJhStreetConcised, StreetConsicedRepository>();
            Register<GJpStreetConcised, StreetConsicedRepository>();
            Register<GKgStreetConcised, StreetConsicedRepository>();
            Register<GKnStreetConcised, StreetConsicedRepository>();
            Register<GKkStreetConcised, StreetConsicedRepository>();
            Register<GKvStreetConcised, StreetConsicedRepository>();
            Register<GMkStreetConcised, StreetConsicedRepository>();
            Register<GPgStreetConcised, StreetConsicedRepository>();
            Register<GTgStreetConcised, StreetConsicedRepository>();
            //Register<GJoinedRegion, JoinedRegionRepository>();
            //Register<GJoinedRegion, RegionRepository>();

            //added by noraini ali CADU2 AND Feature
            Register<GWorkArea, WorkAreaRepository>();
            Register<GBuildingGroupAND, BuildingGroupANDRepository>();
            Register<GBuildingAND, BuildingANDRepository>();
            Register<GPropertyAND, PropertyANDRepository>();
            Register<GLandmarkAND, LandmarkANDRepository>();
            Register<GStreetAND, StreetANDRepository>();
            Register<GJunctionAND, JunctionANDRepository>();
            Register<GLandmarkBoundaryAND, LandmarkBoundaryANDRepository>();

            Register<GJoinedPropertyAND, JoinedPropertyANDRepository>();
            Register<GJoinedLandmarkAND, JoinedLandmarkANDRepository>();
            Register<GJoinedBuildingAND, JoinedBuildingANDRepository>();
            Register<GJoinedStreetAND, JoinedStreetANDRepository>();
            Register<GJoinedJunctionAND, JoinedJunctionANDRepository>();
            Register<GJoinedBuildingGroupAND, JoinedBuildingGroupANDRepository>();

            //added by noraini
            Register<GJupem, JupemRepository>();

            //noraini - ADM Building Group text 
            Register<GBuildingGroupText, BuildingGroupTextRepository>();

            //noraini - mar 2022 - AND Feature text 
            Register<GStreetTextAND, StreetTextANDRepository>();
            Register<GPropertyTextAND, PropertyTextANDRepository>();
            Register<GBuildingTextAND, BuildingTextANDRepository>();
            Register<GLandmarkTextAND, LandmarkTextANDRepository>();
            Register<GBuildingGroupTextAND, BuildingGroupTextANDRepository>();
            // end added

            //noraini - verification for CADU3 Web 
            Register<GPoiWeb, PoiWebRepository>();

            //noraini - Log table
            Register<GPoiLog, PoiLogRepository>();
            Register<GPropertyLog, PropertyLogRepository>();
            Register<GBuildingLog, BuildingLogRepository>();
            Register<GBuildingGroupLog, BuildingGroupLogRepository>();
            Register<GStreetLog, StreetLogRepository>();
            Register<GLandmarkLog, LandmarkLogRepository>();
            //Register<GJunctionLog, JunctionLogRepository>();
        }

        private void Register<TKey, TValue>()
            where TKey : IGObject
            where TValue : IRepository
        {
            if (_library.ContainsKey(typeof(TKey)))
            {
                throw new Exception(string.Format("Duplicate key detected. {0}", typeof(TKey)));
            }
            _library.Add(typeof(TKey), typeof(TValue));
        }

        private IRepository GetRepository<T>() where T : IGObject
        {
            if (!_library.ContainsKey(typeof(T)))
            {
                throw new Exception(string.Format("Unknown persistence object type. {0}", typeof(T)));
            }

            Type TValue;
            _library.TryGetValue(typeof(T), out TValue);

            object[] args = new object[] { SegmentName };
            return (IRepository)Activator.CreateInstance(TValue, args);
        }

        public T GetById<T>(int id) where T : IGObject
        {
            IGObject obj = GetRepository<T>().GetById(id);
            return (obj == null) ? default(T) : (T)obj;
        }

        public IEnumerable<T> GetByIds<T>(IEnumerable<int> ids) where T : IGObject
        {
            return GetByIds<T>(ids, false);
        }

        public IEnumerable<T> GetByIds<T>(IEnumerable<int> ids, bool isSupportCancel) where T : IGObject
        {
            if (isSupportCancel)
            {
                using (ComReleaser comReleaser = new ComReleaser())
                {
                    ITrackCancel trackCancel = new CancelTrackerClass();
                    trackCancel.CancelOnKeyPress = true;
                    trackCancel.CancelOnClick = false;
                    trackCancel.CheckTime = 2000;
                    trackCancel.Progressor = null;
                    comReleaser.ManageLifetime(trackCancel);
                    foreach (T obj in GetRepository<T>().GetByIds(ids).Cast<T>())
                    {
                        if (!trackCancel.Continue())
                        {
                            break;
                        }
                        yield return obj;
                    }
                }
            }
            else
            {
                foreach (T obj in GetRepository<T>().GetByIds(ids).Cast<T>())
                {
                    yield return obj;
                }
            }
        }

        public int Count<T>(Query<T> query) where T : IGObject
        {
            return GetRepository<T>().Count(query.QueryFilter);
        }

        public IEnumerable<T> SpatialSearch<T>(Query<T> query) where T : IGObject
        {
            return SpatialSearch<T>(query, false);
        }

        public IEnumerable<T> SpatialSearch<T>(Query<T> query, bool isSupportCancel) where T : IGObject
        {
            return Search<T>(query.SpatialFilter, isSupportCancel, null);
        }

        public IEnumerable<T> SpatialSearch<T>(IGeometry geometry, esriSpatialRelEnum spatialRel) where T : IGObject
        {
            return SpatialSearch<T>(geometry, spatialRel, false);
        }

        public IEnumerable<T> SpatialSearch<T>(IGeometry geometry, esriSpatialRelEnum spatialRel, bool isSupportCancel) where T : IGObject
        {
            Query<T> query = new Query<T>(SegmentName);
            query.Geometry = geometry;
            query.SpatialRelation = spatialRel;
            return SpatialSearch<T>(query, isSupportCancel);
        }

        public IEnumerable<T> Search<T>() where T : IGObject
        {
            return Search<T>(null as IQueryFilter, false, null);
        }

        public IEnumerable<T> Search<T>(bool isSupportCancel) where T : IGObject
        {
            return Search<T>(null as IQueryFilter, isSupportCancel, null);
        }

        public IEnumerable<T> Search<T>(Query<T> query) where T : IGObject
        {
            return Search<T>(query, false);
        }

        public IEnumerable<T> Search<T>(Query<T> query, bool isSupportCancel) where T : IGObject
        {
            return Search<T>(query, isSupportCancel, null);
        }

        public IEnumerable<T> Search<T>(Query<T> query, bool isSupportCancel, int? limit) where T : IGObject
        {
            return Search<T>(query.QueryFilter, isSupportCancel, limit);
        }

        private IEnumerable<T> Search<T>(IQueryFilter filter, bool isSupportCancel, int? limit) where T : IGObject
        {
            if (isSupportCancel)
            {
                using (ComReleaser comReleaser = new ComReleaser())
                {
                    ITrackCancel trackCancel = new CancelTrackerClass();
                    trackCancel.CancelOnKeyPress = true;
                    trackCancel.CancelOnClick = false;
                    trackCancel.CheckTime = 2000;
                    trackCancel.Progressor = null;
                    comReleaser.ManageLifetime(trackCancel);
                    if (limit.HasValue)
                    {
                        int count = 0;
                        foreach (T obj in GetRepository<T>().Search(filter))
                        {
                            if (!trackCancel.Continue())
                            {
                                break;
                            }
                            if (count >= limit)
                            {
                                break;
                            }
                            count++;
                            yield return obj;
                        }
                    }
                    else
                    {
                        foreach (T obj in GetRepository<T>().Search(filter))
                        {
                            if (!trackCancel.Continue())
                            {
                                break;
                            }
                            yield return obj;
                        }
                    }
                }
            }
            else
            {
                foreach (T obj in GetRepository<T>().Search(filter))
                {
                    yield return obj;
                }
            }
        }

        public T Insert<T>(T obj) where T : IGObject
        {
            return Insert(obj, true);
        }

        public T Insert<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for insert.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserCreate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserCreate)obj).CreatedBy = Session.User.Name;
                    }
                    ((IUserCreate)obj).DateCreated = DateTime.Now.ToString("yyyyMMdd");
                }
                else
                {
                    if (obj.TableName.Contains("_FLOOR_") || obj.TableName.Contains("_MULTISTOREY_"))
                    {
                        if (Session.User != null)
                        {
                            ((IUserCreate)obj).CreatedBy = Session.User.Name;
                        }
                        ((IUserCreate)obj).DateCreated = DateTime.Now.ToString("yyyyMMdd");
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
                else
                {
                    if (obj.TableName.Contains("_FLOOR_") || obj.TableName.Contains("_MULTISTOREY_"))
                    {
                        if (Session.User != null)
                        {
                            ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                        }
                        ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                    }
                }
            }

            return (T)GetRepository<T>().Insert(obj);
        }

        public void Update<T>(T obj) where T : IGObject
        {
            Update(obj, true);
        }

        public void Update<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
                else
                {
                    if (obj.TableName.Contains( "_FLOOR_") || obj.TableName.Contains("_MULTISTOREY_"))
                    {
                        if (Session.User != null)
                        {
                            ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                        }
                        ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                    }
                }
            }

            GetRepository<T>().Update(obj);
        }

        // noraini ali - Sept 2020
        // Only use for Supervisor Id to do Verification Module, 
        // not to update Date & By - Maintain value as AND User
        public void UpdateExt<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }
            //GetRepository<T>().Update(obj);
            //GetRepository<T>().UpdateRemainStatus(obj);
            GetRepository<T>().UpdateRemainStatusByAND(obj);
        }
        // end

        public void Delete<T>(T obj) where T : IGObject
        {
            GetRepository<T>().Delete(obj);
        }

        public T NewObj<T>() where T : IGObject
        {
            return (T)GetRepository<T>().NewObj();
        }

        public void StartTransaction()
        {
            Session.Current.Cadu.StartTransaction();
        }

        public void StartTransaction(Action action)
        {
            Session.Current.Cadu.StartTransaction(action);
        }

        public void EndTransaction()
        {
            Session.Current.Cadu.EndTransaction();
        }

        public void AbortTransaction()
        {
            Session.Current.Cadu.AbortTransaction();
        }

        // noraini ali - April 2021 
        public void UpdateGraphic<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateGraphic(obj);
        }

        public void UpdateInsertByAND<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateInsertByAND(obj);
        }

        //  to cater update feature by AND to update status - update status
        public void UpdateByAND<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateByAND(obj);
        }
        
        //  to cater delete feature by AND to update status - delete status
        public void UpdateDeleteByAND<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateDeleteByAND(obj);
        }

        // to cater Update Graphic - graphic status
        public void UpdateGraphicByAND<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateGraphicByAND(obj);
        }

        //  to cater update data without update STATUS
        public void UpdateRemainStatus<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateRemainStatus(obj);
        }
        // end

        //  to cater update data without create logging
        public void UpdateRemainStatusByAND<T>(T obj, bool validate) where T : IGObject
        {
            if (validate)
            {
                IValidator validator = ValidatorFactory.Create(obj);
                if (validator != null)
                {
                    if (!validator.IsValid())
                    {
                        string msg = string.Join(", ", validator.GetInvalidProperties().ToArray());
                        throw new QualityControlException(string.Format("{0}, {1}\nValues not valid for update.\n{2}?", obj.TableName, obj.ToString(), msg));
                    }
                }
            }

            if (obj is IUserUpdate)
            {
                // noraini ali -  not to update for AND
                if (Session.User.GetGroup().Name != "AND")
                {
                    if (Session.User != null)
                    {
                        ((IUserUpdate)obj).UpdatedBy = Session.User.Name;
                    }
                    ((IUserUpdate)obj).DateUpdated = DateTime.Now.ToString("yyyyMMdd");
                }
            }

            GetRepository<T>().UpdateRemainStatusByAND(obj);
        }
        // end
    }
}

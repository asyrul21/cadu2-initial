﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Repositories
{
    /// <summary>
    /// Interface for the Repository
    /// </summary>
    internal interface IRepository
    {
        int Count(IQueryFilter queryFilter);

        IGObject GetById(int id);

        IEnumerable<IGObject> GetByIds(IEnumerable<int> ids);

        IEnumerable<IGObject> Search(IQueryFilter queryFilter);

        IGObject Insert(IGObject obj);

        void Update(IGObject obj);

        void Delete(IGObject obj);

        IGObject NewObj();

        // noraini - Apr 2021 
        void UpdateGraphic(IGObject obj);

        void UpdateRemainStatus(IGObject obj);

        void UpdateDeleteByAND(IGObject obj);

        void UpdateGraphicByAND(IGObject obj);

        void UpdateByAND(IGObject obj);

        void UpdateInsertByAND(IGObject obj);

        void UpdateRemainStatusByAND(IGObject obj);
    }

}

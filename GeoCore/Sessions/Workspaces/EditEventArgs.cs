﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces
{
    public class EditEventArgs : WorkspaceEventArgs
    {
        public bool IsSave { private set; get; }

        public EditEventArgs(IWorkspace workspace, bool save)
            : base(workspace)
        {
            IsSave = save;
        }
    }
}

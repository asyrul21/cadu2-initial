﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces.Builders
{
    interface IWorkspaceBuilder
    {
        IWorkspace Open();
        IWorkspace Create();
        void SetPath(string path);
        void SetParameter(object obj);
    }
}

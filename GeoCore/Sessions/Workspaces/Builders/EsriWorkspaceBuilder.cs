﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces.Builders
{
    internal abstract class EsriWorkspaceBuilder : IWorkspaceBuilder
    {
        protected object _parameter;
        protected string _path;

        #region IWorkspaceBuilder Members

        public abstract IWorkspace Create();
        public abstract IWorkspace Open();

        public virtual void SetParameter(object obj)
        {
            _parameter = obj;
        }

        public virtual void SetPath(string path)
        {
            _path = path;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;

namespace Geomatic.Core.Sessions.Workspaces.Builders
{
    internal class FileGDBWorkspaceBuilder : EsriWorkspaceBuilder
    {
        protected IWorkspaceFactory2 GetWorkspace()
        {
            Type type = Type.GetTypeFromProgID("esriDataSourcesGDB.FileGDBWorkspaceFactory");
            return (IWorkspaceFactory2)Activator.CreateInstance(type);
        }

        public override IWorkspace Open()
        {
            return GetWorkspace().OpenFromFile(_path, 0);
        }

        public override IWorkspace Create()
        {
            return GetWorkspace().Open((IPropertySet)_parameter, 0);
        }
    }
}

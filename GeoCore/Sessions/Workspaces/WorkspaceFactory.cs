﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using Geomatic.Core.Sessions.Workspaces.Builders;

namespace Geomatic.Core.Sessions.Workspaces
{
    public sealed class WorkspaceFactory
    {
        public static IWorkspace Open(DataSources dataSources, string path)
        {
            IWorkspaceBuilder builder = CreateBuilder(dataSources);
            builder.SetPath(path);

            IWorkspace workspace = builder.Open();
            return workspace;
        }

        public static IWorkspace Create(DataSources dataSources, object parameter)
        {
            IWorkspaceBuilder builder = CreateBuilder(dataSources);
            builder.SetParameter(parameter);

            IWorkspace workspace = builder.Create();
            return workspace;
        }

        private static IWorkspaceBuilder CreateBuilder(DataSources dataSources)
        {
            switch (dataSources)
            {
                case DataSources.SdeWorkspaceFactory:
                    return new SdeWorkspaceBuilder();
                case DataSources.AccessWorkspaceFactory:
                    return new AccessWorkspaceBuilder();
                case DataSources.SqlWorkspaceFactory:
                    return new SqlWorkspaceBuilder();
                case DataSources.ShapefileWorkspaceFactory:
                    return new ShapefileWorkspaceBuilder();
                case DataSources.FileGDBWorkspaceFactory:
                    return new FileGDBWorkspaceBuilder();
                case DataSources.RasterWorkspaceFactory:
                    return new RasterWorkspaceBuilder();
                default:
                    throw new ArgumentException("Invalid workspace type.");
            }
        }
    }
}

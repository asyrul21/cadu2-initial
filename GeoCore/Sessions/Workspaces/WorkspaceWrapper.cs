﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces
{
    internal class WorkspaceWrapper
    {
        public event EventHandler<EditEventArgs> StartEditing;
        public event EventHandler<EditEventArgs> StopEditing;
        public event EventHandler<WorkspaceEventArgs> StartOperation;
        public event EventHandler<WorkspaceEventArgs> StopOperation;
        public event EventHandler<WorkspaceEventArgs> AbortOperation;
        public event EventHandler<WorkspaceEventArgs> UndoOperation;
        public event EventHandler<WorkspaceEventArgs> RedoOperation;
        public event EventHandler<WorkspaceEventArgs> Refreshed;
        public event EventHandler<WorkspaceEventArgs> ConflictsDetected;

        public IWorkspace Workspace { private set; get; }

        public WorkspaceWrapper(IWorkspace workspace)
        {
            Workspace = workspace;
            if (workspace is IWorkspaceEditEvents_Event)
            {
                IWorkspaceEditEvents_Event workspaceEditEvent = (IWorkspaceEditEvents_Event)workspace;
                workspaceEditEvent.OnStartEditing += new IWorkspaceEditEvents_OnStartEditingEventHandler(OnStartEditing);
                workspaceEditEvent.OnStopEditing += new IWorkspaceEditEvents_OnStopEditingEventHandler(OnStopEditing);
                workspaceEditEvent.OnStartEditOperation += new IWorkspaceEditEvents_OnStartEditOperationEventHandler(OnStartOperation);
                workspaceEditEvent.OnStopEditOperation += new IWorkspaceEditEvents_OnStopEditOperationEventHandler(OnStopOperation);
                workspaceEditEvent.OnAbortEditOperation += new IWorkspaceEditEvents_OnAbortEditOperationEventHandler(OnAbortOperation);
                workspaceEditEvent.OnUndoEditOperation += new IWorkspaceEditEvents_OnUndoEditOperationEventHandler(OnUndoOperation);
                workspaceEditEvent.OnRedoEditOperation += new IWorkspaceEditEvents_OnRedoEditOperationEventHandler(OnRedoOperation);
            }
            if (workspace is IVersionEvents_Event)
            {
                IVersionEvents_Event versionEvent = (IVersionEvents_Event)workspace;
                versionEvent.OnRefreshVersion += new IVersionEvents_OnRefreshVersionEventHandler(OnRefreshVersion);
                versionEvent.OnConflictsDetected += new IVersionEvents_OnConflictsDetectedEventHandler(OnConflictsDetected);
            }
        }

        private void OnStartEditing(bool save)
        {
            if (StartEditing != null)
                StartEditing(this, new EditEventArgs(Workspace, save));
        }

        private void OnStopEditing(bool save)
        {
            if (StopEditing != null)
                StopEditing(this, new EditEventArgs(Workspace, save));
        }

        private void OnStartOperation()
        {
            WorkspaceEventArgs e = new OperationEventArgs(Workspace, Workspace.GetModifiedTableNames(esriEditDataChangesType.esriEditDataChangesWithinSession));
            if (StartOperation != null)
                StartOperation(this, e);
        }

        private void OnStopOperation()
        {
            WorkspaceEventArgs e = new OperationEventArgs(Workspace, Workspace.GetModifiedTableNames(esriEditDataChangesType.esriEditDataChangesWithinSession));
            if (StopOperation != null)
                StopOperation(this, e);
        }

        private void OnAbortOperation()
        {
            WorkspaceEventArgs e = new OperationEventArgs(Workspace, Workspace.GetModifiedTableNames(esriEditDataChangesType.esriEditDataChangesWithinSession));
            if (AbortOperation != null)
                AbortOperation(this, e);
        }

        private void OnUndoOperation()
        {
            if (UndoOperation != null)
                UndoOperation(this, new WorkspaceEventArgs(Workspace));
        }

        private void OnRedoOperation()
        {
            if (RedoOperation != null)
                RedoOperation(this, new WorkspaceEventArgs(Workspace));
        }

        private void OnRefreshVersion()
        {
            if (Refreshed != null)
                Refreshed(this, new WorkspaceEventArgs(Workspace));
        }

        private void OnConflictsDetected(ref bool conflictsRemoved, ref bool errorOccurred, ref string errorString)
        {
            if (ConflictsDetected != null)
                ConflictsDetected(this, new WorkspaceEventArgs(Workspace));
        }
    }
}

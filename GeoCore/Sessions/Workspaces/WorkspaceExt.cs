﻿using Earthworm;
using Earthworm.AO;
using ESRI.ArcGIS.DataSourcesRaster;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Geomatic.Core.Sessions.Workspaces
{
    public static class WorkspaceExt
    {
        public static IWorkspace GetWorkspace(this IFeatureClass featureClass)
        {
            IDataset dataset = (IDataset)featureClass;
            return dataset.Workspace;
        }

        private static bool IsBeingEdited(this IWorkspace workspace)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            return workspaceEdit.IsBeingEdited();
        }

        private static void StartEditing(this IWorkspace workspace, bool withUndoRedo)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            workspaceEdit.StartEditing(withUndoRedo);
        }

        private static void StopEditing(this IWorkspace workspace, bool saveEdits)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            workspaceEdit.StopEditing(saveEdits);
        }

        private static bool IsInEditOperation(this IWorkspace workspace)
        {
            IWorkspaceEdit2 workspaceEdit = workspace as IWorkspaceEdit2;
            return workspaceEdit == null ? false : workspaceEdit.IsInEditOperation;
        }

        private static void StartEditOperation(this IWorkspace workspace)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            workspaceEdit.StartEditOperation();
        }

        private static void StopEditOperation(this IWorkspace workspace)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            workspaceEdit.StopEditOperation();
        }

        private static void AbortEditOperation(this IWorkspace workspace)
        {
            IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
            workspaceEdit.AbortEditOperation();
        }

        public static void StartTransaction(this IWorkspace workspace)
        {
            if (!workspace.IsBeingEdited())
            {
                workspace.StartEditing(true);
            }
            if (workspace.IsInEditOperation())
            {
                throw new InvalidOperationException("Already in edit operation.");
            }
            workspace.StartEditOperation();
        }

        public static void EndTransaction(this IWorkspace workspace)
        {
            workspace.StopEditOperation();
        }

        public static void AbortTransaction(this IWorkspace workspace)
        {
            workspace.AbortEditOperation();
        }

        /// <summary>
        /// Edit operation for undo redo.
        /// </summary>
        public static void StartTransaction(this IWorkspace workspace, Action action)
        {
            bool success = false;

            try
            {
                workspace.StartTransaction();

                action();

                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    workspace.EndTransaction();
                }
                else
                {
                    workspace.AbortTransaction();
                }
            }
        }

        public static void Save(this IWorkspace workspace, bool save)
        {
            if (!workspace.IsBeingEdited())
            {
                return;
            }

            // uncommited operation
            if (workspace.IsInEditOperation())
            {
                throw new Exception("Incomplete edit operation.");
            }

            try
            {
                workspace.StopEditing(save);
            }
            catch (COMException ex)
            {
                // Reconcile if only version redefined
                // Need to try save again                
                if (ex.ErrorCode == (int)fdoError.FDO_E_VERSION_REDEFINED)
                {
                    workspace.Reconcile();
                    workspace.StopEditing(save);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Reconcile
        /// Default try 3 times with interval 10 seconds
        /// </summary>
        /// <param name="workspace"></param>
        /// <returns></returns>
        public static bool Reconcile(this IWorkspace workspace)
        {
            return workspace.Reconcile(3, 10000);
        }

        public static bool Reconcile(this IWorkspace workspace, int times, int milliseconds)
        {
            if (times < 1)
            {
                throw new Exception("Need at least one time.");
            }
            int count = 1;
            while (count <= times)
            {
                try
                {
                    IVersion version = (IVersion)workspace;
                    string versionName = version.VersionName;

                    // Reconcile the version. Modify this code to reconcile and handle conflicts
                    // in a manner appropriate for the specific application.
                    IVersionEdit4 versionEdit4 = (IVersionEdit4)workspace;
                    return versionEdit4.Reconcile4(versionName, true, false, true, true);
                }
                catch (COMException ex)
                {
                    if (ex.ErrorCode == (int)fdoError.FDO_E_VERSION_BEING_RECONCILED)
                    {
                        // if reached number of tries throw instead wait
                        if (count == times)
                        {
                            throw new WarningException("Version being reconciled. Please try save again later.");
                        }
                        else
                        {
                            Thread.Sleep(milliseconds);
                        }
                    }
                    else
                    {
                        throw;
                    }
                }
                count++;
            }
            throw new WarningException("Version being reconciled. Please try save again later.");
        }

        public static IFeatureClass OpenFeatureClass(this IWorkspace workspace, string tableName)
        {
            return ((IFeatureWorkspace)workspace).OpenFeatureClass(tableName);
        }

        public static IFeatureClass OpenFeatureClass(this IWorkspace workspace, string queryName, IQueryDef queryDef)
        {
            IFeatureWorkspace featureWorkspace = (IFeatureWorkspace)workspace;
            IFeatureDataset dataset = featureWorkspace.OpenFeatureQuery(queryName, queryDef);
            IFeatureClassContainer classContainer = (IFeatureClassContainer)dataset;
            return classContainer.get_ClassByName(queryName);
        }

        public static IFeatureClass OpenFeatureClass(this IWorkspace workspace, IQueryName2 queryName2)
        {
            IWorkspaceName workspaceName = workspace.GetWorkspaceName();

            // Set the workspace and name of the new QueryTable.
            IDatasetName datasetName = (IDatasetName)queryName2;
            datasetName.WorkspaceName = workspaceName;

            // Open the virtual table.
            IName name = (IName)queryName2;
            return (IFeatureClass)name.Open();
        }

        public static ITable OpenTable(this IWorkspace workspace, string tableName)
        {
            return ((IFeatureWorkspace)workspace).OpenTable(tableName);
        }

        public static ITable OpenTable(this IWorkspace workspace, IQueryName2 queryName2)
        {
            IWorkspaceName workspaceName = workspace.GetWorkspaceName();

            // Set the workspace and name of the new QueryTable.
            IDatasetName datasetName = (IDatasetName)queryName2;
            datasetName.WorkspaceName = workspaceName;

            // Open the virtual table.
            IName name = (IName)queryName2;
            return (ITable)name.Open();
        }

        public static IEnumerable<IDataset> GetDatasets(this IWorkspace workspace)
        {
            return workspace.GetDatasets(esriDatasetType.esriDTAny);
        }

        public static IEnumerable<IDataset> GetDatasets(this IWorkspace workspace, esriDatasetType type)
        {
            IEnumDataset enumDataset = workspace.get_Datasets(type);
            IDataset dataset;
            while ((dataset = enumDataset.Next()) != null)
            {
                yield return dataset;
            }
        }

        public static IRasterDataset OpenRaster(this IWorkspace workspace, string filename)
        {
            return ((IRasterWorkspace2)workspace).OpenRasterDataset(filename);
        }

        public static IQueryDef CreateQueryDef(this IWorkspace workspace)
        {
            IFeatureWorkspace featureWorkspace = (IFeatureWorkspace)workspace;
            return featureWorkspace.CreateQueryDef();
        }

        private static IWorkspaceName GetWorkspaceName(this IWorkspace workspace)
        {
            //Workspaces implement IDataset
            IDataset dataset = (IDataset)workspace;
            return (IWorkspaceName)dataset.FullName;
        }

        /// <summary>
        /// Check if workspace connected
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsConnected(this IWorkspace workspace)
        {
            IWorkspaceFactoryStatus workspaceFactoryStatus = workspace.WorkspaceFactory as IWorkspaceFactoryStatus;

            if (workspaceFactoryStatus == null)
                return false;

            IEnumWorkspaceStatus enumWorkspaceStatus = workspaceFactoryStatus.WorkspaceStatus;

            //Iterate through the status objects until one with a workspace matching the workspaceToCheck parameter is found.
            IWorkspaceStatus workspaceStatus;
            while ((workspaceStatus = enumWorkspaceStatus.Next()) != null)
            {
                if (object.Equals(workspace, workspaceStatus.Workspace))
                    break;
            }

            if (workspaceStatus == null)
            {
                throw new ArgumentException("Specified workspace could not be found.");
            }

            return (workspaceStatus.ConnectionStatus == esriWorkspaceConnectionStatus.esriWCSUp);
        }

        /// <summary>
        /// Reconnect workspace
        /// It's important to note that if a workspace was disconnected and reconnected, all of the
        /// workspace objects - i.e. feature classes and tables - must be reopened
        /// </summary>
        /// <param name="workspace"></param>
        /// <returns></returns>
        public static bool TryReconnect(this IWorkspace workspace, out IWorkspace newWorkspace)
        {
            newWorkspace = null;
            if (workspace.IsConnected())
            {
                return false;
            }

            IWorkspaceFactoryStatus workspaceFactoryStatus = workspace.WorkspaceFactory as IWorkspaceFactoryStatus;

            if (workspaceFactoryStatus == null)
                return false;

            IEnumWorkspaceStatus enumWorkspaceStatus = workspaceFactoryStatus.WorkspaceStatus;

            //Iterate through the status objects until one with a workspace matching the workspaceToCheck parameter is found.
            IWorkspaceStatus workspaceStatus;
            while ((workspaceStatus = enumWorkspaceStatus.Next()) != null)
            {
                if (object.Equals(workspace, workspaceStatus.Workspace))
                    break;
            }

            if (workspaceStatus == null)
            {
                throw new ArgumentException("Specified workspace could not be found.");
            }

            IWorkspaceStatus pingedStatus = workspaceFactoryStatus.PingWorkspaceStatus(workspace);
            if (pingedStatus.ConnectionStatus == esriWorkspaceConnectionStatus.esriWCSAvailable)
            {
                newWorkspace = workspaceFactoryStatus.OpenAvailableWorkspace(pingedStatus);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if version has edits
        /// </summary>
        /// <returns></returns>
        public static bool HasEdits(this IWorkspace workspace)
        {
            bool hasEdits = false;
            IWorkspaceEdit workspaceEdit = workspace as IWorkspaceEdit;
            if (workspaceEdit != null)
            {
                workspaceEdit.HasEdits(ref hasEdits);
            }
            return hasEdits;
        }

        /// <summary>
        /// Check if version has undos
        /// </summary>
        /// <returns></returns>
        public static bool HasUndos(this IWorkspace workspace)
        {
            bool hasUndos = false;
            IWorkspaceEdit workspaceEdit = workspace as IWorkspaceEdit;
            if (workspaceEdit != null)
            {
                workspaceEdit.HasUndos(ref hasUndos);
            }
            return hasUndos;
        }

        /// <summary>
        /// Check if version has redos
        /// </summary>
        /// <returns></returns>
        public static bool HasRedos(this IWorkspace workspace)
        {
            bool hasRedos = false;
            IWorkspaceEdit workspaceEdit = workspace as IWorkspaceEdit;
            if (workspaceEdit != null)
            {
                workspaceEdit.HasRedos(ref hasRedos);
            }
            return hasRedos;
        }

        public static void Undo(this IWorkspace workspace)
        {
            if (workspace.HasUndos())
            {
                IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
                workspaceEdit.UndoEditOperation();
            }
        }

        public static void Redo(this IWorkspace workspace)
        {
            if (workspace.HasRedos())
            {
                IWorkspaceEdit workspaceEdit = (IWorkspaceEdit)workspace;
                workspaceEdit.RedoEditOperation();
            }
        }

        public static IEnumerable<string> GetModifiedTableNames(this IWorkspace workspace, esriEditDataChangesType changeType)
        {
            return workspace.GetModifiedTableNames(changeType, false);
        }

        public static IEnumerable<string> GetModifiedTableNames(this IWorkspace workspace, esriEditDataChangesType changeType, bool includeSchemaName)
        {
            IWorkspaceEdit2 workspaceEdit = workspace as IWorkspaceEdit2;
            if (workspaceEdit == null)
            {
                yield break;
            }
            IDataChangesEx dataChanges = workspaceEdit.get_EditDataChanges(changeType);
            IEnumBSTR enumBstr = dataChanges.ModifiedClasses;
            enumBstr.Reset();
            string name;
            while ((name = enumBstr.Next()) != null)
            {
                yield return includeSchemaName ? name : Regex.Replace(name, @"^\w+.", "", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            }
        }

        public static IEnumerable<string> GetConflictTableNames(this IWorkspace workspace)
        {
            return workspace.GetConflictTableNames(false);
        }

        public static IEnumerable<string> GetConflictTableNames(this IWorkspace workspace, bool includeSchemaName)
        {
            IVersionEdit4 versionEdit4 = (IVersionEdit4)workspace;
            IEnumConflictClass enumConflictClass = versionEdit4.ConflictClasses;
            IConflictClass conflictClass = null;
            while ((conflictClass = enumConflictClass.Next()) != null)
            {
                IDataset dataSet = conflictClass as IDataset;
                if (dataSet == null)
                {
                    continue;
                }
                string name = dataSet.Name;
                yield return includeSchemaName ? name : Regex.Replace(name, @"^\w+.", "", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            }
        }

        public static IEnumerable<int> FindInsertedIds(this IWorkspace workspace, string findingTableName)
        {
            return workspace.FindEditIds(findingTableName, esriEditDataChangesType.esriEditDataChangesWithinSession, esriDifferenceType.esriDifferenceTypeInsert);
        }

        public static IEnumerable<int> FindUpdatedIds(this IWorkspace workspace, string findingTableName)
        {
            return workspace.FindEditIds(findingTableName, esriEditDataChangesType.esriEditDataChangesWithinSession, esriDifferenceType.esriDifferenceTypeUpdateNoChange);
        }

        public static IEnumerable<int> FindDeletedIds(this IWorkspace workspace, string findingTableName)
        {
            return workspace.FindEditIds(findingTableName, esriEditDataChangesType.esriEditDataChangesWithinSession, esriDifferenceType.esriDifferenceTypeDeleteNoChange);
        }

        public static IEnumerable<int> FindEditIds(this IWorkspace workspace, string tableName, esriEditDataChangesType changeType, esriDifferenceType differenceType)
        {
            IWorkspaceEdit2 workspaceEdit = workspace as IWorkspaceEdit2;
            if (workspaceEdit == null)
            {
                yield break;
            }

            IDataChangesEx dataChanges = workspaceEdit.get_EditDataChanges(changeType);

            IFIDSet2 idSet = dataChanges.get_ChangedIDs(tableName, differenceType) as IFIDSet2;
            IEnumIDs enumIds = idSet.IDs;
            int oid = -1;
            while ((oid = enumIds.Next()) != -1)
            {
                yield return oid;
            }
        }

        public static IEnumerable<IVersion> Versions(this IWorkspace workspace)
        {
            if (workspace.Type != esriWorkspaceType.esriRemoteDatabaseWorkspace)
            {
                throw new Exception("Unsupported workspace type.");
            }

            IVersion version = (IVersion)workspace;
            IEnumVersionInfo enumVersionInfo = version.VersionInfo.Children;
            IVersionInfo versionInfo = null;
            while ((versionInfo = enumVersionInfo.Next()) != null)
            {
                yield return workspace.FindVersion(versionInfo.VersionName);
            }
        }

        public static IVersion FindVersion(this IWorkspace workspace, string versionName)
        {
            if (workspace.Type != esriWorkspaceType.esriRemoteDatabaseWorkspace)
            {
                throw new Exception("Unsupported workspace type.");
            }

            IVersionedWorkspace versionedWorkspace = (IVersionedWorkspace)workspace;
            return versionedWorkspace.FindVersion(versionName);
        }

        public static IVersion CreateVersion(this IWorkspace workspace, string newName)
        {
            IVersion version = (IVersion)workspace;
            return version.CreateVersion(newName);
        }

        public static void RefreshVersion(this IWorkspace workspace)
        {
            IVersion version = workspace as IVersion;
            if (version != null)
            {
                version.RefreshVersion();
            }
        }
    }
}

﻿namespace Geomatic.Core.Sessions.Workspaces
{
    public enum DataSources
    {
        SdeWorkspaceFactory = 0,
        AccessWorkspaceFactory,
        SqlWorkspaceFactory,
        ShapefileWorkspaceFactory,
        FileGDBWorkspaceFactory,
        RasterWorkspaceFactory
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces
{
    public class OperationEventArgs : WorkspaceEventArgs
    {
        public IEnumerable<string> Names { protected set; get; }

        public OperationEventArgs(IWorkspace workspace, IEnumerable<string> names)
            : base(workspace)
        {
            Names = names;
        }
    }
}

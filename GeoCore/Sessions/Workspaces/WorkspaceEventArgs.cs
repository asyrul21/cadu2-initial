﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Sessions.Workspaces
{
    public class WorkspaceEventArgs : EventArgs
    {
        public IWorkspace Workspace { protected set; get; }

        public WorkspaceEventArgs(IWorkspace workspace)
        {
            Workspace = workspace;
        }
    }
}

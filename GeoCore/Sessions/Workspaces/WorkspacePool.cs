﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using Geomatic.Core.Config;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Sessions.Workspaces
{
    public sealed class WorkspacePool
    {
        public event EventHandler<EditEventArgs> StartEditing;
        public event EventHandler<EditEventArgs> StopEditing;
        public event EventHandler<WorkspaceEventArgs> StartOperation;
        public event EventHandler<WorkspaceEventArgs> StopOperation;
        public event EventHandler<WorkspaceEventArgs> AbortOperation;
        public event EventHandler<WorkspaceEventArgs> UndoOperation;
        public event EventHandler<WorkspaceEventArgs> RedoOperation;
        public event EventHandler<WorkspaceEventArgs> ConflictsDetected;
        public event EventHandler<WorkspaceChangedEventArgs> WorkspaceChanged;

        private static WorkspacePool _instance;
        private Dictionary<string, WorkspaceWrapper> _workspaceWrappers;
        private IWorkspace _caduWorkspace;
        private IWorkspace _clbWorkspace;
        private IWorkspace _previousCaduWorkspace;

        /// <summary>
        /// Get cadu remote workspace.
        /// must be established and set to current first.
        /// </summary>
        public IWorkspace Cadu
        {
            private set
            {
                _previousCaduWorkspace = _caduWorkspace;
                _caduWorkspace = value;
                OnWorkspaceChanged();
            }
            get
            {
                if (!_caduWorkspace.IsConnected())
                {
                    throw new NetworkException(_caduWorkspace);
                }
                return _caduWorkspace;
            }
        }

        /// <summary>
        /// Get clb remote workspace
        /// </summary>
        public IWorkspace Clb
        {
            private set
            {
                _clbWorkspace = value;
            }
            get
            {
                if (!_clbWorkspace.IsConnected())
                {
                    throw new NetworkException(_clbWorkspace);
                }
                return _clbWorkspace;
            }
        }

        public static WorkspacePool Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WorkspacePool();
                }
                return _instance;
            }
        }

        /// <summary>
        /// A class to manage workspace and listen to workspace event.
        /// Use this class to make sure all workspaces are properly opened and closed.
        /// </summary>
        private WorkspacePool()
        {
            _workspaceWrappers = new Dictionary<string, WorkspaceWrapper>();
        }

        /// <summary>
        /// Open cadu workspace by pre-defined name
        /// </summary>
        /// <param name="versionName"></param>
        /// <param name="setCurrent"></param>
        /// <returns></returns>
        public IWorkspace OpenCadu(VersionName versionName, bool setCurrent)
        {
            switch (versionName)
            {
                case VersionName.AS:
                    return OpenCadu(StringVersionName.AS, setCurrent);
                case VersionName.JH:
                    return OpenCadu(StringVersionName.JH, setCurrent);
                case VersionName.JP:
                    return OpenCadu(StringVersionName.JP, setCurrent);
                case VersionName.KG:
                    return OpenCadu(StringVersionName.KG, setCurrent);
                case VersionName.KK:
                    return OpenCadu(StringVersionName.KK, setCurrent);
                case VersionName.KN:
                    return OpenCadu(StringVersionName.KN, setCurrent);
                case VersionName.KV:
                    return OpenCadu(StringVersionName.KV, setCurrent);
                case VersionName.MK:
                    return OpenCadu(StringVersionName.MK, setCurrent);
                case VersionName.PG:
                    return OpenCadu(StringVersionName.PG, setCurrent);
                case VersionName.TG:
                    return OpenCadu(StringVersionName.TG, setCurrent);
                case VersionName.DEFAULT:
                    return OpenCadu(StringVersionName.DEFAULT, setCurrent);
                default:
                    throw new Exception(string.Format("Unknown version name. {0}", versionName));
            }
        }

        public IWorkspace OpenCadu(string name)
        {
            return OpenCadu(name, false);
        }

        public IWorkspace OpenCadu(string name, bool setCurrent)
        {
            IWorkspace workspace;
            if (!_workspaceWrappers.ContainsKey(name))
            {
                if (name == StringVersionName.DEFAULT)
                {
                    workspace = OpenRemoteWorkspace("cadu");
                }
                else
                {
                    if (Cadu == null)
                    {
                        throw new Exception("No connection established.");
                    }
                    workspace = Cadu.FindVersion(name) as IWorkspace;
                    if (workspace == null)
                    {
                        throw new Exception(string.Format("Cannot find version {0}", name));
                    }
                    WorkspaceWrapper wrapper = CreateWrapper(workspace);
                    _workspaceWrappers.Add(name, wrapper);
                }
            }
            else
            {
                WorkspaceWrapper wrapper;
                if (_workspaceWrappers.TryGetValue(name, out wrapper))
                {
                    workspace = wrapper.Workspace;
                }
                else
                {
                    throw new Exception("Couldn't find workspace.");
                }
            }
            if (setCurrent)
            {
                Cadu = workspace;
            }
            return workspace;
        }

        public IWorkspace OpenClb(string name)
        {
            IWorkspace workspace;
            if (!_workspaceWrappers.ContainsKey(name))
            {
                if (name == StringVersionName.DEFAULT)
                {
                    workspace = OpenRemoteWorkspace("clb");
                }
                else
                {
                    if (Clb == null)
                    {
                        throw new Exception("No connection established.");
                    }
                    workspace = Clb.FindVersion(name) as IWorkspace;
                    if (workspace == null)
                    {
                        throw new Exception(string.Format("Cannot find version {0}", name));
                    }
                    WorkspaceWrapper wrapper = CreateWrapper(workspace);
                    _workspaceWrappers.Add(name, wrapper);
                    return workspace;
                }
            }
            else
            {
                WorkspaceWrapper wrapper;
                if (_workspaceWrappers.TryGetValue(name, out wrapper))
                {
                    workspace = wrapper.Workspace;
                }
                else
                {
                    throw new Exception("Couldn't find workspace.");
                }
            }
            return workspace;
        }

        public IWorkspace OpenRemoteWorkspace(string connectionName)
        {
            ConnectionSection section = ConnectionSection.GetSection();
            if (section == null)
            {
                throw new Exception("Connection setting not found.");
            }

            if (_workspaceWrappers.ContainsKey(connectionName))
            {
                WorkspaceWrapper wrapper;
                if (_workspaceWrappers.TryGetValue(connectionName, out wrapper))
                {
                    return wrapper.Workspace;
                }
            }

            foreach (Connection connection in section.Connections)
            {
                if (connection.Name != connectionName)
                {
                    continue;
                }
                DataSources type;
                switch (connection.Type)
                {
                    case "esriDataSourcesGDB.SdeWorkspaceFactory":
                        type = DataSources.SdeWorkspaceFactory;
                        break;
                    case "esriDataSourcesGDB.AccessWorkspaceFactory":
                        type = DataSources.AccessWorkspaceFactory;
                        break;
                    case "esriDataSourcesGDB.SqlWorkspaceFactory":
                        type = DataSources.SqlWorkspaceFactory;
                        break;
                    default:
                        throw new Exception(string.Format("Invalid workspace type. {0}", connection.Type));
                }

                IPropertySet propertySet = new PropertySet();
                foreach (ConnectionProperty connectionProperty in connection.ConnectionProperties)
                {
                    if (connectionProperty.Name == "password")
                    {
                        string password = StringUtils.Decrypt(connectionProperty.Value, "gds");
                        propertySet.SetProperty(connectionProperty.Name, password);
                    }
                    else
                    {
                        propertySet.SetProperty(connectionProperty.Name, connectionProperty.Value);
                    }
                }

                IWorkspace workspace = WorkspaceFactory.Create(type, propertySet);
                WorkspaceWrapper wrapper = CreateWrapper(workspace);
                _workspaceWrappers.Add(connectionName, wrapper);
                return workspace;
            }
            throw new Exception("Connection setting not found.");
        }

        public IWorkspace OpenFileGdbWorkspace(string path)
        {
            if (!Directory.Exists(path))
            {
                throw new Exception("Directory path not exist.");
            }

            string workspaceName = path + @"(Gdb)";

            WorkspaceWrapper wrapper;
            if (_workspaceWrappers.TryGetValue(workspaceName, out wrapper))
            {
                return wrapper.Workspace;
            }

            IPropertySet propertySet = new PropertySet();
            propertySet.SetProperty("DATABASE", path);

            IWorkspace workspace = WorkspaceFactory.Create(DataSources.FileGDBWorkspaceFactory, propertySet);
            wrapper = CreateWrapper(workspace);
            _workspaceWrappers.Add(workspaceName, wrapper);
            return workspace;
        }

        public IWorkspace OpenShapeWorkspace(string path)
        {
            path = (path.EndsWith("\\")) ? path : path + "\\";

            if (!Directory.Exists(path))
            {
                throw new Exception("Directory path not exist.");
            }

            string workspaceName = path + @"(Shape)";

            WorkspaceWrapper wrapper;
            if (_workspaceWrappers.TryGetValue(workspaceName, out wrapper))
            {
                return wrapper.Workspace;
            }

            IPropertySet propertySet = new PropertySet();
            propertySet.SetProperty("DATABASE", path);

            IWorkspace workspace = WorkspaceFactory.Create(DataSources.ShapefileWorkspaceFactory, propertySet);
            wrapper = CreateWrapper(workspace);
            _workspaceWrappers.Add(workspaceName, wrapper);
            return workspace;
        }

        public IWorkspace OpenRasterWorkspace(string path)
        {
            path = (path.EndsWith("\\")) ? path : path + "\\";

            if (!Directory.Exists(path))
            {
                throw new Exception("Directory path not exist.");
            }

            string workspaceName = path + @"(Raster)";

            WorkspaceWrapper wrapper;
            if (_workspaceWrappers.TryGetValue(workspaceName, out wrapper))
            {
                return wrapper.Workspace;
            }

            IPropertySet propertySet = new PropertySet();
            propertySet.SetProperty("DATABASE", path);

            IWorkspace workspace = WorkspaceFactory.Create(DataSources.RasterWorkspaceFactory, propertySet);
            wrapper = CreateWrapper(workspace);
            _workspaceWrappers.Add(workspaceName, wrapper);
            return workspace;
        }

        public bool HasEdits()
        {
            bool noEdits = true;
            foreach (WorkspaceWrapper wrapper in _workspaceWrappers.Values)
            {
                noEdits &= !wrapper.Workspace.HasEdits();
            }
            return !noEdits;
        }

        public bool HasUndos()
        {
            bool noUndos = true;
            foreach (WorkspaceWrapper wrapper in _workspaceWrappers.Values)
            {
                noUndos &= !wrapper.Workspace.HasUndos();
            }
            return !noUndos;
        }

        public bool HasRedos()
        {
            bool noRedos = true;
            foreach (WorkspaceWrapper wrapper in _workspaceWrappers.Values)
            {
                noRedos &= !wrapper.Workspace.HasRedos();
            }
            return !noRedos;
        }

        public void DiscardEdits()
        {
            foreach (WorkspaceWrapper wrapper in _workspaceWrappers.Values)
            {
                if (wrapper.Workspace.HasEdits())
                {
                    wrapper.Workspace.Save(false);
                    wrapper.Workspace.RefreshVersion();
                }
            }
        }

        /// <summary>
        /// Create a version from cadu workspace
        /// </summary>
        /// <param name="childVersionName"></param>
        /// <returns></returns>
        public bool CreateVersion(string childVersionName)
        {
            if (Cadu == null)
            {
                return false;
            }
            IWorkspace workspace = (IWorkspace)Cadu.CreateVersion(childVersionName);
            WorkspaceWrapper wrapper = CreateWrapper(workspace);
            _workspaceWrappers.Add(childVersionName, wrapper);
            return true;
        }

        /// <summary>
        /// Release all workspace.
        /// </summary>
        public void Close()
        {
            foreach (WorkspaceWrapper wrapper in _workspaceWrappers.Values)
            {
                Marshal.ReleaseComObject(wrapper.Workspace);
            }
            _workspaceWrappers.Clear();
            GC.Collect();
        }

        private WorkspaceWrapper CreateWrapper(IWorkspace workspace)
        {
            WorkspaceWrapper wrapper = new WorkspaceWrapper(workspace);
            wrapper.StartEditing += new EventHandler<EditEventArgs>(OnStartEditing);
            wrapper.StopEditing += new EventHandler<EditEventArgs>(OnStopEditing);
            wrapper.StartOperation += new EventHandler<WorkspaceEventArgs>(OnStartOperation);
            wrapper.StopOperation += new EventHandler<WorkspaceEventArgs>(OnStopOperation);
            wrapper.AbortOperation += new EventHandler<WorkspaceEventArgs>(OnAbortOperation);
            wrapper.UndoOperation += new EventHandler<WorkspaceEventArgs>(OnUndoOperation);
            wrapper.RedoOperation += new EventHandler<WorkspaceEventArgs>(OnRedoOperation);
            wrapper.ConflictsDetected += new EventHandler<WorkspaceEventArgs>(OnConflictsDetected);
            return wrapper;
        }

        private void OnStartEditing(object sender, EditEventArgs e)
        {
            if (StartEditing != null)
                StartEditing(this, e);
        }

        private void OnStopEditing(object sender, EditEventArgs e)
        {
            if (StopEditing != null)
                StopEditing(this, e);
        }

        private void OnStartOperation(object sender, WorkspaceEventArgs e)
        {
            if (StartOperation != null)
                StartOperation(this, e);
        }

        private void OnStopOperation(object sender, WorkspaceEventArgs e)
        {
            if (StopOperation != null)
                StopOperation(this, e);
        }

        private void OnAbortOperation(object sender, WorkspaceEventArgs e)
        {
            if (AbortOperation != null)
                AbortOperation(this, e);
        }

        private void OnUndoOperation(object sender, WorkspaceEventArgs e)
        {
            if (UndoOperation != null)
                UndoOperation(this, e);
        }

        private void OnRedoOperation(object sender, WorkspaceEventArgs e)
        {
            if (RedoOperation != null)
                RedoOperation(this, e);
        }

        private void OnConflictsDetected(object sender, WorkspaceEventArgs e)
        {
            if (ConflictsDetected != null)
                ConflictsDetected(this, e);
        }

        private void OnWorkspaceChanged()
        {
            if (WorkspaceChanged != null)
                WorkspaceChanged(this, new WorkspaceChangedEventArgs(_previousCaduWorkspace, _caduWorkspace));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions.Workspaces;

namespace Geomatic.Core.Sessions.Workspaces
{
    public class WorkspaceChangedEventArgs : WorkspaceEventArgs
    {
        public IWorkspace PreviousWorkspace { protected set; get; }
        public string Name { protected set; get; }

        public WorkspaceChangedEventArgs(IWorkspace oldWorkspace, IWorkspace workspace)
            : base(workspace)
        {
            PreviousWorkspace = oldWorkspace;
            if (workspace is IVersion)
            {
                Name = ((IVersion)workspace).VersionName;
            }
            else
            {
                Name = ((IDataset)workspace).Name;
            }
        }
    }
}

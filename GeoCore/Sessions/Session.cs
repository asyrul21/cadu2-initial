﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Sessions
{
    public sealed class Session
    {
        private Session()
        {
        }

        private static GUser _user;

        public static GUser User
        {
            get { return _user; }
            set { _user = value; }
        }

        public static WorkspacePool Current
        {
            get
            {
                return WorkspacePool.Instance;
            }
        }
    }
}

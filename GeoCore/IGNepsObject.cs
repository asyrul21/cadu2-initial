﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public interface IGNepsObject
    {
        int? UpdateStatus { set; get; }
        int? AndStatus { set; get; }
    }
}

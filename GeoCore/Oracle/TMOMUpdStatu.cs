﻿using Geomatic.Core.Rows;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Geomatic.Core.Oracle
{
    public class TMOMUpdStatus
    {
        private OracleCommand cmd;
        private OracleDBConnection cnn;
        private SegmentName _segmentName;

        public TMOMUpdStatus(SegmentName segment)
        {
            cmd = new OracleCommand();
            cnn = new OracleDBConnection();
            _segmentName = segment;

            cmd.Connection = cnn.openConnection();
        }

        public string selectTMOM(int? CMSNo)
        {
            TMOMQueryBuilder qb = new TMOMQueryBuilder(CMSNo, _segmentName, 0);
            string stmt = qb.selectDataStatement;
            cmd.CommandText = stmt;

            OracleDataReader reader = cmd.ExecuteReader();
            reader.Read();

            return reader.GetString(reader.GetOrdinal("CMS NO"));
        }

        public void UpdateStatus(int? CMSNo, int statusType)
        {
            TMOMQueryBuilder qb = new TMOMQueryBuilder(CMSNo, _segmentName, statusType);
            string stmt = qb.updateStatement;
            cmd.CommandText = stmt;

            //execute update - this process is non-undoable
            cmd.ExecuteNonQuery();
        }

        //destructor
        ~TMOMUpdStatus()
        {
            Console.WriteLine("Destroying TMOMUpdStatus");
            cnn.closeConnection();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using Oracle.ManagedDataAccess.Client;
using Geomatic.Core.Utilities;
using System.Windows.Forms;
using Geomatic.Core.Config;

namespace Geomatic.Core.Oracle
{
    public class OracleDBConnection
    {
        private string server { set; get; }
        private string db { set; get; }
        private string user { set; get; }
        private string pword { set; get; }
        private OracleConnection cnn { set; get; }

        private string connectionString
        {
            get
            {
                OracleConnectionStringBuilder cnnStrBuilder = new OracleConnectionStringBuilder();
                cnnStrBuilder.DataSource = this.server + "/" + this.db;
                cnnStrBuilder.UserID = this.user;
                cnnStrBuilder.Password = StringUtils.Decrypt(pword, "gds");
                return cnnStrBuilder.ConnectionString;
            }
        }

        public OracleDBConnection()
        {
            var oracleConnection = ConfigurationManager.GetSection("OracleConnectionConfig") as NameValueCollection;
            if (oracleConnection == null || oracleConnection.Count == 0)
            {
                throw new Exception("Oracle Connection Settings are not defined");
            }
            else
            {
                this.server = oracleConnection["server"];
                this.db = oracleConnection["db"];
                this.user = oracleConnection["user"];
                this.pword = oracleConnection["password"];
            }
        }

        public OracleConnection openConnection()
        {
            this.cnn = new OracleConnection(this.connectionString);
            this.cnn.Open();
            return cnn;
        }

        public void closeConnection()
        {
            this.cnn.Close();
        }

        public string testConnection()
        {
            string result = "";
            try
            {
                openConnection();
                Console.WriteLine("Oracle Connection Opened Successfully");
                result = "Connection Success";
            }
            catch
            {
                throw new Exception("Oracle connection fail!");
            }
            finally
            {
                closeConnection();
                Console.WriteLine("Oracle Connection Closed");
                result += " and closed successfully";
            }
            return result;
        }
    }
}

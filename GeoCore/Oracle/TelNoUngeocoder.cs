﻿using Geomatic.Core.Features;
using Geomatic.Core.Rows;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Oracle
{
    public class TelNoUngeocoder
    {
        private OracleCommand cmd;
        private OracleDBConnection cnn;
        private SegmentName _segmentName;

        public TelNoUngeocoder(SegmentName segment)
        {
            cmd = new OracleCommand();
            cnn = new OracleDBConnection();
            _segmentName = segment;

            cmd.Connection = cnn.openConnection();
        }

        public string selectTelNo(GPhone phone)
        {
            TelNoQueryBuilder qb = new TelNoQueryBuilder(phone, _segmentName);
            string stmt = qb.selectDataStatement;
            cmd.CommandText = stmt;

            OracleDataReader reader = cmd.ExecuteReader();
            reader.Read();

            return reader.GetString(reader.GetOrdinal("CUSTOMER_NAME"));
        }

        public void ungeocodeTelNo(GPhone phone)
        {
            TelNoQueryBuilder qb = new TelNoQueryBuilder(phone, _segmentName);
            string stmt = qb.ungeocodeTelNoStatement;
            cmd.CommandText = stmt;

            //execute update - this process is non-undoable
            cmd.ExecuteNonQuery();
        }

        public void transferTelNo(GPhone phone, GProperty property)
        {
            TelNoQueryBuilder qb = new TelNoQueryBuilder(phone, _segmentName);
            string stmt = qb.transferTelNoStatement(property.OID.ToString());
            cmd.CommandText = stmt;

            //execute update - this process is non-undoable
            cmd.ExecuteNonQuery();
        }

        //destructor
        ~TelNoUngeocoder()
        {
            Console.WriteLine("Destroying ungeocoder");
            cnn.closeConnection();
        }
    }
}

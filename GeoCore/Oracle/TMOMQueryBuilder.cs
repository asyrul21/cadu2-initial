﻿using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Oracle
{
    public class TMOMQueryBuilder
    {
        private int? cmsnum { get; set; }
        private SegmentName _segmentName;
        private int? newStatus { set; get; }

        private string dateUpdated
        {
            get
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }
        }

        private string tableName {
            get
            {
                return "TMOM_POINT";
            }
        }

        public string selectDataStatement
        {
            get
            {
                return $"SELECT * from {this.tableName} WHERE CMS_NO = '{this.cmsnum}'";
            }
        }

        public string updateStatement
        {
            get
            {
                string updatesql = String.Format("UPDATE {0} SET {1} WHERE {2}", tableName, getSetClause() , getWhereClause());
                return updatesql.Trim();
            }
        }

        private string getSetClause()
        {
            ListStringBuilder setClause = new ListStringBuilder();

            setClause.Add("{0}='{1}'", "STATUS", this.newStatus);  // 2-Completed, 3-Rejected
            setClause.Add("{0}='{1}'", "DATE_CLOSE", DateTime.Now);  // updated date

            return setClause.Join(",");
        }

        private string getWhereClause()
        {
            return String.Format("CMS_NO='{0}'", this.cmsnum);
        }

        //constructor
        public TMOMQueryBuilder(int? CMSNo, SegmentName segment, int statusType)
        {
            this.cmsnum = CMSNo;
            _segmentName = segment;
            this.newStatus = statusType; // 2-Completed, 3-Rejected
        }
    }
}

﻿using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Oracle
{
    public class TelNoQueryBuilder
    {
        private string dataid { get; set; }
        private SegmentName _segmentName;

        //new data
        private string newTermType { set; get; }
        private readonly string newCoorProp = "0,0";
        private readonly int newPropertyId = 0;
        private readonly string updateType = "9";
        private readonly string updatedBy = "CADUPropertyDeleted";
        
        private string dateUpdated
        {
            get
            {
                return DateTime.Now.ToString("yyyyMMdd");
            }
        }

        private string tableName {
            get
            {
                switch (_segmentName)
                {
                    case SegmentName.AS:
                        return "AS_TEL_NO";
                    case SegmentName.JH:
                        return "JH_TEL_NO";
                    case SegmentName.JP:
                        return "JP_TEL_NO";
                    case SegmentName.KG:
                        return "KG_TEL_NO";
                    case SegmentName.KK:
                        return "KK_TEL_NO";
                    case SegmentName.KN:
                        return "KN_TEL_NO";
                    case SegmentName.KV:
                        return "KV_TEL_NO";
                    case SegmentName.MK:
                        return "MK_TEL_NO";
                    case SegmentName.PG:
                        return "PG_TEL_NO";
                    case SegmentName.TG:
                        return "TG_TEL_NO";
                    default:
                        throw new Exception(string.Format("Unknown version. {0}", _segmentName));
                }
            }
        }

        //for testing purposes
        //private string newStreet = "DAMAI 2/3";        //ori data is DAMAI 2/3
        //private string newCustomer = "MAH SUN YEE";    //ori data is MAH SUN YEE
        //private string newAbb = "BAG";                 //ori data is BAG

        public string selectDataStatement
        {
            get
            {
                return $"SELECT * from {this.tableName} WHERE PHONE_NUMBER = '{this.dataid}'";
            }
        }

        public string ungeocodeTelNoStatement
        {
            get
            {
                string updatesql = String.Format("UPDATE {0} SET {1} WHERE {2}", tableName, getSetClause() , getWhereClause());
                return updatesql.Trim();
            }
        }

        public string transferTelNoStatement(string OID)
        {

            string updatesql = String.Format("UPDATE {0} SET {1} WHERE {2}", tableName, getTransferSetClause(OID), getWhereClause());
            return updatesql.Trim();

        }

        private string getTransferSetClause(string OID)
        {
            ListStringBuilder setClause = new ListStringBuilder();
            setClause.Add("{0}='{1}'", "PROPERTY_ID", OID);

            return setClause.Join(",");
        }

        private string getSetClause()
        {
            ListStringBuilder setClause = new ListStringBuilder();

            setClause.Add("{0}='{1}'", "TERM_TYPE", newTermType);
            setClause.Add("{0}='{1}'", "COOR_PROP", newCoorProp);
            setClause.Add("{0}='{1}'", "PROPERTY_ID", newPropertyId);
            setClause.Add("{0}='{1}'", "UPDATE_TYPE", updateType);
            setClause.Add("{0}=TO_DATE('{1}','YYYYMMDD HH24:MI:SS')", "MODIFIED_DATE", dateUpdated);
            setClause.Add("{0}='{1}'", "MODIFIED_USER", updatedBy);

            //for testing purposes
            //setClause.Add("{0}='{1}'", "STREET_NAME", newStreet);
            //setClause.Add("{0}='{1}'", "CUSTOMER_NAME", newCustomer);
            //setClause.Add("{0}='{1}'", "EXCHANGE_ABB", newAbb);

            return setClause.Join(",");
        }

        private string getWhereClause()
        {
            return String.Format("PHONE_NUMBER='{0}'", this.dataid);
        }

        //constructor
        public TelNoQueryBuilder(GPhone phone, SegmentName segment)
        {
            this.dataid = phone.Number;
            _segmentName = segment;
            this.newTermType = getTermType(phone.CabinetType, phone.CoorCAB, phone.CoorDP);
        }


        private string getTermType(string cabinet_type, string coor_cab, string coor_dp)
        {
            string TERM_TYPE = "0";

            if (coor_dp == "0,0" || coor_dp.Contains("-") || coor_dp == "0.0,0.0" || coor_dp == null)
            {
                coor_dp = "0,0";

                if (coor_cab == "0,0" || coor_cab.Contains("-") || coor_cab == "0.0,0.0" || coor_cab == null)
                {
                    coor_cab = "0,0";
                    TERM_TYPE = "0";
                }
                else
                {
                    if (cabinet_type == "CAB")
                    {
                        TERM_TYPE = "3";
                    }
                    else if (cabinet_type == "FTTS")
                    {
                        TERM_TYPE = "4";
                    }
                    else if (cabinet_type == "PFTTS")
                    {
                        TERM_TYPE = "5";
                    }
                    else if (cabinet_type == "FTTO")
                    {
                        TERM_TYPE = "6";
                    }
                    else if (cabinet_type == "SDF")
                    {
                        TERM_TYPE = "7";
                    }
                    else if (cabinet_type == "PCAB")
                    {
                        TERM_TYPE = "8";
                    }
                    else if (cabinet_type == "DDP")
                    {
                        TERM_TYPE = "9";
                    }
                    //shirlin 03/05/2012 - HSBB geocoding
                    else if (cabinet_type == "MUX")
                    {
                        TERM_TYPE = "11";
                    }
                    else if (cabinet_type == "FDC")
                    {
                        TERM_TYPE = "13";
                    }
                    else if (cabinet_type == "VDSL")
                    {
                        TERM_TYPE = "14";
                    }
                }
            }
            else
            {
                if (cabinet_type == "FDC")
                {
                    TERM_TYPE = "12";
                }
                else
                {
                    TERM_TYPE = "2";
                }
            }
            return TERM_TYPE;
        }
    }
}

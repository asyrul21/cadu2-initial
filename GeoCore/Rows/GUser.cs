﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Rows
{
    public class GUser : GRow
    {
        public static Regex PasswordFormat = new Regex(@"^\w{6,}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        private static List<string> _commands;

        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_USER";

        public const string NAME = "USERNAME";
        public const string PASSWORD = "PASSWORD";
        public const string GROUP_ID = "USER_TYPE_ID";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(PASSWORD)]
        public virtual string Password { set; get; }

        [MappedField(GROUP_ID)]
        public virtual int? GroupId { set; get; }

        public override string CreatedBy { set; get; }

        public override string DateCreated { set; get; }

        public override string UpdatedBy { set; get; }

        public override string DateUpdated { set; get; }

        public static IEnumerable<GUser> GetAll()
        {
            RepositoryFactory repo = new RepositoryFactory();
            return repo.Search<GUser>();
        }

        public GUserGroup GetGroup()
        {
            RepositoryFactory repo = new RepositoryFactory();
            return GroupId.HasValue ? repo.GetById<GUserGroup>(GroupId.Value) : null;
        }

        public IEnumerable<GFunction> GetFunctions()
        {
            GUserGroup userGroup = GetGroup();
            if (userGroup == null)
            {
                yield break;
            }
            foreach (GFunction function in userGroup.GetFunctions())
            {
                yield return function;
            }
        }

        public bool CanDo(string command)
        {
            return CanDo(command, false);
        }

        public bool CanDo(string command, bool updated)
        {
            if (updated)
            {
                _commands = GetCommands();
            }
            else
            {
                if (_commands == null)
                {
                    _commands = GetCommands();
                }
            }
            return _commands.Contains(command);
        }

        public List<string> GetCommands()
        {
            return GetCommands(false);
        }

        public List<string> GetCommands(bool updated)
        {
            if (updated)
            {
                List<string> commands = new List<string>();

                foreach (GFunction function in GetFunctions())
                {
                    commands.Add(function.Command);
                }
                _commands = commands;
            }
            else
            {
                if (_commands == null)
                {
                    List<string> commands = new List<string>();

                    foreach (GFunction function in GetFunctions())
                    {
                        commands.Add(function.Command);
                    }
                    _commands = commands;
                }
            }
            return _commands;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GUser))
            {
                return false;
            }

            GUser user = (GUser)obj;
            return (OID == user.OID && TableName == user.TableName && ValueEquals(user, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

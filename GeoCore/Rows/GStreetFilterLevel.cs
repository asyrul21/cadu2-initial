﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetFilterLevel : GReference
    {
        private static List<GStreetFilterLevel> _streetFilterLevels;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_STREETFILTERLEVEL";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "1 (KAMPUNG/TAMAN ROAD)";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetFilterLevel Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetFilterLevel> query = new Query<GStreetFilterLevel>();
                query.Obj.Code = code;

                IEnumerable<GStreetFilterLevel> streetFilterLevel = repo.Search(query);
                if (streetFilterLevel.Count() > 0)
                {
                    return streetFilterLevel.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetFilterLevel> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetFilterLevel> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetFilterLevels = repo.Search<GStreetFilterLevel>().ToList();
            }
            else
            {
                if (_streetFilterLevels == null)
                {
                    _streetFilterLevels = repo.Search<GStreetFilterLevel>().ToList();
                }
            }
            return _streetFilterLevels;
        }
    }
}

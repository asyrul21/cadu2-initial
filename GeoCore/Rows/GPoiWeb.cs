﻿using System;
using System.Collections.Generic;
using System.Linq;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Rows
{
    public abstract partial class GPoiWeb : GRow
    {
        public const string CODE = "POI_CODE";
        public const string NAME = "PLACE_NAME";
        public const string NAME2 = "PLACE_NAME2";
        public const string ABB = "PLC_ABB";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string URL = "URL";
        public const string REF_ID = "REF_OBJECT_ID";
        public const string REF_TYPE = "REF_FEATURE_TYPE";
        public const string STATUS = "STATUS";
        public const string CMS_NO = "CMS_NO";
        public const string ORI_ID = "ORI_ID";
        public const string TEL_NO = "TEL_NO";
        public const string TEL_NO_OWNER = "TEL_NO_OWNER";
        public const string FILTER_LEVEL = "FILTER_LEVEL";

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(ABB)]
        public virtual string Abbreviation { set; get; }

        [MappedField(DESCRIPTION)]
        public virtual string Description { set; get; }

        [MappedField(URL)]
        public virtual string Url { set; get; }

        [MappedField(REF_ID)]
        public virtual int? ParentId { set; get; }

        [MappedField(REF_TYPE)]
        public virtual int? ParentType { set; get; }
        public virtual string ParentTypeName
        {
            get
            {
                if ((ParentType == 1) || (ParentType == 2))
                    return "Property";
                else if (ParentType == 3)
                    return "Building";
                else if (ParentType == 4)
                    return "Landmark";
                else
                    return "-";
            }
        }

        [MappedField(STATUS)]
        public virtual int? Status { set; get; }

        public virtual string StatusType
        {
            get
            {
                if (Status == 1)
                    return "ADD";
                else if (Status == 2)
                    return "DELETE";
                else if (Status == 3)
                    return "EDIT";
                else
                    throw new Exception("Invalid status Type");
            }
        }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(CMS_NO)]
        public virtual int? CMSNo { set; get; }

        [MappedField(ORI_ID)]
        public virtual int? OriId { set; get; }

        [MappedField(TEL_NO)]
        public virtual string TelNo { set; get; }

        [MappedField(TEL_NO_OWNER)]
        public virtual string TelNoOwner { set; get; }

        [MappedField(FILTER_LEVEL)]
        public virtual int? FilterLevel { set; get; }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(Name2);
            return string.Join(" ", sb.ToArray());
        }

        //
        private static List<GPoiWeb> _allPoi;
        public static IEnumerable<GPoiWeb> GetAll(bool updated, SegmentName segment)
        {
            RepositoryFactory repo = new RepositoryFactory(segment);
            if (updated)
            {
                _allPoi = repo.Search<GPoiWeb>().ToList();
            }
            else
            {
                if (_allPoi == null)
                {
                    _allPoi = repo.Search<GPoiWeb>().ToList();
                }
            }
            return _allPoi;
        }
    }
}

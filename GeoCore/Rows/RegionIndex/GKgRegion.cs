﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Rows.RegionIndex
{
    public class GKgRegion : GRegionIndex
    {

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_REGION";

    }
}

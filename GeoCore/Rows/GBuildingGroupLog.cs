﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GBuildingGroupLog : GRow
    {
        public const string CODE = "BUILDING_GROUP_CODE";
        public const string NAME = "BUILDING_GROUP_NAME";
        public const string STREET_ID = "STREET_ID";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string SOURCE = "SOURCE";
        public const string AND_STATUS = "AND_STATUS";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string BUILD_NAME_POS = "BUILD_NAME_POS";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(BUILD_NAME_POS)]
        public virtual int? BuldingNamePos { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GBuildingGroup buildingGroup)
        {
            this.NavigationStatus = buildingGroup.NavigationStatus;
            this.Code = buildingGroup.Code;
            this.Name = buildingGroup.Name;
            this.Source = buildingGroup.Source;
            this.BuldingNamePos = buildingGroup.BuldingNamePos;

            this.DateCreated = buildingGroup.DateCreated;
            this.CreatedBy = buildingGroup.CreatedBy;
            this.DateUpdated = buildingGroup.DateUpdated;
            this.UpdatedBy = buildingGroup.UpdatedBy;

            this.UpdateStatus = buildingGroup.UpdateStatus;
            this.AndStatus = buildingGroup.AndStatus;

            this.ObjectId_Ori = buildingGroup.OID;
            this.LogDate = DateTime.Now;
            this.LogType = buildingGroup.UpdateStatus;

            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

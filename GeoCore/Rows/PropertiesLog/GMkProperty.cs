﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GMkProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "MK_LOG_PROPERTY";
    }
}

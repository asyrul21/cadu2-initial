﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GPgProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "PG_LOG_PROPERTY";
    }
}

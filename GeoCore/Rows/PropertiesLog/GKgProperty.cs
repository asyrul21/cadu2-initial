﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GKgProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_LOG_PROPERTY";
    }
}

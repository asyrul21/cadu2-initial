﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GKvProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KV_LOG_PROPERTY";
    }
}

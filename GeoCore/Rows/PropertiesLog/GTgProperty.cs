﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GTgProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "TG_LOG_PROPERTY";
    }
}

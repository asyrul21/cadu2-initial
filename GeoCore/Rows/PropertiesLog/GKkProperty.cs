﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GKkProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KK_LOG_PROPERTY";
    }
}

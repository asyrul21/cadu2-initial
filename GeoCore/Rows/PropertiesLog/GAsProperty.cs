﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GAsProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "AS_LOG_PROPERTY";
    }
}

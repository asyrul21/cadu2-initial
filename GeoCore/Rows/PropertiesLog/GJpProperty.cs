﻿
namespace Geomatic.Core.Rows.PropertiesLog
{
    public class GJpProperty : GPropertyLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "JP_LOG_PROPERTY";
    }
}

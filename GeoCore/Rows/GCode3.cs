﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GCode3 : GCode
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "CODE_DESC3";

        public const string CODE1 = "CODE1";
        public const string CODE2 = "CODE2";
        public const string CODE3 = "CODE3";
        public const string CODE_NAME = "CODE_NAME";
        public const string EDIT_FLAG = "EDIT_FLAG";
        public const string BUILDING_GROUP_FLAG = "BUILDING_GROUP_FLAG";

        [MappedField(CODE1)]
        public virtual string Code1 { set; get; }

        [MappedField(CODE2)]
        public virtual string Code2 { set; get; }

        [MappedField(CODE3)]
        public virtual string Code3 { set; get; }

        [MappedField(CODE_NAME)]
        public virtual string CodeName { set; get; }

        [MappedField(EDIT_FLAG)]
        public virtual int? EditFlag { set; get; }

        [MappedField(BUILDING_GROUP_FLAG)]
        public virtual int? BuildingGroupFlag { set; get; }

        public virtual bool IsStandardName
        {
            get
            {
                return EditFlag == 1;
            }
        }

        public virtual bool IsBuildingGroupCode
        {
            get
            {
                return BuildingGroupFlag == 1;
            }
        }

        public string GetCodes()
        {
            if (string.IsNullOrEmpty(Code1))
            {
                throw new MissingFieldException("Code 1 is missing");
            }
            if (string.IsNullOrEmpty(Code2))
            {
                throw new MissingFieldException("Code 2 is missing");
            }
            if (string.IsNullOrEmpty(Code3))
            {
                return string.Format("{0}{1}", Code1, Code2);
            }
            return string.Format("{0}{1}{2}", Code1, Code2, Code3);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GCode3))
            {
                return false;
            }

            GCode3 code = (GCode3)obj;
            return (OID == code.OID && TableName == code.TableName && ValueEquals(code, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public static GCode3 Get(string code1, string code2, string code3)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode3> query = new Query<GCode3>();
            query.Obj.Code1 = code1;
            query.Obj.Code2 = code2;
            query.Obj.Code3 = code3;

            IEnumerable<GCode3> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }

        public virtual GCode1 GetCode1()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode1> query = new Query<GCode1>();
            query.Obj.Code1 = Code1;

            IEnumerable<GCode1> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }

        public virtual GCode2 GetCode2()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode2> query = new Query<GCode2>();
            query.Obj.Code1 = Code1;
            query.Obj.Code2 = Code2;

            IEnumerable<GCode2> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }

        public virtual GCode3 GetCode3()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode3> query = new Query<GCode3>();
            query.Obj.Code1 = Code1;
            query.Obj.Code2 = Code2;
            query.Obj.Code3 = Code3;

            IEnumerable<GCode3> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }
    }
}

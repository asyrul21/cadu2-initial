﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetClass : GReference
    {
        private static List<GStreetClass> _streetClasses;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_NAVI_STREETCLASS";

        public const string CODE = "CODE";
        public const string NAME = "NAME";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetClass Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetClass> query = new Query<GStreetClass>();
                query.Obj.Code = code;

                IEnumerable<GStreetClass> streetClass = repo.Search(query);
                if (streetClass.Count() > 0)
                {
                    return streetClass.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetClass> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetClass> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetClasses = repo.Search<GStreetClass>().ToList();
            }
            else
            {
                if (_streetClasses == null)
                {
                    _streetClasses = repo.Search<GStreetClass>().ToList();
                }
            }
            return _streetClasses;
        }
    }
}

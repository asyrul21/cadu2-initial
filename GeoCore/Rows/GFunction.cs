﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GFunction : GRow
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_FUNCTION";

        public const string NAME = "NAME";
        public const string PARENT_ID = "PARENT_ID";
        public const string COMMAND = "COMMAND";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(PARENT_ID)]
        public virtual int? ParentId { set; get; }

        [MappedField(COMMAND)]
        public virtual string Command { set; get; }

        public override string CreatedBy { set; get; }

        public override string DateCreated { set; get; }

        public override string UpdatedBy { set; get; }

        public override string DateUpdated { set; get; }

        public static IEnumerable<GFunction> GetRoot()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GFunction> query = new Query<GFunction>();
            query.Obj.ParentId = 0;
            return repo.Search(query);
        }

        public IEnumerable<GFunction> GetChild()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GFunction> query = new Query<GFunction>();
            query.Obj.ParentId = OID;
            return repo.Search(query);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GFunction))
            {
                return false;
            }

            GFunction function = (GFunction)obj;
            return (OID == function.OID && TableName == function.TableName && ValueEquals(function, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }
    }
}

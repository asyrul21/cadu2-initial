﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GPropertyPermission : GReference
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_PROPTYPE_NO_BUILDING";

        public const string CODE = "CODENUM";
        public const string NAME = "DISPTEXT";
        public const string ABBREVIATION = "CODETEXT";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        [MappedField(ABBREVIATION)]
        public virtual string Abbreviation { set; get; }

        public static GPropertyPermission Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GPropertyPermission> query = new Query<GPropertyPermission>();
                query.Obj.Code = code;

                IEnumerable<GPropertyPermission> permission = repo.Search(query);
                if (permission.Count() > 0)
                {
                    return permission.First();
                }
            }

            return null;
        }

        public static IEnumerable<GPropertyPermission> GetAll()
        {
            RepositoryFactory repo = new RepositoryFactory();
            return repo.Search<GPropertyPermission>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Rows
{
    public class GTollPrice : GRow, IUserCreate, IUserUpdate
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_TOLL_PRICE";

        public const string TOLL_ROUTE_ID = "TOLL_ROUTE_ID";
        public const string VEHICLE_TYPE = "VEHICLE_TYPE";
        public const string PRICE = "PRICE";

        [MappedField(TOLL_ROUTE_ID)]
        public virtual int? TollRouteID { set; get; }

        [MappedField(VEHICLE_TYPE)]
        public virtual int? VehicleType { set; get; }

        public virtual string VehicleTypeValue
        {
            get
            {
                GVehicleType vehicleType = GetVehicleTypeObj();
                return StringUtils.TrimSpaces((vehicleType == null) ? string.Empty : vehicleType.Name);
            }
        }

        [MappedField(PRICE)]
        public virtual double? Price { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        public GVehicleType GetVehicleType()
        {
            return GVehicleType.Get(VehicleType);
        }

        public void Init()
        {
            VehicleType = 1;
        }

        public virtual GVehicleType GetVehicleTypeObj()
        {
            return GVehicleType.Get(VehicleType);
        }

    }
}

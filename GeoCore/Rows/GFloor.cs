﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GPoi = Geomatic.Core.Features.GPoi;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Rows
{
    public abstract class GFloor : GRow, IUserCreate, IUserUpdate, IGNepsObject
    {
        public const string NUMBER = "FLOOR_NUM";
        public const string UNIT_NUM = "UNIT_NUM";
        public const string PROPERTY_ID = "PROPERTY_ID";
        public const string UPDATE_STATUS = "STATUS";
        // noraini - Apr 2021
        public const string AND_STATUS = "AND_STATUS";

        [MappedField(NUMBER)]
        public virtual string Number { set; get; }

        [MappedField(UNIT_NUM)]
        public virtual string NumUnit { set; get; }

        [MappedField(PROPERTY_ID)]
        public virtual int? PropertyId { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        public virtual GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<GProperty>(PropertyId.Value) : null;
        }

        public virtual GStreet GetStreet()
        {
            GProperty property = GetProperty();
            return (property != null) ? property.GetStreet() : null;
        }

        public virtual IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.FloorId = this.OID;
            return repo.Search(query);
        }

        public virtual bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.FloorId = this.OID;
            return repo.Count(query) > 0;
        }

        //public virtual bool HasNumberFloor()
        //{
        //    RepositoryFactory repo = new RepositoryFactory(SegmentName);
        //    Query<GFloor> query = new Query<GFloor>(SegmentName);
        //    query.Obj.Number = Number;
        //    return repo.Count(query) > 0;
        //}



        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Number);
            sb.Add(NumUnit);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }

            return string.Join(" ", sb.ToArray());
        }
    }
}

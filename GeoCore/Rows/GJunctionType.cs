﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GJunctionType : GReference
    {
        private static List<GJunctionType> _junctionTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_JUNCTIONTYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const int STREET_JUNCTION = 1;
        public const int TRAFFIC_LIGHT_JUNCTION = 2;
        public const int ROUNDABOUT = 3;
        public const int TRAFFIC_LIGHT_ROUNDABOUT = 4;
        public const int TOLL_GATE = 5;
        public const int DEAD_END = 6;
        public const int IN_TOLL = 7;
        public const int OUT_TOLL = 8;

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GJunctionType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GJunctionType> query = new Query<GJunctionType>();
                query.Obj.Code = code;

                IEnumerable<GJunctionType> type = repo.Search(query);
                if (type.Count() > 0)
                {
                    return type.First();
                }
            }

            return null;
        }

        public static IEnumerable<GJunctionType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GJunctionType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _junctionTypes = repo.Search<GJunctionType>().ToList();
            }
            else
            {
                if (_junctionTypes == null)
                {
                    _junctionTypes = repo.Search<GJunctionType>().ToList();
                }
            }
            return _junctionTypes;
        }
    }
}

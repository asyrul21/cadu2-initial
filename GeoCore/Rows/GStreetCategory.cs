﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetCategory : GReference
    {
        private static List<GStreetCategory> _streetCategories;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_NAVI_STREETCATEGORY";

        public const string CODE = "CODE";
        public const string NAME = "NAME";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetCategory Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetCategory> query = new Query<GStreetCategory>();
                query.Obj.Code = code;

                IEnumerable<GStreetCategory> streetCategory = repo.Search(query);
                if (streetCategory.Count() > 0)
                {
                    return streetCategory.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetCategory> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetCategory> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetCategories = repo.Search<GStreetCategory>().ToList();
            }
            else
            {
                if (_streetCategories == null)
                {
                    _streetCategories = repo.Search<GStreetCategory>().ToList();
                }
            }
            return _streetCategories;
        }
    }
}

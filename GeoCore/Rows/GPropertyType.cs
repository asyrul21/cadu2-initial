﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GPropertyType : GReference
    {
        private static List<GPropertyType> _propertyTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_PROPTYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "DISPTEXT";
        public const string ABBREVIATION = "CODETEXT";
        public const string DEFAULT_FLOOR = "DEFAULT_FLOOR";
        public const string FLOOR_TYPE = "FLOOR_TYPE";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        [MappedField(ABBREVIATION)]
        public virtual string Abbreviation { set; get; }
        
        [MappedField(DEFAULT_FLOOR)]
        public int? DefaultFloor { set; get; }

        [MappedField(FLOOR_TYPE)]
        public string FloorType { set; get; }

        public static GPropertyType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GPropertyType> query = new Query<GPropertyType>();
                query.Obj.Code = code;

                IEnumerable<GPropertyType> type = repo.Search(query);
                if (type.Count() > 0)
                {
                    return type.First();
                }
            }

            return null;
        }

        public static IEnumerable<GPropertyType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GPropertyType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            // added by noraini ali - Feb 2021
            Query<GPropertyType> query = new Query<GPropertyType>();
            query.AddClause(() => query.Obj.Code, "NOT IN ('1','2')");
            // end
            if (updated)
            {
                _propertyTypes = repo.Search<GPropertyType>(query).ToList();
            }
            else
            {
                if (_propertyTypes == null)
                {
                    _propertyTypes = repo.Search<GPropertyType>(query).ToList();
                }
            }
            return _propertyTypes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core.Rows
{
    public class GNavigationStatus : GReference
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "NAVI_STATUS";

        protected const string CODE = "CODE";
        protected const string NAME = "NAME";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }     
    }
}

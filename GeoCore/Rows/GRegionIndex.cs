﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;

namespace Geomatic.Core.Rows
{
    public abstract class GRegion : GRow, IUserCreate, IUserUpdate
    {
        public const string NAME = "INDEX_";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }
    }
}

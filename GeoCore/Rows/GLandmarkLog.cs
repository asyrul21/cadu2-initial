﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GLandmarkLog : GRow
    {
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string CODE = "LANDMARK_CODE";
        public const string NAME = "LANDMARK_NAME";
        public const string NAME2 = "LANDMARK_NAME2";
        public const string STREET_ID = "STREET_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        public const string AND_STATUS = "AND_STATUS";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GLandmark landmark)
        {
            this.NavigationStatus = landmark.NavigationStatus;
            this.Code = landmark.Code;
            this.Name = landmark.Name;
            this.Name2 = landmark.Name2;
            this.Source = landmark.Source;
            this.StreetId = landmark.StreetId;

            this.DateCreated = landmark.DateCreated;
            this.CreatedBy = landmark.CreatedBy;
            this.DateUpdated = landmark.DateUpdated;
            this.UpdatedBy = landmark.UpdatedBy;

            this.ObjectId_Ori = landmark.OID;
            this.LogDate = DateTime.Now;
            this.LogType = landmark.UpdateStatus;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

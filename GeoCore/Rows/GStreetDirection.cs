﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetDirection : GReference
    {
        private static List<GStreetDirection> _streetDirections;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_STREETDIR";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "TWO WAY";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetDirection Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetDirection> query = new Query<GStreetDirection>();
                query.Obj.Code = code;

                IEnumerable<GStreetDirection> streetDesign = repo.Search(query);
                if (streetDesign.Count() > 0)
                {
                    return streetDesign.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetDirection> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetDirection> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetDirections = repo.Search<GStreetDirection>().ToList();
            }
            else
            {
                if (_streetDirections == null)
                {
                    _streetDirections = repo.Search<GStreetDirection>().ToList();
                }
            }
            return _streetDirections;
        }
    }
}

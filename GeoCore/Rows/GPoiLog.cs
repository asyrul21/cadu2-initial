﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GPoiLog : GRow
    {
        public const string UPDATE_STATUS = "STATUS";
        public const string NAME2 = "PLACE_NAME2";
        public const string CODE = "POI_CODE";
        public const string ABB = "PLC_ABB";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string TEL_NO = "TEL_NO";
        public const string OTHER_TEL_NO = "OTHER_TEL_NO";
        public const string TEL_NO_OWNER = "TEL_NO_OWNER";
        public const string FILTER_LEVEL = "FILTER_LEVEL";
        public const string URL = "URL";
        public const string FAMOUS_CODE = "FAMOUS_CODE";
        public const string DISPLAY_TEXT = "DISPLAY_CODE";
        public const string SOURCE = "SOURCE";
        public const string REF_TYPE = "REF_FEATURE_TYPE";
        public const string REF_OBJECT_ID = "REF_OBJECT_ID";
        public const string AREA_ID = "AREA_ID";
        public const string NAVI_ID = "NAVI_ID";
        public const string NAVI_NAME = "NAVI_NAME";
        public const string NAVI_FC = "NAVI_FC";
        public const string NAVIGATION_STATUS = "NAVIGATION_STATUS";
        public const string NAVI_MATCH = "NAVI_MATCH";
        public const string NAVI_MATCH_DATE = "NAVI_MATCH_DATE";
        public const string NAME = "PLACE_NAME";
        public const string FLOOR_ID = "FLOOR_ID";
        public const string AND_STATUS = "AND_STATUS";
        public const string SHAPE = "SHAPE";
        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";
        
        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(ABB)]
        public virtual string Abbreviation { set; get; }

        [MappedField(DESCRIPTION)]
        public virtual string Description { set; get; }

        [MappedField(TEL_NO)]
        public virtual string TelNo { set; get; }

        [MappedField(OTHER_TEL_NO)]
        public virtual string OtherTelNo { set; get; }

        [MappedField(TEL_NO_OWNER)]
        public virtual string TelNoOwner { set; get; }

        [MappedField(FILTER_LEVEL)]
        public virtual int? FilterLevel { set; get; }

        [MappedField(URL)]
        public virtual string Url { set; get; }

        [MappedField(FAMOUS_CODE)]
        public virtual string FamousCode { set; get; }

        [MappedField(DISPLAY_TEXT)]
        public virtual string DisplayText { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(REF_TYPE)]
        public virtual int? ParentType { set; get; }

        [MappedField(REF_OBJECT_ID)]
        public virtual int? RefObjectId { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(NAVI_ID)]
        public virtual int? NaviId { set; get; }

        [MappedField(NAVI_NAME)]
        public virtual string NaviName { set; get; }

        [MappedField(NAVI_FC)]
        public virtual string NaviFV { set; get; }

        [MappedField(NAVIGATION_STATUS)]
        public virtual string NaviStatus { set; get; }

        [MappedField(NAVI_MATCH)]
        public virtual string NaviMatch { set; get; }

        [MappedField(NAVI_MATCH_DATE)]
        public virtual string NaviMatchDate{ set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(FLOOR_ID)]
        public virtual string FloorId { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(SHAPE)]
        public virtual string Shape { set; get; }

        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GPoi poi)
        {
            this.Name2 = poi.Name2;
            this.Code = poi.Code;
            this.Abbreviation = poi.Abbreviation;
            this.Description = poi.Description;
            this.TelNo = poi.TelNo;
            this.OtherTelNo = poi.OtherTelNo;
            this.TelNoOwner = poi.TelNoOwner;
            this.FilterLevel = poi.FilterLevel;
            this.Url = poi.Url;
            this.FamousCode = poi.Famous;
            this.DisplayText = poi.DisplayText.ToString();
            this.Source = poi.Source;
            this.DateCreated = poi.DateCreated;
            this.CreatedBy = poi.CreatedBy;
            this.DateUpdated = poi.DateUpdated;
            this.UpdatedBy = poi.UpdatedBy;
            this.ParentType = poi.ParentType;
            this.RefObjectId = poi.ParentId;
            this.AreaId = poi.AreaId;
            this.NaviId = poi.NaviId;
            this.NaviName = poi.NaviName;
            this.NaviFV = poi.NaviFV;
            this.NaviStatus = poi.NavigationStatus.ToString();
            this.NaviMatch = poi.NaviMatch;
            this.NaviMatchDate = poi.NaviMatchDate;
            this.Name = poi.Name;

            this.AndStatus = poi.AndStatus;
            this.UpdateStatus = poi.UpdateStatus;

            this.ObjectId_Ori = poi.OID;
            this.LogType = poi.UpdateStatus;
            this.LogDate = DateTime.Now;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

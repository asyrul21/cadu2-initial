﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GPhoneProvider : GReference2
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_TELNO_NUMBER_ASSIGNMENT";

        public const string CODE = "AREA_CODE";
        public const string FROM_NUMBER = "FROM_NUMBER";
        public const string TO_NUMBER = "TO_NUMBER";
        public const string NAME = "PROVIDER";

        [MappedField(CODE)]
        public override string Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        [MappedField(FROM_NUMBER)]
        public virtual int? FromNumber { set; get; }

        [MappedField(TO_NUMBER)]
        public virtual int? ToNumber { set; get; }

        public static GPhoneProvider Get(string areaCode, string number)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPhoneProvider> query = new Query<GPhoneProvider>();
            query.Obj.Code = areaCode;
            query.AddClause(() => query.Obj.FromNumber, string.Format("<= {0}", number));
            query.AddClause(() => query.Obj.ToNumber, string.Format(">= {0}", number));

            IEnumerable<GPhoneProvider> providers = repo.Search(query);
            if (providers.Count() > 0)
            {
                return providers.First();
            }

            return null;
        }
    }
}

﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GAsLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "AS_LOG_LANDMARK";
    }
}

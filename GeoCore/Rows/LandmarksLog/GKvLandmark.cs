﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GKvLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_LOG_LANDMARK";
    }
}

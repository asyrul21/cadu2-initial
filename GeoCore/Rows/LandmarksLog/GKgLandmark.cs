﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GKgLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_LOG_LANDMARK";
    }
}

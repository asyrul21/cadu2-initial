﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GJpLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_LOG_LANDMARK";
    }
}

﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GKkLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KK_LOG_LANDMARK";
    }
}

﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GJhLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JH_LOG_LANDMARK";
    }
}

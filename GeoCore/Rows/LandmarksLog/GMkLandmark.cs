﻿
namespace Geomatic.Core.Rows.LandmarksLog
{
    public class GMkLandmark : GLandmarkLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "MK_LOG_LANDMARK";
    }
}

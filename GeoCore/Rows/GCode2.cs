﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GCode2 : GCode
    {
        private List<GCode3> _code3s;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "CODE_DESC2";

        public const string CODE1 = "CODE1";
        public const string CODE2 = "CODE2";

        [MappedField(CODE1)]
        public virtual string Code1 { set; get; }

        [MappedField(CODE2)]
        public virtual string Code2 { set; get; }

        public static GCode2 Get(string code1, string code2)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode2> query = new Query<GCode2>();
            query.Obj.Code1 = code1;
            query.Obj.Code2 = code2;

            IEnumerable<GCode2> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }

        public virtual IEnumerable<GCode3> GetCode3s()
        {
            return GetCode3s(true);
        }

        public virtual IEnumerable<GCode3> GetCode3s(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode3> query = new Query<GCode3>();
            query.Obj.Code1 = Code1;
            query.Obj.Code2 = Code2;

            if (updated)
            {
                _code3s = repo.Search<GCode3>(query).ToList();
            }
            else
            {
                if (_code3s == null)
                {
                    _code3s = repo.Search<GCode3>(query).ToList();
                }
            }

            return _code3s;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Rows
{
    public abstract class GMultiStorey : GRow, IUserCreate, IUserUpdate, IGNepsObject
    {
        public const string FLOOR = "FLOOR_NUM";
        public const string APARTMENT = "APT_NUM";
        public const string BUILDING_ID = "BUILDING_ID";
        public const string GDS_ID = "GDS_ID";
        public const string NIS_ID = "NIS_ID";
        public const string GR_ID = "GR_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string AND_STATUS = "AND_STATUS";

        [MappedField(FLOOR)]
        public virtual string Floor { set; get; }

        [MappedField(APARTMENT)]
        public virtual string Apartment { set; get; }

        [MappedField(BUILDING_ID)]
        public virtual int? BuildingId { set; get; }

        [MappedField(GDS_ID)]
        public virtual int? GdsId { set; get; }

        [MappedField(NIS_ID)]
        public virtual int? NisId { set; get; }

        [MappedField(GR_ID)]
        public virtual int? GrId { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        public virtual GBuilding GetBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (BuildingId.HasValue) ? repo.GetById<GBuilding>(BuildingId.Value) : null;
        }

        public virtual GProperty GetProperty()
        {
            GBuilding building = GetBuilding();
            return (building != null) ? building.GetProperty() : null;
        }

        public virtual GStreet GetStreet()
        {
            GProperty property = GetProperty();
            return (property != null) ? property.GetStreet() : null;
        }

        public void Init()
        {
            GdsId = 0;
            NisId = 0;
            GrId = 0;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GMultiStorey))
            {
                return false;
            }

            GMultiStorey multiStorey = (GMultiStorey)obj;
            return (OID == multiStorey.OID && TableName == multiStorey.TableName && ValueEquals(multiStorey, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Utilities;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Rows
{
    public abstract class GCode : GRow
    {
        public const string DESCRIPTION = "DESCRIPTION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        [MappedField(DESCRIPTION)]
        public virtual string Description { set; get; } 

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public static string GetCode1(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return string.Empty;
            }
            if (code.Length < 2)
            {
                return string.Empty;
            }
            return Regex.Replace(code, "^(.{2})(.*)", "$1", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }

        public static string GetCode2(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return string.Empty;
            }
            if (code.Length < 4)
            {
                return string.Empty;
            }
            return Regex.Replace(code, "^(.{2})(.{2})(.*)", "$2", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }

        public static string GetCode3(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return string.Empty;
            }
            if (code.Length < 6)
            {
                return string.Empty;
            }
            return Regex.Replace(code, "^(.{4})(.{2})", "$2", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }

        public override string ToString()
        {
            return StringUtils.TrimSpaces(Description);
        }
    }
}

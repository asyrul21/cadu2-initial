﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Rows
{
    public abstract class GReference : GRow
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public virtual int? Code { set; get; }

        public virtual string Name { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return StringUtils.TrimSpaces(Name);
        }
    }
}

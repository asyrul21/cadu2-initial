﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetDesign : GReference
    {
        private static List<GStreetDesign> _streetDesigns;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_STREETDESIGN";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "NORMAL";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetDesign Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetDesign> query = new Query<GStreetDesign>();
                query.Obj.Code = code;

                IEnumerable<GStreetDesign> streetDesign = repo.Search(query);
                if (streetDesign.Count() > 0)
                {
                    return streetDesign.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetDesign> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetDesign> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetDesigns = repo.Search<GStreetDesign>().ToList();
            }
            else
            {
                if (_streetDesigns == null)
                {
                    _streetDesigns = repo.Search<GStreetDesign>().ToList();
                }
            }
            return _streetDesigns;
        }
    }
}

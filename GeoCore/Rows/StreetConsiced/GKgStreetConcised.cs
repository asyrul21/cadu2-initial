﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
//added
using Geomatic.Core.Features.Streets;
using Geomatic.Core.Features;
namespace Geomatic.Core.Rows.StreetConsiced
{
    public class GKgStreetConcised : GReference
    {
        private static List<GKgStreetConcised> _streetName;

        public override string TableName
        {
            //get { return TABLE_NAME; }
            get { return GKgStreet.TABLE_NAME; }
        }

        public const string NAME = "STREET_NAME";
        public const string NAME2 = "STREET_ALT_NAME";
        public const string SECTION = "SECTION_NAME";
        public const string POSTCODE = "POSTAL_NUM";
        public const string CITY = "CITY_NAME";
        public const string SUB_CITY = "SUB_CITY";
        public const string STATE = "STATE_CODE";

        [MappedField(NAME)]
        public override string Name { set; get; }

        [MappedField(NAME2)]
        public string Name2 { set; get; }

        [MappedField(SECTION)]
        public virtual string Section { set; get; }

        [MappedField(POSTCODE)]
        public virtual string Postcode { set; get; }

        [MappedField(CITY)]
        public virtual string City { set; get; }

        [MappedField(SUB_CITY)]
        public virtual string SubCity { set; get; }

        [MappedField(STATE)]
        public virtual string State { set; get; }

        public static IEnumerable<GKgStreetConcised> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GKgStreetConcised> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KG);
            if (updated)
            {
                _streetName = repo.Search<GKgStreetConcised>().ToList();
            }
            else
            {
                if (_streetName == null)
                {
                    _streetName = repo.Search<GKgStreetConcised>().ToList();
                }
            }
            return _streetName;
        }
    }
}

﻿
namespace Geomatic.Core.Rows.BuildingsLog
{
    public class GJpBuilding : GBuildingLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_LOG_BUILDING";
    }
}

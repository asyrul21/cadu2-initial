﻿
namespace Geomatic.Core.Rows.BuildingsLog
{
    public class GJhBuilding : GBuildingLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }
        
        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JH_LOG_BUILDING";
    }
}

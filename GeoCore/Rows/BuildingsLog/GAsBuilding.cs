﻿
namespace Geomatic.Core.Rows.BuildingsLog
{
    public class GAsBuilding : GBuildingLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "AS_LOG_BUILDING";
    }
}

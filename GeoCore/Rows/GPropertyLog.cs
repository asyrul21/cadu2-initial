﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GPropertyLog : GRow
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string TYPE = "PROPERTY_TYPE";
        public const string LOT = "LOT_NUMBER";
        public const string HOUSE = "HOUSE_NUMBER";
        public const string STREET_ID = "STREET_ID";
        public const string NUM_OF_FLOOR = "QUANTITY";
        public const string YEAR_INSTALL = "YEAR_INSTALL";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string AND_STATUS = "AND_STATUS";

        public const string NAME_DISPLAYED = "NAME_DISPLAYED";
        public const string TEL_NO = "TEL_NO";
        public const string OTHER_HOUSE_NUM = "OTHER_HOUSE_NUM";
        public const string OTHER_TEL_NUM = "OTHER_TEL_NUM";
        public const string TEL_NO_OWNER = "TEL_NO_OWNER";
        public const string IPID = "IPID";
        public const string STREET_IPID = "STREET_IPID";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        [MappedField(LOT)]
        public virtual string Lot { set; get; }

        [MappedField(HOUSE)]
        public virtual string House { set; get; }

        [MappedField(YEAR_INSTALL)]
        public virtual int? YearInstall { set; get; }

        [MappedField(NUM_OF_FLOOR)]
        public virtual int? NumberOfFloor { set; get; }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        // new fields
        [MappedField(NAME_DISPLAYED)]
        public virtual string NameDisplayed { set; get; }

        [MappedField(TEL_NO)]
        public virtual int? TelNo { set; get; }

        [MappedField(OTHER_HOUSE_NUM)]
        public virtual int? OtherHouseNum { set; get; }

        [MappedField(OTHER_TEL_NUM)]
        public virtual int? OtherTelNum { set; get; }

        [MappedField(TEL_NO_OWNER)]
        public virtual string TelNoOwner { set; get; }

        [MappedField(IPID)]
        public virtual int? Ipid { set; get; }

        [MappedField(STREET_IPID)]
        public virtual int? StreetIpid { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GProperty property)
        {
            this.ConstructionStatus = property.ConstructionStatus;
            this.House = property.House;
            this.Lot = property.Lot;
            this.Source = property.Source;
            this.YearInstall = property.YearInstall;
            this.Type = property.Type;
            this.NumberOfFloor = property.NumberOfFloor;
            this.Type = property.Type;
            this.StreetId = property.StreetId;
            this.UpdateStatus = property.UpdateStatus;
            this.AndStatus = property.AndStatus;

            this.DateCreated = property.DateCreated;
            this.CreatedBy = property.CreatedBy;
            this.DateUpdated = property.DateUpdated;
            this.UpdatedBy = property.UpdatedBy;

            this.NameDisplayed = "1";
            this.TelNo = 0;
            this.OtherHouseNum = 0;
            this.OtherTelNum = 0;
            this.TelNoOwner = "";
            this.Ipid = 0;
            this.StreetIpid = 0;

            this.ObjectId_Ori = property.OID;
            this.LogDate = DateTime.Now;
            this.LogType = property.UpdateStatus;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

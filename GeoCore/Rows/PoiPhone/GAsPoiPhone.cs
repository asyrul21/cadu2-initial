﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Geomatic.Core.Rows.Phones;

namespace Geomatic.Core.Rows.PhoneRefs
{
    public class GAsPoiPhone : GPoiPhone
    {
        public static IEnumerable<string> AreaCodes = GAsPhone.AreaCodes;

        public static Regex Format = GAsPhone.Format;

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "AS_ADM_POI_PHONE";

        public override IEnumerable<string> GetAreaCodes()
        {
            return AreaCodes;
        }

        public override Regex GetFormat()
        {
            return Format;
        }
    }
}

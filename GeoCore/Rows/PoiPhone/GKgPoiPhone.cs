﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Geomatic.Core.Rows.Phones;

namespace Geomatic.Core.Rows.PhoneRefs
{
    public class GKgPoiPhone : GPoiPhone
    {
        public static IEnumerable<string> AreaCodes = GKgPhone.AreaCodes;

        public static Regex Format = GKgPhone.Format;

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_ADM_POI_PHONE";

        public override IEnumerable<string> GetAreaCodes()
        {
            return AreaCodes;
        }

        public override Regex GetFormat()
        {
            return Format;
        }
    }
}

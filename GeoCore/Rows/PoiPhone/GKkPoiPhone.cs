﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Geomatic.Core.Rows.Phones;

namespace Geomatic.Core.Rows.PhoneRefs
{
    public class GKkPoiPhone : GPoiPhone
    {
        public static IEnumerable<string> AreaCodes = GKkPhone.AreaCodes;

        public static Regex Format = GKkPhone.Format;

        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KK_ADM_POI_PHONE";

        public override IEnumerable<string> GetAreaCodes()
        {
            return AreaCodes;
        }

        public override Regex GetFormat()
        {
            return Format;
        }
    }
}

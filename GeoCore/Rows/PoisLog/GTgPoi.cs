﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Rows.PoisLog
{
    public class GTgPoi : GPoiLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "TG_LOG_POI";
    }
}

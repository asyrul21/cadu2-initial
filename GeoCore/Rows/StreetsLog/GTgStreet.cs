﻿
namespace Geomatic.Core.Rows.StreetsLog
{
    public class GTgStreet : GStreetLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "TG_LOG_STREET";
    }
}

﻿
namespace Geomatic.Core.Rows.StreetsLog
{
    public class GJhStreet : GStreetLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public const string TABLE_NAME = "JH_LOG_STREET";
    }
}

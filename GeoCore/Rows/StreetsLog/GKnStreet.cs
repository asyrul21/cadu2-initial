﻿
namespace Geomatic.Core.Rows.StreetsLog
{
    public class GKnStreet : GStreetLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KN_LOG_STREET";
    }
}

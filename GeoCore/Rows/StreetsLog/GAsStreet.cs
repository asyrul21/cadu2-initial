﻿
namespace Geomatic.Core.Rows.StreetsLog
{
    public class GAsStreet : GStreetLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public const string TABLE_NAME = "AS_LOG_STREET";
    }
}

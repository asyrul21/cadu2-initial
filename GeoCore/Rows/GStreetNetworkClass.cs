﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetNetworkClass : GReference
    {
        private static List<GStreetNetworkClass> _streetNetworkClasses;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_NAVI_ROADCLASS";

        public const string CODE = "CODE";
        public const string NAME = "NAME";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetNetworkClass Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetNetworkClass> query = new Query<GStreetNetworkClass>();
                query.Obj.Code = code;

                IEnumerable<GStreetNetworkClass> networkClass = repo.Search(query);
                if (networkClass.Count() > 0)
                {
                    return networkClass.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetNetworkClass> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetNetworkClass> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetNetworkClasses = repo.Search<GStreetNetworkClass>().ToList();
            }
            else
            {
                if (_streetNetworkClasses == null)
                {
                    _streetNetworkClasses = repo.Search<GStreetNetworkClass>().ToList();
                }
            }
            return _streetNetworkClasses;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Rows
{
    public class GUserGroupFunction : GRow
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_USER_TYPE_FUNCTION";

        public const string GROUP_ID = "USER_TYPE_ID";
        public const string FUNCTION_ID = "FUNCTION_ID";

        [MappedField(GROUP_ID)]
        public virtual int? GroupId { set; get; }

        [MappedField(FUNCTION_ID)]
        public virtual int? FunctionId { set; get; }

        public override string CreatedBy { set; get; }

        public override string DateCreated { set; get; }

        public override string UpdatedBy { set; get; }

        public override string DateUpdated { set; get; }

        public GFunction GetFunction()
        {
            RepositoryFactory repository = new RepositoryFactory();
            return FunctionId.HasValue ? repository.GetById<GFunction>(FunctionId.Value) : null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetDivider : GReference
    {
        private static List<GStreetDivider> _streetDividers;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_NAVI_DIVIDER";

        public const string CODE = "CODE";
        public const string NAME = "NAME";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetDivider Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetDivider> query = new Query<GStreetDivider>();
                query.Obj.Code = code;

                IEnumerable<GStreetDivider> streetDivider = repo.Search(query);
                if (streetDivider.Count() > 0)
                {
                    return streetDivider.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetDivider> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetDivider> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetDividers = repo.Search<GStreetDivider>().ToList();
            }
            else
            {
                if (_streetDividers == null)
                {
                    _streetDividers = repo.Search<GStreetDivider>().ToList();
                }
            }
            return _streetDividers;
        }
    }
}

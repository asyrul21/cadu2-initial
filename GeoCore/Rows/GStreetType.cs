﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetType : GReference
    {
        private static List<GStreetType> _streetTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_STREETTYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "USRTEXT";
        public const string ABBREVIATION = "CODETEXT";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        [MappedField(ABBREVIATION)]
        public virtual string Abbreviation { set; get; }

        public static GStreetType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetType> query = new Query<GStreetType>();
                query.Obj.Code = code;

                IEnumerable<GStreetType> streetType = repo.Search(query);
                if (streetType.Count() > 0)
                {
                    return streetType.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetTypes = repo.Search<GStreetType>().ToList();
            }
            else
            {
                if (_streetTypes == null)
                {
                    _streetTypes = repo.Search<GStreetType>().ToList();
                }
            }
            return _streetTypes;
        }
    }
}

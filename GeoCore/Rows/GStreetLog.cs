﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GStreetLog : GRow
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string STATUS = "STREET_STATUS";
        public const string NETWORK_CLASS = "ROAD_CLASS";
        public const string CLASS = "STREET_CLASS";
        public const string CATEGORY = "STREET_CATEGORY";
        public const string FILTER_LEVEL = "FILTER_LEVEL";
        public const string DESIGN = "DESIGN";
        public const string DIRECTION = "ONE_WAY";
        public const string TOLL_TYPE = "TOLL_TYPE";
        public const string DIVIDER = "DIVIDER";
        public const string TYPE = "STREET_TYPE";
        public const string NAME = "STREET_NAME";
        public const string NAME2 = "STREET_ALT_NAME";
        public const string SECTION = "SECTION_NAME";
        public const string POSTCODE = "POSTAL_NUM";
        public const string CITY = "CITY_NAME";
        public const string SUB_CITY = "SUB_CITY";
        public const string STATE = "STATE_CODE";
        public const string FROM_ID = "SOURCE_ID";
        public const string TO_ID = "TERMINATION_ID";
        public const string SPEED_LIMIT = "SPEED_LIMIT";
        public const string WEIGHT = "MAX_WEIGHT";
        public const string ELEVATION = "STREET_ELEVATION";
        public const string LENGTH = "STREET_LENGTH";
        public const string WIDTH = "STREET_WIDTH";
        public const string HEIGHT = "STREET_HEIGHT";
        public const string LANES = "LANES_COUNT";
        public const string BICYCLE = "NOT_FOR_BICYCLE";
        public const string BUS = "NOT_FOR_BUS";
        public const string CAR = "NOT_FOR_CAR";
        public const string DELIVERY = "NOT_FOR_DELIVERY";
        public const string EMERGENCY = "NOT_FOR_EMERGENCY";
        public const string PEDESTRIAN = "NOT_FOR_PEDESTRIAN";
        public const string TRUCK = "NOT_FOR_TRUCK";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        private const int MIN_LENGTH = 5;
        public const string AND_STATUS = "AND_STATUS";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        [MappedField(STATUS)]
        public virtual int? Status { set; get; }

        [MappedField(NETWORK_CLASS)]
        public virtual int? NetworkClass { set; get; }

        [MappedField(CLASS)]
        public virtual int? Class { set; get; }

        [MappedField(CATEGORY)]
        public virtual int? Category { set; get; }

        [MappedField(FILTER_LEVEL)]
        public virtual int? FilterLevel { set; get; }

        [MappedField(DESIGN)]
        public virtual int? Design { set; get; }

        [MappedField(DIRECTION)]
        public virtual int? Direction { set; get; }

        [MappedField(TOLL_TYPE)]
        public virtual int? TollType { set; get; }

        [MappedField(DIVIDER)]
        public virtual int? Divider { set; get; }

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(SECTION)]
        public virtual string Section { set; get; }

        [MappedField(POSTCODE)]
        public virtual string Postcode { set; get; }

        [MappedField(CITY)]
        public virtual string City { set; get; }

        [MappedField(SUB_CITY)]
        public virtual string SubCity { set; get; }

        [MappedField(STATE)]
        public virtual string State { set; get; }

        [MappedField(FROM_ID)]
        public virtual int? FromNodeId { set; get; }

        [MappedField(TO_ID)]
        public virtual int? ToNodeId { set; get; }

        [MappedField(SPEED_LIMIT)]
        public virtual int? SpeedLimit { set; get; }

        [MappedField(WEIGHT)]
        public virtual double? Weight { set; get; }

        [MappedField(ELEVATION)]
        public virtual int? Elevation { set; get; }

        [MappedField(LENGTH)]
        public virtual double? Length { set; get; }

        [MappedField(WIDTH)]
        public virtual double? Width { set; get; }

        [MappedField(HEIGHT)]
        public virtual double? Height { set; get; }

        [MappedField(LANES)]
        public virtual int? NumOfLanes { set; get; }

        [MappedField(BICYCLE)]
        public virtual int? Bicycle { set; get; }

        [MappedField(BUS)]
        public virtual int? Bus { set; get; }

        [MappedField(CAR)]
        public virtual int? Car { set; get; }

        [MappedField(DELIVERY)]
        public virtual int? Delivery { set; get; }

        [MappedField(EMERGENCY)]
        public virtual int? Emergency { set; get; }

        [MappedField(PEDESTRIAN)]
        public virtual int? Pedestrian { set; get; }

        [MappedField(TRUCK)]
        public virtual int? Truck { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GStreet street)
        {
            this.ConstructionStatus = street.ConstructionStatus;
            this.Status = street.Status;
            this.NetworkClass = street.NetworkClass;
            this.Class = street.Class;
            this.Category = street.Category;
            this.FilterLevel = street.FilterLevel;
            this.Design = street.Design;
            this.Direction = street.Direction;
            this.TollType = street.TollType;
            this.Divider = street.Divider;
            this.Source = street.Source;

            this.Type = street.Type;
            this.Name = street.Name;
            this.Name2 = street.Name2;
            this.Section = street.Section;
            this.Postcode = street.Postcode;
            this.City = street.City;
            this.SubCity = street.SubCity;
            this.State = street.State;

            this.SpeedLimit = street.SpeedLimit;
            this.Weight = street.Weight;
            this.Elevation = street.Elevation;
            this.Length = street.Length;
            this.Width = street.Width;
            this.Height = street.Height;
            this.NumOfLanes = street.NumOfLanes;

            this.FromNodeId = street.FromNodeId;
            this.ToNodeId = street.ToNodeId;

            this.Bicycle = street.Bicycle;
            this.Bus = street.Bus;
            this.Car = street.Car;
            this.Delivery = street.Delivery;
            this.Emergency = street.Emergency;
            this.Pedestrian = street.Pedestrian;
            this.Truck = street.Truck;

            this.DateCreated = street.DateCreated;
            this.CreatedBy = street.CreatedBy;
            this.DateUpdated = street.DateUpdated;
            this.UpdatedBy = street.UpdatedBy;

            this.AndStatus = street.AndStatus;
            this.UpdateStatus = street.UpdateStatus;

            this.ObjectId_Ori = street.OID;
            this.LogDate = DateTime.Now;
            this.LogType = street.UpdateStatus;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

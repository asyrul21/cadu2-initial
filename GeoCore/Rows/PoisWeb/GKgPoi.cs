﻿using System;
using System.Collections.Generic;
using System.Linq;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Rows.PoisWeb
{
    public class GKgPoi : GPoiWeb
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_WEB_POI";
    }
}

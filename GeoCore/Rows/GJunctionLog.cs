﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GJunctionLog : GRow
    {
        public const string TYPE = "JUNCTION_TYPE";
        public const string NAME = "JUNCTION_NAME";
        public const string SOURCE = "SOURCE";
        public const string UPDATE_STATUS = "STATUS";
        public const string AREA_ID = "AREA_ID";
        public const string AND_STATUS = "AND_STATUS";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GJunction Junction)
        {
            this.Type = Junction.Type;
            this.Name = Junction.Name;
            this.Source = Junction.Source;
            this.AreaId = Junction.AreaId;
            this.UpdateStatus = Junction.UpdateStatus;
            this.AreaId = Junction.AreaId;
            this.AndStatus = Junction.AndStatus;

            this.DateCreated = Junction.DateCreated;
            this.CreatedBy = Junction.CreatedBy;
            this.DateUpdated = Junction.DateUpdated;
            this.UpdatedBy = Junction.UpdatedBy;

            this.ObjectId_Ori = Junction.OID;
            this.LogDate = DateTime.Now;
            this.LogType = Junction.UpdateStatus;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

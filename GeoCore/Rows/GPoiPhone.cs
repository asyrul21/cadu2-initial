﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Rows
{
    public abstract class GPoiPhone : GRow
    {
        public static Regex WestFormat = GPhone.WestFormat;

        public static Regex EastFormat = GPhone.EastFormat;

        public static Regex MobileFormat = GPhone.MobileFormat;

        public static Regex TollFreeFormat = GPhone.TollFreeFormat;

        public const string REF_ID = "REF_ID";
        public const string NUMBER = "PHONE_NUMBER";

        [MappedField(NUMBER)]
        public virtual string Number { set; get; }

        [MappedField(REF_ID)]
        public virtual int? RefId { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public abstract IEnumerable<string> GetAreaCodes();

        public abstract Regex GetFormat();

        public virtual Regex GetMobileFormat()
        {
            return MobileFormat;
        }

        public virtual Regex GetTollFreeFormat()
        {
            return TollFreeFormat;
        }

        public virtual GPhone GetPhone()
        {
            if (!string.IsNullOrEmpty(Number))
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                Query<GPhone> query = new Query<GPhone>();
                query.Obj.Number = Number;

                IEnumerable<GPhone> phones = repo.Search(query);
                if (phones.Count() > 0)
                {
                    return phones.First();
                }
            }

            return null;
        }
    }
}

﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GKgBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_LOG_BUILDING_GROUP";
    }
}

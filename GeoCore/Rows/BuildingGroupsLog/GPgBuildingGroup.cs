﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GPgBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "PG_LOG_BUILDING_GROUP";
    }
}

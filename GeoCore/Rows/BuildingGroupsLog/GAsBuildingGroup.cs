﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GAsBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "AS_LOG_BUILDING_GROUP";
    }
}

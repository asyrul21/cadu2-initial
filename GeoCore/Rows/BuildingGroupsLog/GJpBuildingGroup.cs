﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GJpBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_LOG_BUILDING_GROUP";
    }
}

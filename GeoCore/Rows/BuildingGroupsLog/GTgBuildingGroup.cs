﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GTgBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "TG_LOG_BUILDING_GROUP";
    }
}

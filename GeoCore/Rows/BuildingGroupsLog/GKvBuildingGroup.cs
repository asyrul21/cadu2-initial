﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GKvBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_LOG_BUILDING_GROUP";
    }
}

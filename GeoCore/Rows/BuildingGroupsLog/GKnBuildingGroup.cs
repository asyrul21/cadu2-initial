﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GKnBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KN_LOG_BUILDING_GROUP";
    }
}

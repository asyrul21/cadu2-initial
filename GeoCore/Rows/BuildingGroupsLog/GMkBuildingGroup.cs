﻿
namespace Geomatic.Core.Rows.BuildingGroupsLog
{
    public class GMkBuildingGroup : GBuildingGroupLog
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "MK_LOG_BUILDING_GROUP";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetTollType : GReference
    {
        private static List<GStreetTollType> _streetTollTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_TOLLTYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "NORMAL STREET";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetTollType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetTollType> query = new Query<GStreetTollType>();
                query.Obj.Code = code;

                IEnumerable<GStreetTollType> streetTollType = repo.Search(query);
                if (streetTollType.Count() > 0)
                {
                    return streetTollType.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetTollType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetTollType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetTollTypes = repo.Search<GStreetTollType>().ToList();
            }
            else
            {
                if (_streetTollTypes == null)
                {
                    _streetTollTypes = repo.Search<GStreetTollType>().ToList();
                }
            }
            return _streetTollTypes;
        }
    }
}

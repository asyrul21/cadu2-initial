﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Rows
{
    public static class RowExt
    {
        public static IEnumerable<string> GetFieldNames(this IRow row)
        {
            for (int count = 0; count < row.Fields.FieldCount; count++)
            {
                IField field = row.Fields.get_Field(count);
                if (field.Editable)
                {
                    yield return field.Name;
                }
            }
        }

        public static object GetValue(this IRow row, string fieldName)
        {
            int? index = row.Fields.FindField(fieldName);
            if (!index.HasValue)
            {
                throw new Exception(string.Format("Field {0} not found", fieldName));
            }
            return row.get_Value(index.Value);
        }

        public static void SetValue(this IRow row, string fieldName, object value)
        {
            int? index = row.Fields.FindField(fieldName);
            if (!index.HasValue)
            {
                throw new Exception(string.Format("Field {0} not found", fieldName));
            }
            row.set_Value(index.Value, value);
        }
    }
}

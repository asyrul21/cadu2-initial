﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;


namespace Geomatic.Core.Rows
{
    public class GTollRouteType : GReference
    {
        private static List<GTollRouteType> _tollRouteTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_TOLLROUTETYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GTollRouteType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GTollRouteType> query = new Query<GTollRouteType>();
                query.Obj.Code = code;

                IEnumerable<GTollRouteType> tollType = repo.Search(query);
                if (tollType.Count() > 0)
                {
                    return tollType.First();
                }
            }

            return null;
        }

        public static IEnumerable<GTollRouteType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GTollRouteType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _tollRouteTypes = repo.Search<GTollRouteType>().ToList();
            }
            else
            {
                if (_tollRouteTypes == null)
                {
                    _tollRouteTypes = repo.Search<GTollRouteType>().ToList();
                }
            }
            return _tollRouteTypes;
        }
    }
}

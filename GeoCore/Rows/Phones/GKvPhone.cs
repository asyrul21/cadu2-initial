﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Rows.Phones
{
    public class GKvPhone : GPhone
    {
        public static IEnumerable<string> AreaCodes = new List<string> { "003" };

        public static Regex Format = new Regex(@"^(003)\d{8}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KV_TEL_NO";

        public override IEnumerable<string> GetAreaCodes()
        {
            return AreaCodes;
        }

        public override Regex GetFormat()
        {
            return Format;
        }
    }
}

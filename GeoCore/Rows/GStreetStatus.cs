﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GStreetStatus : GReference
    {
        private static List<GStreetStatus> _streetStatus;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_STREETSTATUS";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "PUBLIC";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GStreetStatus Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GStreetStatus> query = new Query<GStreetStatus>();
                query.Obj.Code = code;

                IEnumerable<GStreetStatus> status = repo.Search(query);
                if (status.Count() > 0)
                {
                    return status.First();
                }
            }

            return null;
        }

        public static IEnumerable<GStreetStatus> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreetStatus> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _streetStatus = repo.Search<GStreetStatus>().ToList();
            }
            else
            {
                if (_streetStatus == null)
                {
                    _streetStatus = repo.Search<GStreetStatus>().ToList();
                }
            }
            return _streetStatus;
        }
    }
}

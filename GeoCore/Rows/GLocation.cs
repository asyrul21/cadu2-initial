﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;

namespace Geomatic.Core.Rows
{
    public class GLocation : GRow, IUserCreate, IUserUpdate
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        // added by asyrul
        private static List<GLocation> _allLocation;
        // added end

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ML_LOCATION";

        public const string NIS_ID = "NIS_ID";
        public const string NEW_SECTION = "NEW_SECTION";
        public const string NEW_CITY = "NEW_CITY";
        public const string NEW_STATE = "NEW_STATE";
        public const string ORIGINAL_SECTION = "ORIGINAL_SECTION";
        public const string ORIGINAL_CITY = "ORIGINAL_CITY";
        public const string ORIGINAL_STATE = "ORIGINAL_STATE";
        public const string NEW_FLAG = "NEW_FLAG";
        public const string DEL_FLAG = "DEL_FLAG";

        [MappedField(NIS_ID)]
        public virtual int? NisId { set; get; }

        [MappedField(NEW_SECTION)]
        public virtual string Section { set; get; }

        [MappedField(NEW_CITY)]
        public virtual string City { set; get; }

        [MappedField(NEW_STATE)]
        public virtual string State { set; get; }

        [MappedField(ORIGINAL_SECTION)]
        public virtual string OriginalSection { set; get; }

        [MappedField(ORIGINAL_CITY)]
        public virtual string OriginalCity { set; get; }

        [MappedField(ORIGINAL_STATE)]
        public virtual string OriginalState { set; get; }

        [MappedField(NEW_FLAG)]
        public virtual int? NewFlag { set; get; }

        public virtual string NewFlagValue
        {
            get
            {
                return NewFlag == 1 ? "Yes" : "No";
            }
        }

        [MappedField(DEL_FLAG)]
        public virtual int? DelFlag { set; get; }

        public virtual string DelFlagValue
        {
            get
            {
                return DelFlag == 1 ? "Yes" : "No";
            }
        }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        //added by asyrul

        public static IEnumerable<GLocation> GetAllSectionByStateKV(String liststate, bool updated)
        //public static IEnumerable<GLocation> GetAllSectionByState(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GLocation> query = new Query<GLocation>();
            query.AddClause(() => query.Obj.State, "IN (" + liststate + ")");
            _allLocation = repo.Search(query).ToList();
            return _allLocation;
        }

        public static IEnumerable<GLocation> GetAllSectionByState(String segment, bool updated)
        //public static IEnumerable<GLocation> GetAllSectionByState(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GLocation> query = new Query<GLocation>();
            //query.Obj.State = state;
          
            // added by noraini - OKT 2019
            if (segment == "AS")
            { query.AddClause(() => query.Obj.State, "IN ('KD','PS')"); }
            else if (segment == "JH")
            { query.AddClause(() => query.Obj.State, "IN ('JH')"); }
            else if (segment == "JP")
            { query.AddClause(() => query.Obj.State, "IN ('PK')"); }
            else if (segment == "KG")
            { query.AddClause(() => query.Obj.State, "IN ('SW')"); }
            else if (segment == "KK")
            { query.AddClause(() => query.Obj.State, "IN ('SB','LB')"); }
            else if (segment == "KN")
            { query.AddClause(() => query.Obj.State, "IN ('PH')"); }
            else if (segment == "KV")
            { query.AddClause(() => query.Obj.State, "IN ('SL','WP')"); }
            else if (segment == "MK")
            { query.AddClause(() => query.Obj.State, "IN ('MK','NS')"); }
            else if (segment == "PG")
            { query.AddClause(() => query.Obj.State, "IN ('PP')"); }
            else if (segment == "TG")
            { query.AddClause(() => query.Obj.State, "IN ('TG','KN')"); }

            _allLocation = repo.Search(query).ToList();
            // end added
            return _allLocation;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;


namespace Geomatic.Core.Rows
{
    public class GVehicleType : GReference
    {
        private static List<GVehicleType> _vehicleTypes;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_VEHICLETYPE";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GVehicleType Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GVehicleType> query = new Query<GVehicleType>();
                query.Obj.Code = code;

                IEnumerable<GVehicleType> vehicleType = repo.Search(query);
                if (vehicleType.Count() > 0)
                {
                    return vehicleType.First();
                }
            }

            return null;
        }

        public static IEnumerable<GVehicleType> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GVehicleType> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _vehicleTypes = repo.Search<GVehicleType>().ToList();
            }
            else
            {
                if (_vehicleTypes == null)
                {
                    _vehicleTypes = repo.Search<GVehicleType>().ToList();
                }
            }
            return _vehicleTypes;
        }
    }
}

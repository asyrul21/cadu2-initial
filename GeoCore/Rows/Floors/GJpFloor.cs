﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Rows.Floors
{
    public class GJpFloor : GFloor
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "JP_ADM_FLOOR_ADDRESS";
    }
}

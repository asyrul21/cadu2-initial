﻿using System;
using Earthworm;
using Geomatic.Core.Features;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Rows
{
    public abstract class GBuildingLog : GRow
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string CODE = "BUILDING_CODE";
        public const string NAME = "BUILDING_NAME";
        public const string NAME2 = "BUILDING_NAME2";
        public const string PROPERTY_ID = "PROPERTY_ID";
        public const string GROUP_ID = "BUILDING_GROUP_ID";
        public const string NUM_FLOOR = "NUM_FLOOR";
        public const string UNIT = "FLOOR_NUM_UNIT";
        public const string SPACE = "UNIT_SPACE";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        public const string AND_STATUS = "AND_STATUS";
        public const string FORECAST_NUM_UNIT = "FORECAST_NUM_UNIT";
        public const string FORECAST_SOURCE = "FORECAST_SOURCE";

        public const string NAME_DISPLAYED = "NAME_DISPLAYED";
        public const string TEL_NO = "TEL_NO";
        public const string OTHER_TEL_NO = "OTHER_TEL_NO";
        public const string TEL_NO_OWNER = "TEL_NO_OWNER";
        public const string IPID = "IPID";
        public const string PROPERTY_IPID = "PROPERTY_IPID";

        public const string LOG_TYPE = "LOG_TYPE";
        public const string LOG_USER = "LOG_USER";
        public const string LOG_DATE = "LOG_DATE";
        public const string OBJECTID_ORI = "OBJECTID_ORI";

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(PROPERTY_ID)]
        public virtual int? PropertyId { set; get; }

        [MappedField(GROUP_ID)]
        public virtual int? GroupId { set; get; }

        [MappedField(NUM_FLOOR)]
        public virtual int? NumberOfFloor { set; get; }

        [MappedField(UNIT)]
        public virtual int? Unit { set; get; }

        [MappedField(SPACE)]
        public virtual double? Space { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(FORECAST_NUM_UNIT)]
        public virtual int? ForecastNumUnit { set; get; }

        [MappedField(FORECAST_SOURCE)]
        public virtual int? ForecastSource { set; get; }

        // new fields
        [MappedField(NAME_DISPLAYED)]
        public virtual string NameDisplayed { set; get; }

        [MappedField(TEL_NO)]
        public virtual int? TelNo { set; get; }

        [MappedField(OTHER_TEL_NO)]
        public virtual int? OtherTelNo { set; get; }

        [MappedField(TEL_NO_OWNER)]
        public virtual string TelNoOwner { set; get; }

        [MappedField(IPID)]
        public virtual int? Ipid { set; get; }

        [MappedField(PROPERTY_IPID)]
        public virtual int? PropertyIpid { set; get; }

        // log fields
        [MappedField(LOG_TYPE)]
        public virtual int? LogType { set; get; }

        [MappedField(LOG_USER)]
        public virtual string LogUser { set; get; }

        [MappedField(LOG_DATE)]
        public virtual DateTime LogDate { set; get; }

        [MappedField(OBJECTID_ORI)]
        public virtual int? ObjectId_Ori { set; get; }

        public virtual void CopyFrom(GBuilding Building)
        {
            this.ConstructionStatus = Building.ConstructionStatus;
            this.NavigationStatus = Building.NavigationStatus;
            this.Code = Building.Code;
            this.Name = Building.Name;
            this.Name2 = Building.Name2;
            this.PropertyId = Building.PropertyId;
            this.GroupId = Building.GroupId;
            this.NumberOfFloor = Building.NumberOfFloor;
            this.Unit = Building.Unit;
            this.Space = Building.Space;
            this.Source = Building.Source;
            this.AreaId = Building.AreaId;
            this.UpdateStatus = Building.UpdateStatus;
            this.AndStatus = Building.AndStatus;
            this.ForecastNumUnit = Building.ForecastNumUnit;
            this.ForecastSource = Building.ForecastSource;

            this.DateCreated = Building.DateCreated;
            this.CreatedBy = Building.CreatedBy;
            this.DateUpdated = Building.DateUpdated;
            this.UpdatedBy = Building.UpdatedBy;

            this.NameDisplayed = "1";
            this.TelNo = 0;
            this.OtherTelNo = 0;
            this.TelNoOwner = "";
            this.Ipid = 0;
            this.PropertyIpid = 0;

            this.ObjectId_Ori = Building.OID;
            this.LogType = Building.UpdateStatus;
            this.LogDate = DateTime.Now;
            string strLogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            if (strLogUser.Length <= 50)
            {
                this.LogUser = Session.User.GetGroup().Name + "/" + Session.User.Name;
            }
            else
            {
                this.LogUser = Session.User.Name;
            }
        }
    }
}

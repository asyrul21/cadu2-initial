﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GUserGroup : GRow
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_USER_TYPE";

        public const string NAME = "NAME";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        public override string CreatedBy { set; get; }

        public override string DateCreated { set; get; }

        public override string UpdatedBy { set; get; }

        public override string DateUpdated { set; get; }

        public static IEnumerable<GUserGroup> GetAll()
        {
            RepositoryFactory repo = new RepositoryFactory();
            return repo.Search<GUserGroup>();
        }

        public IEnumerable<GUserGroupFunction> GetGroupFunctions()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUserGroupFunction> query = new Query<GUserGroupFunction>();
            query.Obj.GroupId = OID;
            return repo.Search(query);
        }

        public IEnumerable<GFunction> GetFunctions()
        {
            foreach (GUserGroupFunction userGroupFunction in GetGroupFunctions())
            {
                GFunction function = userGroupFunction.GetFunction();
                if (function != null)
                {
                    yield return function;
                }
            }
        }

        public IEnumerable<GUser> GetUsers()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUser> query = new Query<GUser>();
            query.Obj.GroupId = OID;
            return repo.Search(query);
        }

        public bool HasFunction(string command)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUserGroupFunction> query = new Query<GUserGroupFunction>();
            query.Obj.GroupId = OID;

            return repo.Count<GUserGroupFunction>(query) > 0;
        }

        public bool HasUsers()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GUser> query = new Query<GUser>();
            query.Obj.GroupId = OID;

            return repo.Count<GUser>(query) > 0;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

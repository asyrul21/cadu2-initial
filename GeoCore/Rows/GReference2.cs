﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Rows
{
    public abstract class GReference2 : GReference
    {
        public new virtual string Code { set; get; }
    }
}

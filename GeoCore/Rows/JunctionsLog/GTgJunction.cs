﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GTgJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public const string TABLE_NAME = "TG_LOG_JUNCTION";
    }
}

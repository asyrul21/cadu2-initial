﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GKkJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public const string TABLE_NAME = "KK_LOG_JUNCTION";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GJpJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public const string TABLE_NAME = "JP_LOG_JUNCTION";

    }
}

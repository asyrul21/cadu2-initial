﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GPgJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public const string TABLE_NAME = "PG_LOG_JUNCTION";
    }
}

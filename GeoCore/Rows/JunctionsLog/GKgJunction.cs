﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GKgJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public const string TABLE_NAME = "KG_LOG_JUNCTION";
    }
}

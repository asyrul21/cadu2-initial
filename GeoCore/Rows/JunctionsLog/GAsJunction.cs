﻿using Geomatic.Core.Repositories;

namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GAsJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public const string TABLE_NAME = "AS_LOG_JUNCTION";

    }
}

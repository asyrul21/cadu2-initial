﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GKnJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public const string TABLE_NAME = "KN_LOG_JUNCTION";
    }
}

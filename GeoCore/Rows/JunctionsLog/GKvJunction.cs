﻿
namespace Geomatic.Core.Rows.Junctions
{
    public class GKvJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public const string TABLE_NAME = "KV_LOG_JUNCTION";    
    }
}

﻿
namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GJhJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public const string TABLE_NAME = "JH_LOG_JUNCTION";
    }
}

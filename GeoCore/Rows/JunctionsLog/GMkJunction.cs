﻿

namespace Geomatic.Core.Rows.JunctionsLog
{
    public class GMkJunction : GJunctionLog
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public const string TABLE_NAME = "MK_LOG_JUNCTION";
    }
}
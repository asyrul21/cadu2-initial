﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GSource : GReference
    {
        private static List<GSource> _sources;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_SOURCE";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "INTERNAL";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GSource Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GSource> query = new Query<GSource>();
                query.Obj.Code = code;

                IEnumerable<GSource> source = repo.Search(query);
                if (source.Count() > 0)
                {
                    return source.First();
                }
            }

            return null;
        }

        public static IEnumerable<GSource> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GSource> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _sources = repo.Search<GSource>().ToList();
            }
            else
            {
                if (_sources == null)
                {
                    _sources = repo.Search<GSource>().ToList();
                }
            }
            return _sources;
        }

    }
}

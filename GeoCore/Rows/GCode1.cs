﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GCode1 : GCode
    {
        private static List<GCode1> _code1s;
        private List<GCode2> _code2s;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "CODE_DESC1";

        public const string CODE1 = "CODE";

        [MappedField(CODE1)]
        public virtual string Code1 { set; get; }

        public static GCode1 Get(string code1)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode1> query = new Query<GCode1>();
            query.Obj.Code1 = code1;

            IEnumerable<GCode1> codes = repo.Search(query);
            if (codes.Count() > 0)
            {
                return codes.First();
            }

            return null;
        }

        public static IEnumerable<GCode1> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GCode1> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _code1s = repo.Search<GCode1>().ToList();
            }
            else
            {
                if (_code1s == null)
                {
                    _code1s = repo.Search<GCode1>().ToList();
                }
            }
            return _code1s;
        }

        public virtual IEnumerable<GCode2> GetCode2s()
        {
            return GetCode2s(true);
        }

        public virtual IEnumerable<GCode2> GetCode2s(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GCode2> query = new Query<GCode2>();
            query.Obj.Code1 = Code1;

            if (updated)
            {
                _code2s = repo.Search(query).ToList();
            }
            else
            {
                if (_code2s == null)
                {
                    _code2s = repo.Search(query).ToList();
                }
            }

            return _code2s;
        }
    }
}

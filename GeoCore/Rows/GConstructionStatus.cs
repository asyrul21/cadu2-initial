﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Rows
{
    public class GConstructionStatus : GReference
    {
        private static List<GConstructionStatus> _constructionStatus;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "C_ADM_CONSSTATUS";

        public const string CODE = "CODENUM";
        public const string NAME = "CODETEXT";

        public const string DEFAULT = "ASB";

        [MappedField(CODE)]
        public override int? Code { set; get; }

        [MappedField(NAME)]
        public override string Name { set; get; }

        public static GConstructionStatus Get(int? code)
        {
            if (code.HasValue)
            {
                RepositoryFactory repo = new RepositoryFactory();
                Query<GConstructionStatus> query = new Query<GConstructionStatus>();
                query.Obj.Code = code;

                IEnumerable<GConstructionStatus> status = repo.Search(query);
                if (status.Count() > 0)
                {
                    return status.First();
                }
            }

            return null;
        }

        public static IEnumerable<GConstructionStatus> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GConstructionStatus> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _constructionStatus = repo.Search<GConstructionStatus>().ToList();
            }
            else
            {
                if (_constructionStatus == null)
                {
                    _constructionStatus = repo.Search<GConstructionStatus>().ToList();
                }
            }
            return _constructionStatus;
        }
    }
}

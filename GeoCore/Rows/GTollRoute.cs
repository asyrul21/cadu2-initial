﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;

namespace Geomatic.Core.Rows
{
    public class GTollRoute : GRow, IUserCreate, IUserUpdate
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "ADM_TOLL_ROUTE";

        public const string TOLL_TYPE = "TOLL_TYPE";
        public const string IN_TOLL_SEGMENT = "IN_TOLL_SEGMENT";
        public const string IN_TOLL_ID = "IN_TOLL_ID";
        public const string OUT_TOLL_SEGMENT = "OUT_TOLL_SEGMENT";
        public const string OUT_TOLL_ID = "OUT_TOLL_ID";

        [MappedField(TOLL_TYPE)]
        public virtual int? Type { set; get; }

        public virtual string TypeValue
        {
            get
            {
                GTollRouteType tollRouteType = GetTypeObj();
                return StringUtils.TrimSpaces((tollRouteType == null) ? string.Empty : tollRouteType.Name);
            }
        }

        [MappedField(IN_TOLL_SEGMENT)]
        public virtual string InSegment { set; get; }

        [MappedField(IN_TOLL_ID)]
        public virtual int? InID { set; get; }

        [MappedField(OUT_TOLL_SEGMENT)]
        public virtual string OutSegment { set; get; }

        [MappedField(OUT_TOLL_ID)]
        public virtual int? OutID { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        public virtual IEnumerable<GTollPrice> GetPrices()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollPrice> query = new Query<GTollPrice>(SegmentName);
            query.Obj.TollRouteID = this.OID;
            return repo.Search(query);
        }

        public virtual GTollRouteType GetTypeObj()
        {
            return GTollRouteType.Get(Type);
        }

        private GJunction GetJunction(SegmentName segmentName, int? id)
        {
            if (segmentName == SegmentName.None)
            {
                return null;
            }

            RepositoryFactory repo = new RepositoryFactory(segmentName);
            return id.HasValue ? repo.GetById<GJunction>(id.Value) : null;
        }

        public virtual GJunction GetInJunction()
        {
            return GetJunction(StringUtils.GetSegmentName(InSegment), InID);
        }


        public virtual GJunction GetOutJunction()
        {
            return GetJunction(StringUtils.GetSegmentName(OutSegment), OutID);
        }

        public virtual string GetInName()
        {
            GJunction junction = GetInJunction();
            return junction == null ? string.Empty : GetTollName(junction.Name);
        }

        public virtual string GetOutName()
        {
            GJunction junction = GetOutJunction();
            return junction == null ? string.Empty : GetTollName(junction.Name);
        }

        protected virtual string GetTollName(string tollName)
        {
            return string.IsNullOrEmpty(tollName) ? string.Empty : StringUtils.TrimSpaces(tollName.Replace("TOLL BOOTH", ""));
        }

        public void Init()
        {
            Type = 0;
        }
    }
}

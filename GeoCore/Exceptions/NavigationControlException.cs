﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Exceptions
{
    public class NavigationControlException : WarningException
    {
        public NavigationControlException()
            : this("Navigation ready lock.")
        {
        }

        public NavigationControlException(string message)
            : base(message)
        {
        }
    }
}

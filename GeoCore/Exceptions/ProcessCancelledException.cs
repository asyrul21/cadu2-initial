﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Geomatic.Core.Exceptions
{
    public class ProcessCancelledException : WarningException
    {
        public ProcessCancelledException()
            : this("Process cancelled")
        {
        }

        public ProcessCancelledException(string message)
            : base(message)
        {
        }
    }
}

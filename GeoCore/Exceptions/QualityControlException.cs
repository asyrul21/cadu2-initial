﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Geomatic.Core.Exceptions
{
    public class QualityControlException : WarningException
    {
        public QualityControlException(string message)
            : base(message)
        {
        }
    }
}

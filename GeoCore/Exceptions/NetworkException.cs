﻿using ESRI.ArcGIS.Geodatabase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Exceptions
{
    public class NetworkException : WarningException
    {
        public readonly IWorkspace Workspace;

        public NetworkException(IWorkspace workspace)
            : this(workspace, "Disconnected")
        {
        }

        public NetworkException(IWorkspace workspace, string message)
            : base(message)
        {
            Workspace = workspace;
        }
    }
}

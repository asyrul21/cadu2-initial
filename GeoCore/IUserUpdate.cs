﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public interface IUserUpdate
    {
        string UpdatedBy { set; get; }

        string DateUpdated { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class MultiStoreyValidator : Validator<GMultiStorey>
    {
        public MultiStoreyValidator(GMultiStorey multiStorey)
            : base(multiStorey)
        {
        }

        public static bool CheckBuildingId(int? id)
        {
            return id.HasValue && id.Value != -1;
        }

        [FieldToValidate]
        public bool IsBuildingIdValid
        {
            get
            {
                return CheckBuildingId(_obj.BuildingId);
            }
        }

        public static bool CheckFloor(string floor)
        {
            return !StringUtils.HasExtraSpaces(floor) && !string.IsNullOrEmpty(floor);
        }

        [FieldToValidate]
        public bool IsFloorValid
        {
            get
            {
                return CheckFloor(_obj.Floor);
            }
        }

        public static bool CheckApt(string apt)
        {
            return !StringUtils.HasExtraSpaces(apt) && !string.IsNullOrEmpty(apt);
        }

        [FieldToValidate]
        public bool IsAptValid
        {
            get
            {
                return CheckApt(_obj.Apartment);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class SectionBoundaryValidator : Validator<GSectionBoundary>
    {
        public SectionBoundaryValidator(GSectionBoundary sectionBoundary)
            : base(sectionBoundary)
        {
        }

        public static bool CheckName(string name)
        {
            return !StringUtils.HasExtraSpaces(name) && !string.IsNullOrEmpty(name) && (string.IsNullOrEmpty(name) ? true : !name.Contains("*"));
        }

        [FieldToValidate]
        public bool IsNameValid
        {
            get
            {
                return CheckName(_obj.Name);
            }
        }

        public static bool CheckCity(string city)
        {
            return !StringUtils.HasExtraSpaces(city) && !string.IsNullOrEmpty(city) && (string.IsNullOrEmpty(city) ? true : !city.Contains("*"));
        }

        [FieldToValidate]
        public bool IsCityValid
        {
            get
            {
                return CheckCity(_obj.City);
            }
        }

        public static bool CheckState(string state)
        {
            return !StringUtils.HasExtraSpaces(state) && !string.IsNullOrEmpty(state) && (string.IsNullOrEmpty(state) ? true : !state.Contains("*"));
        }

        [FieldToValidate]
        public bool IsStateValid
        {
            get
            {
                return CheckState(_obj.State);
            }
        }

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        [FieldToValidate]
        public bool IsSourceValid
        {
            get
            {
                return CheckSource(_obj.Source);
            }
        }
    }
}

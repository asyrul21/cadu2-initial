﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class BuildingGroupValidator : Validator<GBuildingGroup>
    {
        public BuildingGroupValidator(GBuildingGroup buildingGroup)
            : base(buildingGroup)
        {
        }

        public static bool CheckCode(string code)
        {
            return !StringUtils.HasExtraSpaces(code);
        }

        [FieldToValidate]
        public bool IsCodeValid
        {
            get
            {
                return CheckCode(_obj.Code);
            }
        }

        public static bool CheckName(string name)
        {
            bool isNameValid = true;
            isNameValid &= !string.IsNullOrEmpty(name);
            isNameValid &= !StringUtils.HasExtraSpaces(name);
            isNameValid &= (string.IsNullOrEmpty(name) ? true : !name.Contains("*"));
            return isNameValid;
        }

        [FieldToValidate]
        public bool IsNameValid
        {
            get
            {
                return CheckName(_obj.Name);
            }
        }

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        [FieldToValidate]
        public bool IsSourceValid
        {
            get
            {
                return CheckSource(_obj.Source);
            }
        }

        public static bool CheckStreetId(int? id)
        {
            return id.HasValue && id.Value != -1;
        }

        [FieldToValidate]
        public bool IsStreetIdValid
        {
            get
            {
                return CheckStreetId(_obj.StreetId);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Validators
{
    public interface IValidator
    {
        bool IsValid();

        IEnumerable<string> GetInvalidProperties();
    }
}

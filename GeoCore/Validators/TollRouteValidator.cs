﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class TollRouteValidator : Validator<GTollRoute>
    {
        public TollRouteValidator(GTollRoute tollRoute)
            : base(tollRoute)
        {
        }

        public static bool CheckType(int? type)
        {
            return type.HasValue;
        }

        [FieldToValidate]
        public bool IsTypeValid
        {
            get
            {
                return CheckType(_obj.Type);
            }
        }

        public static bool CheckInTollSegment(string inTollSegment)
        {
            return !string.IsNullOrEmpty(inTollSegment);
        }

        [FieldToValidate]
        public bool IsInTollSegmentValid
        {
            get
            {
                return CheckInTollSegment(_obj.InSegment);
            }
        }


        public static bool CheckInTollID(int? id)
        {
            return id.HasValue && id.Value != 0;
        }

        [FieldToValidate]
        public bool IsInTollIDValid
        {
            get
            {
                return CheckInTollID(_obj.InID);
            }
        }

        public static bool CheckOutTollSegment(string outTollSegment, int? type)
        {
            if (type == 1)
                return !string.IsNullOrEmpty(outTollSegment);
            else
                return true;
        }

        [FieldToValidate]
        public bool IsOutTollSegmentValid
        {
            get
            {
                return CheckOutTollSegment(_obj.OutSegment,_obj.Type);
            }
        }

        
        public static bool CheckOutTollID(int? id, int? type)
        {
            if (type == 1)
                return id.HasValue && id.Value != 0;
            else
                return true;
        }

        [FieldToValidate]
        public bool IsOutTollIDValid
        {
            get
            {
                return CheckOutTollID(_obj.OutID,_obj.Type);
            }
        }
    }
}

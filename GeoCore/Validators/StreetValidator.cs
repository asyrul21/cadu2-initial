﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using System.Text.RegularExpressions;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Validators
{
    public class StreetValidator : Validator<GStreet>
    {
        public StreetValidator(GStreet street)
            : base(street)
        {
        }

        public static bool CheckConstructionStatus(int? constructionStatus)
        {
            return constructionStatus.HasValue && constructionStatus != 0 && constructionStatus != null;
        }

        //[FieldToValidate]
        //public bool IsConstructionStatusValid
        //{
        //    get
        //    {
        //        return CheckConstructionStatus(_obj.ConstructionStatus);
        //    }
        //}

        public static bool CheckStatus(int? status)
        {
            return status.HasValue;
        }

        [FieldToValidate]
        public bool IsStatusValid
        {
            get
            {
                return CheckStatus(_obj.Status);
            }
        }

        public static bool CheckFilterLevel(int? filterLevel)
        {
            return filterLevel.HasValue;
        }

        [FieldToValidate]
        public bool IsFilterLevelValid
        {
            get
            {
                return CheckFilterLevel(_obj.FilterLevel);
            }
        }

        public static bool CheckDesign(int? design)
        {
            return design.HasValue;
        }

        [FieldToValidate]
        public bool IsDesignValid
        {
            get
            {
                return CheckDesign(_obj.Design);
            }
        }

        public static bool CheckDirection(int? direction)
        {
            return direction.HasValue;
        }

        [FieldToValidate]
        public bool IsDirectionValid
        {
            get
            {
                return CheckDirection(_obj.Direction);
            }
        }

        public static bool CheckTollType(int? tollType)
        {
            return tollType.HasValue;
        }

        [FieldToValidate]
        public bool IsTollTypeValid
        {
            get
            {
                return CheckTollType(_obj.TollType);
            }
        }

        public static bool CheckDivider(int? divider)
        {
            return divider.HasValue;
        }

        [FieldToValidate]
        public bool IsDividerValid
        {
            get
            {
                return CheckDivider(_obj.Divider);
            }
        }

        public static bool CheckLane(int? lane)
        {
            return lane.Value > 0;
        }

        [FieldToValidate]
        public bool IsLaneValid
        {
            get
            {
                return CheckLane(_obj.NumOfLanes);
            }
        }

        public static bool CheckType(int? type)
        {
            //return type.HasValue;

            //added by asyrul
            if(type != 1 && type != 2)
            {
                return true;
            }
            return false;
            //added end
        }

        [FieldToValidate]
        public bool IsTypeValid
        {
            get
            {
                //Console.WriteLine("Obj type: " + _obj.Type);
                return CheckType(_obj.Type);
            }
        }

        public static bool CheckName(int? status, string name)
        {
            bool isNameValid = true;
            if (status != 4 && status != 1) 
            {
                isNameValid &= !string.IsNullOrEmpty(name);
            }
            isNameValid &= !StringUtils.HasExtraSpaces(name);
            isNameValid &= (string.IsNullOrEmpty(name) ? true : !name.Contains("*"));
            return isNameValid;
        }
        
        //[FieldToValidate]
        //public bool IsNameValid
        //{
        //    get
        //    {
        //        return CheckName(_obj.Status, _obj.Name);
        //    }
        //}
        
        public static bool CheckName2(string name2)
        {
            return !StringUtils.HasExtraSpaces(name2) && (string.IsNullOrEmpty(name2) ? true : !name2.Contains("*"));
        }
        
        //[FieldToValidate]
        //public bool IsName2Valid
        //{
        //    get
        //    {
        //        return CheckName2(_obj.Name2);
        //    }
        //}

        public static bool CheckPostcode(string postcode)
        {
            bool isPostcodeValid = true;
            if (string.IsNullOrEmpty(postcode))
            {
                return false;
            }
            isPostcodeValid &= Regex.IsMatch(postcode, @"^[0-9]{5}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            isPostcodeValid &= !Regex.IsMatch(postcode, @"^0{5}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return isPostcodeValid;
        }

        [FieldToValidate]
        public bool IsPostcodeValid
        {
            get
            {
                return CheckPostcode(_obj.Postcode);
            }
        }

        public static bool CheckCity(string city)
        {
            return !StringUtils.HasExtraSpaces(city) && !string.IsNullOrEmpty(city) && (string.IsNullOrEmpty(city) ? true : !city.Contains("*")); 
        }

        [FieldToValidate]
        public bool IsCityValid
        {
            get
            {
                return CheckCity(_obj.City);
            }
        }

        public static bool CheckSubCity(string subCity)
        {
            return !StringUtils.HasExtraSpaces(subCity) && (string.IsNullOrEmpty(subCity) ? true : !subCity.Contains("*"));
        }

        // noraini ali - Jun 2020 - Skip validation on SubCity
        //[FieldToValidate]
        //public bool IsSubCityValid
        //{
        //    get
        //    {
        //        return CheckSubCity(_obj.SubCity);
        //    }
        //}

        public static bool CheckState(string state)
        {
            return !StringUtils.HasExtraSpaces(state) && !string.IsNullOrEmpty(state) && (string.IsNullOrEmpty(state) ? true : !state.Contains("*"));
        }

        [FieldToValidate]
        public bool IsStateValid
        {
            get
            {
                return CheckState(_obj.State);
            }
        }

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        //[FieldToValidate]
        //public bool IsSourceValid
        //{
        //    get
        //    {
        //        return CheckSource(_obj.Source);
        //    }
        //}
    }
}

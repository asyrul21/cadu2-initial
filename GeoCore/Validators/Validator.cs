﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Geomatic.Core.Validators
{
    public class Validator<T> : IValidator where T : IGObject
    {
        protected T _obj;

        public Validator()
            : this(default(T))
        {
        }

        public Validator(T obj)
        {
            _obj = obj;
        }

        public virtual bool IsValid()
        {
            bool isValid = true;
            foreach (bool valid in GetValidatingValues())
            {
                isValid &= valid;
            }
            return isValid;
        }

        protected IEnumerable<bool> GetValidatingValues()
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                foreach (object attribute in propertyInfo.GetCustomAttributes(true))
                {
                    if (attribute is FieldToValidate)
                    {
                        object obj = propertyInfo.GetValue(this, null);
                        if (!(bool)obj)
                        {
                            Console.WriteLine("Failed validation on: " + attribute);
                        }
                        yield return (bool)obj;
                    }
                }
            }
        }

        public IEnumerable<string> GetInvalidProperties()
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                foreach (object attribute in propertyInfo.GetCustomAttributes(true))
                {
                    if (attribute is FieldToValidate)
                    {
                        bool isValid = (bool)propertyInfo.GetValue(this, null);
                        if (isValid)
                        {
                            continue;
                        }
                        yield return propertyInfo.Name;
                    }
                }
            }
        }
    }
}

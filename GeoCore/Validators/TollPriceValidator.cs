﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class TollPriceValidator : Validator<GTollPrice>
    {
        public TollPriceValidator(GTollPrice tollPrice)
            : base(tollPrice)
        {
        }

        public static bool CheckVehicleType(int? vehicleType)
        {
            return vehicleType.HasValue;
        }

        [FieldToValidate]
        public bool IsVehicleTypeValid
        {
            get
            {
                return CheckVehicleType(_obj.VehicleType);
            }
        }

        public static bool CheckPrice(double? price)
        {
            bool validPrice = true;
            validPrice &= price > 0;


            return validPrice;
        }

        [FieldToValidate]
        public bool IsPriceValid
        {
            get
            {
                return CheckPrice(_obj.Price);
            }
        }
    }
}

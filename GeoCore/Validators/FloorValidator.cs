﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class FloorValidator : Validator<GFloor>
    {
        public FloorValidator(GFloor floor)
            : base(floor)
        {
        }

        //public static bool CheckNumber(string number)
        //{
        //    return CheckNumber(number, false);
        //}

        public static bool CheckNumber(string number)
        {
            return !StringUtils.HasExtraSpaces(number) && !string.IsNullOrEmpty(number);
        }
        //public static bool CheckNumber(string number)
        //{
        //    bool isValidFloor = true;

        //    if (!StringUtils.HasExtraSpaces(number) && !string.IsNullOrEmpty(number))
        //    {
        //        //RepositoryFactory repo = new RepositoryFactory();
        //        //Query<GFloor> query = new Query<GFloor>();
        //        //query.Obj.Number = number;
        //        //isValidFloor &= repo.Count(query) > 0;
        //        //HasNumberFloor();


        //    }
        //    return isValidFloor &= !StringUtils.HasExtraSpaces(number) && !string.IsNullOrEmpty(number);

        //    throw new QualityControlException("Floor already have one.");
        //}

        //public static bool CheckNumber(string number, bool hasFloor)
        //{
        //    bool isValidFloor = true;

        //    if (hasFloor)
        //    {
        //        if (!StringUtils.HasExtraSpaces(number) && !string.IsNullOrEmpty(number))
        //        {
        //            RepositoryFactory repo = new RepositoryFactory();
        //            Query<GFloor> query = new Query<GFloor>();
        //            query.Obj.Number = number.ToString();
        //            isValidFloor &= repo.Count(query) > 0;
        //        }
        //    }
        //    return isValidFloor &= !StringUtils.HasExtraSpaces(number) && !string.IsNullOrEmpty(number);
        //    throw new QualityControlException("Floor already have one.");
        //}

        [FieldToValidate]
        public bool IsNumberValid
        {
            get
            {
                return CheckNumber(_obj.Number);
            }
        }

        public static bool CheckUnitNumber(string unitnum)
        {
            return !StringUtils.HasExtraSpaces(unitnum) && !string.IsNullOrEmpty(unitnum);
        }

        [FieldToValidate]
        public bool IsUnitNumberValid
        {
            get
            {
                return CheckUnitNumber(_obj.NumUnit);
            }
        }

        public static bool CheckPropertyId(int? id)
        {
            return id.HasValue && id.Value != -1;
        }

        [FieldToValidate]
        public bool IsPropertyIdValid
        {
            get
            {
                return CheckPropertyId(_obj.PropertyId);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class StreetRestrictionValidator : Validator<GStreetRestriction>
    {
        public StreetRestrictionValidator(GStreetRestriction streetRestriction)
            : base(streetRestriction)
        {
        }

        public static bool CheckJunctionId(int? id)
        {
            return id.HasValue && id != 0;
        }

        [FieldToValidate]
        public bool IsJunctionIdValid
        {
            get
            {
                return CheckJunctionId(_obj.JunctionId);
            }
        }

        public static bool CheckStartId(int? id)
        {
            return id.HasValue && id != 0;
        }

        [FieldToValidate]
        public bool IsStartIdValid
        {
            get
            {
                return CheckStartId(_obj.StartId);
            }
        }

        public static bool CheckEndId1(int? id)
        {
            return id.HasValue && id != 0;
        }

        [FieldToValidate]
        public bool IsEndId1Valid
        {
            get
            {
                return CheckEndId1(_obj.EndId1);
            }
        }
    }
}

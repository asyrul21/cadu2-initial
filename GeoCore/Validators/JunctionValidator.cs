﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class JunctionValidator : Validator<GJunction>
    {
        public JunctionValidator(GJunction junction)
            : base(junction)
        {
        }

        public static bool CheckType(int? type)
        {
            bool isTypeValid = true;
            isTypeValid &= type.HasValue;

            return type.HasValue;
        }

        [FieldToValidate]
        public bool IsTypeValid
        {
            get
            {
                return CheckType(_obj.Type);
            }
        }

        public static bool CheckName(string name)
        {
            bool isNameValid = true;
            isNameValid &= !StringUtils.HasExtraSpaces(name);
            isNameValid &= (string.IsNullOrEmpty(name)? true : !name.Contains("*"));
            return isNameValid;
        }

        // noraini ali - Jun 2020 - Skip Name validation  - problem to update during create Work Area
        //[FieldToValidate]
        //public bool IsNameValid
        //{
        //    get
        //    {
        //        return CheckName(_obj.Name);
        //    }
        //}

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        // noraini ali - Jun 2020 - Skip source validation - problem to update during create Work Area
        //[FieldToValidate]
        //public bool IsSourceValid
        //{
        //    get
        //    {
        //        return CheckSource(_obj.Source);
        //    }
        //}
    }
}

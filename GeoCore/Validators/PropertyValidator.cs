﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Validators
{
    public class PropertyValidator : Validator<GProperty>
    {
        public PropertyValidator(GProperty property)
            : base(property)
        {
        }

        // noraini ali - Jun 2020 - comment to validate property type/Lot/House number because feature cannot update during create Work Area
        public static bool CheckConstructionStatus(int? constructionStatus)
        {
            return constructionStatus != 0 && constructionStatus != null;
        }
        
        //[FieldToValidate]
        //public bool IsConstructionStatusValid
        //{
        //    get
        //    {
        //        return CheckConstructionStatus(_obj.ConstructionStatus);
        //    }
        //}

        public static bool CheckType(int? type)
        {
            return CheckType(type, false);
        }
        
        public static bool CheckType(int? type, bool hasBuilding)
        {
            bool isValidType = true;
        
            if (hasBuilding)
            {
                if (type.HasValue)
                {
                    RepositoryFactory repo = new RepositoryFactory();
                    Query<GPropertyPermission> query = new Query<GPropertyPermission>();
                    query.Obj.Code = type.Value;
                    isValidType &= repo.Count(query) == 0;
                }
            }
            return isValidType &= type.HasValue;
        }
        
        [FieldToValidate]
        public bool IsTypeValid
        {
            get
            {
                return CheckType(_obj.Type, _obj.HasBuilding());
            }
        }

        // noraini ali - Jun 2020 - comment to validate property type/Lot/House number because feature cannot update during create Work Area
        public static bool CheckLotHouse(string lot, string house)
        {
            // Lot / House
            bool isLotHouseValid = true;
            isLotHouseValid =
                    //only house has value
                    (string.IsNullOrEmpty(lot) && !string.IsNullOrEmpty(house)) ||
                    //only lot has value
                    (!string.IsNullOrEmpty(lot) && string.IsNullOrEmpty(house));
        
                isLotHouseValid &= !StringUtils.HasExtraSpaces(lot) && (string.IsNullOrEmpty(lot) ? true : !lot.Contains("*"));
                isLotHouseValid &= !StringUtils.HasExtraSpaces(house) && (string.IsNullOrEmpty(house) ? true : !house.Contains("*"));
            return isLotHouseValid;
        }

        // noraini ali - Jun 2020 - Skip Lot/House validation - problem to update feature in Work Area 
        //[FieldToValidate]
        //public bool IsLotHouseValid
        //{
        //    get
        //    {
        //        return CheckLotHouse(_obj.Lot, _obj.House);
        //    }
        //}

        public static bool CheckYear(int? year)
        {
            return year.HasValue;
        }

        // noraini ali - Jun 2020 - Skip year install validation - problem to update feature in Work Area 
        //[FieldToValidate]
        //public bool IsYearValid
        //{
        //    get
        //    {
        //        return CheckYear(_obj.YearInstall);
        //    }
        //}

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        // noraini ali - Jun 2020 - Skip source validation - problem to update feature in Work Area 
        //[FieldToValidate]
        //public bool IsSourceValid
        //{
        //    get
        //    {
        //        return CheckSource(_obj.Source);
        //    }
        //}
    }
}

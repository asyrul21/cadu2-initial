﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class LandmarkValidator : Validator<GLandmark>
    {
        public LandmarkValidator(GLandmark landmark)
            : base(landmark)
        {
        }

        public static bool CheckCode(string code)
        {
            return !StringUtils.HasExtraSpaces(code);
        }

        // noraini ali - Jun 2020 - Skip Landmark code validation - problem to update feature in Work Area 
        //[FieldToValidate]
        //public bool IsCodeValid
        //{
        //    get
        //    {
        //        return CheckCode(_obj.Code);
        //    }
        //}

        public static bool CheckName(string name)
        {
            return !StringUtils.HasExtraSpaces(name) && !string.IsNullOrEmpty(name) && (string.IsNullOrEmpty(name) ? true : !name.Contains("*"));
        }

        [FieldToValidate]
        public bool IsNameValid
        {
            get
            {
                return CheckName(_obj.Name);
            }
        }

        public static bool CheckName2(string name2)
        {
            return !StringUtils.HasExtraSpaces(name2) && (string.IsNullOrEmpty(name2) ? true : !name2.Contains("*"));
        }

        //[FieldToValidate]
        //public bool IsName2Valid
        //{
        //    get
        //    {
        //        return CheckName2(_obj.Name2);
        //    }
        //}

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        //[FieldToValidate]
        //public bool IsSourceValid
        //{
        //    get
        //    {
        //        return CheckSource(_obj.Source);
        //    }
        //}
    }
}

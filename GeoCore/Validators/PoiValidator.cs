﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Validators
{
    public class PoiValidator : Validator<GPoi>
    {
        public PoiValidator(GPoi poi)
            : base(poi)
        {
        }

        public static bool CheckCode(string code)
        {
            return !StringUtils.HasExtraSpaces(code) && !string.IsNullOrEmpty(code);
        }

        [FieldToValidate]
        public bool IsCodeValid
        {
            get
            {
                return CheckCode(_obj.Code);
            }
        }

        public static bool CheckName(string name)
        {
            return !StringUtils.HasExtraSpaces(name) && !string.IsNullOrEmpty(name) && (string.IsNullOrEmpty(name) ? true : !name.Contains("*"));
        }

        [FieldToValidate]
        public bool IsNameValid
        {
            get
            {
                return CheckName(_obj.Name);
            }
        }

        public static bool CheckName2(string name2)
        {
            return !StringUtils.HasExtraSpaces(name2) && (string.IsNullOrEmpty(name2) ? true : !name2.Contains("*"));
        }

        [FieldToValidate]
        public bool IsName2Valid
        {
            get
            {
                return CheckName2(_obj.Name2);
            }
        }

        public static bool CheckSource(string source)
        {
            return source != "0" && !string.IsNullOrEmpty(source);
        }

        [FieldToValidate]
        public bool IsSourceValid
        {
            get
            {
                return CheckSource(_obj.Source);
            }
        }

        public static bool CheckUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return true;
            }
            return !StringUtils.HasExtraSpaces(url) && GPoi.UrlFormat.IsMatch(url);
        }

        [FieldToValidate]
        public bool IsUrlValid
        {
            get
            {
                return CheckUrl(_obj.Url);
            }
        }

        public static bool CheckParentId(int? id)
        {
            return id.HasValue && id.Value != -1;
        }

        [FieldToValidate]
        public bool IsParentIdValid
        {
            get
            {
                return CheckParentId(_obj.ParentId);
            }
        }
    }
}

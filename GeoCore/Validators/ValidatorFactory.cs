﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Validators
{
    public class ValidatorFactory
    {
        private static Dictionary<Type, Type> _library;

        static ValidatorFactory()
        {
            _library = new Dictionary<Type, Type>();
            Register<GBuildingGroup, BuildingGroupValidator>();
            Register<GBuilding, BuildingValidator>();
            Register<GFloor, FloorValidator>();
            Register<GJunction, JunctionValidator>();
            Register<GLandmark, LandmarkValidator>();
            Register<GMultiStorey, MultiStoreyValidator>();
            Register<GPoi, PoiValidator>();
            Register<GProperty, PropertyValidator>();
            Register<GStreetRestriction, StreetRestrictionValidator>();
            Register<GSectionBoundary, SectionBoundaryValidator>();
            Register<GStreet, StreetValidator>();
            Register<GTollRoute, TollRouteValidator>();
        }

        private static void Register<TKey, TValue>()
            where TKey : IGObject
            where TValue : IValidator
        {
            if (_library.ContainsKey(typeof(TKey)))
            {
                throw new Exception(string.Format("Duplicate key detected. {0}", typeof(TKey)));
            }
            _library.Add(typeof(TKey), typeof(TValue));
        }

        public static IValidator Create<T>(T obj) where T : IGObject
        {
            Type TValue;
            if (_library.TryGetValue(typeof(T), out TValue))
            {
                object[] args = new object[] { obj };
                return (IValidator)Activator.CreateInstance(TValue, args);
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Utilities
{
    public class ListStringBuilder : List<string>
    {
        public new void Add(string value)
        {
            base.Add(value);
        }

        public void Add(string format, params object[] args)
        {
            Add(string.Format(format, args));
        }

        public string Join(string separator)
        {
            return string.Join(separator, ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;

namespace Geomatic.Core.Utilities
{
    public class MapUtils
    {
        public static double ConvertPixelsToMapUnits(IActiveView activeView, int pixelUnits, double minValue)
        {
            double mapUnit = ConvertPixelsToMapUnits(activeView, pixelUnits);
            return (mapUnit > minValue) ? mapUnit : minValue;
        }

        public static double ConvertPixelsToMapUnits(IActiveView activeView, int pixelUnits)
        {
            if (activeView == null)
            {
                return -1;
            }

            //Get the DisplayTransformation 
            IDisplayTransformation displayTransformation = activeView.ScreenDisplay.DisplayTransformation;

            //Get the device frame which will give us the number of pixels in the X direction
            tagRECT deviceRECT = displayTransformation.get_DeviceFrame();
            int pixelExtent = (deviceRECT.right - deviceRECT.left);

            //Get the map extent of the currently visible area
            IEnvelope envelope = displayTransformation.VisibleBounds;
            double realWorldDisplayExtent = envelope.Width;

            //Calculate the size of one pixel
            if (pixelExtent == 0)
            {
                return -1;
            }
            double sizeOfOnePixel = (realWorldDisplayExtent / pixelExtent);

            //Multiply this by the input argument to get the result
            return (pixelUnits * sizeOfOnePixel);
        }

        public static IEnvelope GetExtent(IActiveView activeView, IPoint centrePoint, double scale)
        {
            if (activeView == null)
            {
                throw new Exception("No active view.");
            }

            //Get the DisplayTransformation 
            IDisplayTransformation displayTransformation = activeView.ScreenDisplay.DisplayTransformation;

            //Get the map extent of the currently visible area
            IEnvelope envelope = displayTransformation.VisibleBounds;
            envelope.CenterAt(centrePoint);

            double currentScale = displayTransformation.ScaleRatio;

            double dX = (scale * envelope.Width) / (2 * currentScale);
            double dY = (scale * envelope.Height) / (2 * currentScale);

            envelope.XMin = centrePoint.X - dX;
            envelope.XMax = centrePoint.X + dX;

            envelope.YMin = centrePoint.Y - dY;
            envelope.YMax = centrePoint.Y + dY;

            return envelope;
        }
    }
}

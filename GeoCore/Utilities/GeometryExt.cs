﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using Earthworm.AO;

namespace Geomatic.Core.Utilities
{
    public static class GeometryExt
    {
        /// <summary>
        /// Move shape
        /// </summary>
        /// <typeparam name="T">The type of geometry.</typeparam>
        /// <param name="shape">The current geometry.</param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        public static void Move<T>(this T shape, double dx, double dy) where T : class, IGeometry
        {
            if (shape == null)
                return;

            ITransform2D transform = (ITransform2D)shape;
            transform.Move(dx, dy);
        }

        /// <summary>
        /// Scale shape
        /// </summary>
        /// <typeparam name="T">The type of geometry.</typeparam>
        /// <param name="shape">The current geometry.</param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        public static void Scale<T>(this T shape, IPoint point, double dx, double dy) where T : class, IGeometry
        {
            if (shape == null)
                return;

            ITransform2D transform = (ITransform2D)shape;
            transform.Scale(point, dx, dy);
        }

        public static IPoint CentrePoint(this IEnvelope envelope)
        {
            IPoint point = new PointClass();
            point.X = envelope.XMin + ((envelope.XMax - envelope.XMin) / 2);
            point.Y = envelope.YMin + ((envelope.YMax - envelope.YMin) / 2);
            return point;
        }

        public static void Add(this IPolyline polyline, IPolyline polyline2)
        {
            if (!polyline.IsConnected(polyline2))
            {
                throw new Exception("Line not connected.");
            }

            IPolyline line1 = polyline.Clone();
            IPolyline line2 = polyline2.Clone();

            ISegmentCollection segmentCollection = (ISegmentCollection)line1;

            ISegmentCollection newSegmentCollection;

            if (line2.FromPoint.Equals2(line1.FromPoint) && !line2.ToPoint.Equals2(line1.FromPoint) && !line2.ToPoint.Equals2(line1.ToPoint))
            {
                line2.ReverseOrientation();
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection2.AddSegmentCollection(segmentCollection);
                newSegmentCollection = segmentCollection2;
            }
            else if (!line2.FromPoint.Equals2(line1.FromPoint) && line2.ToPoint.Equals2(line1.FromPoint) && !line2.FromPoint.Equals2(line1.ToPoint))
            {
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection2.AddSegmentCollection(segmentCollection);
                newSegmentCollection = segmentCollection2;
            }
            else if ((line2.FromPoint.Equals2(line1.FromPoint) && line2.ToPoint.Equals2(line1.ToPoint)) ||
                (!line2.FromPoint.Equals2(line1.ToPoint) && line2.ToPoint.Equals2(line1.ToPoint) && !line2.FromPoint.Equals2(line1.FromPoint)))
            {
                line2.ReverseOrientation();
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection.AddSegmentCollection(segmentCollection2);
                newSegmentCollection = segmentCollection;
            }
            else if ((line2.FromPoint.Equals2(line1.ToPoint) && line2.ToPoint.Equals2(line1.FromPoint)) ||
                (line2.FromPoint.Equals2(line1.ToPoint) && !line2.ToPoint.Equals2(line1.FromPoint) && !line2.ToPoint.Equals2(line1.ToPoint)))
            {
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection.AddSegmentCollection(segmentCollection2);
                newSegmentCollection = segmentCollection;
            }
            else if (line2.FromPoint.Equals2(line1.FromPoint) && line2.ToPoint.Equals2(line1.FromPoint))
            {
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection2.AddSegmentCollection(segmentCollection);
                newSegmentCollection = segmentCollection2;
            }
            else if (line2.FromPoint.Equals2(line1.ToPoint) && line2.ToPoint.Equals2(line1.ToPoint))
            {
                ISegmentCollection segmentCollection2 = (ISegmentCollection)line2;
                segmentCollection.AddSegmentCollection(segmentCollection2);
                newSegmentCollection = segmentCollection;
            }
            else
            {
                throw new Exception("Unknown condition.");
            }

            polyline.SetEmpty();
            ((ISegmentCollection)polyline).AddSegmentCollection(newSegmentCollection);
        }

        public static bool IsConnected(this IPolyline polyline, IPolyline polyline2)
        {
            return polyline.FromPoint.Equals2(polyline2.FromPoint) || polyline.FromPoint.Equals2(polyline2.ToPoint) || polyline.ToPoint.Equals2(polyline2.FromPoint) || polyline.ToPoint.Equals2(polyline2.ToPoint);
        }

        /// <summary>
        /// Hit test
        /// </summary>
        /// <typeparam name="T">The type of geometry.</typeparam>
        /// <param name="shape">The current geometry.</param>
        /// <param name="queryPoint"></param>
        /// <param name="searchRadius"></param>
        /// <param name="geometryPart"></param>
        /// <param name="hitPoint"></param>
        /// <param name="hitDistance"></param>
        /// <param name="hitPartIndex"></param>
        /// <param name="hitSegmentIndex"></param>
        /// <param name="isRightSide"></param>
        /// <returns></returns>
        public static bool HitTest<T>(this T shape, IPoint queryPoint, double searchRadius, esriGeometryHitPartType geometryPart, IPoint hitPoint, ref double hitDistance, ref int hitPartIndex, ref int hitSegmentIndex, ref bool isRightSide) where T : class, IGeometry
        {
            if (shape == null)
                return false;

            IHitTest hitTest = (IHitTest)shape;
            return hitTest.HitTest(queryPoint, searchRadius, geometryPart, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide);
        }

        public static IPoint MiddlePoint(this IPolyline line)
        {
            IPoint point = new PointClass(); ;
            line.QueryPoint(esriSegmentExtension.esriNoExtension, 0.5, true, point);
            return point;
        }

        /// <summary>
        /// Clones the current geometry without altering the state of the original object.
        /// </summary>
        /// <typeparam name="T">The type of geometry.</typeparam>
        /// <param name="shape">The current geometry.</param>
        /// <returns>A new copy of the geometry.</returns>
        public static T Clone<T>(this T shape) where T : class, IGeometry
        {
            if (shape == null)
                return default(T);

            IClone clone = (IClone)shape;
            return (T)clone.Clone();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Utilities
{
    internal class DatasetAscendingComparer : IComparer<IDataset>
    {
        public int Compare(IDataset dataset1, IDataset dataset2)
        {
            if (dataset1.Type == esriDatasetType.esriDTRasterDataset && dataset2.Type == esriDatasetType.esriDTFeatureClass)
            {
                return -1;
            }
            else if (dataset1.Type == esriDatasetType.esriDTFeatureClass && dataset2.Type == esriDatasetType.esriDTRasterDataset)
            {
                return 1;
            }
            else if (dataset1.Type == esriDatasetType.esriDTFeatureClass && dataset2.Type == esriDatasetType.esriDTFeatureClass)
            {
                IFeatureClass featureClass1 = dataset1 as IFeatureClass;
                IFeatureClass featureClass2 = dataset2 as IFeatureClass;
                if (featureClass1 == null)
                {
                    return 1;
                }
                if (featureClass2 == null)
                {
                    return -1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPolygon && 
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPolyline)
                {
                    return -1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPolygon &&
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPoint)
                {
                    return -1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPolyline &&
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPoint)
                {
                    return -1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPolyline &&
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPolygon)
                {
                    return 1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPoint &&
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPolygon)
                {
                    return 1;
                }
                if (featureClass1.ShapeType == esriGeometryType.esriGeometryPoint &&
                    featureClass2.ShapeType == esriGeometryType.esriGeometryPolyline)
                {
                    return 1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPolygon &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPolyline)
                {
                    return 1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPolygon &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPoint)
                {
                    return 1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPolyline &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPoint)
                {
                    return 1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPolyline &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPolygon)
                {
                    return -1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPoint &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPolygon)
                {
                    return -1;
                }
                if (featureClass2.ShapeType == esriGeometryType.esriGeometryPoint &&
                    featureClass1.ShapeType == esriGeometryType.esriGeometryPolyline)
                {
                    return -1;
                }           
                return 0;
            }
            else 
            {
                return 0;
            }
        }
    }
}

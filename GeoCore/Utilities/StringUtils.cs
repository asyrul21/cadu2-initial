﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Utilities
{
    public class StringUtils
    {
        public static string Trim(string value)
        {
            return string.IsNullOrEmpty(value) ?
                   string.Empty :
                   value.Trim();
        }

        public static string TrimSpaces(string value)
        {
            return string.IsNullOrEmpty(value) ?
                   string.Empty :
                   Regex.Replace(value, @"\s+", " ", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase).Trim();
        }

        public static bool HasExtraSpaces(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            bool no = true;
            no &= !Regex.IsMatch(value, @"\s{2,}", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            no &= !Regex.IsMatch(value, @"^\s+", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            no &= !Regex.IsMatch(value, @"\s+$", RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return !no;
        }

        public static IEnumerable<int> CommaIntSplit(string value)
        {
            foreach (string split in CommaSplit(value))
            {
                int testInt;
                if (int.TryParse(split, out testInt))
                {
                    yield return testInt;
                }
            }
        }

        public static IEnumerable<string> CommaSplit(string value)
        {
            return Regex.Split(value, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").AsEnumerable();
        }

        public static string CheckNull(string value)
        {
            return string.IsNullOrEmpty(value) ? null : value;
        }

        public static int? CheckInt(string value)
        {
            int testInt;
            return (int.TryParse(value, out testInt)) ? testInt : (int?)null;
        }

        public static bool IsInt(string value)
        {
            int testInt;
            return int.TryParse(value, out testInt);
        }

        public static SegmentName GetSegmentName(string segmentName)
        {
            switch (segmentName)
            {
                case "AS": return SegmentName.AS;
                case "JH": return SegmentName.JH;
                case "JP": return SegmentName.JP;
                case "KG": return SegmentName.KG;
                case "KK": return SegmentName.KK;
                case "KN": return SegmentName.KN;
                case "KV": return SegmentName.KV;
                case "MK": return SegmentName.MK;
                case "PG": return SegmentName.PG;
                case "TG": return SegmentName.TG;
                default: return SegmentName.None;
            }
        }

        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;

        public static string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        internal static string GetSegmentName()
        {
            throw new NotImplementedException();
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }
    }
}

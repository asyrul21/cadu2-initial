﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Utilities
{
    public static class FeatureLayerExt
    {
        public static bool ChangeWorkspace(this IFeatureLayer featureLayer, IFeatureWorkspace oldWorkspace, IFeatureWorkspace newWorkspace, out IFeatureClass oldFeatureClass, out IFeatureClass newFeatureClass)
        {
            IDataset dataSet = featureLayer.FeatureClass as IDataset;
            oldFeatureClass = null;
            newFeatureClass = null;
            if (dataSet != null)
            {
                IWorkspace workspace = dataSet.Workspace;
                if (workspace == oldWorkspace)
                {
                    oldFeatureClass = featureLayer.FeatureClass;
                    newFeatureClass = newWorkspace.OpenFeatureClass(dataSet.Name);
                    if (newFeatureClass != null)
                    {
                        featureLayer.FeatureClass = newFeatureClass;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

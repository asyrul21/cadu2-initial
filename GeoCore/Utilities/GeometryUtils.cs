﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using Geomatic.Core.Features;
using Earthworm.AO;

namespace Geomatic.Core.Utilities
{
    public class GeometryUtils
    {        
        public static T GetNearestFeature<T>(List<T> features, IPoint refPoint) where T : IGFeature
        {
            if (features.Count == 1)
            {
                return features[0];
            }

            if (features.Count > 1)
            {
                T nearestFeature = features[0];
                double nearestDistance = nearestFeature.Shape.DistanceTo(refPoint);
                for (int count = 1; count < features.Count; count++)
                {
                    T feature = features[count];
                    double distance = feature.Shape.DistanceTo(refPoint);
                    if (distance < nearestDistance)
                    {
                        nearestFeature = feature;
                        nearestDistance = distance;
                    }
                }
                return nearestFeature;
            }

            return default(T);
        }
    }
}

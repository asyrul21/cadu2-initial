﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Utilities
{
    public static class StandaloneTableExt
    {
        public static bool ChangeWorkspace(this IStandaloneTable standaloneTable, IFeatureWorkspace oldWorkspace, IFeatureWorkspace newWorkspace, out ITable oldTable, out ITable newTable)
        {
            IDataset dataSet = standaloneTable.Table as IDataset;
            oldTable = null;
            newTable = null;
            if (dataSet != null)
            {
                IWorkspace workspace = dataSet.Workspace;
                if (workspace == oldWorkspace)
                {
                    oldTable = standaloneTable.Table;
                    newTable = newWorkspace.OpenTable(dataSet.Name);
                    if (newTable != null)
                    {
                        standaloneTable.Table = newTable;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

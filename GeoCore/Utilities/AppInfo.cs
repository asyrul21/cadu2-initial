﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Geomatic.Core.Utilities
{
    public class AppInfo
    {
        public static string Title
        {
            get
            {
                AssemblyTitleAttribute attribute = Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyTitleAttribute)) as AssemblyTitleAttribute;
                return (attribute == null ? string.Empty : attribute.Title);
            }
        }

        public static string Version
        {
            get
            {
                AssemblyFileVersionAttribute attribute = Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyFileVersionAttribute)) as AssemblyFileVersionAttribute;
                return (attribute == null ? string.Empty : attribute.Version);
            }
        }
    }
}

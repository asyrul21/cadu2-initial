﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Utilities
{
    internal class DatasetDescendingComparer : IComparer<IDataset>
    {
        private DatasetAscendingComparer ascendingComparer = new DatasetAscendingComparer();
        public int Compare(IDataset dataset1, IDataset dataset2)
        {
            return -ascendingComparer.Compare(dataset1, dataset2);
        }
    }
}

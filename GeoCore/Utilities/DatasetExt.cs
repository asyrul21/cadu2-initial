﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Utilities
{
    public static class DatasetExt
    {
        public static IEnumerable<IDataset> GetSubDataset(this IFeatureDataset featureDataset)
        {
            IEnumDataset enumDataset = featureDataset.Subsets;
            IDataset dataset;
            while ((dataset = enumDataset.Next()) != null)
            {
                if (dataset.Type == esriDatasetType.esriDTFeatureDataset)
                {
                    foreach (IDataset innerDataset in ((IFeatureDataset)dataset).GetSubDataset())
                    {
                        yield return innerDataset;
                    }
                }
                else
                {
                    yield return dataset;
                }
            }
        }

        public static IEnumerable<IDataset> Sort(this IEnumerable<IDataset> datasets, DatasetSortOrder order)
        {
            if (order == DatasetSortOrder.Ascending)
            {
                DatasetAscendingComparer ascendingComparer = new DatasetAscendingComparer();
                List<IDataset> ascendingDataset = datasets.ToList();
                ascendingDataset.Sort(ascendingComparer);
                foreach (IDataset dataset in ascendingDataset)
                {
                    yield return dataset;
                }
            }
            else if (order == DatasetSortOrder.Descending)
            {
                DatasetDescendingComparer descendingComparer = new DatasetDescendingComparer();
                List<IDataset> descendingDataset = datasets.ToList();
                descendingDataset.Sort(descendingComparer);
                foreach (IDataset dataset in descendingDataset)
                {
                    yield return dataset;
                }
            }
            else
            {
                foreach (IDataset dataset in datasets)
                {
                    yield return dataset;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Utilities
{
    // Summary:
    //     Specifies how items in a list are sorted.
    public enum DatasetSortOrder
    {
        // Summary:
        //     The items are not sorted.
        None = 0,
        //
        // Summary:
        //     The items are sorted in ascending order.
        Ascending = 1,
        //
        // Summary:
        //     The items are sorted in descending order.
        Descending = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;

namespace Geomatic.Core.Utilities
{
    public static class TopologyLayerExt
    {
        public static bool ChangeWorkspace(this ITopologyLayer topolygyLayer, IFeatureWorkspace oldWorkspace, IFeatureWorkspace newWorkspace)
        {
            IDataset dataSet = topolygyLayer.Topology as IDataset;
            if (dataSet != null)
            {
                IWorkspace workspace = dataSet.Workspace;
                if (workspace == oldWorkspace)
                {
                    ITopologyWorkspace newTopologyWorkspace = (ITopologyWorkspace)newWorkspace;
                    if (newTopologyWorkspace != null)
                    {
                        topolygyLayer.Topology = newTopologyWorkspace.OpenTopology(dataSet.Name);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

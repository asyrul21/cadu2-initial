﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Geomatic.Core.Logs
{
    public class CsvLog : BaseLog
    {
        public CsvLog(string filename)
            : base(filename)
        {
        }

        protected override void CreateFile(string filename)
        {
            if (!File.Exists(filename + ".csv"))
            {
                _file = filename + ".csv";
                FileStream fileStream = File.Create(_file);
                fileStream.Dispose();
            }
            else
            {
                CreateFile(string.Format("{0}({1})", _filename, ++_fileCount));
            }
        }
    }
}

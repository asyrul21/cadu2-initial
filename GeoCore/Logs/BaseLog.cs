﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Geomatic.Core.Logs
{
    public abstract class BaseLog : ILog
    {
        protected string _file;
        protected int _fileCount = 0;
        protected string _filename;

        public BaseLog(string filename)
        {
            _filename = filename;
            CreateFile(_filename);
        }

        protected abstract void CreateFile(string filename);

        public virtual void Log(string message)
        {
            using (StreamWriter streamWriter = new StreamWriter(_file, true))
            {
                streamWriter.WriteLine(message);
            }
        }
    }
}

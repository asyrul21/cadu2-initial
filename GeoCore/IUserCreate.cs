﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public interface IUserCreate
    {
        string CreatedBy { set; get; }

        string DateCreated { set; get; }
    }
}

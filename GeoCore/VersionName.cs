﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public enum VersionName
    {
        DEFAULT = 0,
        AS,
        JH,
        JP,
        KG,
        KK,
        KN,
        KV,
        MK,
        PG,
        TG
    }

    public class StringVersionName
    {
        public const string DEFAULT = "SDE.DEFAULT";
        public const string AS = "CADUDBA.AS";
        public const string JH = "CADUDBA.JH";
        public const string JP = "CADUDBA.JP";
        public const string KG = "CADUDBA.KG";
        public const string KK = "CADUDBA.KK";
        public const string KN = "CADUDBA.KN";
        public const string KV = "CADUDBA.KV";
        public const string MK = "CADUDBA.MK";
        public const string PG = "CADUDBA.PG";
        public const string TG = "CADUDBA.TG";
    }
}

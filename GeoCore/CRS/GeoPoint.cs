﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public struct GeoPoint
    {
        public double Longitude;
        public double Latitude;
        public double Height;
    }
}

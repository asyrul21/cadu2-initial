﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public struct GeoEllipse
    {
        public double A;
        public double B;
        public double F;
        public double Finv;
        public double E2;
        public double E;
        public double K12;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public struct GeoDatum
    {
        public double DelX;
        public double DelY;
        public double DelZ;
        public double Wx;
        public double Wy;
        public double Wz;
    }
}

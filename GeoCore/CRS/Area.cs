﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public enum Area
    {
        EAST,
        WEST,
        UNDECIDED
    }
}

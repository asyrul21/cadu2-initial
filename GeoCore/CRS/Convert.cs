﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public class Convert
    {
        /*
        CM   -  CHAIN TO METER CONVERSION FACTOR
        WO   -  BASIC LONGITUDE
        SFO  -  SCALE FACTOR AT ORIGIN
        AE   -  ELLIPSOID SEMI MAJOR AXIS
        BE   -  ELLIPSOID SEMI MINOR AXIS
        F    -  FLATTENING
        E2   -  ECCENTRICITY SQUARED
        PHI  -  GEOGRAPHICAL LATITUDE
        W    -  GEOGRAPHICAL LONGITUDE
        CHI  -  ISOMETRIC LATITUDE
        X,Y  -  SKEW COORDINATES
        RN,RE-  RECTIFIED COORDINATES
        G    -  SKEW CONVERGENCE OF MERIDIANS
        GR   -  RECTIFIED CONVERGENCE OF MERIDIANS
        SF   -  SCALE FACTOR AT ANY POINT
        V    -  RADIUS OF CURVATURE IN THE PRIME VERTICAL
        P    -  DISTANCE FROM POLAR AXIS
        NO   -  NUMBER OF STATIONS ENTERED
        */
        private const double CM = 20.1167824940288;
        private const double DELTA = 0.00010;
        private const double RO = 1.0 / 3600.0 * Math.PI / 180.0;
        private const double SFO = 0.99984;

        private static double azmO;
        private static double skewO;

        private static double a;
        private static double aPo;
        private static double b;
        private static double c;
        private static double d;
        private static double fac;
        private static double ilo;
        private static double vo;
        private static double wo;

        private static double falseEast;
        private static double falseNorth;

        private static double xCenter;

        private static double originalLatitude;
        private static double originalLongitude;

        private static GeoDatum wgs84From;
        private static GeoDatum wgs84To;

        private static GeoEllipse wgs84Ellipse;
        private static GeoEllipse mrtEllipse;

        /// <summary>
        /// Convert latitude, longitude to rso
        /// </summary>
        /// <param name="longitute">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="area">East or West Malaysia</param>
        /// <returns>Rso</returns>
        public static Point3D LatLongToRso(double longitute, double latitude, Area area)
        {
            GeoPoint wgs, mrt;

            SetPlace(area);

            wgs.Longitude = DegreeToRadian(longitute);
            wgs.Latitude = DegreeToRadian(latitude);
            wgs.Height = 0;

            mrt = TransformDatum(wgs, wgs84Ellipse, mrtEllipse, wgs84From);
            mrt.Longitude = RadianToDegree(mrt.Longitude);
            mrt.Latitude = RadianToDegree(mrt.Latitude);

            return MrtToWestEastRso(mrt);
        }

        public static GeoPoint RsoToLatLong(double x, double y, Area area)
        {
            GeoPoint wgs, mrt;
            Point3D point;

            point.X = x;
            point.Y = y;
            point.Z = 0;

            SetPlace(area);

            if (area == Area.WEST)
            {
                mrt = WestRsoToMrt(point);
            }
            else if (area == Area.EAST)
            {
                mrt = EastRsoToMrt(point);
            }
            else
            {
                throw new Exception("Undecided area for convertion");
            }

            wgs = TransformDatum(mrt, mrtEllipse, wgs84Ellipse, wgs84To);
            wgs.Longitude = RadianToDegree(wgs.Longitude);
            wgs.Latitude = RadianToDegree(wgs.Latitude); ;

            return wgs;
        }

        private static GeoPoint WestRsoToMrt(Point3D point)
        {
            GeoPoint geoMrt;
            Point3D pointMrt;
            double facX, facY;
            double aix, XI, XII, XIII, XV, XVI, AII, VI;
            double aiso;
            bool cond;
            double diff, range;

            pointMrt.X = Math.Cos(skewO) * (point.Y - falseNorth) + Math.Sin(skewO) * (point.X - falseEast);
            pointMrt.Y = Math.Cos(skewO) * (point.X - falseEast) - Math.Sin(skewO) * (point.Y - falseNorth);

            facX = fac * pointMrt.X;
            facY = fac * pointMrt.Y;

            aix = Math.Cos(facX);
            XI = 0.80 * Math.Sin(facX);
            XII = 0.60 * Math.Sin(facX);
            XIII = Math.Cosh(facY);
            XV = 0.80 * Math.Sinh(facY);
            XVI = 0.60 * Math.Sinh(facY);
            AII = (XI + XVI) / XIII;
            VI = (XII - XV) / aix;

            aiso = ((Math.Log((1.00 + AII) / (1.00 - AII)) / 2.00) - c) / b;
            geoMrt.Latitude = aiso;
            geoMrt.Height = 0;
            geoMrt.Longitude = (b * wo - Math.Atan(VI)) / b;

            cond = true;
            diff = 0.001;
            range = 50;

            for (int i = 0; i < 8; i++)
            {
                if (cond)
                {
                    diff = diff / 10;
                    range = range / 10;
                    cond = Convergence(ref geoMrt.Latitude, aiso, range, diff);
                }
            }

            //if (!cond)
            //{
            //    printf("\nThe Coordinate X:%.4f Y:%.4f Failed To Converge at delta %.12f \n  ", cord_rso.x, cord_rso.y, diff);
            //}

            return geoMrt;
        }

        private static GeoPoint EastRsoToMrt(Point3D point)
        {
            GeoPoint geoMrt;
            Point3D pointMrt;
            double facX, facY;
            double aix, XI, XII, XIII, XV, XVI, AII, VI;
            double aiso;
            bool cond;
            double diff, range;

            pointMrt.Y = Math.Cos(skewO) * (point.X - falseEast) - Math.Sin(skewO) * (point.Y - falseNorth);
            pointMrt.X = Math.Sin(skewO) * (point.X - falseEast) + Math.Cos(skewO) * (point.Y - falseNorth) + xCenter;

            facX = fac * pointMrt.X;
            facY = fac * pointMrt.Y;

            aix = Math.Cos(facX);
            XI = 0.80 * Math.Sin(facX);
            XII = 0.60 * Math.Sin(facX);
            XIII = Math.Cosh(facY);
            XV = 0.80 * Math.Sinh(facY);
            XVI = 0.60 * Math.Sinh(facY);
            AII = (XII - XV) / XIII;
            VI = (XI + XVI) / aix;

            aiso = ((Math.Log((1.00 + AII) / (1.00 - AII)) / 2.00) - c) / b;
            geoMrt.Latitude = aiso;
            geoMrt.Height = 0;
            geoMrt.Longitude = (b * wo + Math.Atan(VI)) / b;

            cond = true;
            diff = 0.001;
            range = 50;

            for (int i = 0; i < 8; i++)
            {
                if (cond)
                {
                    diff = diff / 10;
                    range = range / 10;
                    cond = Convergence(ref geoMrt.Latitude, aiso, range, diff);
                }
            }

            //if (!cond)
            //{
            //    printf("\nThe Coordinate X:%.4f Y:%.4f Failed To Converge at delta %.12f \n  ", point.x, point.y, diff);
            //}

            return geoMrt;
        }


        private static bool Convergence(ref double geoLat, double aiso, double diff, double limit)
        {
            double radDiff, temp;
            int kira;
            double aPrime, bPrime, aIsoPrime, delta;
            bool cond;
            double mrtLat;

            temp = 0;
            kira = 0;
            cond = true;
            radDiff = diff * RO;
            mrtLat = geoLat;

            while (cond)
            {
                kira += 1;

                if (kira > 10000)
                {
                    geoLat = mrtLat;
                    cond = false;
                    return false;
                }
                else
                {
                    aPrime = (Math.PI / 4) + (geoLat / 2.00);
                    bPrime = (1.0 + (mrtEllipse.E * Math.Sin(geoLat))) / (1.0 - (mrtEllipse.E * Math.Sin(geoLat)));
                    aIsoPrime = (Math.Log(Math.Tan(aPrime))) - ((mrtEllipse.E / 2.0) * (Math.Log(bPrime)));
                    delta = aIsoPrime - aiso;

                    if (kira == 1)
                        temp = delta;

                    if (Math.Abs(delta) > limit)
                    {
                        if (geoLat < temp)
                        {
                            geoLat = geoLat - radDiff;
                        }
                        else
                        {
                            geoLat = geoLat + radDiff;
                        }
                        temp = delta;
                    }
                    else
                    {
                        cond = false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Convert degree to radian
        /// </summary>
        /// <param name="degree">Degree</param>
        /// <returns></returns>
        private static double DegreeToRadian(double degree)
        {
            return degree * Math.PI / 180;
        }

        /// <summary>
        /// Convert radian to degree
        /// </summary>
        /// <param name="radian">Radian</param>
        /// <returns></returns>
        private static double RadianToDegree(double radian)
        {
            return radian * 180 / Math.PI;
        }

        private static double AngleToDecimal(double hour, double minute, double second)
        {
            return hour + (minute / 60.0) + (second / 3600.0);
        }

        private static void SetPlace(Area area)
        {
            if (area == Area.EAST)
            {
                SetEast();
            }
            else if (area == Area.WEST)
            {
                SetWest();
            }
            else
            {
                throw new Exception("Undecided area for convertion");
            }

            mrtEllipse.E = Math.Sqrt((2.0 * mrtEllipse.F) - Math.Pow(mrtEllipse.F, 2.0));
            mrtEllipse.E2 = (2.0 * mrtEllipse.F) - Math.Pow(mrtEllipse.F, 2.0);
            mrtEllipse.K12 = Math.Pow(mrtEllipse.E, 2) / (1 - Math.Pow(mrtEllipse.E, 2));

            b = Math.Sqrt(1 + mrtEllipse.K12 * (Math.Pow(Math.Cos(originalLatitude), 4)));
            a = b * ((mrtEllipse.A) * Math.Sqrt(1 - mrtEllipse.E2)) / (1 - mrtEllipse.E2 * Math.Pow(Math.Sin(originalLatitude), 2));
            ilo = Math.Log(Math.Tan((Math.PI / 4.0) + originalLatitude / 2)) - (mrtEllipse.E / 2.0) * Math.Log((1 + mrtEllipse.E * Math.Sin(originalLatitude)) / (1 - mrtEllipse.E * Math.Sin(originalLatitude)));
            vo = (mrtEllipse.A) / Math.Sqrt(1 - mrtEllipse.E2 * Math.Pow(Math.Sin(originalLatitude), 2.0));
            aPo = a / (vo * Math.Cos(originalLatitude));
            c = Math.Log(aPo + Math.Sqrt(Math.Pow(aPo, 2.0) - 1.0)) - b * ilo;

            if (area == Area.EAST)
            {
                wo = originalLongitude - Math.Asin(Math.Sinh(Math.Log(aPo + Math.Sqrt(Math.Pow(aPo, 2.0) - 1.0))) / 0.75) / b;
                d = b * Math.Sqrt(1 - mrtEllipse.E2) / (Math.Cos(originalLatitude) * Math.Sqrt(1 - mrtEllipse.E2 * (Math.Pow(Math.Sin(originalLatitude), 2.0))));
                xCenter = (a * SFO / b) * Math.Atan(Math.Sqrt(Math.Pow(d, 2.0) - 1) / Math.Cos(azmO));
            }
            else
            {
                wo = originalLongitude + Math.Asin(Math.Sinh(Math.Log(aPo + Math.Sqrt(Math.Pow(aPo, 2.0) - 1.0))) * 0.75) / b;
                xCenter = 0;
            }

            fac = b / ((a) * SFO);
        }

        private static void SetEast()
        {
            //miri
            falseEast = 1490476.872;
            falseNorth = 442857.6535;

            skewO = DegreeToRadian(AngleToDecimal(53, 07, 48.3685));
            azmO = DegreeToRadian(AngleToDecimal(53, 18, 56.9537));

            originalLatitude = DegreeToRadian(4.0);
            originalLongitude = DegreeToRadian(115.00);

            wgs84From.DelX = 689.0;
            wgs84From.DelY = -691.0;
            wgs84From.DelZ = 46.0;
            wgs84From.Wx = 0.0;
            wgs84From.Wy = 0.0;
            wgs84From.Wz = 0.0;

            // Transformation parameters from mrt to wgs84 (source:JUMP)
            wgs84To.DelX = -689.0;
            wgs84To.DelY = 691.0;
            wgs84To.DelZ = -46.0;
            wgs84To.Wx = 0.0;
            wgs84To.Wy = 0.0;
            wgs84To.Wz = 0.0;

            wgs84Ellipse.A = 6378137.0;
            wgs84Ellipse.F = 1.0 / 298.257222932869640;
            wgs84Ellipse.E2 = (2 * wgs84Ellipse.F) - Math.Pow(wgs84Ellipse.F, 2.0);

            mrtEllipse.A = 6377298.556;
            mrtEllipse.Finv = 300.801696013858250;
            mrtEllipse.F = 1 / 300.801696013858250;
        }

        private static void SetWest()
        {
            falseEast = 40000 * CM;
            falseNorth = 0;

            skewO = DegreeToRadian(AngleToDecimal(-36.0, -52.0, -11.631590));
            azmO = DegreeToRadian(AngleToDecimal(-36.0, -58.0, -27.1542));

            originalLatitude = DegreeToRadian(4.0);
            originalLongitude = DegreeToRadian(102.25);

            wgs84From.DelX = 379.776025667441;
            wgs84From.DelY = -775.383707150472;
            wgs84From.DelZ = 86.609261868685641;
            wgs84From.Wx = 2.59674346060356;
            wgs84From.Wy = 2.10212749913335;
            wgs84From.Wz = -12.1137664647865;

            // Transformation parameters from mrt to wgs84 (source:JUMP)
            wgs84To.DelX = -379.73137;
            wgs84To.DelY = 775.4071;
            wgs84To.DelZ = -86.59563;
            wgs84To.Wx = -2.59662;
            wgs84To.Wy = -2.10228;
            wgs84To.Wz = 12.11374;

            wgs84Ellipse.A = 6378137.0;
            wgs84Ellipse.F = 1.0 / 298.257222932869640;
            wgs84Ellipse.E2 = (2 * wgs84Ellipse.F) - Math.Pow(wgs84Ellipse.F, 2.0);

            mrtEllipse.A = 6377304.0630;
            mrtEllipse.Finv = 300.801700097124410;
            mrtEllipse.F = 1 / 300.801700097124410;
        }

        private static GeoPoint TransformDatum(GeoPoint geoWgs, GeoEllipse geoEllipseFrom, GeoEllipse geoEllipseTo, GeoDatum geoShift)
        {
            double an = geoEllipseFrom.A / Math.Sqrt(1.0 - geoEllipseFrom.E2 * Math.Pow(Math.Sin(geoWgs.Latitude), 2.0));

            double x84 = (an + geoWgs.Height) * Math.Cos(geoWgs.Latitude) * Math.Cos(geoWgs.Longitude);
            double y84 = (an + geoWgs.Height) * Math.Cos(geoWgs.Latitude) * Math.Sin(geoWgs.Longitude);
            double z84 = (an * (1.0 - geoEllipseFrom.E2) + geoWgs.Height) * Math.Sin(geoWgs.Latitude);

            Point3D pointOut;
            pointOut.X = x84 + geoShift.DelX + geoShift.Wz * RO * y84 - geoShift.Wy * RO * z84;
            pointOut.Y = y84 + geoShift.DelY - geoShift.Wz * RO * x84 + geoShift.Wx + RO * z84;
            pointOut.Z = z84 + geoShift.DelZ + geoShift.Wy * RO * x84 - geoShift.Wx * RO * y84;

            return Point3dToGeoPoint(pointOut, geoEllipseTo);
        }

        private static GeoPoint Point3dToGeoPoint(Point3D point, GeoEllipse geoEllipseTo)
        {
            double t = geoEllipseTo.E2 * point.Z;
            double xysq = Math.Pow(point.X, 2.0) + Math.Pow(point.Y, 2.0);

            double zt = 0;
            double h1 = 0;
            double h2 = 0;
            double sinpi;
            double esqsp;
            double t1;
            // Iterative for height
            for (int count = 1; count < 260; count++)
            {
                zt = point.Z + t;
                h1 = Math.Sqrt(xysq + Math.Pow(zt, 2.0));
                sinpi = zt / h1;
                esqsp = geoEllipseTo.E2 * sinpi;
                h2 = geoEllipseTo.A / Math.Sqrt(1.00 - esqsp * sinpi);
                t1 = h2 * esqsp;
                if (Math.Abs(t1 - t) < DELTA)
                    break;
                else
                    t = t1;
            }

            GeoPoint geoOut;

            // Geodetic height
            geoOut.Height = h1 - h2;

            // Geodetic latitude
            double rtxysq = Math.Sqrt(xysq);
            geoOut.Latitude = Math.Atan2(zt, rtxysq);

            // Geodetic longitude
            geoOut.Longitude = Math.Atan2(point.Y, point.X);

            if (geoOut.Longitude < 0)
                geoOut.Longitude += Math.PI;

            return geoOut;
        }

        private static Point3D MrtToWestEastRso(GeoPoint geoMRT)
        {
            double chi, x, y;
            double bChiC;
            double VIII, VII, V, III, IV, I, X, XIV;

            double radianLatitude = DegreeToRadian(geoMRT.Latitude);
            double radianLongitude = DegreeToRadian(geoMRT.Longitude);

            chi = Math.Log(Math.Tan((Math.PI / 4.0) + radianLatitude / 2.0)) - (mrtEllipse.E / 2.0) * Math.Log((1.0 + mrtEllipse.E * Math.Sin(radianLatitude)) / (1.0 - mrtEllipse.E * Math.Sin(radianLatitude)));
            bChiC = b * chi + c;

            I = Math.Cosh(bChiC);
            III = Math.Cos(skewO) * Math.Sinh(bChiC);
            IV = -Math.Sin(skewO) * Math.Sinh(bChiC);
            V = Math.Cos(b * (wo - radianLongitude));
            VII = Math.Cos(skewO) * Math.Sin(b * (wo - radianLongitude));
            VIII = -Math.Sin(skewO) * Math.Sin(b * (wo - radianLongitude));
            X = (III + VIII) / V;
            XIV = (IV - VII) / I;

            x = a * SFO / b * Math.Atan(X) - xCenter;
            y = a * SFO / b * 0.5 * Math.Log((1.0 + XIV) / (1.0 - XIV));

            Point3D point;
            point.X = Math.Cos(skewO) * y + Math.Sin(skewO) * x + falseEast;
            point.Y = Math.Cos(skewO) * x - Math.Sin(skewO) * y + falseNorth;
            point.Z = 0.0;

            return point;
        }
    }
}

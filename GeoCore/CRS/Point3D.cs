﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.CRS
{
    public struct Point3D
    {
        public double X;
        public double Y;
        public double Z;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public interface IGObject
    {
        string TableName { get; }
        int OID { get; }
        bool IsDataBound { get; }
    }
}

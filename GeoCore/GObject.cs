﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core
{
    public abstract class GObject : MappableFeature, IGObject
    {
        public abstract SegmentName SegmentName { get; }

        public abstract string TableName { get; }

        public const string CREATED_BY = "ORIGINAL_USER";
        public const string DATE_CREATED = "ORIGINAL_DATE";
        public const string UPDATED_BY = "MODIFIED_USER";
        public const string DATE_UPDATED = "MODIFIED_DATE";

        public abstract string CreatedBy { set; get; }

        public abstract string DateCreated { set; get; }

        public abstract string UpdatedBy { set; get; }

        public abstract string DateUpdated { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core
{
    public interface IGNavigationObject
    {
        bool IsNavigationReady { get; }
        int? NavigationStatus { set; get; }
    }
}

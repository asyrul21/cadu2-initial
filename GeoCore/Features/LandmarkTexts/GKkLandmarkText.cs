﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTexts
{
    public class GKkLandmarkText : GLandmarkText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KK_ADM_LANDMARK_TEXT";
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTexts
{
    public class GKvLandmarkText : GLandmarkText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KV_ADM_LANDMARK_TEXT";
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTexts
{
    public class GTgLandmarkText : GLandmarkText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "TG_ADM_LANDMARK_TEXT";
    }
    
}

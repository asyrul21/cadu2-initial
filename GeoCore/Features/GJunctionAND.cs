﻿using Earthworm;
using System.Collections.Generic;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using System.Linq;

namespace Geomatic.Core.Features
{
    /// <summary>
    /// For AND Feature
    /// </summary>
    public abstract class GJunctionAND : GJunction
    {
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override IEnumerable<GStreetAND> GetStreetsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OriId;
            query.Obj.ToNodeId = this.OriId;
            return repo.Search(query);
        }

        public override IEnumerable<GStreet> GetStreets()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OriId;
            query.Obj.ToNodeId = this.OriId;
            return repo.Search(query);
        }

        public override bool HasInTollRoute()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.InSegment = SegmentName.ToString();
            query.Obj.InID = this.OriId;

            return repo.Count(query) > 0;
        }

        public override bool HasOutTollRoute()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.OutSegment = SegmentName.ToString();
            query.Obj.OutID = this.OriId;

            return repo.Count(query) > 0;
        }

        public override bool HasTollRoute()
        {
            bool hasTollRoute;
            hasTollRoute = HasInTollRoute();
            hasTollRoute |= HasOutTollRoute();

            return hasTollRoute;
        }

        public override IEnumerable<GTollRoute> GetTollRoutes()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.InSegment = SegmentName.ToString();
            query.Obj.InID = this.OriId;
            IEnumerable<GTollRoute> tollRoutesIn = repo.Search(query);

            query = new Query<GTollRoute>(SegmentName);
            query.Obj.OutSegment = SegmentName.ToString();
            query.Obj.OutID = this.OriId;
            IEnumerable<GTollRoute> tollRoutesOut = repo.Search(query);

            return tollRoutesIn.Concat(tollRoutesOut);
        }

        public override bool HasStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OriId;
            query.Obj.ToNodeId = this.OriId;
            return repo.Count(query) > 0;
        }

        public override bool HasStreetRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Obj.JunctionId = this.OriId;
            return repo.Count(query) > 0;
        }

        public override IEnumerable<GStreetRestriction> GetStreetRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Obj.JunctionId = this.OriId;
            return repo.Search(query);
        }

        public override void CopyFrom(GJunction junction)
        {
            this.Type = junction.Type;
            this.Name = junction.Name;
            this.Source = junction.Source;
            this.AreaId = junction.AreaId;
            this.Shape = junction.Shape;
            this.OriId = junction.OID;
            this.CreatedBy = junction.CreatedBy;
            this.DateCreated = junction.DateCreated;
            this.UpdatedBy = junction.UpdatedBy;
            this.DateUpdated = junction.DateUpdated;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();
            // added by asyrul
            IEnumerable<GStreet> streetids = GetStreets();
            string streetID = streetids.First().OID.ToString();
            // added end

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            // added by asyrul
            sb.Add(string.Format("Street ID: [ {0} ]", streetID));
            // added end
            sb.Add(Name);

            return string.Join(" ", sb.ToArray());
        }

    }
}

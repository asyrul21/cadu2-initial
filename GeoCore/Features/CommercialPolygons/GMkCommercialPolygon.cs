﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.CommercialPolygons
{
    public class GMkCommercialPolygon : GCommercialPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "MK_Commercial_Area";
    }
}

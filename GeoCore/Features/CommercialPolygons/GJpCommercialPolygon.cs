﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.CommercialPolygons
{
    public class GJpCommercialPolygon : GCommercialPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_Commercial_Area";
    }
}

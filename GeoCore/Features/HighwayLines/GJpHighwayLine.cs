﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.HighwayLines
{
    public class GJpHighwayLine : GHighwayLine
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public const string TABLE_NAME = "JP_Highway_Road_Centerline";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.HighwayLines
{
    public class GAsHighwayLine : GHighwayLine
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public const string TABLE_NAME = "AS_Highway_Road_Centerline";
    }
}

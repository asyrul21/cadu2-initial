﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.GreenaryLines
{
    public class GKgGreenaryLine : GGreenaryLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_Greenary_Line";
    }
}

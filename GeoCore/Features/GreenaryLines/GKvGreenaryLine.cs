﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.GreenaryLines
{
    public class GKvGreenaryLine : GGreenaryLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_Greenary_Line";
    }
}

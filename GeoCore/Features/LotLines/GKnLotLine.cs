﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LotLines
{
    public class GKnLotLine : GLotLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KN_Lot_Boundary";
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Jupem
{
    public class GKnJupem : GJupem
    {
        public const string TABLE_NAME = "KN_ADM_JUPEM";

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Jupem
{
    public class GAsJupem : GJupem
    {
        public const string TABLE_NAME = "AS_ADM_JUPEM";

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }
    }
}

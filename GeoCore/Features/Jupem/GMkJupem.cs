﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Jupem
{
    public class GMkJupem : GJupem
    {
        public const string TABLE_NAME = "MK_ADM_JUPEM";

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Jupem
{
    public class GKvJupem : GJupem
    {
        public const string TABLE_NAME = "KV_ADM_JUPEM";

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }
    }
}

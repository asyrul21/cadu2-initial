﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Jupem
{
    public class GPgJupem : GJupem
    {
        public const string TABLE_NAME = "PG_ADM_JUPEM";

        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName { get { return TABLE_NAME; } }
    }
}

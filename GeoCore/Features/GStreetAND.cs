﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GStreetAND : GStreet
    {
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }
        private const int MIN_LENGTH = 5;

        public virtual GJunctionAND GetFromNODE()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (FromNodeId.HasValue) ? repo.GetById<GJunctionAND>(FromNodeId.Value) : null;
        }

        public virtual GJunctionAND GetToNODE()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (ToNodeId.HasValue) ? repo.GetById<GJunctionAND>(ToNodeId.Value) : null;
        }

        public override IEnumerable<GPropertyAND> GetPropertiesAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyAND> query = new Query<GPropertyAND>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Search(query);
        }

        public virtual bool TrySplitAtPointAND(IPoint point, out GStreetAND newStreet)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            newStreet = null;
            bool isSplitted = false;
            int newPartIndex = 0;
            int newSegmentIndex = 0;
            IPolyline polyline = Polyline;
            ISpatialReference spatialReference = polyline.SpatialReference;

            polyline.SplitAtPoint(point, true, false, out isSplitted, out newPartIndex, out newSegmentIndex);

            if (!isSplitted)
            {
                return false;
            }

            ISegmentCollection segmentCollection = (ISegmentCollection)polyline;

            // new line 1
            IPolyline newLine1 = CreateLine(segmentCollection, spatialReference, 0, newSegmentIndex - 1, MIN_LENGTH);

            Shape = newLine1;
            repo.Update(this);

            // new line 2
            IPolyline newLine2 = CreateLine(segmentCollection, spatialReference, newSegmentIndex, segmentCollection.SegmentCount - 1, MIN_LENGTH);

            newStreet = repo.NewObj<GStreetAND>();
            newStreet.FromNodeId = -1;
            newStreet.ToNodeId = -1;
            newStreet.Shape = newLine2;
            newStreet = repo.Insert(newStreet, false);

            GStreetTextAND newText = repo.NewObj<GStreetTextAND>();

            IPolyline line = newLine2.Clone();
            if (line.FromPoint.X > line.ToPoint.X)
            {
                line.ReverseOrientation();
            }
            newText.Size = 3;
            newText.Shape = line;
            newText.LinkedFeatureID = newStreet.OID;
            newText = repo.Insert(newText);

            return isSplitted;
        }

        public override bool HasProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Count(query) > 0;
        }

        public override bool HasLandmark()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Count(query) > 0;
        }

        public override bool HasBuildingGroup()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Count(query) > 0;
        }

        public override bool HasRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.StartId = OriId;
            query.Obj.EndId1 = OriId;
            query.Obj.EndId2 = OriId;
            query.Obj.EndId3 = OriId;
            query.Obj.EndId4 = OriId;
            query.Obj.EndId5 = OriId;
            query.Obj.EndId6 = OriId;
            query.Obj.EndId7 = OriId;
            query.Obj.EndId8 = OriId;
            query.Obj.EndId9 = OriId;
            query.Obj.EndId10 = OriId;
            return repo.Count(query) > 0;
        }

        public override IEnumerable<GProperty> GetProperties()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Search(query);
        }

        public override IEnumerable<GLandmark> GetLandmarks()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Search(query);
        }

        public override IEnumerable<GBuildingGroup> GetBuildingGroups()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.StreetId = OriId;
            return repo.Search(query);
        }

        public override void CopyFrom(GStreet street)
        {
            this.FromNodeId = street.FromNodeId;
            this.ToNodeId = street.ToNodeId;
            this.ConstructionStatus = street.ConstructionStatus;
            this.NavigationStatus = street.NavigationStatus;
            this.Status = street.Status;
            this.NetworkClass = street.NetworkClass;
            this.Class = street.Class;
            this.Category = street.Category;
            this.FilterLevel = street.FilterLevel;
            this.Design = street.Design;
            this.Direction = street.Direction;
            this.TollType = street.TollType;
            this.Divider = street.Divider;
            this.Source = street.Source;
            this.Shape = street.Shape;
            this.Type = street.Type;
            this.Name = street.Name;
            this.Name2 = street.Name2;
            this.Section = street.Section;
            this.Postcode = street.Postcode;
            this.City = street.City;
            this.SubCity = street.SubCity;
            this.State = street.State;
            //this.AndStatus = 0;
            //this.UpdateStatus = 0;
            this.CreatedBy = street.CreatedBy;
            this.DateCreated = street.DateCreated;
            this.DateUpdated = street.DateUpdated;
            this.UpdatedBy = street.UpdatedBy;
            if (street.NumOfLanes != null)
            {
                this.NumOfLanes = street.NumOfLanes;
            }
            else
                this.NumOfLanes = 1;

            if (street.Divider != null)
            {
                this.Divider = street.Divider;
            }
            else
            {
                this.Divider = 1;
            }
            this.AreaId = street.AreaId;
            this.OriId = street.OID;
            this.Length = street.Length;
        }

        public virtual IEnumerable<GStreetTextAND> GetTextsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            Query<GStreetTextAND> query = new Query<GStreetTextAND>(SegmentName);
            query.Obj.LinkedFeatureID = OID; // OriId;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public void UpdateTextAND()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text AND.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string textValue = CreateTextValue();

            IPolyline textLine = (IPolyline)Shape.Clone();
            if (textLine.FromPoint.X > textLine.ToPoint.X)
            {
                textLine.ReverseOrientation();
            }

            List<GStreetTextAND> texts = GetTextsAND().ToList();

            if (texts.Count == 0)
            {
                GStreetTextAND text = repo.NewObj<GStreetTextAND>();
                text.LinkedFeatureID = OID; // OriId;
                text.Text = textValue;
                text.Shape = textLine;
                text = repo.Insert(text);
            }
            else
            {
                GStreetTextAND text = texts[0];
                text.Text = textValue;
                text.Shape = textLine;
                repo.Update(text);
            }

            // to take care previous program error
            if (texts.Count > 1)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(StringUtils.TrimSpaces(TypeValue));
            sb.Add(Name);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

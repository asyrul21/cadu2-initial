﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.BuildingTextsAND
{
    public class GAsBuildingText : GBuildingTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "AS_AND_BUILDING_TEXT";
    }
}

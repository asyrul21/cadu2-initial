﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.BuildingTextsAND
{
    public class GKnBuildingText : GBuildingTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KN_AND_BUILDING_TEXT";
    }
}

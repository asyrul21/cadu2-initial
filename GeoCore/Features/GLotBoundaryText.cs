﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features
{
    public abstract class GLotBoundaryText : GText
    {
        public const string LOTBOUNDARY_TEXT = "TEXTSTRING";

        [MappedField(LOTBOUNDARY_TEXT)]
        public virtual string lotBoundaryText { set; get; }
    }
}

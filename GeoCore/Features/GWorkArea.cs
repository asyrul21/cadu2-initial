﻿using System;
using System.Collections.Generic;
using Earthworm;
using System.Linq;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;

namespace Geomatic.Core.Features
{

    public class GWorkArea : GPolygonFeature
    {
        private static List<GWorkArea> _workArea;
        //public SegmentName SegmentName { get; private set; }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "AREA";

        public const string AREA_SEGMENT = "AREA_SEGMENT";
        public const string USER_ID = "USERID";
        public const string WO_NO = "WO_NO";
        public const string WO_DESC = "WO_DESC";
        public const string FLAG = "COMPLETE_FLAG";
        public new const string DATE_CREATED = "DATE_CREATED";
        public const string DATE_LAST_MODIFIED = "DATE_LMODIFIED";

        [MappedField(AREA_SEGMENT)]
        public virtual string WorkAreaSegmentName { set; get; }
        public override SegmentName SegmentName
        {
            get
            {
                if (WorkAreaSegmentName == "AS")
                    return SegmentName.AS;
                else if (WorkAreaSegmentName == "JH")
                    return SegmentName.JH;
                else if (WorkAreaSegmentName == "JP")
                    return SegmentName.JP;
                else if (WorkAreaSegmentName == "KG")
                    return SegmentName.KG;
                else if (WorkAreaSegmentName == "KK")
                    return SegmentName.KK;
                else if (WorkAreaSegmentName == "KN")
                    return SegmentName.KN;
                else if (WorkAreaSegmentName == "KV")
                    return SegmentName.KV;
                else if (WorkAreaSegmentName == "MK")
                    return SegmentName.MK;
                else if (WorkAreaSegmentName == "PG")
                    return SegmentName.PG;
                else if (WorkAreaSegmentName == "TG")
                    return SegmentName.TG;

                return SegmentName.None;
            }
        }

        [MappedField(USER_ID)]
        public virtual string user_id { set; get; }

        [MappedField(WO_NO)]
        public virtual string Wo_no { set; get; }

        [MappedField(WO_DESC)]
        public virtual string Wo_desc { set; get; }

        [MappedField(FLAG)]
        public virtual string flag { set; get; }

        [MappedField(DATE_CREATED)]
        public virtual string DateCreated1 { set; get; }

        [MappedField(DATE_LAST_MODIFIED)]
        public virtual string DateUpdated1 { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GWorkArea))
            {
                return false;
            }
            GWorkArea workarea = (GWorkArea)obj;
            return (OID == workarea.OID && TableName == workarea.TableName && ValueEquals(workarea, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public static IEnumerable<GWorkArea> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GWorkArea> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GWorkArea> query = new Query<GWorkArea>();
            query.Obj.flag = "1";
            if (updated)
            {
                _workArea = repo.Search<GWorkArea>(query).ToList();
            }
            else
            {
                if (_workArea == null)
                {
                    _workArea = repo.Search<GWorkArea>(query).ToList();
                }
            }
            return _workArea;
        }

        public static IEnumerable<GWorkArea> GetAll(bool updated, string segment)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GWorkArea> query = new Query<GWorkArea>();
            query.Obj.WorkAreaSegmentName = segment;
            _workArea = repo.Search(query).ToList();
            return _workArea;
        }

        public static IEnumerable<GWorkArea> GetAllCompleteAreaWithSegment()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GWorkArea> query = new Query<GWorkArea>();
            //query.Obj.flag = "1";
            query.AddClause(() => query.Obj.flag, "IN ('1','2')");
            _workArea = repo.Search(query).ToList();
            return _workArea;
        }

        // Added By Noraini Ali Mac 2020 - CADU 2 AND
        public static IEnumerable<GWorkArea> GetAllCompleteAreaByUser()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GWorkArea> query = new Query<GWorkArea>();
            //query.Obj.flag = "1";
            query.Obj.user_id = Session.User.Name.ToString();
            _workArea = repo.Search(query).ToList();
            return _workArea;
        }
        // end

        public virtual IEnumerable<GBuilding> GetBuildingAndStatus(bool filterADD)
        {
            return GetBuildingAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GBuilding> GetBuildingAndStatus(bool includeDeleted, bool filterADD)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GProperty> GetPropertyAndStatus(bool filterADD)
        {
            return GetPropertyAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GProperty> GetPropertyAndStatus(bool includeDeleted, bool filterADD)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GStreet> GetStreetAndStatus(bool filterADD)
        {
            return GetStreetAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GStreet> GetStreetAndStatus(bool includeDeleted, bool filterADD)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GLandmark> GetLandmarkAndStatus(bool filterADD)
        {
            return GetLandmarkAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GLandmark> GetLandmarkAndStatus(bool includeDeleted, bool filterADD)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GJunction> GetJunctionAndStatus(bool filterADD)
        {
            return GetJunctionAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GJunction> GetJunctionAndStatus(bool includeDeleted, bool filterADD)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GJunction> query = new Query<GJunction>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GBuildingGroup> GetBuildingGroupAndStatus(bool filterADD)
        {
            return GetBuildingGroupAndStatus(true, filterADD);
        }

        public virtual IEnumerable<GBuildingGroup> GetBuildingGroupAndStatus(bool includeDeleted, bool filterADD)
        {
            // noraini -
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.AreaId = OID;
            if (filterADD)
            {
                query.Obj.AndStatus = 1;
            }
            else
            {
                query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            }
            //if (!includeDeleted)
            //{
            //    query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            //}
            return repo.Search(query);
        }

        // Added by noraini ali - Jun 2020
        public virtual bool HasProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool HasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool HasBuildingGroup()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool HasLandmark()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool HasJunction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GJunction> query = new Query<GJunction>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool HasStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Obj.AreaId = OID;
            query.AddClause(() => query.Obj.AndStatus, "IN ('1','2','3','4')");
            return repo.Count(query) > 0;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasProperty();
            canDelete &= !HasStreet();
            canDelete &= !HasJunction();
            canDelete &= !HasBuilding();
            canDelete &= !HasBuildingGroup();
            canDelete &= !HasLandmark();
            return canDelete;
        }
        // end

        // Added By Noraini Ali Aug 2020 - CADU 2 AND
        public static IEnumerable<GWorkArea> GetCompletedWorkArea(string segmentname)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GWorkArea> query = new Query<GWorkArea>();
            //query.Obj.flag = "1";
            query.Obj.WorkAreaSegmentName = segmentname;
            query.AddClause(() => query.Obj.flag, "IN ('1','2')"); // 1-Closed/2-Verifying 
            _workArea = repo.Search(query).ToList();
            return _workArea;
        }
        // end

        public override string ToString()
        {
            List<string> sb = new List<string>();
            sb.Add(string.Format("ID : [ {0} ]", OID));
            sb.Add(",");
            sb.Add(WorkAreaSegmentName);
            sb.Add(",");
            sb.Add(user_id);
            sb.Add(",");
            sb.Add(Wo_no);
            sb.Add(",");
            sb.Add(Wo_desc);
            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Regions
{
    public class GKnRegion : GRegion
    {
        public const string TABLE_NAME = "KN_REGION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

     
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Regions
{
    public class GAsRegion : GRegion
    {
        public const string TABLE_NAME = "AS_REGION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

     
    }
}

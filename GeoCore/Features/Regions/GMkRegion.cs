﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Regions
{
    public class GMkRegion : GRegion
    {
        public const string TABLE_NAME = "MK_REGION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

     
    }
}

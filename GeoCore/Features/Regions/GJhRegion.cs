﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Regions
{
    public class GJhRegion : GRegion
    {
        public const string TABLE_NAME = "JH_REGION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

     
    }
}

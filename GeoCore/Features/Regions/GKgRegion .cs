﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Regions
{
    public class GKgRegion : GRegion
    {
        public const string TABLE_NAME = "KG_REGION";

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

     
    }
}

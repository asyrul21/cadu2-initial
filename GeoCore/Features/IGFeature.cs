﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public interface IGFeature : IGObject
    {
        IGeometry Shape { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.StreetTexts
{
    public class GMkStreetText : GStreetText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "MK_ADM_STREET_TEXT";
    }
}

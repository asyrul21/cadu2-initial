﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features
{
    public abstract class GLandmarkBoundary : GPolygonFeature
    {
        // added by noraini ali - Mei 2020 CADU2 AND
        public const string AND_STATUS = "AND_STATUS";
        public const string AREA_ID = "AREA_ID";

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }
        // end

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GLandmarkBoundary))
            {
                return false;
            }

            GLandmarkBoundary landmarkBoundary = (GLandmarkBoundary)obj;
            return (OID == landmarkBoundary.OID && TableName == landmarkBoundary.TableName && ValueEquals(landmarkBoundary, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        // added by Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GLandmarkBoundaryAND> GetLandmarkBoundaryAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkBoundaryAND> query = new Query<GLandmarkBoundaryAND>(SegmentName);
            query.Obj.OriId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GLandmarkBoundaryAND GetLandmarkBoundayANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GLandmarkBoundaryAND> landmarkBoundaryAND = GetLandmarkBoundaryAND().ToList();
            return landmarkBoundaryAND.Count() > 0 ? landmarkBoundaryAND.First() : null;
        }
        // end

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));

            return string.Join(" ", sb.ToArray());
        }
    }
}

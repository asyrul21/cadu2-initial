﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.StreetsAND
{
    public class GKnStreet : GStreetAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KN_AND_STREET";
    }
}

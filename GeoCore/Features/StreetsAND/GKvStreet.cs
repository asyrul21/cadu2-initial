﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features.StreetsAND
{
    public class GKvStreet : GStreetAND
    {
        private static List<GStreet> _streetName;

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_AND_STREET";

        public static IEnumerable<GStreet> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GStreet> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KV);
            Query<GStreet> query = new Query<GStreet>(SegmentName.KV);
            Console.WriteLine(query.QueryFilter.SubFields);

            if (updated)
            {
                _streetName = repo.Search<GStreet>().ToList();
            }
            else
            {
                if (_streetName == null)
                {
                    //_streetName = repo.Search<GStreet>().ToList();
                    _streetName = repo.Search(query).ToList();
                }
            }
            return _streetName;
        }
    }
}

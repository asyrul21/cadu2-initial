﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Pois
{
    public class GKkPoi : GPoi
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KK_ADM_POI";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GPolygonFeature : GFeature, IPolygonFeature
    {
        public IPolygon Polygon
        {
            get
            {
                return Shape as IPolygon;
            }
        }

        private IPolyline Polyline
        {
            get
            {
                return Shape as IPolyline;
            }
        }

        public IPoint FromPoint
        {
            set
            {
                Polygon.FromPoint = value;
                Shape = Polygon;
            }
            get
            {
                return Polygon.FromPoint;
            }
        }

        public IPoint ToPoint
        {
            set
            {
                Polygon.ToPoint = value;
                Shape = Polygon;
            }
            get
            {
                return Polygon.ToPoint;
            }
        }

        public IEnumerable<IPoint> GetVertices()
        {
            IPointCollection4 pointCollection = (IPointCollection4)Shape;
            for (int count = 0; count < pointCollection.PointCount; count++)
            {
                yield return pointCollection.get_Point(count);
            }
        }

        public List<IPoint> Vertices
        {
            get
            {
                return GetVertices().ToList();
            }
        }

        public int PointCount
        {
            get
            {
                IPointCollection4 pointCollection = (IPointCollection4)Shape;
                return pointCollection.PointCount;
            }
        }

        public virtual bool TryAddVertex(IPoint point)
        {
            bool isSplitted = false;
            int newPartIndex = 0;
            int newSegmentIndex = 0;
            IPolygon polygon = Polygon;
            polygon.SplitAtPoint(point, true, false, out isSplitted, out newPartIndex, out newSegmentIndex);

            Shape = polygon;

            return isSplitted;
        }

        public virtual bool TryDeleteVertex(int index)
        {
            if (PointCount <= 4)
            {
                return false;
            }
            IPolygon polygon = Polygon;

            IPointCollection4 pointCollection = (IPointCollection4)polygon;

            if (index == 0 || index >= pointCollection.PointCount - 1)
            {
                pointCollection.RemovePoints(pointCollection.PointCount - 1, 1);
                pointCollection.RemovePoints(0, 1);
                pointCollection.AddPoint(pointCollection.Point[0]);
            }
            else
            {
                pointCollection.RemovePoints(index, 1);
            }

            Shape = polygon;

            return true;
        }
    }
}

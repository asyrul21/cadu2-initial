﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.Junctions
{
    public class GMkJunction : GJunction
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public const string TABLE_NAME = "MK_ADM_JUNCTION";

        //added by asyrul

        private static List<GJunction> _allJunction;
        public static IEnumerable<GJunction> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GJunction> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.MK);
            if (updated)
            {
                _allJunction = repo.Search<GJunction>().ToList();
            }
            else
            {
                if (_allJunction == null)
                {
                    _allJunction = repo.Search<GJunction>().ToList();
                }
            }
            return _allJunction;
        }
        //added end
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.Junctions
{
    public class GKkJunction : GJunction
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public const string TABLE_NAME = "KK_ADM_JUNCTION";

        //added by asyrul

        private static List<GJunction> _allJunction;
        public static IEnumerable<GJunction> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GJunction> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KK);
            if (updated)
            {
                _allJunction = repo.Search<GJunction>().ToList();
            }
            else
            {
                if (_allJunction == null)
                {
                    _allJunction = repo.Search<GJunction>().ToList();
                }
            }
            return _allJunction;
        }
        //added end
    }
}

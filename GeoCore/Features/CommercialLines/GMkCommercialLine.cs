﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.CommercialLines
{
    public class GMkCommercialLine : GCommercialLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "MK_Commercial_Line";
    }
}

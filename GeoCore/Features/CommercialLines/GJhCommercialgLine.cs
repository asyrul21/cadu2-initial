﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.CommercialLines
{
    public class GJhCommercialLine : GCommercialLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JH_Commercial_Line";
    }
}

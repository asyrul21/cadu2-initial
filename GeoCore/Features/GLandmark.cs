﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using System.Text.RegularExpressions;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GLandmark : GPointFeature, IUserCreate, IUserUpdate, IGNavigationObject, IGNepsObject, IGPoiParent
    {
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string CODE = "LANDMARK_CODE";
        public const string NAME = "LANDMARK_NAME";
        public const string NAME2 = "LANDMARK_NAME2";
        public const string STREET_ID = "STREET_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        // noraini ali
        public const string AND_STATUS = "AND_STATUS";
        // end
        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        public virtual string Code1
        {
            get
            {
                return GCode.GetCode1(Code);
            }
        }

        public virtual string Code2
        {
            get
            {
                return GCode.GetCode2(Code);
            }
        }

        public virtual string Code3
        {
            get
            {
                return GCode.GetCode3(Code);
            }
        }

        public virtual GCode1 GetCode1()
        {
            return GCode1.Get(Code1);
        }

        public virtual GCode2 GetCode2()
        {
            return GCode2.Get(Code1, Code2);
        }

        public virtual GCode3 GetCode3()
        {
            return GCode3.Get(Code1, Code2, Code3);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual IEnumerable<GLandmarkText> GetTexts()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkText> query = new Query<GLandmarkText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GStreet GetStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreet>(StreetId.Value) : null;
        }

        public virtual GStreetAND GetStreetAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreetAND>(StreetId.Value) : null;
        }

        public virtual IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = OID;
            query.Obj.ParentType = 4;
            return repo.Search(query);
        }

        public virtual bool HasText()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkText> query = new Query<GLandmarkText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return repo.Count<GLandmarkText>(query) > 0;
        }

        public virtual bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = OID;
            query.Obj.ParentType = 4;
            return repo.Count<GPoi>(query) > 0;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasPoi();
            canDelete &= !HasText();
            return canDelete;
        }

        public void Init()
        {
            NavigationStatus = 0;
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GLandmark))
            {
                return false;
            }

            GLandmark landmark = (GLandmark)obj;
            return (OID == landmark.OID && TableName == landmark.TableName && ValueEquals(landmark, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public string CreateText1Value()
        {
            return Name;
        }

        public string CreateText2Value()
        {
            return Name2;
        }

        public void UpdateText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GLandmarkText> texts = GetTexts().ToList();

            if (texts.Count == 0)
            {
                GLandmarkText text = repo.NewObj<GLandmarkText>();
                text.LinkedFeatureID = OID;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GLandmarkText text2 = repo.NewObj<GLandmarkText>();
                    text2.LinkedFeatureID = OID;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GLandmarkText text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GLandmarkText text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GLandmarkText text2 = repo.NewObj<GLandmarkText>();
                        text2.LinkedFeatureID = OID;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }

            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(Name2);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }

        //added by asyrul

        private static List<GLandmark> _allLandmark;

        public static IEnumerable<GLandmark> GetAll(bool updated, SegmentName segment)
        {
            RepositoryFactory repo = new RepositoryFactory(segment);
            if (updated)
            {
                _allLandmark = repo.Search<GLandmark>().ToList();
            }
            else
            {
                if (_allLandmark == null)
                {
                    _allLandmark = repo.Search<GLandmark>().ToList();
                }
            }
            return _allLandmark;
        }
        //added end

        // added by Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GLandmarkAND> GetLandmarkAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);
            query.Obj.OriId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GLandmarkAND GetLandmarkANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GLandmarkAND> landmarkAND = GetLandmarkAND().ToList();
            return landmarkAND.Count() > 0 ? landmarkAND.First() : null;
        }
        //
        public virtual IEnumerable<GLandmarkAND> GetLandmarkAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GLandmarkAND GetLandmarkANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GLandmarkAND> landmarkAND = GetLandmarkAND(includeDeleted).ToList();
            return landmarkAND.Count() > 0 ? landmarkAND.First() : null;
        }
        public virtual void CopyFrom(GLandmark landmark)
        {
            this.NavigationStatus = landmark.NavigationStatus;
            this.Code = landmark.Code;
            this.Name = landmark.Name;
            this.Name2 = landmark.Name2;
            this.Source = landmark.Source;
            this.StreetId = landmark.StreetId;
        }
        // end add
    }
}

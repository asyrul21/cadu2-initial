﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.HydrographicPolygons
{
    public class GJpHydrographicPolygon : GHydrographicPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_Hydrographic";
    }
}

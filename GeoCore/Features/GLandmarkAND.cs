﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using System.Text.RegularExpressions;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GLandmarkAND : GLandmark
    {
        public const string ORI_ID = "ORIGINAL_ID";

        public static object GAsLandmark { get; set; }
        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OriId;
            query.Obj.ParentType = 4;
            return repo.Search(query);
        }

        public override bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OriId; ;
            query.Obj.ParentType = 4;
            return repo.Count<GPoi>(query) > 0;
        }

        public override GStreet GetStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreet>(StreetId.Value) : null;
        }

        public override void CopyFrom(GLandmark landmark)
        {
            this.NavigationStatus = landmark.NavigationStatus;
            this.Code = landmark.Code;
            this.Name = landmark.Name;
            this.Name2 = landmark.Name2;
            this.Source = landmark.Source;
            this.StreetId = landmark.StreetId;
            this.AreaId = landmark.AreaId;
            this.Shape = landmark.Point;
            this.OriId = landmark.OID;
            this.CreatedBy = landmark.CreatedBy;
            this.DateCreated = landmark.DateCreated;
            this.UpdatedBy = landmark.UpdatedBy;
            this.DateUpdated = landmark.DateUpdated;
        }

        public virtual IEnumerable<GLandmarkTextAND> GetTextsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkTextAND> query = new Query<GLandmarkTextAND>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public void UpdateTextAND()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text AND.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GLandmarkTextAND> texts = GetTextsAND().ToList();

            if (texts.Count == 0)
            {
                GLandmarkTextAND text = repo.NewObj<GLandmarkTextAND>();
                text.LinkedFeatureID = OID; // OriId;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GLandmarkTextAND text2 = repo.NewObj<GLandmarkTextAND>();
                    text2.LinkedFeatureID = OID; // OriId;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GLandmarkTextAND text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GLandmarkTextAND text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GLandmarkTextAND text2 = repo.NewObj<GLandmarkTextAND>();
                        text2.LinkedFeatureID = OID; // OriId;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }

            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(Name);
            sb.Add(Name2);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }
    }
}

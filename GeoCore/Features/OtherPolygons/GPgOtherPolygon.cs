﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.OtherPolygons
{
    public class GPgOtherPolygon : GOtherPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "PG_Other_Lot";
    }
}

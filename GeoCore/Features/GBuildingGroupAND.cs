﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Earthworm;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features
{
    /// <summary>
    /// For AND Feature
    /// </summary>
    public abstract class GBuildingGroupAND : GBuildingGroup
    {
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override IEnumerable<GBuilding> GetBuildings()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.GroupId = this.OriId;
            return repo.Search(query);
        }

        public override IEnumerable<GBuildingAND> GetBuildingsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.GroupId = this.OriId;
            return repo.Search(query);
        }

        public override bool HasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.GroupId = this.OriId;
            return repo.Count(query) > 0;
        }

        public override bool HasBuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.GroupId = this.OriId;
            return repo.Count(query) > 0;
        }

        public override void CopyFrom(GBuildingGroup buildingGroup)
        {
            this.NavigationStatus = buildingGroup.NavigationStatus;
            this.Code = buildingGroup.Code;
            this.Name = buildingGroup.Name;
            this.Source = buildingGroup.Source;
            this.BuldingNamePos = buildingGroup.BuldingNamePos;
            this.AreaId = buildingGroup.AreaId;
            this.Shape = buildingGroup.Point;
            this.StreetId = buildingGroup.StreetId;
            this.OriId = buildingGroup.OID;
            this.CreatedBy = buildingGroup.CreatedBy;
            this.DateCreated = buildingGroup.DateCreated;
            this.UpdatedBy = buildingGroup.UpdatedBy;
            this.DateUpdated = buildingGroup.DateUpdated;
            this.NumUnit = buildingGroup.NumUnit;
        }

        public virtual IEnumerable<GBuildingGroupTextAND> GetTextsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroupTextAND> query = new Query<GBuildingGroupTextAND>(SegmentName);
            query.Obj.LinkedFeatureID = OID; // OriId;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public void UpdateTextAND()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text AND.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string textValue = CreateTextValue();

            IPoint textPoint = (IPoint)Shape.Clone();
            textPoint.Move(0, 0.6);

            List<GBuildingGroupTextAND> texts = GetTextsAND().ToList();

            if (texts.Count == 0)
            {
                GBuildingGroupTextAND text = repo.NewObj<GBuildingGroupTextAND>();
                text.LinkedFeatureID = OID;
                text.Text = textValue;
                text.Shape = textPoint;
                text = repo.Insert(text);
            }
            else
            {
                GBuildingGroupTextAND text = texts[0];
                text.Text = textValue;
                text.Shape = textPoint;
                repo.Update(text);
            }

            // to take care previous program error
            if (texts.Count > 1)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(Name);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(street.TypeValue);
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using Geomatic.Core.Repositories;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GStreet : GLineFeature, IUserCreate, IUserUpdate, IGNavigationObject, IGNepsObject
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string STATUS = "STREET_STATUS";
        public const string NETWORK_CLASS = "ROAD_CLASS";
        public const string CLASS = "STREET_CLASS";
        public const string CATEGORY = "STREET_CATEGORY";
        public const string FILTER_LEVEL = "FILTER_LEVEL";
        public const string DESIGN = "DESIGN";
        public const string DIRECTION = "ONE_WAY";
        public const string TOLL_TYPE = "TOLL_TYPE";
        public const string DIVIDER = "DIVIDER";
        public const string TYPE = "STREET_TYPE";
        public const string NAME = "STREET_NAME";
        public const string NAME2 = "STREET_ALT_NAME";
        public const string SECTION = "SECTION_NAME";
        public const string POSTCODE = "POSTAL_NUM";
        public const string CITY = "CITY_NAME";
        public const string SUB_CITY = "SUB_CITY";
        public const string STATE = "STATE_CODE";
        public const string FROM_ID = "SOURCE_ID";
        public const string TO_ID = "TERMINATION_ID";
        public const string SPEED_LIMIT = "SPEED_LIMIT";
        public const string WEIGHT = "MAX_WEIGHT";
        public const string ELEVATION = "STREET_ELEVATION";
        public const string LENGTH = "STREET_LENGTH";
        public const string WIDTH = "STREET_WIDTH";
        public const string HEIGHT = "STREET_HEIGHT";
        public const string LANES = "LANES_COUNT";
        public const string BICYCLE = "NOT_FOR_BICYCLE";
        public const string BUS = "NOT_FOR_BUS";
        public const string CAR = "NOT_FOR_CAR";
        public const string DELIVERY = "NOT_FOR_DELIVERY";
        public const string EMERGENCY = "NOT_FOR_EMERGENCY";
        public const string PEDESTRIAN = "NOT_FOR_PEDESTRIAN";
        public const string TRUCK = "NOT_FOR_TRUCK";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        private const int MIN_LENGTH = 5;
        // noraini ali
        public const string AND_STATUS = "AND_STATUS";
        // end

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        public virtual string ConstructionStatusValue
        {
            get
            {
                GConstructionStatus constructionStatus = GetConstructionStatusObj();
                return StringUtils.TrimSpaces((constructionStatus == null) ? string.Empty : constructionStatus.Name);
            }
        }

        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        [MappedField(STATUS)]
        public virtual int? Status { set; get; }

        public virtual string StatusValue
        {
            get
            {
                GStreetStatus status = GetStatusObj();
                return StringUtils.TrimSpaces((status == null) ? string.Empty : status.Name);
            }
        }

        [MappedField(NETWORK_CLASS)]
        public virtual int? NetworkClass { set; get; }

        public virtual string NetworkClassValue
        {
            get
            {
                GStreetNetworkClass networkClass = GetNetworkClassObj();
                return StringUtils.TrimSpaces((networkClass == null) ? string.Empty : networkClass.Name);
            }
        }

        [MappedField(CLASS)]
        public virtual int? Class { set; get; }

        public virtual string ClassValue
        {
            get
            {
                GStreetClass streetClass = GetClassObj();
                return StringUtils.TrimSpaces((streetClass == null) ? string.Empty : streetClass.Name);
            }
        }

        [MappedField(CATEGORY)]
        public virtual int? Category { set; get; }

        public virtual string CategoryValue
        {
            get
            {
                GStreetCategory category = GetCategoryObj();
                return StringUtils.TrimSpaces((category == null) ? string.Empty : category.Name);
            }
        }

        [MappedField(FILTER_LEVEL)]
        public virtual int? FilterLevel { set; get; }

        public virtual string FilterLevelValue
        {
            get
            {
                GStreetFilterLevel filterLevel = GetFilterLevelObj();
                return StringUtils.TrimSpaces((filterLevel == null) ? string.Empty : filterLevel.Name);
            }
        }

        [MappedField(DESIGN)]
        public virtual int? Design { set; get; }

        public virtual string DesignValue
        {
            get
            {
                GStreetDesign design = GetDesignObj();
                return StringUtils.TrimSpaces((design == null) ? string.Empty : design.Name);
            }
        }

        [MappedField(DIRECTION)]
        public virtual int? Direction { set; get; }

        public virtual string DirectionValue
        {
            get
            {
                GStreetDirection direction = GetDirectionObj();
                return StringUtils.TrimSpaces((direction == null) ? string.Empty : direction.Name);
            }
        }

        [MappedField(TOLL_TYPE)]
        public virtual int? TollType { set; get; }

        public virtual string TollTypeValue
        {
            get
            {
                GStreetTollType tollType = GetTollTypeObj();
                return StringUtils.TrimSpaces((tollType == null) ? string.Empty : tollType.Name);
            }
        }

        [MappedField(DIVIDER)]
        public virtual int? Divider { set; get; }

        public virtual string DividerValue
        {
            get
            {
                GStreetDivider divider = GetDividerObj();
                return StringUtils.TrimSpaces((divider == null) ? string.Empty : divider.Name);
            }
        }

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        public virtual string TypeValue
        {
            get
            {
                GStreetType streetType = GetTypeObj();
                return StringUtils.TrimSpaces((streetType == null) ? string.Empty : streetType.Name);
            }
        }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(SECTION)]
        public virtual string Section { set; get; }

        [MappedField(POSTCODE)]
        public virtual string Postcode { set; get; }

        [MappedField(CITY)]
        public virtual string City { set; get; }

        [MappedField(SUB_CITY)]
        public virtual string SubCity { set; get; }

        [MappedField(STATE)]
        public virtual string State { set; get; }

        [MappedField(FROM_ID)]
        public virtual int? FromNodeId { set; get; }

        [MappedField(TO_ID)]
        public virtual int? ToNodeId { set; get; }

        [MappedField(SPEED_LIMIT)]
        public virtual int? SpeedLimit { set; get; }

        [MappedField(WEIGHT)]
        public virtual double? Weight { set; get; }

        [MappedField(ELEVATION)]
        public virtual int? Elevation { set; get; }

        [MappedField(LENGTH)]
        public virtual double? Length { set; get; }

        [MappedField(WIDTH)]
        public virtual double? Width { set; get; }

        [MappedField(HEIGHT)]
        public virtual double? Height { set; get; }

        [MappedField(LANES)]
        public virtual int? NumOfLanes { set; get; }

        [MappedField(BICYCLE)]
        public virtual int? Bicycle { set; get; }

        [MappedField(BUS)]
        public virtual int? Bus { set; get; }

        [MappedField(CAR)]
        public virtual int? Car { set; get; }

        [MappedField(DELIVERY)]
        public virtual int? Delivery { set; get; }

        [MappedField(EMERGENCY)]
        public virtual int? Emergency { set; get; }

        [MappedField(PEDESTRIAN)]
        public virtual int? Pedestrian { set; get; }

        [MappedField(TRUCK)]
        public virtual int? Truck { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        // noraini ali
        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        public virtual GConstructionStatus GetConstructionStatusObj()
        {
            return GConstructionStatus.Get(ConstructionStatus);
        }

        public virtual GStreetStatus GetStatusObj()
        {
            return GStreetStatus.Get(Status);
        }

        public virtual GStreetNetworkClass GetNetworkClassObj()
        {
            return GStreetNetworkClass.Get(NetworkClass);
        }

        public virtual GStreetClass GetClassObj()
        {
            return GStreetClass.Get(Class);
        }

        public virtual GStreetCategory GetCategoryObj()
        {
            return GStreetCategory.Get(Category);
        }

        public virtual GStreetFilterLevel GetFilterLevelObj()
        {
            return GStreetFilterLevel.Get(FilterLevel);
        }

        public virtual GStreetDesign GetDesignObj()
        {
            return GStreetDesign.Get(Design);
        }

        public virtual GStreetDirection GetDirectionObj()
        {
            return GStreetDirection.Get(Direction);
        }

        public virtual GStreetTollType GetTollTypeObj()
        {
            return GStreetTollType.Get(TollType);
        }

        public virtual GStreetDivider GetDividerObj()
        {
            return GStreetDivider.Get(Divider);
        }

        public virtual GStreetType GetTypeObj()
        {
            return GStreetType.Get(Type);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual IEnumerable<GStreetText> GetTexts()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetText> query = new Query<GStreetText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual bool HasText()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            Query<GStreetText> query = new Query<GStreetText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Count(query) > 0 : false;
        }

        public virtual bool HasProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasLandmark()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasBuildingGroup()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.StartId = OID;
            query.Obj.EndId1 = OID;
            query.Obj.EndId2 = OID;
            query.Obj.EndId3 = OID;
            query.Obj.EndId4 = OID;
            query.Obj.EndId5 = OID;
            query.Obj.EndId6 = OID;
            query.Obj.EndId7 = OID;
            query.Obj.EndId8 = OID;
            query.Obj.EndId9 = OID;
            query.Obj.EndId10 = OID;
            return repo.Count(query) > 0;
        }

        public virtual GJunction GetFromNode()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (FromNodeId.HasValue) ? repo.GetById<GJunction>(FromNodeId.Value) : null;
        }

        public virtual GJunction GetToNode()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (ToNodeId.HasValue) ? repo.GetById<GJunction>(ToNodeId.Value) : null;
        }

        public virtual IEnumerable<GProperty> GetProperties()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GPropertyAND> GetPropertiesAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyAND> query = new Query<GPropertyAND>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GLandmark> GetLandmarks()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GLandmarkAND> GetLandmarksAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GBuildingGroup> GetBuildingGroups()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }
        public virtual IEnumerable<GBuildingGroupAND> GetBuildingGroupsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);
            query.Obj.StreetId = OID;
            return repo.Search(query);
        }

        ////added by asyrul
        //public virtual IEnumerable<GStreet> GetAllStreetName()
        //{
        //    return GetAllStreetName(true);
        //}

        //public static IEnumerable<GStreet> GetAllStreetName(bool updated)
        //{
        //    RepositoryFactory repo = new RepositoryFactory();

        //    if (updated)
        //    {
        //        _streetName = repo.Search<GStreet>().ToList();
        //    }
        //    else
        //    {
        //        if (_streetName == null)
        //        {
        //            _streetName = repo.Search<GStreet>().ToList();
        //        }
        //    }
        //    return _streetName;
        //}

        ////added end

        /// <summary>
        /// Need to reassign from and to node
        /// </summary>
        /// <param name="point"></param>
        /// <param name="newStreet"></param>
        /// <returns></returns>
        public virtual bool TrySplitAtPoint(IPoint point, out GStreet newStreet)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            newStreet = null;
            bool isSplitted = false;
            int newPartIndex = 0;
            int newSegmentIndex = 0;
            IPolyline polyline = Polyline;
            ISpatialReference spatialReference = polyline.SpatialReference;

            polyline.SplitAtPoint(point, true, false, out isSplitted, out newPartIndex, out newSegmentIndex);

            if (!isSplitted)
            {
                return false;
            }

            ISegmentCollection segmentCollection = (ISegmentCollection)polyline;

            // new line 1
            IPolyline newLine1 = CreateLine(segmentCollection, spatialReference, 0, newSegmentIndex - 1, MIN_LENGTH);

            Shape = newLine1;
            Length = newLine1.Length;   // noraini - update new length
            repo.Update(this);

            // new line 2
            IPolyline newLine2 = CreateLine(segmentCollection, spatialReference, newSegmentIndex, segmentCollection.SegmentCount - 1, MIN_LENGTH);

            newStreet = repo.NewObj<GStreet>();
            newStreet.FromNodeId = -1;
            newStreet.ToNodeId = -1;
            newStreet.Shape = newLine2;
            newStreet.Length = newLine2.Length; // noraini - update new length
            newStreet = repo.Insert(newStreet, false);

            GStreetText newText = repo.NewObj<GStreetText>();

            IPolyline line = newLine2.Clone();
            if (line.FromPoint.X > line.ToPoint.X)
            {
                line.ReverseOrientation();
            }
            newText.Size = 3;
            newText.Shape = line;
            newText.LinkedFeatureID = newStreet.OID;
            newText = repo.Insert(newText);

            return isSplitted;
        }

        public void Init()
        {
            ConstructionStatus = 1;
            NavigationStatus = 0;
            Status = 2;
            FilterLevel = 1;
            Design = 10;
            Direction = 1;
            TollType = 1;
            Source = "1";
            NetworkClass = 4;
            Class = 4;
            Category = 15;
            Divider = 0;
            NumOfLanes = 1;

            Bicycle = 0;
            Bus = 0;
            Car = 0;
            Delivery = 0;
            Emergency = 0;
            Pedestrian = 0;
            Truck = 0;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GStreet))
            {
                return false;
            }

            GStreet street = (GStreet)obj;
            return (OID == street.OID && TableName == street.TableName && ValueEquals(street, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public string CreateTextValue()
        {
            GStreetType streetType = GetTypeObj();

            List<string> sb = new List<string>();
            sb.Add((streetType == null) ? string.Empty : StringUtils.TrimSpaces(streetType.Abbreviation));
            sb.Add(Name);
            return string.Join(" ", sb.ToArray());
        }

        public void UpdateText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string textValue = CreateTextValue();

            IPolyline textLine = (IPolyline)Shape.Clone();
            if (textLine.FromPoint.X > textLine.ToPoint.X)
            {
                textLine.ReverseOrientation();
            }

            List<GStreetText> texts = GetTexts().ToList();

            if (texts.Count == 0)
            {
                GStreetText text = repo.NewObj<GStreetText>();
                text.LinkedFeatureID = OID;
                text.Text = textValue;
                text.Shape = textLine;
                text = repo.Insert(text);
            }
            else
            {
                GStreetText text = texts[0];
                text.Text = textValue;
                text.Shape = textLine;
                repo.Update(text);
            }

            // to take care previous program error
            if (texts.Count > 1)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            } 
        }

        // added by asyrul
        public void DeleteText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to delete text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GStreetText> texts = GetTexts().ToList();
            if (texts.Count != 0) //text found
            {
                texts.ForEach(streetText => { repo.Delete(streetText); });
            }
        }
        // added end

        public bool IsConnectedTo(GStreet street)
        {
            if (Equals(street))
            {
                throw new Exception("Unable to check if connected to itself.");
            }
            return (FromNodeId == street.FromNodeId || ToNodeId == street.FromNodeId || FromNodeId == street.ToNodeId || ToNodeId == street.ToNodeId);
        }

        public int ConnectedAt(GStreet street)
        {
            if (!IsConnectedTo(street))
            {
                throw new Exception("Street not connected");
            }
            if (FromNodeId.HasValue && street.FromNodeId.HasValue && FromNodeId == street.FromNodeId)
            {
                return FromNodeId.Value;
            }
            if (FromNodeId.HasValue && street.ToNodeId.HasValue && FromNodeId == street.ToNodeId)
            {
                return FromNodeId.Value;
            }
            if (ToNodeId.HasValue && street.FromNodeId.HasValue && ToNodeId == street.FromNodeId)
            {
                return ToNodeId.Value;
            }
            if (ToNodeId.HasValue && street.ToNodeId.HasValue && ToNodeId == street.ToNodeId)
            {
                return ToNodeId.Value;
            }
            throw new Exception("Unable to not connected");
        }

        public bool Compare(GStreet street)
        {
            bool isSame = true;

            isSame &= this.ConstructionStatus == street.ConstructionStatus;
            isSame &= this.Status == street.Status;
            isSame &= this.NetworkClass == street.NetworkClass;
            isSame &= this.Class == street.Class;
            isSame &= this.Category == street.Category;
            isSame &= this.FilterLevel == street.FilterLevel;
            isSame &= this.Design == street.Design;
            isSame &= this.Direction == street.Direction;
            isSame &= this.TollType == street.TollType;
            isSame &= this.Divider == street.Divider;
            isSame &= this.Source == street.Source;

            isSame &= this.Type == street.Type;
            isSame &= this.Name == street.Name;
            isSame &= this.Name2 == street.Name2;
            isSame &= this.Section == street.Section;
            isSame &= this.Postcode == street.Postcode;
            isSame &= this.City == street.City;
            isSame &= this.SubCity == street.SubCity;
            isSame &= this.State == street.State;

            isSame &= this.SpeedLimit == street.SpeedLimit;
            isSame &= this.Weight == street.Weight;
            isSame &= this.Elevation == street.Elevation;
            isSame &= this.Length == street.Length;
            isSame &= this.Width == street.Width;
            isSame &= this.Height == street.Height;
            isSame &= this.NumOfLanes == street.NumOfLanes;

            isSame &= this.Bicycle == street.Bicycle;
            isSame &= this.Bus == street.Bus;
            isSame &= this.Car == street.Car;
            isSame &= this.Delivery == street.Delivery;
            isSame &= this.Emergency == street.Emergency;
            isSame &= this.Pedestrian == street.Pedestrian;
            isSame &= this.Truck == street.Truck;

            return isSame;
        }

        public virtual void CopyFrom(GStreet street)
        {
            this.ConstructionStatus = street.ConstructionStatus;
            this.Status = street.Status;
            this.NetworkClass = street.NetworkClass;
            this.Class = street.Class;
            this.Category = street.Category;
            this.FilterLevel = street.FilterLevel;
            this.Design = street.Design;
            this.Direction = street.Direction;
            this.TollType = street.TollType;
            this.Divider = street.Divider;
            this.Source = street.Source;

            this.Type = street.Type;
            this.Name = street.Name;
            this.Name2 = street.Name2;
            this.Section = street.Section;
            this.Postcode = street.Postcode;
            this.City = street.City;
            this.SubCity = street.SubCity;
            this.State = street.State;

            this.SpeedLimit = street.SpeedLimit;
            this.Weight = street.Weight;
            this.Elevation = street.Elevation;
            this.Length = street.Length;
            this.Width = street.Width;
            this.Height = street.Height;
            this.NumOfLanes = street.NumOfLanes;

            this.Bicycle = street.Bicycle;
            this.Bus = street.Bus;
            this.Car = street.Car;
            this.Delivery = street.Delivery;
            this.Emergency = street.Emergency;
            this.Pedestrian = street.Pedestrian;
            this.Truck = street.Truck;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasProperty();
            canDelete &= !HasLandmark();
            canDelete &= !HasBuildingGroup();
            canDelete &= !HasRestriction();
            canDelete &= !HasText();
            return canDelete;
        }

        // Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GStreetAND> GetStreetAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.Obj.OriId = this.OID;
            return (OID > 0 ) ? repo.Search(query) : null;
        }

        public virtual GStreetAND GetStreetANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GStreetAND> streetAND = GetStreetAND().ToList();
            return streetAND.Count() > 0 ? streetAND.First() : null;
        }

        public virtual IEnumerable <GStreetAND> GetStreetsANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GStreetAND> streetAND = GetStreetAND().ToList();
            return streetAND.Count() > 0 ? streetAND : null;
        }

        public virtual IEnumerable<GStreetAND> GetStreetAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GStreetAND GetStreetANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GStreetAND> streetAND = GetStreetAND(includeDeleted).ToList();
            return streetAND.Count() > 0 ? streetAND.First(): null;
        }

        // end add

        public virtual void CopyFromAND(GStreet street)
        {
            this.FromNodeId = street.FromNodeId;
            this.ToNodeId = street.ToNodeId;
            this.ConstructionStatus = street.ConstructionStatus;
            this.NavigationStatus = street.NavigationStatus;
            this.Status = street.Status;
            this.NetworkClass = street.NetworkClass;
            this.Class = street.Class;
            this.Category = street.Category;
            this.FilterLevel = street.FilterLevel;
            this.Design = street.Design;
            this.Direction = street.Direction;
            this.TollType = street.TollType;
            this.Divider = street.Divider;
            this.Source = street.Source;
            this.Shape = street.Shape;
            this.Type = street.Type;
            this.Name = street.Name;
            this.Name2 = street.Name2;
            this.Section = street.Section;
            this.Postcode = street.Postcode;
            this.City = street.City;
            this.SubCity = street.SubCity;
            this.State = street.State;
            this.AndStatus = 0;
            this.UpdateStatus = 0; 
            this.DateUpdated = street.DateUpdated;
            this.UpdatedBy = street.UpdatedBy;
            if (street.NumOfLanes != null)
            {
                this.NumOfLanes = street.NumOfLanes;
            }
            else
                this.NumOfLanes = 1;

            if (street.Divider != null)
            {
                this.Divider = street.Divider;
            }
            else
            { 
                this.Divider = 1;
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(StringUtils.TrimSpaces(TypeValue));
            sb.Add(Name);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkBoundaries
{
    public class GKgLandmarkBoundary : GLandmarkBoundary
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public const string TABLE_NAME = "KG_ADM_LANDMARK_BND";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Features
{
    public static class FeatureExt
    {
        public static IEnumerable<string> GetFieldNames(this IFeature feature)
        {
            return ((IRow)feature).GetFieldNames();
        }

        public static object GetValue(this IFeature feature, string fieldName)
        {
            return ((IRow)feature).GetValue(fieldName);
        }

        public static void SetValue(this IFeature feature, string fieldName, object value)
        {
            ((IRow)feature).SetValue(fieldName, value);
        }
    }
}

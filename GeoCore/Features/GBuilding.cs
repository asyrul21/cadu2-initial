﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Rows;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GBuilding : GPointFeature, IUserCreate, IUserUpdate, IGNavigationObject, IGNepsObject, IGPoiParent
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string CODE = "BUILDING_CODE";
        public const string NAME = "BUILDING_NAME";
        public const string NAME2 = "BUILDING_NAME2";
        public const string PROPERTY_ID = "PROPERTY_ID";
        public const string GROUP_ID = "BUILDING_GROUP_ID";
        public const string NUM_FLOOR = "NUM_FLOOR";
        public const string UNIT = "FLOOR_NUM_UNIT";
        public const string SPACE = "UNIT_SPACE";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        // added by noraini
        public const string AND_STATUS = "AND_STATUS";
        // end

        public const string FORECAST_NUM_UNIT = "FORECAST_NUM_UNIT";
        public const string FORECAST_SOURCE = "FORECAST_SOURCE";

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        public virtual string ConstructionStatusValue
        {
            get
            {
                GConstructionStatus constructionStatus = GetConstructionStatusObj();
                return StringUtils.TrimSpaces((constructionStatus == null) ? string.Empty : constructionStatus.Name);
            }
        }

        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(PROPERTY_ID)]
        public virtual int? PropertyId { set; get; }

        [MappedField(GROUP_ID)]
        public virtual int? GroupId { set; get; }

        [MappedField(NUM_FLOOR)]
        public virtual int? NumberOfFloor { set; get; }

        [MappedField(UNIT)]
        public virtual int? Unit { set; get; }

        [MappedField(SPACE)]
        public virtual double? Space { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // added by noraini
        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        [MappedField(FORECAST_NUM_UNIT)]
        public virtual int? ForecastNumUnit { set; get; }

        [MappedField(FORECAST_SOURCE)]
        public virtual int? ForecastSource { set; get; }

        public virtual string ForecastSourceValue
        {
            get
            {
                GSource source = GetForecastSourceObj();
                return (source == null) ? GSource.DEFAULT : source.Name;
            }
        }

        public virtual string Code1
        {
            get
            {
                return GCode.GetCode1(Code);
            }
        }

        public virtual string Code2
        {
            get
            {
                return GCode.GetCode2(Code);
            }
        }

        public virtual string Code3
        {
            get
            {
                return GCode.GetCode3(Code);
            }
        }

        public virtual GCode1 GetCode1()
        {
            return GCode1.Get(Code1);
        }

        public virtual GCode2 GetCode2()
        {
            return GCode2.Get(Code1, Code2);
        }

        public virtual GCode3 GetCode3()
        {
            return GCode3.Get(Code1, Code2, Code3);
        }

        public virtual GConstructionStatus GetConstructionStatusObj()
        {
            return GConstructionStatus.Get(ConstructionStatus);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual GSource GetForecastSourceObj()
        {
            return GSource.Get(ForecastSource);
        }

        public virtual IEnumerable<GBuildingText> GetTexts()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingText> query = new Query<GBuildingText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<GProperty>(PropertyId.Value) : null;
        }

        public virtual GPropertyAND GetPropertyAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<GPropertyAND>(PropertyId.Value) : null;
        }

        public virtual GStreet GetStreet()
        {
            GProperty property = GetProperty();
            return (property != null) ? property.GetStreet() : null;
        }

        /// <summary>
        /// Default not include deleted
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<GMultiStorey> GetMultiStories()
        {
            return GetMultiStories(false);
        }

        public virtual IEnumerable<GMultiStorey> GetMultiStories(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
            query.Obj.BuildingId = OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = OID;
            query.Obj.ParentType = 3;
            return repo.Search(query);
        }

        // Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GBuildingAND> GetBuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.OriId = this.OID;
            query.AddClause(() => query.Obj.AndStatus, "<> 2");
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GBuildingAND GetBuildingANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingAND> buildingAND = GetBuildingAND().ToList();
            return (buildingAND.Count() > 0 ) ? buildingAND.First() : null;
        }

        // noraini ali - use for verification module, selected data include deleted Status
        public virtual IEnumerable<GBuildingAND> GetBuildingAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GBuildingAND GetBuildingANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingAND> buildingAND = GetBuildingAND(includeDeleted).ToList();
            return (buildingAND.Count() > 0) ? buildingAND.First() : null;
        }
        // end

        public virtual void CopyFrom(GBuilding building)
        {
            this.ConstructionStatus = building.ConstructionStatus;
            this.NavigationStatus = building.NavigationStatus;
            this.Code = building.Code;
            this.Name = building.Name;
            this.Name2 = building.Name2;
            this.NumberOfFloor = building.NumberOfFloor;
            this.PropertyId = building.PropertyId;
            this.Source = building.Source;
            this.ForecastNumUnit = building.ForecastNumUnit;
            this.ForecastSource = building.ForecastSource;
            this.Space = building.Space;
            this.Unit = building.Unit;
        }
        // end add

        public virtual bool HasText()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingText> query = new Query<GBuildingText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasMultiStorey()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
            query.Obj.BuildingId = OID;
            query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public virtual bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = OID;
            query.Obj.ParentType = 3;
            return repo.Count(query) > 0;
        }

        public virtual bool HasGroup()
        {
            if (!GroupId.HasValue)
            {
                return false;
            }

            if (GroupId == 0)
            {
                return false;
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return repo.GetById<GBuildingGroup>(GroupId.Value) != null;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasMultiStorey();
            canDelete &= !HasPoi();
            canDelete &= !HasText();
            canDelete &= !HasGroup();
            return canDelete;
        }

        public void Init()
        {
            ConstructionStatus = 1;
            NavigationStatus = 0;
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GBuilding))
            {
                return false;
            }

            GBuilding building = (GBuilding)obj;
            return (OID == building.OID && TableName == building.TableName && ValueEquals(building, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public string CreateText1Value()
        {
            return Name;
        }

        public string CreateText2Value()
        {
            return Name2;
        }

        public void UpdateText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GBuildingText> texts = GetTexts().ToList();

            if (texts.Count == 0)
            {
                GBuildingText text = repo.NewObj<GBuildingText>();
                text.LinkedFeatureID = OID;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GBuildingText text2 = repo.NewObj<GBuildingText>();
                    text2.LinkedFeatureID = OID;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GBuildingText text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GBuildingText text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GBuildingText text2 = repo.NewObj<GBuildingText>();
                        text2.LinkedFeatureID = OID;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }

            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }


        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(Name2);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }

            return string.Join(" ", sb.ToArray());
        }
    }
}

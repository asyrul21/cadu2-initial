﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ShopPolygons
{
    public class GKgShopPolygon : GShopPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_Shop_Lot";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LotPolygons
{
    public class GKkLotPolygon : GLotPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KK_Lot_Boundary_Polygon";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTextsAND
{
    public class GJhLandmarkText : GLandmarkTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "JH_AND_LANDMARK_TEXT";
    }
    
}

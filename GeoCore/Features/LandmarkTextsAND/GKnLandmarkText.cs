﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTextsAND
{
    public class GKnLandmarkText : GLandmarkTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KN_AND_LANDMARK_TEXT";
    }
    
}

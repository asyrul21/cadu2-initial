﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTextsAND
{
    public class GKgLandmarkText : GLandmarkTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_AND_LANDMARK_TEXT";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkTextsAND
{
    public class GTgLandmarkText : GLandmarkTextAND
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "TG_AND_LANDMARK_TEXT";
    }
    
}

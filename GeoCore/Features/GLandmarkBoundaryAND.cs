﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features
{
    /// <summary>
    /// For AND Feature
    /// </summary>
    public abstract class GLandmarkBoundaryAND : GLandmarkBoundary
    {
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("AND-ORI_ID: [ {0} ]", OriId));

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ResidentialPolygons
{
    public class GJhResidentialPolygon : GResidentialPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JH_Residential_Area";
    }
}

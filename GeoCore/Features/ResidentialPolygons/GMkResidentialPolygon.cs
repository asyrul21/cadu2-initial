﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ResidentialPolygons
{
    public class GMkResidentialPolygon : GResidentialPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "MK_Residential_Area";
    }
}

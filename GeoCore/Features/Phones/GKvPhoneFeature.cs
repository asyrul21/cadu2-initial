﻿// this file was created by asyrul
// to enable updating of TEL_NO table
// feature is cancelled
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Geomatic.Core.Features.Phones
{
    class GKvPhoneFeature : GPhoneFeature
    {
        public static IEnumerable<string> AreaCodes = new List<string> { "003" };

        public static Regex Format = new Regex(@"^(003)\d{8}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KV_TEL_NO";

        public override IEnumerable<string> GetAreaCodes()
        {
            return AreaCodes;
        }

        public override Regex GetFormat()
        {
            return Format;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features
{
    /// <summary>
    /// For AND Feature
    /// </summary>
    public abstract class GBuildingAND : GBuilding
    { 
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override IEnumerable<GMultiStorey> GetMultiStories()
        {
            return GetMultiStories(false);
        }

        public override IEnumerable<GMultiStorey> GetMultiStories(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
            query.Obj.BuildingId = this.OriId;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public override IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OriId;
            query.Obj.ParentType = 3;
            return repo.Search(query);
        }

        public override bool HasMultiStorey()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GMultiStorey> query = new Query<GMultiStorey>(SegmentName);
            query.Obj.BuildingId = this.OriId;
            query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public override bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OriId; ;
            query.Obj.ParentType = 3;
            return repo.Count(query) > 0;
        }

        public override void CopyFrom(GBuilding building)
        {
            this.ConstructionStatus = building.ConstructionStatus;
            this.NavigationStatus = building.NavigationStatus;
            this.Code = building.Code;
            this.Name = building.Name;
            this.Name2 = building.Name2;
            this.NumberOfFloor = building.NumberOfFloor;
            this.PropertyId = building.PropertyId;
            this.Source = building.Source;
            this.ForecastNumUnit = building.ForecastNumUnit;
            this.ForecastSource = building.ForecastSource;
            this.Space = building.Space;
            this.Unit = building.Unit;
            this.AreaId = building.AreaId;
            this.Shape = building.Point;
            this.GroupId = building.GroupId;
            this.OriId = building.OID;
            this.CreatedBy = building.CreatedBy;
            this.DateCreated = building.DateCreated;
            this.UpdatedBy = building.UpdatedBy;
            this.DateUpdated = building.DateUpdated;
        }

        public virtual IEnumerable<GBuildingTextAND> GetTextsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingTextAND> query = new Query<GBuildingTextAND>(SegmentName);
            query.Obj.LinkedFeatureID = OID; // OriId;
            return (OID > 0) ? repo.Search(query) : null;
        }


        public void UpdateTextAND()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text AND.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GBuildingTextAND> texts = GetTextsAND().ToList();

            if (texts.Count == 0)
            {
                GBuildingTextAND text = repo.NewObj<GBuildingTextAND>();
                text.LinkedFeatureID = OID; // OriId;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GBuildingTextAND text2 = repo.NewObj<GBuildingTextAND>();
                    text2.LinkedFeatureID = OID;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GBuildingTextAND text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GBuildingTextAND text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GBuildingTextAND text2 = repo.NewObj<GBuildingTextAND>();
                        text2.LinkedFeatureID = OID;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }

            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(Name);
            sb.Add(Name2);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }

            return string.Join(" ", sb.ToArray());
        }
    }
}

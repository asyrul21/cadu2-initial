﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public interface IPolygonFeature : IGFeature
    {
        IPolygon Polygon { get; }
    }
}

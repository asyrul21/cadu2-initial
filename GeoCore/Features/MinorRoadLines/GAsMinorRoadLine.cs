﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.MinorRoadLines
{
    public class GAsMinorRoadLine : GMinorRoadLine
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public const string TABLE_NAME = "AS_Minor_Road_CenterLine";
    }
}

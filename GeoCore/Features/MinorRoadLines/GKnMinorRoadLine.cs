﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.MinorRoadLines
{
    public class GKnMinorRoadLine : GMinorRoadLine
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public const string TABLE_NAME = "KN_Minor_Road_CenterLine";
    }
}

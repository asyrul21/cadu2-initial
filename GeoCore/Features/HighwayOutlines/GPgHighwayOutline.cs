﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.HighwayOutlines
{
    public class GPgHighwayOutline : GHighwayOutline
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public const string TABLE_NAME = "PG_Highway_Road_OutLine";
    }
}

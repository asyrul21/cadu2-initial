﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.GpsLines
{
    public class GAsGpsLine : GGpsLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "GPS_AS";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core.Features
{
    public abstract class GRegionBoundary : GPolygonFeature
    {
        public const string NAME = "INDEX_";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GRegionBoundary))
            {
                return false;
            }

            GRegionBoundary RegionBoundary = (GRegionBoundary)obj;
            return (OID == RegionBoundary.OID && TableName == RegionBoundary.TableName && ValueEquals(RegionBoundary, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add("INDEX NAME: ");
            sb.Add(Name);

            return string.Join(" ", sb.ToArray());
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroups
{
    public class GTgBuildingGroup : GBuildingGroup
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "TG_ADM_BUILDING_GROUP";

        //added by noraini - fix post test 5

        private static List<GBuildingGroup> _allBuildingGroup;
        public static IEnumerable<GBuildingGroup> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuildingGroup> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.TG);
            if (updated)
            {
                _allBuildingGroup = repo.Search<GBuildingGroup>().ToList();
            }
            else
            {
                if (_allBuildingGroup == null)
                {
                    _allBuildingGroup = repo.Search<GBuildingGroup>().ToList();
                }
            }
            return _allBuildingGroup;
        }
        //added end
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features
{
    public interface IGPropertyPoi
    {
        IGObject GetParent();
    }
}

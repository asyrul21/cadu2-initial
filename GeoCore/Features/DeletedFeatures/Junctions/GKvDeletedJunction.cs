﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Junctions
{
    public class GKvDeletedJunction : GDeletedJunction
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public const string TABLE_NAME = "KV_DEL_ADM_JUNCTION";
    }
}

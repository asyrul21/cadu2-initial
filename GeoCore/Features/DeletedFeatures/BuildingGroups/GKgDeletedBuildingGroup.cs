﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.BuildingGroups
{
    public class GKgDeletedBuildingGroup : GDeletedBuildingGroup
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public const string TABLE_NAME = "KG_DEL_ADM_BUILDING_GROUP";
    }
}

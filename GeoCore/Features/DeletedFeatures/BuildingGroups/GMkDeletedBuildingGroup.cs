﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.BuildingGroups
{
    public class GMkDeletedBuildingGroup : GDeletedBuildingGroup
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public const string TABLE_NAME = "MK_DEL_ADM_BUILDING_GROUP";
    }
}

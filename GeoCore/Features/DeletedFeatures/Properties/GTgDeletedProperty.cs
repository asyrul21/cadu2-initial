﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Properties
{
    public class GTgDeletedProperty : GDeletedProperty
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public const string TABLE_NAME = "TG_DEL_ADM_PROPERTY";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Buildings
{
    public class GPgDeletedBuilding : GDeletedBuilding
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public const string TABLE_NAME = "PG_DEL_ADM_BUILDING";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Buildings
{
    public class GKkDeletedBuilding : GDeletedBuilding
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public const string TABLE_NAME = "KK_DEL_ADM_BUILDING";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Pois
{
    public class GKnDeletedPoi : GDeletedPoi
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public const string TABLE_NAME = "KN_DEL_ADM_POI";
    }
}

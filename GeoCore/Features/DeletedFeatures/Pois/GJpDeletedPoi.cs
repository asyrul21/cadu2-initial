﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Pois
{
    public class GJpDeletedPoi : GDeletedPoi
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public const string TABLE_NAME = "JP_DEL_ADM_POI";
    }
}

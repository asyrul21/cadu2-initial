﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core.Features.DeletedFeatures
{
    public abstract class GDeletedBuilding : GPointFeature, IGDeletedFeature, IUserCreate, IUserUpdate
    {
        public const string DEL_ID = "REF_DEL_OBJECT_ID";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";

        [MappedField(DEL_ID)]
        public virtual int? DeleteId { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }
    }
}

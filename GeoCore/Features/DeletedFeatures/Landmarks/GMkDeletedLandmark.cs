﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.DeletedFeatures.Landmarks
{
    public class GMkDeletedLandmark : GDeletedLandmark
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public const string TABLE_NAME = "MK_DEL_ADM_LANDMARK";
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.BuildingsAND
{
    public class GTgBuilding : GBuildingAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "TG_AND_BUILDING";

        private static List<GBuildingAND> _allBuilding;
        public static IEnumerable<GBuildingAND> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuildingAND> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.TG);
            if (updated)
            {
                _allBuilding = repo.Search<GBuildingAND>().ToList();
            }
            else
            {
                if (_allBuilding == null)
                {
                    _allBuilding = repo.Search<GBuildingAND>().ToList();
                }
            }
            return _allBuilding;
        }
    }
}

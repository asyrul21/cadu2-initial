﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingsAND
{
    public class GKgBuilding : GBuildingAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_AND_BUILDING";

        private static List<GBuildingAND> _allBuilding;
        public static IEnumerable<GBuildingAND> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuildingAND> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KG);
            if (updated)
            {
                _allBuilding = repo.Search<GBuildingAND>().ToList();
            }
            else
            {
                if (_allBuilding == null)
                {
                    _allBuilding = repo.Search<GBuildingAND>().ToList();
                }
            }
            return _allBuilding;
        }
    }
}

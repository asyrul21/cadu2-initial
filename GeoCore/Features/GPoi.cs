﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using System.Text.RegularExpressions;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features
{
    public abstract partial class GPoi : GPointFeature, IUserCreate, IUserUpdate, IGNavigationObject, IGNepsObject, IGBuildingPoi, IGPropertyPoi, IGLandmarkPoi
    {
        public static Regex UrlFormat = new Regex(@"^(https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public const string CODE = "POI_CODE";
        public const string NAME = "PLACE_NAME";
        public const string NAME2 = "PLACE_NAME2";
        public const string ABB = "PLC_ABB";
        public const string NAVI_NAME = "NAVI_NAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string URL = "URL";
        public const string FAMOUS = "FAMOUS_CODE";
        public const string DISPLAY_TEXT = "DISPLAY_CODE";
        public const string REF_ID = "REF_OBJECT_ID";
        public const string REF_TYPE = "REF_FEATURE_TYPE";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string UPDATE_STATUS = "STATUS";
        public const string SOURCE = "SOURCE";
        public const string FLOOR_ID = "FLOOR_ID";
        public const string FILTER_LEVEL = "FILTER_LEVEL";

        // noraini 
        public const string AND_STATUS = "AND_STATUS";

        // noraini - Aug 22 - to copy data to logging table
        public const string TEL_NO = "TEL_NO";
        public const string OTHER_TEL_NO = "OTHER_TEL_NO";
        public const string TEL_NO_OWNER = "TEL_NO_OWNER";
        public const string AREA_ID = "AREA_ID";
        public const string NAVI_ID = "NAVI_ID";
        public const string NAVI_FC = "NAVI_FC";
        public const string NAVI_MATCH = "NAVI_MATCH";
        public const string NAVI_MATCH_DATE = "NAVI_MATCH_DATE";

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(NAME2)]
        public virtual string Name2 { set; get; }

        [MappedField(ABB)]
        public virtual string Abbreviation { set; get; }

        [MappedField(NAVI_NAME)]
        public virtual string NaviName { set; get; }

        [MappedField(DESCRIPTION)]
        public virtual string Description { set; get; }

        [MappedField(URL)]
        public virtual string Url { set; get; }

        [MappedField(FAMOUS)]
        public virtual string Famous { set; get; }

        [MappedField(DISPLAY_TEXT)]
        public virtual int? DisplayText { set; get; }

        [MappedField(FILTER_LEVEL)]
        public virtual int? FilterLevel { set; get; }

        [MappedField(REF_ID)]
        public virtual int? ParentId { set; get; }

        [MappedField(REF_TYPE)]
        public virtual int? ParentType { set; get; }

        public virtual string ParentTypeValue
        {
            get
            {
                if (ParentType == (int)ParentClass.Property1 || ParentType == (int)ParentClass.Property2)
                    return "Property";
                else if (ParentType == (int)ParentClass.Building)
                    return "Building";
                else if (ParentType == (int)ParentClass.Landmark)
                    return "Landmark";
                else
                    throw new Exception("Invalid Parent Type");
            }
        }

        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return NavigationStatus == 1 ? "READY" : "NOT READY";
            }
        }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        public virtual string UpdateStatusType
        {
            get
            {
                if (UpdateStatus == 1)
                    return "ADD";
                else if (UpdateStatus == 2)
                    return "DELETE";
                else if (UpdateStatus == 3 || UpdateStatus == 4)
                    return "EDIT";
                else
                    return "-"; ;
            }
        }

        [MappedField(FLOOR_ID)]
        public virtual int? FloorId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }

        // noraini
        [MappedField(TEL_NO)]
        public virtual string TelNo { set; get; }

        [MappedField(TEL_NO_OWNER)]
        public virtual string TelNoOwner { set; get; }

        [MappedField(OTHER_TEL_NO)]
        public virtual string OtherTelNo { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(NAVI_ID)]
        public virtual int? NaviId { set; get; }

        [MappedField(NAVI_FC)]
        public virtual string NaviFV { set; get; }

        [MappedField(NAVI_MATCH)]
        public virtual string NaviMatch { set; get; }

        [MappedField(NAVI_MATCH_DATE)]
        public virtual string NaviMatchDate { set; get; }
        // end

        public virtual bool IsFamous
        {
            get
            {
                return Famous == "F";
            }
        }

        public virtual string Code1
        {
            get
            {
                return GCode.GetCode1(Code);
            }
        }

        public virtual string Code2
        {
            get
            {
                return GCode.GetCode2(Code);
            }
        }

        public virtual string Code3
        {
            get
            {
                return GCode.GetCode3(Code);
            }
        }

        public virtual GCode1 GetCode1()
        {
            return GCode1.Get(Code1);
        }

        public virtual GCode2 GetCode2()
        {
            return GCode2.Get(Code1, Code2);
        }

        public virtual GCode3 GetCode3()
        {
            return GCode3.Get(Code1, Code2, Code3);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        #region IGBuildingPoi Members

        IGObject IGBuildingPoi.GetParent()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (ParentId.HasValue) ? repo.GetById<GBuilding>(ParentId.Value) : null;
        }

        #endregion

        #region IGPropertyPoi Members

        IGObject IGPropertyPoi.GetParent()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (ParentId.HasValue) ? repo.GetById<GProperty>(ParentId.Value) : null;
        }

        #endregion

        #region IGLandmarkPoi Members

        IGObject IGLandmarkPoi.GetParent()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (ParentId.HasValue) ? repo.GetById<GLandmark>(ParentId.Value) : null;
        }

        #endregion

        public void Init()
        {
            NavigationStatus = 0;
            Source = "1";
            DisplayText = 1;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GPoi))
            {
                return false;
            }

            GPoi poi = (GPoi)obj;
            return (OID == poi.OID && TableName == poi.TableName && ValueEquals(poi, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public virtual GStreet GetStreet()
        {
            if (ParentType.HasValue)
            {
                switch (ParentType.Value)
                {
                    case 1:
                    case 2:
                        {
                            GProperty property = ((IGPropertyPoi)this).GetParent() as GProperty;
                            return (property != null) ? property.GetStreet() : null;
                        }
                    case 3:
                        {
                            GBuilding building = ((IGBuildingPoi)this).GetParent() as GBuilding;
                            return (building != null) ? building.GetStreet() : null;
                        }
                    case 4:
                        {
                            GLandmark landmark = ((IGLandmarkPoi)this).GetParent() as GLandmark;
                            return (landmark != null) ? landmark.GetStreet() : null;
                        }
                    default:
                        throw new Exception("Unknown poi parent type.");
                }
            }

            return null;
        }

        public virtual GFloor GetFloor()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (FloorId.HasValue) ? repo.GetById<GFloor>(FloorId.Value) : null;
        }

        public IEnumerable<GPoiPhone> GetPhones()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoiPhone> query = new Query<GPoiPhone>(SegmentName);
            query.Obj.RefId = OID;
            return repo.Search(query);
        }

        public virtual bool HasPhone()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoiPhone> query = new Query<GPoiPhone>(SegmentName);
            query.Obj.RefId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasPhone();
            return canDelete;
        }

        //added by asyrul
        public virtual void DiassociateAllNumbers()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GPoiPhone> phones = GetPhones().ToList();
            foreach (GPoiPhone phone in phones)
            {
                repo.Delete(phone);
            }
        }
        //added end

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(Name2);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }

            return string.Join(" ", sb.ToArray());
        }

        //added by asyrul

        private static List<GPoi> _allPoi;

        public static IEnumerable<GPoi> GetAll(bool updated, SegmentName segment)
        {
            RepositoryFactory repo = new RepositoryFactory(segment);
            if (updated)
            {
                _allPoi = repo.Search<GPoi>().ToList();
            }
            else
            {
                if (_allPoi == null)
                {
                    _allPoi = repo.Search<GPoi>().ToList();
                }
            }
            return _allPoi;
        }
        //added end

        private void CreateFeatureAsParentShape(GPoiWeb poi)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if ((ParentType == 1) || (ParentType == 2))
            {
                GProperty prop = repo.GetById<GProperty>(poi.ParentId.Value);
                this.Shape = prop.Shape;
            }
            else if (ParentType == 3)
            {
                GBuilding build = repo.GetById<GBuilding>(poi.ParentId.Value);
                this.Shape = build.Shape;
            }
            else if (ParentType == 4)
            {
                GLandmark land = repo.GetById<GLandmark>(poi.ParentId.Value);
                this.Shape = land.Shape;
            }
        }

        public virtual void CopyFromWEB(GPoiWeb poi)
        {
            this.Name = poi.Name;
            this.Name2 = poi.Name2;
            this.Code = poi.Code;
            this.Description = poi.Description;
            this.Abbreviation = poi.Abbreviation;
            this.Url = poi.Url;
            this.UpdateStatus = poi.Status;
        }

        public virtual void CopyFromWEB(GPoiWeb poi, bool addtrue)
        {
            this.ParentId = poi.ParentId.HasValue ? poi.ParentId.Value : 0;
            this.ParentType = poi.ParentType;
            this.TelNo = poi.TelNo;
            this.TelNoOwner = poi.TelNoOwner;
            //use default value
            this.FilterLevel = 1;
            this.DisplayText = 1;
            this.Source = "1"; // Internal
            // copy parent shape
            CreateFeatureAsParentShape(poi);
        }

        public virtual void CopyFromWEB(GPoiWeb poi, bool addfalse, bool deltrue)
        {
            this.UpdateStatus = poi.Status;
        }

    }
}
//
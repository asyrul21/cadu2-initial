﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ParkingLines
{
    public class GTgParkingLine : GParkingLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "TG_Parking_Lot";
    }
}

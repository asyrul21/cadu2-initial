﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features
{
    public abstract class GBuildingPolygon : GPolygonFeature
    {
        public override string CreatedBy
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string DateCreated
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string UpdatedBy
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string DateUpdated
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public void Init()
        {
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GBuildingPolygon))
            {
                return false;
            }

            GBuildingPolygon buildingPolygon = (GBuildingPolygon)obj;
            return (OID == buildingPolygon.OID && TableName == buildingPolygon.TableName && ValueEquals(buildingPolygon, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }
    }
}

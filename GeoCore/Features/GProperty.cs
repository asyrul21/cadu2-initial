﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GPhone = Geomatic.Core.Rows.GPhone;
using ESRI.ArcGIS.Geometry;
using System.Windows.Forms;
using Geomatic.Core.Oracle;

namespace Geomatic.Core.Features
{
    public abstract class GProperty : GPointFeature, IUserCreate, IUserUpdate, IGNepsObject, IGPoiParent
    {
        public const string CONS_STATUS = "CONS_STATUS";
        public const string TYPE = "PROPERTY_TYPE";
        public const string LOT = "LOT_NUMBER";
        public const string HOUSE = "HOUSE_NUMBER";
        public const string STREET_ID = "STREET_ID";
        public const string NUM_OF_FLOOR = "QUANTITY";
        public const string YEAR_INSTALL = "YEAR_INSTALL";
        public const string SOURCE = "SOURCE";
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS";
        // noraini ali
        public const string AND_STATUS = "AND_STATUS";
        // end

        [MappedField(CONS_STATUS)]
        public virtual int? ConstructionStatus { set; get; }

        public virtual string ConstructionStatusValue
        {
            get
            {
                GConstructionStatus constructionStatus = GetConstructionStatusObj();
                return (constructionStatus == null) ? string.Empty : constructionStatus.Name;
            }
        }

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        public virtual string TypeValue
        {
            get
            {
                GPropertyType propertyType = GetTypeObj();
                return StringUtils.TrimSpaces((propertyType == null) ? string.Empty : propertyType.Name);
            }
        }

        public virtual string TypeAbbreviationValue
        {
            get
            {
                GPropertyType propertyType = GetTypeObj();
                return StringUtils.TrimSpaces((propertyType == null) ? string.Empty : propertyType.Abbreviation);
            }
        }

        [MappedField(LOT)]
        public virtual string Lot { set; get; }

        [MappedField(HOUSE)]
        public virtual string House { set; get; }

        [MappedField(YEAR_INSTALL)]
        public virtual int? YearInstall { set; get; }

        [MappedField(NUM_OF_FLOOR)]
        public virtual int? NumberOfFloor { set; get; }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        public virtual GConstructionStatus GetConstructionStatusObj()
        {
            return GConstructionStatus.Get(ConstructionStatus);
        }

        public virtual GPropertyType GetTypeObj()
        {
            return GPropertyType.Get(Type);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual IEnumerable<GPropertyText> GetTexts()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyText> query = new Query<GPropertyText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GStreet GetStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreet>(StreetId.Value) : null;
        }

        public virtual IEnumerable<GPhone> GetPhones()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OID;
            query.AddClause(() => query.Obj.ParentType, "IN (1, 2)");
            return repo.Search(query);
        }

        public virtual IEnumerable<GFloor> GetFloors()
        {
            return GetFloors(false);
        }

        public virtual IEnumerable<GFloor> GetFloors(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GFloor> query = new Query<GFloor>(SegmentName);
            query.Obj.PropertyId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public virtual IEnumerable<GBuilding> GetBuildings()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GBuildingAND> GetbuildingsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return repo.Search(query);
        }

        // Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GPropertyAND> GetPropertyAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyAND> query = new Query<GPropertyAND>(SegmentName);
            query.Obj.OriId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GPropertyAND GetPropertyANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GPropertyAND> propertyAND = GetPropertyAND().ToList();
            return propertyAND.Count() > 0 ? propertyAND.First() : null;
        }

        public virtual IEnumerable<GPropertyAND> GetPropertyAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyAND> query = new Query<GPropertyAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GPropertyAND GetPropertyANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GPropertyAND> propertyAND = GetPropertyAND(includeDeleted).ToList();
            return propertyAND.Count() > 0 ? propertyAND.First() : null;
        }

        public virtual void CopyFrom(GProperty property)
        {
            this.ConstructionStatus = property.ConstructionStatus;
            this.House = property.House;
            this.Lot = property.Lot;
            this.Source = property.Source;
            this.YearInstall = property.YearInstall;
            this.Type = property.Type;
            this.NumberOfFloor = property.NumberOfFloor;
            this.Type = property.Type;
            this.StreetId = property.StreetId;
        }
        // end add

        public virtual bool CanHasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GPropertyPermission> query = new Query<GPropertyPermission>();
            query.Obj.Code = Type;
            return repo.Count<GPropertyPermission>(query) == 0;
        }

        public virtual bool HasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasBuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.PropertyId = this.OID;
            query.AddClause(() => query.Obj.AndStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public virtual bool HasPoi()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OID;
            query.AddClause(() => query.Obj.ParentType, "IN (1, 2)");
            return repo.Count(query) > 0;
        }

        public virtual bool HasPhone()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasFloor()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GFloor> query = new Query<GFloor>(SegmentName);
            query.Obj.PropertyId = this.OID;
            query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public virtual bool HasText()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyText> query = new Query<GPropertyText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return repo.Count(query) > 0;
        }

        // Noraini Ali - OKT 2020
        public virtual IEnumerable<GBuilding> Getbuildings()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GBuilding Getbuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuilding> building = Getbuildings().ToList();
            return (building.Count() > 0) ? building.First() : null;
        }

        public virtual IEnumerable<GBuildingAND> GetbuildingsAnd()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.PropertyId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GBuildingAND GetbuildingAnd()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingAND> building = GetbuildingsAnd().ToList();
            return (building.Count() > 0) ? building.First() : null;
        }
        // end add

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasFloor();
            canDelete &= !HasPoi();
            canDelete &= !HasPhone();
            canDelete &= !HasBuilding();
            canDelete &= !HasText();
            return canDelete;
        }

        public void Init()
        {
            ConstructionStatus = 1;
            Source = "1";
            YearInstall = DateTime.Today.Year;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GProperty))
            {
                return false;
            }

            GProperty property = (GProperty)obj;
            return (OID == property.OID && TableName == property.TableName && ValueEquals(property, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public string CreateText1Value()
        {
            List<string> sb = new List<string>();
            sb.Add(Lot);
            sb.Add(House);
            return string.Join(" ", sb.ToArray());
        }

        public string CreateText2Value()
        {
            GPropertyType propertyType = GetTypeObj();
            return (propertyType == null) ? string.Empty : StringUtils.TrimSpaces(propertyType.Abbreviation);
        }

        public void UpdateText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GPropertyText> texts = GetTexts().ToList();

            if (texts.Count == 0)
            {
                GPropertyText text = repo.NewObj<GPropertyText>();
                text.LinkedFeatureID = OID;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GPropertyText text2 = repo.NewObj<GPropertyText>();
                    text2.LinkedFeatureID = OID;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GPropertyText text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GPropertyText text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GPropertyText text2 = repo.NewObj<GPropertyText>();
                        text2.LinkedFeatureID = OID;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }
            
            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }

        //added by asyrul
        public void ungeocodeAllPhones()
        {
            TelNoUngeocoder ung = new TelNoUngeocoder(SegmentName);
            IEnumerable<GPhone> phones = this.GetPhones();

            foreach (GPhone phone in phones)
            {
                //MessageBox.Show(ung.selectTelNo(phone));
                ung.ungeocodeTelNo(phone);
            }
        }
        //added end

        //added by syafiq
        public void transferAllPhones(GProperty destinationProp)
        {
            TelNoUngeocoder ung = new TelNoUngeocoder(SegmentName);
            IEnumerable<GPhone> phones = this.GetPhones();

            foreach (GPhone phone in phones)
            {
                ung.transferTelNo(phone, destinationProp);
            }
        }
        //added end

        // Added by Noraini Ali - Aug 2020 - Get phone No
        public virtual IEnumerable<GPoiPhone> GetPhoneNo()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoiPhone> query = new Query<GPoiPhone>();
            query.Obj.RefId = OID;
            return repo.Search(query);
        }
        // end added

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Lot);
            sb.Add(House);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }
    }
}

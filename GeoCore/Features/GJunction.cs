﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using GJunctionType = Geomatic.Core.Rows.GJunctionType;

namespace Geomatic.Core.Features
{
    public abstract class GJunction : GPointFeature, IUserCreate, IUserUpdate, IGNepsObject
    {
        public const string TYPE = "JUNCTION_TYPE";
        public const string NAME = "JUNCTION_NAME";
        public const string SOURCE = "SOURCE";
        public const string UPDATE_STATUS = "STATUS";
        public const string AREA_ID = "AREA_ID";
        //public const string STREET_ID = "STREET_ID";
        // noraini ali
        public const string AND_STATUS = "AND_STATUS";
        // end

        [MappedField(TYPE)]
        public virtual int? Type { set; get; }

        public virtual string TypeValue
        {
            get
            {
                GJunctionType junctionType = GetTypeObj();
                return StringUtils.TrimSpaces((junctionType == null) ? string.Empty : junctionType.Name);
            }
        }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        //added by asyrul
        //[MappedField(STREET_ID)]
        //public virtual int? StreetId { set; get; }
        //added end

        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        public virtual GJunctionType GetTypeObj()
        {
            return GJunctionType.Get(Type);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual bool HasStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OID;
            query.Obj.ToNodeId = this.OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasMoreThanOneStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OID;
            query.Obj.ToNodeId = this.OID;
            return repo.Count(query) > 1;
        }

        public virtual bool AllStreetMustStatus2()
        {
            int count = 0;
            IEnumerable<GStreet> streets = GetStreets();
            foreach (GStreet street in streets)
            {
                if(street.AndStatus != 2)
                {
                    count = count + 1;
                }
            }
            return count < 1;
        }

        public virtual IEnumerable<GStreet> GetStreets()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OID;
            query.Obj.ToNodeId = this.OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GStreetAND> GetStreetsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.Operator = Operator.OR;
            query.Obj.FromNodeId = this.OID;
            query.Obj.ToNodeId = this.OID;
            return repo.Search(query);
        }

        // added by asyrul
        public virtual string getStreetId()
        {
            GStreet street = this.GetStreets().First();
            Console.WriteLine("Street: " + street.OID.ToString());
            return street.OID.ToString();
        }
        // added end

        public virtual bool HasStreetRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Obj.JunctionId = this.OID;
            return repo.Count(query) > 0;
        }

        public virtual IEnumerable<GStreetRestriction> GetStreetRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.Obj.JunctionId = this.OID;
            return repo.Search(query);
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasStreet();
            canDelete &= !HasStreetRestriction();
            canDelete &= !HasTollRoute();
            return canDelete;
        }

        public virtual bool CanDeleteJunction()
        {
            bool canDelete = true;
            canDelete &= !HasMoreThanOneStreet();
            canDelete &= !HasStreetRestriction();
            canDelete &= !HasTollRoute();
            return canDelete;
        }

        public virtual bool CanDeleteUnionJunction()
        {
            bool canDelete = true;
            canDelete &= AllStreetMustStatus2();
            canDelete &= !HasStreetRestriction();
            canDelete &= !HasTollRoute();
            return canDelete;
        }

        public void Init()
        {
            Type = 1;
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GJunction))
            {
                return false;
            }

            GJunction junction = (GJunction)obj;
            return (OID == junction.OID && TableName == junction.TableName && ValueEquals(junction, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();
            // added by asyrul
            IEnumerable<GStreet> streetids = GetStreets();
            string streetID = streetids.First().OID.ToString();
            // added end

            sb.Add(string.Format("ID: [ {0} ]", OID));
            // added by asyrul
            sb.Add(string.Format("Street ID: [ {0} ]", streetID));
            // added end
            sb.Add(Name);

            return string.Join(" ", sb.ToArray());
        }

        public virtual bool HasInTollRoute()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.InSegment = SegmentName.ToString();
            query.Obj.InID = this.OID;

            return repo.Count(query) > 0;
        }

        public virtual bool HasOutTollRoute()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.OutSegment = SegmentName.ToString();
            query.Obj.OutID = this.OID;

            return repo.Count(query) > 0;
        }

        public virtual bool HasTollRoute()
        {
            bool hasTollRoute;
            hasTollRoute = HasInTollRoute();
            hasTollRoute |= HasOutTollRoute();

            return hasTollRoute;
        }

        public virtual IEnumerable<GTollRoute> GetTollRoutes()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GTollRoute> query = new Query<GTollRoute>(SegmentName);
            query.Obj.InSegment = SegmentName.ToString();
            query.Obj.InID = this.OID;
            IEnumerable<GTollRoute> tollRoutesIn = repo.Search(query);

            query = new Query<GTollRoute>(SegmentName);
            query.Obj.OutSegment = SegmentName.ToString();
            query.Obj.OutID = this.OID;
            IEnumerable<GTollRoute> tollRoutesOut = repo.Search(query);

            return tollRoutesIn.Concat(tollRoutesOut);
        }

        // Noraini Ali - CADU 2 AND - module verification AND Features
        public virtual IEnumerable<GJunctionAND> GetJunctionAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GJunctionAND> query = new Query<GJunctionAND>(SegmentName);
            query.Obj.OriId = this.OID;
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GJunctionAND GetJunctionANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GJunctionAND> junctionAND = GetJunctionAND().ToList();
            return junctionAND.Count() > 0 ? junctionAND.First() : null;
        }
        // end add
        public virtual IEnumerable<GJunctionAND> GetJunctionAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GJunctionAND> query = new Query<GJunctionAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }
        public virtual GJunctionAND GetJunctionANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GJunctionAND> junctionAND = GetJunctionAND(includeDeleted).ToList();
            return junctionAND.Count() > 0 ? junctionAND.First() : null;
        }

        public virtual void CopyFrom(GJunction junction)
        {
            this.Type = junction.Type;
            this.Name = junction.Name;
            this.Source = junction.Source;
        }
    }
}

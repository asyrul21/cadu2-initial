﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public interface ILineFeature : IGFeature
    {
        IPolyline Polyline { get; }
    }
}

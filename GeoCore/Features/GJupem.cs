﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features
{
    public abstract class GJupem : GPolygonFeature
    {
        public const string SEGMENT_NAME = "SEGMENT_NAME";
        public const string EXC_ABB = "EXC_ABB";
        public const string LOT_NUM = "LOT_NUM";
        public const string BND_TYPE = "BND_TYPE";
        public const string BND_NAME = "BND_NAME";
        public const string STATE_CODE = "STATE_CODE";

        [MappedField(SEGMENT_NAME)]
        public virtual string segment_name { set; get; }

        [MappedField(EXC_ABB)]
        public virtual string exc_abb { set; get; }

        [MappedField(LOT_NUM)]
        public virtual string lot_num { set; get; }

        [MappedField(BND_TYPE)]
        public virtual string bnd_type { set; get; }

        [MappedField(BND_NAME)]
        public virtual string bnd_name { set; get; }

        [MappedField(STATE_CODE)]
        public virtual string state_code { set; get; }

        #region function not to Implement
        public override string CreatedBy
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string DateCreated
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string UpdatedBy
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override string DateUpdated
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        #endregion

        public override bool Equals(object obj)
        {
            if (!(obj is GJupem))
            {
                return false;
            }

            GJupem jupem = (GJupem)obj;
            return (OID == jupem.OID && TableName == jupem.TableName && ValueEquals(jupem, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID : [ {0} ]", OID));
            sb.Add(",");
            sb.Add(segment_name);
            sb.Add(",");
            sb.Add(exc_abb);
            sb.Add(",");
            sb.Add(lot_num);
            sb.Add(",");
            sb.Add(bnd_type);
            sb.Add(",");
            sb.Add(bnd_name);
            sb.Add(",");
            sb.Add(state_code);

            return string.Join(" ", sb.ToArray());
        }
    }
}

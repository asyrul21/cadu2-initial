﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;



namespace Geomatic.Core.Features
{
    public abstract class GRegion : GPolygonFeature
    {
        public const string INDEX = "INDEX_";

        [MappedField(INDEX)]
        public virtual string index { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GRegion))
            {
                return false;
            }

            GRegion region = (GRegion)obj;
            return (OID == region.OID && TableName == region.TableName && ValueEquals(region, true));
        }


        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID : [ {0} ]", OID));
            sb.Add(",");
            sb.Add("INDEX : ");
            sb.Add(index);

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Rows;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    public abstract class GFloor : Geomatic.Core.Rows.GFloor  //, IPointFeature
    {
        public new abstract string Number { set; get; }

        //asyrul
        public new abstract string NumUnit { set; get; }

        public new abstract int? PropertyId { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract int? AndStatus { set; get; }

        public abstract int? PropertyType { set; get; }

        [MappedField(GPropertyType.TABLE_NAME + "." + GPropertyType.NAME)]
        public virtual string PropertyTypeValue { set; get; }

        public abstract int? StreetId { set; get; }

        public abstract int? StreetType { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public virtual string StreetTypeValue { set; get; }

        public abstract string StreetName { set; get; }

        public abstract string StreetName2 { set; get; }

        public abstract string Section { set; get; }

        public abstract string Postcode { set; get; }

        public abstract string City { set; get; }

        public abstract string State { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        public IPoint Point
        {
            get
            {
                return Shape as IPoint;
            }
        }

        public double X
        {
            get
            {
                return Point.X;
            }
        }

        public double Y
        {
            get
            {
                return Point.Y;
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Number);
            sb.Add(",");
            sb.Add(StringUtils.TrimSpaces(StreetTypeValue));
            sb.Add(StreetName);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core.Features.JoinedFeatures
{
    public abstract class GWorkArea : Geomatic.Core.Features.GWorkArea
    {
        public override abstract string Wo_no { set; get; }
        public override abstract string Wo_desc { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(",");
            sb.Add("WO NO: ");
            sb.Add(Wo_no);
            sb.Add("WO_DESC");
            sb.Add(Wo_desc);

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GStreetType = Geomatic.Core.Rows.GStreetType;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GJunctionAND : Geomatic.Core.Features.GJunctionAND
    {
        public new abstract int? Type { set; get; }

        [MappedField(GJunctionType.TABLE_NAME + "." + GJunctionType.NAME)]
        public new virtual string TypeValue { set; get; }

        public new abstract string Name { set; get; }

        public new abstract string Source { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract int? AreaId { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        public new abstract int? OriId { set; get; }

        public new abstract int? AndStatus { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(",");
            sb.Add(Name);

            sb.Add(",");
            sb.Add(AreaId.ToString());
            sb.Add(",");
            sb.Add("[");
            sb.Add(UpdateStatus.ToString());
            sb.Add(",");
            sb.Add(AndStatus.ToString());
            sb.Add("]");

            return string.Join(" ", sb.ToArray());
        }   
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GTgStreet = Geomatic.Core.Features.Streets.GTgStreet;
using GTgProperty = Geomatic.Core.Features.Properties.GTgProperty;

namespace Geomatic.Core.Features.JoinedFeatures.Floors
{
    public class GTgFloor : GFloor
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.Floors.GTgFloor.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + NUMBER)]
        public override string Number { set; get; }

        // asyrul
        [MappedField(TABLE_NAME + "." + UNIT_NUM)]
        public override string NumUnit { set; get; }
        // asyrul end

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GTgProperty.TABLE_NAME + "." + GTgProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GTgProperty.TABLE_NAME + "." + GTgProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }
    }
}

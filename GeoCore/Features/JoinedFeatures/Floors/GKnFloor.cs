﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKnStreet = Geomatic.Core.Features.Streets.GKnStreet;
using GKnProperty = Geomatic.Core.Features.Properties.GKnProperty;

namespace Geomatic.Core.Features.JoinedFeatures.Floors
{
    public class GKnFloor : GFloor
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.Floors.GKnFloor.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + NUMBER)]
        public override string Number { set; get; }

        // asyrul
        [MappedField(TABLE_NAME + "." + UNIT_NUM)]
        public override string NumUnit { set; get; }
        // asyrul end

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }
    }
}

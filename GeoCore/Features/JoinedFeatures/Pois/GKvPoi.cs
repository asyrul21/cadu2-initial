﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKvStreet = Geomatic.Core.Features.Streets.GKvStreet;

namespace Geomatic.Core.Features.JoinedFeatures.Pois
{
    public class GKvPoi : GPoi
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = Geomatic.Core.Features.Pois.GKvPoi.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + ABB)]
        public override string Abbreviation { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_NAME)]
        public override string NaviName { set; get; }

        [MappedField(TABLE_NAME + "." + DESCRIPTION)]
        public override string Description { set; get; }

        [MappedField(TABLE_NAME + "." + URL)]
        public override string Url { set; get; }

        [MappedField(TABLE_NAME + "." + FAMOUS)]
        public override string Famous { set; get; }

        [MappedField(TABLE_NAME + "." + DISPLAY_TEXT)]
        public override int? DisplayText { set; get; }

        [MappedField(TABLE_NAME + "." + FILTER_LEVEL)]
        public override int? FilterLevel { set; get; }

        [MappedField(TABLE_NAME + "." + REF_ID)]
        public override int? ParentId { set; get; }

        [MappedField(TABLE_NAME + "." + REF_TYPE)]
        public override int? ParentType { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(TABLE_NAME + "." + FLOOR_ID)]
        public override int? FloorId { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + TEL_NO)]
        public override int? TelNo { set; get; }

        [MappedField(TABLE_NAME + "." + TEL_NO_OWNER)]
        public override string TelNoOwner { set; get; }

        [MappedField(TABLE_NAME + "." + OTHER_TEL_NO)]
        public override int? OtherTelNo { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_ID)]
        public override int? NaviId { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_FC)]
        public override string NaviFV { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_MATCH)]
        public override string NaviMatch { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_MATCH_DATE)]
        public override string NaviMatchDate { set; get; }
    }
}

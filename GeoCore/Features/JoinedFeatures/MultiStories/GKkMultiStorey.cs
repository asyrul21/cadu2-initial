﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKkStreet = Geomatic.Core.Features.Streets.GKkStreet;
using GKkProperty = Geomatic.Core.Features.Properties.GKkProperty;
using GKkBuilding = Geomatic.Core.Features.Buildings.GKkBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GKkMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GKkMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GKkBuilding.TABLE_NAME + "." + GKkBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GKkBuilding.TABLE_NAME + "." + GKkBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GKkProperty.TABLE_NAME + "." + GKkProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKkProperty.TABLE_NAME + "." + GKkProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKkStreet.TABLE_NAME + "." + GKkStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GTgStreet = Geomatic.Core.Features.Streets.GTgStreet;
using GTgProperty = Geomatic.Core.Features.Properties.GTgProperty;
using GTgBuilding = Geomatic.Core.Features.Buildings.GTgBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GTgMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GTgMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GTgBuilding.TABLE_NAME + "." + GTgBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GTgBuilding.TABLE_NAME + "." + GTgBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GTgProperty.TABLE_NAME + "." + GTgProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GTgProperty.TABLE_NAME + "." + GTgProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GTgStreet.TABLE_NAME + "." + GTgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GMkStreet = Geomatic.Core.Features.Streets.GMkStreet;
using GMkProperty = Geomatic.Core.Features.Properties.GMkProperty;
using GMkBuilding = Geomatic.Core.Features.Buildings.GMkBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GMkMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GMkMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GMkBuilding.TABLE_NAME + "." + GMkBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GMkBuilding.TABLE_NAME + "." + GMkBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GMkProperty.TABLE_NAME + "." + GMkProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GMkProperty.TABLE_NAME + "." + GMkProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GMkStreet.TABLE_NAME + "." + GMkStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

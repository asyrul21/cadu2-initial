﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKgStreet = Geomatic.Core.Features.Streets.GKgStreet;
using GKgProperty = Geomatic.Core.Features.Properties.GKgProperty;
using GKgBuilding = Geomatic.Core.Features.Buildings.GKgBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GKgMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GKgMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GKgBuilding.TABLE_NAME + "." + GKgBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GKgBuilding.TABLE_NAME + "." + GKgBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GKgProperty.TABLE_NAME + "." + GKgProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKgProperty.TABLE_NAME + "." + GKgProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

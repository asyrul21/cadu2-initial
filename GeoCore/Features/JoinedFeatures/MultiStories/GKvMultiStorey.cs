﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKvStreet = Geomatic.Core.Features.Streets.GKvStreet;
using GKvProperty = Geomatic.Core.Features.Properties.GKvProperty;
using GKvBuilding = Geomatic.Core.Features.Buildings.GKvBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GKvMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GKvMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GKvBuilding.TABLE_NAME + "." + GKvBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GKvBuilding.TABLE_NAME + "." + GKvBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GKvProperty.TABLE_NAME + "." + GKvProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKvProperty.TABLE_NAME + "." + GKvProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKvStreet.TABLE_NAME + "." + GKvStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

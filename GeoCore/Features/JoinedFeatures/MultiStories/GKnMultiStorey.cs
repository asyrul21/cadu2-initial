﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GKnStreet = Geomatic.Core.Features.Streets.GKnStreet;
using GKnProperty = Geomatic.Core.Features.Properties.GKnProperty;
using GKnBuilding = Geomatic.Core.Features.Buildings.GKnBuilding;

namespace Geomatic.Core.Features.JoinedFeatures.MultiStories
{
    public class GKnMultiStorey : GMultiStorey
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Rows.MultiStories.GKnMultiStorey.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + FLOOR)]
        public override string Floor { set; get; }

        [MappedField(TABLE_NAME + "." + APARTMENT)]
        public override string Apartment { set; get; }

        [MappedField(TABLE_NAME + "." + BUILDING_ID)]
        public override int? BuildingId { set; get; }

        [MappedField(TABLE_NAME + "." + GDS_ID)]
        public override int? GdsId { set; get; }

        [MappedField(TABLE_NAME + "." + NIS_ID)]
        public override int? NisId { set; get; }

        [MappedField(TABLE_NAME + "." + GR_ID)]
        public override int? GrId { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(GKnBuilding.TABLE_NAME + "." + GKnBuilding.NAME)]
        public override string BuildingName { set; get; }

        [MappedField(GKnBuilding.TABLE_NAME + "." + GKnBuilding.NAME2)]
        public override string BuildingName2 { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

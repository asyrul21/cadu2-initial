﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GStreetType = Geomatic.Core.Rows.GStreetType;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GPoi : Geomatic.Core.Features.GPoi
    {
        public new abstract string Code { set; get; }

        public new abstract string Name { set; get; }

        public new abstract string Name2 { set; get; }

        public new abstract string Abbreviation { set; get; }

        public new abstract string NaviName { set; get; }

        public new abstract string Description { set; get; }

        public new abstract string Url { set; get; }

        public new abstract string Famous { set; get; }

        public new abstract int? DisplayText { set; get; }

        public new abstract int? FilterLevel { set; get; }

        public new abstract int? ParentId { set; get; }

        public new abstract int? ParentType { set; get; }

        public new virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        public new abstract int? NavigationStatus { set; get; }

        public new abstract string Source { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract int? AndStatus { set; get; }

        public new abstract int? FloorId { set; get; }

        public abstract int? StreetType { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public virtual string StreetTypeValue { set; get; }

        public abstract string StreetName { set; get; }

        public abstract string StreetName2 { set; get; }

        public abstract string Section { set; get; }

        public abstract string Postcode { set; get; }

        public abstract string City { set; get; }

        public abstract string State { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        // noraini
        public new abstract int? TelNo { set; get; }
        public new abstract string TelNoOwner { set; get; }
        public new abstract int? OtherTelNo { set; get; }
        public new abstract int? AreaId { set; get; }
        public new abstract int? NaviId { set; get; }
        public new abstract string NaviFV { set; get; }
        public new abstract string NaviMatch { set; get; }
        public new abstract string NaviMatchDate { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(Name2);
            sb.Add(",");
            sb.Add(StringUtils.TrimSpaces(StreetTypeValue));
            sb.Add(StreetName);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

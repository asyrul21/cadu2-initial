﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GStreetType = Geomatic.Core.Rows.GStreetType;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GProperty : Geomatic.Core.Features.GProperty
    {
        public new abstract int? ConstructionStatus { set; get; }

        [MappedField(GConstructionStatus.TABLE_NAME + "." + GConstructionStatus.NAME)]
        public new virtual string ConstructionStatusValue { set; get; }

        public new abstract int? Type { set; get; }

        [MappedField(GPropertyType.TABLE_NAME + "." + GPropertyType.NAME)]
        public new virtual string TypeValue { set; get; }

        public new abstract string Lot { set; get; }

        public new abstract string House { set; get; }

        public new abstract int? YearInstall { set; get; }

        public new abstract int? NumberOfFloor { set; get; }

        public new abstract int? StreetId { set; get; }

        public abstract int? StreetType { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public virtual string StreetTypeValue { set; get; }

        public abstract string StreetName { set; get; }

        public abstract string StreetName2 { set; get; }

        public abstract string Section { set; get; }

        public abstract string Postcode { set; get; }

        public abstract string City { set; get; }

        public abstract string State { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract int? AreaId { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        // noraini ali
        public new abstract int? AndStatus { set; get; }
        // end

        public new abstract Geomatic.Core.Features.GStreet GetStreet();

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Lot);
            sb.Add(House);
            sb.Add(",");
            sb.Add(StringUtils.TrimSpaces(StreetTypeValue));
            sb.Add(StreetName);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);
            sb.Add(",");

            sb.Add(AreaId.ToString());
            sb.Add(",");
            sb.Add("[");
            sb.Add(UpdateStatus.ToString());
            sb.Add(",");
            sb.Add(AndStatus.ToString());
            sb.Add("]");

            return string.Join(" ", sb.ToArray());
        }
    }
}

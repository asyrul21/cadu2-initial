﻿// not used, can be deleted
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;

namespace Geomatic.Core.Features.JoinedFeatures
{
    public abstract class GRegion : Geomatic.Core.Features.GRegion
    {
        public new abstract string index { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(",");
            sb.Add("INDEX: ");
            sb.Add(index);

            return string.Join(" ", sb.ToArray());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GAsStreet = Geomatic.Core.Features.Streets.GAsStreet;

namespace Geomatic.Core.Features.JoinedFeatures.BuildingGroupsAND
{
    public class GAsBuildingGroup : GBuildingGroupAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.BuildingGroupsAND.GAsBuildingGroup.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + ORI_ID)]
        public override int? OriId { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + BUILD_NAME_POS)]
        public override int? BuldingNamePos { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_UNIT)]
        public override int? NumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override string ForecastSource { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GAsStreet = Geomatic.Core.Features.Streets.GAsStreet;
using GAsProperty = Geomatic.Core.Features.Properties.GAsProperty;
using GAsBuildingGroup = Geomatic.Core.Features.BuildingGroups.GAsBuildingGroup;

namespace Geomatic.Core.Features.JoinedFeatures.BuildingsAND
{
    public class GAsBuilding : GBuildingAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.BuildingsAND.GAsBuilding.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + GROUP_ID)]
        public override int? GroupId { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_FLOOR)]
        public override int? NumberOfFloor { set; get; }

        [MappedField(TABLE_NAME + "." + UNIT)]
        public override int? Unit { set; get; }

        [MappedField(TABLE_NAME + "." + SPACE)]
        public override int? Space { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override int? ForecastSource { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(GAsProperty.TABLE_NAME + "." + GAsProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GAsProperty.TABLE_NAME + "." + GAsProperty.LOT)]
        public override string Lot { set; get; }

        [MappedField(GAsProperty.TABLE_NAME + "." + GAsProperty.HOUSE)]
        public override string House { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GAsStreet.TABLE_NAME + "." + GAsStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + ORI_ID)]
        public override int? OriId { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        // noraini ali - OKT 2021
        [MappedField(GAsBuildingGroup.TABLE_NAME + "." + GAsBuildingGroup.NAME)]
        public override string BuildGrpName { set; get; }
        // end

        public override Geomatic.Core.Features.GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<Geomatic.Core.Features.GProperty>(PropertyId.Value) : null;
        }
    }
}

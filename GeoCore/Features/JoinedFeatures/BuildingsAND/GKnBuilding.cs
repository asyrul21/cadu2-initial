﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GKnStreet = Geomatic.Core.Features.Streets.GKnStreet;
using GKnProperty = Geomatic.Core.Features.Properties.GKnProperty;
using GKnBuildingGroup = Geomatic.Core.Features.BuildingGroups.GKnBuildingGroup;

namespace Geomatic.Core.Features.JoinedFeatures.BuildingsAND
{
    public class GKnBuilding : GBuildingAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.BuildingsAND.GKnBuilding.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + GROUP_ID)]
        public override int? GroupId { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_FLOOR)]
        public override int? NumberOfFloor { set; get; }

        [MappedField(TABLE_NAME + "." + UNIT)]
        public override int? Unit { set; get; }

        [MappedField(TABLE_NAME + "." + SPACE)]
        public override int? Space { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override int? ForecastSource { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.LOT)]
        public override string Lot { set; get; }

        [MappedField(GKnProperty.TABLE_NAME + "." + GKnProperty.HOUSE)]
        public override string House { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKnStreet.TABLE_NAME + "." + GKnStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + ORI_ID)]
        public override int? OriId { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        // noraini ali - OKT 2021
        [MappedField(GKnBuildingGroup.TABLE_NAME + "." + GKnBuildingGroup.NAME)]
        public override string BuildGrpName { set; get; }
        // end

        public override Geomatic.Core.Features.GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<Geomatic.Core.Features.GProperty>(PropertyId.Value) : null;
        }
    }
}

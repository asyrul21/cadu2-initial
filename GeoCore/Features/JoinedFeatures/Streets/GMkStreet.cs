﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features.JoinedFeatures.Streets
{
    public class GMkStreet : GStreet
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.Streets.GMkStreet.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + STATUS)]
        public override int? Status { set; get; }

        [MappedField(TABLE_NAME + "." + NETWORK_CLASS)]
        public override int? NetworkClass { set; get; }

        [MappedField(TABLE_NAME + "." + CLASS)]
        public override int? Class { set; get; }

        [MappedField(TABLE_NAME + "." + CATEGORY)]
        public override int? Category { set; get; }

        [MappedField(TABLE_NAME + "." + FILTER_LEVEL)]
        public override int? FilterLevel { set; get; }

        [MappedField(TABLE_NAME + "." + DESIGN)]
        public override int? Design { set; get; }

        [MappedField(TABLE_NAME + "." + DIRECTION)]
        public override int? Direction { set; get; }

        [MappedField(TABLE_NAME + "." + TOLL_TYPE)]
        public override int? TollType { set; get; }

        [MappedField(TABLE_NAME + "." + DIVIDER)]
        public override int? Divider { set; get; }

        [MappedField(TABLE_NAME + "." + TYPE)]
        public override int? Type { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + SECTION)]
        public override string Section { set; get; }

        [MappedField(TABLE_NAME + "." + POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(TABLE_NAME + "." + CITY)]
        public override string City { set; get; }

        [MappedField(TABLE_NAME + "." + SUB_CITY)]
        public override string SubCity { set; get; }

        [MappedField(TABLE_NAME + "." + STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + FROM_ID)]
        public override int? FromNodeId { set; get; }

        [MappedField(TABLE_NAME + "." + TO_ID)]
        public override int? ToNodeId { set; get; }

        [MappedField(TABLE_NAME + "." + SPEED_LIMIT)]
        public override int? SpeedLimit { set; get; }

        [MappedField(TABLE_NAME + "." + WEIGHT)]
        public override double? Weight { set; get; }

        [MappedField(TABLE_NAME + "." + ELEVATION)]
        public override int? Elevation { set; get; }

        [MappedField(TABLE_NAME + "." + LENGTH)]
        public override double? Length { set; get; }

        [MappedField(TABLE_NAME + "." + WIDTH)]
        public override double? Width { set; get; }

        [MappedField(TABLE_NAME + "." + HEIGHT)]
        public override double? Height { set; get; }

        [MappedField(TABLE_NAME + "." + LANES)]
        public override int? NumOfLanes { set; get; }

        [MappedField(TABLE_NAME + "." + BICYCLE)]
        public override int? Bicycle { set; get; }

        [MappedField(TABLE_NAME + "." + BUS)]
        public override int? Bus { set; get; }

        [MappedField(TABLE_NAME + "." + CAR)]
        public override int? Car { set; get; }

        [MappedField(TABLE_NAME + "." + DELIVERY)]
        public override int? Delivery { set; get; }

        [MappedField(TABLE_NAME + "." + EMERGENCY)]
        public override int? Emergency { set; get; }

        [MappedField(TABLE_NAME + "." + TRUCK)]
        public override int? Truck { set; get; }

        [MappedField(TABLE_NAME + "." + PEDESTRIAN)]
        public override int? Pedestrian { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }
        // end
    }
}

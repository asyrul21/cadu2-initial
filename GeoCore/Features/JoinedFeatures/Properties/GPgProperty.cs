﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GPgStreet = Geomatic.Core.Features.Streets.GPgStreet;

namespace Geomatic.Core.Features.JoinedFeatures.Properties
{
    public class GPgProperty : GProperty
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = Geomatic.Core.Features.Properties.GPgProperty.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + TYPE)]
        public override int? Type { set; get; }

        [MappedField(TABLE_NAME + "." + LOT)]
        public override string Lot { set; get; }

        [MappedField(TABLE_NAME + "." + HOUSE)]
        public override string House { set; get; }

        [MappedField(TABLE_NAME + "." + YEAR_INSTALL)]
        public override int? YearInstall { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_OF_FLOOR)]
        public override int? NumberOfFloor { set; get; }

        [MappedField(TABLE_NAME + "." + STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        // noraini ali
        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }
        // end

        public override Geomatic.Core.Features.GStreet GetStreet()
        {
            RepositoryFactory repository = new RepositoryFactory(SegmentName);
            return repository.GetById<Geomatic.Core.Features.GStreet>(StreetId.Value);
        }
    }
}

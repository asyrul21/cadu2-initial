﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GStreetStatus = Geomatic.Core.Rows.GStreetStatus;
using GStreetType = Geomatic.Core.Rows.GStreetType;
using GStreetNetworkClass = Geomatic.Core.Rows.GStreetNetworkClass;
using GStreetClass = Geomatic.Core.Rows.GStreetClass;
using GStreetCategory = Geomatic.Core.Rows.GStreetCategory;
using GStreetFilterLevel = Geomatic.Core.Rows.GStreetFilterLevel;
using GStreetDirection = Geomatic.Core.Rows.GStreetDirection;
using GStreetDesign = Geomatic.Core.Rows.GStreetDesign;
using GStreetTollType = Geomatic.Core.Rows.GStreetTollType;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GStreet : Geomatic.Core.Features.GStreet
    {
        public new abstract int? ConstructionStatus { set; get; }

        [MappedField(GConstructionStatus.TABLE_NAME + "." + GConstructionStatus.NAME)]
        public new virtual string ConstructionStatusValue { set; get; }

        public new virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        public new abstract int? NavigationStatus { set; get; }

        public new virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        public new abstract int? Status { set; get; }

        [MappedField(GStreetStatus.TABLE_NAME + "." + GStreetStatus.NAME)]
        public new virtual string StatusValue { set; get; }

        public new abstract int? NetworkClass { set; get; }

        [MappedField(GStreetNetworkClass.TABLE_NAME + "." + GStreetNetworkClass.NAME)]
        public new virtual string NetworkClassValue { set; get; }

        public new abstract int? Class { set; get; }

        [MappedField(GStreetClass.TABLE_NAME + "." + GStreetClass.NAME)]
        public new virtual string ClassValue { set; get; }

        public new abstract int? Category { set; get; }

        [MappedField(GStreetCategory.TABLE_NAME + "." + GStreetCategory.NAME)]
        public new virtual string CategoryValue { set; get; }

        public new abstract int? FilterLevel { set; get; }

        [MappedField(GStreetFilterLevel.TABLE_NAME + "." + GStreetFilterLevel.NAME)]
        public new virtual string FilterLevelValue { set; get; }

        public new abstract int? Design { set; get; }

        [MappedField(GStreetDesign.TABLE_NAME + "." + GStreetDesign.NAME)]
        public new virtual string DesignValue { set; get; }

        public new abstract int? Direction { set; get; }

        [MappedField(GStreetDirection.TABLE_NAME + "." + GStreetDirection.NAME)]
        public new virtual string DirectionValue { set; get; }

        public new abstract int? TollType { set; get; }

        [MappedField(GStreetTollType.TABLE_NAME + "." + GStreetTollType.NAME)]
        public new virtual string TollTypeValue { set; get; }

        public new abstract int? Divider { set; get; }

        public new abstract int? Type { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public new virtual string TypeValue { set; get; }

        public new abstract string Name { set; get; }

        public new abstract string Name2 { set; get; }

        public new abstract string Section { set; get; }

        public new abstract string Postcode { set; get; }

        public new abstract string City { set; get; }

        public new abstract string SubCity { set; get; }

        public new abstract string State { set; get; }

        public new abstract int? FromNodeId { set; get; }

        public new abstract int? ToNodeId { set; get; }

        public new abstract int? SpeedLimit { set; get; }

        public new abstract double? Weight { set; get; }

        public new abstract int? Elevation { set; get; }

        public new abstract double? Length { set; get; }

        public new abstract double? Width { set; get; }

        public new abstract double? Height { set; get; }

        public new abstract int? NumOfLanes { set; get; }

        public new abstract int? Bicycle { set; get; }

        public new abstract int? Bus { set; get; }

        public new abstract int? Car { set; get; }

        public new abstract int? Delivery { set; get; }

        public new abstract int? Emergency { set; get; }

        public new abstract int? Truck { set; get; }

        public new abstract int? Pedestrian { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract string Source { set; get; }

        public new abstract int? AreaId { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        // noraini ali
        public new abstract int? AndStatus { set; get; }
        // end

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(StringUtils.TrimSpaces(TypeValue));
            sb.Add(Name);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);
            sb.Add(",");
            sb.Add(string.Format("Len: {0:n2}", Length));
            sb.Add(",");
            sb.Add(AreaId.ToString());

            sb.Add(",");
            sb.Add("[");
            sb.Add(UpdateStatus.ToString());
            sb.Add(",");
            sb.Add(AndStatus.ToString());
            sb.Add("]");

            return string.Join(" ", sb.ToArray());
        }
    }
}

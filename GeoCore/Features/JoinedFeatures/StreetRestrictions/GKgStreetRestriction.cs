﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features.JoinedFeatures.StreetRestrictions
{
    public class GKgStreetRestriction : GStreetRestriction
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.StreetRestrictions.GKgStreetRestriction.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + STATUS)]
        public override int? Status { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + JUNCTION_ID)]
        public override int? JunctionId { set; get; }

        [MappedField(TABLE_NAME + "." + START_ID)]
        public override int? StartId { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID1)]
        public override int? EndId1 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID2)]
        public override int? EndId2 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID3)]
        public override int? EndId3 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID4)]
        public override int? EndId4 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID5)]
        public override int? EndId5 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID6)]
        public override int? EndId6 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID7)]
        public override int? EndId7 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID8)]
        public override int? EndId8 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID9)]
        public override int? EndId9 { set; get; }

        [MappedField(TABLE_NAME + "." + END_ID10)]
        public override int? EndId10 { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }
    }
}

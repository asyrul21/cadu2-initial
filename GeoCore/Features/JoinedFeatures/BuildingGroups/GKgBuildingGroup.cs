﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GKgStreet = Geomatic.Core.Features.Streets.GKgStreet;

namespace Geomatic.Core.Features.JoinedFeatures.BuildingGroups
{
    public class GKgBuildingGroup : GBuildingGroup
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.BuildingGroups.GKgBuildingGroup.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // added by noraini 
        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }
        // end

        [MappedField(TABLE_NAME + "." + BUILD_NAME_POS)]
        public override int? BuldingNamePos { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_UNIT)]
        public override int? NumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override string ForecastSource { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features.JoinedFeatures.Regions
{
    public class GKvRegion : GRegion
    {
        //public const string INDEX = "INDEX_";

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public const string TABLE_NAME = Geomatic.Core.Features.Regions.GKvRegion.TABLE_NAME;

        public override string TableName { get { return TABLE_NAME; } }

        //[MappedField(INDEX)]
        public override string index { set; get; }
    }
}

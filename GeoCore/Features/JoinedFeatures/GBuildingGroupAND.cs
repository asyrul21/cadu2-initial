﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using GStreetType = Geomatic.Core.Rows.GStreetType;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GBuildingGroupAND : Geomatic.Core.Features.GBuildingGroupAND
    {
        public new abstract string Code { set; get; }

        public new abstract string Name { set; get; }

        public new virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        public new abstract int? NavigationStatus { set; get; }

        public new abstract string Source { set; get; }

        public new abstract int? StreetId { set; get; }

        public abstract int? StreetType { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public virtual string StreetTypeValue { set; get; }

        public abstract string StreetName { set; get; }

        public abstract string StreetName2 { set; get; }

        public abstract string Section { set; get; }

        public abstract string Postcode { set; get; }

        public abstract string City { set; get; }

        public abstract string State { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        public new abstract int? OriId { set; get; }

        public new abstract int? AndStatus { set; get; }

        public new abstract int? AreaId { set; get; }

        public new abstract int? BuldingNamePos { set; get; }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(Name);
            sb.Add(",");
            sb.Add(StringUtils.TrimSpaces(StreetTypeValue));
            sb.Add(StreetName);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);
            sb.Add(",");
            sb.Add("M :");
            sb.Add(BuldingNamePos.ToString());
            sb.Add(",");
            sb.Add(AreaId.ToString());
            sb.Add(",");
            sb.Add("[");
            sb.Add(UpdateStatus.ToString());
            sb.Add(",");
            sb.Add(AndStatus.ToString());
            sb.Add("]");

            return string.Join(" ", sb.ToArray());
        }
    }
}

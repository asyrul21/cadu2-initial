﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Rows;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features.JoinedFeatures
{
    public abstract class GMultiStorey : Geomatic.Core.Rows.GMultiStorey, IPointFeature
    {
        public new abstract string Floor { set; get; }

        public new abstract string Apartment { set; get; }

        public new abstract int? BuildingId { set; get; }

        public new abstract int? GdsId { set; get; }

        public new abstract int? NisId { set; get; }

        public new abstract int? GrId { set; get; }

        public new abstract int? UpdateStatus { set; get; }

        public new abstract int? AndStatus { set; get; }

        public abstract string BuildingName { set; get; }

        public abstract string BuildingName2 { set; get; }

        public abstract int? PropertyType { set; get; }

        [MappedField(GPropertyType.TABLE_NAME + "." + GPropertyType.NAME)]
        public virtual string PropertyTypeValue { set; get; }

        public abstract int? StreetId { set; get; }

        public abstract int? StreetType { set; get; }

        [MappedField(GStreetType.TABLE_NAME + "." + GStreetType.NAME)]
        public virtual string StreetTypeValue { set; get; }

        public abstract string StreetName { set; get; }

        public abstract string StreetName2 { set; get; }

        public abstract string Section { set; get; }

        public abstract string Postcode { set; get; }

        public abstract string City { set; get; }

        public abstract string State { set; get; }

        public new abstract string UpdatedBy { set; get; }

        public new abstract string DateUpdated { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }

        public IPoint Point
        {
            get
            {
                return Shape as IPoint;
            }
        }

        public double X
        {
            get
            {
                return Point.X;
            }
        }

        public double Y
        {
            get
            {
                return Point.Y;
            }
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Floor);
            sb.Add(Apartment);
            sb.Add(BuildingName);
            sb.Add(BuildingName2);
            sb.Add(",");
            sb.Add(StringUtils.TrimSpaces(StreetTypeValue));
            sb.Add(StreetName);
            sb.Add(",");
            sb.Add(Section);
            sb.Add(",");
            sb.Add(Postcode);
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

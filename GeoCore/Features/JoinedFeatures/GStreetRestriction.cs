﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Rows;

namespace Geomatic.Core.Features.JoinedFeatures
{
    /// <summary>
    /// For search purpose only
    /// </summary>
    public abstract class GStreetRestriction : Geomatic.Core.Features.GStreetRestriction
    {
        public new virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        public new abstract int? NavigationStatus { set; get; }

        public new abstract int? Status { set; get; }

        public new abstract string Source { set; get; }

        [MappedField(GSource.TABLE_NAME + "." + GSource.NAME)]
        public new string SourceValue { set; get; }

        public new abstract int? JunctionId { set; get; }

        public new abstract int? StartId { set; get; }

        public new abstract int? EndId1 { set; get; }

        public new abstract int? EndId2 { set; get; }

        public new abstract int? EndId3 { set; get; }

        public new abstract int? EndId4 { set; get; }

        public new abstract int? EndId5 { set; get; }

        public new abstract int? EndId6 { set; get; }

        public new abstract int? EndId7 { set; get; }

        public new abstract int? EndId8 { set; get; }

        public new abstract int? EndId9 { set; get; }

        public new abstract int? EndId10 { set; get; }

        public new abstract string CreatedBy { set; get; }

        public new abstract string DateCreated { set; get; }
    }
}

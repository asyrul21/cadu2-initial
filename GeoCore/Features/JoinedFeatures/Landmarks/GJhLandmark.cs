﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GJhStreet = Geomatic.Core.Features.Streets.GJhStreet;

namespace Geomatic.Core.Features.JoinedFeatures.Landmarks
{
    public class GJhLandmark : GLandmark
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.Landmarks.GJhLandmark.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + STREET_ID)]
        public override int? StreetId { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GJhStreet.TABLE_NAME + "." + GJhStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }
        // end
    }
}

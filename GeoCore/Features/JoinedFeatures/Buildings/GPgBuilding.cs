﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GPgStreet = Geomatic.Core.Features.Streets.GPgStreet;
using GPgProperty = Geomatic.Core.Features.Properties.GPgProperty;
using GPgBuildingGroup = Geomatic.Core.Features.BuildingGroups.GPgBuildingGroup;

namespace Geomatic.Core.Features.JoinedFeatures.Buildings
{
    public class GPgBuilding : GBuilding
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.Buildings.GPgBuilding.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + GROUP_ID)]
        public override int? GroupId { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_FLOOR)]
        public override int? NumberOfFloor { set; get; }

        [MappedField(TABLE_NAME + "." + UNIT)]
        public override int? Unit { set; get; }

        [MappedField(TABLE_NAME + "." + SPACE)]
        public override int? Space { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override int? ForecastSource { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(GPgProperty.TABLE_NAME + "." + GPgProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GPgProperty.TABLE_NAME + "." + GPgProperty.LOT)]
        public override string Lot { set; get; }

        [MappedField(GPgProperty.TABLE_NAME + "." + GPgProperty.HOUSE)]
        public override string House { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GPgStreet.TABLE_NAME + "." + GPgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }
        // end

        // noraini ali - OKT 2021
        [MappedField(GPgBuildingGroup.TABLE_NAME + "." + GPgBuildingGroup.NAME)]
        public override string BuildGrpName { set; get; }
        // end

        public override Geomatic.Core.Features.GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<Geomatic.Core.Features.GProperty>(PropertyId.Value) : null;
        }
    }
}

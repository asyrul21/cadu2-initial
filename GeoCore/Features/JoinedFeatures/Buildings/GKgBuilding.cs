﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using GKgStreet = Geomatic.Core.Features.Streets.GKgStreet;
using GKgProperty = Geomatic.Core.Features.Properties.GKgProperty;
using GKgBuildingGroup = Geomatic.Core.Features.BuildingGroups.GKgBuildingGroup;

namespace Geomatic.Core.Features.JoinedFeatures.Buildings
{
    public class GKgBuilding : GBuilding
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = Geomatic.Core.Features.Buildings.GKgBuilding.TABLE_NAME;

        [MappedField(TABLE_NAME + "." + CONS_STATUS)]
        public override int? ConstructionStatus { set; get; }

        [MappedField(TABLE_NAME + "." + CODE)]
        public override string Code { set; get; }

        [MappedField(TABLE_NAME + "." + NAME)]
        public override string Name { set; get; }

        [MappedField(TABLE_NAME + "." + NAME2)]
        public override string Name2 { set; get; }

        [MappedField(TABLE_NAME + "." + PROPERTY_ID)]
        public override int? PropertyId { set; get; }

        [MappedField(TABLE_NAME + "." + GROUP_ID)]
        public override int? GroupId { set; get; }

        [MappedField(TABLE_NAME + "." + NUM_FLOOR)]
        public override int? NumberOfFloor { set; get; }

        [MappedField(TABLE_NAME + "." + UNIT)]
        public override int? Unit { set; get; }

        [MappedField(TABLE_NAME + "." + SPACE)]
        public override int? Space { set; get; }

        [MappedField(TABLE_NAME + "." + NAVI_STATUS)]
        public override int? NavigationStatus { set; get; }

        [MappedField(TABLE_NAME + "." + SOURCE)]
        public override string Source { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_NUM_UNIT)]
        public override int? ForecastNumUnit { set; get; }

        [MappedField(TABLE_NAME + "." + FORECAST_SOURCE)]
        public override int? ForecastSource { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATE_STATUS)]
        public override int? UpdateStatus { set; get; }

        [MappedField(GKgProperty.TABLE_NAME + "." + GKgProperty.TYPE)]
        public override int? PropertyType { set; get; }

        [MappedField(GKgProperty.TABLE_NAME + "." + GKgProperty.LOT)]
        public override string Lot { set; get; }

        [MappedField(GKgProperty.TABLE_NAME + "." + GKgProperty.HOUSE)]
        public override string House { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.TYPE)]
        public override int? StreetType { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME)]
        public override string StreetName { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.NAME2)]
        public override string StreetName2 { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.SECTION)]
        public override string Section { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.POSTCODE)]
        public override string Postcode { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.CITY)]
        public override string City { set; get; }

        [MappedField(GKgStreet.TABLE_NAME + "." + GKgStreet.STATE)]
        public override string State { set; get; }

        [MappedField(TABLE_NAME + "." + AREA_ID)]
        public override int? AreaId { set; get; }

        [MappedField(TABLE_NAME + "." + CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(TABLE_NAME + "." + UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(TABLE_NAME + "." + DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // noraini ali
        [MappedField(TABLE_NAME + "." + AND_STATUS)]
        public override int? AndStatus { set; get; }
        // end

        // noraini ali - OKT 2021
        [MappedField(GKgBuildingGroup.TABLE_NAME + "." + GKgBuildingGroup.NAME)]
        public override string BuildGrpName { set; get; }
        // end

        public override Geomatic.Core.Features.GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<Geomatic.Core.Features.GProperty>(PropertyId.Value) : null;
        }
    }
}

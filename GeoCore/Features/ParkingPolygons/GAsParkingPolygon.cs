﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ParkingPolygons
{
    public class GAsParkingPolygon : GParkingPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "AS_Parking_Lot_Polygon";
    }
}

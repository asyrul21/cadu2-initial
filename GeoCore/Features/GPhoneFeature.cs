﻿// this file was created by asyrul
// to enable updating of TEL_NO table
// feature is cancelled
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using System.Text.RegularExpressions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using System.Windows.Forms;

namespace Geomatic.Core.Features
{
    public abstract class GPhoneFeature : GFeature
    {
        public static Regex WestFormat = new Regex(@"^(003|004|005|006|007|009)\d{8}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public static Regex EastFormat = new Regex(@"^(082|083|084|085|086|087|088|089)\d{8}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public static Regex MobileFormat = new Regex(@"^6(011|012|013|014|016|017|018|019)\d{8}$", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public static Regex TollFreeFormat = new Regex(@"$1(300|700|800)\d{6}", RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public const string NUMBER = "PHONE_NUMBER";
        public const string PROPERTY_ID = "PROPERTY_ID";
        public const string LOT = "LOT_NUM";
        public const string HOUSE = "HOUSE_NUM";
        public const string APT = "APT";
        public const string FLOOR = "FLOOR";
        public const string BUILDING = "BUILDING_NAME";
        public const string STREET_TYPE = "STREET_TYPE";
        public const string STREET_NAME = "STREET_NAME";
        public const string SECTION = "SECTION_NAME";
        public const string POSTCODE = "POSTCODE";
        public const string CITY = "CITY_NAME";
        public const string STATE = "STATE_CODE";

        // added by asyrul
        public const string CABINET_TYPE = "CABINET_TYPE";
        public const string COOR_DP = "COOR_DP";
        public const string COOR_CAB = "COOR_CAB";
        public const string COOR_PROP = "COOR_PROP";
        public const string UPDATE_TYPE = "UPDATE_TYPE";
        public const string TERM_TYPE = "TERM_TYPE";


        // added end

        [MappedField(NUMBER)]
        public virtual string Number { set; get; }

        [MappedField(PROPERTY_ID)]
        public virtual int? PropertyId { set; get; }

        [MappedField(LOT)]
        public virtual string Lot { set; get; }

        [MappedField(HOUSE)]
        public virtual string House { set; get; }

        [MappedField(APT)]
        public virtual string Apartment { set; get; }

        [MappedField(FLOOR)]
        public virtual string Floor { set; get; }

        [MappedField(BUILDING)]
        public virtual string Building { set; get; }

        [MappedField(STREET_TYPE)]
        public virtual string StreetType { set; get; }

        [MappedField(STREET_NAME)]
        public virtual string StreetName { set; get; }

        [MappedField(SECTION)]
        public virtual string Section { set; get; }

        [MappedField(POSTCODE)]
        public virtual string Postcode { set; get; }

        [MappedField(CITY)]
        public virtual string City { set; get; }

        [MappedField(STATE)]
        public virtual string State { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        // added by asyrul
        [MappedField(CABINET_TYPE)]
        public virtual string CabinetType { set; get; }

        [MappedField(COOR_DP)]
        public virtual string CoorDP { set; get; }

        [MappedField(COOR_CAB)]
        public virtual string CoorCAB { set; get; }

        [MappedField(COOR_PROP)]
        public virtual string CoorProp { set; get; }

        [MappedField(UPDATE_TYPE)]
        public virtual string UpdateType { set; get; }

        [MappedField(TERM_TYPE)]
        public virtual string TermType { set; get; }
        // added end

        public abstract IEnumerable<string> GetAreaCodes();

        public abstract Regex GetFormat();

        public virtual Regex GetMobileFormat()
        {
            return MobileFormat;
        }

        public virtual Regex GetTollFreeFormat()
        {
            return TollFreeFormat;
        }

        public virtual GProperty GetProperty()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (PropertyId.HasValue) ? repo.GetById<GProperty>(PropertyId.Value) : null;
        }

        // added by asyrul
        public void unGeocode()
        {
            //RepositoryFactory repo = new RepositoryFactory(SegmentName);
            MessageBox.Show("This OID: " + this.OID);

            this.TermType = getTermType(this.CabinetType, this.CoorCAB, this.CoorDP);
            this.DateUpdated = DateTime.Now.ToString("yyyyMMdd");
            this.CoorProp = "0,0";
            this.PropertyId = 0;
            this.UpdateType = "9";
            this.UpdatedBy = "CADUPropertyDeleted";

            //repo.Update(this);
        }

        private string getTermType(string cabinet_type, string coor_cab, string coor_dp)
        {
            string TERM_TYPE = "0";

            if (coor_dp == "0,0" || coor_dp.Contains("-") || coor_dp == "0.0,0.0" || coor_dp == null)
            {
                coor_dp = "0,0";

                if (coor_cab == "0,0" || coor_cab.Contains("-") || coor_cab == "0.0,0.0" || coor_cab == null)
                {
                    coor_cab = "0,0";
                    TERM_TYPE = "0";
                }
                else
                {
                    if (cabinet_type == "CAB")
                    {
                        TERM_TYPE = "3";
                    }
                    else if (cabinet_type == "FTTS")
                    {
                        TERM_TYPE = "4";
                    }
                    else if (cabinet_type == "PFTTS")
                    {
                        TERM_TYPE = "5";
                    }
                    else if (cabinet_type == "FTTO")
                    {
                        TERM_TYPE = "6";
                    }
                    else if (cabinet_type == "SDF")
                    {
                        TERM_TYPE = "7";
                    }
                    else if (cabinet_type == "PCAB")
                    {
                        TERM_TYPE = "8";
                    }
                    else if (cabinet_type == "DDP")
                    {
                        TERM_TYPE = "9";
                    }
                    //shirlin 03/05/2012 - HSBB geocoding
                    else if (cabinet_type == "MUX")
                    {
                        TERM_TYPE = "11";
                    }
                    else if (cabinet_type == "FDC")
                    {
                        TERM_TYPE = "13";
                    }
                    else if (cabinet_type == "VDSL")
                    {
                        TERM_TYPE = "14";
                    }
                }
            }
            else
            {
                if (cabinet_type == "FDC")
                {
                    TERM_TYPE = "12";
                }
                else
                {
                    TERM_TYPE = "2";
                }
            }
            return TERM_TYPE;
        }

        // added end
    }
}

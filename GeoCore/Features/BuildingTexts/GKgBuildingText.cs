﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.BuildingTexts
{
    public class GKgBuildingText : GBuildingText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "KG_ADM_BUILDING_TEXT";
    }
}

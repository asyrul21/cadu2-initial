﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features
{
    public abstract class GStreetRestriction : GLineFeature, IUserCreate, IGNavigationObject
    {
        public const string STATUS = "TR_STATUS";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string SOURCE = "SOURCE";
        public const string JUNCTION_ID = "JUNCTION_ID";
        public const string START_ID = "START_STREET_ID";
        public const string END_ID1 = "END_STREET0_ID";
        public const string END_ID2 = "END_STREET1_ID";
        public const string END_ID3 = "END_STREET2_ID";
        public const string END_ID4 = "END_STREET3_ID";
        public const string END_ID5 = "END_STREET4_ID";
        public const string END_ID6 = "END_STREET5_ID";
        public const string END_ID7 = "END_STREET6_ID";
        public const string END_ID8 = "END_STREET7_ID";
        public const string END_ID9 = "END_STREET8_ID";
        public const string END_ID10 = "END_STREET9_ID";

        [MappedField(STATUS)]
        public virtual int? Status { set; get; }

        public virtual string StatusValue
        {
            get
            {
                return null;
            }
        }

        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(JUNCTION_ID)]
        public int? JunctionId { set; get; }

        [MappedField(START_ID)]
        public int? StartId { set; get; }

        [MappedField(END_ID1)]
        public int? EndId1 { set; get; }

        [MappedField(END_ID2)]
        public int? EndId2 { set; get; }

        [MappedField(END_ID3)]
        public int? EndId3 { set; get; }

        [MappedField(END_ID4)]
        public int? EndId4 { set; get; }

        [MappedField(END_ID5)]
        public int? EndId5 { set; get; }

        [MappedField(END_ID6)]
        public int? EndId6 { set; get; }

        [MappedField(END_ID7)]
        public int? EndId7 { set; get; }

        [MappedField(END_ID8)]
        public int? EndId8 { set; get; }

        [MappedField(END_ID9)]
        public int? EndId9 { set; get; }

        [MappedField(END_ID10)]
        public int? EndId10 { set; get; }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public GJunction GetJunction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (JunctionId.HasValue) ? repo.GetById<GJunction>(JunctionId.Value) : null;
        }

        public IEnumerable<GStreet> GetStreets()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GStreet> streets = new List<GStreet>();
            GStreet street = GetStartStreet();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet1();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet2();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet3();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet4();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet5();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet6();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet7();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet8();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet9();
            if (street != null)
            {
                streets.Add(street);
            }
            street = GetEndStreet10();
            if (street != null)
            {
                streets.Add(street);
            }
            return streets;
        }

        public GStreet GetStartStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StartId.HasValue) ? repo.GetById<GStreet>(StartId.Value) : null;
        }

        public GStreet GetEndStreet1()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId1.HasValue) ? repo.GetById<GStreet>(EndId1.Value) : null;
        }

        public GStreet GetEndStreet2()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId2.HasValue) ? repo.GetById<GStreet>(EndId2.Value) : null;
        }

        public GStreet GetEndStreet3()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId3.HasValue) ? repo.GetById<GStreet>(EndId3.Value) : null;
        }

        public GStreet GetEndStreet4()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId4.HasValue) ? repo.GetById<GStreet>(EndId4.Value) : null;
        }

        public GStreet GetEndStreet5()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId5.HasValue) ? repo.GetById<GStreet>(EndId5.Value) : null;
        }

        public GStreet GetEndStreet6()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId6.HasValue) ? repo.GetById<GStreet>(EndId6.Value) : null;
        }

        public GStreet GetEndStreet7()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId7.HasValue) ? repo.GetById<GStreet>(EndId7.Value) : null;
        }

        public GStreet GetEndStreet8()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId8.HasValue) ? repo.GetById<GStreet>(EndId8.Value) : null;
        }

        public GStreet GetEndStreet9()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId9.HasValue) ? repo.GetById<GStreet>(EndId9.Value) : null;
        }

        public GStreet GetEndStreet10()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (EndId10.HasValue) ? repo.GetById<GStreet>(EndId10.Value) : null;
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public void Init()
        {
            NavigationStatus = 1;
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GStreetRestriction))
            {
                return false;
            }

            GStreetRestriction streetRestriction = (GStreetRestriction)obj;
            return (OID == streetRestriction.OID && TableName == streetRestriction.TableName && ValueEquals(streetRestriction, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));

            return string.Join(" ", sb.ToArray());
        }
    }
}

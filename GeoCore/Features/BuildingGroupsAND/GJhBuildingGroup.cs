﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupsAND
{
    public class GJhBuildingGroup : GBuildingGroupAND
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JH; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JH_AND_BUILDING_GROUP";

        private static List<GBuildingGroupAND> _allBuildingGroup;
        public static IEnumerable<GBuildingGroupAND> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuildingGroupAND> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.JH);
            if (updated)
            {
                _allBuildingGroup = repo.Search<GBuildingGroupAND>().ToList();
            }
            else
            {
                if (_allBuildingGroup == null)
                {
                    _allBuildingGroup = repo.Search<GBuildingGroupAND>().ToList();
                }
            }
            return _allBuildingGroup;
        }
    }
}

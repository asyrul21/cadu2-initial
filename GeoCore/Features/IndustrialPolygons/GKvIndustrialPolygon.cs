﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.IndustrialPolygons
{
    public class GKvIndustrialPolygon : GIndustrialPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_Industrial_Area";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.HydrographicLines
{
    public class GKkHydrographicLine : GHydrographicLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KK_Hydrographic_Line";
    }
}

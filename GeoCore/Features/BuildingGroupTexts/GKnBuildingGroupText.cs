﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupTexts
{
    public class GKnBuildingGroupText : GBuildingGroupText
    {
        public override string TableName { get { return TABLE_NAME; } }
        public const string TABLE_NAME = "KN_ADM_BUILDING_GROUP_TEXT"; 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupTexts
{
    public class GTgBuildingGroupText : GBuildingGroupText
    {
        public override string TableName { get { return TABLE_NAME; } }
        public const string TABLE_NAME = "TG_ADM_BUILDING_GROUP_TEXT";
    }
}

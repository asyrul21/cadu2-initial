﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupTexts
{
    public class GMkBuildingGroupText : GBuildingGroupText
    {
        public override string TableName { get { return TABLE_NAME; } }
        public const string TABLE_NAME = "MK_ADM_BUILDING_GROUP_TEXT";
    }
}

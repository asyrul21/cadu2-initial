﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GPointFeature : GFeature, IPointFeature
    {
        public IPoint Point
        {
            get
            {
                return Shape as IPoint;
            }
        }

        public double X
        {
            get
            {
                return Point.X;
            }
        }

        public double Y
        {
            get
            {
                return Point.Y;
            }
        }
    }
}

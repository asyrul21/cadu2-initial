﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.ResidentialLines
{
    public class GJpResidentialLine : GResidentialLine
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.JP; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "JP_Residential_Line";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.BungalowPolygons
{
    public class GAsBungalowPolygon : GBungalowPolygon
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.AS; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "AS_Bungalow_Lot";
    }
}

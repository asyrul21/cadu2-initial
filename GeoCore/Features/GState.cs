﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;

namespace Geomatic.Core.Features
{
    public class GState : GPolygonFeature
    {
        private static List<GState> _states;

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override SegmentName SegmentName
        {
            get { return SegmentName.None; }
        }

        public const string TABLE_NAME = "ML_STATE_RSO";

        public const string CODE = "STATE_ABB";
        public const string NAME = "STATE_NAME";
        public const string SEGMENT_NAME = "DATA_SEGMENT";

        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(SEGMENT_NAME)]
        public virtual string Segment { set; get; }

        public override string CreatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateCreated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string UpdatedBy
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string DateUpdated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public static IEnumerable<GState> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GState> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory();
            if (updated)
            {
                _states = repo.Search<GState>().ToList();
            }
            else
            {
                if (_states == null)
                {
                    _states = repo.Search<GState>().ToList();
                }
            }
            return _states;
        }

        public static IEnumerable<GState> GetAll(bool updated, string segment)
        {
            RepositoryFactory repo = new RepositoryFactory();
            Query<GState> query = new Query<GState>();
            query.Obj.Segment = segment;
            _states = repo.Search(query).ToList();
            return _states;
        }

        public override string ToString()
        {
            return StringUtils.TrimSpaces(Code);
        }
    }
}

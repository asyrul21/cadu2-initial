﻿
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using System.Collections.Generic;
using GConstructionStatus = Geomatic.Core.Rows.GConstructionStatus;
using GPropertyType = Geomatic.Core.Rows.GPropertyType;
using GPhone = Geomatic.Core.Rows.GPhone;
using ESRI.ArcGIS.Geometry;
using System.Windows.Forms;
using Geomatic.Core.Oracle;
using System.Linq;
using Geomatic.Core.Utilities;
using System;

namespace Geomatic.Core.Features
{
    public abstract class GPropertyAND : GProperty
    {
        public const string ORI_ID = "ORIGINAL_ID";

        [MappedField(ORI_ID)]
        public virtual int OriId { set; get; }

        public override IEnumerable<GFloor> GetFloors()
        {
            return GetFloors(false);
        }

        public override IEnumerable<GFloor> GetFloors(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GFloor> query = new Query<GFloor>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            }
            return repo.Search(query);
        }

        public override IEnumerable<GBuilding> Getbuildings()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            return repo.Search(query);
        }

        public override GBuilding Getbuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuilding> building = Getbuildings().ToList();
            return (building.Count() > 0) ? building.First() : null;
        }

        public override IEnumerable<GBuildingAND> GetbuildingsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            return repo.Search(query);
        }

        public virtual GBuildingAND GetbuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingAND> building = GetbuildingsAND().ToList();
            return (building.Count() > 0) ? building.First() : null;
        }
        // end add

        public override IEnumerable<GPoi> GetPois()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.Obj.ParentId = this.OriId;
            query.AddClause(() => query.Obj.ParentType, "IN (1, 2)");
            return repo.Search(query);
        }

        public override bool HasFloor()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GFloor> query = new Query<GFloor>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            query.AddClause(() => query.Obj.UpdateStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public override bool HasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            query.AddClause(() => query.Obj.AndStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public override bool HasBuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            query.AddClause(() => query.Obj.AndStatus, "<> 2");
            return repo.Count(query) > 0;
        }

        public override bool HasPhone()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            return repo.Count(query) > 0;
        }

        public override IEnumerable<GPhone> GetPhones()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPhone> query = new Query<GPhone>(SegmentName);
            query.Obj.PropertyId = this.OriId;
            return repo.Search(query);
        }

        public void ungeocodeAllPhones2()
        {
            TelNoUngeocoder ung = new TelNoUngeocoder(SegmentName);
            IEnumerable<GPhone> phones = this.GetPhones();

            foreach (GPhone phone in phones)
            {
                //MessageBox.Show(ung.selectTelNo(phone));
                ung.ungeocodeTelNo(phone);
            }
        }

        public override void CopyFrom(GProperty property)
        {
            this.ConstructionStatus = property.ConstructionStatus;
            this.House = property.House;
            this.Lot = property.Lot;
            this.Source = property.Source;
            this.YearInstall = property.YearInstall;
            this.Type = property.Type;
            this.NumberOfFloor = property.NumberOfFloor;
            this.Type = property.Type;
            this.StreetId = property.StreetId;
            this.AreaId = property.AreaId;
            this.Shape = property.Point;
            this.OriId = property.OID;
            this.CreatedBy = property.CreatedBy;
            this.DateCreated = property.DateCreated;
            this.UpdatedBy = property.UpdatedBy;
            this.DateUpdated = property.DateUpdated;
        }

        public virtual IEnumerable<GPropertyTextAND> GetTextsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GPropertyTextAND> query = new Query<GPropertyTextAND>(SegmentName);
            query.Obj.LinkedFeatureID = OID; // OriId;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public void UpdateTextAND()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text AND.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string text1Value = CreateText1Value();
            string text2Value = CreateText2Value();

            IPoint text1Point = (IPoint)Shape.Clone();
            text1Point.Move(0, 0.6);
            IPoint text2Point = (IPoint)Shape.Clone();
            text2Point.Move(0, -1.3);

            List<GPropertyTextAND> texts = GetTextsAND().ToList();

            if (texts.Count == 0)
            {
                GPropertyTextAND text = repo.NewObj<GPropertyTextAND>();
                text.LinkedFeatureID = OID; // OriId;
                text.Text = text1Value;
                text.Shape = text1Point;
                text = repo.Insert(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    GPropertyTextAND text2 = repo.NewObj<GPropertyTextAND>();
                    text2.LinkedFeatureID = OID; // OriId;
                    text2.Text = text2Value;
                    text2.Shape = text2Point;
                    text2 = repo.Insert(text2);
                }
            }
            else
            {
                GPropertyTextAND text = texts[0];
                text.Text = text1Value;
                text.Shape = text1Point;
                repo.Update(text);

                if (!string.IsNullOrEmpty(text2Value))
                {
                    if (texts.Count >= 2)
                    {
                        GPropertyTextAND text2 = texts[1];
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        repo.Update(text2);
                    }
                    else
                    {
                        GPropertyTextAND text2 = repo.NewObj<GPropertyTextAND>();
                        text2.LinkedFeatureID = OID; // OriId;
                        text2.Text = text2Value;
                        text2.Shape = text2Point;
                        text2 = repo.Insert(text2);
                    }
                }
                else
                {
                    if (texts.Count >= 2)
                    {
                        repo.Delete(texts[1]);
                    }
                }
            }

            // to take care previous program error
            if (texts.Count > 2)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }


        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("OriId: [ {0} ]", OriId));
            sb.Add(Lot);
            sb.Add(House);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(StringUtils.TrimSpaces(street.TypeValue));
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(",");
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }
    }
}

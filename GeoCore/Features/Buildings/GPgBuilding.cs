﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.Buildings
{
    public class GPgBuilding : GBuilding
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.PG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "PG_ADM_BUILDING";

        //added by asyrul

        private static List<GBuilding> _allBuilding;
        public static IEnumerable<GBuilding> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuilding> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.PG);
            if (updated)
            {
                _allBuilding = repo.Search<GBuilding>().ToList();
            }
            else
            {
                if (_allBuilding == null)
                {
                    _allBuilding = repo.Search<GBuilding>().ToList();
                }
            }
            return _allBuilding;
        }
        //added end
    }
}

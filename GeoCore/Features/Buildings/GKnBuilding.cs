﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.Buildings
{
    public class GKnBuilding : GBuilding
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KN_ADM_BUILDING";

        //added by asyrul

        private static List<GBuilding> _allBuilding;
        public static IEnumerable<GBuilding> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GBuilding> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KN);
            if (updated)
            {
                _allBuilding = repo.Search<GBuilding>().ToList();
            }
            else
            {
                if (_allBuilding == null)
                {
                    _allBuilding = repo.Search<GBuilding>().ToList();
                }
            }
            return _allBuilding;
        }
        //added end
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.JunctionsAND
{
    public class GKvJunction : GJunctionAND
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public const string TABLE_NAME = "KV_AND_JUNCTION";

        private static List<GJunctionAND> _allJunction;
        public static IEnumerable<GJunctionAND> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GJunctionAND> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KV);
            if (updated)
            {
                _allJunction = repo.Search<GJunctionAND>().ToList();
            }
            else
            {
                if (_allJunction == null)
                {
                    _allJunction = repo.Search<GJunctionAND>().ToList();
                }
            }
            return _allJunction;
        }
    }
}

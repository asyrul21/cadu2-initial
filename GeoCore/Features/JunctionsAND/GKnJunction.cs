﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.JunctionsAND
{
    public class GKnJunction : GJunctionAND
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public const string TABLE_NAME = "KN_AND_JUNCTION";

        //added by asyrul

        private static List<GJunctionAND> _allJunction;
        public static IEnumerable<GJunctionAND> GetAll()
        {
            return GetAll(true);
        }

        public static IEnumerable<GJunctionAND> GetAll(bool updated)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName.KN);
            if (updated)
            {
                _allJunction = repo.Search<GJunctionAND>().ToList();
            }
            else
            {
                if (_allJunction == null)
                {
                    _allJunction = repo.Search<GJunctionAND>().ToList();
                }
            }
            return _allJunction;
        }
    }
}

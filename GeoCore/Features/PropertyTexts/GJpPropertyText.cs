﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.PropertyTexts
{
    public class GJpPropertyText : GPropertyText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "JP_ADM_PROPERTY_TEXT";
    }
}

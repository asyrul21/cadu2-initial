﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.PropertyTexts
{
    public class GTgPropertyText : GPropertyText
    {
        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public const string TABLE_NAME = "TG_ADM_PROPERTY_TEXT";
    }
}

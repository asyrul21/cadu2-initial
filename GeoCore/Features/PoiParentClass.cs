﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features
{
    public abstract partial class GPoi
    {
        public enum ParentClass
        {
            Property1 = 1,
            Property2,
            Building,
            Landmark
        }
    }
}

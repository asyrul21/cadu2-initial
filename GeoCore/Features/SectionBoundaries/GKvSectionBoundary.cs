﻿using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.SectionBoundaries
{
    public class GKvSectionBoundary : GSectionBoundary
    {

        public override SegmentName SegmentName
        {
            get { return SegmentName.KV; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KV_ADM_BOUNDARY_SECTION";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.SectionBoundaries
{
    public class GKgSectionBoundary : GSectionBoundary
    {
        public override SegmentName SegmentName
        {
            get { return SegmentName.KG; }
        }

        public override string TableName { get { return TABLE_NAME; } }

        public const string TABLE_NAME = "KG_ADM_BOUNDARY_SECTION";
    }
}

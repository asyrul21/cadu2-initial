﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextMapper;

namespace Geomatic.Core.Features
{
    public abstract class GText : TextFeature, IGText
    {
        public abstract string TableName { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.MajorRoadOutlines
{
    public class GKnMajorRoadOutline : GMajorRoadOutline
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KN; }
        }

        public const string TABLE_NAME = "KN_Major_Road_OutLine";
    }
}

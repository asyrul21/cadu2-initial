﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;

namespace Geomatic.Core.Features
{
    public abstract class GSectionBoundary : GPolygonFeature, IUserCreate, IUserUpdate
    {
        public const string NAME = "SECTION_NAME";
        public const string CITY = "CITY_NAME";
        public const string STATE = "STATE_CODE";
        public const string SOURCE = "SOURCE";

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        [MappedField(CITY)]
        public virtual string City { set; get; }

        [MappedField(STATE)]
        public virtual string State { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public void Init()
        {
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GSectionBoundary))
            {
                return false;
            }

            GSectionBoundary sectionBoundary = (GSectionBoundary)obj;
            return (OID == sectionBoundary.OID && TableName == sectionBoundary.TableName && ValueEquals(sectionBoundary, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            return canDelete;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);
            sb.Add(",");
            sb.Add(City);
            sb.Add(",");
            sb.Add(State);

            return string.Join(" ", sb.ToArray());
        }
    }
}

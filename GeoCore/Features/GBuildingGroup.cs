﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using System.Text.RegularExpressions;
using Geomatic.Core.Utilities;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GBuildingGroup : GPointFeature, IUserCreate, IUserUpdate, IGNavigationObject, IGNepsObject
    {
        public const string CODE = "BUILDING_GROUP_CODE";
        public const string NAME = "BUILDING_GROUP_NAME";
        public const string STREET_ID = "STREET_ID";
        public const string NAVI_STATUS = "NAVIGATION_STATUS";
        public const string SOURCE = "SOURCE";
        public const string AND_STATUS = "AND_STATUS";  
        public const string AREA_ID = "AREA_ID";
        public const string UPDATE_STATUS = "STATUS"; // added by noraini - Mac 2021
        public const string BUILD_NAME_POS = "BUILD_NAME_POS"; // added by syafiq - June 2021

        public const string NUM_UNIT = "NUM_UNIT";
        public const string FORECAST_NUM_UNIT = "FORECAST_NUM_UNIT";
        public const string FORECAST_SOURCE = "FORECAST_SOURCE";


        [MappedField(CODE)]
        public virtual string Code { set; get; }

        [MappedField(NAME)]
        public virtual string Name { set; get; }

        public virtual bool IsNavigationReady
        {
            get
            {
                return (NavigationStatus == 1);
            }
        }

        [MappedField(NAVI_STATUS)]
        public virtual int? NavigationStatus { set; get; }

        public virtual string NavigationStatusValue
        {
            get
            {
                return (NavigationStatus == 1) ? "READY" : "NOT READY";
            }
        }

        [MappedField(STREET_ID)]
        public virtual int? StreetId { set; get; }

        [MappedField(SOURCE)]
        public virtual string Source { set; get; }

        public virtual string SourceValue
        {
            get
            {
                GSource source = GetSourceObj();
                return StringUtils.TrimSpaces((source == null) ? string.Empty : source.Name);
            }
        }

        [MappedField(CREATED_BY)]
        public override string CreatedBy { set; get; }

        [MappedField(DATE_CREATED)]
        public override string DateCreated { set; get; }

        [MappedField(UPDATED_BY)]
        public override string UpdatedBy { set; get; }

        [MappedField(DATE_UPDATED)]
        public override string DateUpdated { set; get; }
        
        // added by noraini
        [MappedField(AREA_ID)]
        public virtual int? AreaId { set; get; }

        [MappedField(AND_STATUS)]
        public virtual int? AndStatus { set; get; }
        // end

        // added by noraini - Mac 2021
        [MappedField(UPDATE_STATUS)]
        public virtual int? UpdateStatus { set; get; }

        [MappedField(BUILD_NAME_POS)]
        public virtual int? BuldingNamePos { set; get; }

        [MappedField(NUM_UNIT)]
        public virtual int? NumUnit { set; get; }

        [MappedField(FORECAST_NUM_UNIT)]
        public virtual int? ForecastNumUnit { set; get; }

        [MappedField(FORECAST_SOURCE)]
        public virtual string ForecastSource { set; get; }

        public virtual string ForecastSourceValue
        {
            get
            {
                GSource source = GetForecastSourceObj();
                return (source == null) ? GSource.DEFAULT : source.Name;
            }

        }

        public virtual string Code1
        {
            get
            {
                return GCode.GetCode1(Code);
            }
        }

        public virtual string Code2
        {
            get
            {
                return GCode.GetCode2(Code);
            }
        }

        public virtual string Code3
        {
            get
            {
                return GCode.GetCode3(Code);
            }
        }

        public virtual GCode1 GetCode1()
        {
            return GCode1.Get(Code1);
        }

        public virtual GCode2 GetCode2()
        {
            return GCode2.Get(Code1, Code2);
        }

        public virtual GCode3 GetCode3()
        {
            return GCode3.Get(Code1, Code2, Code3);
        }

        public virtual GSource GetSourceObj()
        {
            int testInt;
            return int.TryParse(Source, out testInt) ? GSource.Get(testInt) : null;
        }

        public virtual GSource GetForecastSourceObj()
        {
            return int.TryParse(ForecastSource, out int testInt) ? GSource.Get(testInt) : null;
        }

        public virtual IEnumerable<GBuilding> GetBuildings()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.GroupId = OID;
            return repo.Search(query);
        }

        public virtual IEnumerable<GBuildingAND> GetBuildingsAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.GroupId = OID;
            return repo.Search(query);
        }

        public virtual GStreet GetStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreet>(StreetId.Value) : null;
        }

        public virtual GStreetAND GetStreetAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            return (StreetId.HasValue) ? repo.GetById<GStreetAND>(StreetId.Value) : null;
        }

        public virtual bool HasBuilding()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.Obj.GroupId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool HasBuildingAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.Obj.GroupId = OID;
            return repo.Count(query) > 0;
        }

        public virtual bool CanDelete()
        {
            bool canDelete = true;
            canDelete &= !HasBuilding();
            return canDelete;
        }

        public void Init()
        {
            NavigationStatus = 0;
            Source = "1";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GBuildingGroup))
            {
                return false;
            }

            GBuildingGroup buildingGroup = (GBuildingGroup)obj;
            return (OID == buildingGroup.OID && TableName == buildingGroup.TableName && ValueEquals(buildingGroup, true));
        }

        public override int GetHashCode()
        {
            return OID;
        }

        public override string ToString()
        {
            List<string> sb = new List<string>();

            sb.Add(string.Format("ID: [ {0} ]", OID));
            sb.Add(Name);

            GStreet street = GetStreet();
            if (street != null)
            {
                sb.Add(",");
                sb.Add(street.TypeValue);
                sb.Add(street.Name);
                sb.Add(",");
                sb.Add(street.Section);
                sb.Add(",");
                sb.Add(street.Postcode);
                sb.Add(street.City);
                sb.Add(street.State);
            }
            return string.Join(" ", sb.ToArray());
        }

        // noraini 
        public virtual IEnumerable<GBuildingGroupAND> GetBuildingGroupAND()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);
            query.Obj.OriId = this.OID;
            query.AddClause(() => query.Obj.AndStatus, "<> 2");
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GBuildingGroupAND GetBuildingGroupANDId()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingGroupAND> buildingGroupAND = GetBuildingGroupAND().ToList();
            return buildingGroupAND.Count() > 0 ? buildingGroupAND.First() : null;
        }

        public virtual IEnumerable<GBuildingGroupAND> GetBuildingGroupAND(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);
            query.Obj.OriId = this.OID;
            if (!includeDeleted)
            {
                query.AddClause(() => query.Obj.AndStatus, "<> 2");
            }
            return (OID > 0) ? repo.Search(query) : null;
        }

        public virtual GBuildingGroupAND GetBuildingGroupANDId(bool includeDeleted)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            IEnumerable<GBuildingGroupAND> buildingGroupAND = GetBuildingGroupAND(includeDeleted).ToList();
            return buildingGroupAND.Count() > 0 ? buildingGroupAND.First() : null;
        }

        public virtual void CopyFrom(GBuildingGroup buildingGroup)
        {
            this.NavigationStatus = buildingGroup.NavigationStatus;
            this.Code = buildingGroup.Code;
            this.Name = buildingGroup.Name;
            this.Source = buildingGroup.Source;
            this.BuldingNamePos = buildingGroup.BuldingNamePos;
            this.ForecastSource = buildingGroup.ForecastSource;
        }

        public virtual IEnumerable<GBuildingGroupText> GetTexts()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            Query<GBuildingGroupText> query = new Query<GBuildingGroupText>(SegmentName);
            query.Obj.LinkedFeatureID = OID;
            return (OID > 0) ? repo.Search(query) : null;
        }

        public string CreateTextValue()
        {
            return Name;
        }

        public void UpdateText()
        {
            if (OID < 1)
            {
                throw new Exception("Unable to update text.");
            }

            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            string textValue = CreateTextValue();

            IPoint textPoint = (IPoint)Shape.Clone();
            textPoint.Move(0, 0.6);

            List<GBuildingGroupText> texts = GetTexts().ToList();

            if (texts.Count == 0)
            {
                GBuildingGroupText text = repo.NewObj<GBuildingGroupText>();
                text.LinkedFeatureID = OID;
                text.Text = textValue;
                text.Shape = textPoint;
                text = repo.Insert(text);
            }
            else
            {
                GBuildingGroupText text = texts[0];
                text.Text = textValue;
                text.Shape = textPoint;
                repo.Update(text);
            }

            // to take care previous program error
            if (texts.Count > 1)
            {
                for (int count = 2; count < texts.Count; count++)
                {
                    repo.Delete(texts[count]);
                }
            }
        }
    }
}

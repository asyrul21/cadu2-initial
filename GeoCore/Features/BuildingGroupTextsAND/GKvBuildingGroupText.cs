﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupTextsAND
{
    public class GKvBuildingGroupText : GBuildingGroupTextAND
    {
        public override string TableName { get { return TABLE_NAME; } }
        public const string TABLE_NAME = "KV_AND_BUILDING_GROUP_TEXT";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;

namespace Geomatic.Core.Features.BuildingGroupTextsAND
{
    public class GMkBuildingGroupText : GBuildingGroupTextAND
    {
        public override string TableName { get { return TABLE_NAME; } }
        public const string TABLE_NAME = "MK_AND_BUILDING_GROUP_TEXT";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Utilities;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.Core.Features
{
    public abstract class GLineFeature : GFeature, ILineFeature
    {
        public IPolyline Polyline
        {
            get
            {
                return Shape as IPolyline;
            }
        }

        public IPoint FromPoint
        {
            set
            {
                Polyline.FromPoint = value;
                Shape = Polyline;
            }
            get
            {
                return Polyline.FromPoint;
            }
        }

        public IPoint ToPoint
        {
            set
            {
                Polyline.ToPoint = value;
                Shape = Polyline;
            }
            get
            {
                return Polyline.ToPoint;
            }
        }

        public IEnumerable<IPoint> Vertices
        {
            get
            {
                IPointCollection4 pointCollection = (IPointCollection4)Shape;
                for (int count = 0; count < pointCollection.PointCount; count++)
                {
                    yield return pointCollection.get_Point(count);
                }
            }
        }

        public int SegmentCount
        {
            get
            {
                ISegmentCollection segmentCollection = (ISegmentCollection)Shape;
                return segmentCollection.SegmentCount;
            }
        }

        public int PointCount
        {
            get
            {
                IPointCollection4 pointCollection = (IPointCollection4)Shape;
                return pointCollection.PointCount;
            }
        }

        public ISegment GetSegment(int index)
        {
            ISegmentCollection segmentCollection = (ISegmentCollection)Shape;
            return segmentCollection.Segment[index];
        }

        protected IPolyline CreateLine(ISegmentCollection segmentCollection, ISpatialReference spatialReference, int fromIndex, int toIndex, double minLength)
        {
            IPolyline newLine = new PolylineClass();
            newLine.SpatialReference = spatialReference;
            ISegmentCollection newSegmentCollection = (ISegmentCollection)newLine;
            for (int count = fromIndex; count <= toIndex; count++)
            {
                newSegmentCollection.AddSegment(segmentCollection.Segment[count], System.Type.Missing, System.Type.Missing);
            }

            if (newLine.Length < minLength)
            {
                throw new Exception(string.Format("Length is less than {0} meter.", minLength));
            }
            return newLine;
        }

        public virtual bool TryAddVertex(IPoint point)
        {
            bool isSplitted = false;
            int newPartIndex = 0;
            int newSegmentIndex = 0;
            IPolyline polyline = Polyline;
            polyline.SplitAtPoint(point, true, false, out isSplitted, out newPartIndex, out newSegmentIndex);

            Shape = polyline;

            return isSplitted;
        }

        public virtual bool TryDeleteVertex(int index)
        {
            IPolyline polyline = Polyline;

            IPointCollection4 pointCollection = (IPointCollection4)polyline;

            if (index == 0 || index >= pointCollection.PointCount - 1)
            {
                return false;
            }

            pointCollection.RemovePoints(index, 1);

            Shape = polyline;

            return true;
        }

        public virtual bool TryDeleteVertex(IPoint point)
        {
            double searchRadius = 1;
            double hitDistance = 0;
            int hitPartIndex = 0;
            int hitSegmentIndex = 0;
            bool isRightSide = false;

            IPoint hitPoint = new PointClass();
            IPolyline polyline = Polyline;
            if (!polyline.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
            {
                return false;
            }

            return TryDeleteVertex(hitSegmentIndex);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkBoundariesAND
{
    public class GTgLandmarkBoundary : GLandmarkBoundaryAND
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.TG; }
        }

        public const string TABLE_NAME = "TG_AND_LANDMARK_BND";
    }
}

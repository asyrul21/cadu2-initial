﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkBoundariesAND
{
    public class GMkLandmarkBoundary : GLandmarkBoundaryAND
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.MK; }
        }

        public const string TABLE_NAME = "MK_AND_LANDMARK_BND";
    }
}

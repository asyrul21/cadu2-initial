﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LandmarkBoundariesAND
{
    public class GKkLandmarkBoundary: GLandmarkBoundaryAND
    {
        public override string TableName { get { return TABLE_NAME; } }

        public override SegmentName SegmentName
        {
            get { return SegmentName.KK; }
        }

        public const string TABLE_NAME = "KK_AND_LANDMARK_BND";
    }
}

﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LotBoundaryTexts
{
    public class GTgLotBoundaryText : GLotBoundaryText
    {
        public const string TABLE_NAME = "TG_LOTBOUNDARY_TEXT";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

    }
}

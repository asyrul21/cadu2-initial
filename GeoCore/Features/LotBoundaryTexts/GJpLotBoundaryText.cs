﻿using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.Core.Features.LotBoundaryTexts
{
    public class GJpLotBoundaryText : GLotBoundaryText
    {
        public const string TABLE_NAME = "JP_LOTBOUNDARY_TEXT";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }
    }
}

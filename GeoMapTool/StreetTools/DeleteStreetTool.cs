﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;
using Geomatic.MapTool.JunctionTools;

namespace Geomatic.MapTool.StreetTools
{
    public class DeleteStreetTool : DeleteTool
    {
        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        public DeleteStreetTool()
        {
            _name = "Delete Street";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a street to delete.");
            OnStepReported("Select a street to delete.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;
                    RepositoryFactory repo = new RepositoryFactory(SegmentName);
                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GStreet> streets;
                    List<GStreetAND> streetsAND;

                    GStreet street = null;
                    GStreetAND streetAND = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        streets = repo.SpatialSearch<GStreet>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                        streetsAND = repo.SpatialSearch<GStreetAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }
                    // AND USER
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDStreetTools andStreet = new ANDStreetTools();
                        switch (streetsAND.Count)
                        {
                            case int n when (n > 1):
                                streetAND = ChooseFeature(streetsAND, null);
                                andStreet.CheckWithinWorkArea(SegmentName, streetAND);
                                andStreet.CheckUserOwnWorkArea(SegmentName, streetAND);
                                break;

                            case int n when (n == 1):
                                streetAND = streetsAND[0];
                                andStreet.CheckWithinWorkArea(SegmentName, streetAND);
                                andStreet.CheckUserOwnWorkArea(SegmentName, streetAND);
                                break;

                            case int n when (n == 0):
                                if (streets.Count == 0) { return; }
                                else if (streets.Count == 1)
                                {
                                    street = streets[0];
                                }
                                else if (streets.Count > 1)
                                {
                                    street = ChooseFeature(streets, null);
                                }
                                andStreet.CheckWithinWorkArea(SegmentName, street);
                                andStreet.CheckUserOwnWorkArea(SegmentName, street);
                                break;
                        }
                    }
                    // OTHER USER GROUP
                    else
                    {
                        switch (streets.Count)
                        {
                            case int n when (n == 0):
                                break;

                            case int n when (n > 1):
                                street = ChooseFeature(streets, null);
                                break;

                            case int n when (n == 1):
                                street = streets[0];
                                break;
                        }

                        // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                        if (Session.User.GetGroup().Name != "SUPERVISOR")
                        {
                            //if (street.AreaId != null)
                            //{
                            //    throw new QualityControlException("feature is LOCK under AND Working Area.");
                            //}

                            // noraini ali - Aug 2021 - check feature lock by AND
                            ANDStreetTools andStreet = new ANDStreetTools();
                            andStreet.CheckFeatureLock(street.AreaId, SegmentName);
                        }
                        // noraini - Mac 2021 - Add rule - not to allow MLI (Supervisor) to delete feature AND verification 
                        else
                        {
                            GStreetAND StreetAnd = street.GetStreetANDId();
                            if (StreetAnd != null)
                            {
                                throw new Exception("Unable to edit feature without verification.");
                            }
                        }
                        // end add
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        if (Session.User.GetGroup().Name == "AND" && streetAND != null)
                        {
                            street = repo.GetById<GStreet>(streetAND.OriId);
                            highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                            highlightJunction(repo, streetAND.FromNodeId.Value, false, _selectedFeaturePoints);
                            highlightJunction(repo, streetAND.ToNodeId.Value, false, _selectedFeaturePoints);
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                            .SetDefaultButton(MessageBoxDefaultButton.Button2)
                            .SetCaption("Delete Street AND")
                            .SetText("Are you sure you want to delete this street AND?\nId: {0}\n{1} {2}", streetAND.OID, StringUtils.TrimSpaces(streetAND.TypeValue), streetAND.Name);
                        }
                        else
                        {
                            if (street != null)
                            {
                                highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                highlightJunction(repo, street.FromNodeId.Value, false, _selectedFeaturePoints);
                                highlightJunction(repo, street.ToNodeId.Value, false, _selectedFeaturePoints);
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                    .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                    .SetCaption("Delete Street")
                                    .SetText("Are you sure you want to delete this street?\nId: {0}\n{1} {2}", street.OID, StringUtils.TrimSpaces(street.TypeValue), street.Name);
                            }
                        }

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            string Message1 = $"Unable to delete Street ID {street.OID}.";
                            string MessageId = "";
                            MessageId = CheckStreetUnableToDelete(repo, street);
                            if (MessageId.Length > 0)
                            {
                                throw new QualityControlException(Message1 + MessageId);
                            }
                        }
                        else
                        {
                            if (street.HasProperty())
                            {
                                string Message1 = $"Unable to delete Street ID {street.OID} has property";
                                string MessageId = "\nList Property(s) ID :";

                                foreach (GProperty property in street.GetProperties())
                                {
                                    highlightProperty(repo, property.OID, false, _selectedFeaturePoints);
                                    MessageId = MessageId + "\n - " + property.OID;
                                }
                                throw new QualityControlException(Message1 + MessageId);
                            }

                            if (street.HasLandmark())
                            {
                                string Message1 = $"Unable to delete Street ID {street.OID} has Landmark";
                                string MessageId = "\nList Landmark(s) ID :";

                                foreach (GLandmark landmark in street.GetLandmarks())
                                {
                                    highlightLandmark(repo, landmark.OID, false, _selectedFeaturePoints);
                                    MessageId = MessageId + "\n - " + landmark.OID;
                                }
                                throw new QualityControlException(Message1 + MessageId);
                            }

                            if (street.HasBuildingGroup())
                            {

                                foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                {
                                    highlightBuildingGroup(repo, buildingGroup.OID, false, _selectedFeaturePoints);
                                }
                                throw new QualityControlException("Unable to delete street. Street has building group.");
                            }

                            if (street.HasRestriction())
                            {
                                throw new QualityControlException("Unable to delete street. Street has restriction.");
                            }

                            if (street.IsNavigationReady)
                            {
                                if (!Session.User.CanDo(Command.NavigationItem))
                                {
                                    throw new NavigationControlException();
                                }
                            }
                        }
                    }

                    repo.StartTransaction(() =>
                    {
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            //ANDStreetTools andStreet = new ANDStreetTools();
                            //andStreet.Delete(repo, street);

                            //noraini - Feb 2022 - Delete Junction
                            DeleteStreetByAND(repo, street);
                        }
                        else
                        {
                            GJunction fromNode = street.GetFromNode();
                            GJunction toNode = street.GetToNode();

                            foreach (GStreetText text in street.GetTexts())
                            {
                                repo.Delete(text);
                            }

                            OnReported("Street deleted. {0}", street.OID);
                            repo.Delete(street);

                            if (fromNode.Equals(toNode))
                            {

                                if (fromNode.CanDelete())
                                {
                                    int id = fromNode.OID;
                                    repo.Delete(fromNode);
                                    OnReported("Junction deleted. {0}", id);
                                }
                            }
                            else
                            {
                                if (fromNode.CanDelete())
                                {
                                    int id = fromNode.OID;
                                    repo.Delete(fromNode);
                                    OnReported("Junction deleted. {0}", id);
                                }
                                if (street.AndStatus == 1)
                                {
                                    int id = toNode.OID;
                                    repo.Delete(toNode);
                                    OnReported("Junction deleted. {0}", id);
                                }
                                else
                                {
                                    if (toNode.CanDelete())
                                    {
                                        int id = toNode.OID;
                                        repo.Delete(toNode);
                                        OnReported("Junction deleted. {0}", id);
                                    }
                                }
                            }
                        }
                    });

                    OnReported("Select street to delete.");
                    OnStepReported("Select street to delete.");
                }

                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                finally
                {
                    InProgress = false;
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
        }

        // noraini ali - Mac 2021
        private string CheckStreetUnableToDelete(RepositoryFactory repo, GStreet street)
        {
            String Message1 = "\nList Property(s) ID :";
            int len1 = Message1.Length;

            // Property belong to street
            foreach (GProperty property in street.GetProperties())
            {
                GPropertyAND propAND = property.GetPropertyANDId();
                if (propAND == null)
                {
                    highlightProperty(repo, property.OID, false, _selectedFeaturePoints);
                    Message1 = Message1 + "\n - " + property.OID;
                }
            }

            foreach (GPropertyAND propertyAND in street.GetPropertiesAND())
            {
                if (propertyAND.AndStatus != 2)
                {
                    highlightProperty(repo, propertyAND.OriId, false, _selectedFeaturePoints);
                    Message1 = Message1 + "\n - " + propertyAND.OriId;
                }
            }
            if (Message1.Length > len1)
            {
                return Message1;
            }

            // Landmark belong to street
            string Message2 = "\nList Landmark(s) ID :";
            int len2 = Message2.Length;
            foreach (GLandmark landmark in street.GetLandmarks())
            {
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                if (landmarkAND == null)
                {
                    highlightLandmark(repo, landmark.OID, false, _selectedFeaturePoints);
                    Message2 = Message2 + "\n - " + landmark.OID;
                }
            }

            foreach (GLandmarkAND landmarkAND in street.GetLandmarksAND())
            {
                if (landmarkAND.AndStatus != 2)
                {
                    highlightLandmark(repo, landmarkAND.OriId, false, _selectedFeaturePoints);
                    Message2 = Message2 + "\n - " + landmarkAND.OriId;
                }
            }
            if (Message2.Length > len2)
            {
                return Message2;
            }

            // Building Group belong to street
            string Message3 = "\nList Building Group(s) ID :";
            int len3 = Message3.Length;
            foreach (GBuildingGroup buildGrp in street.GetBuildingGroups())
            {
                GBuildingGroupAND buildGrpAND = buildGrp.GetBuildingGroupANDId();
                if (buildGrpAND == null)
                {
                    highlightLandmark(repo, buildGrp.OID, false, _selectedFeaturePoints);
                    Message3 = Message3 + "\n - " + buildGrp.OID;
                }
            }

            foreach (GBuildingGroupAND buildGrpAND in street.GetBuildingGroupsAND())
            {
                if (buildGrpAND.AndStatus != 2)
                {
                    highlightLandmark(repo, buildGrpAND.OriId, false, _selectedFeaturePoints);
                    Message3 = Message3 + "\n - " + buildGrpAND.OriId;
                }
            }
            if (Message3.Length > len3)
            {
                return Message3;
            }
            return "";
        }

        private string CheckStreetUnableToDelete(RepositoryFactory repo, GStreetAND street)
        {
            String Message1 = "\nList Property(s) ID :";
            int len1 = Message1.Length;

            foreach (GProperty property in street.GetProperties())
            {
                GPropertyAND propAND = property.GetPropertyANDId();
                if (propAND == null)
                {
                    highlightProperty(repo, property.OID, false, _selectedFeaturePoints);
                    Message1 = Message1 + "\n - " + property.OID;
                }
            }

            foreach (GPropertyAND propertyAND in street.GetPropertiesAND())
            {
                if (propertyAND.AndStatus != 2)
                {
                    highlightProperty(repo, propertyAND.OriId, false, _selectedFeaturePoints);
                    Message1 = Message1 + "\n - " + propertyAND.OriId;
                }
            }
            if (Message1.Length > len1)
            {
                return Message1;
            }

            // Landmark belong to street
            string Message2 = "\nList Landmark(s) ID :";
            int len2 = Message2.Length;

            foreach (GLandmark landmark in street.GetLandmarks())
            {
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                if (landmarkAND == null)
                {
                    highlightLandmark(repo, landmark.OID, false, _selectedFeaturePoints);
                    Message2 = Message2 + "\n - " + landmark.OID;
                }
            }

            foreach (GLandmarkAND landmarkAND in street.GetLandmarksAND())
            {
                if (landmarkAND.AndStatus != 2)
                {
                    highlightLandmark(repo, landmarkAND.OriId, false, _selectedFeaturePoints);
                    Message2 = Message2 + "\n - " + landmarkAND.OriId;
                }
            }

            // Building Group belong to street
            string Message3 = "\nList Building Group(s) ID :";
            int len3 = Message3.Length;

            foreach (GBuildingGroup buildGrp in street.GetBuildingGroups())
            {
                GBuildingGroupAND buildGrpAND = buildGrp.GetBuildingGroupANDId();
                if (buildGrpAND == null)
                {
                    highlightLandmark(repo, buildGrp.OID, false, _selectedFeaturePoints);
                    Message3 = Message3 + "\n - " + buildGrp.OID;
                }
            }

            foreach (GBuildingGroupAND buildGrpAND in street.GetBuildingGroupsAND())
            {
                if (buildGrpAND.AndStatus != 2)
                {
                    highlightLandmark(repo, buildGrpAND.OriId, false, _selectedFeaturePoints);
                    Message3 = Message3 + "\n - " + buildGrpAND.OriId;
                }
            }

            if (Message3.Length == len3)
            {
                return Message3;
            }
            return "";
        }

        // noraini ali - Feb 2022 - Delete street and junction
        private void DeleteStreetByAND(RepositoryFactory repo, GStreet street)
        {
            GJunction fromNode = street.GetFromNode();
            GJunction toNode = street.GetToNode();

            foreach (GStreetText text in street.GetTexts())
            {
                repo.Delete(text);
            }

            OnReported("Update status street to deleted. {0}", street.OID);
            ANDStreetTools andStreet = new ANDStreetTools();
            andStreet.Delete(repo, street);

            if (fromNode.Equals(toNode))
            {
                if (fromNode.CanDeleteUnionJunction())
                {
                    int id = fromNode.OID;
                    ANDJunctionTools andJunction = new ANDJunctionTools();
                    andJunction.Delete(repo, fromNode);
                    OnReported("Update status junction to deleted. {0}", id);
                }
            }
            else
            {
                if (fromNode.CanDeleteUnionJunction())
                {
                    int id = fromNode.OID;
                    ANDJunctionTools andJunction = new ANDJunctionTools();
                    andJunction.Delete(repo, fromNode);
                    OnReported("Update status junction to deleted. {0}", id);
                }
                if (toNode.CanDeleteUnionJunction())
                {
                    int id = toNode.OID;
                    ANDJunctionTools andJunction = new ANDJunctionTools();
                    andJunction.Delete(repo, toNode);
                    OnReported("Update status junction to deleted. {0}", id);
                }  
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
//using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using System.Diagnostics;

namespace Geomatic.MapTool.StreetTools
{
    public partial class EditStreetTool : EditTool
    {
        private CommandPool _commandPool;
        private string _selectMethod;
        private int  editStreet = 0;
        private const string messageDefault = "Select a street(s) to edit. Right click to change tracker method";
        private const string messagePolygon = "Fence street(s) to edit street section. Double click to end drawing. Right click to change tracker method";

        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        public EditStreetTool()
        {
            _name = "Edit Street";

            // added by noraini - Feb 2021
            InitMenu();
            InitCommand();
            OnRectangle(null, null);
            // added end

            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        // added by noraini - feb 2021
        private void InitCommand()
        {
            _commandPool = new CommandPool();

            _commandPool.Register(Command.Rectangle, menuRectangle);
            _commandPool.Register(Command.Polygon, menuPolygon);

            _commandPool.RegisterRadio(Command.SelectMethod,
                                                Command.Rectangle,
                                                Command.Polygon);
        }
        // added end

        public override void OnClick()
        {
            base.OnClick();
            //OnReported("Select a street(s) to edit.");
            //OnStepReported("Select a street(s) to edit.");
            OnReported(messageDefault);
            OnStepReported(messageDefault);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

             // added by noraini - Fen 2021
            if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(contextMenu, x, y);
            }

            // added end
            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    // added by asyrul
                    IGeometry geometry;
                    switch (_selectMethod)
                    {
                        case Command.Rectangle:
                            {
                                geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                editStreet = 1;
                                break;
                            }
                        case Command.Polygon:
                            {
                                geometry = new PolygonTracker().TrackNew(ActiveView, point);
                                editStreet = 2;
                                break;
                            }

                        default:
                            throw new Exception("Unknown draw method.");
                    }
                    // added end

                    //IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GStreet> streets;
                    GStreet street = null;

                    List<GStreetAND> streetsAND;
                    GStreetAND streetAND = null;

                    if (geometry == null)
                    {
                        return;
                    }

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        streets = repo.SpatialSearch<GStreet>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                        streetsAND = repo.SpatialSearch<GStreetAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    // added by noraini - if use fence - process to edit street section
                    if (editStreet == 0 || editStreet == 1)
                    {
                        int _workAreaId = 0;
                        // AND USER
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDStreetTools andStreet = new ANDStreetTools();
                            _workAreaId = andStreet.GetWorkAreaId(SegmentName, point);

                            streetsAND.RemoveAll(theStreet => theStreet.AndStatus == 2);

                            switch (streetsAND.Count)
                            {
                                case int n when (n > 1):
                                    streetAND = ChooseFeature(streetsAND, null);
                                    street = repo.GetById<GStreet>(streetAND.OriId);
                                    //Check if the street is good to go
                                    //DoChecking(street);
                                    break;

                                case int n when (n == 1):
                                    streetAND = streetsAND[0];
                                    street = repo.GetById<GStreet>(streetAND.OriId);
                                    //DoChecking(street);
                                    break;

                                case int n when (n == 0):
                                    if (streets.Count == 0) { return; }
                                    else if (streets.Count == 1)
                                    {
                                        street = streets[0];
                                        //Check if the street is good to go
                                        //DoChecking(street);
                                    }
                                    else if (streets.Count > 1)
                                    {
                                        street = ChooseFeature(streets, null);
                                        //Check if the street is good to go
                                        //DoChecking(street);
                                    }
                                    break;
                            }
                        }
                        //OTHER USER GROUP
                        else
                        {
                            switch (streets.Count)
                            {
                                case int n when (n == 0):
                                    break;

                                case int n when (n > 1):
                                    street = ChooseFeature(streets, null);
                                    break;

                                case int n when (n == 1):
                                    street = streets[0];
                                    break;
                            }
                        }

                        bool success = false;

                        try
                        {
                            using (new WaitCursor())
                            {
                                repo.StartTransaction();
                            }
                            // AND USER
                            bool _CanEdit = false;
                            ANDStreetTools andStreet = new ANDStreetTools();
                            if (_CanEdit = andStreet.ValidateUserCanEdit(SegmentName, _workAreaId))
                            {
                                if (street != null)
                                {
                                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                    highlightJunction(repo, street.FromNodeId.Value, false, _selectedFeaturePoints);
                                    highlightJunction(repo, street.ToNodeId.Value, false, _selectedFeaturePoints);
                                    andStreet.Update(repo, street);
                                }
                                else
                                {
                                    highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                                    highlightJunction(repo, streetAND.FromNodeId.Value, false, _selectedFeaturePoints);
                                    highlightJunction(repo, streetAND.ToNodeId.Value, false, _selectedFeaturePoints);
                                    andStreet.Update(repo, streetAND);
                                }
                            }
                            //OTHER USER GROUP
                            else
                            {
                                // noraini ali - NOV 2020
                                if (street == null)
                                { 
                                    andStreet.Update(repo, streetAND);
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                using (EditStreetForm form = new EditStreetForm(street))
                                {
                                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                    highlightJunction(repo, street.FromNodeId.Value, false, _selectedFeaturePoints);
                                    highlightJunction(repo, street.ToNodeId.Value, false, _selectedFeaturePoints);

                                    if (form.ShowDialog() != DialogResult.OK)
                                    {
                                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                        return;
                                    }

                                    //// noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                    //if (Session.User.GetGroup().Name != "SUPERVISOR" && street.AreaId != null)
                                    //{
                                    //    throw new QualityControlException("feature is LOCK under AND Working Area.");
                                    //}
                                    //// end add

                                    // noraini ali - Apr 2021 - check feature lock by user AND
                                    andStreet.CheckFeatureLock(street.AreaId, SegmentName);

                                    // noraini ali - Mei 2021 - check feature on verification
                                    andStreet.CheckFeatureOnVerification(street);

                                    using (new WaitCursor())
                                    {
                                        form.SetValues();
                                        repo.Update(street);

                                        if (!form.ShowText)
                                        {
                                            street.DeleteText();
                                        }
                                    }
                                }
                                OnReported("Street updated. {0}", street.OID);
                            }
                            //OnReported("Select a street to edit.");
                            //OnStepReported("Select a street to edit.");
                            OnReported(messageDefault);
                            OnStepReported(messageDefault);
                            success = true;
                        }
                        catch
                        {
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                            throw;
                        }
                        finally
                        {
                            if (success)
                            {
                                repo.EndTransaction();
                            }
                            else
                            {
                                repo.AbortTransaction();
                            }
                        }
                    }
                    else  // added by noraini - feb 2021 - fence to change street section use for supervisor
                    {
                        FenceToEditStreetSection(repo, streets);
                        OnReported(messageDefault);
                        OnStepReported(messageDefault);
                    }
                    // end

                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                finally
                {
                    InProgress = false;
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
        }

        private void DoChecking(GStreet street)
        {
            //Check if Street is within work area.
            ANDStreetTools andStreet = new ANDStreetTools();

            andStreet.CheckWithinWorkArea(SegmentName, street);
            andStreet.CheckUserOwnWorkArea(SegmentName, street);

            if (street.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    throw new NavigationControlException();
                }
            }
            if (street.HasRestriction())
            {
                throw new QualityControlException("Street has restriction");
            }
        }

        // added by noraini - Feb 2021
        private void OnRectangle(object sender, EventArgs e)
        {
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            _selectMethod = Command.Rectangle;
            _commandPool.Check(Command.Rectangle);
        }

        // added by noraini - Feb 2021
        private void OnPolygon(object sender, EventArgs e)
        {
            OnReported(messagePolygon);
            OnStepReported(messagePolygon);

            _selectMethod = Command.Polygon;
            _commandPool.Check(Command.Polygon);
        }

        // added by noraini - Feb 2021
        // function to edit street section by fence-Allow only for supervisor Id 
        private void FenceToEditStreetSection(RepositoryFactory repo, List<GStreet> streets)
        {
            if (Session.User.GetGroup().Name != "SUPERVISOR")
            {
                throw new Exception("This function ONLY allowed for Supervisor.");
            }

            int featureCount = streets.Count();
            //Checked if featureCOunt more than 500. if yes, then cannot proceed
            if (featureCount > 500)
            {
                Reset();
                throw new Exception("Please draw a smaller fence. Feature count exceed 500." + "\nStreet Count = " + featureCount);
            }

            foreach (GStreet Street in streets)
            {
                // Cannot proceed if feature lock by working Area 
                //if (Street.AreaId != null)
                //{
                //    throw new QualityControlException("Fail ... feature is LOCK under AND Working Area.");
                //}

                // noraini ali - Aug 2021 - check feature lock by AND
                ANDStreetTools andStreet = new ANDStreetTools();
                andStreet.CheckFeatureLock(Street.AreaId, SegmentName);
            }

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }

                OnStepReported("Total Selected street = " + featureCount);

                using (EditStreetForm2 form = new EditStreetForm2(streets[0]))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        repo.Update(streets[0]);
                    }

                    //creates and start the instance of Stopwatch
                    Stopwatch stopwatch = Stopwatch.StartNew();

                    // to update all street in fence
                    GStreet editStreetSection = repo.GetById<GStreet>(streets[0].OID);

                    int currentCount = 0;
                    using (UI.Forms.ProgressForm form1 = new UI.Forms.ProgressForm())
                    {
                       form1.setMinimun(0);
                       form1.setMaximum(featureCount);
                       form1.Show();
                   
                       streets.ForEach(street => {
                           //highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                           street.Section = editStreetSection.Section;
                           street.Postcode = editStreetSection.Postcode;
                           street.City = editStreetSection.City;
                           street.SubCity = editStreetSection.SubCity;
                           street.State = editStreetSection.State;
                           repo.Update(street);
                           currentCount = currentCount + 1;
                           form1.setValue(currentCount);
                       });
                    }

                    System.Threading.Thread.Sleep(500);
                    stopwatch.Stop();
                    Console.WriteLine(stopwatch.ElapsedMilliseconds/ 60000 + " minute");
                }

                MessageBox.Show("Update Street Section Done. Total Street Updated : " + streets.Count.ToString(), "Edit Street Section");
                success = true;
            }

            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }
    }
}
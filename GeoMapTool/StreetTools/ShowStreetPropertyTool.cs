﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Utilities;
using Earthworm.AO;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.Core.Sessions;
using Geomatic.MapTool.PropertyTools;

namespace Geomatic.MapTool.StreetTools
{
    public class ShowStreetPropertyTool : HighlightTool
    {
        #region Fields

        protected GStreet _selectedStreet;
        protected GStreetAND _selectedStreetAND;
        protected MoveLine _selectedStreetLine;
        protected List<MovePoint> _properties;

        protected List<MoveLine> _selectedFeatureLines;

        #endregion

        public ShowStreetPropertyTool()
        {
            _name = "Show Street Property";
            _properties = new List<MovePoint>();

            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, ColorUtils.Select, 2);
            }
            OnReported("Select a street to show property.");
            OnStepReported("Select a street to show property.");
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                if (button == MouseKey.Left)
                {
                    using (new WaitCursor())
                    {
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            List<GProperty> _NewListproperties = new List<GProperty>();
                            List<GPropertyAND> _NewListpropertiesAND = new List<GPropertyAND>();

                            List<GStreetAND> streetsAND = SelectStreetAND(repo, point);
                            List<GStreet> streets = SelectStreet(repo, point);
                            if (streets.Count == 0 && streetsAND.Count == 0)
                            {
                                _selectedStreet = null;
                                _selectedStreetAND = null;
                                _selectedStreetLine.Stop();
                                foreach (MovePoint movePoint in _properties)
                                {
                                    movePoint.Stop();
                                }
                                _properties.Clear();
                            }
                            if (streetsAND.Count > 0)
                            {
                                GStreetAND streetAND = streetsAND[0];
                                if (streetsAND.Count > 1)
                                {
                                    if (streetsAND[0].AndStatus != 2)
                                    {
                                        streetAND = streetsAND[0];
                                    }
                                    else if (streetsAND[1].AndStatus != 2)
                                    {
                                        streetAND = streetsAND[1];
                                    }
                                }
                                else
                                {
                                   streetAND = streetsAND[0];
                                }

                                highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);

                                if (_selectedStreetAND == null)
                                {
                                    _selectedStreetAND = streetAND;
                                    _selectedStreet = null;
                                    _selectedStreetLine.Stop();
                                    _selectedStreetLine.Line = _selectedStreetAND.Polyline;
                                    _selectedStreetLine.Start(point);
                                    foreach (MovePoint movePoint in _properties)
                                    {
                                        movePoint.Stop();
                                    }
                                    _properties.Clear();
                                    foreach (GPropertyAND property in _selectedStreetAND.GetPropertiesAND())
                                    {
                                        if (property.AndStatus != 2)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                            movePoint.Point = property.Point;
                                            movePoint.Start(point);
                                            _properties.Add(movePoint);
                                        }
                                    }
                                    foreach (GProperty property in _selectedStreetAND.GetProperties())
                                    {
                                        GPropertyAND propAND = property.GetPropertyANDId();
                                        if (propAND == null)
                                        {
                                            //if (property.AndStatus != 2 && property.AndStatus != 3)
                                            //{
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(point);
                                                _properties.Add(movePoint);
                                            //}
                                        }
                                    }
                                }
                                else
                                {
                                    if (!_selectedStreetAND.Equals(streetAND))
                                    {
                                        _selectedStreetAND = streetAND;
                                        _selectedStreet = null;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = _selectedStreetAND.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _properties)
                                        {
                                            movePoint.Stop();
                                        }
                                        _properties.Clear();

                                        foreach (GPropertyAND property in _selectedStreetAND.GetPropertiesAND())
                                        {
                                            if (property.AndStatus != 2)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(point);
                                                _properties.Add(movePoint);
                                            }
                                        }
                                        foreach (GProperty property in _selectedStreetAND.GetProperties())
                                        {
                                            GPropertyAND propAND = property.GetPropertyANDId();
                                            if (propAND == null)
                                            {
                                                //if (property.AndStatus != 2 && property.AndStatus != 3)
                                                //{
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                    movePoint.Point = property.Point;
                                                    movePoint.Start(point);
                                                    _properties.Add(movePoint);
                                                //}
                                            }
                                        }
                                    }
                                    else
                                    {
                                        _selectedStreetAND = streetAND;
                                        _selectedStreet = null;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = _selectedStreetAND.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _properties)
                                        {
                                            movePoint.Stop();
                                        }
                                        _properties.Clear();

                                        foreach (GPropertyAND property in _selectedStreetAND.GetPropertiesAND())
                                        {
                                            if (property.AndStatus != 2)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(point);
                                                _properties.Add(movePoint);
                                            }
                                        }
                                        foreach (GProperty property in _selectedStreetAND.GetProperties())
                                        {
                                            GPropertyAND propAND = property.GetPropertyANDId();
                                            if (propAND == null)
                                            {
                                                //if (property.AndStatus != 2 && property.AndStatus != 3)
                                                //{
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                    movePoint.Point = property.Point;
                                                    movePoint.Start(point);
                                                    _properties.Add(movePoint);
                                                //}
                                            }
                                        }
                                    }
                                }
                            }
                            else if (streets.Count > 0 && streetsAND.Count == 0)
                            {
                                GStreet street = streets[0];

                                highlightStreet(repo, street.OID, true, _selectedFeatureLines);

                                if (_selectedStreet == null)
                                {
                                    _selectedStreet = street;
                                    _selectedStreetAND = null;
                                    _selectedStreetLine.Stop();
                                    _selectedStreetLine.Line = _selectedStreet.Polyline;
                                    _selectedStreetLine.Start(point);
                                    foreach (MovePoint movePoint in _properties)
                                    {
                                        movePoint.Stop();
                                    }
                                    _properties.Clear();
                                    foreach (GProperty property in _selectedStreet.GetProperties())
                                    {
                                        GPropertyAND propAND = property.GetPropertyANDId();
                                        if (propAND == null)
                                        {
                                            //if (property.AndStatus != 2 && property.AndStatus != 3)
                                            //{
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(point);
                                                _properties.Add(movePoint);
                                            //}
                                        }
                                    }
                                    foreach (GPropertyAND property in _selectedStreet.GetPropertiesAND())
                                    {
                                        if (property.AndStatus != 2) {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                            movePoint.Point = property.Point;
                                            movePoint.Start(point);
                                            _properties.Add(movePoint);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!_selectedStreet.Equals(street))
                                    {
                                        _selectedStreet = street;
                                        _selectedStreetAND = null;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = _selectedStreet.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _properties)
                                        {
                                            movePoint.Stop();
                                        }
                                        _properties.Clear();
                                        foreach (GProperty property in _selectedStreet.GetProperties())
                                        {
                                            GPropertyAND propAND = property.GetPropertyANDId();
                                            if (propAND == null)
                                            {
                                                //if (property.AndStatus != 2 && property.AndStatus != 3)
                                                //{
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                    movePoint.Point = property.Point;
                                                    movePoint.Start(point);
                                                    _properties.Add(movePoint);
                                                //}
                                            }
                                        }
                                        foreach (GPropertyAND property in _selectedStreet.GetPropertiesAND())
                                        {
                                            if (property.AndStatus != 2)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(point);
                                                _properties.Add(movePoint);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        {
                                            _selectedStreet = street;
                                            _selectedStreetAND = null;
                                            _selectedStreetLine.Stop();
                                            _selectedStreetLine.Line = _selectedStreet.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (MovePoint movePoint in _properties)
                                            {
                                                movePoint.Stop();
                                            }
                                            _properties.Clear();
                                            foreach (GProperty property in _selectedStreet.GetProperties())
                                            {
                                                GPropertyAND propAND = property.GetPropertyANDId();
                                                if (propAND == null)
                                                {
                                                    //if (property.AndStatus != 2 && property.AndStatus != 3)
                                                    //{
                                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                        movePoint.Point = property.Point;
                                                        movePoint.Start(point);
                                                        _properties.Add(movePoint);
                                                    //}
                                                }
                                            }
                                            foreach (GPropertyAND property in _selectedStreet.GetPropertiesAND())
                                            {
                                                if (property.AndStatus != 2)
                                                {
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                                    movePoint.Point = property.Point;
                                                    movePoint.Start(point);
                                                    _properties.Add(movePoint);
                                                }
                                            }
                                        }
                                    }
                                }
                            } 
                        }
                        else
                        {
                            List<GStreet> streets = SelectStreet(repo, point);
                            if (streets.Count > 0)
                            {
                                GStreet street = streets[0];
                                if (_selectedStreet == null)
                                {
                                    _selectedStreet = street;
                                    _selectedStreetLine.Line = _selectedStreet.Polyline;
                                    _selectedStreetLine.Start(point);
                                    foreach (GProperty property in _selectedStreet.GetProperties())
                                    {
                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                        movePoint.Point = property.Point;
                                        movePoint.Start(point);
                                        _properties.Add(movePoint);
                                    }
                                }
                                else
                                {
                                    if (!_selectedStreet.Equals(street))
                                    {
                                        _selectedStreet = street;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = _selectedStreet.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _properties)
                                        {
                                            movePoint.Stop();
                                        }
                                        _properties.Clear();
                                        foreach (GProperty property in _selectedStreet.GetProperties())
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                            movePoint.Point = property.Point;
                                            movePoint.Start(point);
                                            _properties.Add(movePoint);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _selectedStreet = null;
                                _selectedStreetLine.Stop();
                                foreach (MovePoint movePoint in _properties)
                                {
                                    movePoint.Stop();
                                }
                                _properties.Clear();
                            }
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        private List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetAND>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedStreet = null;
            _selectedStreetAND = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
            foreach (MovePoint movePoint in _properties)
            {
                movePoint.Stop();
            }
            _properties.Clear();
        }

        public override bool Deactivate()
        {
            _selectedStreet = null;
            _selectedStreetAND = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            foreach (MovePoint movePoint in _properties)
            {
                movePoint.Stop();
            }
            _properties.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
            if (_properties != null)
            {
                foreach (MovePoint movePoint in _properties)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }
    }
}

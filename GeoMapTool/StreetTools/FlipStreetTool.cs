﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Sessions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Earthworm.AO;
using Geomatic.UI.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Commands;
using Geomatic.MapTool.JunctionTools;

namespace Geomatic.MapTool.StreetTools
{
    public partial class FlipStreetTool : Tool
    {
        #region Fields

        private GStreet _selectedStreet;
        protected MoveLine _selectedStreetLine;

        #endregion

        public FlipStreetTool()
        {
            _name = "Flip Street";
            InitMenu();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported("Select a street.");
            OnStepReported("Select a street.");
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                if (button == MouseKey.Left)
                {
                    using (new WaitCursor())
                    {
                        List<GStreet> streets = SelectStreet(repo, point);
                        if (streets.Count > 0)
                        {
                            GStreet street = streets[0];
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                ANDStreetTools andStreet = new ANDStreetTools();
                                andStreet.CheckWithinWorkArea(SegmentName, street);
                                andStreet.CheckUserOwnWorkArea(SegmentName, street);
                            }
                            else
                            {
                                // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                //if (Geomatic.Core.Sessions.Session.User.GetGroup().Name != "SUPERVISOR")
                                //{
                                //    if (street.AreaId != null)
                                //    {
                                //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                                //    }
                                //}

                                // noraini ali - Aug 2021 - check feature lock by AND
                                ANDStreetTools andStreet = new ANDStreetTools();
                                andStreet.CheckFeatureLock(street.AreaId, SegmentName);
                                // end add
                            }

                            _selectedStreet = street;
                            _selectedStreetLine.Stop();
                            _selectedStreetLine.Line = _selectedStreet.Polyline;
                            _selectedStreetLine.Start(point);
                            OnReported("Right click menu to flip.");
                            OnStepReported("Right click menu to flip.");
                        }
                        else
                        {
                            _selectedStreet = null;
                            _selectedStreetLine.Stop();
                            OnReported("Select a street.");
                            OnStepReported("Select a street.");
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    if (_selectedStreet == null)
                    {
                        MapDocument.ShowContextMenu(x, y);
                        return;
                    }

                    List<GStreet> streets;
                    using (new WaitCursor())
                    {
                        streets = SelectStreet(repo, point);
                    }
                    if (streets.Count > 0)
                    {
                        GStreet street = streets[0];
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDStreetTools andStreet = new ANDStreetTools();
                            andStreet.CheckWithinWorkArea(SegmentName, street);
                            andStreet.CheckUserOwnWorkArea(SegmentName, street);
                        }
                            if (street.Equals(_selectedStreet))
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                    }
                    else
                    {
                        MapDocument.ShowContextMenu(x, y);
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        private void OnFlip(object sender, EventArgs e)
        {
            if (_selectedStreet == null)
            {
                return;
            }

            using (new WaitCursor())
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                if (_selectedStreet.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }
                IPolyline line = _selectedStreet.Polyline.Clone();
                line.ReverseOrientation();

                GJunction fromNode = _selectedStreet.GetFromNode();
                GJunction toNode = _selectedStreet.GetToNode();
                _selectedStreet.FromNodeId = toNode.OID;
                _selectedStreet.ToNodeId = fromNode.OID;
                _selectedStreet.Shape = line;

                repo.StartTransaction(() =>
                {
                    if (Session.User.GetGroup().Name == "AND") //check if AND User
                    {
                        ANDStreetTools andStreet = new ANDStreetTools();
                        GStreetAND _selectedStreetAND = _selectedStreet.GetStreetANDId(); //check if street already exist in AND Table
                        if (_selectedStreetAND == null) //streetAND not yet exist in AND Table
                        {
                            //insert current street to AND street
                            andStreet.CreateStreetAND(repo, _selectedStreet);

                            // Get FromNode and ToNode (Junction)
                            GJunction FromNodeADM = _selectedStreet.GetFromNode();
                            GJunction ToNodeADM = _selectedStreet.GetToNode();

                            //Insert Junction AND
                            ANDJunctionTools andJunction = new ANDJunctionTools();
                            andJunction.CreateJunctionAND(repo, FromNodeADM);
                            andJunction.CreateJunctionAND(repo, ToNodeADM);

                            //Get Inserted Street AND and Junction AND
                            _selectedStreetAND = _selectedStreet.GetStreetANDId();
                            GJunctionAND FromNodeAND = FromNodeADM.GetJunctionANDId();
                            GJunctionAND ToNodeAND = ToNodeADM.GetJunctionANDId();

                            //Update the Street AND to the new Junction AND
                            _selectedStreetAND.FromNodeId = FromNodeAND.OID;
                            _selectedStreetAND.ToNodeId = ToNodeAND.OID;
                            repo.UpdateGraphic(_selectedStreetAND,true);

                            //flipped the inserted AND street
                            andStreet.Flip(repo, _selectedStreet);
                        }
                        else //streetAND exist in AND Table
                        {
                            andStreet.Flip(repo, _selectedStreet); //flipped the existing AND street
                        }  
                    }
                    else //not an AND User
                    {
                        repo.UpdateGraphic(_selectedStreet,true);
                    }
                    OnReported("Street flipped. {0}", _selectedStreet.OID);
                });
            }
        }

        protected override void Reset()
        {
            _selectedStreet = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
        }

        public override bool Deactivate()
        {
            _selectedStreet = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Earthworm.AO;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.MapTool.JunctionTools;
using Geomatic.MapTool.Properties;

namespace Geomatic.MapTool.StreetTools
{
    public partial class AddStreetTool : AddTool
    {
        #region Fields

        protected MovePolygon _area;
        protected NewLine _newLine;
        protected IRgbColor _color = ColorUtils.Get(Color.Green);
        private bool _panStarted;
        private int _panningHandle;

        private const string Message1 = "Start draw street. Middle click to pan. Control + click to end draw.";
        private const string Message2 = "Middle click to pan. Control + click to end draw. Right click for menu.";

        public override int Cursor
        {
            get
            {
                if (_panStarted)
                {
                    return _panningHandle;
                }
                else
                {
                    return base.Cursor;
                }
            }
        }

        #endregion

        public AddStreetTool()
        {
            _name = "Add Street";
            InitMenu();
            _panningHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPanning);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_area == null)
            {
                _area = new MovePolygon(ScreenDisplay, _color, _color, 1);
            }
            if (_newLine == null)
            {
                _newLine = new NewLine(ScreenDisplay, _color, 1.4);
            }
            OnReported(Message1);
            OnStepReported(Message1);
            _panStarted = false;
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    return;
                }
                ScreenDisplay.PanStart(point);
                _panStarted = true;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (!_area.IsStarted)
            {
                _area.Polygon = (IPolygon)point.Buffer(5);
                _area.Start(point);
            }
            _area.MoveTo(point);
            _newLine.MoveTo(point);
            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    ScreenDisplay.PanMoveTo(point);
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                if (button == MouseKey.Left)
                {
                    if (!InProgress)
                    {
                        List<GJunction> junctions;
                        using (new WaitCursor())
                        {
                            junctions = repo.SpatialSearch<GJunction>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        }

                        if (junctions.Count == 0)
                        {
                            _newLine.Start(point);
                        }
                        else if (junctions.Count == 1)
                        {
                            _newLine.Start(junctions[0].Point);
                            _newLine.MoveTo(point);
                        }
                        else if (junctions.Count > 1)
                        {
                            throw new QualityControlException("Found multiple junctions.");
                        }
                        InProgress = true;
                        OnReported(Message2);
                        OnStepReported(Message2);
                    }
                    else
                    {
                        _newLine.AddPoint(point);
                        if (shift == KeyCode.Ctrl)
                        {
                            try
                            {
                                _newLine.Stop();
                                if (Session.User.GetGroup().Name != "AND")
                                {
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    andStreet.checkingPointInWorkArea(SegmentName, point);
                                }
                                CreateStreet();
                            }
                            catch
                            {
                                throw;
                            }
                            finally
                            {
                                InProgress = false;
                            }
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    if (!InProgress)
                    {
                        MapDocument.ShowContextMenu(x, y);
                    }
                    else
                    {
                        MapDocument.ShowContextMenu(contextMenu, x, y);
                    }
                }
                else if (button == MouseKey.Middle)
                {
                    if (_panStarted)
                    {
                        ScreenDisplay.PanStop();
                        _panStarted = false;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        private void OnUndoLast(object sender, EventArgs e)
        {
            if (_newLine.PointCount >= 2)
            {
                _newLine.RemoveLastPoint();
            }
        }

        private void OnRedraw(object sender, EventArgs e)
        {
            if (!InProgress)
            {
                return;
            }
            _newLine.Stop();
            InProgress = false;
            OnReported(Message1);
            OnStepReported(Message1);
        }

        protected virtual void CreateStreet()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            GJunction fromNode = null;
            GJunction toNode = null;
            GStreet newStreet = null;
            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();

                    List<GJunction> junctions;

                    junctions = repo.SpatialSearch<GJunction>(_newLine.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                    if (junctions.Count == 0)
                    {
                        // Create Junction
                        GJunction newJunction = repo.NewObj<GJunction>();
                        newJunction.Init();
                        newJunction.Shape = _newLine.FromPoint;

                        //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDJunctionTools andJunction = new ANDJunctionTools();
                            andJunction.UpdateCreatedByUserAND(newJunction);
                            andJunction.UpdateModifiedByUserAND(newJunction);
                        }

                        fromNode = repo.Insert(newJunction);

                        // noraini ali - Apr 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDJunctionTools andJunction = new ANDJunctionTools();

                            andJunction.CheckWithinWorkArea(SegmentName, fromNode);
                            andJunction.CheckUserOwnWorkArea(SegmentName, fromNode);

                            andJunction.AddJunctionAND(repo, fromNode);
                        }
                        // end
                    }
                    else if (junctions.Count == 1)
                    {
                        fromNode = junctions[0];
                        _newLine.FromPoint = fromNode.Point;
                    }
                    else
                    {
                        throw new QualityControlException("Found multiple junctions.");
                    }

                    junctions = repo.SpatialSearch<GJunction>(_newLine.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                    if (junctions.Count == 0)
                    {
                        // Create Junction
                        GJunction newJunction = repo.NewObj<GJunction>();
                        newJunction.Init();
                        newJunction.Shape = _newLine.ToPoint;

                        //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDJunctionTools andJunction = new ANDJunctionTools();
                            andJunction.UpdateCreatedByUserAND(newJunction);
                            andJunction.UpdateModifiedByUserAND(newJunction);
                        }

                        toNode = repo.Insert(newJunction);

                        // noraini ali - Apr 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDJunctionTools andJunction = new ANDJunctionTools();

                            andJunction.CheckWithinWorkArea(SegmentName, toNode);
                            andJunction.CheckUserOwnWorkArea(SegmentName, toNode);

                            andJunction.AddJunctionAND(repo, toNode);
                        }
                        // end
                    }
                    else if (junctions.Count == 1)
                    {
                        toNode = junctions[0];
                        _newLine.ToPoint = toNode.Point;
                    }
                    else
                    {
                        throw new QualityControlException("Found multiple junctions.");
                    }

                    if (_newLine.Length < 5)
                    {
                        throw new QualityControlException("Unable to create street with length lesser than 5 meter.");
                    }

                    newStreet = repo.NewObj<GStreet>();
                    newStreet.Init();
                    newStreet.FromNodeId = fromNode.OID;
                    newStreet.ToNodeId = toNode.OID;
                    if (Settings.Default.AddStreetName != "" || Settings.Default.AddStreetSection != ""
                        || Settings.Default.AddStreetCity != "" || Settings.Default.AddStreetState != ""
                        || Settings.Default.AddStreetPostcode != "")
                    {
                        using (QuestionMessageBox box = new QuestionMessageBox())
                        {
                            box.SetButtons(MessageBoxButtons.YesNo);
                            box.SetCaption("Keep previous data");
                            box.SetText("Do you want to maintain the existing attributes?");
                            box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                            if (box.Show() == DialogResult.Yes)
                            {
                                newStreet.Type = Settings.Default.AddStreetType;
                                newStreet.Name = Settings.Default.AddStreetName;
                                newStreet.Section = Settings.Default.AddStreetSection;
                                newStreet.City = Settings.Default.AddStreetCity;
                                newStreet.State = Settings.Default.AddStreetState;
                                newStreet.Postcode = Settings.Default.AddStreetPostcode;
                            }
                            else
                            {
                                Settings.Default.AddStreetType = 1;
                                Settings.Default.AddStreetName = "";
                                Settings.Default.AddStreetSection = "";
                                Settings.Default.AddStreetCity = "";
                                Settings.Default.AddStreetState = "";
                                Settings.Default.AddStreetPostcode = "";
                            }
                        }
                    }

                    newStreet.Length = _newLine.Length;
                    newStreet.Shape = _newLine.Geometry;

                    //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDStreetTools andStreet = new ANDStreetTools();
                        andStreet.UpdateCreatedByUserAND(newStreet);
                        andStreet.UpdateModifiedByUserAND(newStreet);
                    }

                    newStreet = repo.Insert(newStreet, false);
                }

                using (AddStreetForm form = new AddStreetForm(newStreet))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        repo.UpdateRemainStatus(newStreet,true);

                        //Syafiq start - April 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDStreetTools andStreet = new ANDStreetTools();

                            // Check for User Own Work Area & Activity Within Work Area 
                            andStreet.CheckWithinWorkArea(SegmentName, newStreet);
                            andStreet.CheckUserOwnWorkArea(SegmentName, newStreet);

                            // create Street AND
                            andStreet.AddStreetAND(repo, newStreet);

                            //Get AND fromNode, if not exist need to create AND junction
                            GStreetAND StreetAND = newStreet.GetStreetANDId();
                            //repo.Update(StreetAND);
                        }
                        //Syafiq end - April 2020

                        // added by asyrul
                        if (!form.ShowText)
                        {
                            newStreet.DeleteText();
                        }
                        // added end

                        OnReported("New street created. {0}", newStreet.OID);

                        Settings.Default.AddStreetType = newStreet.Type.GetValueOrDefault();
                        Settings.Default.AddStreetName = newStreet.Name;
                        Settings.Default.AddStreetSection = newStreet.Section;
                        Settings.Default.AddStreetCity = newStreet.City;
                        Settings.Default.AddStreetState = newStreet.State;
                        Settings.Default.AddStreetPostcode = newStreet.Postcode;

                        OnReported(Message1);
                        OnStepReported(Message1);
                    }
                }

                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        protected override void Reset()
        {
            if (InProgress)
            {
                if (_newLine != null)
                {
                    _newLine.Stop();
                }
                InProgress = false;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
        }

        public override bool Deactivate()
        {
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }
            if (_newLine != null)
            {
                _newLine.Stop();
                _newLine = null;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
            if (_newLine != null)
            {
                _newLine.Refresh(hDC);
            }
        }
    }
}

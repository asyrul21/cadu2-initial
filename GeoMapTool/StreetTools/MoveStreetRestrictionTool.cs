﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool.StreetTools
{
    public class MoveStreetRestrictionTool : MoveTool
    {
        #region Fields

        protected GStreetRestriction _selectedStreetRestriction;
        protected MoveLine _selectedStreetRestrictionLine;
        private const string Message1 = "Select a street restriction to move.";
        private const string Message2 = "Drag selected landmark to move.";

        #endregion

        public MoveStreetRestrictionTool()
        {
            _name = "Move Street Restriction";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedStreetRestrictionLine == null)
            {
                _selectedStreetRestrictionLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Intersects(_selectedStreetRestriction.Polyline))
                            {
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedStreetRestrictionLine.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreetRestriction> streetRestrictions = SelectStreetRestriction(repo, point);
                                if (streetRestrictions.Count > 0)
                                {
                                    GStreetRestriction streetRestriction = streetRestrictions[0];
                                    _selectedStreetRestrictionLine.Line = streetRestriction.Polyline;
                                    _selectedStreetRestrictionLine.Start(point);
                                    _selectedStreetRestriction = streetRestriction;

                                    _progress = Progress.TryMove;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreetRestriction> streetRestrictions = SelectStreetRestriction(repo, point);
                                if (streetRestrictions.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedStreetRestrictionLine.Stop();
                                    _selectedStreetRestriction = null;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                GStreetRestriction streetRestriction = streetRestrictions[0];
                                if (!_selectedStreetRestriction.Equals(streetRestriction))
                                {
                                    _selectedStreetRestrictionLine.Line = streetRestriction.Polyline;
                                    _selectedStreetRestrictionLine.Stop();
                                    _selectedStreetRestrictionLine.Start(point);
                                    _selectedStreetRestriction = streetRestriction;
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }
                            IPolyline newPolyline;

                            using (new WaitCursor())
                            {
                                newPolyline = (IPolyline)_selectedStreetRestrictionLine.Stop();
                                _progress = Progress.Select;

                                repo.StartTransaction(() =>
                                {
                                    _selectedStreetRestriction.Shape = newPolyline;
                                    repo.Update(_selectedStreetRestriction);
                                });
                                _selectedStreetRestriction = null;
                            }
                            OnReported(Message1);
                            OnStepReported(Message1);
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreetRestriction> SelectStreetRestriction(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetRestriction>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_selectedStreetRestrictionLine != null)
            {
                _selectedStreetRestrictionLine.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_selectedStreetRestrictionLine != null)
            {
                _selectedStreetRestrictionLine.Stop();
                _selectedStreetRestrictionLine = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetRestrictionLine != null)
            {
                _selectedStreetRestrictionLine.Refresh(hDC);
            }
        }
    }
}

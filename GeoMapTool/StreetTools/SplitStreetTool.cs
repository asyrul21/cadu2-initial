﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using Earthworm.AO;
using Geomatic.MapTool.JunctionTools;
using System.Windows.Forms;
using Geomatic.MapTool.BuildingGroupTools;
using Geomatic.MapTool.LandmarkTools;
using Geomatic.MapTool.PropertyTools;

namespace Geomatic.MapTool.StreetTools
{
    public class SplitStreetTool : Tool
    {
        private enum Progress
        {
            Select = 0,
            TrySplit
        }

        #region Fields

        private Progress _progress;
        protected MovePolygon _area;
        protected MoveLine _selectedStreetLine;
        protected GStreet _selectedStreet;
        protected GStreetAND _selectedStreetAND;
        protected IRgbColor _color = ColorUtils.Get(Color.Green);

        private const string Message1 = "Select a street.";
        private const string Message2 = "Click on a point to split.";

        #endregion

        public SplitStreetTool()
        {
            _name = "Split Street";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_area == null)
            {
                _area = new MovePolygon(ScreenDisplay, _color, _color, 1);
            }
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TrySplit:
                        _area.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GStreet> streets = SelectStreet(repo, point);
                                    List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        ANDStreetTools andStreet = new ANDStreetTools();
                                        if (streetsAND.Count > 0)
                                        {
                                            GStreetAND street = streetsAND[0];
                                            //Check within work area
                                            andStreet.CheckWithinWorkArea(SegmentName, street);
                                            //Check user own the work area
                                            andStreet.CheckUserOwnWorkArea(SegmentName, street);
                                            //Check Property, Landmark and BuildingGroup associate to street within work area.
                                            ANDPropertyTools andProperty = new ANDPropertyTools();
                                            ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                            foreach (GProperty property in street.GetProperties())
                                            {
                                                andProperty.CheckWithinWorkArea(SegmentName, property);
                                            }
                                            foreach (GLandmark landmark in street.GetLandmarks())
                                            {
                                                andLandmark.CheckWithinWorkArea(SegmentName, landmark);
                                            }
                                            foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                            {
                                                andBuildingGroup.CheckWithinWorkArea(SegmentName, buildingGroup);
                                            }

                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            _selectedStreetAND = street;

                                            _area.Polygon = (IPolygon)point.Buffer(5);
                                            _area.Start(point);

                                            _progress = Progress.TrySplit;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        if (streets.Count > 0)
                                        {
                                            GStreet street = streets[0];
                                            //Check within work area
                                            andStreet.CheckWithinWorkArea(SegmentName, street);
                                            //Check user own the work area
                                            andStreet.CheckUserOwnWorkArea(SegmentName, street);
                                            //Check Property, Landmark and BuildingGroup associate to street within work area.
                                            ANDPropertyTools andProperty = new ANDPropertyTools();
                                            ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                            foreach (GProperty property in street.GetProperties())
                                            {
                                                andProperty.CheckWithinWorkArea(SegmentName, property);
                                            }
                                            foreach (GLandmark landmark in street.GetLandmarks())
                                            {
                                                andLandmark.CheckWithinWorkArea(SegmentName, landmark);
                                            }
                                            foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                            {
                                                andBuildingGroup.CheckWithinWorkArea(SegmentName, buildingGroup);
                                            }

                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            _selectedStreet = street;

                                            _area.Polygon = (IPolygon)point.Buffer(5);
                                            _area.Start(point);

                                            _progress = Progress.TrySplit;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            _selectedStreet = null;
                                            _selectedStreetLine.Stop();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                    }
                                    else
                                    {
                                        if (streets.Count > 0)
                                        {
                                            GStreet street = streets[0];
                                            // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                            //if (Session.User.GetGroup().Name != "SUPERVISOR")
                                            //{
                                            //    if (street.AreaId != null)
                                            //    {
                                            //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                                            //    }
                                            //
                                            //}

                                            // noraini ali - Aug 2021 - check feature lock by AND
                                            ANDStreetTools andStreet = new ANDStreetTools();
                                            andStreet.CheckFeatureLock(street.AreaId, SegmentName);

                                            //end add
                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            _selectedStreet = street;

                                            _area.Polygon = (IPolygon)point.Buffer(5);
                                            _area.Start(point);

                                            _progress = Progress.TrySplit;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            _selectedStreet = null;
                                            _selectedStreetLine.Stop();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                    }

                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TrySplit:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GStreet> streets = SelectStreet(repo, point);
                                    List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                    //AND USER
                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        ANDStreetTools andStreet = new ANDStreetTools();
                                        ANDJunctionTools andJunction = new ANDJunctionTools();

                                        //ADM not exist, AND not exist
                                        if (streets.Count == 0 && streetsAND.Count == 0)
                                        {
                                            _area.Stop();
                                            _selectedStreetLine.Stop();
                                            _progress = Progress.Select;
                                            _selectedStreet = null;
                                            _selectedStreetAND = null;
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }
                                        //AND exist
                                        if (_selectedStreetAND != null && streetsAND.Count > 0)
                                        {
                                            int streetIndex = streetsAND.IndexOf(_selectedStreetAND);
                                            GStreetAND streetAND = streetsAND[streetIndex];
                                            if (!_selectedStreetAND.Equals(streetAND))
                                            {
                                                _selectedStreetLine.Stop();
                                                _selectedStreetLine.Line = streetAND.Polyline;
                                                _selectedStreetLine.Start(point);
                                                _selectedStreetAND = streetAND;
                                                OnReported(Message2);
                                                OnStepReported(Message2);
                                                break;
                                            }

                                            GStreet streetSplitOutput = null;
                                            List<GJunctionAND> junctions;

                                            junctions = repo.SpatialSearch<GJunctionAND>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                            if (junctions.Count > 0)
                                            {
                                                using (QuestionMessageBox box = new QuestionMessageBox())
                                                {
                                                    box.SetButtons(MessageBoxButtons.YesNo);
                                                    box.SetCaption("Split Street");
                                                    box.SetText("Split Street on selected junction, proceed?");
                                                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                                    if (box.Show() != DialogResult.Yes)
                                                    {
                                                        throw new QualityControlException("Found junction within 5 meter.");
                                                    }
                                                }
                                            }

                                            if (_selectedStreet.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            if (_selectedStreet.HasRestriction())
                                            {
                                                throw new QualityControlException("Street has restriction");
                                            }
                                            repo.StartTransaction(() =>
                                            {
                                                GStreet street = repo.GetById<GStreet>(streetAND.OriId);

                                                // Split
                                                GStreet streetForSplit = repo.NewObj<GStreet>();
                                                streetForSplit.Init();
                                                streetForSplit.CopyFrom(street);
                                                streetForSplit.AreaId = street.AreaId;
                                                streetForSplit.FromNodeId = street.FromNodeId;
                                                streetForSplit.ToNodeId = street.ToNodeId;
                                                streetForSplit.Length = street.Length;
                                                streetForSplit.Shape = street.Shape;
                                                streetForSplit = repo.Insert(streetForSplit, false);

                                                // update Created and Modified user
                                                andStreet.UpdateCreatedByUserAND(streetForSplit);
                                                andStreet.UpdateModifiedByUserAND(streetForSplit);
                                                repo.UpdateInsertByAND(streetForSplit, true);

                                                if (streetForSplit.TrySplitAtPoint(point, out streetSplitOutput))
                                                {
                                                    // new Street
                                                    streetSplitOutput.AreaId = streetForSplit.AreaId;
                                                    SetJunctionAND(repo, streetSplitOutput);
                                                    double newLen = streetSplitOutput.Length.Value;

                                                    streetSplitOutput.CopyFrom(streetForSplit);
                                                    streetSplitOutput.AreaId = streetForSplit.AreaId;
                                                    streetSplitOutput.NavigationStatus = 0;
                                                    streetSplitOutput.Length = newLen; // overwrite value street length from CopyFrom

                                                    // new street, need to update Created and Modified user
                                                    andStreet.UpdateCreatedByUserAND(streetSplitOutput);
                                                    andStreet.UpdateModifiedByUserAND(streetSplitOutput);
                                                    repo.UpdateInsertByAND(streetSplitOutput,true);

                                                    // street
                                                    SetJunctionAND(repo, streetForSplit);
                                                    streetForSplit.NavigationStatus = 0;
                                                    repo.UpdateInsertByAND(streetForSplit,true);

                                                    // add streetSplitOutput street to AND
                                                    andStreet.CreateStreetAND(repo, streetSplitOutput);
                                                    GJunction streetSplitOutputFromNodeADM = streetSplitOutput.GetFromNode();
                                                    GJunction streetSplitOutputToNodeADM = streetSplitOutput.GetToNode();
                                                    GStreetAND streetSplitOutputAND = streetSplitOutput.GetStreetANDId();
                                                    streetSplitOutputAND.FromNodeId = streetSplitOutputFromNodeADM.OID;
                                                    streetSplitOutputAND.ToNodeId = streetSplitOutputToNodeADM.OID;

                                                    andStreet.UpdateModifiedByUserAND(streetSplitOutputAND);
                                                    repo.UpdateRemainStatus(streetSplitOutputAND,true);

                                                    // add streetForSplit street to AND
                                                    andStreet.CreateStreetAND(repo, streetForSplit);
                                                    GJunction streetForSplitFromNodeADM = streetForSplit.GetFromNode();
                                                    GJunction streetForSplitToNodeADM = streetForSplit.GetToNode();
                                                    GStreetAND streetForSplitAND = streetForSplit.GetStreetANDId();
                                                    streetForSplitAND.FromNodeId = streetForSplitFromNodeADM.OID;
                                                    streetForSplitAND.ToNodeId = streetForSplitToNodeADM.OID;
                                                    andStreet.UpdateModifiedByUserAND(streetForSplitAND);
                                                    repo.UpdateRemainStatus(streetForSplitAND,true);

                                                    // Update -> set property, landmark, and buildingGroup to nearer street
                                                    foreach (GProperty property in street.GetProperties())
                                                    {
                                                        if (property.Shape.DistanceTo(streetSplitOutput.Shape) < property.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GPropertyAND propertyAND = property.GetPropertyANDId();
                                                            if (propertyAND != null)
                                                            {
                                                                propertyAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(propertyAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                                                andProperty.CreatePropertyAND(repo, property);
                                                                GPropertyAND insertedPropertyAND = property.GetPropertyANDId();
                                                                insertedPropertyAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(insertedPropertyAND, true);
                                                                repo.UpdateByAND(property, true);
                                                            }
                                                        }
                                                        if (property.Shape.DistanceTo(streetForSplit.Shape) < property.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GPropertyAND propertyAND = property.GetPropertyANDId();
                                                            if (propertyAND != null)
                                                            {
                                                                propertyAND.StreetId = streetForSplit.OID;
                                                                repo.Update(propertyAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                                                andProperty.CreatePropertyAND(repo, property);
                                                                GPropertyAND insertedPropertyAND = property.GetPropertyANDId();
                                                                insertedPropertyAND.StreetId = streetForSplit.OID;
                                                                repo.Update(insertedPropertyAND,true);
                                                                repo.UpdateByAND(property, true);
                                                            }
                                                        }
                                                    }
                                                    foreach (GLandmark landmark in street.GetLandmarks())
                                                    {
                                                        if (landmark.Shape.DistanceTo(streetSplitOutput.Shape) < landmark.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                                                            if (landmarkAND != null)
                                                            {
                                                                landmarkAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(landmarkAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                                                andLandmark.CreateLandmarkAND(repo, landmark);
                                                                GLandmarkAND insertedLandmarkAND = landmark.GetLandmarkANDId();
                                                                insertedLandmarkAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(insertedLandmarkAND, true);
                                                                repo.UpdateByAND(landmark, true);
                                                            }
                                                        }
                                                        if (landmark.Shape.DistanceTo(streetForSplit.Shape) < landmark.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                                                            if (landmarkAND != null)
                                                            {
                                                                landmarkAND.StreetId = streetForSplit.OID;
                                                                repo.Update(landmarkAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                                                andLandmark.CreateLandmarkAND(repo, landmark);
                                                                GLandmarkAND insertedLandmarkAND = landmark.GetLandmarkANDId();
                                                                insertedLandmarkAND.StreetId = streetForSplit.OID;
                                                                repo.Update(insertedLandmarkAND,true);
                                                                repo.UpdateByAND(landmark, true);
                                                            }
                                                        }
                                                    }
                                                    foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                                    {
                                                        if (buildingGroup.Shape.DistanceTo(streetSplitOutput.Shape) < buildingGroup.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                            if (buildingGroupAND != null)
                                                            {
                                                                buildingGroupAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(buildingGroupAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                                                andBuildingGroup.CreateBuildingGroupAND(repo, buildingGroup);
                                                                GBuildingGroupAND insertedBuildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                                insertedBuildingGroupAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(insertedBuildingGroupAND,true);
                                                                repo.UpdateByAND(buildingGroup, true);
                                                            }
                                                        }
                                                        if (buildingGroup.Shape.DistanceTo(streetForSplit.Shape) < buildingGroup.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                            if (buildingGroupAND != null)
                                                            {
                                                                buildingGroupAND.StreetId = streetForSplit.OID;
                                                                repo.Update(buildingGroupAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                                                andBuildingGroup.CreateBuildingGroupAND(repo, buildingGroup);
                                                                GBuildingGroupAND insertedBuildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                                insertedBuildingGroupAND.StreetId = streetForSplit.OID;
                                                                repo.Update(buildingGroup, true);
                                                                repo.UpdateByAND(insertedBuildingGroupAND, true);
                                                            }
                                                        }
                                                    }
                                                }

                                                // Delete Street
                                                andStreet.Delete(repo, street);

                                                ShowSuccessMessage();
                                            });
                                            _progress = Progress.Select;
                                            _selectedStreetLine.Stop();
                                            _area.Stop();
                                            _selectedStreet = null;
                                            _selectedStreetAND = null;
                                            OnReported("New street created. {0}", streetSplitOutput.OID);
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                        //ADM exist, AND does not exist
                                        else if (_selectedStreet != null && streets.Count > 0)
                                        {
                                            //GStreet street = streets[0];
                                            int streetIndex = streets.IndexOf(_selectedStreet);
                                            GStreet street = streets[streetIndex];
                                            if (!_selectedStreet.Equals(street))
                                            {
                                                _selectedStreetLine.Stop();
                                                _selectedStreetLine.Line = street.Polyline;
                                                _selectedStreetLine.Start(point);
                                                _selectedStreet = street;
                                                OnReported(Message2);
                                                OnStepReported(Message2);
                                                break;
                                            }

                                            GStreet streetSplitOutput = null;
                                            List<GJunction> junctions;

                                            junctions = repo.SpatialSearch<GJunction>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            if (junctions.Count > 0)
                                            {
                                                using (QuestionMessageBox box = new QuestionMessageBox())
                                                {
                                                    box.SetButtons(MessageBoxButtons.YesNo);
                                                    box.SetCaption("Split Street");
                                                    box.SetText("Split Street on selected junction, proceed?");
                                                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                                    if (box.Show() != DialogResult.Yes)
                                                    {
                                                        throw new QualityControlException("Found junction within 5 meter.");
                                                    }
                                                }
                                            }

                                            if (_selectedStreet.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            if (_selectedStreet.HasRestriction())
                                            {
                                                throw new QualityControlException("Street has restriction");
                                            }
                                            repo.StartTransaction(() =>
                                            {
                                                // Split
                                                GStreet streetForSplit = repo.NewObj<GStreet>();
                                                streetForSplit.Init();
                                                streetForSplit.CopyFrom(street);
                                                streetForSplit.AreaId = street.AreaId;
                                                streetForSplit.FromNodeId = street.FromNodeId;
                                                streetForSplit.ToNodeId = street.ToNodeId;
                                                streetForSplit.Length = street.Length;
                                                streetForSplit.Shape = street.Shape;
                                                streetForSplit = repo.Insert(streetForSplit,false); 

                                                // new street, need to update Created and Modified user
                                                andStreet.UpdateCreatedByUserAND(streetForSplit);
                                                andStreet.UpdateModifiedByUserAND(streetForSplit);
                                                repo.UpdateInsertByAND(streetForSplit, true);

                                                if (streetForSplit.TrySplitAtPoint(point, out streetSplitOutput))
                                                {
                                                    // new Street
                                                    streetSplitOutput.AreaId = streetForSplit.AreaId;  // set WA
                                                    SetJunctionAND(repo, streetSplitOutput);
                                                    double newLen = streetSplitOutput.Length.Value;

                                                    streetSplitOutput.CopyFrom(streetForSplit);
                                                    streetSplitOutput.NavigationStatus = 0;
                                                    streetSplitOutput.Length = newLen; // overwrite value length from CopyFrom
                                                    streetSplitOutput.AreaId = streetForSplit.AreaId;
                                                    // new street, need to update Created and Modified user
                                                    andStreet.UpdateCreatedByUserAND(streetSplitOutput);
                                                    andStreet.UpdateModifiedByUserAND(streetSplitOutput);

                                                    repo.UpdateInsertByAND(streetSplitOutput,true); 

                                                    // street
                                                    SetJunctionAND(repo, streetForSplit);
                                                    streetForSplit.NavigationStatus = 0;
                                                    //repo.UpdateRemainStatus(streetForSplit,true);
                                                    repo.UpdateInsertByAND(streetForSplit, true);

                                                    // add streetSplitOutput street to AND
                                                    andStreet.CreateStreetAND(repo, streetSplitOutput); 
                                                    GJunction streetSplitOutputFromNodeADM = streetSplitOutput.GetFromNode();
                                                    GJunction streetSplitOutputToNodeADM = streetSplitOutput.GetToNode();
                                                    GStreetAND streetSplitOutputAND = streetSplitOutput.GetStreetANDId();
                                                    streetSplitOutputAND.FromNodeId = streetSplitOutputFromNodeADM.OID;
                                                    streetSplitOutputAND.ToNodeId = streetSplitOutputToNodeADM.OID;
                                                    // new street, need to update Created and Modified user
                                                    andStreet.UpdateCreatedByUserAND(streetSplitOutputAND);
                                                    andStreet.UpdateModifiedByUserAND(streetSplitOutputAND);
                                                    repo.UpdateRemainStatus(streetSplitOutputAND,true);

                                                    // add streetForSplit street to AND
                                                    andStreet.CreateStreetAND(repo, streetForSplit); 
                                                    GJunction streetForSplitFromNodeADM = streetForSplit.GetFromNode();
                                                    GJunction streetForSplitToNodeADM = streetForSplit.GetToNode();

                                                    GStreetAND streetForSplitAND = streetForSplit.GetStreetANDId();
                                                    streetForSplitAND.FromNodeId = streetForSplitFromNodeADM.OID;
                                                    streetForSplitAND.ToNodeId = streetForSplitToNodeADM.OID;

                                                    // new street, need to update Created and Modified user
                                                    andStreet.UpdateCreatedByUserAND(streetForSplitAND);
                                                    andStreet.UpdateModifiedByUserAND(streetForSplitAND);
                                                    repo.UpdateRemainStatus(streetForSplitAND,true); 

                                                    // Update -> set property, landmark, and buildingGroup to nearer street
                                                    foreach (GProperty property in street.GetProperties())
                                                    {
                                                        if (property.Shape.DistanceTo(streetSplitOutput.Shape) < property.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GPropertyAND propertyAND = property.GetPropertyANDId();
                                                            if (propertyAND != null)
                                                            {
                                                                propertyAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(propertyAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                                                andProperty.CreatePropertyAND(repo, property);
                                                                GPropertyAND insertedPropertyAND = property.GetPropertyANDId();

                                                                insertedPropertyAND.StreetId = streetSplitOutput.OID;
                                                                insertedPropertyAND.CreatedBy = property.CreatedBy;
                                                                insertedPropertyAND.DateCreated = property.DateCreated;
                                                                andProperty.UpdateModifiedByUserAND(insertedPropertyAND);
                                                                repo.Update(insertedPropertyAND,true);

                                                                repo.UpdateByAND(property, true);
                                                            }
                                                        }
                                                        if (property.Shape.DistanceTo(streetForSplit.Shape) < property.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GPropertyAND propertyAND = property.GetPropertyANDId();
                                                            if (propertyAND != null)
                                                            {
                                                                propertyAND.StreetId = streetForSplit.OID;
                                                                repo.Update(propertyAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                                                andProperty.CreatePropertyAND(repo, property);
                                                                GPropertyAND insertedPropertyAND = property.GetPropertyANDId();
                                                                insertedPropertyAND.StreetId = streetForSplit.OID;

                                                                // Property AND - maintain created as adm feature, update by User AND
                                                                insertedPropertyAND.CreatedBy = property.CreatedBy;
                                                                insertedPropertyAND.DateCreated = property.DateCreated;
                                                                andProperty.UpdateModifiedByUserAND(insertedPropertyAND);
                                                                repo.Update(insertedPropertyAND,true);
                                                                repo.UpdateByAND(property, true);
                                                            }    
                                                        }
                                                    }
                                                    foreach (GLandmark landmark in street.GetLandmarks())
                                                    {
                                                        if (landmark.Shape.DistanceTo(streetSplitOutput.Shape) < landmark.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                                                            if (landmarkAND != null)
                                                            {
                                                                landmarkAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(landmarkAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                                                andLandmark.CreateLandmarkAND(repo, landmark);
                                                                GLandmarkAND insertedLandmarkAND = landmark.GetLandmarkANDId();
                                                                insertedLandmarkAND.StreetId = streetSplitOutput.OID;

                                                                // Landmark AND - maintain created as adm feature, update by User AND
                                                                insertedLandmarkAND.CreatedBy = landmark.CreatedBy;
                                                                insertedLandmarkAND.DateCreated = landmark.DateCreated;
                                                                andLandmark.UpdateModifiedByUserAND(insertedLandmarkAND);
                                                                repo.Update(insertedLandmarkAND, true);
                                                                repo.UpdateByAND(landmark, true);
                                                            }    
                                                        }
                                                        if (landmark.Shape.DistanceTo(streetForSplit.Shape) < landmark.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                                                            if (landmarkAND != null)
                                                            {
                                                                landmarkAND.StreetId = streetForSplit.OID;
                                                                repo.Update(landmarkAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                                                andLandmark.CreateLandmarkAND(repo, landmark);
                                                                GLandmarkAND insertedLandmarkAND = landmark.GetLandmarkANDId();
                                                                insertedLandmarkAND.StreetId = streetForSplit.OID;

                                                                // Landmark AND - maintain created as adm feature, update by User AND
                                                                insertedLandmarkAND.CreatedBy = landmark.CreatedBy;
                                                                insertedLandmarkAND.DateCreated = landmark.DateCreated;
                                                                andLandmark.UpdateModifiedByUserAND(insertedLandmarkAND);
                                                                repo.Update(insertedLandmarkAND,true);
                                                                repo.UpdateByAND(landmark, true);
                                                            }   
                                                        }
                                                    }
                                                    foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                                    {
                                                        if (buildingGroup.Shape.DistanceTo(streetSplitOutput.Shape) < buildingGroup.Shape.DistanceTo(streetForSplit.Shape))
                                                        {
                                                            GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                            if (buildingGroupAND != null)
                                                            {
                                                                buildingGroupAND.StreetId = streetSplitOutput.OID;
                                                                repo.Update(buildingGroupAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                                                andBuildingGroup.CreateBuildingGroupAND(repo, buildingGroup);
                                                                GBuildingGroupAND insertedBuildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                                insertedBuildingGroupAND.StreetId = streetSplitOutput.OID;

                                                                // Landmark AND - maintain created as adm feature, update by User AND
                                                                insertedBuildingGroupAND.CreatedBy = buildingGroup.CreatedBy;
                                                                insertedBuildingGroupAND.DateCreated = buildingGroup.DateCreated;
                                                                andBuildingGroup.UpdateModifiedByUserAND(insertedBuildingGroupAND);
                                                                repo.Update(insertedBuildingGroupAND,true);
                                                                repo.UpdateByAND(buildingGroup, true);
                                                            }   
                                                        }
                                                        if (buildingGroup.Shape.DistanceTo(streetForSplit.Shape) < buildingGroup.Shape.DistanceTo(streetSplitOutput.Shape))
                                                        {
                                                            GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                            if (buildingGroupAND != null)
                                                            {
                                                                buildingGroupAND.StreetId = streetForSplit.OID;
                                                                repo.Update(buildingGroupAND,true);
                                                            }
                                                            else
                                                            {
                                                                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                                                andBuildingGroup.CreateBuildingGroupAND(repo, buildingGroup);
                                                                GBuildingGroupAND insertedBuildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                                                                insertedBuildingGroupAND.StreetId = streetForSplit.OID;

                                                                // Building Group AND - maintain created as adm feature, update by User AND
                                                                insertedBuildingGroupAND.CreatedBy = buildingGroup.CreatedBy;
                                                                insertedBuildingGroupAND.DateCreated = buildingGroup.DateCreated;
                                                                andBuildingGroup.UpdateModifiedByUserAND(insertedBuildingGroupAND);
                                                                repo.Update(insertedBuildingGroupAND,true);
                                                                repo.UpdateByAND(buildingGroup, true);
                                                            }   
                                                        }
                                                    }
                                                }

                                                // Delete Street
                                                andStreet.Delete(repo, street);

                                                ShowSuccessMessage();
                                            });
                                            _progress = Progress.Select;
                                            _selectedStreetLine.Stop();
                                            _area.Stop();
                                            _selectedStreet = null;
                                            _selectedStreetAND = null;
                                            OnReported("New street created. {0}", streetSplitOutput.OID);
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }

                                    }
                                    else //group others user
                                    {
                                        if (streets.Count == 0)
                                        {
                                            _area.Stop();
                                            _selectedStreetLine.Stop();
                                            _progress = Progress.Select;
                                            _selectedStreet = null;
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }

                                        int streetIndex = streets.IndexOf(_selectedStreet);
                                        GStreet street = streets[streetIndex];
                                        if (!_selectedStreet.Equals(street))
                                        {
                                            _selectedStreetLine.Stop();
                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            _selectedStreet = street;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }

                                        GStreet newStreet = null;

                                        List<GJunction> junctions;

                                        junctions = repo.SpatialSearch<GJunction>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                        if (junctions.Count > 0)
                                        {
                                            using (QuestionMessageBox box = new QuestionMessageBox())
                                            {
                                                box.SetButtons(MessageBoxButtons.YesNo);
                                                box.SetCaption("Split Street");
                                                box.SetText("Split Street on selected junction, proceed?");
                                                box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                                if (box.Show() != DialogResult.Yes)
                                                {
                                                    throw new QualityControlException("Found junction within 5 meter.");
                                                }
                                            }
                                        }

                                        if (_selectedStreet.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }

                                        if (_selectedStreet.HasRestriction())
                                        {
                                            throw new QualityControlException("Street has restriction");
                                        }

                                        repo.StartTransaction(() =>
                                        {
                                            if (street.TrySplitAtPoint(point, out newStreet))
                                            {
                                                // new Street
                                                SetJunction(repo, newStreet);
                                                double newlen = newStreet.Length.Value; // keep new street length
                                                newStreet.CopyFrom(street);
                                                newStreet.NavigationStatus = 0;
                                                newStreet.Length = newlen;  // set new street length
                                                repo.Update(newStreet);

                                                // street
                                                SetJunction(repo, street);
                                                street.NavigationStatus = 0;

                                                repo.Update(street);

                                                // set property and landmark to nearer street
                                                foreach (GProperty property in street.GetProperties())
                                                {
                                                    if (property.Shape.DistanceTo(newStreet.Shape) < property.Shape.DistanceTo(street.Shape))
                                                    {
                                                        property.StreetId = newStreet.OID;
                                                        repo.Update(property);
                                                    }
                                                }

                                                foreach (GLandmark landmark in street.GetLandmarks())
                                                {
                                                    if (landmark.Shape.DistanceTo(newStreet.Shape) < landmark.Shape.DistanceTo(street.Shape))
                                                    {
                                                        landmark.StreetId = newStreet.OID;
                                                        repo.Update(landmark);
                                                    }
                                                }

                                                foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
                                                {
                                                    if (buildingGroup.Shape.DistanceTo(newStreet.Shape) < buildingGroup.Shape.DistanceTo(street.Shape))
                                                    {
                                                        buildingGroup.StreetId = newStreet.OID;
                                                        repo.Update(buildingGroup);
                                                    }
                                                }

                                                ShowSuccessMessage();
                                            }
                                        });
                                        _progress = Progress.Select;
                                        _selectedStreetLine.Stop();
                                        _area.Stop();
                                        _selectedStreet = null;
                                        OnReported("New street created. {0}", newStreet.OID);
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        private List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetAND>(query).ToList();
        }

        private void SetJunction(RepositoryFactory repo, GStreet street)
        {
            GJunction fromNode = null;
            GJunction toNode = null;

            List<GJunction> junctions;

            junctions = repo.SpatialSearch<GJunction>(street.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

            if (junctions.Count == 0)
            {
                // Create Junction
                GJunction newJunction = repo.NewObj<GJunction>();
                newJunction.Init();
                newJunction.Shape = street.FromPoint;
                fromNode = repo.Insert(newJunction);
            }
            else if (junctions.Count == 1)
            {
                fromNode = junctions[0];
                street.FromPoint = fromNode.Point;
            }
            else
            {
                throw new QualityControlException("Found multiple junctions.");
            }

            junctions = repo.SpatialSearch<GJunction>(street.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

            if (junctions.Count == 0)
            {
                // Create Junction
                GJunction newJunction = repo.NewObj<GJunction>();
                newJunction.Init();
                newJunction.Shape = street.ToPoint;
                toNode = repo.Insert(newJunction);
            }
            else if (junctions.Count == 1)
            {
                toNode = junctions[0];
                street.ToPoint = toNode.Point;
            }
            else
            {
                throw new QualityControlException("Found multiple junctions.");
            }

            street.FromNodeId = fromNode.OID;
            street.ToNodeId = toNode.OID;
        }

        private void SetJunctionAND(RepositoryFactory repo, GStreet street)
        {
            GJunction fromNode = null;
            GJunction toNode = null;

            List<GJunction> junctions;

            junctions = repo.SpatialSearch<GJunction>(street.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

            if (junctions.Count == 0)
            {
                // Create Junction
                GJunction newJunction = repo.NewObj<GJunction>();
                newJunction.Init();
                newJunction.Shape = street.FromPoint;
                newJunction.AreaId = street.AreaId; // set WA
                fromNode = repo.Insert(newJunction);
                ANDJunctionTools andJunction = new ANDJunctionTools();
                andJunction.UpdateCreatedByUserAND(fromNode);
                andJunction.UpdateModifiedByUserAND(fromNode);
                repo.UpdateInsertByAND(fromNode, true);

                // Create AND Junction
                // Add to AND Junction
                andJunction.CreateJunctionAND(repo, fromNode);
                GJunctionAND fromNodeAND = fromNode.GetJunctionANDId();
                andJunction.UpdateCreatedByUserAND(fromNodeAND);
                andJunction.UpdateModifiedByUserAND(fromNodeAND);
                repo.UpdateRemainStatus(fromNodeAND, true);
            }
            else if (junctions.Count == 1)
            {
                fromNode = junctions[0];
                street.FromPoint = fromNode.Point;
            }
            else
            {
                throw new QualityControlException("Found multiple junctions.");
            }

            junctions = repo.SpatialSearch<GJunction>(street.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

            if (junctions.Count == 0)
            {
                // Create Junction
                GJunction newJunction = repo.NewObj<GJunction>();
                newJunction.Init();
                newJunction.Shape = street.ToPoint;
                newJunction.AreaId = street.AreaId; // set WA
                toNode = repo.Insert(newJunction);
                ANDJunctionTools andJunction = new ANDJunctionTools();
                andJunction.UpdateCreatedByUserAND(toNode);
                andJunction.UpdateModifiedByUserAND(toNode);
                repo.UpdateInsertByAND(toNode, true);

                // Add to AND Junction
                andJunction.CreateJunctionAND(repo, toNode);
                GJunctionAND toNodeAND = toNode.GetJunctionANDId();
                andJunction.UpdateCreatedByUserAND(toNodeAND);
                andJunction.UpdateModifiedByUserAND(toNodeAND);
                repo.UpdateRemainStatus(toNodeAND, true);
            }
            else if (junctions.Count == 1)
            {
                toNode = junctions[0];
                street.ToPoint = toNode.Point;
            }
            else
            {
                throw new QualityControlException("Found multiple junctions.");
            }

            street.FromNodeId = fromNode.OID;
            street.ToNodeId = toNode.OID;
        }

        private void ShowSuccessMessage()
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Split Street Successful");
                box.Show();
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            _selectedStreet = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
            if (_area != null)
            {
                _area.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            _selectedStreet = null;
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
        }
    }
}

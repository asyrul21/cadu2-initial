﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public partial class AddStreetRestrictionTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuEnd;
        private ToolStripMenuItem menuCancel;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuEnd = new ToolStripMenuItem();
            this.menuCancel = new ToolStripMenuItem();
            // 
            // menuEnd
            //
            this.menuEnd.Name = "menuEnd";
            this.menuEnd.Text = "End";
            this.menuEnd.Click += new EventHandler(this.OnEnd);
            // 
            // menuCancel
            //
            this.menuCancel.Name = "menuCancel";
            this.menuCancel.Text = "Cancel";
            this.menuCancel.Click += new EventHandler(this.OnCancel);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuEnd,
            this.menuCancel});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

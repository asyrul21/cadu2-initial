﻿// created by asyrul on the 28th January 2019
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Earthworm.AO;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;
using Geomatic.MapTool.JunctionTools;

namespace Geomatic.MapTool.StreetTools
{
    public partial class CopyStreetAttributeTool : HighlightTool
    {
        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        private enum Progress
        {
            SelectSourceAttributeStreet = 0,
            SelectDestinationStreetAndTryCopy,
            Copying
        }

        #region Fields

        private Progress _progress;
        private GStreet _selectedStreetAttributeSource;
        private GStreet _selectedDestinationStreet;

        private const string Message1 = "Select the source attribute Street.";
        private const string Message2 = "Select the street to copy attributes to.";
        private const string Message3 = "Copying attributes to street...";

        #endregion

        public CopyStreetAttributeTool()
        {
            _name = "Copy Street Attribute";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y) // SelectDestinationStreetAndTryCopy
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.SelectSourceAttributeStreet:
                        break;
                    case Progress.SelectDestinationStreetAndTryCopy:
                        if (button != 1)
                        {
                            break;
                        }
                        using (new WaitCursor())
                        {
                            Console.WriteLine("Mouse DOWN on Selectdestinationstreet and try Copy!");

                            //get newly clicked street
                            List<GStreet> streets = SelectStreet(repo, point);
                            if (streets.Count == 0)
                            {
                                //go back
                                _progress = Progress.SelectSourceAttributeStreet; 
                                _selectedStreetAttributeSource = null;
                                OnReported(Message2);
                                OnStepReported(Message2);
                                break;
                            }

                            GStreet destinationstreet = streets[0];
                            highlightStreet(repo, destinationstreet.OID, false, _selectedFeatureLines);
                            highlightJunction(repo, destinationstreet.FromNodeId.Value, true, _selectedFeaturePoints);
                            highlightJunction(repo, destinationstreet.ToNodeId.Value, true, _selectedFeaturePoints);

                            if (Session.User.GetGroup().Name == "AND")
                            {
                                ANDStreetTools andStreet = new ANDStreetTools();
                                andStreet.CheckWithinWorkArea(SegmentName, destinationstreet);
                                andStreet.CheckUserOwnWorkArea(SegmentName, destinationstreet);
                            }
                            if (!_selectedStreetAttributeSource.Equals(destinationstreet))
                                //if source attribute street is not equal as destination street
                            {
                                //Console.WriteLine("Street name: " + destinationstreet.Name);
                                //Console.WriteLine("Street city: " + destinationstreet.City);
                                //Console.WriteLine("Street section: " + destinationstreet.Section);
                                //Console.WriteLine("Street state: " + destinationstreet.State);

                                //set destination street as the newly clicked
                                _selectedDestinationStreet = destinationstreet;
                                _progress = Progress.Copying;

                                // set message
                                OnReported(Message3);
                                OnStepReported(Message3);
                                break;
                            }

                            //if user picks the same street as the source
                            _progress = Progress.SelectDestinationStreetAndTryCopy;
                        }
                        break;
                    case Progress.Copying:
                        Console.WriteLine("Mouse DOWN on Copying!");
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch(Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
            finally
            {
                InProgress = false;
            }

        }

        public override void OnMouseUp(int button, int shift, int x, int y) // SelectSourceAttributeStreet, Copying
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.SelectSourceAttributeStreet:
                        if (button == MouseKey.Left)
                        {
                            Console.WriteLine("Mouse up on select Source!");
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                if (streets.Count > 0)
                                {
                                    GStreet sourcestreet = streets[0];
                                    highlightStreet(repo, sourcestreet.OID, true, _selectedFeatureLines);
                                    highlightJunction(repo, sourcestreet.FromNodeId.Value, false, _selectedFeaturePoints);
                                    highlightJunction(repo, sourcestreet.ToNodeId.Value, false, _selectedFeaturePoints);
                                    _selectedStreetAttributeSource = sourcestreet;
                                    _progress = Progress.SelectDestinationStreetAndTryCopy;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;

                    case Progress.SelectDestinationStreetAndTryCopy:
                        //does nothing, runs when user selects same destination as the source
                        Console.WriteLine("Mouse UP on Try copy! - Only runs if user selects same " +
                                          "destination as the source");

                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;

                    case Progress.Copying:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            Console.WriteLine("Mouse UP on Copying! - Trying to copy success");
                            _progress = Progress.SelectSourceAttributeStreet;

                            using (QuestionMessageBox box = new QuestionMessageBox())
                            {
                                box.SetButtons(MessageBoxButtons.YesNo);
                                box.SetCaption("Copy street attribute");
                                box.SetText("Confirm to copy street attribute to selected street?");
                                box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                if (box.Show() != DialogResult.Yes)
                                {
                                    throw new ProcessCancelledException();
                                }
                            }

                            repo.StartTransaction(() =>
                            {
                                if (Session.User.GetGroup().Name == "AND") //check if AND User
                                {
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    GStreetAND _selectedDestinationStreetAND = _selectedDestinationStreet.GetStreetANDId(); //check if street already exist in AND Table
                                    if (_selectedDestinationStreetAND == null) //street not yet exist in AND Table
                                    {
                                        //insert current street to AND street
                                        andStreet.CreateStreetAND(repo, _selectedDestinationStreet);
                                        _selectedDestinationStreetAND = _selectedDestinationStreet.GetStreetANDId();

                                        // Get FromNode and ToNode (Junction)
                                        GJunction FromNodeADM = _selectedDestinationStreet.GetFromNode();
                                        GJunction ToNodeADM = _selectedDestinationStreet.GetToNode();

                                        //Insert Junction AND
                                        ANDJunctionTools andJunction = new ANDJunctionTools();
                                        andJunction.CreateJunctionAND(repo, FromNodeADM);
                                        andJunction.CreateJunctionAND(repo, ToNodeADM);

                                        //Get Inserted Street AND and Junction AND

                                        GJunctionAND FromNodeAND = FromNodeADM.GetJunctionANDId();
                                        GJunctionAND ToNodeAND = ToNodeADM.GetJunctionANDId();

                                        //Update the Street AND to the new Junction AND
                                        _selectedDestinationStreetAND.FromNodeId = FromNodeAND.OID;
                                        _selectedDestinationStreetAND.ToNodeId = ToNodeAND.OID;
                                        repo.Update(_selectedDestinationStreetAND);

                                        andStreet.CopyStreetAttribute(repo, _selectedDestinationStreet, _selectedStreetAttributeSource);
                                        
                                    }
                                    else //street exist in AND Table
                                    {
                                        andStreet.CopyStreetAttribute(repo, _selectedDestinationStreet, _selectedStreetAttributeSource);
                                    }
                                }
                                else //not an AND User
                                {
                                    _selectedDestinationStreet.CopyFrom(_selectedStreetAttributeSource);
                                    repo.Update(_selectedDestinationStreet);
                                }

                                //OnReported("Copy street attibute success.");
                                MessageBox.Show("Copy street attibute success.");
                                OnReported(Message1);
                                OnStepReported(Message1);

                                // refernce from CopyStreetTool

                                //        GStreet newStreet = repo.NewObj<GStreet>();
                                //        newStreet.CopyFrom(_selectedStreet);

                                //        newStreet.FromNodeId = fromNode.OID;
                                //        newStreet.ToNodeId = toNode.OID;
                                //        newStreet.NavigationStatus = 0;
                                //        newStreet.Shape = newLine;
                                //        newStreet = repo.Insert(newStreet);

                                //        OnReported("New street created. {0}", newStreet.OID);
                                //        OnReported("Select a street.");
                                //        OnStepReported("Select a street.");
                            });

                            _selectedStreetAttributeSource = null;
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.SelectSourceAttributeStreet;
            _selectedStreetAttributeSource = null;
            _selectedDestinationStreet = null;
        }

        public override bool Deactivate()
        {
            _progress = Progress.SelectSourceAttributeStreet;
            _selectedStreetAttributeSource = null;
            _selectedDestinationStreet = null;

            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
        }
    }
}

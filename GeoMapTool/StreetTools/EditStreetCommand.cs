﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.StreetTools
{
    public partial class EditStreetTool
    {
        class Command
        {
            public const string SelectMethod = "SelectMethod";
            public const string Polygon = "Fence";
            public const string Rectangle = "Single";
            public const string NavigationItem = "NavigationItem";
        }
    }
}

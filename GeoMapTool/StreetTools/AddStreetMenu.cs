﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public partial class AddStreetTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuUndoLast;
        private ToolStripMenuItem menuRedraw;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuUndoLast = new ToolStripMenuItem();
            this.menuRedraw = new ToolStripMenuItem();
            // 
            // menuUndoLast
            //
            this.menuUndoLast.Name = "menuUndoLast";
            this.menuUndoLast.Text = "Undo Last";
            this.menuUndoLast.Click += new EventHandler(this.OnUndoLast);
            // 
            // menuRedraw
            //
            this.menuRedraw.Name = "menuRedraw";
            this.menuRedraw.Text = "Redraw";
            this.menuRedraw.Click += new EventHandler(this.OnRedraw);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuUndoLast,
            this.menuRedraw});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

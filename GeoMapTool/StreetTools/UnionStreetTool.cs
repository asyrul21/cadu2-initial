﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Earthworm.AO;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using System.Windows.Forms;
using Geomatic.UI.Commands;
using Geomatic.MapTool.JunctionTools;
using Geomatic.MapTool.PropertyTools;
using Geomatic.MapTool.LandmarkTools;
using Geomatic.MapTool.BuildingGroupTools;

namespace Geomatic.MapTool.StreetTools
{
    public class UnionStreetTool : HighlightTool
    {
        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        #region Fields

        private int _pencilHandle;

        protected MoveLine _selectedStreetLine;
        private const string Message1 = "Select a street.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                return _pencilHandle;
            }
        }

        #endregion

        public UnionStreetTool()
        {
            _name = "Union Street";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                if (button == MouseKey.Left)
                {
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        //ADM
                        GStreet street = null, nearbyStreet = null;
                        List<GStreet> streets, nearbyStreets;
                        GJunction fromNode, toNode;
                        //AND
                        GStreetAND streetAND = null, nearbyStreetAND = null;
                        List<GStreetAND> streetsAND, nearbyStreetsAND;

                        ANDStreetTools andStreet = new ANDStreetTools();
                        ANDJunctionTools andJunction = new ANDJunctionTools();

                        using (new WaitCursor())
                        {
                            streets = SelectStreet(repo, point);
                            streetsAND = SelectStreetAND(repo, point);

                            streetsAND.RemoveAll(theStreet => theStreet.AndStatus == 2);
                            streets.RemoveAll(theStreet => theStreet.AndStatus == 2);
                            Console.WriteLine(streetsAND.Count);

                            switch (streetsAND.Count)
                            {
                                case int n when (n > 0): //StreetAND exist
                                    {
                                        streetAND = streetsAND[0];
                                        street = repo.GetById<GStreet>(streetAND.OriId);
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                        highlightJunction(repo, street.FromNodeId.Value, false, _selectedFeaturePoints);
                                        highlightJunction(repo, street.ToNodeId.Value, false, _selectedFeaturePoints);

                                        //Check if the street is good to go
                                        DoChecking(street);

                                        nearbyStreetsAND = new List<GStreetAND>();

                                        fromNode = street.GetFromNode();
                                        toNode = street.GetToNode();

                                        if (fromNode != null)
                                        {
                                            foreach (GStreetAND street1 in fromNode.GetStreetsAND())
                                            {
                                                if (street1.Equals(streetAND))
                                                {
                                                    continue;
                                                }
                                                if (!(streetAND.FromPoint.Equals2(street1.FromPoint) || streetAND.FromPoint.Equals2(street1.ToPoint)))
                                                {
                                                    continue;
                                                }
                                                nearbyStreetsAND.Add(street1);
                                            }
                                        }
                                        if (toNode != null)
                                        {
                                            foreach (GStreetAND street1 in toNode.GetStreetsAND())
                                            {
                                                if (street1.Equals(streetAND))
                                                {
                                                    continue;
                                                }
                                                if (!(streetAND.ToPoint.Equals2(street1.FromPoint) || streetAND.ToPoint.Equals2(street1.ToPoint)))
                                                {
                                                    continue;
                                                }
                                                nearbyStreetsAND.Add(street1);
                                            }
                                        }

                                        nearbyStreetsAND.RemoveAll(theNearbyStreetAND => theNearbyStreetAND.AndStatus == 2);

                                        if (nearbyStreetsAND.Count == 1)
                                        {
                                            nearbyStreetAND = nearbyStreetsAND[0];
                                        }
                                        else if (nearbyStreetsAND.Count < 1)
                                        {
                                            throw new Exception("No Nearby AND Street to Union.");                                            
                                        }
                                        else
                                        {
                                            nearbyStreetAND = ChooseFeature(nearbyStreetsAND, null);
                                        }
                                        using (new WaitCursor())
                                        {
                                            if (nearbyStreetAND == null)
                                            {
                                                throw new Exception("No Street.");
                                            }
                                            highlightStreet(repo, nearbyStreetAND.OriId, false, _selectedFeatureLines);
                                            highlightJunction(repo, nearbyStreetAND.FromNodeId.Value, false, _selectedFeaturePoints);
                                            highlightJunction(repo, nearbyStreetAND.ToNodeId.Value, false, _selectedFeaturePoints);
                                            if (nearbyStreetAND.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }
                                            if (nearbyStreetAND.HasRestriction())
                                            {
                                                throw new QualityControlException("Street has restriction");
                                            }
                                            if (!streetAND.Compare(nearbyStreetAND))
                                            {
                                                using (QuestionMessageBox box = new QuestionMessageBox())
                                                {
                                                    box.SetButtons(MessageBoxButtons.YesNo);
                                                    box.SetCaption("Union street");
                                                    box.SetText("Info is not same, proceed?");
                                                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                                    if (box.Show() != DialogResult.Yes)
                                                    {
                                                        throw new ProcessCancelledException();
                                                    }
                                                }
                                            }

                                            nearbyStreet = repo.GetById<GStreet>(nearbyStreetAND.OriId);

                                            repo.StartTransaction(() =>
                                            {
                                                //Start Union Process
                                                double oldLen = street.Length.Value;
                                                SetForUnion(street, nearbyStreet);
                                                double newLen = street.Length.Value; 

                                                //add new street & copy from deleted street
                                                GStreet mergedStreet = repo.NewObj<GStreet>();
                                                mergedStreet.Init();
                                                mergedStreet.CopyFrom(street);
                                                mergedStreet.AreaId = street.AreaId;
                                                mergedStreet.Shape = street.Shape;
                                                mergedStreet.FromNodeId = street.FromNodeId;
                                                mergedStreet.ToNodeId = street.ToNodeId;
                                                mergedStreet.Length = newLen; // set new length

                                                mergedStreet.NavigationStatus = 0;
                                                andStreet.UpdateCreatedByUserAND(mergedStreet);
                                                andStreet.UpdateModifiedByUserAND(mergedStreet);
                                                mergedStreet = repo.Insert(mergedStreet, false);
                                                repo.UpdateInsertByAND(mergedStreet, true);

                                                //insert mergedStreet to AND
                                                andStreet.CreateStreetAND(repo, mergedStreet);
                                                GJunction mergedStreetFromNodeADM = mergedStreet.GetFromNode();
                                                GJunction mergedStreetToNodeADM = mergedStreet.GetToNode();
                                                GStreetAND mergedStreetAND = mergedStreet.GetStreetANDId();
                                                mergedStreetAND.FromNodeId = mergedStreetFromNodeADM.OID;
                                                mergedStreetAND.ToNodeId = mergedStreetToNodeADM.OID;

                                                andStreet.UpdateCreatedByUserAND(mergedStreetAND);
                                                andStreet.UpdateModifiedByUserAND(mergedStreetAND);
                                                repo.UpdateRemainStatus(mergedStreetAND, true);

                                                // Update the street feature
                                                UpdateLinkedFeatureAND(repo, street, mergedStreet);

                                                // Update the nearbyStreet feature
                                                UpdateLinkedFeatureAND(repo, nearbyStreet, mergedStreet);

                                                // Delete Street & Delete junction
                                                street.Length = oldLen; // set old length
                                                repo.UpdateRemainStatus(street, true);
                                                andStreet.Delete(repo, street);
                                                andStreet.Delete(repo, nearbyStreet);

                                                // Delete the middle junction
                                                if (fromNode.CanDeleteUnionJunction())
                                                {
                                                    int id = fromNode.OID;
                                                    andJunction.Delete(repo, fromNode);
                                                    OnReported("Junction deleted. {0}", id);
                                                }

                                                if (toNode.CanDeleteUnionJunction())
                                                {
                                                    int id = toNode.OID;
                                                    andJunction.Delete(repo, toNode);
                                                    OnReported("Junction deleted. {0}", id);
                                                }
                                                ShowSuccessMessage();

                                                OnReported("Street {0} unioned with street {1}.", street.OID, nearbyStreet.OID);

                                                OnReported(Message1);
                                                OnStepReported(Message1);
                                            });
                                        }
                                    }
                                    break;

                                case int n when (n == 0): //StreetAND does not exist
                                    if (streets.Count == 0) { return; }
                                    else if (streets.Count > 0)
                                    {
                                        street = streets[0];
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                        highlightJunction(repo, street.FromNodeId.Value, false, _selectedFeaturePoints);
                                        highlightJunction(repo, street.ToNodeId.Value, false, _selectedFeaturePoints);
                                        //Check if the street is good to go
                                        DoChecking(street);

                                        nearbyStreets = new List<GStreet>();

                                        fromNode = street.GetFromNode();
                                        toNode = street.GetToNode();

                                        if (fromNode != null)
                                        {
                                            foreach (GStreet street1 in fromNode.GetStreets())
                                            {
                                                if (street1.Equals(street))
                                                {
                                                    continue;
                                                }
                                                if (!(street.FromPoint.Equals2(street1.FromPoint) || street.FromPoint.Equals2(street1.ToPoint)))
                                                {
                                                    continue;
                                                }
                                                nearbyStreets.Add(street1);
                                            }
                                        }

                                        if (toNode != null)
                                        {
                                            foreach (GStreet street1 in toNode.GetStreets())
                                            {
                                                if (street1.Equals(street))
                                                {
                                                    continue;
                                                }
                                                if (!(street.ToPoint.Equals2(street1.FromPoint) || street.ToPoint.Equals2(street1.ToPoint)))
                                                {
                                                    continue;
                                                }
                                                nearbyStreets.Add(street1);
                                            }
                                        }
                                        // remove nearby street that are AND
                                        nearbyStreets.RemoveAll(theStreet => theStreet.AndStatus != null);
                                        nearbyStreets.RemoveAll(theStreet => theStreet.AreaId == null);

                                        if (nearbyStreets.Count == 1)
                                        {
                                            nearbyStreet = nearbyStreets[0];
                                        }
                                        else if (nearbyStreets.Count < 1)
                                        {
                                            throw new Exception("No Nearby Street to Union.");
                                        }
                                        else
                                        {
                                            nearbyStreet = ChooseFeature(nearbyStreets, null);
                                        }

                                        using (new WaitCursor())
                                        {
                                            highlightStreet(repo, nearbyStreet.OID, false, _selectedFeatureLines);
                                            highlightJunction(repo, nearbyStreet.FromNodeId.Value, false, _selectedFeaturePoints);
                                            highlightJunction(repo, nearbyStreet.ToNodeId.Value, false, _selectedFeaturePoints);
                                            if (nearbyStreet.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            if (nearbyStreet.HasRestriction())
                                            {
                                                throw new QualityControlException("Street has restriction");
                                            }

                                            if (!street.Compare(nearbyStreet))
                                            {
                                                using (QuestionMessageBox box = new QuestionMessageBox())
                                                {
                                                    box.SetButtons(MessageBoxButtons.YesNo);
                                                    box.SetCaption("Union street");
                                                    box.SetText("Info is not same, proceed?");
                                                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                                    if (box.Show() != DialogResult.Yes)
                                                    {
                                                        throw new ProcessCancelledException();
                                                    }
                                                }
                                            }

                                            repo.StartTransaction(() =>
                                            {
                                                //Start Union Process
                                                double oldLen = street.Length.Value;
                                                SetForUnion(street, nearbyStreet);
                                                double newLen = street.Length.Value;
                                                // Merge
                                                //add new street & copy from deleted street
                                                GStreet mergedStreet = repo.NewObj<GStreet>();
                                                mergedStreet.Init();
                                                mergedStreet.CopyFrom(street);
                                                mergedStreet.AreaId = street.AreaId;
                                                mergedStreet.FromNodeId = street.FromNodeId;
                                                mergedStreet.ToNodeId = street.ToNodeId;
                                                mergedStreet.Length = newLen;
                                                mergedStreet.Shape = street.Shape;

                                                andStreet.UpdateCreatedByUserAND(mergedStreet);
                                                andStreet.UpdateModifiedByUserAND(mergedStreet);
                                                mergedStreet.NavigationStatus = 0;
                                                mergedStreet = repo.Insert(mergedStreet, false);
                                                repo.UpdateInsertByAND(mergedStreet, true);

                                                //insert mergedStreet to AND
                                                andStreet.CreateStreetAND(repo, mergedStreet);
                                                GJunction mergedStreetFromNodeADM = mergedStreet.GetFromNode();
                                                GJunction mergedStreetToNodeADM = mergedStreet.GetToNode();
                                                GStreetAND mergedStreetAND = mergedStreet.GetStreetANDId();
                                                mergedStreetAND.FromNodeId = mergedStreetFromNodeADM.OID;
                                                mergedStreetAND.ToNodeId = mergedStreetToNodeADM.OID;
                                                andStreet.UpdateCreatedByUserAND(mergedStreetAND);
                                                andStreet.UpdateModifiedByUserAND(mergedStreetAND);
                                                repo.UpdateRemainStatus(mergedStreetAND, true);

                                                // Update the street feature. set all the street feature to the new merged street.
                                                UpdateLinkedFeatureAND(repo, street, mergedStreet);

                                                // Update the nearbyStreet feature. set all the nearbyStreet feature to the new merged street.
                                                UpdateLinkedFeatureAND(repo, nearbyStreet, mergedStreet);

                                                // Delete Street
                                                street.Length = oldLen;
                                                repo.UpdateRemainStatus(street, true);
                                                andStreet.Delete(repo, street);
                                                andStreet.Delete(repo, nearbyStreet);

                                                // Delete the middle junction
                                                if (fromNode.CanDeleteUnionJunction())
                                                {
                                                    int id = fromNode.OID;
                                                    andJunction.Delete(repo, fromNode);
                                                    OnReported("Junction deleted. {0}", id);
                                                }

                                                if (toNode.CanDeleteUnionJunction())
                                                {
                                                    int id = toNode.OID;
                                                    andJunction.Delete(repo, toNode);
                                                    OnReported("Junction deleted. {0}", id);
                                                }
                                                ShowSuccessMessage();

                                                OnReported("Street {0} unioned with street {1}.", street.OID, nearbyStreet.OID);

                                                OnReported(Message1);
                                                OnStepReported(Message1);
                                            });
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    else
                    {
                        GStreet street = null;
                        GStreet nearbyStreet = null;
                        List<GStreet> streets;
                        List<GStreet> nearbyStreets;
                        GJunction fromNode;
                        GJunction toNode;

                        using (new WaitCursor())
                        {
                            streets = SelectStreet(repo, point);

                            if (streets.Count == 0)
                            {
                                return;
                            }

                            street = streets[0];
                            highlightStreet(repo, street.OID, true, _selectedFeatureLines);

                            // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                            //if (Session.User.GetGroup().Name != "SUPERVISOR")
                            //{
                            //    if (street.AreaId != null)
                            //    {
                            //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                            //    }
                            //}

                            // noraini ali - Aug 2021 - check feature lock by AND
                            ANDStreetTools andStreet = new ANDStreetTools();
                            andStreet.CheckFeatureLock(street.AreaId, SegmentName);

                            // end add

                            if (street.IsNavigationReady)
                            {
                                if (!Session.User.CanDo(Command.NavigationItem))
                                {
                                    throw new NavigationControlException();
                                }
                            }

                            if (street.HasRestriction())
                            {
                                throw new QualityControlException("Street has restriction");
                            }

                            nearbyStreets = new List<GStreet>();

                            fromNode = street.GetFromNode();
                            toNode = street.GetToNode();

                            if (fromNode != null)
                            {
                                foreach (GStreet street1 in fromNode.GetStreets())
                                {
                                    if (street1.Equals(street))
                                    {
                                        continue;
                                    }
                                    if (!(street.FromPoint.Equals2(street1.FromPoint) || street.FromPoint.Equals2(street1.ToPoint)))
                                    {
                                        continue;
                                    }
                                    nearbyStreets.Add(street1);
                                }
                            }

                            if (toNode != null)
                            {
                                foreach (GStreet street1 in toNode.GetStreets())
                                {
                                    if (street1.Equals(street))
                                    {
                                        continue;
                                    }
                                    if (!(street.ToPoint.Equals2(street1.FromPoint) || street.ToPoint.Equals2(street1.ToPoint)))
                                    {
                                        continue;
                                    }
                                    nearbyStreets.Add(street1);
                                }
                            }
                        }

                        nearbyStreet = ChooseFeature(nearbyStreets, null);

                        using (new WaitCursor())
                        {
                            if (nearbyStreet == null)
                            {
                                throw new Exception("No Street.");
                            }
                            highlightStreet(repo, nearbyStreet.OID, false, _selectedFeatureLines);
                            if (nearbyStreet.IsNavigationReady)
                            {
                                if (!Session.User.CanDo(Command.NavigationItem))
                                {
                                    throw new NavigationControlException();
                                }
                            }

                            if (nearbyStreet.HasRestriction())
                            {
                                throw new QualityControlException("Street has restriction");
                            }

                            if (!street.Compare(nearbyStreet))
                            {
                                using (QuestionMessageBox box = new QuestionMessageBox())
                                {
                                    box.SetButtons(MessageBoxButtons.YesNo);
                                    box.SetCaption("Union street");
                                    box.SetText("Info is not same, proceed?");
                                    box.SetDefaultButton(MessageBoxDefaultButton.Button2);

                                    if (box.Show() != DialogResult.Yes)
                                    {
                                        throw new ProcessCancelledException();
                                    }
                                }
                            }

                            repo.StartTransaction(() =>
                            {
                                //Start Union Process
                                SetForUnion(street, nearbyStreet);
                                
                                street.NavigationStatus = 0;
                                repo.Update(street);

                                UpdateLinkedFeature(repo, nearbyStreet, street);

                                OnReported("Street {0} unioned with street {1}.", street.OID, nearbyStreet.OID);

                                repo.Delete(nearbyStreet);

                                if (fromNode.CanDelete())
                                {
                                    int id = fromNode.OID;
                                    repo.Delete(fromNode);
                                    OnReported("Junction deleted. {0}", id);
                                }

                                if (toNode.CanDelete())
                                {
                                    int id = toNode.OID;
                                    repo.Delete(toNode);
                                    OnReported("Junction deleted. {0}", id);
                                }

                                ShowSuccessMessage();

                                OnReported(Message1);
                                OnStepReported(Message1);
                            });
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    MapDocument.ShowContextMenu(x, y);
                }

            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
            finally
            {
                InProgress = false;
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        private List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetAND>(query).ToList();
        }

        private void DoChecking(GStreet street)
        {
            //Check if Street, Property, Landmark and BuildingGroup associate to street is within work area.
            ANDStreetTools andStreet = new ANDStreetTools();
            ANDPropertyTools andProperty = new ANDPropertyTools();
            ANDLandmarkTools andLandmark = new ANDLandmarkTools();
            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();

            andStreet.CheckWithinWorkArea(SegmentName, street);
            andStreet.CheckUserOwnWorkArea(SegmentName, street);
            foreach (GProperty property in street.GetProperties())
            {
                andProperty.CheckWithinWorkArea(SegmentName, property);
            }
            foreach (GLandmark landmark in street.GetLandmarks())
            {
                andLandmark.CheckWithinWorkArea(SegmentName, landmark);
            }
            foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
            {
                andBuildingGroup.CheckWithinWorkArea(SegmentName, buildingGroup);
            }
            if (street.IsNavigationReady)
            {
                if (!Session.User.CanDo(Command.NavigationItem))
                {
                    throw new NavigationControlException();
                }
            }
            if (street.HasRestriction())
            {
                throw new QualityControlException("Street has restriction");
            }
        }

        private void SetForUnion(GStreet street, GStreet nearbyStreet)
        {
            ISegmentCollection segmentCollection = (ISegmentCollection)street.Polyline;
            IPolyline newLine;

            if (nearbyStreet.FromPoint.Equals2(street.FromPoint) && !nearbyStreet.ToPoint.Equals2(street.FromPoint) && !nearbyStreet.ToPoint.Equals2(street.ToPoint))
            {
                IPolyline line = nearbyStreet.Polyline;
                line.ReverseOrientation();
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)line;
                nearbySegmentCollection.AddSegmentCollection(segmentCollection);
                newLine = (IPolyline)nearbySegmentCollection;
                street.FromNodeId = nearbyStreet.ToNodeId;
            }
            else if (!nearbyStreet.FromPoint.Equals2(street.FromPoint) && nearbyStreet.ToPoint.Equals2(street.FromPoint) && !nearbyStreet.FromPoint.Equals2(street.ToPoint))
            {
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)nearbyStreet.Polyline;
                nearbySegmentCollection.AddSegmentCollection(segmentCollection);
                newLine = (IPolyline)nearbySegmentCollection;
                street.FromNodeId = nearbyStreet.FromNodeId;
            }
            else if ((nearbyStreet.FromPoint.Equals2(street.FromPoint) && nearbyStreet.ToPoint.Equals2(street.ToPoint)) ||
                (!nearbyStreet.FromPoint.Equals2(street.ToPoint) && nearbyStreet.ToPoint.Equals2(street.ToPoint) && !nearbyStreet.FromPoint.Equals2(street.FromPoint)))
            {
                IPolyline line = nearbyStreet.Polyline;
                line.ReverseOrientation();
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)line;
                segmentCollection.AddSegmentCollection(nearbySegmentCollection);
                newLine = (IPolyline)segmentCollection;
                street.ToNodeId = nearbyStreet.FromNodeId;
            }
            else if ((nearbyStreet.FromPoint.Equals2(street.ToPoint) && nearbyStreet.ToPoint.Equals2(street.FromPoint)) ||
                (nearbyStreet.FromPoint.Equals2(street.ToPoint) && !nearbyStreet.ToPoint.Equals2(street.FromPoint) && !nearbyStreet.ToPoint.Equals2(street.ToPoint)))
            {
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)nearbyStreet.Polyline;
                segmentCollection.AddSegmentCollection(nearbySegmentCollection);
                newLine = (IPolyline)segmentCollection;
                street.ToNodeId = nearbyStreet.ToNodeId;
            }
            else if (nearbyStreet.FromPoint.Equals2(street.FromPoint) && nearbyStreet.ToPoint.Equals2(street.FromPoint))
            {
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)nearbyStreet.Polyline;
                nearbySegmentCollection.AddSegmentCollection(segmentCollection);
                newLine = (IPolyline)nearbySegmentCollection;
            }
            else if (nearbyStreet.FromPoint.Equals2(street.ToPoint) && nearbyStreet.ToPoint.Equals2(street.ToPoint))
            {
                ISegmentCollection nearbySegmentCollection = (ISegmentCollection)nearbyStreet.Polyline;
                segmentCollection.AddSegmentCollection(nearbySegmentCollection);
                newLine = (IPolyline)segmentCollection;
            }
            else
            {
                throw new Exception("Unknown condition.");
            }

            street.Shape = newLine;
            street.Length = newLine.Length; //noraini - update new length
        }

        private void UpdateLinkedFeatureAND(RepositoryFactory repo, GStreet street, GStreet mergedStreet)
        {
            ANDPropertyTools andProperty = new ANDPropertyTools();
            ANDLandmarkTools andLandmark = new ANDLandmarkTools();
            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();

            foreach (GProperty property in street.GetProperties())
            {
                GPropertyAND propertyAND = property.GetPropertyANDId();
                if (propertyAND != null)
                {
                    propertyAND.StreetId = mergedStreet.OID;
                    repo.Update(propertyAND);
                }
                else
                {
                    andProperty.CreatePropertyAND(repo, property);
                    GPropertyAND insertedPropertyAND = property.GetPropertyANDId();
                    insertedPropertyAND.StreetId = mergedStreet.OID;
                    // Property AND  - maintain created as adm feature, update by User AND
                    insertedPropertyAND.CreatedBy = property.CreatedBy;
                    insertedPropertyAND.DateCreated = property.DateCreated;
                    andProperty.UpdateModifiedByUserAND(insertedPropertyAND);
                    repo.Update(insertedPropertyAND);
                    repo.UpdateByAND(property, true);
                }
            }
            foreach (GLandmark landmark in street.GetLandmarks())
            {
                GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
                if (landmarkAND != null)
                {
                    landmarkAND.StreetId = mergedStreet.OID;
                    repo.Update(landmarkAND);
                }
                else
                {
                    andLandmark.CreateLandmarkAND(repo, landmark);
                    GLandmarkAND insertedLandmarkAND = landmark.GetLandmarkANDId();
                    insertedLandmarkAND.StreetId = mergedStreet.OID;
                    // Landmark AND - maintain created as adm feature, update by User AND
                    insertedLandmarkAND.CreatedBy = landmark.CreatedBy;
                    insertedLandmarkAND.DateCreated = landmark.DateCreated;
                    andProperty.UpdateModifiedByUserAND(insertedLandmarkAND);
                    repo.Update(insertedLandmarkAND);
                    repo.UpdateByAND(landmark, true);
                } 
            }
            foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
            {
                GBuildingGroupAND buildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                if (buildingGroupAND != null)
                {
                    buildingGroupAND.StreetId = mergedStreet.OID;
                    repo.Update(buildingGroupAND);
                }
                else
                {
                    andBuildingGroup.CreateBuildingGroupAND(repo, buildingGroup);
                    GBuildingGroupAND insertedBuildingGroupAND = buildingGroup.GetBuildingGroupANDId();
                    insertedBuildingGroupAND.StreetId = mergedStreet.OID;
                    // Building Group AND - maintain created as adm feature, update by User AND
                    insertedBuildingGroupAND.CreatedBy = buildingGroup.CreatedBy;
                    insertedBuildingGroupAND.DateCreated = buildingGroup.DateCreated;
                    andProperty.UpdateModifiedByUserAND(insertedBuildingGroupAND);
                    repo.Update(insertedBuildingGroupAND);
                    repo.UpdateByAND(buildingGroup, true);
                }
            }
            foreach (GStreetText text in street.GetTexts())
            {
                repo.Delete(text);
            }
        }

        private void UpdateLinkedFeature(RepositoryFactory repo, GStreet street, GStreet mergedStreet)
        {
            foreach (GProperty property in street.GetProperties())
            {
                property.StreetId = mergedStreet.OID;
                repo.UpdateRemainStatus(property, true);
            }

            foreach (GLandmark landmark in street.GetLandmarks())
            {
                landmark.StreetId = mergedStreet.OID;
                repo.UpdateRemainStatus(landmark, true);
            }

            foreach (GBuildingGroup buildingGroup in street.GetBuildingGroups())
            {
                buildingGroup.StreetId = mergedStreet.OID;
                repo.UpdateRemainStatus(buildingGroup, true);
            }

            foreach (GStreetText text in street.GetTexts())
            {
                repo.Delete(text);
            }
        }

        private void ShowSuccessMessage()
        {
            using (InfoMessageBox box = new InfoMessageBox())
            {
                box.SetText("Union Street Successful");
                box.Show();
            }
        }

        protected override void Reset()
        {
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
        }

        public override bool Deactivate()
        {
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using Earthworm.AO;
using Geomatic.Core.Sessions;
using Geomatic.MapTool.JunctionTools;

namespace Geomatic.MapTool.StreetTools
{
    public class CopyStreetTool : Tool
    {
        private enum Progress
        {
            Select = 0,
            TryCopy,
            Copying
        }

        #region Fields

        private Progress _progress;
        protected MoveLine _moveLine;
        private GStreet _selectedStreet;
        private GStreetAND _selectedStreetAND;

        #endregion

        public CopyStreetTool()
        {
            _name = "Copy Street";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_moveLine == null)
            {
                _moveLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported("Select a street.");
            OnStepReported("Select a street.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryCopy:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            List<GStreet> streets = SelectStreet(repo, point);
                            List<GStreetAND> streetsAND = SelectStreetAND(repo, point);
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                if (streets.Count == 0 && streetsAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedStreet = null;
                                    _moveLine.Stop();
                                    OnReported("Select a street.");
                                    OnStepReported("Select a street.");
                                    break;
                                }
                                if (streetsAND.Count > 0)
                                {
                                    GStreetAND street = streetsAND[0];
                                    if (!_selectedStreetAND.Equals(street))
                                    {
                                        _moveLine.Stop();
                                        _moveLine.Line = street.Polyline;
                                        _moveLine.Start(point);
                                        _selectedStreetAND = street;
                                        break;
                                    }
                                }
                                if (streets.Count > 0)
                                {
                                    GStreet street = streets[0];
                                    if (!_selectedStreet.Equals(street))
                                    {
                                        _moveLine.Stop();
                                        _moveLine.Line = street.Polyline;
                                        _moveLine.Start(point);
                                        _selectedStreet = street;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (streets.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedStreet = null;
                                    _moveLine.Stop();
                                    OnReported("Select a street.");
                                    OnStepReported("Select a street.");
                                    break;
                                }

                                GStreet street = streets[0];
                                if (!_selectedStreet.Equals(street))
                                {
                                    _moveLine.Stop();
                                    _moveLine.Line = street.Polyline;
                                    _moveLine.Start(point);
                                    _selectedStreet = street;
                                    break;
                                }
                            }

                            _progress = Progress.Copying;
                        }
                        break;
                    case Progress.Copying:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryCopy:
                        break;
                    case Progress.Copying:
                        if (button != 1)
                        {
                            break;
                        }
                        _moveLine.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    List<GStreetAND> streetsAND = SelectStreetAND(repo, point);
                                    List<GStreet> streets = SelectStreet(repo, point);
                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        _moveLine.Line = street.Polyline;
                                        _moveLine.Start(point);
                                        _selectedStreet = street;
                                        _progress = Progress.TryCopy;
                                        OnReported("Drag selected street to copy.");
                                        OnStepReported("Drag selected street to copy.");
                                    }
                                    if (streetsAND.Count > 0)
                                    {
                                        GStreetAND street = streetsAND[0];
                                        _moveLine.Line = street.Polyline;
                                        _moveLine.Start(point);
                                        _selectedStreetAND = street;
                                        _progress = Progress.TryCopy;
                                        OnReported("Drag selected street to copy.");
                                        OnStepReported("Drag selected street to copy.");
                                    }
                                }
                                else
                                {
                                    List<GStreet> streets = SelectStreet(repo, point);
                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        _moveLine.Line = street.Polyline;
                                        _moveLine.Start(point);
                                        _selectedStreet = street;
                                        _progress = Progress.TryCopy;
                                        OnReported("Drag selected street to copy.");
                                        OnStepReported("Drag selected street to copy.");
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryCopy:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Copying:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            IPolyline newLine = (IPolyline)_moveLine.Stop();
                            _progress = Progress.Select;

                            repo.StartTransaction(() =>
                            {
                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    //all query
                                    GJunction fromNode = null;
                                    GJunction toNode = null;
                                    ANDJunctionTools andJunction = new ANDJunctionTools();
                                    

                                    List<GJunction> junctions;
                                    List<GJunctionAND> junctionsAND;

                                    //from point
                                    junctions = repo.SpatialSearch<GJunction>(newLine.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    junctionsAND = repo.SpatialSearch<GJunctionAND>(newLine.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (junctions.Count == 0)
                                    {
                                        GJunction newJunction = repo.NewObj<GJunction>();
                                        newJunction.Init();
                                        newJunction.Shape = newLine.FromPoint;
                                        andJunction.CheckWithinWorkArea(SegmentName, newLine.FromPoint);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, newLine.FromPoint);
                                        fromNode = repo.Insert(newJunction);
                                        repo.UpdateInsertByAND(fromNode, true);
                                        andJunction.CreateJunctionAND(repo, fromNode);
                                    }
                                    else if (junctions.Count == 1)
                                    {
                                        fromNode = junctions[0];
                                        if (_selectedStreet.FromNodeId == fromNode.OID)
                                        {
                                            throw new Exception("Unable to create duplicate street.");
                                        }
                                        newLine.FromPoint = fromNode.Point;
                                        andJunction.CheckWithinWorkArea(SegmentName, newLine.FromPoint);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, newLine.FromPoint);
                                    }
                                    /*else if (junctionsAND.Count == 1)
                                    {
                                        fromNode = junctionsAND[0];
                                        if (_selectedStreetAND.FromNodeId == fromNode.OID)
                                        {
                                            throw new Exception("Unable to create duplicate street.");
                                        }
                                        newLine.FromPoint = fromNode.Point;
                                    }*/
                                    else
                                    {
                                        throw new Exception("Found multiple junctions.");
                                    }

                                    //to point
                                    junctions = repo.SpatialSearch<GJunction>(newLine.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    junctionsAND = repo.SpatialSearch<GJunctionAND>(newLine.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (junctions.Count == 0)
                                    {
                                        GJunction newJunction = repo.NewObj<GJunction>();
                                        newJunction.Init();
                                        newJunction.Shape = newLine.ToPoint;
                                        andJunction.CheckWithinWorkArea(SegmentName, newLine.ToPoint);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, newLine.ToPoint);
                                        toNode = repo.Insert(newJunction);
                                        repo.UpdateInsertByAND(toNode, true);
                                        andJunction.CreateJunctionAND(repo, toNode);
                                    }
                                    else if (junctions.Count == 1)
                                    {
                                        toNode = junctions[0];
                                        if (_selectedStreet.ToNodeId == toNode.OID)
                                        {
                                            throw new Exception("Unable to create duplicate street.");
                                        }
                                        newLine.ToPoint = toNode.Point;
                                        andJunction.CheckWithinWorkArea(SegmentName, newLine.ToPoint);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, newLine.ToPoint);
                                    }
                                    else
                                    {
                                        throw new Exception("Found multiple junctions.");
                                    }

                                    //insert into database
                                    GStreet newStreet = repo.NewObj<GStreet>();
                                    newStreet.CopyFrom(_selectedStreet);

                                    newStreet.FromNodeId = fromNode.OID;
                                    newStreet.ToNodeId = toNode.OID;
                                    newStreet.NavigationStatus = 0;
                                    newStreet.Shape = newLine;
                                    newStreet = repo.Insert(newStreet);
                                    repo.UpdateInsertByAND(newStreet, true);


                                    ANDStreetTools andStreet = new ANDStreetTools();

                                    //insert current street to AND street
                                    andStreet.CreateStreetAND(repo, newStreet);

                                    // Get FromNode and ToNode (Junction)
                                    GJunction FromNodeADM = newStreet.GetFromNode();
                                    GJunction ToNodeADM = newStreet.GetToNode();

                                    //Insert Junction AND
                                    andJunction.CreateJunctionAND(repo, FromNodeADM);
                                    andJunction.CreateJunctionAND(repo, ToNodeADM);

                                    //Get Inserted Street AND and Junction AND
                                    GStreetAND newStreetAND = _selectedStreet.GetStreetANDId();
                                    GJunctionAND FromNodeAND = FromNodeADM.GetJunctionANDId();
                                    GJunctionAND ToNodeAND = ToNodeADM.GetJunctionANDId();

                                    //Update the Street AND to the new Junction AND
                                    newStreetAND.FromNodeId = FromNodeAND.OID;
                                    newStreetAND.ToNodeId = ToNodeAND.OID;
                                    repo.UpdateRemainStatus(newStreetAND, true);

                                    OnReported("New street created. {0}", newStreet.OID);
                                    OnReported("Select a street.");
                                    OnStepReported("Select a street.");
                                }
                                else
                                {
                                    GJunction fromNode = null;
                                    GJunction toNode = null;

                                    List<GJunction> junctions;

                                    junctions = repo.SpatialSearch<GJunction>(newLine.FromPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (junctions.Count == 0)
                                    {
                                        // Create Junction
                                        GJunction newJunction = repo.NewObj<GJunction>();
                                        newJunction.Init();
                                        newJunction.Shape = newLine.FromPoint;
                                        fromNode = repo.Insert(newJunction);
                                    }
                                    else if (junctions.Count == 1)
                                    {
                                        fromNode = junctions[0];
                                        if (_selectedStreet.FromNodeId == fromNode.OID)
                                        {
                                            throw new Exception("Unable to create duplicate street.");
                                        }
                                        newLine.FromPoint = fromNode.Point;
                                    }
                                    else
                                    {
                                        throw new Exception("Found multiple junctions.");
                                    }

                                    junctions = repo.SpatialSearch<GJunction>(newLine.ToPoint.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (junctions.Count == 0)
                                    {
                                        // Create Junction
                                        GJunction newJunction = repo.NewObj<GJunction>();
                                        newJunction.Init();
                                        newJunction.Shape = newLine.ToPoint;
                                        toNode = repo.Insert(newJunction);
                                    }
                                    else if (junctions.Count == 1)
                                    {
                                        toNode = junctions[0];
                                        if (_selectedStreet.ToNodeId == toNode.OID)
                                        {
                                            throw new Exception("Unable to create duplicate street.");
                                        }
                                        newLine.ToPoint = toNode.Point;
                                    }
                                    else
                                    {
                                        throw new Exception("Found multiple junctions.");
                                    }

                                    if (newLine.Length < 5)
                                    {
                                        throw new Exception("Unable to create street with length lesser than 5 meter.");
                                    }

                                    GStreet newStreet = repo.NewObj<GStreet>();
                                    newStreet.CopyFrom(_selectedStreet);

                                    newStreet.FromNodeId = fromNode.OID;
                                    newStreet.ToNodeId = toNode.OID;
                                    newStreet.NavigationStatus = 0;
                                    newStreet.Shape = newLine;
                                    newStreet = repo.Insert(newStreet);

                                    OnReported("New street created. {0}", newStreet.OID);
                                    OnReported("Select a street.");
                                    OnStepReported("Select a street.");
                                }
                            });

                            _selectedStreet = null;
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        private List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetAND>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            _selectedStreet = null;
            if (_moveLine != null)
            {
                _moveLine.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            _selectedStreet = null;
            if (_moveLine != null)
            {
                _moveLine.Stop();
                _moveLine = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_moveLine != null)
            {
                _moveLine.Refresh(hDC);
            }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.StreetTools
{

    public class ANDStreetTools : ValidateWorkAreaAndStatus
    {
        public void CreateStreetAND(RepositoryFactory repo, GStreet Street)
        {
            GStreet street = repo.GetById<GStreet>(Street.OID);
            GStreetAND StreetAND = repo.NewObj<GStreetAND>();
            StreetAND.CopyFrom(street);
            repo.Insert(StreetAND, false);
        }

        public void AddStreetAND(RepositoryFactory repo, GStreet Street)
        {
            // update street ADM - Area ID
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(Street.Shape, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => Street.AreaId = thearea.OID);
            repo.UpdateInsertByAND(Street, true);

            // create new street AND as copy from ADM street 
            CreateStreetAND(repo, Street);

            // update street AND - CreatedBy, DateCreated, Updatedby & DateUpdated
            GStreetAND StreetAND = Street.GetStreetANDId();
            UpdateCreatedByUserAND(StreetAND);
            UpdateModifiedByUserAND(StreetAND);
            repo.UpdateRemainStatus(StreetAND, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GStreet Street)
        {
            GStreetAND absObjAND = Street.GetStreetANDId();
            if (absObjAND == null)
            {
                // Create Street AND if not exist
                CreateStreetAND(repo, Street);
            }

            // Update Street AND - User info,STATUS & AND_STATUS as Delete status
            GStreetAND StreetAND = Street.GetStreetANDId();
            StreetAND.CreatedBy = Street.CreatedBy;
            StreetAND.DateCreated = Street.DateCreated;
            UpdateModifiedByUserAND(StreetAND);
            repo.UpdateDeleteByAND(StreetAND, true);

            // update Street ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(Street, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GStreetAND StreetAND)
        {
            // Update Street ADM - AND_STATUS & STATUS as Delete status
            GStreet Street = repo.GetById<GStreet>(StreetAND.OriId);
            repo.UpdateDeleteByAND(Street, true);

            // Update Street AND -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(StreetAND);
            repo.UpdateDeleteByAND(StreetAND, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GStreet Street)
        {
            GStreetAND absObjAND = Street.GetStreetANDId();
            if (absObjAND == null)
            {
                // Create Street AND
                CreateStreetAND(repo, Street);
            }

            //Get Street AND 
            GStreetAND StreetAND = Street.GetStreetANDId();

            //Display form 
            using (EditStreetANDForm form = new EditStreetANDForm(StreetAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - 
                    // AreadId, Updatedby & DateUpdated
                    List<GWorkArea> area;
                    area = repo.SpatialSearch<GWorkArea>(Street.Shape, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
                    area.ForEach(thearea => StreetAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(StreetAND);
                    repo.Update(StreetAND);

                    // Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => Street.AreaId = thearea.OID);
                    repo.UpdateByAND(Street, true);

                    // update work area flag as open 
                    int AreaID = StreetAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GStreetAND StreetAND)
        {
            GStreet Street = repo.GetById<GStreet>(StreetAND.OriId);
            if (Street == null)
            {
                throw new QualityControlException("Cannot proceed, street ADM not found.");
            }

            //Display form 
            using (EditStreetANDForm form = new EditStreetANDForm(StreetAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(StreetAND);
                    repo.Update(StreetAND, true);

                    // update ADM - STATUS & AND_STATUS to update status
                    repo.UpdateByAND(Street, true);

                    // update work area flag as open 
                    int AreaID = StreetAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GStreet Street, IPoint newPoint)
        {
            //Restrict from moving out, NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                throw new QualityControlException("Street Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != Street.AreaId)
                {
                    throw new QualityControlException("Cannot move out to different work area.");
                }
            });

            // if AND Street not exist so create AND Street 
            GStreetAND absObjAND = Street.GetStreetANDId();
            if (absObjAND == null)
            {
                // Create Property AND
                CreateStreetAND(repo, Street);
            }

            // set new location for AND Street
            GStreetAND StreetAND = Street.GetStreetANDId();
            StreetAND.Shape = newPoint;
            UpdateModifiedByUserAND(StreetAND);
            repo.UpdateGraphic(StreetAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(Street, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GStreetAND StreetAND, IPoint newPoint)
        {
            GStreet Street = repo.GetById<GStreet>(StreetAND.OriId);
            if (Street == null)
            {
                throw new QualityControlException("Cannot proceed, street ADM not found.");
            }

            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                throw new QualityControlException("Street Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != StreetAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out to different work area.");
                }
            });

            // set new location for AND Property & STATUS & AND_STATUS (Update Graphic)
            StreetAND.Shape = newPoint;
            UpdateModifiedByUserAND(StreetAND);
            repo.UpdateGraphic(StreetAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Street, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        //Flip
        public void Flip(RepositoryFactory repo, GStreet Street)
        {
            GStreetAND absObjAND = Street.GetStreetANDId();
            if (absObjAND == null)
            {
                // Create Street AND
                CreateStreetAND(repo, Street);
            }

            //Get Street AND 
            GStreetAND StreetAND = Street.GetStreetANDId();

            //Do the Flip
            IPolyline line = StreetAND.Polyline.Clone();
            line.ReverseOrientation();
            GJunction fromNode = StreetAND.GetFromNode();
            GJunction toNode = StreetAND.GetToNode();
            StreetAND.FromNodeId = toNode.OID;
            StreetAND.ToNodeId = fromNode.OID;

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Street, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        //Reshape
        public void Reshape(RepositoryFactory repo, GStreet Street, NewLine StreetLine)
        {
            GStreetAND absObjAND = Street.GetStreetANDId();
            if (absObjAND == null)
            {
                // Create Street AND
                CreateStreetAND(repo, Street);
            }

            //Get Street AND 
            GStreetAND StreetAND = Street.GetStreetANDId();

            //Do the Reshape
            StreetAND.Length = StreetLine.Length;
            StreetAND.Shape = StreetLine.Geometry;

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Street, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        //CopyStreetAttribute
        public void CopyStreetAttribute(RepositoryFactory repo, GStreet StreetDestination, GStreet StreetSource)
        {
            //Get Street AND 
            GStreetAND StreetAND = StreetDestination.GetStreetANDId();

            //Do the CopyStreetAttribute
            StreetAND.CopyFrom(StreetSource);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateByAND(StreetDestination, true);

            // update work area flag as open 
            int AreaID = StreetAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        public List<GStreet> FilterStreetInWorkArea(List<GStreet> _streets, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GStreet> streetsList = new List<GStreet>();
            foreach (GStreet _street in _streets)
            {
                if (_street.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_street.AndStatus.ToString()) || (_street.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_street.AreaId.ToString()) || (_street.AreaId > 0))
                        {
                            if (_street.AreaId.Value == _workareaid)
                            {
                                streetsList.Add(_street);
                            }
                        }
                    }
                }
            }
            return streetsList;
        }

        public List<GStreetAND> FilterStreetInWorkArea(List<GStreetAND> _streets, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GStreetAND> streetsList = new List<GStreetAND>();
            foreach (GStreetAND _street in _streets)
            {
                if (_street.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_street.AndStatus.ToString()) || (_street.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_street.AreaId.ToString()) || (_street.AreaId > 0))
                        {
                            if (_street.AreaId.Value == _workareaid)
                            {
                                streetsList.Add(_street);
                            }
                        }
                    }
                }
            }
            return streetsList;
        }

        public void CheckFeatureOnVerification(GStreet street)
        {
            GStreetAND StreetAND = street.GetStreetANDId();
            if (StreetAND != null)
            {
                throw new QualityControlException("Unable to edit feature without verification.");
            }
        }
    }
}

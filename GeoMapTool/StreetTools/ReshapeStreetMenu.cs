﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public partial class ReshapeStreetTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuUndoLast;
        private ToolStripMenuItem menuCancel;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuUndoLast = new ToolStripMenuItem();
            this.menuCancel = new ToolStripMenuItem();
            // 
            // menuUndoLast
            //
            this.menuUndoLast.Name = "menuUndoLast";
            this.menuUndoLast.Text = "Undo Last";
            this.menuUndoLast.Click += new EventHandler(this.OnUndoLast);
            // 
            // menuCancel
            //
            this.menuCancel.Name = "menuCancel";
            this.menuCancel.Text = "Cancel";
            this.menuCancel.Click += new EventHandler(this.OnCancel);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuUndoLast,
            this.menuCancel});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

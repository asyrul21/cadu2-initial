﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool.StreetTools
{
    public class ShowStreetRestrictionStreetTool : ChooseTool
    {
        #region Fields

        protected GStreetRestriction _selectedStreetRestriction;
        protected MoveLine _selectedStreetRestictionLine;
        protected IRgbColor _color1 = ColorUtils.Get(Color.Red);
        protected List<MoveLine> _streets;

        private const string Message1 = "Select a street restriction to show street.";

        #endregion

        public ShowStreetRestrictionStreetTool()
        {
            _name = "Show Street Restriction Street";
            _streets = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedStreetRestictionLine == null)
            {
                _selectedStreetRestictionLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                if (button == MouseKey.Left)
                {
                    using (new WaitCursor())
                    {
                        List<GStreetRestriction> streetRestrictions = SelectStreetRestriction(repo, point);
                        if (streetRestrictions.Count > 0)
                        {
                            GStreetRestriction streetRestriction;
                            if (streetRestrictions.Count == 1)
                            {
                                streetRestriction = streetRestrictions[0];
                            }
                            else
                            {
                                streetRestriction = ChooseFeature(streetRestrictions, null);
                            }
                            if (_selectedStreetRestriction == null)
                            {
                                _selectedStreetRestriction = streetRestriction;
                                _selectedStreetRestictionLine.Line = _selectedStreetRestriction.Polyline;
                                _selectedStreetRestictionLine.Start(point);
                                foreach (GStreet street in _selectedStreetRestriction.GetStreets())
                                {
                                    MoveLine moveLine = new MoveLine(ScreenDisplay, _color1);
                                    moveLine.Line = street.Polyline;
                                    moveLine.Start(point);
                                    _streets.Add(moveLine);
                                }
                            }
                            else
                            {
                                if (!_selectedStreetRestriction.Equals(streetRestriction))
                                {
                                    _selectedStreetRestriction = streetRestriction;
                                    _selectedStreetRestictionLine.Stop();
                                    _selectedStreetRestictionLine.Line = _selectedStreetRestriction.Polyline;
                                    _selectedStreetRestictionLine.Start(point);
                                    foreach (MoveLine moveLine in _streets)
                                    {
                                        moveLine.Stop();
                                    }
                                    _streets.Clear();
                                    foreach (GStreet street in _selectedStreetRestriction.GetStreets())
                                    {
                                        MoveLine moveLine = new MoveLine(ScreenDisplay, _color1);
                                        moveLine.Line = street.Polyline;
                                        moveLine.Start(point);
                                        _streets.Add(moveLine);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _selectedStreetRestriction = null;
                            _selectedStreetRestictionLine.Stop();
                            foreach (MoveLine moveLine in _streets)
                            {
                                moveLine.Stop();
                            }
                            _streets.Clear();
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreetRestriction> SelectStreetRestriction(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetRestriction> query = new Query<GStreetRestriction>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetRestriction>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedStreetRestriction = null;
            if (_selectedStreetRestictionLine != null)
            {
                _selectedStreetRestictionLine.Stop();
            }
            foreach (MoveLine moveLine in _streets)
            {
                moveLine.Stop();
            }
            _streets.Clear();
        }

        public override bool Deactivate()
        {
            _selectedStreetRestriction = null;
            if (_selectedStreetRestictionLine != null)
            {
                _selectedStreetRestictionLine.Stop();
                _selectedStreetRestictionLine = null;
            }
            foreach (MoveLine moveLine in _streets)
            {
                moveLine.Stop();
            }
            _streets.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetRestictionLine != null)
            {
                _selectedStreetRestictionLine.Refresh(hDC);
            }
            if (_streets != null)
            {
                foreach (MoveLine moveLine in _streets)
                {
                    moveLine.Refresh(hDC);
                }
            }
        }
    }
}

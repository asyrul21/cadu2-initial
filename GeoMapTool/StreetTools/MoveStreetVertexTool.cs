﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earthworm.AO;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Commands;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public class MoveStreetVertexTool : StreetVertexTool
    {
        private enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        protected MovePoint _selectedVertex;
        protected MoveLinePoint _moveLinePoint;

        private const string Message1 = "Select a street.";
        private const string Message2 = "Drag a vertex to move.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _crosshairHandle;
                }
            }
        }

        #endregion

        public MoveStreetVertexTool()
        {
            _name = "Move Street Vertex";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_moveLinePoint == null)
            {
                _moveLinePoint = new MoveLinePoint(ScreenDisplay, _color2, 1);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y) // TryMove
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();

                            if (Session.User.GetGroup().Name == "AND")
                            {
                                if (_selectedStreetAND == null)
                                {
                                    IPolyline line = _selectedStreet.Polyline;

                                    if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                    {
                                        if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedStreet.PointCount - 1)
                                        {
                                            break;
                                        }
                                        _moveLinePoint.Index = hitSegmentIndex;
                                        _selectedVertex = _streetVertices[hitSegmentIndex];
                                        _moveLinePoint.Start(point);
                                        _progress = Progress.Moving;
                                        break;
                                    }

                                    List<GStreet> streets = SelectStreet(repo, point);
                                    if (streets.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _selectedStreet = null;
                                        _selectedStreetLine.Stop();
                                        foreach (MovePoint movePoint in _streetVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _streetVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GStreet street = streets[0];
                                    if (!_selectedStreet.Equals(street))
                                    {
                                        if (street.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }

                                        _selectedStreetLine.Stop();
                                        _moveLinePoint.Line = street.Polyline;
                                        _selectedStreetLine.Line = street.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _streetVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _streetVertices.Clear();
                                        foreach (IPoint vertex in street.Vertices)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _streetVertices.Add(movePoint);
                                        }
                                        _selectedStreet = street;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                }
                                else { 
                                IPolyline line = _selectedStreetAND.Polyline;

                                if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                {
                                    if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedStreetAND.PointCount - 1)
                                    {
                                        break;
                                    }
                                    _moveLinePoint.Index = hitSegmentIndex;
                                    _selectedVertex = _streetVertices[hitSegmentIndex];
                                    _moveLinePoint.Start(point);
                                    _progress = Progress.Moving;
                                    break;
                                }

                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);
                                if (streetsAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedStreetAND = null;
                                    _selectedStreetLine.Stop();
                                    foreach (MovePoint movePoint in _streetVertices)
                                    {
                                        movePoint.Stop();
                                    }
                                    _streetVertices.Clear();
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                GStreetAND streetAND = streetsAND[0];
                                    if (!_selectedStreetAND.Equals(streetAND))
                                    {
                                        if (streetAND.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }

                                        _selectedStreetLine.Stop();
                                        _moveLinePoint.Line = streetAND.Polyline;
                                        _selectedStreetLine.Line = streetAND.Polyline;
                                        _selectedStreetLine.Start(point);
                                        foreach (MovePoint movePoint in _streetVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _streetVertices.Clear();
                                        foreach (IPoint vertex in streetAND.Vertices)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _streetVertices.Add(movePoint);
                                        }
                                        _selectedStreet = streetAND;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    break;
                                }
                            }
                            else  // others User Group
                            {
                                IPolyline line = _selectedStreet.Polyline;

                                if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                {
                                    if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedStreet.PointCount - 1)
                                    {
                                        break;
                                    }
                                    _moveLinePoint.Index = hitSegmentIndex;
                                    _selectedVertex = _streetVertices[hitSegmentIndex];
                                    _moveLinePoint.Start(point);
                                    _progress = Progress.Moving;
                                    break;
                                }

                                List<GStreet> streets = SelectStreet(repo, point);
                                if (streets.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedStreet = null;
                                    _selectedStreetLine.Stop();
                                    foreach (MovePoint movePoint in _streetVertices)
                                    {
                                        movePoint.Stop();
                                    }
                                    _streetVertices.Clear();
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                GStreet street = streets[0];
                                if (!_selectedStreet.Equals(street))
                                {
                                    if (street.IsNavigationReady)
                                    {
                                        if (!Session.User.CanDo(Command.NavigationItem))
                                        {
                                            throw new NavigationControlException();
                                        }
                                    }

                                    _selectedStreetLine.Stop();
                                    _moveLinePoint.Line = street.Polyline;
                                    _selectedStreetLine.Line = street.Polyline;
                                    _selectedStreetLine.Start(point);
                                    foreach (MovePoint movePoint in _streetVertices)
                                    {
                                        movePoint.Stop();
                                    }
                                    _streetVertices.Clear();
                                    foreach (IPoint vertex in street.Vertices)
                                    {
                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                        movePoint.Point = vertex;
                                        movePoint.Start(point);
                                        _streetVertices.Add(movePoint);
                                    }
                                    _selectedStreet = street;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                    break;
                                }
                            }
                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _moveLinePoint.MoveTo(point);
                        _selectedVertex.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y) // Select, Moving
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GStreet> streets = SelectStreet(repo, point);
                                    List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        if (streets.Count < 1 && streetsAND.Count < 1) // no street exist
                                        {
                                            _selectedStreet = null;
                                            _selectedStreetAND = null;
                                            _selectedStreetLine.Stop();
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                        else if (streets.Count > 0 && streetsAND.Count == 0) // street ADM exist
                                        {
                                            GStreet street = streets[0];
                                            if (street.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            // Check for User Own Work Area & Activity Within Work Area
                                            ANDStreetTools andStreet = new ANDStreetTools();

                                            andStreet.CheckWithinWorkArea(SegmentName, street);
                                            andStreet.CheckUserOwnWorkArea(SegmentName, street);

                                            _moveLinePoint.Line = street.Polyline;
                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in street.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreet = street;
                                            _progress = Progress.TryMove;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else // Street AND exist
                                        {
                                            GStreetAND streetAND = streetsAND[0];

                                            // Check for User Own Work Area & Activity Within Work Area
                                            ANDStreetTools andStreet = new ANDStreetTools();

                                            andStreet.CheckWithinWorkArea(SegmentName, streetAND);
                                            andStreet.CheckUserOwnWorkArea(SegmentName, streetAND);

                                            _moveLinePoint.Line = streetAND.Polyline;
                                            _selectedStreetLine.Line = streetAND.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in streetAND.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreetAND = streetAND;
                                            _progress = Progress.TryMove;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                    }
                                    else  // Others User Group
                                    {
                                        if (streets.Count > 0)
                                        {
                                            GStreet street = streets[0];
                                            if (street.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                            //if (Session.User.GetGroup().Name != "SUPERVISOR")
                                            //{
                                            //    if (street.AreaId != null)
                                            //    {
                                            //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                                            //    }
                                            //}

                                            // noraini ali - Aug 2021 - check feature lock by AND
                                            ANDStreetTools andStreet = new ANDStreetTools();
                                            andStreet.CheckFeatureLock(street.AreaId, SegmentName);
                                            // end add

                                            _moveLinePoint.Line = street.Polyline;
                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in street.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreet = street;
                                            _progress = Progress.TryMove;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            _selectedStreet = null;
                                            _selectedStreetLine.Stop();
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button == MouseKey.Left)
                            {
                                try
                                {
                                    using (new WaitCursor())
                                    {
                                        IPolyline line = (IPolyline)_moveLinePoint.Stop();
                                        _selectedVertex.Stop();

                                        if (Session.User.GetGroup().Name == "AND")
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                ValidateWorkAreaAndStatus validateUser = new ValidateWorkAreaAndStatus();
                                                ANDStreetTools andStreet = new ANDStreetTools();
                                                validateUser.CheckWithinWorkArea(SegmentName, line);
                                                if (_selectedStreetAND == null)
                                                {
                                                    GStreetAND insertedStreetAND = _selectedStreet.GetStreetANDId();
                                                    if (insertedStreetAND == null)
                                                    {
                                                        andStreet.CreateStreetAND(repo, _selectedStreet);
                                                        insertedStreetAND = _selectedStreet.GetStreetANDId();
                                                    }
                                                    
                                                    _selectedStreetAND = insertedStreetAND;
                                                }
                                                _selectedStreetAND.Shape = line;
                                                andStreet.UpdateModifiedByUserAND(_selectedStreetAND); // update Modified by & date
                                                repo.UpdateGraphic(_selectedStreetAND, true);
                                                repo.UpdateGraphicByAND(_selectedStreet, true);  // udpate status to the selected street
                                            });

                                            _selectedStreetLine.Stop();
                                            _moveLinePoint.Line = line;
                                            _selectedStreetLine.Line = line;
                                            _selectedStreetLine.Start(point);
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            foreach (IPoint vertex in _selectedStreetAND.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _progress = Progress.TryMove;
                                            break;
                                        }
                                        else // other User Group
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                _selectedStreet.Shape = line;
                                                repo.UpdateGraphic(_selectedStreet, true);
                                            });

                                            _selectedStreetLine.Stop();
                                            _moveLinePoint.Line = line;
                                            _selectedStreetLine.Line = line;
                                            _selectedStreetLine.Start(point);
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            foreach (IPoint vertex in _selectedStreet.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _progress = Progress.TryMove;
                                            break;
                                        }
                                    }
                                }
                                catch
                                {
                                    _moveLinePoint.Stop();
                                    _selectedStreetLine.Stop();
                                    foreach (MovePoint movePoint in _streetVertices)
                                    {
                                        movePoint.Stop();
                                    }
                                    _streetVertices.Clear();
                                    _selectedStreet = null;
                                    _selectedStreetAND = null;
                                    _progress = Progress.Select;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    throw;
                                }
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_moveLinePoint != null)
            {
                _moveLinePoint.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Earthworm.AO;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Sessions;
using Geomatic.UI.Commands;
using Geomatic.MapTool.JunctionTools;

namespace Geomatic.MapTool.StreetTools
{
    public partial class ReshapeStreetTool : Tool
    {
        private enum Progress
        {
            Select = 0,
            Drawing
        }

        #region Fields

        protected MovePolygon _area;
        protected NewLine _newLine;
        protected IRgbColor _color = ColorUtils.Get(Color.Green);
        private bool _panStarted;
        private int _panningHandle;
        private Progress _progress;
        private GStreet _selectedStreet;

        private const string Message1 = "Select a street.";
        private const string Message2 = "Middle click to pan. Control + click to end draw. Right click for menu.";

        public override int Cursor
        {
            get
            {
                if (_panStarted)
                {
                    return _panningHandle;
                }
                else
                {
                    return base.Cursor;
                }
            }
        }

        #endregion

        public ReshapeStreetTool()
        {
            _name = "Reshape Street";
            InitMenu();
            _panningHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPanning);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_area == null)
            {
                _area = new MovePolygon(ScreenDisplay, _color, _color, 1);
            }
            if (_newLine == null)
            {
                _newLine = new NewLine(ScreenDisplay, _color, 1.4);
            }
            OnReported(Message1);
            OnStepReported(Message1);
            _panStarted = false;
            _progress = Progress.Select;
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    return;
                }
                ScreenDisplay.PanStart(point);
                _panStarted = true;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (!_area.IsStarted)
            {
                _area.Polygon = (IPolygon)point.Buffer(5);
                _area.Start(point);
            }
            _area.MoveTo(point);
            _newLine.MoveTo(point);
            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    ScreenDisplay.PanMoveTo(point);
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                if (button == MouseKey.Left)
                {
                    if (_progress == Progress.Select)
                    {
                        using (new WaitCursor())
                        {
                            List<GStreet> streets = SelectStreet(repo, point);
                            if (streets.Count > 0)
                            {
                                GStreet street = streets[0];
                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    andStreet.CheckWithinWorkArea(SegmentName, street);
                                    andStreet.CheckUserOwnWorkArea(SegmentName, street);
                                }
                                else
                                {
                                    // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                    //if (Geomatic.Core.Sessions.Session.User.GetGroup().Name != "SUPERVISOR")
                                    //{
                                    //    if (street.AreaId != null)
                                    //    {
                                    //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                                    //    }
                                    //}

                                    // noraini ali - Aug 2021 - check feature lock by AND
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    andStreet.CheckFeatureLock(street.AreaId, SegmentName);
                                    // end add
                                }
                                if (street.IsNavigationReady)
                                {
                                    if (!Session.User.CanDo(Command.NavigationItem))
                                    {
                                        throw new NavigationControlException();
                                    }
                                }
                                _selectedStreet = street;
                                _newLine.Start(_selectedStreet.GetFromNode().Point);
                                _newLine.MoveTo(point);
                                _progress = Progress.Drawing;
                                OnReported(Message2);
                                OnStepReported(Message2);
                            }
                        }
                    }
                    else if (_progress == Progress.Drawing)
                    {
                        _newLine.AddPoint(point);
                        if (shift == KeyCode.Ctrl)
                        {
                            using (new WaitCursor())
                            {
                                _newLine.Stop();
                                _newLine.ToPoint = _selectedStreet.GetToNode().Point;
                                if (_newLine.Length < 5)
                                {
                                    throw new QualityControlException("Unable to create street with length lesser than 5 meter.");
                                }

                                _selectedStreet.Length = _newLine.Length;
                                _selectedStreet.Shape = _newLine.Geometry;
                                repo.StartTransaction(() =>
                                {
                                    if (Session.User.GetGroup().Name == "AND") //check if AND User
                                    {
                                        ANDStreetTools andStreet = new ANDStreetTools();
                                        GStreetAND _selectedStreetAND = _selectedStreet.GetStreetANDId(); //check if street already exist in AND Table
                                        if (_selectedStreetAND == null) //street not yet exist in AND Table
                                        {
                                            //insert current street to AND street
                                            andStreet.CreateStreetAND(repo, _selectedStreet);
                                            _selectedStreetAND = _selectedStreet.GetStreetANDId();

                                            // Get FromNode and ToNode (Junction)
                                            GJunction FromNodeADM = _selectedStreet.GetFromNode();
                                            GJunction ToNodeADM = _selectedStreet.GetToNode();

                                            //Insert Junction AND
                                            ANDJunctionTools andJunction = new ANDJunctionTools();
                                            andJunction.CreateJunctionAND(repo, FromNodeADM);
                                            andJunction.CreateJunctionAND(repo, ToNodeADM);

                                            //Get Inserted Street AND and Junction AND
                                            GJunctionAND FromNodeAND = FromNodeADM.GetJunctionANDId();
                                            GJunctionAND ToNodeAND = ToNodeADM.GetJunctionANDId();

                                            //Update the Street AND to the new Junction AND
                                            _selectedStreetAND.FromNodeId = FromNodeAND.OID;
                                            _selectedStreetAND.ToNodeId = ToNodeAND.OID;
                                            repo.UpdateGraphic(_selectedStreetAND, true);

                                            //Reshape the Street AND
                                            andStreet.Reshape(repo, _selectedStreet, _newLine);
                                        }
                                        else //street exist in AND Table
                                        {
                                            andStreet.Reshape(repo, _selectedStreet, _newLine);
                                        }
                                    }
                                    else //not an AND User
                                    {
                                        repo.UpdateGraphic(_selectedStreet, true);
                                    }
                                });

                                _selectedStreet = null;
                                _progress = Progress.Select;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    if (_progress == Progress.Select)
                    {
                        MapDocument.ShowContextMenu(x, y);
                    }
                    else if (_progress == Progress.Drawing)
                    {
                        MapDocument.ShowContextMenu(contextMenu, x, y);
                    }
                }
                else if (button == MouseKey.Middle)
                {
                    if (_panStarted)
                    {
                        ScreenDisplay.PanStop();
                        _panStarted = false;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        private void OnUndoLast(object sender, EventArgs e)
        {
            if (_newLine.PointCount >= 2)
            {
                _newLine.RemoveLastPoint();
            }
        }

        private void OnCancel(object sender, EventArgs e)
        {
            if (_progress != Progress.Drawing)
            {
                return;
            }
            _newLine.Stop();
            _selectedStreet = null;
            _progress = Progress.Select;
            OnReported(Message1);
            OnStepReported(Message1);
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        protected override void Reset()
        {
            if (_progress == Progress.Drawing)
            {
                if (_newLine != null)
                {
                    _newLine.Stop();
                }
                _selectedStreet = null;
                _progress = Progress.Select;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
        }

        public override bool Deactivate()
        {
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }
            if (_newLine != null)
            {
                _newLine.Stop();
                _newLine = null;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
            if (_newLine != null)
            {
                _newLine.Refresh(hDC);
            }
        }
    }
}

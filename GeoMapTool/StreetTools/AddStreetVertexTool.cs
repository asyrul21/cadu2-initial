﻿using Earthworm.AO;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.MapTool.JunctionTools;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.StreetTools
{
    public class AddStreetVertexTool : StreetVertexTool
    {
        private enum Progress
        {
            Select = 0,
            AddVertex
        }

        #region Fields

        private Progress _progress;

        private const string Message1 = "Select a street.";
        private const string Message2 = "Click a point to add vertex.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _crosshairHandle;
                }
            }
        }

        #endregion

        public AddStreetVertexTool()
        {
            _name = "Add Street Vertex";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {

                                    List<GStreet> streets = SelectStreet(repo, point);
                                    List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                    if (streets.Count == 0 && streetsAND.Count == 0)
                                    {
                                        _selectedStreet = null;
                                        _selectedStreetAND = null;
                                        _selectedStreetLine.Stop();
                                        foreach (MovePoint movePoint in _streetVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _streetVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        throw new Exception("Selection must be a street!");
                                    }

                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        ANDStreetTools andStreet = new ANDStreetTools();

                                        if (streets.Count > 0 && streetsAND.Count == 0)
                                        {
                                            GStreet street = streets[0];
                                            if (street.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            // Check for User Own Work Area & Activity Within Work Area 
                                            andStreet.CheckWithinWorkArea(SegmentName, street);
                                            andStreet.CheckUserOwnWorkArea(SegmentName, street);

                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in street.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }

                                            _selectedStreet = street;
                                            _selectedStreetAND = null;
                                            _progress = Progress.AddVertex;

                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            GStreetAND streetAND = streetsAND[0];

                                            // Check for User Own Work Area & Activity Within Work Area 
                                            andStreet.CheckWithinWorkArea(SegmentName, streetAND);
                                            andStreet.CheckUserOwnWorkArea(SegmentName, streetAND);

                                            _selectedStreetLine.Line = streetAND.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in streetAND.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreetAND = streetAND;
                                            _progress = Progress.AddVertex;

                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                    }
                                    else // Others User Group
                                    {
                                        if (streets.Count > 0)
                                        {
                                            GStreet street = streets[0];
                                            if (street.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in street.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreet = street;
                                            _progress = Progress.AddVertex;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            _selectedStreet = null;
                                            _selectedStreetLine.Stop();
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.AddVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 4, 1);
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        if(_selectedStreet != null && _selectedStreetAND == null)
                                        {
                                            GStreet street = _selectedStreet;
                                            repo.StartTransaction(() =>
                                            {
                                                ANDStreetTools andStreet = new ANDStreetTools();
                                                if (string.IsNullOrEmpty(street.AndStatus.ToString()) || (street.AndStatus == 0))
                                                {
                                                    //insert current street to AND street
                                                    andStreet.CreateStreetAND(repo, street);

                                                    //// Get FromNode and ToNode (Junction)
                                                    //GJunction FromNodeADM = street.GetFromNode();
                                                    //GJunction ToNodeADM = street.GetToNode();
                                                    //
                                                    ////Insert Junction AND
                                                    //ANDJunctionTools andJunction = new ANDJunctionTools();
                                                    //andJunction.CreateJunctionAND(repo, FromNodeADM);
                                                    //andJunction.CreateJunctionAND(repo, ToNodeADM);

                                                    //Get Inserted Street AND and Junction AND
                                                    GStreetAND newStreetAND = street.GetStreetANDId();
                                                    //GJunctionAND FromNodeAND = FromNodeADM.GetJunctionANDId();
                                                    //GJunctionAND ToNodeAND = ToNodeADM.GetJunctionANDId();

                                                    //Update the Street AND to the new Junction AND
                                                    //newStreetAND.FromNodeId = FromNodeAND.OID;
                                                    //newStreetAND.ToNodeId = ToNodeAND.OID;
                                                    andStreet.UpdateModifiedByUserAND(newStreetAND); // update Modified by & date
                                                    repo.UpdateGraphic(newStreetAND,true);
                                                    repo.UpdateGraphicByAND(street, true); // update STATUS for ADM street
                                                }
                                            });

                                            GStreetAND addedStreetAND = street.GetStreetANDId();

                                            _selectedStreetLine.Line = addedStreetAND.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (IPoint vertex in addedStreetAND.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreetAND = addedStreetAND;
                                        } 

                                        IPolyline line = _selectedStreetAND.Polyline;
                                        if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                        {
                                            try
                                            {
                                                if (_selectedStreetAND.TryAddVertex(point))
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        repo.UpdateGraphic(_selectedStreetAND,true);

                                                        _selectedStreetLine.Stop();
                                                        _selectedStreetLine.Line = _selectedStreetAND.Polyline;
                                                        _selectedStreetLine.Start(point);
                                                        foreach (MovePoint movePoint in _streetVertices)
                                                        {
                                                            movePoint.Stop();
                                                        }
                                                        _streetVertices.Clear();
                                                        foreach (IPoint vertex in _selectedStreetAND.Vertices)
                                                        {
                                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                            movePoint.Point = vertex;
                                                            movePoint.Start(point);
                                                            _streetVertices.Add(movePoint);
                                                        }
                                                    });
                                                }
                                            }
                                            catch
                                            {
                                                _selectedStreetLine.Stop();
                                                foreach (MovePoint movePoint in _streetVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _streetVertices.Clear();
                                                _selectedStreet = null;
                                                _selectedStreetAND = null;
                                                _progress = Progress.Select;

                                                OnReported(Message1);
                                                OnStepReported(Message1);
                                                throw;
                                            }
                                            break;
                                        }

                                        List<GStreetAND> streetsAND = SelectStreetAND(repo, point);
                                        if (streetsAND.Count == 0)
                                        {
                                            _progress = Progress.Select;
                                            _selectedStreetAND = null;
                                            _selectedStreetLine.Stop();
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }

                                        GStreetAND streetAND = streetsAND[0];
                                        if (!_selectedStreet.Equals(streetAND))
                                        {
                                            if (streetAND.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            _selectedStreetLine.Stop();
                                            _selectedStreetLine.Line = streetAND.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            foreach (IPoint vertex in streetAND.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreetAND = streetAND;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }
                                    }
                                    else  //Others User Group
                                    {
                                        IPolyline line = _selectedStreet.Polyline;
                                        if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                        {
                                            try
                                            {
                                                if (_selectedStreet.TryAddVertex(point))
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        repo.UpdateGraphic(_selectedStreet,true);

                                                        _selectedStreetLine.Stop();
                                                        _selectedStreetLine.Line = _selectedStreet.Polyline;
                                                        _selectedStreetLine.Start(point);
                                                        foreach (MovePoint movePoint in _streetVertices)
                                                        {
                                                            movePoint.Stop();
                                                        }
                                                        _streetVertices.Clear();
                                                        foreach (IPoint vertex in _selectedStreet.Vertices)
                                                        {
                                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                            movePoint.Point = vertex;
                                                            movePoint.Start(point);
                                                            _streetVertices.Add(movePoint);
                                                        }
                                                    });
                                                }
                                            }
                                            catch
                                            {
                                                _selectedStreetLine.Stop();
                                                foreach (MovePoint movePoint in _streetVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _streetVertices.Clear();
                                                _selectedStreet = null;
                                                _progress = Progress.Select;
                                                OnReported(Message1);
                                                OnStepReported(Message1);
                                                throw;
                                            }
                                            break;
                                        }

                                        List<GStreet> streets = SelectStreet(repo, point);
                                        if (streets.Count == 0)
                                        {
                                            _progress = Progress.Select;
                                            _selectedStreet = null;
                                            _selectedStreetLine.Stop();
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }

                                        GStreet street = streets[0];
                                        if (!_selectedStreet.Equals(street))
                                        {
                                            if (street.IsNavigationReady)
                                            {
                                                if (!Session.User.CanDo(Command.NavigationItem))
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }

                                            _selectedStreetLine.Stop();
                                            _selectedStreetLine.Line = street.Polyline;
                                            _selectedStreetLine.Start(point);
                                            foreach (MovePoint movePoint in _streetVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _streetVertices.Clear();
                                            foreach (IPoint vertex in street.Vertices)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _streetVertices.Add(movePoint);
                                            }
                                            _selectedStreet = street;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

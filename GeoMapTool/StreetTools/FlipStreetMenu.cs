﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public partial class FlipStreetTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuFlip;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuFlip = new ToolStripMenuItem();
            // 
            // menuFlip
            //
            this.menuFlip.Name = "menuFlip";
            this.menuFlip.Text = "Flip";
            this.menuFlip.Click += new EventHandler(this.OnFlip);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuFlip});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

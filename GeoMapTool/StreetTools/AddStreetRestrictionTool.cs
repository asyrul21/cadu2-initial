﻿using Earthworm.AO;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.StreetTools
{
    public partial class AddStreetRestrictionTool : AddTool
    {
        protected enum Progress
        {
            SelectStart = 0,
            SelectEnd
        }

        #region Fields

        protected List<MoveLine> _selectedStreetLines;
        protected List<GStreet> _selectedStreets;
        protected Progress _progress;
        private const string Message1 = "Select start street.";

        #endregion

        public AddStreetRestrictionTool()
        {
            _name = "Add Street Restriction";
            InitMenu();
            _selectedStreetLines = new List<MoveLine>();
            _selectedStreets = new List<GStreet>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.SelectStart;
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.SelectStart:
                        {
                            if (button == MouseKey.Left)
                            {
                                List<GStreet> streets;
                                GStreet street = null;
                                using (new WaitCursor())
                                {
                                    streets = SelectStreet(repo, point);

                                    if (streets.Count == 0)
                                    {
                                        return;
                                    }

                                    street = streets[0];

                                    if (street.Direction == 4)
                                    {
                                        throw new QualityControlException("Unable to add cannot go through street.");
                                    }

                                    _selectedStreets.Add(street);

                                    MoveLine moveLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
                                    moveLine.Line = street.Polyline;
                                    moveLine.Start(point);
                                    _selectedStreetLines.Add(moveLine);

                                    _progress = Progress.SelectEnd;
                                    OnReported("Select end street {0}. Right click for menu.", _selectedStreets.Count);
                                    OnStepReported("Select end street {0}. Right click for menu.", _selectedStreets.Count);
                                }
                                break;
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.SelectEnd:
                        {
                            if (button == MouseKey.Left)
                            {
                                List<GStreet> streets;
                                GStreet street = null;
                                using (new WaitCursor())
                                {
                                    streets = SelectStreet(repo, point);

                                    if (streets.Count == 0)
                                    {
                                        return;
                                    }

                                    street = streets[0];

                                    if (_selectedStreets.Contains(street))
                                    {
                                        throw new QualityControlException("Unable to add same street.");
                                    }

                                    if (street.Direction == 4)
                                    {
                                        throw new QualityControlException("Unable to add cannot go through street.");
                                    }

                                    GStreet previousStreet1 = _selectedStreets[_selectedStreets.Count - 1];

                                    if (!previousStreet1.IsConnectedTo(street))
                                    {
                                        throw new QualityControlException("Street not connected.");
                                    }

                                    int newConnectedNodeId = street.ConnectedAt(previousStreet1);

                                    if (_selectedStreets.Count >= 2)
                                    {
                                        GStreet previousStreet2 = _selectedStreets[_selectedStreets.Count - 2];

                                        int connectedNodeId = previousStreet2.ConnectedAt(previousStreet1);

                                        if (newConnectedNodeId == connectedNodeId)
                                        {
                                            throw new QualityControlException("Unable to continue with this street.");
                                        }
                                    }

                                    if (previousStreet1.Direction == 2)
                                    {
                                        if (previousStreet1.FromNodeId == newConnectedNodeId)
                                        {
                                            throw new QualityControlException("Street direction conflict.");
                                        }
                                    }

                                    if (previousStreet1.Direction == 3)
                                    {
                                        if (previousStreet1.ToNodeId == newConnectedNodeId)
                                        {
                                            throw new QualityControlException("Street direction conflict.");
                                        }
                                    }

                                    if (street.Direction == 2)
                                    {
                                        if (street.ToNodeId == newConnectedNodeId)
                                        {
                                            throw new QualityControlException("Street direction conflict.");
                                        }
                                    }

                                    if (street.Direction == 3)
                                    {
                                        if (street.FromNodeId == newConnectedNodeId)
                                        {
                                            throw new QualityControlException("Street direction conflict.");
                                        }
                                    }

                                    _selectedStreets.Add(street);

                                    MoveLine moveLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
                                    moveLine.Line = street.Polyline;
                                    moveLine.Start(point);
                                    _selectedStreetLines.Add(moveLine);

                                    if (_selectedStreets.Count == 11)
                                    {
                                        CreateStreetRestriction();
                                    }
                                    else
                                    {
                                        OnReported("Select end street {0}. Right click for menu.", _selectedStreets.Count);
                                        OnStepReported("Select end street {0}. Right click for menu.", _selectedStreets.Count);
                                    }
                                }
                                break;
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(contextMenu, x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private void CreateStreetRestriction()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            if (_selectedStreets.Count >= 2)
            {
                GStreetRestriction newStreetRestriction = repo.NewObj<GStreetRestriction>();
                newStreetRestriction.Init();

                GJunction junction;

                if (_selectedStreets.Count > 2)
                {
                    newStreetRestriction.Status = 1;
                }
                else
                {
                    newStreetRestriction.Status = 0;
                }

                if (_selectedStreets[0].FromNodeId == _selectedStreets[0].ConnectedAt(_selectedStreets[1]))
                {
                    newStreetRestriction.JunctionId = _selectedStreets[0].ToNodeId;
                }
                else
                {
                    newStreetRestriction.JunctionId = _selectedStreets[0].FromNodeId;
                }

                junction = newStreetRestriction.GetJunction();
                if (junction == null)
                {
                    throw new Exception("Junction not found.");
                }

                newStreetRestriction.StartId = _selectedStreets[0].OID;
                newStreetRestriction.EndId1 = _selectedStreets[1].OID;

                if (_selectedStreets.Count >= 3)
                {
                    newStreetRestriction.EndId2 = _selectedStreets[2].OID;
                }

                if (_selectedStreets.Count >= 4)
                {
                    newStreetRestriction.EndId3 = _selectedStreets[3].OID;
                }

                if (_selectedStreets.Count >= 5)
                {
                    newStreetRestriction.EndId4 = _selectedStreets[4].OID;
                }

                if (_selectedStreets.Count >= 6)
                {
                    newStreetRestriction.EndId5 = _selectedStreets[5].OID;
                }

                if (_selectedStreets.Count >= 7)
                {
                    newStreetRestriction.EndId6 = _selectedStreets[6].OID;
                }

                if (_selectedStreets.Count >= 8)
                {
                    newStreetRestriction.EndId7 = _selectedStreets[7].OID;
                }

                if (_selectedStreets.Count >= 9)
                {
                    newStreetRestriction.EndId8 = _selectedStreets[8].OID;
                }

                if (_selectedStreets.Count >= 10)
                {
                    newStreetRestriction.EndId9 = _selectedStreets[9].OID;
                }

                if (_selectedStreets.Count >= 11)
                {
                    newStreetRestriction.EndId10 = _selectedStreets[10].OID;
                }

                IPolyline line = _selectedStreets[0].Polyline.Clone();

                for (int count = 1; count < _selectedStreets.Count; count++)
                {
                    line.Add(_selectedStreets[count].Polyline.Clone());
                }

                if (!line.FromPoint.Equals2(junction.Point))
                {
                    line.ReverseOrientation();
                }

                IPoint centrePoint1 = line.Envelope.CentrePoint();

                line.Scale(junction.Point, 0.5, 0.5);

                IPoint centrePoint2 = line.Envelope.CentrePoint();

                double x = centrePoint1.X - centrePoint2.X;
                double y = centrePoint1.Y - centrePoint2.Y;

                line.Move(x, y);

                newStreetRestriction.Shape = line;

                bool success = false;

                try
                {
                    using (new WaitCursor())
                    {
                        repo.StartTransaction();
                    }

                    newStreetRestriction = repo.Insert(newStreetRestriction, false);

                    using (AddStreetRestrictionForm form = new AddStreetRestrictionForm(newStreetRestriction))
                    {
                        if (form.ShowDialog() != DialogResult.OK)
                        {
                            throw new ProcessCancelledException();
                        }

                        using (new WaitCursor())
                        {
                            form.SetValues();
                            repo.Update(newStreetRestriction);
                            OnReported("New street restriction created. {0}", newStreetRestriction.OID);
                            OnReported(Message1);
                            OnStepReported(Message1);

                            _progress = Progress.SelectStart;
                            foreach (MoveLine moveLine in _selectedStreetLines)
                            {
                                moveLine.Stop();
                            }
                            _selectedStreetLines.Clear();
                            _selectedStreets.Clear();
                        }
                    }
                    success = true;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (success)
                    {
                        repo.EndTransaction();
                    }
                    else
                    {
                        repo.AbortTransaction();
                    }
                }
            }
        }

        private void OnEnd(object sender, EventArgs e)
        {
            CreateStreetRestriction();
        }

        private void OnCancel(object sender, EventArgs e)
        {
            _progress = Progress.SelectStart;
            foreach (MoveLine moveLine in _selectedStreetLines)
            {
                moveLine.Stop();
            }
            _selectedStreetLines.Clear();
            _selectedStreets.Clear();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.SelectStart;
            foreach (MoveLine moveLine in _selectedStreetLines)
            {
                moveLine.Stop();
            }
            _selectedStreetLines.Clear();
            _selectedStreets.Clear();
        }

        public override bool Deactivate()
        {
            _progress = Progress.SelectStart;
            foreach (MoveLine moveLine in _selectedStreetLines)
            {
                moveLine.Stop();
            }
            _selectedStreetLines.Clear();
            _selectedStreets.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLines != null)
            {
                foreach (MoveLine moveLine in _selectedStreetLines)
                {
                    moveLine.Refresh(hDC);
                }
            }
        }
    }
}

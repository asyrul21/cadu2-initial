﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.StreetTools
{
    public class DeleteStreetRestrictionTool : DeleteTool
    {
        public DeleteStreetRestrictionTool()
        {
            _name = "Delete Street Restriction";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a street restriction to delete.");
            OnStepReported("Select a street restriction to delete.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GStreetRestriction> restrictions;

                    GStreetRestriction restriction = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        restrictions = repo.SpatialSearch<GStreetRestriction>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (restrictions.Count == 1)
                    {
                        restriction = restrictions[0];
                    }
                    else if (restrictions.Count > 1)
                    {
                        restriction = ChooseFeature(restrictions, null);
                    }

                    if (restriction == null)
                    {
                        return;
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Delete Street Restriction")
                                  .SetText("Are you sure you want to delete this street restriction?");

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        if (restriction.IsNavigationReady)
                        {
                            if (!Session.User.CanDo(Command.NavigationItem))
                            {
                                throw new NavigationControlException();
                            }
                        }

                        repo.StartTransaction(() =>
                        {
                            repo.Delete(restriction);
                        });

                        OnReported("Select a street restriction to delete.");
                        OnStepReported("Select a street restriction to delete.");
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

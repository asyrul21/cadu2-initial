﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Earthworm.AO;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;

namespace Geomatic.MapTool.StreetTools
{
    public abstract class StreetVertexTool : Tool
    {
        #region Fields

        protected GStreet _selectedStreet;
        protected GStreetAND _selectedStreetAND;
        protected MoveLine _selectedStreetLine;
        protected List<MovePoint> _streetVertices;
        protected int _crosshairHandle;

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        //protected GStreetAND _selectedStreetAND;
        #endregion

        public StreetVertexTool()
        {
            _streetVertices = new List<MovePoint>();
            _crosshairHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            OnReported("Select a street.");
            OnStepReported("Select a street.");
        }

        protected List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        protected List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreetAND>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedStreet = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
            foreach (MovePoint movePoint in _streetVertices)
            {
                movePoint.Stop();
            }
            _streetVertices.Clear();
        }

        public override bool Deactivate()
        {
            _selectedStreet = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            foreach (MovePoint movePoint in _streetVertices)
            {
                movePoint.Stop();
            }
            _streetVertices.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
            if (_streetVertices != null)
            {
                foreach (MovePoint movePoint in _streetVertices)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;
using ESRI.ArcGIS.SystemUI;

namespace Geomatic.MapTool
{
    public abstract class AddTool : HighlightTool
    {
        #region Fields

        private int _pencilHandle;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                return _pencilHandle;
            }
        }

        #endregion

        public AddTool()
        {
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }
    }
}

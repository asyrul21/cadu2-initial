﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Search;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool
{
    public abstract class VerifyTool : ChooseTool
    {
        protected enum Progress
        {
            Select = 0,
            SelectParent
        }

        protected IRgbColor _color = ColorUtils.Get(Color.DarkGreen);
        protected Progress _progress;
        protected MovePolygon _parentRadius;

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;

            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
        }

        protected List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GStreet>(query).ToList();
        }

        protected List<GProperty> SelectProperty(RepositoryFactory repo, IPoint point)
        {
            Query<GProperty> query = new Query<GProperty>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GProperty>(query).ToList();
        }

        protected List<GBuilding> SelectBuilding(RepositoryFactory repo, IPoint point)
        {
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuilding>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
        }
    }
}

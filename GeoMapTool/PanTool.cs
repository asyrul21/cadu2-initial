﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI;

namespace Geomatic.MapTool
{
    public sealed class PanTool : Tool
    {
        #region Fields

        private int _panHandle;
        private int _panningHandle;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (InProgress)
                {
                    return _panningHandle;
                }
                else
                {
                    return _panHandle;
                }
            }
        }

        #endregion

        public PanTool()
        {
            _panHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPan);
            _panningHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPanning);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (!InProgress)
                {
                    ScreenDisplay.PanStart(point);
                    InProgress = true;
                }
            }
            else if (button == MouseKey.Right)
            {
                if (!InProgress)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    ScreenDisplay.PanMoveTo(point);
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    ScreenDisplay.PanStop();
                    InProgress = false;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public partial class AddBuildingPolygonTool : Tool
    {
        #region Fields

        protected NewPolygon _newPolygon;

        #endregion

        public AddBuildingPolygonTool()
        {
            _name = "Add Building Polygon";
            InitMenu();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_newPolygon == null)
            {
                _newPolygon = new NewPolygon(ScreenDisplay, ColorUtils.Select, 2);
            }
            OnReported("Start draw building polygon. Control + click to end draw.");
            OnStepReported("Start draw building polygon. Control + click to end draw.");
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                if (button == MouseKey.Left)
                {
                    if (!InProgress)
                    {
                        _newPolygon.Start(point);
                        InProgress = true;
                        OnReported("Control + click to end draw. Right click for menu.");
                        OnStepReported("Control + click to end draw. Right click for menu.");
                    }
                    else
                    {
                        _newPolygon.AddPoint(point);
                        if (shift == KeyCode.Ctrl)
                        {
                            try
                            {
                                _newPolygon.Stop();
                                CreateBuildingPolygon();
                            }
                            catch
                            {
                                throw;
                            }
                            finally
                            {
                                InProgress = false;
                            }
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    if (!InProgress)
                    {
                        MapDocument.ShowContextMenu(x, y);
                    }
                    else
                    {
                        MapDocument.ShowContextMenu(contextMenu, x, y);
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                _newPolygon.MoveTo(point);
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        private void OnUndoLast(object sender, EventArgs e)
        {
            if (_newPolygon.PointCount >= 3)
            {
                _newPolygon.RemoveLastPoint();
            }
        }

        private void OnRedraw(object sender, EventArgs e)
        {
            if (!InProgress)
            {
                return;
            }
            _newPolygon.Stop();
            InProgress = false;
            OnReported("Start draw building polygon. Control + click to end draw.");
            OnStepReported("Start draw building polygon. Control + click to end draw.");
        }

        private void CreateBuildingPolygon()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            using (new WaitCursor())
            {
                repo.StartTransaction(() =>
                {
                    GBuildingPolygon newBuildingPolygon = repo.NewObj<GBuildingPolygon>();
                    newBuildingPolygon.Init();
                    newBuildingPolygon.Shape = _newPolygon.Geometry;
                    newBuildingPolygon = repo.Insert(newBuildingPolygon);
                    OnReported("New building polygon created. {0}", newBuildingPolygon.OID);
                    OnReported("Start draw building polygon. Control + click to end draw.");
                    OnStepReported("Start draw building polygon. Control + click to end draw.");
                });
            }
        }

        protected override void Reset()
        {
            if (InProgress)
            {
                if (_newPolygon != null)
                {
                    _newPolygon.Stop();
                }
                InProgress = false;
            }
        }

        public override bool Deactivate()
        {
            if (_newPolygon != null)
            {
                _newPolygon.Stop();
                _newPolygon = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_newPolygon != null)
            {
                _newPolygon.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public class DeleteBuildingPolygonTool : DeleteTool
    {
        private const string Message1 = "Select a building polygon to delete.";

        public DeleteBuildingPolygonTool()
        {
            _name = "Delete Building Polygon";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GBuildingPolygon> buildingPolygons;

                    GBuildingPolygon buildingPolygon = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        buildingPolygons = repo.SpatialSearch<GBuildingPolygon>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (buildingPolygons.Count == 1)
                    {
                        buildingPolygon = buildingPolygons[0];
                    }
                    else if (buildingPolygons.Count > 1)
                    {
                        buildingPolygon = ChooseFeature(buildingPolygons, null);
                    }

                    if (buildingPolygon == null)
                    {
                        return;
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Delete Building Polygon")
                                  .SetText("Are you sure you want to delete this building polygon?\nId: {0}", buildingPolygon.OID);

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        repo.StartTransaction(() =>
                        {
                            OnReported("Building polygon deleted. {0}", buildingPolygon.OID);
                            repo.Delete(buildingPolygon);
                        });

                        OnReported("Select a building polygon to delete.");
                        OnStepReported("Select a building polygon to delete.");
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

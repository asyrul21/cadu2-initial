﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Repositories;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Search;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Utilities;
using Earthworm.AO;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public class MoveBuildingPolygonTool : MoveTool
    {
        #region Fields

        protected GBuildingPolygon _selectedBuildingPolygon;
        protected MovePolygon _selectedBuildingPolygonPolygon;

        private const string Message1 = "Select a building polygon to move.";
        private const string Message2 = "Drag selected building polygon to move.";

        #endregion

        public MoveBuildingPolygonTool()
        {
            _name = "Move Building Polygon";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedBuildingPolygonPolygon == null)
            {
                _selectedBuildingPolygonPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Intersects(_selectedBuildingPolygon.Polygon))
                            {
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }
                        _selectedBuildingPolygonPolygon.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                                if (buildingPolygons.Count > 0)
                                {
                                    GBuildingPolygon buildingPolygon = buildingPolygons[0];
                                    _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                    _selectedBuildingPolygonPolygon.Start(point);
                                    _selectedBuildingPolygon = buildingPolygon;

                                    _progress = Progress.TryMove;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                                // no building polygon found
                                if (buildingPolygons.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedBuildingPolygonPolygon.Stop();
                                    _selectedBuildingPolygon = null;
                                    break;
                                }

                                // take 1st building polygon
                                GBuildingPolygon buildingPolygon = buildingPolygons[0];
                                if (!_selectedBuildingPolygon.Equals(buildingPolygon))
                                {
                                    _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                    _selectedBuildingPolygonPolygon.Stop();
                                    _selectedBuildingPolygonPolygon.Start(point);
                                    _selectedBuildingPolygon = buildingPolygon;
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            IPolygon newPolygon = _selectedBuildingPolygonPolygon.Stop() as IPolygon;
                            _progress = Progress.Select;

                            repo.StartTransaction(() =>
                            {
                                _selectedBuildingPolygon.Shape = newPolygon;
                                repo.Update(_selectedBuildingPolygon);
                            });
                            _selectedBuildingPolygon = null;
                        }
                        OnReported(Message1);
                        OnStepReported(Message1);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected List<GBuildingPolygon> SelectBuildingPolygon(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingPolygon> query = new Query<GBuildingPolygon>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuildingPolygon>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;

            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Stop();
                _selectedBuildingPolygonPolygon = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.Core.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using ESRI.ArcGIS.Display;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public class DeleteBuildingPolygonVertexTool : BuildingPolygonVertexTool
    {
        private enum Progress
        {
            Select = 0,
            DeleteVertex
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _pencilHandle;
                }
            }
        }

        #endregion

        public DeleteBuildingPolygonVertexTool()
        {
            _name = "Delete Building Polygon Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                                    if (buildingPolygons.Count > 0)
                                    {
                                        GBuildingPolygon buildingPolygon = buildingPolygons[0];
                                        _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                        _selectedBuildingPolygonPolygon.Start(point);
                                        List<IPoint> vertices = buildingPolygon.Vertices;
                                        for (int count = 0; count < buildingPolygon.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _buildingPolygonVertices.Add(movePoint);
                                        }
                                        _selectedBuildingPolygon = buildingPolygon;
                                        _progress = Progress.DeleteVertex;
                                        OnReported("Select a vertex to delete.");
                                        OnStepReported("Select a vertex to delete.");
                                    }
                                    else
                                    {
                                        _selectedBuildingPolygon = null;
                                        _selectedBuildingPolygonPolygon.Stop();
                                        foreach (MovePoint movePoint in _buildingPolygonVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _buildingPolygonVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.DeleteVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = 1;
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    IPolygon polygon = _selectedBuildingPolygon.Polygon;
                                    if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                    {
                                        if (_selectedBuildingPolygon.PointCount <= 4)
                                        {
                                            throw new QualityControlException("Unable to remove point ");
                                        }

                                        if (_selectedBuildingPolygon.TryDeleteVertex(hitSegmentIndex))
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                repo.Update(_selectedBuildingPolygon);

                                                _selectedBuildingPolygonPolygon.Stop();
                                                _selectedBuildingPolygonPolygon.Polygon = _selectedBuildingPolygon.Polygon;
                                                _selectedBuildingPolygonPolygon.Start(point);
                                                foreach (MovePoint movePoint in _buildingPolygonVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _buildingPolygonVertices.Clear();
                                                List<IPoint> vertices = _selectedBuildingPolygon.Vertices;
                                                for (int count = 0; count < _selectedBuildingPolygon.PointCount - 1; count++)
                                                {
                                                    IPoint vertex = vertices[count];
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                    movePoint.Point = vertex;
                                                    movePoint.Start(point);
                                                    _buildingPolygonVertices.Add(movePoint);
                                                }
                                            });
                                            OnReported("Select a vertex to delete.");
                                            OnStepReported("Select a vertex to delete.");
                                        }
                                        break;
                                    }

                                    List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                                    if (buildingPolygons.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _selectedBuildingPolygon = null;
                                        _selectedBuildingPolygonPolygon.Stop();
                                        foreach (MovePoint movePoint in _buildingPolygonVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _buildingPolygonVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GBuildingPolygon buildingPolygon = buildingPolygons[0];
                                    if (!_selectedBuildingPolygon.Equals(buildingPolygon))
                                    {
                                        _selectedBuildingPolygonPolygon.Stop();
                                        _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                        _selectedBuildingPolygonPolygon.Start(point);
                                        foreach (MovePoint movePoint in _buildingPolygonVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _buildingPolygonVertices.Clear();
                                        List<IPoint> vertices = buildingPolygon.Vertices;
                                        for (int count = 0; count < buildingPolygon.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _buildingPolygonVertices.Add(movePoint);
                                        }
                                        _selectedBuildingPolygon = buildingPolygon;
                                        OnReported("Select a vertex to delete.");
                                        OnStepReported("Select a vertex to delete.");
                                        break;
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

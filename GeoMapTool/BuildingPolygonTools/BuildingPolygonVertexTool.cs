﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Features;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public abstract class BuildingPolygonVertexTool : Tool
    {
        #region Fields

        protected GBuildingPolygon _selectedBuildingPolygon;
        protected MovePolygon _selectedBuildingPolygonPolygon;
        protected List<MovePoint> _buildingPolygonVertices;

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        protected const string Message1 = "Select a building polygon.";

        #endregion

        public BuildingPolygonVertexTool()
        {
            _buildingPolygonVertices = new List<MovePoint>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedBuildingPolygonPolygon == null)
            {
                _selectedBuildingPolygonPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        protected List<GBuildingPolygon> SelectBuildingPolygon(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingPolygon> query = new Query<GBuildingPolygon>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuildingPolygon>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedBuildingPolygon = null;
            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Stop();
            }
            foreach (MovePoint movePoint in _buildingPolygonVertices)
            {
                movePoint.Stop();
            }
            _buildingPolygonVertices.Clear();
        }

        public override bool Deactivate()
        {
            _selectedBuildingPolygon = null;
            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Stop();
                _selectedBuildingPolygonPolygon = null;
            }
            foreach (MovePoint movePoint in _buildingPolygonVertices)
            {
                movePoint.Stop();
            }
            _buildingPolygonVertices.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedBuildingPolygonPolygon != null)
            {
                _selectedBuildingPolygonPolygon.Refresh(hDC);
            }
            if (_buildingPolygonVertices != null)
            {
                foreach (MovePoint movePoint in _buildingPolygonVertices)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.FeedBacks;
using Geomatic.UI;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.UI.Forms.MessageBoxes;
using ESRI.ArcGIS.Display;

namespace Geomatic.MapTool.BuildingPolygonTools
{
    public class MoveBuildingPolygonVertexTool : BuildingPolygonVertexTool
    {
        private enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;
        protected MovePoint _selectedVertex;
        protected MovePolygonPoint _movePolygonPoint;
        private const string Message2 = "Drag a vertex to move.";

        #endregion

        #region Properties

        public MoveBuildingPolygonVertexTool()
        {
            _name = "Move Building Polygon Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        #endregion

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_movePolygonPoint == null)
            {
                _movePolygonPoint = new MovePolygonPoint(ScreenDisplay, _color2);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            double searchRadius = 1;
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();
                            IPolygon polygon = _selectedBuildingPolygon.Polygon;

                            if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                            {
                                if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedBuildingPolygon.PointCount - 1)
                                {
                                    hitSegmentIndex = 0;
                                }
                                _movePolygonPoint.Index = hitSegmentIndex;
                                _selectedVertex = _buildingPolygonVertices[hitSegmentIndex];
                                _movePolygonPoint.Start(point);
                                _progress = Progress.Moving;
                                break;
                            }

                            List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                            if (buildingPolygons.Count == 0)
                            {
                                _progress = Progress.Select;
                                _selectedBuildingPolygon = null;
                                _selectedBuildingPolygonPolygon.Stop();
                                foreach (MovePoint movePoint in _buildingPolygonVertices)
                                {
                                    movePoint.Stop();
                                }
                                _buildingPolygonVertices.Clear();
                                OnReported(Message1);
                                OnStepReported(Message1);
                                break;
                            }

                            GBuildingPolygon buildingPolygon = buildingPolygons[0];
                            if (!_selectedBuildingPolygon.Equals(buildingPolygon))
                            {
                                _selectedBuildingPolygonPolygon.Stop();
                                _movePolygonPoint.Polygon = buildingPolygon.Polygon;
                                _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                _selectedBuildingPolygonPolygon.Start(point);
                                foreach (MovePoint movePoint in _buildingPolygonVertices)
                                {
                                    movePoint.Stop();
                                }
                                _buildingPolygonVertices.Clear();

                                List<IPoint> vertices = buildingPolygon.Vertices;
                                for (int count = 0; count < buildingPolygon.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _buildingPolygonVertices.Add(movePoint);
                                }
                                _selectedBuildingPolygon = buildingPolygon;
                                OnReported(Message2);
                                OnStepReported(Message2);
                                break;
                            }
                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }
                        _movePolygonPoint.MoveTo(point);
                        _selectedVertex.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GBuildingPolygon> buildingPolygons = SelectBuildingPolygon(repo, point);
                                    if (buildingPolygons.Count > 0)
                                    {
                                        GBuildingPolygon buildingPolygon = buildingPolygons[0];
    
                                        _movePolygonPoint.Polygon = buildingPolygon.Polygon;
                                        _selectedBuildingPolygonPolygon.Polygon = buildingPolygon.Polygon;
                                        _selectedBuildingPolygonPolygon.Start(point);
                                        List<IPoint> vertices = buildingPolygon.Vertices;
                                        for (int count = 0; count < buildingPolygon.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _buildingPolygonVertices.Add(movePoint);
                                        }
                                        _selectedBuildingPolygon = buildingPolygon;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedBuildingPolygon = null;
                                        _selectedBuildingPolygonPolygon.Stop();
                                        foreach (MovePoint movePoint in _buildingPolygonVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _buildingPolygonVertices.Clear();
                                        OnReported("Select a street.");
                                        OnStepReported("Select a street.");
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            using (new WaitCursor())
                            {
                                IPolygon polygon = (IPolygon)_movePolygonPoint.Stop();
                                _selectedVertex.Stop();

                                repo.StartTransaction(() =>
                                {
                                    _selectedBuildingPolygon.Shape = polygon;
                                    repo.Update(_selectedBuildingPolygon);
                                });

                                _selectedBuildingPolygonPolygon.Stop();
                                _movePolygonPoint.Polygon = polygon;
                                _selectedBuildingPolygonPolygon.Polygon = polygon;
                                _selectedBuildingPolygonPolygon.Start(point);
                                foreach (MovePoint movePoint in _buildingPolygonVertices)
                                {
                                    movePoint.Stop();
                                }
                                _buildingPolygonVertices.Clear();
                                List<IPoint> vertices = _selectedBuildingPolygon.Vertices;
                                for (int count = 0; count < _selectedBuildingPolygon.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _buildingPolygonVertices.Add(movePoint);
                                }
                                _progress = Progress.TryMove;
                                break;
                            }
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_movePolygonPoint != null)
            {
                _movePolygonPoint.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;

namespace Geomatic.MapTool
{
    public class ToolReportedEventArgs : EventArgs
    {
        public ITool Tool { private set; get; }
        public string Message { private set; get; }

        public ToolReportedEventArgs(ITool tool, string message)
        {
            Tool = tool;
            Message = message;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI;

namespace Geomatic.MapTool
{
    public sealed class ZoomInTool : Tool
    {
        #region Fields

        private IRubberBand _rubberBand;
        private int _zoomInHandle;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                return _zoomInHandle;
            }
        }

        #endregion

        public ZoomInTool()
        {
            _zoomInHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorZoomIn);

            _rubberBand = new RubberEnvelopeClass();
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (point.IsEmpty)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                if (!InProgress)
                {
                    try
                    {
                        InProgress = true;
                        IGeometry geometry = _rubberBand.TrackNew(ScreenDisplay, null);

                        if (geometry == null)
                            return;

                        if (geometry.Envelope.IsEmpty || (geometry.Envelope.Width == 0 && geometry.Envelope.Height == 0))
                        {
                            IEnvelope envelope = ActiveView.Extent;
                            envelope.Expand(0.5, 0.5, true);
                            envelope.CenterAt(point);
                            ActiveView.Extent = envelope;
                            ActiveView.Refresh();
                        }
                        else
                        {
                            ActiveView.Extent = geometry.Envelope;
                            ActiveView.Refresh();
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        InProgress = false;
                    }
                }
            }
            else if (button == MouseKey.Right)
            {
                if (!InProgress)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
        }
    }
}

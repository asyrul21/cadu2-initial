﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.ADF.BaseClasses;
using ESRI.ArcGIS.Controls;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using Geomatic.UI;
using Geomatic.Core;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;

namespace Geomatic.MapTool
{
    /// <summary>
    /// Tool base class
    /// Error is consumed in overriding method. 
    /// try to catch the error or if you can fix the bug.
    /// </summary>
    public abstract class Tool : BaseTool, IToolEvent, IToolHook
    {
        public event EventHandler<ToolReportedEventArgs> Reported;
        public event EventHandler<ToolStepReportedEventArgs> StepReported;

        protected bool InProgress { set; get; }
        protected GeoMapDocument MapDocument { set; get; }
        protected SegmentName SegmentName { set; get; }
        protected string _name;
        private int _arrowHandle;
        private IHookHelper _hookHelper;

        public override int Cursor
        {
            get
            {
                return _arrowHandle;
            }
        }

        protected IHookHelper HookHelper
        {
            get
            {
                if (_hookHelper == null)
                    _hookHelper = new HookHelperClass();
                return _hookHelper;
            }
        }

        protected IActiveView ActiveView
        {
            get
            {
                return HookHelper.ActiveView;
            }
        }

        protected IScreenDisplay ScreenDisplay
        {
            get
            {
                return ActiveView.ScreenDisplay;
            }
        }

        protected IDisplayTransformation DisplayTransformation
        {
            get
            {
                return ScreenDisplay.DisplayTransformation;
            }
        }

        protected bool IsActive
        {
            get
            {
                if (HookHelper.Hook == null)
                {
                    return false;
                }
                if (HookHelper.Hook is IMapControl4)
                {
                    IMapControl4 mapControl = (IMapControl4)HookHelper.Hook;
                    return mapControl.CurrentTool == this;
                }
                if (HookHelper.Hook is ToolbarControl)
                {
                    ToolbarControl toolbarControl = (ToolbarControl)HookHelper.Hook;
                    return toolbarControl.CurrentTool == this;
                }

                throw new Exception("Unknown hook.");
            }
        }

        public Tool()
        {
            _arrowHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorArrow);
        }

        public override void OnClick()
        {
            InProgress = false;
        }

        public override void OnCreate(object hook)
        {
            HookHelper.Hook = hook;
        }

        public override bool Deactivate()
        {
            InProgress = false;
            OnStepReported("");
            return base.Deactivate();
        }

        public void SetHook(object hook)
        {
            HookHelper.Hook = hook;
        }

        public virtual void OnDocumentChanged(GeoMapDocument mapDocument)
        {
            MapDocument = mapDocument;
        }

        protected virtual void Reset()
        {
        }

        public virtual void OnSegmentChanged(SegmentName segmentName)
        {
            SegmentName = segmentName;
            if (IsActive)
            {
                Reset();
            }
        }

        public virtual void OnUndo()
        {
            if (IsActive)
            {
                Reset();
            }
        }

        public virtual void OnRedo()
        {
            if (IsActive)
            {
                Reset();
            }
        }

        public virtual void OnSave()
        {
            if (IsActive)
            {
                Reset();
            }
        }

        protected void OnReported(string format, params object[] args)
        {
            OnReported(string.Format(format, args));
        }

        protected void OnReported(string message)
        {
            if (Reported != null)
                Reported(this, new ToolReportedEventArgs(this, message));
        }

        protected void OnStepReported(string format, params object[] args)
        {
            OnStepReported(string.Format(format, args));
        }

        protected void OnStepReported(string message)
        {
            if (StepReported != null)
                StepReported(this, new ToolStepReportedEventArgs(this, message));
        }
    }
}

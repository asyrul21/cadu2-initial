﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI.Forms.Choose;
using Geomatic.Core.Features;
using Geomatic.Core.Exceptions;
using System.Windows.Forms;
using Geomatic.Core.Repositories;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.MapTool
{
    public abstract class ChooseTool : SingleLayerTool
    {
        protected const int POINT_SIZE = 10;
        protected const int LINE_WIDTH = 2;

        protected T ChooseFeature<T>(List<T> features, T suggestedFeature) where T : IGFeature
        {
            using (ChooseFeatureForm chooseForm = ChooseFormFactory.CreateForm(features))
            {
                chooseForm.SuggestedFeature = suggestedFeature;
                chooseForm.SelectedFeatureChanged += new EventHandler<SelectedFeatureChangedEventArgs>(ChooseForm_SelectedFeatureChanged);
                DialogResult result = chooseForm.ShowDialog();
                chooseForm.SelectedFeatureChanged -= ChooseForm_SelectedFeatureChanged;
                _toolLayer.Clear();
                if (result != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                return (T)chooseForm.SelectedFeature;
            }
        }

        // added by asyrul
        protected IEnumerable<IGFeature> ChooseMultipleBuilding(GBuildingGroup buildingGroup)
        {
            using (ChooseBuildingByDistanceForm chooseForm = new ChooseBuildingByDistanceForm(buildingGroup))
            {
                chooseForm.SelectedFeatureChanged += new EventHandler<SelectedFeatureChangedEventArgs>(ChooseForm_SelectedFeatureChanged);
                DialogResult result = chooseForm.ShowDialog();
                chooseForm.SelectedFeatureChanged -= ChooseForm_SelectedFeatureChanged;
                _toolLayer.Clear();
                if (result != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }
                return chooseForm.SelectedFeatures;
            }
        }
        // added end

        protected IEnumerable<IGFeature> ChooseBuildingToDissociate(GBuildingGroup buildingGroup)
        {
            using (ChooseBuildingToDissociateForm chooseForm = new ChooseBuildingToDissociateForm(buildingGroup))
            {
                chooseForm.SelectedFeatureChanged += new EventHandler<SelectedFeatureChangedEventArgs>(ChooseForm_SelectedFeatureChanged);
                DialogResult result = chooseForm.ShowDialog();
                chooseForm.SelectedFeatureChanged -= ChooseForm_SelectedFeatureChanged;
                _toolLayer.Clear();
                if (result != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }
                return chooseForm.SelectedFeatures;
            }
        }

        private void ChooseForm_SelectedFeatureChanged(object sender, SelectedFeatureChangedEventArgs e)
        {
            //_toolLayer.Clear(); // comment coz this code will clear the highlighted feature
            if (e.Feature != null)
            {
                //_toolLayer.Add(e.Feature); comment coz this code will clear the highlighted feature

                // noraini ali - OKT 2020
                // to highlight feature in list ChooseFeature form
                HighlightSelectionFeature(e.Feature);
            }
        }

        private void HighlightSelectionFeature(IGFeature feature)
        {
            IGeometry geometry = feature.Shape;
            switch (geometry.GeometryType)
            {
                case esriGeometryType.esriGeometryPoint:
                    IPoint point = new PointClass();
                    point.PutCoords(geometry.Envelope.XMin, geometry.Envelope.YMax);
                    MovePoint movePoint = new MovePoint(ScreenDisplay, ColorUtils.Get(255, 0, 0), ColorUtils.Get(0, 0, 0), POINT_SIZE);
                    movePoint.Point = point;
                    movePoint.Start(point);
                    break;

                case esriGeometryType.esriGeometryPolyline:
                    IPolyline polyline = (IPolyline)geometry;
                    MoveLine moveline = new MoveLine(ScreenDisplay, ColorUtils.Get(255, 0, 0), LINE_WIDTH);
                    moveline.Line = polyline;
                    moveline.Start(polyline.FromPoint);
                    break;

                default:
                    break;
            }
        }
    }
}

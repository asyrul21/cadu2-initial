﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;
using System.Drawing;

namespace Geomatic.MapTool
{
    public abstract class MoveTool : HighlightTool
    {
        protected enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private int _crossHandle;
        protected Progress _progress;
        protected IRgbColor _color = ColorUtils.Get(Color.DarkGreen);

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Moving)
                {
                    return _crossHandle;
                }
                return base.Cursor;
            }
        }

        #endregion

        public MoveTool()
        {
            _crossHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }
    }
}

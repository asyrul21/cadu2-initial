﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using Geomatic.Core;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.PoiTools
{
    public class EditPoiTool : EditTool
    {
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public EditPoiTool()
        {
            _name = "Edit Poi";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a poi to edit.");
            OnStepReported("Select a poi to edit.");

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GPoi> pois;

                    GPoi poi = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        pois = repo.SpatialSearch<GPoi>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    if (pois.Count == 1)
                    {
                        poi = pois[0];
                    }
                    else if (pois.Count > 1)
                    {
                        poi = ChooseFeature(pois, null);
                    }

                    if (poi == null)
                    {
                        return;
                    }

                    highlightPOI(repo, poi, true, _selectedFeaturePoints);

                    bool success = false;

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        using (EditPoiForm form = new EditPoiForm(poi))
                        {
                            if (form.ShowDialog() != DialogResult.OK)
                            {
                                return;
                            }
                            using (new WaitCursor())
                            {
                                form.SetValues();
                                repo.Update(poi);
                            }
                        }
                        OnReported("Poi updated. {0}", poi.OID);
                        OnReported("Select a poi to edit.");
                        OnStepReported("Select a poi to edit.");
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        success = true;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

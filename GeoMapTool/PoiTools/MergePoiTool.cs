﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Earthworm.AO;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Forms.MergePoi;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.PoiTools
{
    public class MergePoiTool : EditTool
    {
        private enum Progress
        {
            SelectFromFeature = 0,
            SelectToFeature
        }

        private Progress _progress;

        private const string Message1 = "Select 1st feature (property, building, landmark). Middle click to pan.";
        private const string Message2 = "Select 2nd feature (property, building, landmark). Middle click to pan.";

        private IGPoiParent _poiParent1;
        private IGPoiParent _poiParent2;
        private bool _panStarted;
        private int _panningHandle;

        // noraini ali - OKT 2020 - highlight feature
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public override int Cursor
        {
            get
            {
                if (_panStarted)
                {
                    return _panningHandle;
                }
                else
                {
                    return base.Cursor;
                }
            }
        }

        public MergePoiTool()
        {
            _name = "Merge Poi";
            _panningHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPanning);
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
            _progress = Progress.SelectFromFeature;
            _panStarted = false;

            // noraini ali - NOV 2020
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();

        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    return;
                }
                ScreenDisplay.PanStart(point);
                _panStarted = true;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    ScreenDisplay.PanMoveTo(point);
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.SelectFromFeature:
                        {
                            if (button == MouseKey.Left)
                            {
                                List<IGPoiParent> features;
                                using (new WaitCursor())
                                {
                                    features = repo.SpatialSearch<GBuilding>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>();
                                    features.AddRange(repo.SpatialSearch<GLandmark>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>());
                                    features.AddRange(repo.SpatialSearch<GProperty>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>());
                                }

                                if (features.Count > 0)
                                {
                                    if (features[0] is GProperty)
                                    {
                                        GProperty property = (GProperty)features[0];
                                        if (property.HasBuilding())
                                        {
                                            using (InfoMessageBox box = new InfoMessageBox())
                                            {
                                                box.SetText("Property has building.");
                                                box.Show();
                                            }
                                            return;
                                        }
                                        highlightProperty(repo, property.OID, true, _selectedFeaturePoints);
                                    }
                                    // noraini ali - NOV 2020
                                    else if (features[0] is GBuilding)
                                    {
                                        GBuilding building = (GBuilding)features[0];
                                        highlightBuilding(repo, building.OID, true, _selectedFeaturePoints);
                                    }
                                    else if (features[0] is GLandmark)
                                    {
                                        GLandmark landmark = (GLandmark)features[0];
                                        highlightLandmark(repo, landmark.OID, true, _selectedFeaturePoints);
                                    }

                                    _poiParent1 = features[0];
                                    
                                    _progress = Progress.SelectToFeature;

                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            else if (button == MouseKey.Middle)
                            {
                                if (_panStarted)
                                {
                                    ScreenDisplay.PanStop();
                                    _panStarted = false;
                                }
                            }
                            break;
                        }
                    case Progress.SelectToFeature:
                        {
                            if (button == MouseKey.Left)
                            {
                                List<IGPoiParent> features;
                                using (new WaitCursor())
                                {
                                    features = repo.SpatialSearch<GBuilding>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>();
                                    features.AddRange(repo.SpatialSearch<GLandmark>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>());
                                    features.AddRange(repo.SpatialSearch<GProperty>(point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)), esriSpatialRelEnum.esriSpatialRelIntersects).ToList<IGPoiParent>());
                                }

                                if (features.Count > 0)
                                {
                                    if (_poiParent1.Equals(features[0]))
                                    {
                                        using (InfoMessageBox box = new InfoMessageBox())
                                        {
                                            box.SetText("Unable to select same feature.");
                                            box.Show();
                                        }
                                        return;
                                    }

                                    if (features[0] is GProperty)
                                    {
                                        GProperty property = (GProperty)features[0];
                                        if (property.HasBuilding())
                                        {
                                            using (InfoMessageBox box = new InfoMessageBox())
                                            {
                                                box.SetText("Property has building.");
                                                box.Show();
                                            }
                                            return;
                                        }
                                        highlightProperty(repo, property.OID, true, _selectedFeaturePoints);
                                    }
                                    // noraini ali - NOV 2020
                                    else if (features[0] is GBuilding)
                                    {
                                        GBuilding building = (GBuilding)features[0];
                                        highlightBuilding(repo, building.OID, true, _selectedFeaturePoints);
                                    }
                                    else if (features[0] is GLandmark)
                                    {
                                        GLandmark landmark = (GLandmark)features[0];
                                        highlightLandmark(repo, landmark.OID, true, _selectedFeaturePoints);
                                    }

                                    _poiParent2 = features[0];
                                   
                                    _progress = Progress.SelectFromFeature;

                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    //highlightClear(_selectedFeaturePoints, _selectedFeatureLines);

                                    using (MergePoiForm form = new MergePoiForm(_poiParent1, _poiParent2))
                                    {
                                        if (form.ShowDialog() == DialogResult.OK)
                                        {
                                            using (new WaitCursor())
                                            {
                                                repo.StartTransaction(() =>
                                                {
                                                    int type1;
                                                    if (_poiParent1 is GProperty)
                                                    {
                                                        type1 = (int)GPoi.ParentClass.Property1;
                                                    }
                                                    else if (_poiParent1 is GBuilding)
                                                    {
                                                        type1 = (int)GPoi.ParentClass.Building;
                                                    }
                                                    else if (_poiParent1 is GLandmark)
                                                    {
                                                        type1 = (int)GPoi.ParentClass.Landmark;
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("Unknown parent type.");
                                                    }

                                                    int type2;
                                                    if (_poiParent2 is GProperty)
                                                    {
                                                        type2 = (int)GPoi.ParentClass.Property1;
                                                    }
                                                    else if (_poiParent2 is GBuilding)
                                                    {
                                                        type2 = (int)GPoi.ParentClass.Building;
                                                    }
                                                    else if (_poiParent2 is GLandmark)
                                                    {
                                                        type2 = (int)GPoi.ParentClass.Landmark;
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("Unknown parent type.");
                                                    }

                                                    foreach (GPoi poi in form.Pois1)
                                                    {
                                                        poi.ParentType = type1;
                                                        poi.ParentId = _poiParent1.OID;
                                                        poi.Shape = _poiParent1.Shape;
                                                        repo.Update(poi);
                                                    }
                                                    foreach (GPoi poi in form.Pois2)
                                                    {
                                                        poi.ParentType = type2;
                                                        poi.ParentId = _poiParent2.OID;
                                                        poi.Shape = _poiParent2.Shape;
                                                        repo.Update(poi);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            else if (button == MouseKey.Middle)
                            {
                                if (_panStarted)
                                {
                                    ScreenDisplay.PanStop();
                                    _panStarted = false;
                                }
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            if (InProgress)
            {
                InProgress = false;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
            _poiParent1 = null;
            _poiParent2 = null;
        }

        public override bool Deactivate()
        {
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
            _poiParent1 = null;
            _poiParent2 = null;
            return base.Deactivate();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool.PoiTools
{
    public class ShowPoiStreetTool : ChooseTool
    {
        #region Fields

        protected GPoi _selectedPoi;
        protected MoveLine _selectedStreetLine;
        protected MovePoint _selectedPoiPoint;
        protected IRgbColor _color1 = ColorUtils.Get(Color.Red);
       private const string Message1 = "Select a poi to show street.";

        #endregion

        public ShowPoiStreetTool()
        {
            _name = "Show Poi Street";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _selectedPoi = null;
            if (_selectedPoiPoint == null)
            {
                _selectedPoiPoint = new MovePoint(ScreenDisplay, ColorUtils.Select, POINT_SIZE);
            }
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, _color1, 2);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                if (button == MouseKey.Left)
                {
                    using (new WaitCursor())
                    {
                        List<GPoi> pois = SelectPoi(repo, point);
                        if (pois.Count > 0)
                        {
                            GPoi poi;
                            if (pois.Count == 1)
                            {
                                poi = pois[0];
                            }
                            else
                            {
                                poi = ChooseFeature(pois, null);
                            }
                            if (_selectedPoi == null)
                            {
                                _selectedPoi = poi;
                                _selectedPoiPoint.Point = poi.Point;
                                _selectedPoiPoint.Start(point);

                                GStreet street = _selectedPoi.GetStreet();
                                if (street != null)
                                {
                                    _selectedStreetLine.Line = street.Polyline;
                                    _selectedStreetLine.Start(point);
                                }
                            }
                            else
                            {
                                if (!_selectedPoi.Equals(poi))
                                {
                                    _selectedPoi = poi;
                                    _selectedPoiPoint.Point = poi.Point;
                                    _selectedPoiPoint.Restart(point);

                                    GStreet street = _selectedPoi.GetStreet();
                                    _selectedStreetLine.Stop();
                                    if (street != null)
                                    {
                                        _selectedStreetLine.Line = street.Polyline;
                                        _selectedStreetLine.Start(point);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _selectedPoi = null;
                            _selectedPoiPoint.Stop();
                            _selectedStreetLine.Stop();
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GPoi> SelectPoi(RepositoryFactory repo, IPoint point)
        {
            Query<GPoi> query = new Query<GPoi>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GPoi>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedPoi = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
            if (_selectedPoiPoint != null)
            {
                _selectedPoiPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _selectedPoi = null;
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            if (_selectedPoiPoint != null)
            {
                _selectedPoiPoint.Stop();
                _selectedPoiPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
            if (_selectedPoiPoint != null)
            {
                _selectedPoiPoint.Refresh(hDC);
            }
        }
    }
}

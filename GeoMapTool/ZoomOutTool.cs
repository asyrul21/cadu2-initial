﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI;

namespace Geomatic.MapTool
{
    public sealed class ZoomOutTool : Tool
    {
        #region Fields

        private int _zoomOutHandle;
        private IRubberBand _rubberBand;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                return _zoomOutHandle;
            }
        }

        #endregion

        public ZoomOutTool()
        {
            _zoomOutHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorZoomOut);

            _rubberBand = new RubberEnvelopeClass();
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (point.IsEmpty)
            {
                return;
            }

            if (button == MouseKey.Left)
            { 
                if (!InProgress)
                {
                    try
                    {
                        InProgress = true;
                        IGeometry geometry = _rubberBand.TrackNew(ScreenDisplay, null);

                        if (geometry == null)
                            return;

                        if (geometry.Envelope.IsEmpty || (geometry.Envelope.Width == 0 && geometry.Envelope.Height == 0))
                        {
                            IEnvelope envelope = ActiveView.Extent;
                            envelope.Expand(2, 2, true);
                            envelope.CenterAt(point);
                            ActiveView.Extent = envelope;
                            ActiveView.Refresh();
                        }
                        else
                        {
                            double newWidth = ActiveView.Extent.Width * (ActiveView.Extent.Width / geometry.Envelope.Width);
                            double newHeight = ActiveView.Extent.Height * (ActiveView.Extent.Height / geometry.Envelope.Height);

                            //Set the new extent coordinates
                            IEnvelope envelope = new EnvelopeClass();
                            envelope.PutCoords(ActiveView.Extent.XMin - ((geometry.Envelope.XMin - ActiveView.Extent.XMin) * (ActiveView.Extent.Width / geometry.Envelope.Width)),
                                ActiveView.Extent.YMin - ((geometry.Envelope.YMin - ActiveView.Extent.YMin) * (ActiveView.Extent.Height / geometry.Envelope.Height)),
                                (ActiveView.Extent.XMin - ((geometry.Envelope.XMin - ActiveView.Extent.XMin) * (ActiveView.Extent.Width / geometry.Envelope.Width))) + newWidth,
                                (ActiveView.Extent.YMin - ((geometry.Envelope.YMin - ActiveView.Extent.YMin) * (ActiveView.Extent.Height / geometry.Envelope.Height))) + newHeight);

                            ActiveView.Extent = envelope;
                            ActiveView.Refresh(); 
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        InProgress = false;
                    }
                }
            }
            else if (button == MouseKey.Right)
            {
                if (!InProgress)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using System.Collections.Generic;
using System.Linq;
using Geomatic.Core;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.UI.FeedBacks;
using ESRI.ArcGIS.Geometry;
using System;

namespace Geomatic.MapTool
{
    // Implementation - noraini ali - Mei 2020
    public class ValidateWorkAreaAndStatus
    {
        public void CheckUserOwnWorkArea(SegmentName SegmentName, NewLine FeatureLine)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(FeatureLine.Geometry, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR IF USER OWN WORK AREA
            area.ForEach(thearea =>
            {
                if (thearea.user_id.ToUpper() != Session.User.Name)
                {
                    throw new QualityControlException("You Does Not Own This Working Area!");
                }
                if (thearea.flag == "2" )
                {
                    throw new QualityControlException("The Work Area are currently locked. Verifying in Progress !");
                }
            });

            
        }

        public void CheckWithinWorkArea(SegmentName SegmentName, NewLine FeatureLine)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(FeatureLine.Geometry, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR WITHIN WORK AREA
            if (area.Count < 1)
            {
                System.Diagnostics.Debug.WriteLine("Area Count" + area.Count); //use
                throw new QualityControlException("Out Of Work Area Bound.");
            }
        }

        public void CheckUserOwnWorkArea(SegmentName SegmentName, GFeature feature)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(feature.Shape, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR IF USER OWN WORK AREA
            area.ForEach(thearea =>
            {
                if (thearea.user_id.ToUpper() != Session.User.Name)
                {
                    throw new QualityControlException("You Does Not Own This Working Area!");
                }
                if (thearea.flag == "2")
                {
                    throw new QualityControlException("The Work Area are currently locked. Verifying in Progress !");
                }
            });
        }

        public void CheckWithinWorkArea(SegmentName SegmentName, GFeature feature)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(feature.Shape, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR WITHIN WORK AREA
            if (area.Count < 1)
            {
                System.Diagnostics.Debug.WriteLine("Area Count "+ area.Count); //use
                throw new QualityControlException(feature.TableName.Substring (feature.TableName.LastIndexOf("_")+1) + " Out of Area Bound.");
            }
        }

        //public bool ValidateAndStatus(int ExistAndStatus, int NewAndStatus)
        //{
        //    bool status = true;
        //
        //    if (ExistAndStatus == 1)
        //    {
        //        if (NewAndStatus == 2) { status = true; }
        //        else status = false;
        //    }
        //    else if (ExistAndStatus == 3)
        //    {
        //        if (NewAndStatus == 4) { status = false; }
        //    }
        //    return status;
        //}

        public void CheckUserOwnWorkArea(SegmentName SegmentName, IPoint iPoint)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(iPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR IF USER OWN WORK AREA
            area.ForEach(thearea =>
            {
                if (thearea.user_id.ToUpper() != Session.User.Name)
                {
                    throw new QualityControlException("You Does Not Own This Working Area!");
                }
                if (thearea.flag == "2")
                {
                    throw new QualityControlException("The Work Area are currently locked. Verifying in Progress !");
                }
            });
        }

        public void CheckWithinWorkArea(SegmentName SegmentName, IPoint iPoint)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(iPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            //CHECK FOR WITHIN WORK AREA
            if (area.Count < 1)
            {
                System.Diagnostics.Debug.WriteLine("Area Count" + area.Count); //use
                throw new QualityControlException("Out Of Work Area Bound.");
            }
        }

        public void CheckWithinWorkArea(SegmentName SegmentName, IPolyline line)
        {
            //Implementation - Syafiq March 2020
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(line, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            //CHECK FOR WITHIN WORK AREA
            if (area.Count < 1)
            {
                throw new QualityControlException("Out Of Work Area Bound.");
            }
        }

        public void CheckWithinSameWorkArea(SegmentName SegmentName, IPoint iPoint, int? WorkAreaOID )
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(iPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            System.Diagnostics.Debug.WriteLine("Line " + area);

            if (area.Count == 1 && area[0].OID != WorkAreaOID)
            {
                throw new QualityControlException("Not Within the same Work Area.");
            }
        }

        public void UpdateWorkAreaCompletedFlag(RepositoryFactory repo, GWorkArea workarea)
        {
                workarea.flag = "0";
                repo.Update(workarea);
        }

        public void UpdateCreatedByUserAND(GFeature feature)
        {
            // noraini ali - Jul 2020 - Set Created by/Date Created
            feature.CreatedBy = Geomatic.Core.Sessions.Session.User.Name;
            feature.DateCreated = DateTime.Now.ToString("yyyyMMdd");
        }

        public void UpdateModifiedByUserAND(GFeature feature)
        {
            // noraini ali - Jul 2020 - Set Created by/Date Created
            feature.UpdatedBy = Geomatic.Core.Sessions.Session.User.Name;
            feature.DateUpdated = DateTime.Now.ToString("yyyyMMdd");
        }

        public int GetWorkAreaId(SegmentName SegmentName, IPoint iPoint)
        {
            // noraini ali - Jul 2020 - Get Work Area Id
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(iPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            if (area.Count > 0)
            {
                return area[0].OID;
            }
            else
            {
                return 0;
            }
        }

        // noraini ali - NOV 2020
        // validate user can edit feature or only can review
        public bool ValidateUserCanEdit(SegmentName SegmentName, int _workId)
        {
            bool _CanEdit = false;

            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            if (_workId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(_workId);
                if (workarea != null)
                {
                    if (Session.User.GetGroup().Name == "AND" && workarea.user_id.ToUpper() == Session.User.Name && workarea.flag != "2")
                    {
                        _CanEdit = true;
                    }
                }
            }
            return _CanEdit;
        }

        // noraini ali - NOV 2020
        // checking if user point in work area
        public void checkingPointInWorkArea(SegmentName SegmentName, IPoint point)
        {
            int _workAreaId = GetWorkAreaId(SegmentName, point);
            if (_workAreaId > 0 && Session.User.GetGroup().Name != "SUPERVISOR")
            {
                string msg1 = "Area LOCK by AND Working Area Id : " + _workAreaId;
                throw new QualityControlException(msg1);
            }
            //return true;
        }

        public void CheckFeatureLock(int? areaid, SegmentName SegmentName)
        {
            if (Session.User.GetGroup().Name != "SUPERVISOR")
            {
                if (areaid != null)
                {
                    RepositoryFactory repo = new RepositoryFactory(SegmentName);
                    GWorkArea work_area = repo.GetById<GWorkArea>(areaid.Value);
                    if (work_area != null)
                    {
                        string msg1 = "feature LOCK by AND Working Area id : " + areaid;
                        throw new QualityControlException(msg1);
                    }
                    else
                    {
                        string msg1 = "feature dangling! please ask ADMIN to move it.";
                        string msg2 = "\nSegment : " + SegmentName.ToString();
                        string msg3 = "\nWork Area id : " + areaid;
                        throw new QualityControlException(msg1 + msg2 + msg3);
                    }
                }
            }
        }
    }
}

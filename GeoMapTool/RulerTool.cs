﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI;
using System.Windows.Forms;

namespace Geomatic.MapTool
{
    public sealed class RulerTool : Tool
    {
        #region Fields

        private INewLineFeedback _newLineFeedback;
        private int _crossHandle;

        private const string messageDefault = "Click a point on Map. Double click to end point.";
        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (InProgress)
                {
                    return _crossHandle;
                }
                else
                {
                    return base.Cursor;
                }
            }
        }

        #endregion

        public RulerTool()
        {
            _crossHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            //added by noraini ali - Feb 2021
            OnReported(messageDefault);
            OnStepReported(messageDefault);
            // end

            if (_newLineFeedback == null)
            {
                _newLineFeedback = new NewLineFeedbackClass();
            }
            _newLineFeedback.Display = ScreenDisplay;
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (button == MouseKey.Left)
            {
                if (!InProgress)
                {
                    InProgress = true;
                    _newLineFeedback.Start(point);
                }
                else
                {
                    _newLineFeedback.AddPoint(point);
                }

                OnReported(messageDefault);
                OnStepReported(messageDefault);
            }
            else if (button == MouseKey.Right)
            {
                if (!InProgress)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (!InProgress)
                return;

            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            _newLineFeedback.MoveTo(point);
        }

        public override void OnDblClick()
        {
            if (!InProgress)
                return;

            IPolyline polyline = _newLineFeedback.Stop();
            InProgress = false;
            if (polyline == null)
                return;
            double? length = polyline.Length;
            if (!length.HasValue)
                return;
            OnReported(" Ruler Distance : {0:0.0000}", length.Value);
            OnStepReported(" Ruler Distance : {0:0.0000}", length.Value);
        }

        public override bool Deactivate()
        {
            _newLineFeedback.Stop();
            _newLineFeedback = null;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            _newLineFeedback.Refresh(hDC);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Sessions;
using Geomatic.Core;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.JunctionTools
{
    public class DeleteJunctionTool : DeleteTool
    {
        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        public DeleteJunctionTool()
        {
            _name = "Delete Junction";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a junction to delete.");
            OnStepReported("Select a junction to delete.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GJunction> junctions;
                    List<GJunctionAND> junctionsAND;

                    GJunction junction = null;
                    GJunctionAND junctionAND = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        junctions = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        junctionsAND = repo.SpatialSearch<GJunctionAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDJunctionTools andJunction = new ANDJunctionTools();
                        andJunction.CheckWithinWorkArea(SegmentName, point);
                        andJunction.CheckUserOwnWorkArea(SegmentName, point);
                        int _workAreaId = andJunction.GetWorkAreaId(SegmentName, point);

                        List<GJunction> _NewListJunctions = new List<GJunction>();
                        List<GJunctionAND> _NewListJunctionsAND = new List<GJunctionAND>();
                        List<GJunction> _junctions = new List<GJunction>(); // list to store all ADM feature to use in ChooseForm

                        // Get Junctions & JunctionsAND List
                        if (junctions.Count > 0)
                        {
                            _NewListJunctions = andJunction.FilterJunctionInWorkArea(junctions, _workAreaId);
                        }

                        if (junctionsAND.Count > 0)
                        {
                            _NewListJunctionsAND = andJunction.FilterJunctionInWorkArea(junctionsAND, _workAreaId);
                        }

                        // Add Junctions & JunctionsAND List to new list
                        foreach (GJunction junctionTemp in _NewListJunctions)
                        {
                            _junctions.Add(junctionTemp);
                        }

                        foreach (GJunctionAND junctionTempAND in _NewListJunctionsAND)
                        {
                            GJunction Junction = repo.GetById<GJunction>(junctionTempAND.OriId);
                            if (Junction != null)
                                _junctions.Add(Junction);
                        }

                        if (_junctions.Count == 0)
                        {
                            return;
                        }
                        else if (_junctions.Count == 1)
                        {
                            junction = _junctions[0];

                            // if OID exist in AND feature
                            GJunctionAND _junctionAND = junction.GetJunctionANDId();
                            if (_junctionAND != null)
                            {
                                junctionAND = _junctionAND;
                                junction = null;
                            }
                        }
                        else
                        {
                            junction = ChooseFeature(_junctions, null);

                            // if OID exist in AND feature
                            GJunctionAND _junctionAND = repo.GetById<GJunctionAND>(junction.OID);
                            if (_junctionAND != null)
                            {
                                junctionAND = _junctionAND;
                                junction = null;
                            }
                        }
                        
                        using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                        {
                            highlightJunction(repo, junction.OID, true, _selectedFeaturePoints);
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                      .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                      .SetCaption("Delete Junction")
                                      .SetText("Are you sure you want to delete this junction?\nId: {0}", junction.OID);

                            if (confirmBox.Show() == DialogResult.No)
                            {
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }
                        }

                        using (new WaitCursor())
                        {
                            if (!junction.AllStreetMustStatus2())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has street.");
                            }

                            if (junction.HasStreetRestriction())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has street restriction.");
                            }

                            if (junction.HasTollRoute())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has toll route.");
                            }

                            repo.StartTransaction(() =>
                            {
                                int oid = junction.OID;
                                andJunction.Delete(repo, junction);
                                OnReported("Junction deleted. {0}", oid);
                            });

                            OnReported("Select a junction to delete.");
                            OnStepReported("Select a junction to delete.");
                        }


                    }
                    else     //other user group
                    {
                        if (junctions.Count == 1)
                        {
                            junction = junctions[0];
                        }
                        else if (junctions.Count > 1)
                        {
                            junction = ChooseFeature(junctions, null);
                        }

                        if (junction == null)
                        {
                            return;
                        }

                        using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                        {
                            highlightJunction(repo, junction.OID, true, _selectedFeaturePoints);
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                      .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                      .SetCaption("Delete Junction")
                                      .SetText("Are you sure you want to delete this junction?\nId: {0}", junction.OID);

                            if (confirmBox.Show() == DialogResult.No)
                            {
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }
                        }

                        using (new WaitCursor())
                        {
                            if (junction.HasStreet())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has street.");
                            }

                            if (junction.HasStreetRestriction())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has street restriction.");
                            }

                            if (junction.HasTollRoute())
                            {
                                throw new QualityControlException("Unable to delete junction. Junction has toll route.");
                            }

                            repo.StartTransaction(() =>
                            {
                                int oid = junction.OID;
                                repo.Delete(junction);
                                OnReported("Junction deleted. {0}", oid);
                            });

                            OnReported("Select a junction to delete.");
                            OnStepReported("Select a junction to delete.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                finally
                {
                    InProgress = false;
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
            }
        }
    }
}

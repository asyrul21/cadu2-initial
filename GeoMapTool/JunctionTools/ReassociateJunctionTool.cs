﻿// created by asyrul on the 31st January 2019
// feature is cancelled
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Earthworm.AO;


namespace Geomatic.MapTool.JunctionTools
{
    public partial class ReassociateJunctionTool : ReassociateTool
    {
        #region Fields

        //protected List<GJunction> _selectedJunctions;
        protected GJunction _selectedJunction;
        //protected List<MovePoint> _selectedJunctionPoints;

        #endregion

        public ReassociateJunctionTool()
        {
            _name = "Reassociate Junction";
            InitMenu();
            //_selectedJunctionPoints = new List<MovePoint>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a Junction to reassociate.");
            OnStepReported("Select a Junction to reassociate.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                                List<GJunction> junctions;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    junctions = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (junctions.Count == 0)
                                    {
                                        return;
                                    }
                                    if(junctions.Count > 1)
                                    {
                                        throw new Exception("Please select only one junction!");
                                    }

                                    _selectedJunction = junctions[0];

                                    //foreach (GJunction junction in _selectedJunctions)
                                    //{
                                    //    MovePoint movePoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
                                    //    movePoint.Point = junction.Point;
                                    //    movePoint.Start(junction.Point);
                                    //    _selectedJunctionPoints.Add(movePoint);
                                    //}

                                    _progress = Progress.SelectParent;

                                    OnReported("Select a street for junction association or right-click for reselect menu.");
                                    OnStepReported("Select a street for junction association or right-click for reselect menu.");

                                }
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                //selecting street for junctions to be associated with
                                List<GStreet> streets = SelectStreet(repo, point);
                                if (streets.Count > 0)
                                {
                                    GStreet street = streets[0];

                                    //confirm box
                                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                                    confirmBox.SetCaption("Reassociate Junction")
                                              .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

                                    if (confirmBox.Show() == DialogResult.Yes)
                                    {
                                        using (new WaitCursor())
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                //foreach (GJunction junction in _selectedJunctions)
                                                //{
                                                //    //woudl cause error because junction doesnt have a street ID column
                                                //    //junction.StreetId = street.OID;
                                                //    //repo.Update(junction);
                                                //}

                                                // set initial street pointing to null
                                                //GStreet currentStreet = _selectedJunction.GetStreets().First();
                                                //currentStreet.FromNodeId = 0;
                                                //currentStreet.ToNodeId = 0;

                                                //// set new street to point to junction
                                                //street.FromNodeId = _selectedJunction.OID;
                                                //street.ToNodeId = _selectedJunction.OID;
                                                //repo.Update(currentStreet);
                                                //repo.Update(street);
                                            });
                                            OnReported("Select a junction to reassociate.");
                                            OnStepReported("Select a junction to reassociate.");
                                            Reset();
                                        }
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported("Select a junction to reassociate.");
            OnStepReported("Select a junction to reassociate.");
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedJunction = null;
            //foreach (MovePoint movePoint in _selectedJunctionPoints)
            //{
            //    movePoint.Stop();
            //}
            //_selectedJunctionPoints.Clear();
        }

    }
}

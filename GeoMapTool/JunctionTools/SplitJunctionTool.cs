﻿using Earthworm.AO;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Geomatic.MapTool.StreetTools;
using System.Windows.Forms;

namespace Geomatic.MapTool.JunctionTools
{
    public class SplitJunctionTool : Tool
    {
        protected enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        protected MovePolygon _area;
        protected GStreet _selectedStreet;
        protected GStreetAND _selectedStreetAND;
        protected MoveLine _selectedStreetLine;
        protected MoveLinePoint _moveLinePoint;
        protected IRgbColor _color = ColorUtils.Get(Color.Green);
        protected IRgbColor _color1 = ColorUtils.Get(Color.Blue);
        protected bool _from;

        private const string Message1 = "Select a street.";
        private const string Message2 = "Drag junction to split.";

        #endregion

        public SplitJunctionTool()
        {
            _name = "Split Junction";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_area == null)
            {
                _area = new MovePolygon(ScreenDisplay, _color, _color, 1);
            }
            if (_selectedStreetLine == null)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, ColorUtils.Select);
            }
            if (_moveLinePoint == null)
            {
                _moveLinePoint = new MoveLinePoint(ScreenDisplay, _color1, 1);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)  // Trymove
        {
            if (InProgress) { return; }
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left) { break; }
                        using (new WaitCursor())
                        {
                            double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();

                            if (Session.User.GetGroup().Name == "AND")
                            {
                                IPolyline line = _selectedStreetAND.Polyline;
                                if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                {
                                    if (hitSegmentIndex != 0 && hitSegmentIndex != _selectedStreetAND.PointCount - 1)
                                    {
                                        break;
                                    }
                                    _moveLinePoint.Index = hitSegmentIndex;
                                    _moveLinePoint.Start(point);
                                    _from = (hitSegmentIndex == 0);
                                    _progress = Progress.Moving;
                                    break;
                                }
                            }
                            else
                            {
                                IPolyline line = _selectedStreet.Polyline;
                                if (line.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                {
                                    if (hitSegmentIndex != 0 && hitSegmentIndex != _selectedStreet.PointCount - 1)
                                    {
                                        break;
                                    }
                                    _moveLinePoint.Index = hitSegmentIndex;
                                    _moveLinePoint.Start(point);
                                    _from = (hitSegmentIndex == 0);
                                    _progress = Progress.Moving;
                                    break;
                                }
                            }
                            break;
                        }
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress) { return; }
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        _area.MoveTo(point);
                        break;
                    case Progress.Moving:
                        _area.MoveTo(point);
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _moveLinePoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y) // Select, TryMove, Moving
        {
            if (InProgress) { return; }
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    ANDStreetTools andStreet = new ANDStreetTools();
                                    switch (streetsAND.Count)
                                    {
                                        case int n when (n > 0):  //AND EXIST
                                            {
                                                GStreetAND street = streetsAND[0];
                                                if (street.IsNavigationReady)
                                                {
                                                    if (!Session.User.CanDo(Command.NavigationItem))
                                                    {
                                                        throw new NavigationControlException();
                                                    }
                                                }
                                                _area.Polygon = (IPolygon)point.Buffer(5);
                                                _area.Start(point);
                                                _moveLinePoint.Line = street.Polyline;
                                                _selectedStreetLine.Stop();
                                                _selectedStreetLine.Line = street.Polyline;
                                                _selectedStreetLine.Start(point);
                                                _selectedStreetAND = street;
                                                _progress = Progress.TryMove;
                                                OnReported(Message2);
                                                OnStepReported(Message2);
                                            }
                                            break;

                                        case int n when (n == 0): //AND NOT EXIST
                                            if (streets.Count == 0) //street adm not exist
                                            {
                                                _area.Stop();
                                                _selectedStreet = null;
                                                _selectedStreetAND = null;
                                                _selectedStreetLine.Stop();
                                            }
                                            else if (streets.Count > 0) //street adm exist
                                            {
                                                GStreet street = streets[0];
                                                if (street.IsNavigationReady)
                                                {
                                                    if (!Session.User.CanDo(Command.NavigationItem))
                                                    {
                                                        throw new NavigationControlException();
                                                    }
                                                }
                                                andStreet.CheckWithinWorkArea(SegmentName, street);
                                                andStreet.CheckUserOwnWorkArea(SegmentName, street);
                                                using (QuestionMessageBox box = new QuestionMessageBox())
                                                {
                                                    box.SetCaption("Split Junction");
                                                    box.SetText("Confirm to split junction of this street? This will generate AND Street, proceed?");
                                                    box.SetDefaultButton(MessageBoxDefaultButton.Button1);

                                                    if (box.Show() != DialogResult.Yes)
                                                    {
                                                        throw new ProcessCancelledException();
                                                    }
                                                }
                                                repo.StartTransaction(() =>
                                                {
                                                    GStreetAND streetAND1 = street.GetStreetANDId();
                                                    if (streetAND1 == null)
                                                    {
                                                        andStreet.CreateStreetAND(repo, street);
                                                    }
                                                });

                                                repo.UpdateByAND(street, true); // noraini

                                                GStreetAND streetAND = street.GetStreetANDId();
                                                _area.Polygon = (IPolygon)point.Buffer(5);
                                                _area.Start(point);
                                                _moveLinePoint.Line = street.Polyline;
                                                _selectedStreetLine.Stop();
                                                _selectedStreetLine.Line = street.Polyline;
                                                _selectedStreetLine.Start(point);
                                                _selectedStreetAND = streetAND;
                                                _progress = Progress.TryMove;
                                                OnReported(Message2);
                                                OnStepReported(Message2);
                                            }
                                            break;
                                    }
                                }
                                else //Others group
                                {
                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];

                                        // noraini ali - Aug 2021 - check feature lock
                                        ANDJunctionTools andJunction = new ANDJunctionTools();
                                        andJunction.CheckFeatureLock(street.AreaId,SegmentName);

                                        if (street.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }
                                        _area.Polygon = (IPolygon)point.Buffer(5);
                                        _area.Start(point);
                                        _moveLinePoint.Line = street.Polyline;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = street.Polyline;
                                        _selectedStreetLine.Start(point);
                                        _selectedStreet = street;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _area.Stop();
                                        _selectedStreet = null;
                                        _selectedStreetLine.Stop();
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    ANDJunctionTools andJunction = new ANDJunctionTools();
                                    if (streetsAND.Count == 0)
                                    {
                                        _area.Stop();
                                        _progress = Progress.Select;
                                        _selectedStreetAND = null;
                                        _selectedStreetLine.Stop();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }
                                    GStreetAND street = streetsAND[0];
                                    if (!_selectedStreetAND.Equals(street))
                                    {
                                        if (street.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }
                                        andJunction.CheckWithinWorkArea(SegmentName, street);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, street);
                                        _moveLinePoint.Line = street.Polyline;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = street.Polyline;
                                        _selectedStreetLine.Start(point);
                                        _selectedStreetAND = street;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (streets.Count == 0)
                                    {
                                        _area.Stop();
                                        _progress = Progress.Select;
                                        _selectedStreet = null;
                                        _selectedStreetLine.Stop();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }
                                    GStreet street = streets[0];
                                    if (!_selectedStreet.Equals(street))
                                    {
                                        if (street.IsNavigationReady)
                                        {
                                            if (!Session.User.CanDo(Command.NavigationItem))
                                            {
                                                throw new NavigationControlException();
                                            }
                                        }

                                        _moveLinePoint.Line = street.Polyline;
                                        _selectedStreetLine.Stop();
                                        _selectedStreetLine.Line = street.Polyline;
                                        _selectedStreetLine.Start(point);
                                        _selectedStreet = street;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left) { break; }
                        using (new WaitCursor())
                        {
                            try
                            {
                                _area.Stop();
                                _selectedStreetLine.Stop();
                                _moveLinePoint.Stop();

                                List<GJunction> junctions = repo.SpatialSearch<GJunction>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                List<GJunctionAND> junctionsAND = repo.SpatialSearch<GJunctionAND>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                repo.StartTransaction(() =>
                                {
                                    //AND user
                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        ANDJunctionTools andJunction = new ANDJunctionTools();
                                        ANDStreetTools andStreet = new ANDStreetTools();
                                        ValidateWorkAreaAndStatus validateWorkAreaAndStatus = new ValidateWorkAreaAndStatus();
                                        validateWorkAreaAndStatus.CheckUserOwnWorkArea(SegmentName, point);
                                        validateWorkAreaAndStatus.CheckWithinWorkArea(SegmentName, point);
                                        validateWorkAreaAndStatus.CheckWithinSameWorkArea(SegmentName, point, _selectedStreetAND.AreaId);

                                        if (junctionsAND.Count > 0)
                                        {
                                            GJunctionAND nearestJunctionAND = GeometryUtils.GetNearestFeature(junctionsAND, point);
                                            GJunction fromNode = _selectedStreetAND.GetFromNode();
                                            GJunction toNode = _selectedStreetAND.GetToNode();

                                            if (_from)
                                            {
                                                if (_selectedStreetAND.FromNodeId == nearestJunctionAND.OriId)
                                                {
                                                    throw new QualityControlException("Cannot split and drag junction to the same location! Please reselect street and drag junction to another location.");
                                                }
                                                else
                                                {
                                                    _selectedStreetAND.FromNodeId = nearestJunctionAND.OriId;
                                                    _selectedStreetAND.FromPoint = nearestJunctionAND.Point;
                                                }
                                            }
                                            else
                                            {
                                                if (_selectedStreetAND.ToNodeId == nearestJunctionAND.OriId)
                                                {
                                                    throw new QualityControlException("Cannot split and drag junction to the same location! Please reselect street and drag junction to another location.");
                                                }
                                                else
                                                {
                                                    _selectedStreetAND.ToNodeId = nearestJunctionAND.OriId;
                                                    _selectedStreetAND.ToPoint = nearestJunctionAND.Point;
                                                }
                                            }
                                            _selectedStreetAND.NavigationStatus = 0;
                                            validateWorkAreaAndStatus.UpdateModifiedByUserAND(_selectedStreetAND);
                                            repo.Update(_selectedStreetAND);

                                            if (fromNode.CanDeleteUnionJunction())
                                            {
                                                andJunction.Delete(repo, fromNode);
                                            }

                                            if (toNode.CanDeleteUnionJunction())
                                            {
                                                andJunction.Delete(repo, toNode);
                                            }
                                        }
                                        else
                                        {
                                            if (junctions.Count == 0)
                                            {
                                                if (_from)
                                                {
                                                    GJunction fromNode = _selectedStreetAND.GetFromNode();

                                                    if (fromNode.GetStreets().Count() == 1 && _selectedStreetAND.FromNodeId != _selectedStreetAND.ToNodeId)
                                                    {
                                                        GJunctionAND fromNodeAND = fromNode.GetJunctionANDId();
                                                        if (fromNodeAND == null)
                                                        {
                                                            andJunction.CreateJunctionAND(repo, fromNode);
                                                            fromNodeAND = fromNode.GetJunctionANDId();
                                                        }
                                                        fromNodeAND.Shape = point;
                                                        validateWorkAreaAndStatus.UpdateModifiedByUserAND(fromNodeAND);
                                                        repo.UpdateGraphic(fromNodeAND, true);

                                                        // noraini - Update STATUS for from Node - Update Graphic
                                                        repo.UpdateGraphicByAND(fromNode, true);

                                                        if (fromNode.CanDeleteUnionJunction())
                                                        {
                                                            andJunction.Delete(repo, fromNode);
                                                        }

                                                        _selectedStreetAND.FromPoint = point;
                                                    }
                                                    else
                                                    {
                                                        GJunction newJunction = repo.NewObj<GJunction>();
                                                        newJunction.Init();
                                                        newJunction.Shape = point;
                                                        newJunction = repo.Insert(newJunction, true);

                                                        // noraini - set WA and created by AND User - new junction
                                                        newJunction.AreaId = _selectedStreetAND.AreaId;
                                                        validateWorkAreaAndStatus.UpdateCreatedByUserAND(newJunction);
                                                        validateWorkAreaAndStatus.UpdateModifiedByUserAND(newJunction);
                                                        repo.UpdateInsertByAND(newJunction, true);

                                                        GJunctionAND junctionAND = newJunction.GetJunctionANDId();
                                                        if (junctionAND == null)
                                                        {
                                                            andJunction.CreateJunctionAND(repo, newJunction);

                                                            // noraini - Set Modify by AND User - new AND junction
                                                            GJunctionAND newJunctionAND = newJunction.GetJunctionANDId();
                                                            validateWorkAreaAndStatus.UpdateCreatedByUserAND(newJunctionAND);
                                                            validateWorkAreaAndStatus.UpdateModifiedByUserAND(newJunctionAND);
                                                            repo.UpdateRemainStatus(newJunctionAND, true);
                                                        }

                                                        if (fromNode.CanDeleteUnionJunction())
                                                        {
                                                            andJunction.Delete(repo, fromNode);
                                                        }
                                                        _selectedStreetAND.FromNodeId = newJunction.OID;
                                                        _selectedStreetAND.FromPoint = point;
                                                    }
                                                    _selectedStreetAND.NavigationStatus = 0;
                                                    validateWorkAreaAndStatus.UpdateModifiedByUserAND(_selectedStreetAND);
                                                    repo.Update(_selectedStreetAND);
                                                }
                                                else
                                                {
                                                    GJunction toNode = _selectedStreetAND.GetToNode();

                                                    if (toNode.GetStreets().Count() == 1 && _selectedStreetAND.FromNodeId != _selectedStreetAND.ToNodeId)
                                                    {
                                                        GJunctionAND toNodeAND = toNode.GetJunctionANDId();
                                                        if (toNodeAND == null)
                                                        {
                                                            andJunction.CreateJunctionAND(repo, toNode);
                                                            toNodeAND = toNode.GetJunctionANDId();
                                                        }
                                                        toNodeAND.Shape = point;

                                                        // noraini - Set Modify by AND User - toNodeAND
                                                        validateWorkAreaAndStatus.UpdateModifiedByUserAND(toNodeAND);
                                                        repo.UpdateGraphic(toNodeAND, true);

                                                        // noraini - Set STATUS for toNode - Update Graphic
                                                        repo.UpdateGraphicByAND(toNode, true);

                                                        _selectedStreetAND.ToPoint = point;
                                                    }
                                                    else
                                                    {
                                                        GJunction newJunction = repo.NewObj<GJunction>();
                                                        newJunction.Init();
                                                        newJunction.Shape = point;
                                                        newJunction = repo.Insert(newJunction);

                                                        // noraini - set WA and created by AND User - new junction
                                                        newJunction.AreaId = _selectedStreetAND.AreaId;
                                                        validateWorkAreaAndStatus.UpdateCreatedByUserAND(newJunction);
                                                        validateWorkAreaAndStatus.UpdateModifiedByUserAND(newJunction);
                                                        repo.UpdateInsertByAND(newJunction, true);

                                                        GJunctionAND junctionAND = newJunction.GetJunctionANDId();
                                                        if (junctionAND == null)
                                                        {
                                                            andJunction.CreateJunctionAND(repo, newJunction);

                                                            // noraini - Set Created & Modify by AND User - new AND junction
                                                            GJunctionAND newJunctionAND = newJunction.GetJunctionANDId();
                                                            validateWorkAreaAndStatus.UpdateCreatedByUserAND(newJunctionAND);
                                                            validateWorkAreaAndStatus.UpdateModifiedByUserAND(newJunctionAND);
                                                            repo.UpdateRemainStatus(newJunctionAND, true);
                                                        }
                                                        if (toNode.CanDeleteUnionJunction())
                                                        {
                                                            //andJunction.Delete(repo, toNode);
                                                        }
                                                        _selectedStreetAND.ToNodeId = newJunction.OID;
                                                        _selectedStreetAND.ToPoint = point;
                                                    }

                                                    _selectedStreetAND.NavigationStatus = 0;
                                                    validateWorkAreaAndStatus.UpdateModifiedByUserAND(_selectedStreetAND);
                                                    repo.Update(_selectedStreetAND);
                                                }
                                            }
                                            else if (junctions.Count > 0)
                                            {
                                                GJunction nearestJunction = GeometryUtils.GetNearestFeature(junctions, point);
                                                GJunction fromNode = _selectedStreetAND.GetFromNode();
                                                GJunction toNode = _selectedStreetAND.GetToNode();
                                               
                                                if (_from)
                                                {
                                                    if (_selectedStreetAND.FromNodeId == nearestJunction.OID)
                                                    {
                                                        throw new QualityControlException("Cannot split and drag junction to the same location! Please reselect street and drag junction to another location.");
                                                    }
                                                    else
                                                    {
                                                        _selectedStreetAND.FromNodeId = nearestJunction.OID;
                                                        _selectedStreetAND.FromPoint = nearestJunction.Point;
                                                        //repo.UpdateInsertByAND(nearestJunction, true);
                                                        //GJunctionAND nearestJunctionAND = nearestJunction.GetJunctionANDId();
                                                        //if (nearestJunctionAND == null)
                                                        //{
                                                        //    andJunction.CreateJunctionAND(repo, nearestJunction);
                                                        //    nearestJunctionAND = nearestJunction.GetJunctionANDId();
                                                        //
                                                        //    // noraini - set modified by AND User - new junction
                                                        //    andJunction.UpdateModifiedByUserAND(nearestJunctionAND);
                                                        //    repo.UpdateRemainStatus(nearestJunctionAND, true);
                                                        //}
                                                    }
                                                }
                                                else
                                                {
                                                    if (_selectedStreetAND.ToNodeId == nearestJunction.OID)
                                                    {
                                                        throw new QualityControlException("Cannot split and drag junction to the same location! Please reselect street and drag junction to another location.");
                                                    }
                                                    else
                                                    {
                                                        _selectedStreetAND.ToNodeId = nearestJunction.OID;
                                                        _selectedStreetAND.ToPoint = nearestJunction.Point;

                                                        //repo.UpdateInsertByAND(nearestJunction, true);
                                                        //GJunctionAND nearestJunctionAND = nearestJunction.GetJunctionANDId();
                                                        //if (nearestJunctionAND == null)
                                                        //{
                                                        //    andJunction.CreateJunctionAND(repo, nearestJunction);
                                                        //    nearestJunctionAND = nearestJunction.GetJunctionANDId();
                                                        //
                                                        //    // noraini - set modified by AND User - new junction
                                                        //    andJunction.UpdateModifiedByUserAND(nearestJunctionAND);
                                                        //    repo.UpdateRemainStatus(nearestJunctionAND, true);
                                                        //}
                                                    }
                                                }
                                                _selectedStreetAND.NavigationStatus = 0;
                                                validateWorkAreaAndStatus.UpdateModifiedByUserAND(_selectedStreetAND);
                                                repo.Update(_selectedStreetAND);

                                                if (fromNode.CanDeleteUnionJunction())
                                                {
                                                    andJunction.Delete(repo, fromNode);
                                                }

                                                if (toNode.CanDeleteUnionJunction())
                                                {
                                                    andJunction.Delete(repo, toNode);
                                                }
                                            }
                                        }
                                    }
                                    else //Other user
                                    {
                                        if (junctions.Count == 0)
                                        {
                                            if (_from)
                                            {
                                                GJunction fromNode = _selectedStreet.GetFromNode();

                                                if (fromNode.GetStreets().Count() == 1 && _selectedStreet.FromNodeId != _selectedStreet.ToNodeId)
                                                {
                                                    fromNode.Shape = point;
                                                    repo.Update(fromNode);

                                                    _selectedStreet.FromPoint = point;
                                                }
                                                else
                                                {
                                                    GJunction newJunction = repo.NewObj<GJunction>();
                                                    newJunction.Init();
                                                    newJunction.Shape = point;
                                                    newJunction = repo.Insert(newJunction);

                                                    _selectedStreet.FromNodeId = newJunction.OID;
                                                    _selectedStreet.FromPoint = point;
                                                }

                                                _selectedStreet.NavigationStatus = 0;
                                                repo.Update(_selectedStreet);
                                            }
                                            else
                                            {
                                                GJunction toNode = _selectedStreet.GetToNode();

                                                if (toNode.GetStreets().Count() == 1 && _selectedStreet.FromNodeId != _selectedStreet.ToNodeId)
                                                {
                                                    toNode.Shape = point;
                                                    repo.Update(toNode);

                                                    _selectedStreet.ToPoint = point;
                                                }
                                                else
                                                {
                                                    GJunction newJunction = repo.NewObj<GJunction>();
                                                    newJunction.Init();
                                                    newJunction.Shape = point;
                                                    newJunction = repo.Insert(newJunction);

                                                    _selectedStreet.ToNodeId = newJunction.OID;
                                                    _selectedStreet.ToPoint = point;
                                                }

                                                _selectedStreet.NavigationStatus = 0;
                                                repo.Update(_selectedStreet);
                                            }
                                        }
                                        else if (junctions.Count > 0)
                                        {
                                            GJunction nearestJunction = GeometryUtils.GetNearestFeature(junctions, point);
                                            GJunction fromNode = _selectedStreet.GetFromNode();
                                            GJunction toNode = _selectedStreet.GetToNode();

                                            if (_from)
                                            {
                                                _selectedStreet.FromNodeId = nearestJunction.OID;
                                                _selectedStreet.FromPoint = nearestJunction.Point;
                                            }
                                            else
                                            {
                                                _selectedStreet.ToNodeId = nearestJunction.OID;
                                                _selectedStreet.ToPoint = nearestJunction.Point;
                                            }

                                            _selectedStreet.NavigationStatus = 0;
                                            repo.Update(_selectedStreet);

                                            if (fromNode.CanDelete())
                                            {
                                                repo.Delete(fromNode);
                                            }

                                            if (toNode.CanDelete())
                                            {
                                                repo.Delete(toNode);
                                            }
                                        }
                                    }
                                });
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedStreet = null;
                                _selectedStreetAND = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GStreet> SelectStreet(RepositoryFactory repo, IPoint point)
        {
            Query<GStreet> query = new Query<GStreet>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        private List<GStreetAND> SelectStreetAND(RepositoryFactory repo, IPoint point)
        {
            Query<GStreetAND> query = new Query<GStreetAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_area != null)
            {
                _area.Stop();
            }
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
            }
            if (_moveLinePoint != null)
            {
                _moveLinePoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Stop();
                _selectedStreetLine = null;
            }
            if (_moveLinePoint != null)
            {
                _moveLinePoint.Stop();
                _moveLinePoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
            if (_selectedStreetLine != null)
            {
                _selectedStreetLine.Refresh(hDC);
            }
            if (_moveLinePoint != null)
            {
                _moveLinePoint.Refresh(hDC);
            }
        }
    }
}

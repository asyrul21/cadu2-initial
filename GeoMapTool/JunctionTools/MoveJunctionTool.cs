﻿using Earthworm.AO;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.MapTool.StreetTools;

namespace Geomatic.MapTool.JunctionTools
{
    public class MoveJunctionTool : MoveTool
    {
        #region Fields

        protected FeedBackCollection _moveVertex;
        protected GJunction _selectedJunction;
        protected MovePoint _selectedJunctionPoint;
       
        //added by asyrul
        protected FeedBackCollection _areaCircle;
        protected const double RADIUS1 = 30;
        //added end

        //added by noraini
        protected GJunctionAND _selectedJunctionAND;
        // end

        private const string Message1 = "Select a junction.";
        private const string Message2 = "Drag selected junction to move.";

        #endregion

        public MoveJunctionTool()
        {
            _name = "Move Junction";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_moveVertex == null)
            {
                _moveVertex = new FeedBackCollection();
            }
            if (_selectedJunctionPoint == null)
            {
                _selectedJunctionPoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
            }
            // added by asyrul
            if (_areaCircle == null)
            {
                _areaCircle = new FeedBackCollection();
            }
            // added end
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y) //Trymove
        {
            if (InProgress)
            {
                return;
            }
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left){break;}
                        using (new WaitCursor())
                        {
                            // noraini ali - apr 2020
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                if (_selectedJunctionAND != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedJunctionAND.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                                else
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedJunction.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                            }
                            else
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedJunction.Point))
                                {
                                    _progress = Progress.Moving;
                                }
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (!_areaCircle.IsStarted)
                        {
                            MovePolygon movePolygon1 = new MovePolygon(ScreenDisplay, _color1, _color1, 1);
                            movePolygon1.Polygon = (IPolygon)point.Buffer(RADIUS1);
                            _areaCircle.Add(movePolygon1);
                            _areaCircle.Start(point);
                        }
                        _areaCircle.MoveTo(point);
                        break;
                    case Progress.TryMove:
                        _areaCircle.MoveTo(point); 
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left){break;}
                        _selectedJunctionPoint.MoveTo(point);
                        _moveVertex.MoveTo(point);
                        _areaCircle.MoveTo(point); 
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y) //Select, TryMove, Moving
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GJunction> junctions = SelectJunction(repo, point);
                                List<GJunctionAND> junctionsAND = SelectJunctionAND(repo, point);

                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    ANDJunctionTools andJunction = new ANDJunctionTools();
                                    ANDStreetTools andStreet = new ANDStreetTools();

                                    if (junctions.Count < 1 && junctionsAND.Count < 1)
                                    {
                                        _selectedJunction = null;
                                        _selectedJunctionAND = null;
                                        _selectedJunctionPoint.Stop();
                                        _moveVertex.Stop();

                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                    else if (junctions.Count > 0 && junctionsAND.Count == 0) // junction ADM exist
                                    {
                                        GJunction junction = junctions[0];
                                        andJunction.CheckWithinWorkArea(SegmentName, junction);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, junction);
                                        
                                        List<GStreet> streets = junction.GetStreets().ToList();
                                        List<GStreetAND> streetsAND = junction.GetStreetsAND().ToList();
                                        foreach (GStreet street in streets)
                                        {
                                            andStreet.CheckWithinWorkArea(SegmentName, street);
                                        }
                                        CheckAndGetSegmentStreet(streets, junction);

                                        _selectedJunctionPoint.Point = junction.Point;
                                        _selectedJunctionPoint.Start(point);
                                        _moveVertex.Start(junction.Point);
                                        _selectedJunction = junction;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else // JunctionAND exist
                                    {
                                        GJunctionAND junctionAND = junctionsAND[0];
                                        andJunction.CheckWithinWorkArea(SegmentName, junctionAND);
                                        andJunction.CheckUserOwnWorkArea(SegmentName, junctionAND);

                                        List<GStreetAND> streetsAND = junctionAND.GetStreetsAND().ToList();
                                        GJunction junction = repo.GetById<GJunction>(junctionAND.OriId);
                                        List<GStreet> streets = junction.GetStreets().ToList();
                                        if(streetsAND.Count == streets.Count)
                                        {
                                            foreach (GStreetAND street in streetsAND)
                                            {
                                                andStreet.CheckWithinWorkArea(SegmentName, street);
                                            }
                                            CheckAndGetSegmentStreetAND(streetsAND, junction);
                                        }
                                        else if(streetsAND.Count < streets.Count)
                                        {
                                            foreach (GStreet street in streets)
                                            {
                                                andStreet.CheckWithinWorkArea(SegmentName, street);
                                            }
                                            CheckAndGetSegmentStreet(streets, junction);
                                        }                                       

                                        _selectedJunctionPoint.Point = junctionAND.Point;
                                        _selectedJunctionPoint.Start(point);
                                        _moveVertex.Start(junctionAND.Point);
                                        _selectedJunctionAND = junctionAND;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                }
                                else   // group others users
                                {
                                    if (junctions.Count > 0)
                                    {
                                        GJunction junction = junctions[0];
                                        // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                        //if (Session.User.GetGroup().Name != "SUPERVISOR")
                                        //{
                                        //    if (junction.AreaId != null)
                                        //    {
                                        //        throw new QualityControlException("feature is LOCK under AND Working Area.");
                                        //    }
                                        //}

                                        // noraini ali - Aug 2021 - check feature lock by AND
                                        ANDJunctionTools andJunction = new ANDJunctionTools();
                                        andJunction.CheckFeatureLock(junction.AreaId, SegmentName);
                                        // end add

                                        List<GStreet> streets = junction.GetStreets().ToList();

                                        CheckAndGetSegmentStreet(streets, junction);

                                        _selectedJunctionPoint.Point = junction.Point;
                                        _selectedJunctionPoint.Start(point);
                                        _moveVertex.Start(junction.Point);
                                        _selectedJunction = junction;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedJunction = null;
                                        _selectedJunctionPoint.Stop();
                                        _moveVertex.Stop();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GJunction> junctions = SelectJunction(repo, point);
                                List<GJunctionAND> junctionsAND = SelectJunctionAND(repo, point);

                                if (junctions.Count == 0 && junctionsAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedJunction = null;
                                    _selectedJunctionAND = null;
                                    _selectedJunctionPoint.Stop();
                                    _moveVertex.Stop();
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }
                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    if (_selectedJunctionAND != null)
                                    {
                                        GJunctionAND junctionAND = junctionsAND[0];
                                        if (!_selectedJunctionAND.Equals(junctionAND))
                                        {
                                            bool canPerform = Session.User.CanDo(Command.NavigationItem);
                                            List<GStreetAND> streetsAND = junctionAND.GetStreetsAND().ToList();
                                            foreach (GStreetAND streetAND in streetsAND)
                                            {
                                                if (streetAND.IsNavigationReady)
                                                {
                                                    if (!canPerform)
                                                    {
                                                       throw new NavigationControlException();
                                                    }
                                                }
                                            }
                                            _moveVertex.Stop();
                                            foreach (GStreetAND streetAND in streetsAND)
                                            {
                                                MoveLinePoint moveLinePoint = new MoveLinePoint(ScreenDisplay, _color1, 1);
                                                IPolyline polyline = new PolylineClass();
                                                ISegmentCollection segmentCollection = (ISegmentCollection)polyline;
                                                if (streetAND.FromNodeId == junctionAND.OID)
                                                {
                                                    segmentCollection.AddSegment(streetAND.GetSegment(0));
                                                    moveLinePoint.Line = polyline;
                                                    moveLinePoint.Index = 0;
                                                }
                                                if (streetAND.ToNodeId == junctionAND.OID)
                                                {
                                                    segmentCollection.AddSegment(streetAND.GetSegment(streetAND.SegmentCount - 1));
                                                    moveLinePoint.Line = polyline;
                                                    moveLinePoint.Index = 1;
                                                }
                                                _moveVertex.Add(moveLinePoint);
                                            }
                                            _selectedJunctionPoint.Stop();
                                            _selectedJunctionPoint.Point = junctionAND.Point;
                                            _selectedJunctionPoint.Start(point);
                                            _moveVertex.Start(junctionAND.Point);
                                            _selectedJunctionAND = junctionAND;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    GJunction junction = junctions[0];
                                    if (!_selectedJunction.Equals(junction))
                                    {
                                        bool canPerform = Session.User.CanDo(Command.NavigationItem);

                                        List<GStreet> streets = junction.GetStreets().ToList();

                                        foreach (GStreet street in streets)
                                        {
                                            if (street.IsNavigationReady)
                                            {
                                                if (!canPerform)
                                                {
                                                    throw new NavigationControlException();
                                                }
                                            }
                                        }

                                        _moveVertex.Stop();

                                        foreach (GStreet street in streets)
                                        {
                                            MoveLinePoint moveLinePoint = new MoveLinePoint(ScreenDisplay, _color1, 1);
                                            IPolyline polyline = new PolylineClass();
                                            ISegmentCollection segmentCollection = (ISegmentCollection)polyline;
                                            if (street.FromNodeId == junction.OID)
                                            {
                                                segmentCollection.AddSegment(street.GetSegment(0));
                                                moveLinePoint.Line = polyline;
                                                moveLinePoint.Index = 0;
                                            }
                                            if (street.ToNodeId == junction.OID)
                                            {
                                                segmentCollection.AddSegment(street.GetSegment(street.SegmentCount - 1));
                                                moveLinePoint.Line = polyline;
                                                moveLinePoint.Index = 1;
                                            }
                                            _moveVertex.Add(moveLinePoint);
                                        }

                                        _selectedJunctionPoint.Stop();
                                        _selectedJunctionPoint.Point = junction.Point;
                                        _selectedJunctionPoint.Start(point);
                                        _moveVertex.Start(junction.Point);
                                        _selectedJunction = junction;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                        break;
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left){ break; }
                        using (new WaitCursor())
                        {
                            try
                            {
                                _moveVertex.Stop();
                                _selectedJunctionPoint.Stop();

                                List<GJunction> junctions = repo.SpatialSearch<GJunction>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                List<GJunctionAND> junctionsAND = repo.SpatialSearch<GJunctionAND>(point.Buffer(5), esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                int counter = 0; //for testing purposes

                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    repo.StartTransaction(() =>
                                    {
                                        ANDJunctionTools andJunction = new ANDJunctionTools();
                                        ANDStreetTools andStreet = new ANDStreetTools();

                                        if (_selectedJunctionAND == null)
                                        {
                                            andJunction.CreateJunctionAND(repo, _selectedJunction);
                                            GJunctionAND junctionAND = _selectedJunction.GetJunctionANDId();
                                            List<GStreet> streets = _selectedJunction.GetStreets().ToList();
                  
                                            // Create AND Street where releted to junction
                                            foreach (GStreet street in streets)
                                            {
                                                GStreetAND streetAND = street.GetStreetANDId();
                                                if(streetAND == null)
                                                {
                                                    andStreet.CreateStreetAND(repo, street);
                                                    streetAND = street.GetStreetANDId();
                                                }

                                                if (street.FromNodeId == _selectedJunction.OID)
                                                {
                                                    streetAND.FromNodeId = _selectedJunction.OID;
                                                }
                                                if (street.ToNodeId == _selectedJunction.OID)
                                                {
                                                    streetAND.ToNodeId = _selectedJunction.OID;
                                                }
                                                andJunction.UpdateModifiedByUserAND(streetAND);
                                                repo.UpdateGraphic(streetAND,true);
                                                // noraini - Aug 2021
                                                repo.UpdateGraphicByAND(street, true);
                                            }
                                            _selectedJunctionAND = junctionAND;
                                        }
                                        else
                                        {
                                            GJunction _selectedJunction = repo.GetById<GJunction>(_selectedJunctionAND.OriId);
                                            List<GStreet> streets = _selectedJunction.GetStreets().ToList();
                                            List<GStreetAND> streetsAND = _selectedJunctionAND.GetStreetsAND().ToList();
                                            if(streetsAND.Count == streets.Count)
                                            {
                                                foreach (GStreetAND street in streetsAND)
                                                {
                                                    andStreet.CheckWithinWorkArea(SegmentName, street);
                                                }
                                            }
                                            else if (streetsAND.Count < streets.Count)
                                            {
                                                foreach (GStreet street in streets)
                                                {
                                                    andStreet.CheckWithinWorkArea(SegmentName, street);
                                                }
                                                // Create AND Street where releted to junction
                                                foreach (GStreet street in streets)
                                                {
                                                    GStreetAND streetAND = street.GetStreetANDId();
                                                    if (streetAND == null)
                                                    {
                                                        andStreet.CreateStreetAND(repo, street);
                                                        streetAND = street.GetStreetANDId();
                                                    }
         
                                                    if (street.FromNodeId == _selectedJunction.OID)
                                                    {
                                                        streetAND.FromNodeId = _selectedJunction.OID;
                                                    }
                                                    if (street.ToNodeId == _selectedJunction.OID)
                                                    {
                                                        streetAND.ToNodeId = _selectedJunction.OID;
                                                    }
                                                    andJunction.UpdateModifiedByUserAND(streetAND);
                                                    repo.UpdateGraphic(streetAND, true);
                                                    // noraini - Aug 2021
                                                    repo.UpdateGraphicByAND(street, true);
                                                }
                                            }                                           
                                        }
                                        
                                        ValidateWorkAreaAndStatus validateWorkAreaAndStatus = new ValidateWorkAreaAndStatus();
                                        if (junctionsAND.Count == 0)
                                        {
                                            validateWorkAreaAndStatus.CheckWithinSameWorkArea(SegmentName, point, _selectedJunctionAND.AreaId);
                                            andJunction.Move(repo, _selectedJunctionAND, point);
                                            foreach (GStreetAND streetAND in _selectedJunctionAND.GetStreetsAND())
                                            {
                                                if (streetAND.FromNodeId == _selectedJunctionAND.OriId)
                                                {
                                                    streetAND.FromPoint = point;
                                                    List<IPoint> tobedeleted = new List<IPoint>();
                                                    if (streetAND.Vertices.Count() > 2)
                                                    {
                                                        foreach (IPoint vertex in streetAND.Vertices)
                                                        {
                                                            if (vertex.Equals2(point))
                                                            {
                                                                continue;
                                                            }
                                                            if (point.Buffer(RADIUS1).Contains(vertex))
                                                            {
                                                                counter++;
                                                                tobedeleted.Add(vertex);
                                                            }
                                                        }
                                                        foreach (IPoint vertex in tobedeleted)
                                                        {
                                                            streetAND.TryDeleteVertex(vertex);
                                                        }
                                                    }
                                                }
                                                if (streetAND.ToNodeId == _selectedJunctionAND.OriId)
                                                {
                                                    streetAND.ToPoint = point;
                                                    List<IPoint> tobedeleted = new List<IPoint>();
                                                    if (streetAND.Vertices.Count() > 2)
                                                    {
                                                        foreach (IPoint vertex in streetAND.Vertices)
                                                        {
                                                            if (vertex.Equals2(point))
                                                            {
                                                                continue;
                                                            }
                                                            if (point.Buffer(RADIUS1).Contains(vertex))
                                                            {
                                                                counter++;
                                                                tobedeleted.Add(vertex);
                                                            }
                                                        }
                                                        foreach (IPoint vertex in tobedeleted)
                                                        {
                                                            streetAND.TryDeleteVertex(vertex);
                                                        }
                                                    }
                                                }
                                                andJunction.UpdateModifiedByUserAND(streetAND);
                                                repo.UpdateGraphic(streetAND,true);
                                            }
                                        }
                                        else if (junctionsAND.Count > 0)
                                        {
                                            GJunction nearestJunction = GeometryUtils.GetNearestFeature(junctionsAND, point);
                                            if (_selectedJunctionAND.Equals(nearestJunction))
                                            {
                                                _selectedJunctionAND.Shape = point;
                                                repo.UpdateGraphic(_selectedJunctionAND,true);
                                                foreach (GStreetAND streetAND in _selectedJunctionAND.GetStreetsAND())
                                                {
                                                    if (streetAND.FromNodeId == _selectedJunctionAND.OID)
                                                    {
                                                        streetAND.FromPoint = point;
                                                    }
                                                    if (streetAND.ToNodeId == _selectedJunctionAND.OID)
                                                    {
                                                        streetAND.ToPoint = point;
                                                    }
                                                    andJunction.UpdateModifiedByUserAND(streetAND);
                                                    repo.UpdateGraphic(streetAND,true);
                                                }
                                            }
                                            else
                                            {
                                                foreach (GStreetAND streetAND in _selectedJunctionAND.GetStreetsAND())
                                                {
                                                    if (streetAND.FromNodeId == _selectedJunction.OID)
                                                    {
                                                        streetAND.FromNodeId = nearestJunction.OID;
                                                        streetAND.FromPoint = nearestJunction.Point;
                                                    }
                                                    if (streetAND.ToNodeId == _selectedJunction.OID)
                                                    {
                                                        streetAND.ToNodeId = nearestJunction.OID;
                                                        streetAND.ToPoint = nearestJunction.Point;
                                                    }
                                                    andJunction.UpdateModifiedByUserAND(streetAND);
                                                    repo.UpdateGraphic(streetAND,true);
                                                }
                                                if (_selectedJunctionAND.CanDelete())
                                                {
                                                    repo.Delete(_selectedJunctionAND);
                                                }
                                            }
                                        }
                                    });
                                }
                                else  // others User Group
                                {
                                    repo.StartTransaction(() =>
                                    {
                                        if (junctions.Count == 0)
                                        {
                                            _selectedJunction.Shape = point;
                                            repo.UpdateGraphic(_selectedJunction,true);

                                            foreach (GStreet street in _selectedJunction.GetStreets())
                                            {
                                                if (street.FromNodeId == _selectedJunction.OID)
                                                {
                                                    street.FromPoint = point;

                                                        //added by asyrul
                                                        List<IPoint> tobedeleted = new List<IPoint>();
                                                    if (street.Vertices.Count() > 2)
                                                    {
                                                        foreach (IPoint vertex in street.Vertices)
                                                        {
                                                            if (vertex.Equals2(point))
                                                            {
                                                                continue;
                                                            }
                                                            if (point.Buffer(RADIUS1).Contains(vertex))
                                                            {
                                                                counter++;
                                                                tobedeleted.Add(vertex);
                                                            }
                                                        }
                                                        foreach (IPoint vertex in tobedeleted)
                                                        {
                                                            street.TryDeleteVertex(vertex);
                                                        }
                                                    }
                                                        //added end
                                                    }
                                                if (street.ToNodeId == _selectedJunction.OID)
                                                {
                                                    street.ToPoint = point;

                                                        //added by asyrul
                                                        List<IPoint> tobedeleted = new List<IPoint>();
                                                    if (street.Vertices.Count() > 2)
                                                    {
                                                        foreach (IPoint vertex in street.Vertices)
                                                        {
                                                            if (vertex.Equals2(point))
                                                            {
                                                                continue;
                                                            }
                                                            if (point.Buffer(RADIUS1).Contains(vertex))
                                                            {
                                                                counter++;
                                                                tobedeleted.Add(vertex);
                                                            }
                                                        }
                                                        foreach (IPoint vertex in tobedeleted)
                                                        {
                                                            street.TryDeleteVertex(vertex);
                                                        }
                                                    }
                                                        //added end
                                                    }
                                                repo.UpdateGraphic(street,true);
                                            }
                                        }
                                        else if (junctions.Count > 0)
                                        {
                                            GJunction nearestJunction = GeometryUtils.GetNearestFeature(junctions, point);

                                            if (_selectedJunction.Equals(nearestJunction))
                                            {
                                                _selectedJunction.Shape = point;
                                                repo.UpdateGraphic(_selectedJunction,true);
                                                foreach (GStreet street in _selectedJunction.GetStreets())
                                                {
                                                    if (street.FromNodeId == _selectedJunction.OID)
                                                    {
                                                        street.FromPoint = point;
                                                    }
                                                    if (street.ToNodeId == _selectedJunction.OID)
                                                    {
                                                        street.ToPoint = point;
                                                    }
                                                    repo.UpdateGraphic(street,true);
                                                }
                                            }
                                            else
                                            {
                                                foreach (GStreet street in _selectedJunction.GetStreets())
                                                {
                                                    if (street.FromNodeId == _selectedJunction.OID)
                                                    {
                                                        street.FromNodeId = nearestJunction.OID;
                                                        street.FromPoint = nearestJunction.Point;
                                                    }
                                                    if (street.ToNodeId == _selectedJunction.OID)
                                                    {
                                                        street.ToNodeId = nearestJunction.OID;
                                                        street.ToPoint = nearestJunction.Point;
                                                    }
                                                    repo.UpdateGraphic(street,true);
                                                }

                                                if (_selectedJunction.CanDelete())
                                                {
                                                    repo.Delete(_selectedJunction);
                                                }
                                            }
                                        }
                                    });
                                }
                            //MessageBox.Show("Number of vertex within area: " + counter);                           
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedJunction = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GJunction> SelectJunction(RepositoryFactory repo, IPoint point)
        {
            Query<GJunction> query = new Query<GJunction>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        // added by noraini ali - Mei 2020 Cadu2 AND
        private List<GJunctionAND> SelectJunctionAND(RepositoryFactory repo, IPoint point)
        {
            Query<GJunctionAND> query = new Query<GJunctionAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }
        // end

        private  void CheckAndGetSegmentStreet(List<GStreet> streets, GJunction junction)
        {
            foreach (GStreet street in streets)
            {
                if (street.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }
            }

            foreach (GStreet street in streets)
            {
                MoveLinePoint moveLinePoint = new MoveLinePoint(ScreenDisplay, _color1, 1);
                IPolyline polyline = new PolylineClass();
                ISegmentCollection segmentCollection = (ISegmentCollection)polyline;
                if (street.FromNodeId == junction.OID)
                {
                    segmentCollection.AddSegment(street.GetSegment(0)); //get first segment
                    moveLinePoint.Line = polyline;
                    moveLinePoint.Index = 0;
                }
                if (street.ToNodeId == junction.OID)
                {
                    segmentCollection.AddSegment(street.GetSegment(street.SegmentCount - 1)); //get last segment
                    moveLinePoint.Line = polyline;
                    moveLinePoint.Index = 1;
                }
                _moveVertex.Add(moveLinePoint);
            }
        }

        private void CheckAndGetSegmentStreetAND(List<GStreetAND> streets, GJunction junction)
        {
            foreach (GStreet street in streets)
            {
                if (street.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }
            }

            foreach (GStreetAND street in streets)
            {
                MoveLinePoint moveLinePoint = new MoveLinePoint(ScreenDisplay, _color1, 1);
                IPolyline polyline = new PolylineClass();
                ISegmentCollection segmentCollection = (ISegmentCollection)polyline;
                if (street.FromNodeId == junction.OID)
                {
                    segmentCollection.AddSegment(street.GetSegment(0)); //get first segment
                    moveLinePoint.Line = polyline;
                    moveLinePoint.Index = 0;
                }
                if (street.ToNodeId == junction.OID)
                {
                    segmentCollection.AddSegment(street.GetSegment(street.SegmentCount - 1)); //get last segment
                    moveLinePoint.Line = polyline;
                    moveLinePoint.Index = 1;
                }
                _moveVertex.Add(moveLinePoint);
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            _selectedJunction = null;
            if (_moveVertex != null)
            {
                _moveVertex.Stop();
            }
            if (_selectedJunctionPoint != null)
            {
                _selectedJunctionPoint.Stop();
            }
            //added by asyrul
            if (_areaCircle != null)
            {
                _areaCircle.Stop(); ;
            }
            //added end
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_moveVertex != null)
            {
                _moveVertex.Stop();
                _moveVertex = null;
            }
            if (_selectedJunctionPoint != null)
            {
                _selectedJunctionPoint.Stop();
                _selectedJunctionPoint = null;
            }
            //added by asyrul
            if (_areaCircle != null)
            {
                _areaCircle.Stop(); ;
                _areaCircle = null;
            }
            //added end
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_moveVertex != null)
            {
                _moveVertex.Refresh(hDC);
            }
            if (_selectedJunctionPoint != null)
            {
                _selectedJunctionPoint.Refresh(hDC);
            }
            if (_areaCircle != null)
            {
                _areaCircle.Refresh(hDC);
            }
        }
    }
}

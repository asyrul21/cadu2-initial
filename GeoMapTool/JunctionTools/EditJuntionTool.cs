﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Search;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.JunctionTools
{
    public class EditJuntionTool : EditTool
    {
        protected List<MoveLine> _selectedFeatureLines;
        protected List<MovePoint> _selectedFeaturePoints;

        public EditJuntionTool()
        {
            _name = "Edit Junction";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a junction to edit.");
            OnStepReported("Select a junction to edit.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (!InProgress)
                {
                    try
                    {
                        InProgress = true;

                        RepositoryFactory repo = new RepositoryFactory(SegmentName);

                        IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                        List<GJunction> junctions;
                        GJunction junction = null;

                        List<GJunctionAND> junctionsAND;
                        GJunctionAND junctionAND = null;

                        using (new WaitCursor())
                        {
                            if (geometry == null)
                            {
                                return;
                            }

                            junctions = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            junctionsAND = repo.SpatialSearch<GJunctionAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        }

                        if (junctions.Count == 0 && junctionsAND.Count == 0)
                        {
                            return;
                        }

                        int _workAreaId = 0;
                        //AND USER
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDJunctionTools andJunction = new ANDJunctionTools();
                            _workAreaId = andJunction.GetWorkAreaId(SegmentName, point);
                            if (junctionsAND.Count == 1)
                            {
                                junctionAND = junctionsAND[0];
                            }
                            else if (junctionsAND.Count > 1)
                            {
                                junctionAND = ChooseFeature(junctionsAND, null);
                            }

                            if (junctionAND == null)
                            {
                                if (junctions.Count == 1)
                                {
                                    junction = junctions[0];
                                }
                                else if (junctions.Count > 1)
                                {
                                    junction = ChooseFeature(junctions, null);
                                }

                                if (junction == null)
                                {
                                    return;
                                }
                            }
                        }
                        //Other USER
                        else
                        {
                            if (junctions.Count == 1)
                            {
                                junction = junctions[0];
                            }
                            else if (junctions.Count > 1)
                            {
                                junction = ChooseFeature(junctions, null);
                            }

                            if (junction == null)
                            {
                                return;
                            }
                        }

                        bool success = false;

                        try
                        {
                            using (new WaitCursor())
                            {
                                repo.StartTransaction();
                            }

                            bool _CanEdit = false;
                            ANDJunctionTools andJunction = new ANDJunctionTools();
                            if (_CanEdit = andJunction.ValidateUserCanEdit(SegmentName, _workAreaId))
                            {
                                if (junction != null)
                                {
                                    highlightJunction(repo, junction.OID, true, _selectedFeaturePoints);
                                    andJunction.Update(repo, junction);
                                    OnReported("Junction updated. {0}", junction.OID);
                                }
                                else
                                {
                                    junction = repo.GetById<GJunction>(junctionAND.OriId);
                                    highlightJunction(repo, junction.OID, true, _selectedFeaturePoints);
                                    andJunction.Update(repo, junctionAND);
                                    OnReported("Junction updated. {0}", junctionAND.OriId);
                                }
                            }
                            else
                            {
                                if (junction == null)
                                {
                                    andJunction.Update(repo, junctionAND);
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                highlightJunction(repo, junction.OID, true, _selectedFeaturePoints);
                                using (EditJunctionForm form = new EditJunctionForm(junction))
                                {
                                    if (form.ShowDialog() != DialogResult.OK)
                                    {
                                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                        return;
                                    }
                                    using (new WaitCursor())
                                    {
                                        form.SetValues();
                                        repo.Update(junction);
                                    }
                                }
                                OnReported("Junction updated. {0}", junction.OID);
                            }
                            
                            OnReported("Select a junction to edit.");
                            OnStepReported("Select a junction to edit.");
                            success = true;
                        }
                        catch
                        {
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                            throw;
                        }
                        finally
                        {
                            if (success)
                            {
                                repo.EndTransaction();
                            }
                            else
                            {
                                repo.AbortTransaction();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                        {
                            box.Show();
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        }
                    }
                    finally
                    {
                        InProgress = false;
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
            }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.JunctionTools
{

    public class ANDJunctionTools : ValidateWorkAreaAndStatus
    {
        public void CreateJunctionAND(RepositoryFactory repo, GJunction Junction)
        {
            GJunction junction = repo.GetById<GJunction>(Junction.OID);
            GJunctionAND JunctionAND = repo.NewObj<GJunctionAND>();
            JunctionAND.CopyFrom(junction);
            repo.Insert(JunctionAND, false);
        }

        public void AddJunctionAND(RepositoryFactory repo, GJunction Junction)
        {
            // update junction ADM - Area ID
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(Junction.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => Junction.AreaId = thearea.OID);
            repo.UpdateInsertByAND(Junction, true);

            // create new junction AND as copy from ADM junction 
            CreateJunctionAND(repo, Junction);

            // update junction AND - CreatedBy, DateCreated, Updatedby & DateUpdated
            GJunctionAND JunctionAND = Junction.GetJunctionANDId();
            UpdateCreatedByUserAND(JunctionAND);
            UpdateModifiedByUserAND(JunctionAND);
            repo.UpdateRemainStatus(JunctionAND, true);

            // update work area flag as open 
            int AreaID = JunctionAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GJunction Junction)
        {
            GJunctionAND absObjAND = Junction.GetJunctionANDId();
            if (absObjAND == null)
            {
                // Create Junction AND if not exist
                CreateJunctionAND(repo, Junction);
            }

            // Update Junction AND - User info,STATUS & AND_STATUS as Delete status
            GJunctionAND JunctionAND = Junction.GetJunctionANDId();
            UpdateModifiedByUserAND(JunctionAND);
            repo.UpdateDeleteByAND(JunctionAND, true);

            // update Junction ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(Junction, true);

            // update work area flag as open 
            int AreaID = JunctionAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GJunctionAND JunctionAND)
        {
            // Update Junction ADM - AND_STATUS & STATUS as Delete status
            GJunction Junction = repo.GetById<GJunction>(JunctionAND.OriId);
            repo.UpdateDeleteByAND(Junction, true);

            // Update Junction AND -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(JunctionAND);
            repo.UpdateDeleteByAND(JunctionAND, true);

            // update work area flag as open 
            int AreaID = JunctionAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GJunction Junction)
        {
            GJunctionAND absObjAND = Junction.GetJunctionANDId();
            if (absObjAND == null)
            {
                // Create Junction AND
                CreateJunctionAND(repo, Junction);
            }

            //Get Junction AND 
            GJunctionAND JunctionAND = Junction.GetJunctionANDId();

            //Display form 
            using (EditJunctionANDForm form = new EditJunctionANDForm(JunctionAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - 
                    // AreadId, Updatedby & DateUpdated
                    List<GWorkArea> area;
                    area = repo.SpatialSearch<GWorkArea>(Junction.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
                    area.ForEach(thearea => JunctionAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(JunctionAND);
                    repo.Update(JunctionAND);

                    // Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => Junction.AreaId = thearea.OID);
                    repo.UpdateByAND(Junction, true);

                    // update work area flag as open 
                    int AreaID = JunctionAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GJunctionAND JunctionAND)
        {
            GJunction Junction = repo.GetById<GJunction>(JunctionAND.OriId);
            if (Junction == null)
            {
                throw new QualityControlException("Cannot proceed, junction ADM not found.");
            }

            //Display form 
            using (EditJunctionANDForm form = new EditJunctionANDForm(JunctionAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(JunctionAND);
                    repo.Update(JunctionAND, true);

                    // update ADM - STATUS & AND_STATUS to update status
                    repo.UpdateByAND(Junction, true);

                    // update work area flag as open 
                    int AreaID = JunctionAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GJunction Junction, IPoint newPoint)
        {
            //Restrict from moving out, NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                throw new QualityControlException("Junction Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != Junction.AreaId)
                {
                    throw new QualityControlException("Cannot move out to different work area.");
                }
            });

            // if AND Junction not exist so create AND Junction 
            GJunctionAND absObjAND = Junction.GetJunctionANDId();
            if (absObjAND == null)
            {
                // Create Property AND
                CreateJunctionAND(repo, Junction);
            }

            // set new location for AND Junction
            GJunctionAND JunctionAND = Junction.GetJunctionANDId();
            JunctionAND.Shape = newPoint;
            UpdateModifiedByUserAND(JunctionAND);
            repo.UpdateGraphic(JunctionAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(Junction, true);

            // update work area flag as open 
            int AreaID = JunctionAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GJunctionAND JunctionAND, IPoint newPoint)
        {
            GJunction Junction = repo.GetById<GJunction>(JunctionAND.OriId);
            if (Junction == null)
            {
                throw new QualityControlException("Cannot proceed, junction ADM not found.");
            }

            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                throw new QualityControlException("Junction Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != JunctionAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out to different work area.");
                }
            });

            // set new location for AND Property & STATUS & AND_STATUS (Update Graphic)
            JunctionAND.Shape = newPoint;
            UpdateModifiedByUserAND(JunctionAND);
            repo.UpdateGraphic(JunctionAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Junction, true);

            // update work area flag as open 
            int AreaID = JunctionAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        public List<GJunction> FilterJunctionInWorkArea(List<GJunction> _junctions, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GJunction> junctionsList = new List<GJunction>();
            foreach (GJunction _junction in _junctions)
            {
                if (_junction.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_junction.AndStatus.ToString()) || (_junction.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_junction.AreaId.ToString()) || (_junction.AreaId > 0))
                        {
                            if (_junction.AreaId.Value == _workareaid)
                            {
                                junctionsList.Add(_junction);
                            }
                        }
                    }
                }
            }
            return junctionsList;
        }

        public List<GJunctionAND> FilterJunctionInWorkArea(List<GJunctionAND> _junctions, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GJunctionAND> junctionsList = new List<GJunctionAND>();
            foreach (GJunctionAND _junction in _junctions)
            {
                if (_junction.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_junction.AndStatus.ToString()) || (_junction.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_junction.AreaId.ToString()) || (_junction.AreaId > 0))
                        {
                            if (_junction.AreaId.Value == _workareaid)
                            {
                                junctionsList.Add(_junction);
                            }
                        }
                    }
                }
            }
            return junctionsList;
        }
    }
}

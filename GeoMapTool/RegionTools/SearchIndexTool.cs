﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//GeoCore
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;

//GeoUI
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;

//ESRI ArcGIS
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;

namespace Geomatic.MapTool.RegionTools
{
    public class SearchIndexTool : EditTool
    {
        public SearchIndexTool()
        {
            _name = "Search Index";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a region to view.");
            OnStepReported("Select a region to view.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            //IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    MessageBox.Show("Searching for a region!");
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;

namespace Geomatic.MapTool
{
    public abstract class DeleteTool : HighlightTool
    {
        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (button == MouseKey.Right)
            {
                if (InProgress)
                {
                    return;
                }

                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

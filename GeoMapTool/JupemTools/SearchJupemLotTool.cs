﻿using System;
using System.Windows.Forms;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.MapTool.JupemTools
{
    public class SearchJupemLotTool : EditTool
    {
        public SearchJupemLotTool()
        {
            _name = "Search Jupem Lot";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a jupem to view.");
            OnStepReported("Select a jupem to view.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    MessageBox.Show("Searching for a jupem lot!");
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

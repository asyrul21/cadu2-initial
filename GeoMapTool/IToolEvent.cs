﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core;
using Geomatic.UI.Forms.Documents;

namespace Geomatic.MapTool
{
    public interface IToolEvent
    {
        event EventHandler<ToolReportedEventArgs> Reported;
        event EventHandler<ToolStepReportedEventArgs> StepReported;

        void OnDocumentChanged(MapDocument mapDocument);
        void OnSegmentChanged(SegmentName version);
        void OnUndo();
        void OnRedo();
        void OnSave();
    }
}

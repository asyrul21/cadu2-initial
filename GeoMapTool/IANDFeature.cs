﻿using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.UI.FeedBacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool
{
    interface IANDFeature
    {
        void Insert(SegmentName SegmentName, GFeature feature, int andStatus);
        void Update(SegmentName SegmentName, GFeature feature, int andStatus, string classname);
        void Delete(SegmentName SegmentName, GFeature feature, int andStatus, string classname);
    }
}


﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public class DeleteBuildingGroupTool : DeleteTool
    {
        private const string Message1 = "Select a building group to delete.";
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public DeleteBuildingGroupTool()
        {
            _name = "Delete Building Group";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GBuildingGroup> buildingGroups;

                    GBuildingGroup buildingGroup = null;

                    // added by noraini ali -Mei 2020
                    List<GBuildingGroupAND> buildingGroupsAND;
                    GBuildingGroupAND buildingGroupAND = null;
                    // end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        buildingGroups = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        buildingGroupsAND = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                    {
                        return;
                    }

                    if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id & Checking point in Working Area & Owner
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                        andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                        int _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                        // filter street ADM & AND in this working area only
                        List<GBuildingGroup> _NewListBuildingGroups = new List<GBuildingGroup>();
                        List<GBuildingGroupAND> _NewListBuildingGroupsAND = new List<GBuildingGroupAND>();

                        if (buildingGroups.Count > 0)
                        {
                            _NewListBuildingGroups = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroups, _workAreaId);
                        }

                        if (buildingGroupsAND.Count > 0)
                        {
                            _NewListBuildingGroupsAND = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroupsAND, _workAreaId);
                        }

                        // create new list to store all ADM feature with filter - use in ChooseForm
                        List<GBuildingGroup> _buildingGroups = new List<GBuildingGroup>();
                        
                        foreach (GBuildingGroup BuildGrpTemp in _NewListBuildingGroups)
                        {
                            _buildingGroups.Add(BuildGrpTemp);
                        }

                        foreach (GBuildingGroupAND BuildGrpTempAND in _NewListBuildingGroupsAND)
                        {
                            GBuildingGroup buildingGroupTemp = repo.GetById<GBuildingGroup>(BuildGrpTempAND.OriId);
                            if (buildingGroupTemp != null)
                                _buildingGroups.Add(buildingGroupTemp);
                        }

                        buildingGroupAND = null;
                        if (_buildingGroups.Count == 0)
                        {
                            return;
                        }
                        else if (_buildingGroups.Count == 1)
                        {
                            buildingGroup = _buildingGroups[0];

                            // if OID exist in AND feature
                            GBuildingGroupAND BuildingGrpTempAND = buildingGroup.GetBuildingGroupANDId();
                            if (BuildingGrpTempAND != null)
                            {
                                buildingGroupAND = BuildingGrpTempAND;
                                buildingGroup = null;
                            }
                        }
                        else
                        {
                            OnReported(Message1);
                            OnStepReported(Message1);
                            buildingGroup = ChooseFeature(_buildingGroups, null);

                            // if OID exist in AND feature
                            GBuildingGroupAND buildingGroupTempAND = repo.GetById<GBuildingGroupAND>(buildingGroup.OID);
                            if (buildingGroupTempAND != null)
                            {
                                buildingGroupAND = buildingGroupTempAND;
                                buildingGroup = null;
                            }
                        }

                        //noraini ali -OKT 2020 - highlight feature
                        highlightBuildGroup_Building(repo, buildingGroup, buildingGroupAND, _selectedFeaturePoints);
                        highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);

                        // Validation on feature
                        bool b_statusCanDelete = true;
                        if (buildingGroup != null)
                        {
                            if (buildingGroup.HasBuildingAND())
                            {
                                b_statusCanDelete = false;
                            }
                            else
                            {
                                // checking previous is point to this building group but reassociate other b.grp
                                if (buildingGroup.HasBuilding())
                                {
                                    foreach (GBuilding building in buildingGroup.GetBuildings())
                                    {
                                        // checking AND building - if not exist it means building Grp cannot delete
                                        // because this building belong to
                                        GBuildingAND buildingAND = building.GetBuildingANDId();
                                        if (buildingAND == null)
                                        {
                                            b_statusCanDelete = false;
                                        }
                                    }
                                }
                            }

                            if (b_statusCanDelete)
                            {
                                ConfirmationFeatureToDelete(buildingGroup);
                            }
                            else
                            {
                                string msgtext = "Can't Delete, Please Diassociate Building in Building Group";
                                MessageBox.Show(msgtext, "Delete Building Group", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            b_statusCanDelete = true;
                            if (buildingGroupAND != null)
                            {
                                if (buildingGroupAND.HasBuildingAND())
                                {
                                    b_statusCanDelete = false;
                                }
                                else
                                {
                                    // checking previous is point to this building group but reassociate other b.grp
                                    if (buildingGroupAND.HasBuilding())
                                    {
                                        foreach (GBuilding building in buildingGroupAND.GetBuildings())
                                        {
                                            GBuildingAND buildingAND = building.GetBuildingANDId();
                                            if (buildingAND == null)
                                            {
                                                b_statusCanDelete = false;
                                            }
                                        }
                                    }
                                }
                            }
                            if (b_statusCanDelete)
                            {
                                ConfirmationFeatureToDelete(buildingGroupAND);
                            }
                            else
                            {
                                string msgtext = "Can't Delete, Please Diassociate Building in Building Group";
                                MessageBox.Show(msgtext, "Delete Building Group", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        // noraini ali - OKT 2020 
                        // Clear Selected Feature
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                    // end added
                    else  // OTHERS USER
                    {
                        //// checking user non-AND cannot touch feature in Working Area
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                        //{
                        //    return;
                        //}

                        if (buildingGroups.Count == 1)
                        {
                            buildingGroup = buildingGroups[0];
                        }
                        else if (buildingGroups.Count > 1)
                        {
                            buildingGroup = ChooseFeature(buildingGroups, null);
                        }

                        if (buildingGroup == null)
                        {
                            return;
                        }

                        //noraini ali -OKT 2020 - highlight feature
                        buildingGroupAND = null;
                        highlightBuildGroup_Building(repo, buildingGroup, buildingGroupAND, _selectedFeaturePoints);
                        highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);

                        // noraini ali - Mei 2021 - check feature lock by AND
                        andBuildingGroup.CheckFeatureLock(buildingGroup.AreaId,SegmentName);

                        // noraini ali - Mei 2021 - check feature on verification
                        andBuildingGroup.CheckFeatureOnVerification(buildingGroup);

                        if (!buildingGroup.HasBuilding())
                        {
                            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                            {
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                        .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                        .SetCaption("Delete Building Group")
                                        .SetText("Are you sure you want to delete this building group?\nId: {0}\n{1}", buildingGroup.OID, buildingGroup.Name);

                                if (confirmBox.Show() == DialogResult.No)
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }
                            }
                        
                            using (new WaitCursor())
                            {
                                if (buildingGroup.IsNavigationReady)
                                {
                                    if (!Session.User.CanDo(Command.NavigationItem))
                                    {
                                        throw new NavigationControlException();
                                    }
                                }

                                repo.StartTransaction(() =>
                                {
                                    foreach (GBuilding building in buildingGroup.GetBuildings())
                                    {
                                        building.GroupId = 0;
                                        repo.Update(building);
                                    }

                                    OnReported("Building group deleted. {0}", buildingGroup.OID);
                                    repo.Delete(buildingGroup);
                                });

                                OnReported(Message1);
                                OnStepReported(Message1);
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                            }
                        }
                        else
                        {
                            string msgtext = "Can't Delete, Please Diassociate Building in Building Group";
                            MessageBox.Show(msgtext, "Delete Building Group", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }

                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        private void ConfirmationFeatureToDelete(GBuildingGroupAND buildingGroupAND)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                .SetDefaultButton(MessageBoxDefaultButton.Button2)
                .SetCaption("Delete Building Group")
                .SetText("Are you sure you want to delete this building group?\nId: {0}\n{1}", buildingGroupAND.OriId, buildingGroupAND.Name);

                if (confirmBox.Show() == DialogResult.No)
                {
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    return;
                }
            }

            using (new WaitCursor())
            {

                if (buildingGroupAND.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }

                repo.StartTransaction(() =>
                {
                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                    andBuildingGroup.Delete(repo, buildingGroupAND);
                });

            }
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            OnReported(Message1);
            OnStepReported(Message1);
        }

        private void ConfirmationFeatureToDelete(GBuildingGroup buildingGroup)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            using (QuestionMessageBox confirmBox = new QuestionMessageBox())
            {
                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                .SetDefaultButton(MessageBoxDefaultButton.Button2)
                .SetCaption("Delete Building Group")
                .SetText("Are you sure you want to delete this building group?\nId: {0}\n{1}", buildingGroup.OID, buildingGroup.Name);

                if (confirmBox.Show() == DialogResult.No)
                {
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    return;
                }
            }

            using (new WaitCursor())
            {

                if (buildingGroup.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }

                repo.StartTransaction(() =>
                {
                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                    andBuildingGroup.Delete(repo, buildingGroup);
                });

            }

            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            OnReported(Message1);
            OnStepReported(Message1);
        }
    }
}

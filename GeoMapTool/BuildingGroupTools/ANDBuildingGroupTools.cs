﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.MapTool.BuildingTools;

namespace Geomatic.MapTool.BuildingGroupTools
{
    // Implementation - noraini ali - Apr 2021- Apply STATUS & AND_STATUS - Data Intergration to NEPS

    public class ANDBuildingGroupTools : ValidateWorkAreaAndStatus
    {
        public void CreateBuildingGroupAND(RepositoryFactory repo, GBuildingGroup BuildingGroup)
        {
            GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(BuildingGroup.OID);
            GBuildingGroupAND BuildingGroupAND = repo.NewObj<GBuildingGroupAND>();
            BuildingGroupAND.CopyFrom(buildingGroup);
            repo.Insert(BuildingGroupAND, false);
        }

        public void AddBuildingGroupAND(RepositoryFactory repo, GBuildingGroup BuildingGroup)
        {
            // update ADM - AreaId
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(BuildingGroup.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => BuildingGroup.AreaId = thearea.OID);
            repo.UpdateInsertByAND(BuildingGroup, true);

            // create new AND as copy from ADM buildingGroup 
            CreateBuildingGroupAND(repo, BuildingGroup);

            // update AND - 
            // CreatedBy, DateCreated, Updatedby & DateUpdated
            GBuildingGroupAND BuildingGroupAND = BuildingGroup.GetBuildingGroupANDId();
            UpdateCreatedByUserAND(BuildingGroupAND);
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.UpdateRemainStatus(BuildingGroupAND, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GBuildingGroup BuildingGroup)
        {
            GBuildingGroupAND absObjAND = BuildingGroup.GetBuildingGroupANDId();
            if (absObjAND == null)
            {
                // Create BuildingGroup AND
                CreateBuildingGroupAND(repo, BuildingGroup);
            }

            // Update AND - User info,STATUS & AND_STATUS as Delete status
            GBuildingGroupAND BuildingGroupAND = BuildingGroup.GetBuildingGroupANDId();
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.UpdateDeleteByAND(BuildingGroupAND, true);

            // update ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(BuildingGroup, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GBuildingGroupAND BuildingGroupAND)
        {
            // Update ADM feature - AND_STATUS & STATUS as Delete status
            GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(BuildingGroupAND.OriId);
            repo.UpdateDeleteByAND(BuildingGroup, true);

            // Update AND table -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.UpdateDeleteByAND(BuildingGroupAND, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GBuildingGroup BuildingGroup)
        {
            GBuildingGroupAND absObjAND = BuildingGroup.GetBuildingGroupANDId();
            if (absObjAND == null)
            {
                // Create BuildingGroup AND
                CreateBuildingGroupAND(repo, BuildingGroup);
            }

            //Get data from the AND Table
            GBuildingGroupAND BuildingGroupAND = BuildingGroup.GetBuildingGroupANDId();

            // update buuilding floor unit 
            ANDBuildingTools andBuilding = new ANDBuildingTools();
            andBuilding.UpdateNumUnit(repo, BuildingGroupAND.OriId);

            //Display form and update AND Table
            using (EditBuildingGroupANDForm form = new EditBuildingGroupANDForm(BuildingGroupAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // to update Area Id for ADM & AND BuildingGroup
                    List<GWorkArea> area;

                    //Set other required data for AND BuildingGroup Table (AreaId, OriId, status n andstatus)
                    area = repo.SpatialSearch<GWorkArea>(BuildingGroup.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

                    // Update AND Feature - AreadId, Updatedby & DateUpdated
                    area.ForEach(thearea => BuildingGroupAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(BuildingGroupAND);
                    repo.Update(BuildingGroupAND);

                    // noraini ali - Apr 2021 - Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => BuildingGroup.AreaId = thearea.OID);
                    BuildingGroup.NumUnit = BuildingGroupAND.NumUnit;
                    repo.UpdateByAND(BuildingGroup, true);

                    // update work area flag as open 
                    int AreaID = BuildingGroupAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GBuildingGroupAND BuildingGroupAND)
        {
            GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(BuildingGroupAND.OriId);
            if (BuildingGroup == null)
            {
                throw new QualityControlException("Cannot proceed, buildingGroup ADM not found.");
            }

            // update buuilding floor unit 
            ANDBuildingTools andBuilding = new ANDBuildingTools();
            andBuilding.UpdateNumUnit(repo, BuildingGroupAND.OriId);

            //Display form and update AND Table
            using (EditBuildingGroupANDForm form = new EditBuildingGroupANDForm(BuildingGroupAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(BuildingGroupAND);
                    repo.Update(BuildingGroupAND, true);

                    // update ADM - STATUS & AND_STATUS & num unit
                    BuildingGroup.NumUnit = BuildingGroupAND.NumUnit;
                    repo.UpdateByAND(BuildingGroup, true);

                    // update work area flag as open 
                    int AreaID = BuildingGroupAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GBuildingGroup BuildingGroup, IPoint newPoint)
        {
            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("BuildingGroup Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != BuildingGroup.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // if AND buildingGroup not exist so create AND BuildingGroup 
            GBuildingGroupAND absObjAND = BuildingGroup.GetBuildingGroupANDId();
            if (absObjAND == null)
            {
                // Create BuildingGroup AND
                CreateBuildingGroupAND(repo, BuildingGroup);
            }

            // set new location for AND BuildingGroup
            GBuildingGroupAND BuildingGroupAND = BuildingGroup.GetBuildingGroupANDId();
            BuildingGroupAND.Shape = newPoint;
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.UpdateGraphic(BuildingGroupAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(BuildingGroup, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GBuildingGroupAND BuildingGroupAND, IPoint newPoint)
        {
            GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(BuildingGroupAND.OriId);
            if (BuildingGroup == null)
            {
                throw new QualityControlException("Cannot proceed, buildingGroup ADM not found.");
            }

            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("BuildingGroup Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != BuildingGroupAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // set new location for AND BuildingGroup & STATUS & AND_STATUS (Update Graphic)
            BuildingGroupAND.Shape = newPoint;
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.UpdateGraphic(BuildingGroupAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(BuildingGroup, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GBuildingGroup BuildingGroup, int streetId)
        {
            GBuildingGroupAND absObjAND = BuildingGroup.GetBuildingGroupANDId();
            if (absObjAND == null)
            {
                // Create BuildingGroup AND
                CreateBuildingGroupAND(repo, BuildingGroup);
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            GBuildingGroupAND BuildingGroupAND = BuildingGroup.GetBuildingGroupANDId();
            BuildingGroupAND.StreetId = streetId;
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.Update(BuildingGroupAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(BuildingGroup, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GBuildingGroupAND BuildingGroupAND, int streetId)
        {
            GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(BuildingGroupAND.OriId);
            if (BuildingGroup == null)
            {
                throw new QualityControlException("Cannot proceed, buildingGroup ADM not found.");
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            BuildingGroupAND.StreetId = streetId;
            UpdateModifiedByUserAND(BuildingGroupAND);
            repo.Update(BuildingGroupAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(BuildingGroup, true);

            // update work area flag as open 
            int AreaID = BuildingGroupAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        // noraini ali - OKT 2020
        public List<GBuildingGroup> FilterBuildingGroupInWorkArea(List<GBuildingGroup> _buildingGroups, int _workareaid)
        {
            // filter ADM BuildingGroup - get only fresh ADM
            List<GBuildingGroup> BuildingGroups = new List<GBuildingGroup>();
            foreach (GBuildingGroup _buildingGroupTemp in _buildingGroups)
            {
                if (_buildingGroupTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_buildingGroupTemp.AndStatus.ToString()) || (_buildingGroupTemp.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_buildingGroupTemp.AreaId.ToString()) || (_buildingGroupTemp.AreaId > 0))
                        {
                            if (_buildingGroupTemp.AreaId.Value == _workareaid)
                            {
                                BuildingGroups.Add(_buildingGroupTemp);
                            }
                        }
                    }
                }
            }
            return BuildingGroups;
        }

        public List<GBuildingGroupAND> FilterBuildingGroupInWorkArea(List<GBuildingGroupAND> _BuildingGroups, int _workareaid)
        {
            // filter ADM BuildingGroup - get only fresh ADM
            List<GBuildingGroupAND> BuildingGroupsAND = new List<GBuildingGroupAND>();
            foreach (GBuildingGroupAND _buildingGroupTemp in _BuildingGroups)
            {
                if (_buildingGroupTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_buildingGroupTemp.AreaId.ToString()) || _buildingGroupTemp.AreaId > 0)
                    {
                        if (_buildingGroupTemp.AreaId.Value == _workareaid)
                        {
                            BuildingGroupsAND.Add(_buildingGroupTemp);
                        }
                    }
                }
            }
            return BuildingGroupsAND;
        }

        public void CheckFeatureOnVerification(GBuildingGroup buildingGrp)
        {
            GBuildingGroupAND buildingGrpAND = buildingGrp.GetBuildingGroupANDId();
            if (buildingGrpAND != null)
            {
                throw new QualityControlException("Unable to edit feature without verification.");
            }
        }

        // noraini ali - OKT 2020
        public List<GStreet> FilterStreetInWorkArea(List<GStreet> _streets, int _workareaid)
        {
            // filter ADM Street - get only fresh ADM
            List<GStreet> streets = new List<GStreet>();
            foreach (GStreet _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_streetTemp.AndStatus.ToString()) || (_streetTemp.AndStatus == 0))
                    {
                        if ((!string.IsNullOrEmpty(_streetTemp.AreaId.ToString())) || _streetTemp.AreaId > 0)
                        {
                            if (_streetTemp.AreaId.Value == _workareaid)
                            {
                                streets.Add(_streetTemp);
                            }
                        }
                    }
                }
            }
            return streets;
        }

        public List<GStreetAND> FilterStreetInWorkArea(List<GStreetAND> _streets, int _workareaid)
        {
            // filter AND Street only in this working area
            List<GStreetAND> streetsAND = new List<GStreetAND>();
            foreach (GStreetAND _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_streetTemp.AreaId.ToString()) || _streetTemp.AreaId > 0)
                    {
                        if (_streetTemp.AreaId.Value == _workareaid)
                        {
                            streetsAND.Add(_streetTemp);
                        }
                    }
                }
            }
            return streetsAND;
        }
    }
}


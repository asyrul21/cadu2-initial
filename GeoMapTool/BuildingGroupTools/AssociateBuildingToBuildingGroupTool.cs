﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Earthworm.AO;
using Geomatic.MapTool.BuildingTools;
using Geomatic.Core.Exceptions;
using GMultiStorey = Geomatic.Core.Rows.GMultiStorey;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public partial class AssociateBuildingToBuildingGroupTool : ReassociateTool
    {
        #region Fields

        //protected List<GBuildingGroup> _selectedBuildingGroups;
        protected GBuildingGroup _selectedBuildingGroup;
        protected GBuilding _selectedBuilding;
        protected List<MovePoint> _selectedBuildingGroupPoints;
        protected List<MovePoint> _selectedBuildingPoints;
        private string message1 = "Select a Building Group.";
        private string message2 = "Select a Building to Associate.";
        protected GBuildingGroupAND _selectedBuildingGroupAND;
        protected GBuildingAND _selectedBuildingAND;
        // noraini ali - OKT 2020
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        int _workAreaId = 0;
        #endregion

        public AssociateBuildingToBuildingGroupTool()
        {
            _name = "Associate Building to Building Group";
            InitMenu();
            _selectedBuildingGroupPoints = new List<MovePoint>();
            _selectedBuildingPoints = new List<MovePoint>();
            // noraini ali - OKT 2020
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(message1);
            OnStepReported(message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                List<GBuildingGroup> buildingGroups;
                                List<GBuildingGroupAND> buildingGroupsAND;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    buildingGroups = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    buildingGroupsAND = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                    {
                                        return;
                                    }

                                    // addded by noraini ali - Mei 2020
                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id & Checking point in Working Area & Owner
                                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                                        andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                                        // filter street ADM & AND in this working area only
                                        if (buildingGroups.Count > 0)
                                        {
                                            buildingGroups = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroups, _workAreaId);
                                        }
                                        if (buildingGroupsAND.Count > 0)
                                        {
                                            buildingGroupsAND = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroupsAND, _workAreaId);
                                        }

                                        if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                        {
                                            throw new Exception("Building Group not found.");
                                        }

                                        if (buildingGroups.Count > 0)
                                        {
                                            _selectedBuildingGroup = buildingGroups[0];

                                            //MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            //movePoint.Point = _selectedBuildingGroup.Point;
                                            //movePoint.Start(_selectedBuildingGroup.Point);
                                            //_selectedBuildingGroupPoints.Add(movePoint);

                                            highlightBuildingGroup(repo, _selectedBuildingGroup.OID, true, _selectedFeaturePoints);
                                            _selectedBuildingGroupPoints = _selectedFeaturePoints;

                                            //noraini ali -OKT 2020 - highlight feature
                                            foreach (GBuilding building in _selectedBuildingGroup.GetBuildings())
                                            {
                                                GBuildingAND buildingAND = building.GetBuildingANDId();
                                                if (buildingAND == null)
                                                {
                                                    highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                                                }
                                            }

                                            foreach (GBuildingAND buildingAND in _selectedBuildingGroup.GetBuildingsAND())
                                            {
                                                highlightBuilding(repo, buildingAND.OriId, false, _selectedFeaturePoints);
                                            }
                                        }
                                        else
                                        {
                                            if (buildingGroupsAND.Count > 0)
                                            {
                                                _selectedBuildingGroupAND = buildingGroupsAND[0];

                                                //MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                                //movePoint.Point = _selectedBuildingGroupAND.Point;
                                                //movePoint.Start(_selectedBuildingGroupAND.Point);
                                                //_selectedBuildingGroupPoints.Add(movePoint);

                                                highlightBuildingGroup(repo, _selectedBuildingGroupAND.OriId, true, _selectedFeaturePoints);
                                                _selectedBuildingGroupPoints = _selectedFeaturePoints;

                                                //noraini ali -OKT 2020 - highlight feature
                                                foreach (GBuilding building in _selectedBuildingGroupAND.GetBuildings())
                                                {
                                                    GBuildingAND buildingAND = building.GetBuildingANDId();
                                                    if (buildingAND == null)
                                                    {
                                                        highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                                                    }
                                                }
                                                foreach (GBuildingAND buildingAND in _selectedBuildingGroupAND.GetBuildingsAND())
                                                {
                                                    highlightBuilding(repo, buildingAND.OriId, false, _selectedFeaturePoints);
                                                }
                                            }
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported(message2 + " or right-click for reselect menu.");
                                        OnStepReported(message2 + " or right-click for reselect menu.");

                                    }
                                    else  //OTHERS USER
                                    {
                                        //// checking user non-AND cannot touch feature in Working Area
                                        //ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                                        //{
                                        //    return;
                                        //}

                                        _selectedBuildingGroup = buildingGroups[0];

                                        // added by noraini - 21/05/2019
                                        //MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                        //movePoint.Point = _selectedBuildingGroup.Point;
                                        //movePoint.Start(_selectedBuildingGroup.Point);
                                        //_selectedBuildingGroupPoints.Add(movePoint);

                                        highlightBuildingGroup(repo, _selectedBuildingGroup.OID, true, _selectedFeaturePoints);
                                        _selectedBuildingGroupPoints = _selectedFeaturePoints;
                                        // end

                                        //noraini ali -OKT 2020 - highlight feature
                                        foreach (GBuilding building in _selectedBuildingGroup.GetBuildings())
                                        {
                                            highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                                        }
                                    }

                                    _progress = Progress.SelectParent;
                                    OnReported(message2 + " or right-click for reselect menu.");
                                    OnStepReported(message2 + " or right-click for reselect menu.");
                                }
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuilding> buildings = SelectBuilding(repo, point);
                                List<GBuildingAND> buildingsAND = SelectBuildingAND(repo, point);

                                if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id
                                    ANDBuildingTools andBuilding = new ANDBuildingTools();
                                    andBuilding.CheckWithinWorkArea(SegmentName, point);
                                    andBuilding.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaIdBuilding = andBuilding.GetWorkAreaId(SegmentName, point);

                                    if (_workAreaIdBuilding != _workAreaId)
                                    {
                                        throw new Exception("Unable associate building in different Work Area.");
                                    }

                                    // filter feature building in work area
                                    if (buildings.Count > 0)
                                    {
                                        buildings = andBuilding.FilterBuildingInWorkArea(buildings, _workAreaId);
                                    }
                                    if (buildingsAND.Count > 0)
                                    {
                                        buildingsAND = andBuilding.FilterBuildingInWorkArea(buildingsAND, _workAreaId);
                                    }

                                    if (buildings.Count == 0 && buildingsAND.Count == 0)
                                    {
                                        return;
                                    }

                                    // Selected ADM building
                                    if (buildings.Count > 0)
                                    {
                                        _selectedBuilding = buildings[0];

                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                        movePoint.Point = _selectedBuilding.Point;
                                        movePoint.Start(_selectedBuilding.Point);
                                        _selectedBuildingPoints.Add(movePoint);

                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        // selected ADM building Group
                                        if (_selectedBuildingGroupAND == null)
                                        {
                                            confirmBox.SetCaption("Associate Building Group")
                                                        .SetText("Are you sure you want to associate {0} to {1} ?", StringUtils.TrimSpaces(_selectedBuilding.Name), StringUtils.TrimSpaces(_selectedBuildingGroup.Name));

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                using (new WaitCursor())
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        ANDBuildingTools AndBuilding = new ANDBuildingTools();
                                                        AndBuilding.Associate(repo, _selectedBuilding, _selectedBuildingGroup.OID);
                                                    });
                                                    OnReported(message1);
                                                    OnStepReported(message1);
                                                    Reset();
                                                }
                                            }
                                            else
                                            {
                                                _selectedBuilding = null;
                                                foreach (MovePoint movePoint1 in _selectedBuildingPoints)
                                                {
                                                    movePoint1.Stop();
                                                }
                                                _selectedBuildingPoints.Clear();
                                            }
                                        }
                                        else // AND building Group
                                        {
                                            confirmBox.SetCaption("Associate Building Group")
                                                        .SetText("Are you sure you want to associate {0} to {1} ?", StringUtils.TrimSpaces(_selectedBuilding.Name), StringUtils.TrimSpaces(_selectedBuildingGroupAND.Name));

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                using (new WaitCursor())
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        ANDBuildingTools AndBuilding = new ANDBuildingTools();
                                                        AndBuilding.Associate(repo, _selectedBuilding, _selectedBuildingGroupAND.OriId);
                                                    });
                                                    OnReported(message1);
                                                    OnStepReported(message1);
                                                    Reset();
                                                }
                                            }
                                            else
                                            {
                                                _selectedBuilding = null;
                                                foreach (MovePoint movePoint1 in _selectedBuildingPoints)
                                                {
                                                    movePoint1.Stop();
                                                }
                                                _selectedBuildingPoints.Clear();
                                            }
                                        }
                                    }
                                    else // Selected AND building
                                    {
                                        if (buildingsAND.Count > 0)
                                        {
                                            _selectedBuildingAND = buildingsAND[0];

                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = _selectedBuildingAND.Point;
                                            movePoint.Start(_selectedBuildingAND.Point);
                                            _selectedBuildingPoints.Add(movePoint);

                                            QuestionMessageBox confirmBox = new QuestionMessageBox();
                                            // Selected ADM building Group
                                            if (_selectedBuildingGroup != null)
                                            {
                                                confirmBox.SetCaption("Associate Building Group")
                                                            .SetText("Are you sure you want to associate {0} to {1} ?", StringUtils.TrimSpaces(_selectedBuildingAND.Name), StringUtils.TrimSpaces(_selectedBuildingGroup.Name));

                                                if (confirmBox.Show() == DialogResult.Yes)
                                                {
                                                    using (new WaitCursor())
                                                    {
                                                        repo.StartTransaction(() =>
                                                        {
                                                            ANDBuildingTools AndBuilding = new ANDBuildingTools();
                                                            AndBuilding.Associate(repo, _selectedBuildingAND, _selectedBuildingGroup.OID);
                                                        });
                                                        OnReported(message1);
                                                        OnStepReported(message1);
                                                        Reset();
                                                    }
                                                }
                                                else 
                                                {
                                                    _selectedBuildingAND = null;
                                                    foreach (MovePoint movePoint1 in _selectedBuildingPoints)
                                                    {
                                                        movePoint1.Stop();
                                                    }
                                                    _selectedBuildingPoints.Clear();
                                                }
                                            }
                                            else // Selected AND building Group
                                            {
                                                confirmBox.SetCaption("Associate Building Group")
                                                        .SetText("Are you sure you want to associate {0} to {1} ?", StringUtils.TrimSpaces(_selectedBuildingAND.Name), StringUtils.TrimSpaces(_selectedBuildingGroupAND.Name));

                                                if (confirmBox.Show() == DialogResult.Yes)
                                                {
                                                    using (new WaitCursor())
                                                    {
                                                        repo.StartTransaction(() =>
                                                        {
                                                            ANDBuildingTools AndBuilding = new ANDBuildingTools();
                                                            AndBuilding.Associate(repo, _selectedBuildingAND, _selectedBuildingGroupAND.OriId);
                                                        });
                                                        OnReported(message1);
                                                        OnStepReported(message1);
                                                        Reset();
                                                    }
                                                }
                                                else
                                                {
                                                    _selectedBuildingAND = null;
                                                    foreach (MovePoint movePoint1 in _selectedBuildingPoints)
                                                    {
                                                        movePoint1.Stop();
                                                    }
                                                    _selectedBuildingPoints.Clear();
                                                }
                                            }
                                        }
                                    }
                                }
                                else // OTHERS USER
                                {
                                    if (buildings.Count > 0)
                                    {
                                        _selectedBuilding = buildings[0];

                                        // 
                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                        movePoint.Point = _selectedBuilding.Point;
                                        movePoint.Start(_selectedBuilding.Point);
                                        _selectedBuildingPoints.Add(movePoint);
                                        // end

                                        ANDBuildingTools andBuilding = new ANDBuildingTools();

                                        // noraini ali - Mei 2021 - check feature lock by AND
                                        andBuilding.CheckFeatureLock(_selectedBuilding.AreaId,SegmentName);

                                        // noraini ali - Mei 2021 - check feature on verification
                                        andBuilding.CheckFeatureOnVerification(_selectedBuilding);

                                        QuestionMessageBox confirmBox = new QuestionMessageBox();

                                        confirmBox.SetCaption("Associate Building Group")
                                            .SetText("Are you sure you want to associate {0} to {1} ?", StringUtils.TrimSpaces(_selectedBuilding.Name), StringUtils.TrimSpaces(_selectedBuildingGroup.Name));


                                        if (confirmBox.Show() == DialogResult.Yes)
                                        {
                                            using (new WaitCursor())
                                            {
                                                repo.StartTransaction(() =>
                                                { 
                                                    _selectedBuilding.GroupId = _selectedBuildingGroup.OID;
                                                    repo.Update(_selectedBuilding);

                                                    // update num unit
                                                    AutoUpdateNumUnit obj = new AutoUpdateNumUnit();
                                                    obj.UpdateTotalNumUnit(repo, _selectedBuildingGroup.OID);
                                                });
                                                OnReported(message1);
                                                OnStepReported(message1);
                                                Reset();
                                            }
                                        }
                                        else
                                        {
                                            _selectedBuilding = null;
                                            foreach (MovePoint movePoint1 in _selectedBuildingPoints)
                                            {
                                                movePoint1.Stop();
                                            }
                                            _selectedBuildingPoints.Clear();
                                        }

                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported(message1);
            OnStepReported(message1);
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedBuilding = null;
            _selectedBuildingGroup = null;
            foreach (MovePoint movePoint in _selectedBuildingGroupPoints)
            {
                movePoint.Stop();
            }
            _selectedBuildingGroupPoints.Clear();
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            foreach (MovePoint movePoint in _selectedBuildingGroupPoints)
            {
                movePoint.Refresh(hDC);
            }
        }
    }
}

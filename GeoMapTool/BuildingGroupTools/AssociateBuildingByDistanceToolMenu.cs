﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.BuildingGroupTools
{
    partial class AssociateBuildingByDistanceTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuReselect;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuReselect = new ToolStripMenuItem();
            // 
            // menuReselect
            //
            this.menuReselect.Name = "menuReselect";
            this.menuReselect.Text = "Reselect";
            this.menuReselect.Click += new EventHandler(this.OnReselect);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] {
            this.menuReselect});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

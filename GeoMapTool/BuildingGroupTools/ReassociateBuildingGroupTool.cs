﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Earthworm.AO;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public partial class ReassociateBuildingGroupTool : ReassociateTool
    {
        #region Fields

        protected List<GBuildingGroup> _selectedBuildingGroups;
        protected List<GBuildingGroupAND> _selectedBuildingGroupsAND;
        protected List<MovePoint> _selectedBuildingGroupPoints;
        int _workAreaId = 0;

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        #endregion

        public ReassociateBuildingGroupTool()
        {
            _name = "Reassociate Building Group";
            InitMenu();
            _selectedBuildingGroupPoints = new List<MovePoint>();

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a building group to reassociate.");
            OnStepReported("Select a building group to reassociate.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                                List<GBuildingGroup> buildingGroups;
                                List<GBuildingGroupAND> buildingGroupsAND;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    buildingGroups = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    buildingGroupsAND = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                    {
                                        return;
                                    }

                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id & Checking point in Working Area & Owner
                                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                                        andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                                        // filter street ADM & AND in this working area only

                                        if (buildingGroups.Count > 0)
                                        {
                                            buildingGroups = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroups, _workAreaId);
                                        }

                                        if (buildingGroupsAND.Count > 0)
                                        {
                                            buildingGroupsAND = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroupsAND, _workAreaId);
                                        }

                                        if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                        {
                                            throw new QualityControlException("Building Group is not in this work area.");
                                        }

                                        _selectedBuildingGroups = buildingGroups;
                                        _selectedBuildingGroupsAND = buildingGroupsAND;

                                        foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                                        {
                                            //MovePoint movePoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = buildingGroup.Point;
                                            movePoint.Start(buildingGroup.Point);
                                            _selectedBuildingGroupPoints.Add(movePoint);

                                            // noraini ali - OKT 2020
                                            if (buildingGroup.StreetId.HasValue)
                                            {
                                                highlightStreet(repo, buildingGroup.StreetId.Value, false, _selectedFeatureLines);
                                            }
                                        }

                                        foreach (GBuildingGroupAND buildingGroup in _selectedBuildingGroupsAND)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = buildingGroup.Point;
                                            movePoint.Start(buildingGroup.Point);
                                            _selectedBuildingGroupPoints.Add(movePoint);

                                            // noraini ali - OKT 2020 - hightlight feature
                                            if (buildingGroup.StreetId.HasValue)
                                            {
                                                highlightStreet(repo, buildingGroup.StreetId.Value, false, _selectedFeatureLines);
                                            }
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a street for building group association or right-click for reselect menu.");
                                        OnStepReported("Select a street for building group association or right-click for reselect menu.");
                                    }
                                    else  // OTHERS USER
                                    {
                                        //// checking user non-AND cannot touch feature in Working Area
                                        //ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                                        //{
                                        //    return;
                                        //}

                                        _selectedBuildingGroups = buildingGroups;

                                        foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                                        {
                                            if (Core.Sessions.Session.User.GetGroup().Name == "MLI_UPDATE")
                                            {
                                                if (buildingGroup.IsNavigationReady)
                                                {
                                                    throw new QualityControlException("Building Group Navi is ready, cannot reassociate by user group MLI");
                                                }
                                            }

                                            // noraini ali - Mei 2021 - check feature lock by AND
                                            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                            andBuildingGroup.CheckFeatureLock(buildingGroup.AreaId,SegmentName);

                                            // noraini ali - Mei 2021 - check feature on verification
                                            andBuildingGroup.CheckFeatureOnVerification(buildingGroup);
                                        }

                                        foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = buildingGroup.Point;
                                            movePoint.Start(buildingGroup.Point);
                                            _selectedBuildingGroupPoints.Add(movePoint);

                                            // noraini ali - OKT 2020
                                            if (buildingGroup.StreetId.HasValue)
                                            {
                                                highlightStreet(repo, buildingGroup.StreetId.Value, false, _selectedFeatureLines);
                                            }
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a street for building group association or right-click for reselect menu.");
                                        OnStepReported("Select a street for building group association or right-click for reselect menu.");
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                if (streets.Count == 0 && streetsAND.Count == 0)
                                {
                                    return;
                                }

                                // Noraini ali - OKT 2020
                                if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id & Checking point in Working Area & Owner
                                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                    andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                                    andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaIdStreet = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                                    if (_workAreaIdStreet != _workAreaId)
                                    {
                                        throw new Exception("Unable Reassociate to Street in diffrence Work Area.");
                                    }

                                    // filter street ADM & AND in this working area only
                                    if (streets.Count > 0)
                                    {
                                        streets = andBuildingGroup.FilterStreetInWorkArea(streets, _workAreaId);
                                    }

                                    if (streetsAND.Count > 0)
                                    {
                                        streetsAND = andBuildingGroup.FilterStreetInWorkArea(streetsAND, _workAreaId);
                                    }

                                    if (streets.Count == 0 && streetsAND.Count == 0)
                                    {
                                        throw new Exception("Selected street is not in this work area.");
                                    }

                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        // noraini ali - OKT 2020
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                        ReassociateNewStreet(repo, street, point);
                                    }
                                    else
                                    {
                                        if (streetsAND.Count > 0)
                                        {
                                            GStreetAND street = streetsAND[0];
                                            // noraini ali - OKT 2020
                                            highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                            ReassociateNewStreet(repo, street, point);
                                        }
                                    }

                                    OnReported("Select a building group to reassociate.");
                                    OnStepReported("Select a building group to reassociate.");
                                    Reset(); 
                                }
                                else  // OTHERS USER
                                {

                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        // noraini ali - OKT 2020
                                        highlightStreet(repo, street.OID, false, _selectedFeatureLines);

                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        confirmBox.SetCaption("Reassociate Building Group")
                                                  .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

                                        if (confirmBox.Show() == DialogResult.Yes)
                                        {
                                            using (new WaitCursor())
                                            {
                                                repo.StartTransaction(() =>
                                                {
                                                    foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                                                    {
                                                        buildingGroup.StreetId = street.OID;
                                                        repo.Update(buildingGroup);
                                                    }
                                                });
                                                OnReported("Select a building group to reassociate.");
                                                OnStepReported("Select a building group to reassociate.");
                                                Reset();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported("Select a building group to reassociate.");
            OnStepReported("Select a building group to reassociate.");
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedBuildingGroups = null;
            _selectedBuildingGroupsAND = null;

            foreach (MovePoint movePoint in _selectedBuildingGroupPoints)
            {
                movePoint.Stop();
            }
            _selectedBuildingGroupPoints.Clear();

            // noraini ali - OKT 2020 
            // Clear Selected Feature
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            foreach (MovePoint movePoint in _selectedBuildingGroupPoints)
            {
                movePoint.Refresh(hDC);
            }
        }

        // noraini ali - OKT 2020
        private void ReassociateNewStreet(RepositoryFactory repo, GStreet street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Landmark")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                        {
                            andBuildingGroup.Reassociate(repo, buildingGroup, street.OID);
                        }
                        foreach (GBuildingGroupAND buildingGroupAND in _selectedBuildingGroupsAND)
                        {
                            andBuildingGroup.Reassociate(repo, buildingGroupAND, street.OID);
                        }
                    });
                }
            }
        }

        private void ReassociateNewStreet(RepositoryFactory repo, GStreetAND street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Landmark")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        foreach (GBuildingGroup buildingGroup in _selectedBuildingGroups)
                        {
                            andBuildingGroup.Reassociate(repo, buildingGroup, street.OriId);
                        }

                        foreach (GBuildingGroupAND buildingGroupAND in _selectedBuildingGroupsAND)
                        {
                            andBuildingGroup.Reassociate(repo, buildingGroupAND, street.OriId);
                        }
                    });
                }
            }
        }
    }
}

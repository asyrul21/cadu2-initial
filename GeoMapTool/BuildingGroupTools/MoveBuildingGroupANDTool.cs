﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.UI.Utilities;
using System.Windows.Forms;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public class MoveBuildingGroupANDTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GBuildingGroupAND _selectedBuildingGroup;
        protected MovePoint _selectedBuildingGroupPoint;

        private const string Message1 = "Select a building group (AND) to move.";
        private const string Message2 = "Drag selected building group (AND) to move.";

        #endregion

        public MoveBuildingGroupANDTool()
        {
            _name = "Move Building Group (AND)";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedBuildingGroupPoint == null)
            {
                _selectedBuildingGroupPoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
            }
            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuildingGroup.Point))
                            {
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedBuildingGroupPoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingGroupAND> buildingGroups = SelectBuildingGroup(repo, point);
                                if (buildingGroups.Count > 0)
                                {
                                    GBuildingGroupAND buildingGroup = buildingGroups[0];
                                    _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                    _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                    _selectedBuildingGroup = buildingGroup;

                                    GStreet street = buildingGroup.GetStreet();
                                    if (street != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                        _parentRadius.Start(point);
                                    }

                                    _progress = Progress.TryMove;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingGroupAND> buildingGroups = SelectBuildingGroup(repo, point);
                                if (buildingGroups.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedBuildingGroupPoint.Stop();
                                    _selectedBuildingGroup = null;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                GBuildingGroupAND buildingGroup = buildingGroups[0];
                                if (!_selectedBuildingGroup.Equals(buildingGroup))
                                {
                                    _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                    _selectedBuildingGroupPoint.Stop();
                                    _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                    _selectedBuildingGroup = buildingGroup;

                                    GStreet street = buildingGroup.GetStreet();
                                    if (street != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                        _parentRadius.Stop();
                                        _parentRadius.Start(point);
                                    }
                                    else
                                    {
                                        _parentRadius.Stop();
                                    }
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }

                            IPoint newPoint;
                            GStreet street;
                            List<GStreet> streets;
                            try
                            {
                                using (new WaitCursor())
                                {
                                    newPoint = (IPoint)_selectedBuildingGroupPoint.Stop();
                                    street = _selectedBuildingGroup.GetStreet();
                                }
                                if (street != null)
                                {
                                    using (new WaitCursor())
                                    {
                                        double distance = newPoint.DistanceTo(street.Shape);
                                        streets = repo.SpatialSearch<GStreet>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                                    }
                                    if (streets.Count > 0)
                                    {
                                        GStreet nearestStreet = GeometryUtils.GetNearestFeature(streets, newPoint);
                                        if (!nearestStreet.Equals(street))
                                        {
                                            QuestionMessageBox chooseNewBox = new QuestionMessageBox();
                                            chooseNewBox.SetCaption("Move Building Group (AND)")
                                                        .SetText("Another street nearby. Choose new street?");

                                            if (chooseNewBox.Show() == DialogResult.Yes)
                                            {
                                                GStreet newStreet = ChooseFeature(streets, nearestStreet);
                                                _selectedBuildingGroup.StreetId = newStreet.OID;
                                            }
                                        }
                                    }
                                }
                                using (new WaitCursor())
                                {
                                    repo.StartTransaction(() =>
                                    {
                                        _selectedBuildingGroup.Shape = newPoint;
                                        repo.Update(_selectedBuildingGroup);
                                    });
                                }
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedBuildingGroup = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GBuildingGroupAND> SelectBuildingGroup(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Stop();
                _selectedBuildingGroupPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Refresh(hDC);
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Earthworm.AO;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public class EditBuildingGroupTool : EditTool
    {
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public EditBuildingGroupTool()
        {
            _name = "Edit Building Group";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a building group to edit.");
            OnStepReported("Select a building group to edit.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GBuildingGroup> buildingGroups;
                    GBuildingGroup buildingGroup = null;

                    // added by noraini ali - Mei 2020
                    List<GBuildingGroupAND> buildingGroupsAND;
                    GBuildingGroupAND buildingGroupAND = null;
                    // end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        buildingGroups = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        buildingGroupsAND = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                    {
                        return;
                    }

                    int _workAreaId = 0;
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id 
                        ANDBuildingGroupTools andBuildingGrp = new ANDBuildingGroupTools();
                        _workAreaId = andBuildingGrp.GetWorkAreaId(SegmentName, point);

                        if (_workAreaId > 0)
                        {
                            List<GBuildingGroup> _NewListbuildingGroups = new List<GBuildingGroup>();
                            List<GBuildingGroupAND> _NewListbuildingGroupsAND = new List<GBuildingGroupAND>();

                            if (buildingGroups.Count > 0)
                            {
                                _NewListbuildingGroups = andBuildingGrp.FilterBuildingGroupInWorkArea(buildingGroups, _workAreaId);
                            }

                            if (buildingGroupsAND.Count > 0)
                            {
                                _NewListbuildingGroupsAND = andBuildingGrp.FilterBuildingGroupInWorkArea(buildingGroupsAND, _workAreaId);
                            }

                            //Assign filter feature in new list
                            List<GBuildingGroup> _buildingGroups = new List<GBuildingGroup>();

                            foreach (GBuildingGroup buildingGrpTemp in _NewListbuildingGroups)
                            {
                                _buildingGroups.Add(buildingGrpTemp);
                            }

                            foreach (GBuildingGroupAND buildingGrpTempAND in _NewListbuildingGroupsAND)
                            {
                                GBuildingGroup BuildingGrp = repo.GetById<GBuildingGroup>(buildingGrpTempAND.OriId);
                                if (BuildingGrp != null)
                                    _buildingGroups.Add(BuildingGrp);
                            }

                            buildingGroupAND = null;
                            if (_buildingGroups.Count == 0)
                            {
                                return;
                            }
                            else if (_buildingGroups.Count == 1)
                            {
                                buildingGroup = _buildingGroups[0];
                                buildingGroupAND = null;

                                GBuildingGroupAND buildingGrpTempAND = buildingGroup.GetBuildingGroupANDId();
                                if (buildingGrpTempAND != null)
                                {
                                    buildingGroupAND = buildingGrpTempAND;
                                    buildingGroup = null;
                                }

                                //noraini ali -OKT 2020
                                highlightBuildGroup_Building(repo, buildingGroup, buildingGroupAND, _selectedFeaturePoints);
                                if (buildingGroupAND != null && buildingGroupAND.StreetId.HasValue)
                                {
                                    highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);
                                }
                            }
                            else
                            {
                                buildingGroup = ChooseFeature(_buildingGroups, null);
                                buildingGroupAND = null;

                                // if OID exist in AND feature
                                GBuildingGroupAND BuildingGrpTempAND = repo.GetById<GBuildingGroupAND>(buildingGroup.OID);
                                if (BuildingGrpTempAND != null)
                                {
                                    buildingGroupAND = BuildingGrpTempAND;
                                    buildingGroup = null;
                                }

                                if (buildingGroupAND != null && buildingGroupAND.StreetId.HasValue)
                                {
                                    highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);
                                }
                            }       
                        }
                        else // feature not in work area
                        {
                            buildingGroup = null;
                            buildingGroupAND = null;

                            if (buildingGroups.Count > 0)
                            {
                                buildingGroup = buildingGroups[0];

                            }
                            else if (buildingGroups.Count == 0)
                            {
                                if (buildingGroupsAND.Count == 0)
                                {
                                    return;
                                }
                                else if (buildingGroupsAND.Count > 0)
                                {
                                    buildingGroupAND = buildingGroupsAND[0];
                                }
                            }

                            //noraini ali -OKT 2020
                            highlightBuildGroup_Building(repo, buildingGroup, buildingGroupAND, _selectedFeaturePoints);

                            if (buildingGroupAND != null && buildingGroupAND.StreetId.HasValue)
                            {
                                highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);
                            }
                        }
                    }
                    else  // OTHERS USER
                    {
                        //// checking user non-AND cannot touch feature in Working Area
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        andBuildingGroup.checkingPointInWorkArea(SegmentName, point); 

                        buildingGroupAND = null;
                        if (buildingGroups.Count == 1)
                        {
                            buildingGroup = buildingGroups[0];
                            //noraini ali -OKT 2020
                            highlightBuildGroup_Building(repo, buildingGroup, buildingGroupAND, _selectedFeaturePoints);
                            highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);
                        }
                        else if (buildingGroups.Count > 1)
                        {
                            buildingGroup = ChooseFeature(buildingGroups, null);
                            //noraini ali -OKT 2020
                            highlightBuildGroup_StreetId(repo, buildingGroup, buildingGroupAND, _selectedFeatureLines);
                        }

                        // noraini ali - Mei 2021 - check feature lock by AND
                        andBuildingGroup.CheckFeatureLock(buildingGroup.AreaId,SegmentName);

                        // noraini ali - Mei 2021 - check feature on verification
                        andBuildingGroup.CheckFeatureOnVerification(buildingGroup);

                        if (buildingGroup == null)
                        {
                            return;
                        }
                    }

                    bool success = false;

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        // noraini ali - Apr 2020
                        // checking feature can edit or review only
                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                        bool _CanEdit = andBuildingGroup.ValidateUserCanEdit(SegmentName, _workAreaId);
                        if (_CanEdit)
                        {
                            if (buildingGroupAND != null)
                            {
                                andBuildingGroup.Update(repo, buildingGroupAND);
                                OnReported("Building group updated. {0}", buildingGroupAND.OID);
                            }
                            else
                            { 
                                andBuildingGroup.Update(repo, buildingGroup);
                                OnReported("Building group updated. {0}", buildingGroup.OID);
                            }
                        }
                        // end
                        else
                        {
                            // noraini ali - NOV 2020
                            if (buildingGroup == null)
                            {
                                andBuildingGroup.Update(repo, buildingGroupAND);
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }
                            // end

                            // update num unit
                            AutoUpdateNumUnit obj = new AutoUpdateNumUnit();
                            obj.UpdateTotalNumUnit(repo, buildingGroup.OID);

                            using (EditBuildingGroupForm form = new EditBuildingGroupForm(buildingGroup))
                            {
                                if (form.ShowDialog() != DialogResult.OK)
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                // noraini ali - Jun 2020 - not allow to edit feature in working area
                                if (Geomatic.Core.Sessions.Session.User.GetGroup().Name != "SUPERVISOR")
                                {
                                    // noraini ali - Aug 2021 - check feature lock by AND
                                    andBuildingGroup.CheckFeatureLock(buildingGroup.AreaId, SegmentName);
                                }
                                else
                                {
                                    // noraini - Apr 2021 - Add rule - not allow supervisor to delete AND feature without verification. 
                                    GBuildingGroupAND BuildingGrpAND = buildingGroup.GetBuildingGroupANDId();
                                    if (BuildingGrpAND != null)
                                    {
                                        throw new QualityControlException("Unable edit feature without verification.");
                                    }
                                }
                                // end add

                                using (new WaitCursor())
                                {
                                    form.SetValues();
                                    repo.Update(buildingGroup);
                                }
                            }
                            OnReported("Building group updated. {0}", buildingGroup.OID);
                        }
                        OnReported("Select a building group to edit.");
                        OnStepReported("Select a building group to edit.");
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        success = true;
                    }
                    catch
                    {
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Earthworm.AO;
using Geomatic.MapTool.BuildingTools;
using GMultiStorey = Geomatic.Core.Rows.GMultiStorey;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public partial class AssociateBuildingByDistanceTool : ReassociateTool
    {
        #region Fields

        //protected List<GBuildingGroup> _selectedBuildingGroups;
        protected GBuildingGroup _selectedBuildingGroup = null;
        protected GBuilding _selectedBuilding;
        protected IEnumerable<IGFeature> _selectedBuildings;
        private string message1 = "Select a Building Group.";
        protected GBuildingGroupAND _selectedBuildingGroupAND = null;
        protected GBuildingAND _selectedBuildingAND;
        // noraini ali - OKT 2020
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        int _workAreaId = 0;
        #endregion

        public AssociateBuildingByDistanceTool()
        {
            _name = "Associate Building By Distance";
            InitMenu();
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public AssociateBuildingByDistanceTool(GBuildingGroup bdg)
        {
            _name = "Associate Building By Distance";
            InitMenu();
            _selectedBuildingGroup = bdg;
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if(_selectedBuildingGroup == null)
            {
                OnReported(message1);
                OnStepReported(message1);
            }
            else
            {
                RepositoryFactory repo = new RepositoryFactory(SegmentName);
                _selectedBuildings = ChooseMultipleBuilding(_selectedBuildingGroup);

                using (new WaitCursor())
                {

                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                    confirmBox.SetCaption("Associate Building Group")
                              .SetText("Are you sure you want to associate selected buildings ?");

                    if (confirmBox.Show() == DialogResult.Yes)
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction(() =>
                            {
                                foreach (IGFeature feature in _selectedBuildings)
                                {
                                    GBuilding building = (GBuilding)feature;
                                    building.GroupId = _selectedBuildingGroup.OID;
                                    repo.Update(building);

                                    // update num unit
                                    AutoUpdateNumUnit obj = new AutoUpdateNumUnit();
                                    obj.UpdateTotalNumUnit(repo, _selectedBuildingGroup.OID);
                                }
                            });
                            OnReported(message1);
                            OnStepReported(message1);
                            Reset();
                        }
                    }
                    else
                    {
                        _selectedBuildings = null;
                        Reset();
                    }
                }

                return;
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }

                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                List<GBuildingGroup> buildingGroups;
                                List<GBuildingGroupAND> buildingGroupsAND;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    buildingGroups = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    buildingGroupsAND = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                    {
                                        return;
                                    }

                                    // noraini ali - Sept 2020 - Create AND Building to Associate to Building Group
                                    if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id & Checking point in Working Area & Owner
                                        ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                                        andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                                        // filter street ADM & AND in this working area only

                                        if (buildingGroups.Count > 0)
                                        {
                                            buildingGroups = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroups, _workAreaId);
                                        }

                                        if (buildingGroupsAND.Count > 0)
                                        {
                                            buildingGroupsAND = andBuildingGroup.FilterBuildingGroupInWorkArea(buildingGroupsAND, _workAreaId);
                                        }

                                        if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                        {
                                            return;
                                        }

                                        //Assign filter feature in new list
                                        List<GBuildingGroup> _buildingGroups = new List<GBuildingGroup>();
                                        foreach (GBuildingGroup buildingGrpTemp in buildingGroups)
                                        {
                                            _buildingGroups.Add(buildingGrpTemp);
                                        }

                                        foreach (GBuildingGroupAND buildingGrpTempAND in buildingGroupsAND)
                                        {
                                            GBuildingGroup BuildingGrp = repo.GetById<GBuildingGroup>(buildingGrpTempAND.OriId);
                                            if (BuildingGrp != null)
                                                _buildingGroups.Add(BuildingGrp);
                                        }

                                        _selectedBuildingGroup = _buildingGroups[0];
                                        highlightBuildingGroup(repo, _selectedBuildingGroup.OID, true, _selectedFeaturePoints);
                                        _selectedBuildings = ChooseMultipleBuilding(_selectedBuildingGroup);

                                        using (new WaitCursor())
                                        {
                                            QuestionMessageBox confirmBox = new QuestionMessageBox();
                                            confirmBox.SetCaption("Associate Building Group")
                                                      .SetText("Are you sure you want to associate selected buildings ?");

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                using (new WaitCursor())
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        foreach (IGFeature feature in _selectedBuildings)
                                                        {
                                                            string strtableName = feature.TableName.ToString();
                                                            bool b1 = strtableName.Contains("AND");
                                                            if (b1)
                                                            {
                                                                GBuildingAND buildingAND = (GBuildingAND)feature;
                                                                buildingAND.GroupId = _selectedBuildingGroup.OID;
                                                                List<GMultiStorey> storey = buildingAND.GetMultiStories().ToList();
                                                                buildingAND.Unit = storey.Count;
                                                                repo.Update(buildingAND);

                                                            }
                                                            else
                                                            {
                                                                GBuilding building = (GBuilding)feature;
                                                                List<GMultiStorey> storey = building.GetMultiStories().ToList();
                                                                // create AND Building
                                                                ANDBuildingTools andBuilding = new ANDBuildingTools();
                                                                //andBuilding.AddBuildingAND(repo, building);
                                                                andBuilding.CreateBuildingAND(repo, building);

                                                                // Update status & floor num unit - ADM building
                                                                building.Unit = storey.Count;
                                                                repo.UpdateByAND(building, true);

                                                                GBuildingAND buildingAnd = building.GetBuildingANDId();
                                                                andBuilding.UpdateModifiedByUserAND(buildingAnd);
                                                                buildingAnd.GroupId = _selectedBuildingGroup.OID;
                                                                buildingAnd.Unit = storey.Count;
                                                                repo.Update(buildingAnd);
                                                            }
                                                        }

                                                        ANDBuildingTools ANDBuilding = new ANDBuildingTools();
                                                        ANDBuilding.AssociateByDistance(_selectedBuildingGroup);

                                                    });
                                                    OnReported(message1);
                                                    OnStepReported(message1);
                                                    Reset();
                                                }
                                            }
                                            else
                                            {
                                                _selectedBuildings = null;
                                                Reset();
                                            }
                                        }
                                    }
                                    else   // OTHERS USER
                                    {
                                        //// checking user non-AND cannot touch feature in Working Area
                                        //ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                        //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                                        //{
                                        //    return;
                                        //}

                                        _selectedBuildingGroup = buildingGroups[0];
                                        highlightBuildingGroup(repo, _selectedBuildingGroup.OID, true, _selectedFeaturePoints);
                                        _selectedBuildings = ChooseMultipleBuilding(_selectedBuildingGroup);

                                        foreach (IGFeature feature in _selectedBuildings)
                                        {
                                            GBuilding building = (GBuilding)feature;
                                            ANDBuildingTools andBuilding = new ANDBuildingTools();

                                            // noraini ali - Mei 2021 - check feature lock by AND
                                            andBuilding.CheckFeatureLock(building.AreaId,SegmentName);

                                            // noraini ali - Mei 2021 - check feature on verification
                                            andBuilding.CheckFeatureOnVerification(building);
                                        }

                                        using (new WaitCursor())
                                        {
                                            QuestionMessageBox confirmBox = new QuestionMessageBox();
                                            confirmBox.SetCaption("Associate Building Group")
                                                      .SetText("Are you sure you want to associate selected buildings ?");

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                using (new WaitCursor())
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        foreach (IGFeature feature in _selectedBuildings)
                                                        {
                                                            GBuilding building = (GBuilding)feature;
                                                            List<GMultiStorey> storey = building.GetMultiStories().ToList();
                                                            building.GroupId = _selectedBuildingGroup.OID;
                                                            building.Unit = storey.Count;
                                                            repo.Update(building);

                                                            // update num unit 
                                                            AutoUpdateNumUnit obj = new AutoUpdateNumUnit();
                                                            obj.UpdateTotalNumUnit(repo, _selectedBuildingGroup.OID);                                                      }
                                                    });
                                                    OnReported(message1);
                                                    OnStepReported(message1);
                                                    Reset();
                                                }
                                            }
                                            else
                                            {
                                                _selectedBuildings = null;
                                                Reset();
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported(message1);
            OnStepReported(message1);
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedBuilding = null;
            _selectedBuildingGroup = null;
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
        }
    }
}

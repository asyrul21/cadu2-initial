﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using GMultiStorey = Geomatic.Core.Rows.GMultiStorey;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public partial class AutoUpdateNumUnit
    {
        public void UpdateTotalNumUnit(RepositoryFactory repo, int buildGrpId)
        {
            int buildGrpTotalUnit = 0;

            GBuildingGroup _buildingGroup = repo.GetById<GBuildingGroup>(buildGrpId);

            // get all building associate to building group Id
            foreach (GBuilding building in _buildingGroup.GetBuildings())
            {
                // update total unit storey for each building
                List<GMultiStorey> storey = building.GetMultiStories().ToList();
                int buildUnit = storey.Count;
                int intUnit = 0;

                // no need to update data because value 0 and empty is same
                if (buildUnit == 0 && string.IsNullOrEmpty(building.Unit.ToString()))
                {
                    break;
                }

                if (!string.IsNullOrEmpty(building.Unit.ToString()))
                {
                    intUnit = building.Unit.Value;
                }

                if (buildUnit != intUnit)
                {
                    building.Unit = buildUnit;
                    repo.Update(building, false);
                }
                buildGrpTotalUnit += buildUnit;
            }

            // update building group - num unit
            _buildingGroup.NumUnit = buildGrpTotalUnit;
            repo.Update(_buildingGroup);
        }
    }
}

﻿using Earthworm.AO;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.MapTool.StreetTools;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public class AddBuildingGroupTool : AddTool
    {
        #region Fields

        protected FeedBackCollection _area;
        protected IRgbColor _color3 = ColorUtils.Get(Color.Red);
        private const int RADIUS1 = 25;
        private const int RADIUS2 = 50;
        private const int RADIUS3 = 100;
        private const string Message1 = "Click on map location to add building group.";
        private const string Message2 = "Select a street from the list for building group association.";

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        #endregion

        public AddBuildingGroupTool()
        {
            _name = "Add Building Group";
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            if (_area == null)
            {
                _area = new FeedBackCollection();
            }

            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (!_area.IsStarted)
            {
                MovePolygon movePolygon1 = new MovePolygon(ScreenDisplay, _color1, _color1, 1);
                movePolygon1.Polygon = (IPolygon)point.Buffer(RADIUS1);

                _area.Add(movePolygon1);

                MovePolygon movePolygon2 = new MovePolygon(ScreenDisplay, _color2, _color2, 1);
                movePolygon2.Polygon = (IPolygon)point.Buffer(RADIUS2);

                _area.Add(movePolygon2);

                MovePolygon movePolygon3 = new MovePolygon(ScreenDisplay, _color3, _color3, 1);
                movePolygon3.Polygon = (IPolygon)point.Buffer(RADIUS3);

                _area.Add(movePolygon3);

                _area.Start(point);
            }

            _area.MoveTo(point);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                InProgress = true;
                try
                {
                    CreateBuildingGroup(point);
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                InProgress = false;
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        protected virtual void CreateBuildingGroup(IPoint point)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GStreet> streets;
            GStreet street = null;

            List<GStreetAND> streetsAND;
            GStreetAND streetAND = null;

            using (new WaitCursor())
            {
                streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
               streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
            }

            if (streets.Count == 0)
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }
            if (streets.Count == 0)
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }

            //noraini ali- April 2020- cadu 2 User AND
            if (Session.User.GetGroup().Name == "AND")
            {
                // get Work Area Id & Checking point in Working Area & Owner
                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                int _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                // filter street ADM & AND in this working area only               
                List<GStreet> _NewListStreets = new List<GStreet>();
                List<GStreetAND> _NewListStreetsAND = new List<GStreetAND>();

                if (streets.Count > 0)
                {
                    _NewListStreets = andBuildingGroup.FilterStreetInWorkArea(streets, _workAreaId);
                }

                if (streetsAND.Count > 0)
                {
                    _NewListStreetsAND = andBuildingGroup.FilterStreetInWorkArea(streetsAND, _workAreaId);
                }

                // create new list to store all ADM feature to use in ChooseForm
                List<GStreet> _streets = new List<GStreet>();
               
                foreach (GStreet streetTemp in _NewListStreets)
                {
                    _streets.Add(streetTemp);
                }

                foreach (GStreetAND streetTempAND in _NewListStreetsAND)
                {
                    GStreet Street = repo.GetById<GStreet>(streetTempAND.OriId);
                    if (Street != null)
                        _streets.Add(Street);
                }

                streetAND = null;
                if (_streets.Count == 0)
                {
                    throw new Exception("No streets found within radius in this working area");
                }
                else if (_streets.Count == 1)
                {
                    street = _streets[0];
                    streetAND = null;

                    GStreetAND StreetTempAnd = street.GetStreetANDId();
                    if (StreetTempAnd != null)
                    {
                        streetAND = StreetTempAnd;
                        street = null;
                    }
                }
                else // _streets.count > 1
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(_streets, GeometryUtils.GetNearestFeature(_streets, point));

                    // if OID exist in AND feature
                    GStreetAND StreetTempAND = repo.GetById<GStreetAND>(street.OID);
                    if (StreetTempAND != null)
                    {
                        streetAND = StreetTempAND;
                        street = null;
                    }
                }

                // noraini ali - OKT 2020
                // Hightlight Feature Street
                if (streetAND != null)
                {
                    highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                }
                else
                {
                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                }
                    
            }
            else   // OTHERS USER
            {
                // checking user non-AND cannot touch feature in Working Area
                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                andBuildingGroup.checkingPointInWorkArea(SegmentName, point);
                //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                //{
                //    return;
                //}

                if (streets.Count == 1)
                {
                    street = streets[0];
                }
                else if (streets.Count > 1)
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(streets, GeometryUtils.GetNearestFeature(streets, point));
                }
                else if (streets.Count == 0)
                {
                    throw new Exception("No streets found within radius");
                }

                highlightStreet(repo, street.OID, true, _selectedFeatureLines);
            }

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }

                GBuildingGroup newBuildingGroup = repo.NewObj<GBuildingGroup>();
                newBuildingGroup.Init();

                //noraini ali - Okt 2020
                if (streetAND != null)
                {
                    newBuildingGroup.StreetId = streetAND.OriId;
                }
                else
                {
                    newBuildingGroup.StreetId = street.OID;
                }
                newBuildingGroup.Shape = point;

                //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                if (Session.User.GetGroup().Name == "AND")
                {
                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                    andBuildingGroup.UpdateCreatedByUserAND(newBuildingGroup);
                    andBuildingGroup.UpdateModifiedByUserAND(newBuildingGroup);
                }

                newBuildingGroup = repo.Insert(newBuildingGroup, false);

                using (AddBuildingGroupForm form = new AddBuildingGroupForm(newBuildingGroup))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        //repo.Update(newBuildingGroup);
                        repo.UpdateRemainStatus(newBuildingGroup, true);

                        //noraini ali- April 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            // create AND Building Group
                            ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                            andBuildingGroup.AddBuildingGroupAND(repo, newBuildingGroup);
                        }
                        // end
                        highlightBuildingGroup(repo, newBuildingGroup.OID, true, _selectedFeaturePoints);

                        OnReported("New building group created. {0}", newBuildingGroup.OID);
                        OnReported(Message1);
                        OnStepReported(Message1);
                        // noraini ali - OKT 2020 
                        // Clear Selected Feature
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        public override bool Deactivate()
        {
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.UI.Utilities;
using System.Windows.Forms;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.BuildingGroupTools
{
    public class MoveBuildingGroupTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GBuildingGroup _selectedBuildingGroup;
        protected MovePoint _selectedBuildingGroupPoint;
        protected bool bool_confirmMove = true;

        protected GBuildingGroupAND _selectedBuildingGroupAND;

        private const string Message1 = "Select a building group to move.";
        private const string Message2 = "Drag selected building group to move.";

        #endregion

        public MoveBuildingGroupTool()
        {
            _name = "Move Building Group";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedBuildingGroupPoint == null)
            {
                _selectedBuildingGroupPoint = new MovePoint(ScreenDisplay, _colorRed);
            }
            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                            {
                                if (_selectedBuildingGroup != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuildingGroup.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                                if (_selectedBuildingGroupAND != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuildingGroupAND.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                            }
                            else
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuildingGroup.Point))
                                {
                                    _progress = Progress.Moving;
                                }
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedBuildingGroupPoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingGroup> buildingGroups = SelectBuildingGroup(repo, point);
                                List<GBuildingGroupAND> buildingGroupsAND = SelectBuildingGroupAND(repo, point);

                                if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                {
                                    break;
                                }

                                // added by noraini ali - Mei 2020
                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id & Checking point in Working Area & Owner
                                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                    andBuildingGroup.CheckWithinWorkArea(SegmentName, point);
                                    andBuildingGroup.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaId = andBuildingGroup.GetWorkAreaId(SegmentName, point);

                                    if (buildingGroups.Count > 0 && buildingGroupsAND.Count == 0)
                                    {
                                        GBuildingGroup buildingGroup = buildingGroups[0];

                                        _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                        _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                        _selectedBuildingGroup = buildingGroup;

                                        GStreet street = buildingGroup.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                    else if (buildingGroups.Count == 0 && buildingGroupsAND.Count > 0)
                                    {
                                        GBuildingGroupAND buildingGroupAND = buildingGroupsAND[0];

                                        _selectedBuildingGroupPoint.Point = buildingGroupAND.Point;
                                        _selectedBuildingGroupPoint.Start(buildingGroupAND.Point);
                                        _selectedBuildingGroupAND = buildingGroupAND;

                                        GStreet street = buildingGroupAND.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                    else if (buildingGroups.Count > 0 && buildingGroupsAND.Count > 0)
                                    {
                                        GBuildingGroup buildingGroup = buildingGroups[0];
                                        GBuildingGroupAND buildingGroupAND = buildingGroupsAND[0];

                                        if (buildingGroup.OID == buildingGroupAND.OriId)
                                        {
                                       
                                            _selectedBuildingGroupPoint.Point = buildingGroupAND.Point;
                                            _selectedBuildingGroupPoint.Start(buildingGroupAND.Point);
                                            _selectedBuildingGroupAND = buildingGroupAND;

                                            GStreet street = buildingGroupAND.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Start(point);
                                            }
                                        }
                                    }
                                }
                                // end added
                                else //OTHERS USER
                                {
                                    //// checking user non-AND cannot touch feature in Working Area
                                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                    //if (!andBuildingGroup.checkingPointInWorkArea(SegmentName, point))
                                    //{
                                    //    return;
                                    //}

                                    if (buildingGroups.Count > 0)
                                    {
                                        GBuildingGroup buildingGroup = buildingGroups[0];
                                        _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                        _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                        _selectedBuildingGroup = buildingGroup;

                                        // noraini ali - Mei 2021 - check feature lock by AND
                                        andBuildingGroup.CheckFeatureLock(_selectedBuildingGroup.AreaId,SegmentName);

                                        // noraini ali - Mei 2021 - check feature on verification
                                        andBuildingGroup.CheckFeatureOnVerification(_selectedBuildingGroup);

                                        // noraini ali - OKT 2020
                                        if (Core.Sessions.Session.User.GetGroup().Name == "MLI_UPDATE")
                                        {
                                            if (buildingGroup.IsNavigationReady)
                                            {
                                                throw new QualityControlException("Building Group Navi is ready, cannot move by group user MLI");
                                            }
                                        }
                                        // end add

                                        GStreet street = buildingGroup.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                }

                                _progress = Progress.TryMove;
                                OnReported(Message2);
                                OnStepReported(Message2);
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;

                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingGroup> buildingGroups = SelectBuildingGroup(repo, point);
                                List<GBuildingGroupAND> buildingGroupsAND = SelectBuildingGroupAND(repo, point);

                                if (buildingGroups.Count == 0 && buildingGroupsAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedBuildingGroupPoint.Stop();
                                    _selectedBuildingGroup = null;
                                    _selectedBuildingGroupAND = null;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();

                                    if (buildingGroups.Count > 0 && buildingGroupsAND.Count == 0)
                                    {
                                        GBuildingGroup buildingGroup = buildingGroups[0];

                                        if (!_selectedBuildingGroup.Equals(buildingGroup))
                                        {
                                            _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                            _selectedBuildingGroupPoint.Stop();
                                            _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                            _selectedBuildingGroup = buildingGroup;

                                            GStreet street = buildingGroup.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (buildingGroups.Count == 0 && buildingGroupsAND.Count > 0)
                                    {
                                        GBuildingGroupAND buildingGroupAND = buildingGroupsAND[0];
                                        if (!_selectedBuildingGroupAND.Equals(buildingGroupAND))
                                        {
                                            _selectedBuildingGroupPoint.Point = buildingGroupAND.Point;
                                            _selectedBuildingGroupPoint.Stop();
                                            _selectedBuildingGroupPoint.Start(buildingGroupAND.Point);
                                            _selectedBuildingGroupAND = buildingGroupAND;

                                            GStreet street = buildingGroupAND.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (buildingGroups.Count > 0 && buildingGroupsAND.Count > 0)
                                    {
                                        GBuildingGroup buildingGroup = buildingGroups[0];
                                        GBuildingGroupAND buildingGroupAND = buildingGroupsAND[0];

                                        if (buildingGroup.OID == buildingGroupAND.OriId)
                                        {
                                            if (!_selectedBuildingGroupAND.Equals(buildingGroupAND))
                                            {
                                                _selectedBuildingGroupPoint.Point = buildingGroupAND.Point;
                                                _selectedBuildingGroupPoint.Stop();
                                                _selectedBuildingGroupPoint.Start(buildingGroupAND.Point);
                                                _selectedBuildingGroupAND = buildingGroupAND;

                                                GStreet street = buildingGroupAND.GetStreet();
                                                if (street != null)
                                                {
                                                    _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                    _parentRadius.Stop();
                                                    _parentRadius.Start(point);
                                                }
                                                else
                                                {
                                                    _parentRadius.Stop();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (buildingGroups.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _parentRadius.Stop();
                                        _selectedBuildingGroupPoint.Stop();
                                        _selectedBuildingGroup = null;
                                        _selectedBuildingGroupAND = null;
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GBuildingGroup buildingGroup = buildingGroups[0];
                                    if (!_selectedBuildingGroup.Equals(buildingGroup))
                                    {
                                        _selectedBuildingGroupPoint.Point = buildingGroup.Point;
                                        _selectedBuildingGroupPoint.Stop();
                                        _selectedBuildingGroupPoint.Start(buildingGroup.Point);
                                        _selectedBuildingGroup = buildingGroup;

                                        GStreet street = buildingGroup.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Stop();
                                            _parentRadius.Start(point);
                                        }
                                        else
                                        {
                                            _parentRadius.Stop();
                                        }
                                        break;
                                    }
                                }
                               
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }

                            IPoint newPoint;
                            GStreet street;
                            List<GStreet> streets;
                            try
                            {
                                using (new WaitCursor())
                                {
                                    //newPoint = (IPoint)_selectedBuildingGroupPoint.Stop();
                                    if (Core.Sessions.Session.User.GetGroup().Name != "AND")
                                    {
                                        street = _selectedBuildingGroup.GetStreet();
                                    }
                                    else
                                    {
                                        if (_selectedBuildingGroup == null)
                                        {
                                            GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(_selectedBuildingGroupAND.OriId);
                                            street = BuildingGroup.GetStreet();
                                        }
                                        else
                                        {
                                            street = _selectedBuildingGroup.GetStreet();
                                        }
                                    }
                                }

                                // noraini ali - Sept 2020-Confirmation to move feature
                                // process move the original feature (ADM)
                                // confirm to move so need to update ADM feature to previous location
                                // and create AND at new location

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    bool_confirmMove = true;

                                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                                    confirmBox.SetCaption("Move Building Group")
                                        .SetButtons(MessageBoxButtons.OKCancel)
                                        .SetText("Confirm Move Building Group to New Location ?");

                                    if (confirmBox.Show() == DialogResult.Cancel)
                                    {
                                        bool_confirmMove = false;
                                    }
                                }

                                // noraini ali - NOV 2020
                                newPoint = (IPoint)_selectedBuildingGroupPoint.Stop();

                                if (bool_confirmMove)
                                {
                                    if (street != null)
                                    {
                                        using (new WaitCursor())
                                        {
                                            double distance = newPoint.DistanceTo(street.Shape);
                                            streets = repo.SpatialSearch<GStreet>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                                        }
                                        if (streets.Count > 0)
                                        {
                                            GStreet nearestStreet = GeometryUtils.GetNearestFeature(streets, newPoint);
                                            if (!nearestStreet.Equals(street))
                                            {
                                                QuestionMessageBox chooseNewBox = new QuestionMessageBox();
                                                chooseNewBox.SetCaption("Move Building Group")
                                                            .SetText("Another street nearby. Choose new street?");

                                                if (chooseNewBox.Show() == DialogResult.Yes)
                                                {
                                                    GStreet newStreet = ChooseFeature(streets, nearestStreet);
                                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                                    {
                                                        // noraini ali - OKT 2020 - cater if choose AND Street
                                                        GStreetAND newStreetAND = repo.GetById<GStreetAND>(newStreet.OID);
                                                        if (newStreetAND != null)
                                                        {
                                                            if (_selectedBuildingGroupAND != null)
                                                            {
                                                                _selectedBuildingGroupAND.StreetId = newStreetAND.OriId;
                                                                repo.UpdateGraphicByAND(_selectedBuildingGroupAND, true);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_selectedBuildingGroupAND != null)
                                                            {
                                                                _selectedBuildingGroupAND.StreetId = newStreet.OID;
                                                                repo.UpdateGraphicByAND(_selectedBuildingGroupAND, true);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _selectedBuildingGroup.StreetId = newStreet.OID;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    using (new WaitCursor())
                                    {
                                        repo.StartTransaction(() =>
                                        {
                                        // added by noraini ali - apr 2020
                                        if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                            {
                                                ANDBuildingGroupTools andBuildingGroup = new ANDBuildingGroupTools();
                                                if (_selectedBuildingGroupAND != null)
                                                {
                                                    GBuildingGroup BuildingGroup = repo.GetById<GBuildingGroup>(_selectedBuildingGroupAND.OriId);
                                                    andBuildingGroup.Move(repo, BuildingGroup, newPoint);
                                                }
                                                else
                                                {
                                                    andBuildingGroup.Move(repo, _selectedBuildingGroup, newPoint);
                                                }
                                            }
                                        // end
                                        else
                                            {
                                                _selectedBuildingGroup.Shape = newPoint;
                                                repo.UpdateGraphic(_selectedBuildingGroup, true);
                                            }
                                        });
                                    }
                                }
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedBuildingGroup = null;
                                _selectedBuildingGroupAND = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GBuildingGroup> SelectBuildingGroup(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingGroup> query = new Query<GBuildingGroup>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        // added by noraini ali - Mei 2020
        private List<GBuildingGroupAND> SelectBuildingGroupAND(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingGroupAND> query = new Query<GBuildingGroupAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }
        // end

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Stop();
                _selectedBuildingGroupPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedBuildingGroupPoint != null)
            {
                _selectedBuildingGroupPoint.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool
{
    public partial class SelectTool
    {
        class Command
        {
            public const string SelectMethod = "SelectMethod";
            //public const string Polygon = "Polygon";
            //public const string Rectangle = "Rectangle";
            public const string Polygon = "Fence";
            public const string Rectangle = "Single";
            public const string Circle = "Circle";
            public const string FeatureOption = "FeatureOption";
            public const string Street = "Street";
            public const string Junction = "Junction";
            public const string Property = "Property";
            public const string Building = "Building";
            public const string BuildingGroup = "BuildingGroup";
            public const string Landmark = "Landmark";
            public const string Poi = "Poi";
            //added by asyrul
            public const string Region = "Region";
            //added by noraini ali - CADU2 AND
            public const string WorkArea = "WorkArea";
            public const string PropertyAND = "PropertyAND";
            public const string LandmarkAND = "LandmarkAND";
            public const string BuildingAND = "BuildingAND";
            public const string BuildingGroupAND = "BuildingGroupAND";
            public const string StreetAND = "StreetAND";
            public const string JunctionAND = "JunctionAND";
        }
    }
}

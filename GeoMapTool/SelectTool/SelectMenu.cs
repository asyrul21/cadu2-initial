﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool
{
    public partial class SelectTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuRectangle;
        private ToolStripMenuItem menuPolygon;
        private ToolStripMenuItem menuCircle;
        private ToolStripSeparator separator1;
        private ToolStripMenuItem menuStreet;
        private ToolStripMenuItem menuJunction;
        private ToolStripMenuItem menuProperty;
        private ToolStripMenuItem menuBuilding;
        private ToolStripMenuItem menuBuildingGroup;
        private ToolStripMenuItem menuLandmark;
        private ToolStripMenuItem menuPoi;

        //added by asyrul
        private ToolStripMenuItem menuRegion;

        // added by noraini - Jan 2020 - AND Feature
        private ToolStripSeparator separator2;
        private ToolStripMenuItem menuWorkArea;   
        private ToolStripMenuItem menuPropertyAND;
        private ToolStripMenuItem menuLandmarkAND;
        private ToolStripMenuItem menuStreetAND;
        private ToolStripMenuItem menuJunctionAND;
        private ToolStripMenuItem menuBuildingAND;
        private ToolStripMenuItem menuBuildingGroupAND;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuRectangle = new ToolStripMenuItem();
            this.menuPolygon = new ToolStripMenuItem();
            this.menuCircle = new ToolStripMenuItem();
            this.separator1 = new ToolStripSeparator();
            this.menuStreet = new ToolStripMenuItem();
            this.menuJunction = new ToolStripMenuItem();
            this.menuProperty = new ToolStripMenuItem();
            this.menuBuilding = new ToolStripMenuItem();
            this.menuBuildingGroup = new ToolStripMenuItem();
            this.menuLandmark = new ToolStripMenuItem();
            this.menuPoi = new ToolStripMenuItem();
            //added
            this.menuRegion = new ToolStripMenuItem();
            // added by noraini ali - CADU2 AND
            this.separator2 = new ToolStripSeparator();
            this.menuWorkArea = new ToolStripMenuItem();
            this.menuStreetAND = new ToolStripMenuItem();
            this.menuJunctionAND = new ToolStripMenuItem();
            this.menuPropertyAND = new ToolStripMenuItem();
            this.menuBuildingAND = new ToolStripMenuItem();
            this.menuBuildingGroupAND = new ToolStripMenuItem();
            this.menuLandmarkAND = new ToolStripMenuItem();
            // 
            // menuRectangle
            //
            this.menuRectangle.Name = "menuRectangle";
            //this.menuRectangle.Text = "Rectangle";
            this.menuRectangle.Text = "Single";
            this.menuRectangle.Click += new EventHandler(this.OnRectangle);
            // 
            // menuPolygon
            //
            this.menuPolygon.Name = "menuPolygon";
            //this.menuPolygon.Text = "Polygon";
            this.menuPolygon.Text = "Fence";
            this.menuPolygon.Click += new EventHandler(this.OnPolygon);
            // 
            // menuCircle
            //
            this.menuCircle.Name = "menuCircle";
            this.menuCircle.Text = "Circle";
            this.menuCircle.Click += new EventHandler(this.OnCircle);
            // 
            // menuStreet
            //
            this.menuStreet.Name = "menuStreet";
            this.menuStreet.Text = "Street";
            this.menuStreet.Click += new EventHandler(this.OnStreet);
            // 
            // menuJunction
            //
            this.menuJunction.Name = "menuJunction";
            this.menuJunction.Text = "Junction";
            this.menuJunction.Click += new EventHandler(this.OnJunction);
            // 
            // menuProperty
            //
            this.menuProperty.Name = "menuProperty";
            this.menuProperty.Text = "Property";
            this.menuProperty.Click += new EventHandler(this.OnProperty);
            // 
            // menuBuilding
            //
            this.menuBuilding.Name = "menuBuilding";
            this.menuBuilding.Text = "Building";
            this.menuBuilding.Click += new EventHandler(this.OnBuilding);
            // 
            // menuBuildingGroup
            //
            this.menuBuildingGroup.Name = "menuBuildingGroup";
            this.menuBuildingGroup.Text = "BuildingGroup";
            this.menuBuildingGroup.Click += new EventHandler(this.OnBuildingGroup);
            // 
            // menuLandmark
            //
            this.menuLandmark.Name = "menuLandmark";
            this.menuLandmark.Text = "Landmark";
            this.menuLandmark.Click += new EventHandler(this.OnLandmark);

            //added by asyrul
            // 
            // menuRegion
            //
            this.menuRegion.Name = "menuRegion";
            this.menuRegion.Text = "Region";
            this.menuRegion.Click += new EventHandler(this.OnRegion);
            //added end

            //added by noraini - Jan 2020 - CADU2 AND
            // 
            // menuWorkArea
            this.menuWorkArea.Name = "menuWorkArea";
            this.menuWorkArea.Text = "WorkArea";
            this.menuWorkArea.Click += new EventHandler(this.OnWorkArea);
            //
            // menuStreetAND
            this.menuStreetAND.Name = "menuStreetAND";
            this.menuStreetAND.Text = "StreetAND";
            this.menuStreetAND.Click += new EventHandler(this.OnStreetAND);
            //
            // menuJunctionAND
            this.menuJunctionAND.Name = "menuJunctionAND";
            this.menuJunctionAND.Text = "JunctionAND";
            this.menuJunctionAND.Click += new EventHandler(this.OnJunctionAND);
            //
            // menuPropertyAND
            this.menuPropertyAND.Name = "menuPropertyAND";
            this.menuPropertyAND.Text = "PropertyAND";
            this.menuPropertyAND.Click += new EventHandler(this.OnPropertyAND);
            //
            // menuBuildingAND
            this.menuBuildingAND.Name = "menuBuildingAND";
            this.menuBuildingAND.Text = "BuildingAND";
            this.menuBuildingAND.Click += new EventHandler(this.OnBuildingAND);
            //
            // menuBuildingGroupAND
            this.menuBuildingGroupAND.Name = "menuBuildingGroupAND";
            this.menuBuildingGroupAND.Text = "BuildingGroupAND";
            this.menuBuildingGroupAND.Click += new EventHandler(this.OnBuildingGroupAND);
            //
            // menuLandmarkAND
            this.menuLandmarkAND.Name = "menuLandmarkAND";
            this.menuLandmarkAND.Text = "LandmarkAND";
            this.menuLandmarkAND.Click += new EventHandler(this.OnLandmarkAND);
            //added end
            // 
            // menuPolygon
            //
            this.menuPoi.Name = "menuPoi";
            this.menuPoi.Text = "Poi";
            this.menuPoi.Click += new EventHandler(this.OnPoi);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] {
            this.menuRectangle,
            this.menuPolygon,
            this.menuCircle,
            this.separator1,
            this.menuStreet,
            this.menuJunction,
            this.menuProperty,
            this.menuBuilding,
            this.menuBuildingGroup,
            this.menuLandmark,
            this.menuPoi,
            this.menuRegion,
            this.separator2,
            this.menuWorkArea,
            this.menuStreetAND,
            this.menuJunctionAND,
            this.menuPropertyAND,
            this.menuBuildingAND,
            this.menuBuildingGroupAND,
            this.menuLandmarkAND});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features.JoinedFeatures;
using IGFeature = Geomatic.Core.Features.IGFeature;

namespace Geomatic.MapTool
{
    public partial class SelectTool : Tool
    {
        private CommandPool _commandPool;
        private string _selectMethod;
        private string _featureOption;

        // noraini ali
        List<IGFeature> FilteredData;
        protected new const string Message = "Select feature to display.Right click for menu.";
        // end

        public SelectTool()
        {
            _name = "Select";
            InitMenu();
            InitCommand();
            OnRectangle(null, null);
            OnStreet(null, null);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message);
            OnStepReported(Message);
        }

        private void InitCommand()
        {
            _commandPool = new CommandPool();

            _commandPool.Register(Command.Rectangle, menuRectangle);
            _commandPool.Register(Command.Polygon, menuPolygon);
            _commandPool.Register(Command.Circle, menuCircle);

            _commandPool.Register(Command.Street, menuStreet);
            _commandPool.Register(Command.Junction, menuJunction);
            _commandPool.Register(Command.Property, menuProperty);
            _commandPool.Register(Command.Building, menuBuilding);
            _commandPool.Register(Command.BuildingGroup, menuBuildingGroup);
            _commandPool.Register(Command.Landmark, menuLandmark);
            _commandPool.Register(Command.Poi, menuPoi);

            //added by asyrul
            _commandPool.Register(Command.Region, menuRegion);
            //added end

            // added by noraini CADU 2 AND Jan 2020
            _commandPool.Register(Command.WorkArea, menuWorkArea);
            _commandPool.Register(Command.StreetAND, menuStreetAND);
            _commandPool.Register(Command.JunctionAND, menuJunctionAND);
            _commandPool.Register(Command.PropertyAND, menuPropertyAND);
            _commandPool.Register(Command.BuildingAND, menuBuildingAND);
            _commandPool.Register(Command.BuildingGroupAND, menuBuildingGroupAND);
            _commandPool.Register(Command.LandmarkAND, menuLandmarkAND);

            _commandPool.RegisterRadio(Command.SelectMethod,
                                                Command.Rectangle,
                                                Command.Polygon,
                                                Command.Circle);

            _commandPool.RegisterRadio(Command.FeatureOption,
                                                Command.Street,
                                                Command.Junction,
                                                Command.Property,
                                                Command.Building,
                                                Command.BuildingGroup,
                                                Command.Landmark,
                                                Command.Poi,
                                                Command.Region,
                                                Command.WorkArea,
                                                Command.StreetAND,
                                                Command.JunctionAND,
                                                Command.PropertyAND,
                                                Command.BuildingAND,
                                                Command.BuildingGroupAND,
                                                Command.LandmarkAND);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry;

                    switch (_selectMethod)
                    {
                        case Command.Rectangle:
                            {
                                geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                break;
                            }
                        case Command.Polygon:
                            {
                                geometry = new UI.Utilities.GeometryTrackers.PolygonTracker().TrackNew(ActiveView, point);
                                break;
                            }
                        case Command.Circle:
                            {
                                geometry = new CircleTracker().TrackNew(ActiveView, point);
                                break;
                            }
                        default:
                            throw new Exception("Unknown draw method.");
                    }

                    if (geometry == null)
                    {
                        return;
                    }

                    using (new WaitCursor())
                    {
                        IEnumerable<IGFeature> features;

                        switch (_featureOption)
                        {
                            case Command.Street:
                                features = repo.SpatialSearch<GStreet>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureStreet(features);
                                break;
                            case Command.Junction:
                                features = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureJunc(features);
                                break;
                            case Command.Property:
                                features = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureProp(features);                               
                                break;
                            case Command.Building:
                                features = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureBuild(features);
                                break;
                            case Command.BuildingGroup:
                                features = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureBuildGrp(features);
                                break;
                            case Command.Landmark:
                                features = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureLand(features);
                                break;
                            case Command.Poi:
                                features = repo.SpatialSearch<GPoi>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeature(features);
                                break;
                            case Command.Region:
                                //MessageBox.Show("Selecting region!");
                                //return;
                                features = repo.SpatialSearch<Geomatic.Core.Features.GRegion>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeature(features);
                                //features = repo.SpatialSearch<GRegion>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                break;
                            // added by noraini - Jan 2020 - CADU 2 AND
                            case Command.WorkArea:
                                features = repo.SpatialSearch<Core.Features.GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeature(features);
                                break;
                            case Command.PropertyAND:
                                features = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeaturePropAND(features);
                                break;
                            case Command.LandmarkAND:
                                features = repo.SpatialSearch<GLandmarkAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureLandAND(features);
                                break;
                            case Command.JunctionAND:
                                features = repo.SpatialSearch<GJunctionAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureJuncAND(features);
                                break;
                            case Command.StreetAND:
                                features = repo.SpatialSearch<GStreetAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureStreetAND(features);
                                break;
                            case Command.BuildingAND:
                                features = repo.SpatialSearch<GBuildingAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureBuildAND(features);
                                break;
                            case Command.BuildingGroupAND:
                                features = repo.SpatialSearch<GBuildingGroupAND>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects);
                                FilteredData = FilteredFeatureBuildGrpAND(features);
                                break; 
                            default:
                                throw new Exception("Unknown option.");
                        }

                        //HighlightFeatures(FilteredData);
                        System.Diagnostics.Debug.WriteLine(FilteredData);
                        MapDocument.SelectFeatures(FilteredData, true);
                        MapDocument.RefreshSelection();
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Right)
            {
                if (InProgress)
                {
                    return;
                }

                OnReported(Message);
                OnStepReported(Message);
                MapDocument.ShowContextMenu(contextMenu, x, y);
                //MapDocument.RefreshSelection();
            }
        }

        private void OnRectangle(object sender, EventArgs e)
        {
            _selectMethod = Command.Rectangle;
            _commandPool.Check(Command.Rectangle);
        }

        private void OnPolygon(object sender, EventArgs e)
        {
            _selectMethod = Command.Polygon;
            _commandPool.Check(Command.Polygon);
        }

        private void OnCircle(object sender, EventArgs e)
        {
            _selectMethod = Command.Circle;
            _commandPool.Check(Command.Circle);
        }

        private void OnStreet(object sender, EventArgs e)
        {
            _featureOption = Command.Street;
            _commandPool.Check(Command.Street);
        }

        private void OnJunction(object sender, EventArgs e)
        {
            _featureOption = Command.Junction;
            _commandPool.Check(Command.Junction);
        }

        private void OnProperty(object sender, EventArgs e)
        {
            _featureOption = Command.Property;
            _commandPool.Check(Command.Property);
        }

        private void OnBuilding(object sender, EventArgs e)
        {
            _featureOption = Command.Building;
            _commandPool.Check(Command.Building);
        }

        private void OnBuildingGroup(object sender, EventArgs e)
        {
            _featureOption = Command.BuildingGroup;
            _commandPool.Check(Command.BuildingGroup);
        }

        private void OnLandmark(object sender, EventArgs e)
        {
            _featureOption = Command.Landmark;
            _commandPool.Check(Command.Landmark);
        }

        private void OnPoi(object sender, EventArgs e)
        {
            _featureOption = Command.Poi;
            _commandPool.Check(Command.Poi);
        }

        //added by asyrul
        private void OnRegion(object sender, EventArgs e)
        {
            _featureOption = Command.Region;
            _commandPool.Check(Command.Region);
        }
        //added end

        //added by noraini - Jan 2020
        private void OnWorkArea(object sender, EventArgs e)
        {
            _featureOption = Command.WorkArea;
            _commandPool.Check(Command.WorkArea);
        }
        private void OnPropertyAND(object sender, EventArgs e)
        {
            _featureOption = Command.PropertyAND;
            _commandPool.Check(Command.PropertyAND);
        }

        private void OnLandmarkAND(object sender, EventArgs e)
        {
            _featureOption = Command.LandmarkAND;
            _commandPool.Check(Command.LandmarkAND);
        }

        private void OnStreetAND(object sender, EventArgs e)
        {
            _featureOption = Command.StreetAND;
            _commandPool.Check(Command.StreetAND);
        }

        private void OnJunctionAND(object sender, EventArgs e)
        {
            _featureOption = Command.JunctionAND;
            _commandPool.Check(Command.JunctionAND);
        }

        private void OnBuildingAND(object sender, EventArgs e)
        {
            _featureOption = Command.BuildingAND;
            _commandPool.Check(Command.BuildingAND);
        }

        private void OnBuildingGroupAND(object sender, EventArgs e)
        {
            _featureOption = Command.BuildingGroupAND;
            _commandPool.Check(Command.BuildingGroupAND);
        }
    }
}

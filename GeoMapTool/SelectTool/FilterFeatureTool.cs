﻿using System.Collections.Generic;
using System.Linq;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;

namespace Geomatic.MapTool
{
    // Implementation - noraini ali - OKT 2021
    // partial class to filter selected feature for user AND

    public partial class SelectTool
    {
        public List<IGFeature> FilteredFeatureProp(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GProperty feat = repo.GetById<GProperty>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureStreet(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GStreet feat = repo.GetById<GStreet>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureBuild(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GBuilding feat = repo.GetById<GBuilding>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureBuildGrp(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GBuildingGroup feat = repo.GetById<GBuildingGroup>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureLand(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GLandmark feat = repo.GetById<GLandmark>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureJunc(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GJunction feat = repo.GetById<GJunction>(feature.OID);
                    if (string.IsNullOrEmpty(feat.AndStatus.ToString()) || (feat.AndStatus == 0))
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }
        
        public List<IGFeature> FilteredFeaturePropAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GPropertyAND feat = repo.GetById<GPropertyAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureStreetAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GStreetAND feat = repo.GetById<GStreetAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureBuildAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GBuildingAND feat = repo.GetById<GBuildingAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureBuildGrpAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GBuildingGroupAND feat = repo.GetById<GBuildingGroupAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureLandAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GLandmarkAND feat = repo.GetById<GLandmarkAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        public List<IGFeature> FilteredFeatureJuncAND(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            if (!CheckGroupUserAND())
            {
                filteredFeatures = FilteredFeature(features);
            }
            else
            {
                foreach (IGFeature feature in features)
                {
                    GJunctionAND feat = repo.GetById<GJunctionAND>(feature.OID);
                    if (feat != null && feat.AndStatus != 2)
                    {
                        filteredFeatures.Add(feature);
                    }
                }
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }
        
        public List<IGFeature> FilteredFeature(IEnumerable<IGFeature> features)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            List<IGFeature> filteredFeatures = new List<IGFeature>();

            foreach (IGFeature feature in features)
            {
                filteredFeatures.Add(feature);
            }

            DisplayFeatureCount(filteredFeatures);
            return filteredFeatures;
        }

        private void DisplayFeatureCount(List<IGFeature> FilteredData)
        {
            OnReported(Message);
            OnStepReported(Message);

            int count = FilteredData.Count();
            if (count > 0)
            {
                OnReported(Message + " [ Total Selected " + _featureOption + " : " + count.ToString() + " ]");
                OnStepReported(Message + " [ Total Selected " + _featureOption + " : " + count.ToString() + " ]");
            }
        }

        private void HighlightFeatures(List<IGFeature> FilteredData)
        {
            foreach (IGFeature feature in FilteredData)
            {
                MapDocument.HighlightSelectionFeature(feature);
            }
        }

        private bool CheckGroupUserAND()
        {
            bool UserAND = false;
            if (Session.User.GetGroup().Name == "AND")
            {
                UserAND = true;
            }
            return UserAND;
        }
    }
}

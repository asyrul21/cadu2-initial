﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using System;
using System.Text;
using GMultiStorey = Geomatic.Core.Rows.GMultiStorey;
using Geomatic.MapTool.BuildingGroupTools;

namespace Geomatic.MapTool.BuildingTools
{
    // Implementation - noraini ali - Apr 2021- Apply STATUS & AND_STATUS - Data Intergration to NEPS

    public class ANDBuildingTools : ValidateWorkAreaAndStatus
    {
        StringBuilder sb = new StringBuilder();

        public void CreateBuildingAND(RepositoryFactory repo, GBuilding Building)
        {
            GBuilding building = repo.GetById<GBuilding>(Building.OID);
            GBuildingAND BuildingAND = repo.NewObj<GBuildingAND>();
            BuildingAND.CopyFrom(building);
            repo.Insert(BuildingAND, false);
        }

        public void AddBuildingAND(RepositoryFactory repo, GBuilding Building)
        {
            // update ADM - AreaId
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(Building.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => Building.AreaId = thearea.OID);
            repo.UpdateInsertByAND(Building, true);

            // create new AND as copy from ADM building 
            CreateBuildingAND(repo, Building);

            // update AND - 
            // CreatedBy, DateCreated, Updatedby & DateUpdated
            GBuildingAND BuildingAND = Building.GetBuildingANDId();
            UpdateCreatedByUserAND(BuildingAND);
            UpdateModifiedByUserAND(BuildingAND);
            repo.UpdateRemainStatus(BuildingAND, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GBuilding Building)
        {
            GBuildingAND absObjAND = Building.GetBuildingANDId();
            if (absObjAND == null)
            {
                // Create Building AND
                CreateBuildingAND(repo, Building);
            }

            // Update AND - User info,STATUS & AND_STATUS as Delete status
            GBuildingAND BuildingAND = Building.GetBuildingANDId();
            UpdateModifiedByUserAND(BuildingAND);
            repo.UpdateDeleteByAND(BuildingAND, true);

            // update ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(Building, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GBuildingAND BuildingAND)
        {
            // Update ADM feature - AND_STATUS & STATUS as Delete status
            GBuilding Building = repo.GetById<GBuilding>(BuildingAND.OriId);
            repo.UpdateDeleteByAND(Building, true);

            // Update AND table -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(BuildingAND);
            repo.UpdateDeleteByAND(BuildingAND, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GBuilding Building)
        {
            GBuildingAND absObjAND = Building.GetBuildingANDId();
            if (absObjAND == null)
            {
                // Create Building AND
                CreateBuildingAND(repo, Building);
            }

            //Get data from the AND Table
            GBuildingAND BuildingAND = Building.GetBuildingANDId();

            //Display form and update AND Table
            using (EditBuildingANDForm form = new EditBuildingANDForm(BuildingAND))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append(form.MessageButtonClick());
                sb.Append("\n");
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");

                using (new WaitCursor())
                {
                    form.SetValues();

                    // to update Area Id for ADM & AND Building
                    List<GWorkArea> area;

                    //Set other required data for AND Building Table (AreaId, OriId, status n andstatus)
                    area = repo.SpatialSearch<GWorkArea>(Building.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

                    // Update AND Feature - AreadId, Updatedby & DateUpdated
                    area.ForEach(thearea => BuildingAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(BuildingAND);
                    repo.Update(BuildingAND);

                    // noraini ali - Apr 2021 - Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => Building.AreaId = thearea.OID);

                    // noraini - Okt 2022 - enhancement building group 
                    // update ADM Building same as floor num unit in AND building
                    Building.Unit = BuildingAND.Unit;
                    repo.UpdateByAND(Building, true);

                    // to update num unit belong to building group Id
                    if (BuildingAND.HasGroup())
                    {
                        UpdateNumUnit(repo, BuildingAND.GroupId.Value);
                    }
                    // end

                    // update work area flag as open 
                    int AreaID = BuildingAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GBuildingAND BuildingAND)
        {
            GBuilding Building = repo.GetById<GBuilding>(BuildingAND.OriId);
            if (Building == null)
            {
                throw new QualityControlException("Cannot proceed, building ADM not found.");
            }

            //Display form and update AND Table
            using (EditBuildingANDForm form = new EditBuildingANDForm(BuildingAND))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append(form.MessageButtonClick());
                sb.Append("\n");
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(BuildingAND);
                    repo.Update(BuildingAND, true);

                    // update ADM - STATUS & AND_STATUS to update status
                    Building.Unit = BuildingAND.Unit;
                    repo.UpdateByAND(Building, true);

                    // noraini - Okt 2022 - enhancement building group 
                    // to update num unit belong to building group Id
                    if (BuildingAND.HasGroup())
                    {
                        UpdateNumUnit(repo, BuildingAND.GroupId.Value);
                    }
                    // end

                    // update work area flag as open 
                    int AreaID = BuildingAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GBuilding Building, IPoint newPoint)
        {
            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Building Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != Building.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // if AND building not exist so create AND Building 
            GBuildingAND absObjAND = Building.GetBuildingANDId();
            if (absObjAND == null)
            {
                // Create Building AND
                CreateBuildingAND(repo, Building);
            }

            // set new location for AND Building
            GBuildingAND BuildingAND = Building.GetBuildingANDId();
            BuildingAND.Shape = newPoint;
            UpdateModifiedByUserAND(BuildingAND);
            repo.UpdateGraphic(BuildingAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(Building, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GBuildingAND BuildingAND, IPoint newPoint)
        {
            GBuilding Building = repo.GetById<GBuilding>(BuildingAND.OriId);
            if (Building == null)
            {
                throw new QualityControlException("Cannot proceed, building ADM not found.");
            }

            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Building Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != BuildingAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // set new location for AND Building & STATUS & AND_STATUS (Update Graphic)
            BuildingAND.Shape = newPoint;
            UpdateModifiedByUserAND(BuildingAND);
            repo.UpdateGraphic(BuildingAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Building, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GBuilding Building, int PropertyId)
        {
            GBuildingAND absObjAND = Building.GetBuildingANDId();
            if (absObjAND == null)
            {
                // Create Building AND
                CreateBuildingAND(repo, Building);
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            GBuildingAND BuildingAND = Building.GetBuildingANDId();
            BuildingAND.PropertyId = PropertyId;
            UpdateModifiedByUserAND(BuildingAND);
            repo.Update(BuildingAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Building, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GBuildingAND BuildingAND, int PropertyId)
        {
            GBuilding Building = repo.GetById<GBuilding>(BuildingAND.OriId);
            if (Building == null)
            {
                throw new QualityControlException("Cannot proceed, building ADM not found.");
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            BuildingAND.PropertyId = PropertyId;
            UpdateModifiedByUserAND(BuildingAND);
            repo.Update(BuildingAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Building, true);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Associate(RepositoryFactory repo, GBuilding Building, int buildingGrpid)
        {
            GBuildingAND absObjAND = Building.GetBuildingANDId();
            if (absObjAND == null)
            {
                // Create Building AND
                CreateBuildingAND(repo, Building);
            }

            List<GMultiStorey> storey = Building.GetMultiStories().ToList();
            // Update AND Building - Assosiate with new group id
            GBuildingAND BuildingAND = Building.GetBuildingANDId();

            // Update ADM Building - Value Building Group ID, Modified date & Modified User
            BuildingAND.GroupId = buildingGrpid;
            UpdateModifiedByUserAND(BuildingAND);
            BuildingAND.Unit = storey.Count;
            repo.Update(BuildingAND);

            // Update status & floor num unit - ADM building
            Building.Unit = storey.Count;
            repo.UpdateByAND(Building, true);

            // update building & building group - floor num unit & num unit
            UpdateNumUnit(repo, buildingGrpid);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Associate(RepositoryFactory repo, GBuildingAND BuildingAND, int buildingGrpid)
        {
            // get ADM Building to update AndStatus
            GBuilding Building = repo.GetById<GBuilding>(BuildingAND.OriId);

            List<GMultiStorey> storey = Building.GetMultiStories().ToList();

            // Update AND Building - Value Building Group Id, Modified date & Modified User
            BuildingAND.GroupId = buildingGrpid;
            UpdateModifiedByUserAND(BuildingAND);
            BuildingAND.Unit = storey.Count;
            repo.Update(BuildingAND);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            Building.Unit = storey.Count;
            repo.UpdateByAND(Building, true);

            // update building & building group - floor num unit & num unit
            UpdateNumUnit(repo, buildingGrpid);

            // update work area flag as open 
            int AreaID = BuildingAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        // noraini ali - OKT 2020
        public List<GBuilding> FilterBuildingInWorkArea(List<GBuilding> _buildings, int _workareaid)
        {
            // filter ADM Building - get only fresh ADM
            List<GBuilding> buildings = new List<GBuilding>();
            foreach (GBuilding _buildingTemp in _buildings)
            {
                if (_buildingTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_buildingTemp.AndStatus.ToString()) || (_buildingTemp.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_buildingTemp.AreaId.ToString()) || (_buildingTemp.AreaId > 0))
                        {
                            if (_buildingTemp.AreaId.Value == _workareaid)
                            {
                                buildings.Add(_buildingTemp);
                            }
                        }
                    }
                }
            }
            return buildings;
        }

        public List<GBuildingAND> FilterBuildingInWorkArea(List<GBuildingAND> _buildings, int _workareaid)
        {
            // filter ADM Building - get only fresh ADM
            List<GBuildingAND> buildingsAND = new List<GBuildingAND>();
            foreach (GBuildingAND _buildingTemp in _buildings)
            {
                if (_buildingTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_buildingTemp.AreaId.ToString()) || _buildingTemp.AreaId > 0)
                    {
                        if (_buildingTemp.AreaId.Value == _workareaid)
                        {
                            buildingsAND.Add(_buildingTemp);
                        }
                    }
                }
            }
            return buildingsAND;
        }

        public void CheckFeatureOnVerification(GBuilding building)
        {
            GBuildingAND buildingAND = building.GetBuildingANDId();
            if (buildingAND != null)
            {
                throw new QualityControlException("Unable to edit feature without verification.");
            }
        }

        public string MessageButtonClick()
        {
            return "\n" + sb.ToString();
        }

        public void AssociateByDistance(GBuildingGroup buildGroup)
        {
            RepositoryFactory repo = new RepositoryFactory(buildGroup.SegmentName);
            UpdateNumUnit(repo, buildGroup.OID);

            // update work area flag as open 
            int AreaID = buildGroup.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Dissociate(RepositoryFactory repo, int buildingGroupId)
        {
            // update 
            UpdateNumUnit(repo, buildingGroupId);
            GBuildingGroup buildGroup = repo.GetById<GBuildingGroup>(buildingGroupId);

            // update work area flag as open 
            int AreaID = buildGroup.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void UpdateNumUnit(RepositoryFactory repo, int buildingGroupId)
        {
            GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(buildingGroupId);

            // check building must be in work area 
            if (ValidateWorkArea(repo, buildingGroup.OID))
            {
                // create AND Building Group to update num unit
                GBuildingGroupAND absObjAND = buildingGroup.GetBuildingGroupANDId();
                if (absObjAND == null)
                {
                    // Create Building Group AND
                    ANDBuildingGroupTools andBuildingGrp = new ANDBuildingGroupTools();
                    andBuildingGrp.CreateBuildingGroupAND(repo, buildingGroup);
                }

                // get AND building group and update total floor num unit for each building associated
                GBuildingGroupAND absObjAnd = buildingGroup.GetBuildingGroupANDId();
                UpdateTotalFloorUnit(repo, absObjAnd.OriId);

                // update total num unit for building group
                UpdateTotalNumUnit(repo, absObjAnd.OriId);
            }
        }

        public void UpdateTotalFloorUnit(RepositoryFactory repo, int buildingGroupAND_OriId)
        {
            GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(buildingGroupAND_OriId);
            GBuildingGroupAND buildingGroupAnd = buildingGroup.GetBuildingGroupANDId();

            // get all building AND to update floor num unit
            foreach (GBuildingAND buildingAND in buildingGroupAnd.GetBuildingsAND())
            {
                if (buildingAND.AndStatus != 2)
                {
                    List<GMultiStorey> storey = buildingAND.GetMultiStories().ToList();
                    int buildFlrUnit = storey.Count;

                    if (buildFlrUnit == 0 && string.IsNullOrEmpty(buildingAND.Unit.ToString()))
                    {
                        break;
                    }

                    int intUnit = buildingAND.Unit ?? 0;

                    if (buildFlrUnit != intUnit)
                    {
                        buildingAND.Unit = buildFlrUnit;
                        UpdateModifiedByUserAND(buildingAND);
                        repo.Update(buildingAND, false);
                    }
                }
            }

            // get all building ADM with AND not exist to update floor num unit
            foreach (GBuilding building in buildingGroupAnd.GetBuildings())
            {
                if (building.AndStatus != 2)
                {
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND == null)
                    {
                        List<GMultiStorey> storey = building.GetMultiStories().ToList();
                        int buildFlrUnit = storey.Count;
                        int intUnit = 0;

                        if (buildFlrUnit == 0 && string.IsNullOrEmpty(building.Unit.ToString()))
                        {
                            break;
                        }

                        if (!string.IsNullOrEmpty(building.Unit.ToString()))
                        {
                            intUnit = building.Unit.Value;
                        }

                        if (buildFlrUnit != intUnit)
                        {
                            //create building AND
                            CreateBuildingAND(repo, building);

                            // update floor unit for AND building
                            GBuildingAND buildingAnd = building.GetBuildingANDId();
                            buildingAnd.Unit = buildFlrUnit;
                            UpdateModifiedByUserAND(buildingAnd);
                            repo.Update(buildingAnd, false);

                            // update floor unit for ADM building
                            building.Unit = buildFlrUnit;
                            repo.UpdateByAND(building, false);
                        }
                    }
                }
            }
        }

        public void UpdateTotalNumUnit(RepositoryFactory repo, int buildingGroupAND_OriId)
        {
            GBuildingGroup buildingGroup = repo.GetById<GBuildingGroup>(buildingGroupAND_OriId);
            GBuildingGroupAND buildingGroupAnd = buildingGroup.GetBuildingGroupANDId();

            int totalFlrUnit = 0;

            // get total num unit for each building AND
            foreach (GBuildingAND buildingAND in buildingGroupAnd.GetBuildingsAND())
            {
                if (buildingAND.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(buildingAND.Unit.ToString()))
                    {
                        totalFlrUnit += buildingAND.Unit.Value;
                    }
                }
            }

            // get total num unit for each building ADM which AND not exist
            foreach (GBuilding building in buildingGroupAnd.GetBuildings())
            {
                if (building.AndStatus != 2)
                {
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND == null && !string.IsNullOrEmpty(building.Unit.ToString()))
                    {
                        totalFlrUnit += building.Unit.Value;
                    }
                }
            }

            if (totalFlrUnit > 0)
            {
                // update AND building group - num unit
                buildingGroupAnd.NumUnit = totalFlrUnit;
                UpdateModifiedByUserAND(buildingGroupAnd);
                repo.Update(buildingGroupAnd, false);

                // update ADM building group - num unit
                buildingGroup.NumUnit = totalFlrUnit;
                repo.UpdateByAND(buildingGroup, false);
            }
        }

        private bool ValidateWorkArea(RepositoryFactory repo, int buildGrpId)
        {
            bool pass = true;

            // check work area for building AND
            // if must be same work area as building group- to update floor num unit

            string Message1 = "\nUnable to update feature in different or not in work area :";
            string Message2 = "";

            GBuildingGroup buildGroup = repo.GetById<GBuildingGroup>(buildGrpId);
            if (string.IsNullOrEmpty(buildGroup.AreaId.ToString()) || buildGroup.AreaId.Value == 0)
            {
                Message2 = "\nBuilding Group ID: " + buildGroup.OID.ToString() + "-" + buildGroup.Name;
            }

            foreach (GBuildingAND _buildingAND in buildGroup.GetBuildingsAND())
            {
                if (_buildingAND.AndStatus != 2)
                {
                    List<GMultiStorey> storey = _buildingAND.GetMultiStories().ToList();
                    if (storey.Count != _buildingAND.Unit)
                    {
                        if (buildGroup.AreaId != _buildingAND.AreaId || string.IsNullOrEmpty(_buildingAND.AreaId.ToString())
                            || _buildingAND.AreaId.Value == 0)
                        {
                            bool isBuildingValid = false;
                            pass &= isBuildingValid;
                            Message2 = Message2 + "\nBuilding ID: " + _buildingAND.OriId.ToString() + "-" + _buildingAND.Name;
                        }
                    }
                }
            }

            // check work area for building ADM which AND not exist
            // if must be same work area as building group- to update floor num unit
            foreach (GBuilding _building in buildGroup.GetBuildings())
            {
                if (_building.AndStatus != 2)
                {
                    GBuildingAND buildingAND = _building.GetBuildingANDId();
                    if (buildingAND == null)
                    {
                        List<GMultiStorey> storey = _building.GetMultiStories().ToList();
                        if (storey.Count != _building.Unit)
                        {
                            if (buildGroup.AreaId != _building.AreaId || string.IsNullOrEmpty(_building.AreaId.ToString())
                                || _building.AreaId.Value == 0)
                            {
                                bool isBuildingValid = false;
                                pass &= isBuildingValid;
                                Message2 = Message2 + "\nBuilding ID: " + _building.OID.ToString() + "-" + _building.Name;
                            }
                        }
                    }
                }
            }

            if (!pass)
            {
                throw new QualityControlException(Message1 + "\n" + Message2);
            }

            return pass;
        }
    }
}


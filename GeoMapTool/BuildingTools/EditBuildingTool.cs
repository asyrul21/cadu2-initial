﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.Core.Sessions;
using Geomatic.UI.FeedBacks;
using Geomatic.MapTool.BuildingGroupTools;

namespace Geomatic.MapTool.BuildingTools
{
    public class EditBuildingTool : EditTool
    {
        #region Fields
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        protected const string Message1 = "Select a building to edit.";
        protected const string Message2 = "Clicked building location on map.";
        protected const string MessageForm1 = "Opening form completed";
        protected const string MessageButton1 = "Add button clicked";
        protected const string MessageButton2 = "Edit button clicked";
        protected const string MessageButton3 = "Delete button clicked";
        protected const string MessageButton4 = "Apply button edit building form clicked";
        protected const string MessageButton5 = "Apply button format address form clicked";
        #endregion

        public EditBuildingTool()
        {
            _name = "Edit Building";
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            OnReported(Message2);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GBuilding> buildings;
                    GBuilding building = null;

                    // added by noraini ali - Mei 2020
                    List<GBuildingAND> buildingsAND;
                    GBuildingAND buildingAND = null;

                    // end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }
                        
                        buildings = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        buildingsAND = repo.SpatialSearch<GBuildingAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (buildings.Count == 0 && buildingsAND.Count == 0)
                    {
                        return;
                    }

                    int _workAreaId = 0;
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id 
                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                        _workAreaId = andBuilding.GetWorkAreaId(SegmentName, point);

                        if (_workAreaId > 0)
                        {
                            List<GBuilding> _NewListbuildings = new List<GBuilding>();
                            List<GBuildingAND> _NewListbuildingsAND = new List<GBuildingAND>();

                            if (buildings.Count > 0)
                            {
                                _NewListbuildings = andBuilding.FilterBuildingInWorkArea(buildings, _workAreaId);
                            }

                            if (buildingsAND.Count > 0)
                            {
                                _NewListbuildingsAND = andBuilding.FilterBuildingInWorkArea(buildingsAND, _workAreaId);
                            }

                            //Assign filter feature in new list
                            List<GBuilding> _buildings = new List<GBuilding>();
                            foreach (GBuilding buildingTemp in _NewListbuildings)
                            {
                                _buildings.Add(buildingTemp);
                            }

                            foreach (GBuildingAND buildingTempAND in _NewListbuildingsAND)
                            {
                                GBuilding Building = repo.GetById<GBuilding>(buildingTempAND.OriId);
                                if (Building != null)
                                    _buildings.Add(Building);
                            }

                            buildingAND = null;
                            if (_buildings.Count == 0)
                            {
                                return;
                            }
                            else if (_buildings.Count == 1)
                            {
                                building = _buildings[0];
                                buildingAND = null;

                                // if OID exist in AND feature
                                GBuildingAND BuildingTempAND = building.GetBuildingANDId();
                                if (BuildingTempAND != null)
                                {
                                    buildingAND = BuildingTempAND;
                                    building = null;
                                }

                                // noraini ali - OKT 2020 - highlight feature
                                highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, true);
                            }
                            else
                            {
                                building = ChooseFeature(_buildings, null);
                                buildingAND = null;

                                // if OID exist in AND feature
                                GBuildingAND BuildingTempAND = repo.GetById<GBuildingAND>(building.OID);
                                if (BuildingTempAND != null)
                                {
                                    buildingAND = BuildingTempAND;
                                    building = null;
                                }
                                // noraini ali - OKT 2020 - highlight feature
                                highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, false);
                            }
                        }
                        else // feature not in work area - user can review only
                        {
                            if (buildings.Count > 0)
                            {
                                building = buildings[0];
                            }
                            else if (buildings.Count == 0)
                            {
                                if (buildingsAND.Count == 0)
                                {
                                    return;
                                }
                                else if (buildingsAND.Count > 0)
                                {
                                    buildingAND = buildingsAND[0];
                                }
                            }
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, true);
                        }
                    }
                    else  // OTHERS GROUP
                    {
                        // checking user non-AND cannot touch feature in Working Area
                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                        //if (!andBuilding.checkingPointInWorkArea(SegmentName, point))
                        //{
                        //    return;
                        //}

                        if (buildings.Count == 1)
                        {
                            building = buildings[0];
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, true);
                        }
                        else if (buildings.Count > 1)
                        {
                            building = ChooseFeature(buildings, null);
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, false);
                        }

                        if (building == null)
                        {
                            return;
                        }
                    }

                    bool success = false;

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        // added by noraini ali - Mei 2020
                        // checking feature can edit or review only
                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                        bool _CanEdit = andBuilding.ValidateUserCanEdit(SegmentName, _workAreaId);
                        if (_CanEdit)
                        {
                            if (building != null)
                            {
                                andBuilding.Update(repo, building);
                                OnReported(andBuilding.MessageButtonClick());
                                OnReported("Building updated. {0}", building.OID);
                                success = true;
                            }
                            else
                            {
                                andBuilding.Update(repo, buildingAND);
                                OnReported(andBuilding.MessageButtonClick());
                                OnReported("Building updated. {0}", buildingAND.OID);
                                success = true;
                            }
                        }
                        else
                        {
                            // noraini ali - NOV 2020
                            if (building == null)
                            {
                                andBuilding.Update(repo, buildingAND);
                                andBuilding.MessageButtonClick();
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }

                            using (EditBuildingForm form = new EditBuildingForm(building))
                            {
                                OnReported(MessageForm1);

                                if (form.ShowDialog() != DialogResult.OK)
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                OnReported(form.MessageButtonClick());
                                OnReported(MessageButton4);

                                // noraini ali - Mei 2021 - check feature lock by AND
                                andBuilding.CheckFeatureLock(building.AreaId,SegmentName);

                                // noraini ali - Mei 2021 - check feature on verification
                                andBuilding.CheckFeatureOnVerification(building);

                                using (new WaitCursor())
                                {
                                    form.SetValues();
                                    repo.Update(building);

                                    // noraini - Okt 2022 - enhancement building group 
                                    // to update num unit belong to building group Id
                                    if (building.HasGroup())
                                    {
                                        AutoUpdateNumUnit obj = new AutoUpdateNumUnit();
                                        obj.UpdateTotalNumUnit(repo, building.GroupId.Value);
                                    }
                                }
                            }

                            OnReported("Building updated. {0}", building.OID);
                            OnReported(Message1);
                            OnStepReported(Message1);
                            success = true;
                        }
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }

                    catch
                    {
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                }

                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Utilities;
using System.Windows.Forms;

namespace Geomatic.MapTool.BuildingTools
{
    public class MoveBuildingTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GBuilding _selectedBuilding;
        protected MovePoint _selectedBuildingPoint;
        protected MovePoint _selectedBuildingPointAND;
        protected GBuildingAND _selectedBuildingAND;

        #endregion

        public MoveBuildingTool()
        {
            _name = "Move Building";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedBuildingPoint == null)
            {
                _selectedBuildingPoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }
            if (_selectedBuildingPointAND == null)
            {
                _selectedBuildingPointAND = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported("Select a building to move.");
            OnStepReported("Select a building to move.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            MapDocument.ShowContextMenu(x, y);
                            break;
                        }
                        using (new WaitCursor())
                        {
                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                            {
                                if (_selectedBuilding != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuilding.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                                if (_selectedBuildingAND != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuildingAND.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                            }
                            else
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuilding.Point))
                                {
                                    _progress = Progress.Moving;
                                }
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }

                        _selectedBuildingPoint.MoveTo(point);
                        _selectedBuildingPointAND.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuilding> buildings = SelectBuilding(repo, point);
                                List<GBuildingAND> buildingsAND = SelectBuildingAND(repo, point);

                                if (buildings.Count == 0 && buildingsAND.Count == 0)
                                {
                                    break;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    _selectedBuilding = null;
                                    _selectedBuildingAND = null;

                                    ANDBuildingTools andBuilding = new ANDBuildingTools();

                                    if (buildings.Count > 0 && buildingsAND.Count == 0)
                                    {
                                        GBuilding building = buildings[0];

                                        andBuilding.CheckWithinWorkArea(SegmentName, building);
                                        andBuilding.CheckUserOwnWorkArea(SegmentName, building);

                                        _selectedBuildingPoint.Point = building.Point;
                                        _selectedBuildingPoint.Start(building.Point);
                                        _selectedBuilding = building;

                                        GProperty property = building.GetProperty();
                                        if (property != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                            _parentRadius.Start(property.Point);
                                        }
                                    }
                                    else if (buildings.Count == 0 && buildingsAND.Count > 0)
                                    {
                                        GBuildingAND buildingAND = buildingsAND[0];

                                        andBuilding.CheckWithinWorkArea(SegmentName, buildingAND);
                                        andBuilding.CheckUserOwnWorkArea(SegmentName, buildingAND);

                                        _selectedBuildingPointAND.Point = buildingAND.Point;
                                        _selectedBuildingPointAND.Start(buildingAND.Point);
                                        _selectedBuildingAND = buildingAND;

                                        GProperty property = buildingAND.GetProperty();
                                        if (property != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                            _parentRadius.Start(property.Point);
                                        }
                                    }
                                    else if (buildings.Count > 0 && buildingsAND.Count > 0)
                                    {
                                        GBuilding building = buildings[0];
                                        GBuildingAND buildingAND = buildingsAND[0];

                                        // selected as same property ADM & AND overlapping location
                                        // So active AND feature
                                        if (building.OID == buildingAND.OriId)
                                        {
                                            andBuilding.CheckWithinWorkArea(SegmentName, buildingAND);
                                            andBuilding.CheckUserOwnWorkArea(SegmentName, buildingAND);

                                            _selectedBuildingPointAND.Point = buildingAND.Point;
                                            _selectedBuildingPointAND.Start(buildingAND.Point);
                                            _selectedBuildingAND = buildingAND;

                                            GProperty property = buildingAND.GetProperty();
                                            if (property != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                                _parentRadius.Start(property.Point);
                                            }
                                        }
                                    }
                                }
                                else  // case non AND user
                                {
                                    // checking user non-AND cannot touch feature in Working Area
                                    ANDBuildingTools andBuilding = new ANDBuildingTools();
                                    //if (!andBuilding.checkingPointInWorkArea(SegmentName, point))
                                    //{
                                    //    return;
                                    //}

                                    if (buildings.Count > 0)
                                    {
                                        GBuilding building = buildings[0];
                                        _selectedBuildingPoint.Point = building.Point;
                                        _selectedBuildingPoint.Start(building.Point);
                                        _selectedBuilding = building;

                                        // noraini ali - Mei 2021 - check feature lock by AND
                                        andBuilding.CheckFeatureLock(building.AreaId,SegmentName);

                                        // noraini ali - Mei 2021 - check feature on verification
                                        andBuilding.CheckFeatureOnVerification(building);

                                        GProperty property = building.GetProperty();
                                        if (property != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                            _parentRadius.Start(property.Point);
                                        }
                                    }
                                }
                                _progress = Progress.TryMove;
                                OnReported("Drag selected building to move.");
                                OnStepReported("Drag selected building to move.");
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuilding> buildings = SelectBuilding(repo, point);
                                List<GBuildingAND> buildingsAND = SelectBuildingAND(repo, point);

                                // no building found
                                if (buildings.Count == 0 && buildingsAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedBuildingPoint.Stop();
                                    _selectedBuildingPointAND.Stop();
                                    _selectedBuilding = null;
                                    _selectedBuildingAND = null;
                                    break;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {                                  
                                    ANDBuildingTools andBuilding = new ANDBuildingTools();

                                    if (buildings.Count > 0 && buildingsAND.Count == 0)
                                    {
                                        // take 1st building
                                        GBuilding building = buildings[0];
                                        if (!_selectedBuilding.Equals(building))
                                        {
                                            _selectedBuildingPoint.Point = building.Point;
                                            _selectedBuildingPoint.Stop();
                                            _selectedBuildingPoint.Start(building.Point);
                                            _selectedBuilding = building;

                                            GProperty property = building.GetProperty();
                                            if (property != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(property.Point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (buildings.Count == 0 && buildingsAND.Count > 0)
                                    {
                                        // take 1st building
                                        GBuildingAND buildingAND = buildingsAND[0];
                                        if (!_selectedBuildingAND.Equals(buildingAND))
                                        {
                                            _selectedBuildingPointAND.Point = buildingAND.Point;
                                            _selectedBuildingPointAND.Stop();
                                            _selectedBuildingPointAND.Start(buildingAND.Point);
                                            _selectedBuildingAND = buildingAND;

                                            GProperty property = buildingAND.GetProperty();
                                            if (property != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(property.Point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (buildings.Count > 0 && buildingsAND.Count > 0)
                                    {
                                        GBuilding building = buildings[0];
                                        GBuildingAND buildingAND = buildingsAND[0];

                                        if (building.OID == buildingAND.OriId)
                                        {
                                            if (!_selectedBuildingAND.Equals(buildingAND))
                                            {
                                                _selectedBuildingPointAND.Point = buildingAND.Point;
                                                _selectedBuildingPointAND.Stop();
                                                _selectedBuildingPointAND.Start(buildingAND.Point);
                                                _selectedBuildingAND = buildingAND;

                                                GProperty property = buildingAND.GetProperty();
                                                if (property != null)
                                                {
                                                    _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                                    _parentRadius.Stop();
                                                    _parentRadius.Start(property.Point);
                                                }
                                                else
                                                {
                                                    _parentRadius.Stop();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                else // non AND User
                                { 
                                    // no building found
                                    if (buildings.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _parentRadius.Stop();
                                        _selectedBuildingPoint.Stop();
                                        _selectedBuilding = null;
                                        break;
                                    }

                                    // take 1st building
                                    GBuilding building = buildings[0];
                                    if (!_selectedBuilding.Equals(building))
                                    {
                                        _selectedBuildingPoint.Point = building.Point;
                                        _selectedBuildingPoint.Stop();
                                        _selectedBuildingPoint.Start(building.Point);
                                        _selectedBuilding = building;

                                        GProperty property = building.GetProperty();
                                        if (property != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                            _parentRadius.Stop();
                                            _parentRadius.Start(property.Point);
                                        }
                                        else
                                        {
                                            _parentRadius.Stop();
                                        }
                                        break;
                                    }
                                }                           
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }
                            using (new WaitCursor())
                            {
                                //IPoint newPoint = _selectedBuildingPoint.Stop() as IPoint;
                                //IPoint newPointAND = _selectedBuildingPointAND.Stop() as IPoint;

                                _parentRadius.Stop();
                                _progress = Progress.Select;

                                if (Core.Sessions.Session.User.GetGroup().Name != "AND")
                                {
                                    IPoint newPoint = _selectedBuildingPoint.Stop() as IPoint;

                                    GProperty property = _selectedBuilding.GetProperty();
                                    if (property != null)
                                    {
                                        double distance = newPoint.DistanceTo(property.Shape);
                                        List<GProperty> properties = repo.SpatialSearch<GProperty>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                        if (properties.Count > 0)
                                        {
                                            GProperty nearestProperty = GeometryUtils.GetNearestFeature(properties, newPoint);
                                            if (!nearestProperty.Equals(property))
                                            {
                                                throw new QualityControlException("Another property nearby.");
                                            }
                                        }
                                    }

                                    repo.StartTransaction(() =>
                                    {
                                        _selectedBuilding.Shape = newPoint;
                                        //repo.Update(_selectedBuilding);
                                        repo.UpdateGraphic(_selectedBuilding, true);

                                        foreach (GPoi poi in _selectedBuilding.GetPois())
                                        {
                                            poi.Shape = newPoint;
                                            repo.Update(poi);
                                        }
                                    });
                                    _selectedBuilding = null;
                                }
                                // added by noraini ali - Mei 2020 (case user AND)
                                else   //Case user AND
                                {
                                    // noraini ali - Sept 2020-Confirmation to move feature
                                    // process move the original feature (ADM)
                                    // confirm to move so need to update ADM feature to previous location
                                    // and create AND at new location

                                    bool bool_confirmMove = true;
                                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                                    confirmBox.SetCaption("Move Building")
                                        .SetButtons(MessageBoxButtons.OKCancel)
                                        .SetText("Confirm Move Building to New Location ?");

                                    if (confirmBox.Show() == DialogResult.Cancel)
                                    {
                                        bool_confirmMove = false;
                                    }
                                    
                                    if (bool_confirmMove)
                                    {
                                        // selected feature ADM Builidng
                                        if (_selectedBuilding != null)
                                        {
                                            IPoint newPoint = _selectedBuildingPoint.Stop() as IPoint;
                                            GProperty property = _selectedBuilding.GetProperty();
                                            if (property != null)
                                            {
                                                double distance = newPoint.DistanceTo(property.Shape);
                                                List<GProperty> properties = repo.SpatialSearch<GProperty>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                if (properties.Count > 0)
                                                {
                                                    GProperty nearestProperty = GeometryUtils.GetNearestFeature(properties, newPoint);
                                                    if (!nearestProperty.Equals(property))
                                                    {
                                                        throw new QualityControlException("Another property nearby.");
                                                    }
                                                }
                                            }
                                            repo.StartTransaction(() =>
                                            {
                                                //if (bool_confirmMove)
                                                //{
                                                // create AND Bulding with new Location
                                                ANDBuildingTools andBuilding = new ANDBuildingTools();
                                                if (_selectedBuilding != null)
                                                {
                                                    andBuilding.Move(repo, _selectedBuilding, newPoint);
                                                }
                                                else
                                                {
                                                    GBuilding Building = repo.GetById<GBuilding>(_selectedBuildingAND.OriId);
                                                    andBuilding.Move(repo, Building, newPoint);
                                                }
                                                //}
                                            });
                                            _selectedBuilding = null;
                                        }
                                        else // case Selected AND
                                        {
                                            IPoint newPointAND = _selectedBuildingPointAND.Stop() as IPoint;

                                            ANDBuildingTools andBuilding = new ANDBuildingTools();

                                            GProperty property = _selectedBuildingAND.GetProperty();
                                            if (property != null)
                                            {
                                                double distance = newPointAND.DistanceTo(property.Shape);
                                                List<GProperty> properties = repo.SpatialSearch<GProperty>(newPointAND.Buffer(distance), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                if (properties.Count > 0)
                                                {
                                                    GProperty nearestProperty = GeometryUtils.GetNearestFeature(properties, newPointAND);
                                                    if (!nearestProperty.Equals(property))
                                                    {
                                                        throw new QualityControlException("Another property nearby.");
                                                    }
                                                }

                                                repo.StartTransaction(() =>
                                                {
                                                    if (bool_confirmMove)
                                                    {
                                                        // create AND Bulding with new Location
                                                        GBuilding Building = repo.GetById<GBuilding>(_selectedBuildingAND.OriId);
                                                        andBuilding.Move(repo, Building, newPointAND);
                                                    }
                                                });
                                                _selectedBuildingAND = null;
                                            }
                                        }
                                    }
                                }
                            }
                            OnReported("Select a building to move.");
                            OnStepReported("Select a building to move.");
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected List<GBuilding> SelectBuilding(RepositoryFactory repo, IPoint point)
        {
            Query<GBuilding> query = new Query<GBuilding>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuilding>(query).ToList();
        }

        // added by noraini ali - 
        protected List<GBuildingAND> SelectBuildingAND(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuildingAND>(query).ToList();
        }
        // end

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Stop();
            }
            if (_selectedBuildingPointAND != null)
            {
                _selectedBuildingPointAND.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Stop();
                _selectedBuildingPoint = null;
            }
            if (_selectedBuildingPointAND != null)
            {
                _selectedBuildingPointAND.Stop();
                _selectedBuildingPointAND = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Refresh(hDC);
            }
            if (_selectedBuildingPointAND != null)
            {
                _selectedBuildingPointAND.Refresh(hDC);
            }
        }
    }
}

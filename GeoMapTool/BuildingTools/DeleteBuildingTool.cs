﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Rows;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.MapTool.BuildingTools
{
    public class DeleteBuildingTool : DeleteTool
    {
        private const string Message1 = "Select a building to delete.";
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public DeleteBuildingTool()
        {
            _name = "Delete Building";
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GBuilding> buildings;

                    GBuilding building = null;

                    List<GBuildingAND> buildingsAND;
                    GBuildingAND buildingAND = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        buildings = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        buildingsAND = repo.SpatialSearch<GBuildingAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (buildings.Count == 0 && buildingsAND.Count == 0)
                    {
                        return;
                    }

                    if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id & Checking point in Working Area & Owner
                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                        andBuilding.CheckWithinWorkArea(SegmentName, point);
                        andBuilding.CheckUserOwnWorkArea(SegmentName, point);
                        int _workAreaId = andBuilding.GetWorkAreaId(SegmentName, point);

                        List<GBuilding> _NewListbuildings = new List<GBuilding>();
                        List<GBuildingAND> _NewListbuildingsAND = new List<GBuildingAND>();

                        if (buildings.Count > 0)
                        {
                            _NewListbuildings = andBuilding.FilterBuildingInWorkArea(buildings, _workAreaId);
                        }

                        if (buildingsAND.Count > 0)
                        {
                            _NewListbuildingsAND = andBuilding.FilterBuildingInWorkArea(buildingsAND, _workAreaId);
                        }

                        // create new list to store all ADM feature to use in ChooseForm
                        List<GBuilding> _buildings = new List<GBuilding>();

                        foreach (GBuilding buildingTemp in _NewListbuildings)
                        {
                            _buildings.Add(buildingTemp);
                        }

                        foreach (GBuildingAND buildingTempAND in _NewListbuildingsAND)
                        {
                            GBuilding Building = repo.GetById<GBuilding>(buildingTempAND.OriId);
                            if (Building != null)
                                _buildings.Add(Building);
                        }

                        buildingAND = null;
                        if (_buildings.Count == 0)
                        {
                            return;
                        }
                        else if (_buildings.Count == 1)
                        {
                            building = _buildings[0];

                            // if OID exist in AND feature
                            GBuildingAND BuildingTempAND = building.GetBuildingANDId();
                            if (BuildingTempAND != null)
                            {
                                buildingAND = BuildingTempAND;
                                building = null;
                            }
                            // noraini ali - OKT 2020 - highlight feature
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, true);
                        }
                        else
                        {
                            building = ChooseFeature(_buildings, null);

                            // if OID exist in AND feature
                            GBuildingAND BuildingTempAND = repo.GetById<GBuildingAND>(building.OID);
                            if (BuildingTempAND != null)
                            {
                                buildingAND = BuildingTempAND;
                                building = null;
                            }
                            // noraini ali - OKT 2020 - highlight feature
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, false);
                        }
                    }
                    else  // OTHERS USER
                    {
                        // checking user non-AND cannot touch feature in Working Area
                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                        //if (!andBuilding.checkingPointInWorkArea(SegmentName, point))
                        //{
                        //    return;
                        //}

                        buildingAND = null;
                        if (buildings.Count == 1)
                        {
                            building = buildings[0];
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, true);
                        }
                        else if (buildings.Count > 1)
                        {
                            building = ChooseFeature(buildings, null);
                            highlightBuilding_PropertyNBuildGrp(repo, building, buildingAND, _selectedFeaturePoints, false);
                        }

                        if (building == null)
                        {
                            return;
                        }

                        // noraini ali - Mei 2021 - check feature lock by AND
                        andBuilding.CheckFeatureLock(building.AreaId,SegmentName);

                        // noraini ali - Mei 2021 - check feature on verification
                        andBuilding.CheckFeatureOnVerification(building);
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        // added by noraini ali - Mei 2020
                        if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                        {
                            if (buildingAND != null)
                            {
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Delete Building")
                                  .SetText("Are you sure you want to delete this building?\nId: {0}\n{1} {2}", buildingAND.OriId, buildingAND.Name, buildingAND.Name2);
                            }
                            else
                            {
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                .SetCaption("Delete Building")
                                .SetText("Are you sure you want to delete this building?\nId: {0}\n{1} {2}", building.OID, building.Name, building.Name2);
                            }
                        }
                        else
                        {
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                               .SetDefaultButton(MessageBoxDefaultButton.Button2)
                               .SetCaption("Delete Building")
                               .SetText("Are you sure you want to delete this building?\nId: {0}\n{1} {2}", building.OID, building.Name, building.Name2);
                        }

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            if (buildingAND != null)
                            {
                                // noraini ali -  Jul 2020 - validation building on MSA
                                ValidationFeatureToDelete(buildingAND);
                            }
                            else
                            {
                                // noraini ali -  Jul 2020 - validation building on MSA
                                ValidationFeatureToDelete(building);
                            }
                        }
                        else //OTHERS GROUP
                        {
                            //if (building.HasMultiStorey())
                            //modified by asyrul
                            if (building.HasMultiStorey() || building.HasPoi() || building.HasGroup())
                            {
                                throw new QualityControlException("Unable to delete building. Building has multistorey or poi or in Building Group");
                            }

                            if (building.IsNavigationReady)
                            {
                                if (!Session.User.CanDo(Command.NavigationItem))
                                {
                                    throw new NavigationControlException();
                                }
                            }
                        }

                        repo.StartTransaction(() =>
                        {
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                ANDBuildingTools andBuilding = new ANDBuildingTools();
                                if (buildingAND != null)
                                {
                                    andBuilding.Delete(repo, buildingAND);
                                }
                                else
                                {
                                    andBuilding.Delete(repo, building);
                                }
                            }
                            else
                            {
                                GProperty property = building.GetProperty();

                                if (property == null)
                                {
                                    foreach (GPoi poi in building.GetPois())
                                    {
                                        foreach (GPoiPhone poiPhone in poi.GetPhones())
                                        {
                                            repo.Delete(poiPhone);
                                        }
                                        repo.Delete(poi);
                                    }
                                }
                                else
                                {
                                    foreach (GPoi poi in building.GetPois())
                                    {
                                        poi.ParentType = (int)ParentClass.Property1;
                                        poi.ParentId = property.OID;
                                        poi.Shape = property.Shape;
                                        repo.Update(poi);
                                    }
                                }
                                foreach (GBuildingText text in building.GetTexts())
                                {
                                    repo.Delete(text);
                                }

                                OnReported("Building deleted. {0}", building.OID);
                                repo.Delete(building);
                            }
                        });

                    }
                    OnReported(Message1);
                    OnStepReported(Message1);
                    // noraini ali - Highlight feature
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }

                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        // added by noraini ali - Mei 2020
        private void ValidationFeatureToDelete(GBuilding building)
        {
            using (new WaitCursor())
            {
                if (building.HasMultiStorey() || building.HasGroup())
                {
                    throw new QualityControlException("Unable to delete building. Building has multistorey or in Building Group");
                }

                if (building.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }
            }
        }

        private void ValidationFeatureToDelete(GBuildingAND building)
        {
            using (new WaitCursor())
            {

                if (building.HasMultiStorey() || building.HasGroup())
                {
                    throw new QualityControlException("Unable to delete building. Building has multistorey or in Building Group");
                }
 
                if (building.IsNavigationReady)
                {
                    if (!Session.User.CanDo(Command.NavigationItem))
                    {
                        throw new NavigationControlException();
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Utilities;

namespace Geomatic.MapTool.BuildingTools
{
    public class MoveBuildingANDTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GBuildingAND _selectedBuilding;
        protected MovePoint _selectedBuildingPoint;

        #endregion

        public MoveBuildingANDTool()
        {
            _name = "Move Building (AND)";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary> 
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedBuildingPoint == null)
            {
                _selectedBuildingPoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
            }
            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported("Select a building (AND) to move.");
            OnStepReported("Select a building (AND) to move.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedBuilding.Point))
                            {
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }
                        _selectedBuildingPoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingAND> buildings = SelectBuildingAND(repo, point);
                                if (buildings.Count > 0)
                                {
                                    GBuildingAND building = buildings[0];
                                    _selectedBuildingPoint.Point = building.Point;
                                    _selectedBuildingPoint.Start(building.Point);
                                    _selectedBuilding = building;

                                    GProperty property = building.GetProperty();
                                    if (property != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                        _parentRadius.Start(property.Point);
                                    }
                                    // added by noraini ali - apr 2020 - search in Property AND
                                    else
                                    {
                                        GPropertyAND propertyAND = building.GetPropertyAND();
                                        if (property != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)propertyAND.Point.Buffer(10);
                                            _parentRadius.Start(propertyAND.Point);
                                        }
                                    }
                                    // end added

                                    _progress = Progress.TryMove;
                                    OnReported("Drag selected building (AND) to move.");
                                    OnStepReported("Drag selected building (AND) to move.");
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GBuildingAND> buildings = SelectBuildingAND(repo, point);
                                // no building found
                                if (buildings.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedBuildingPoint.Stop();
                                    _selectedBuilding = null;
                                    break;
                                }

                                // take 1st building
                                GBuildingAND building = buildings[0];
                                if (!_selectedBuilding.Equals(building))
                                {
                                    _selectedBuildingPoint.Point = building.Point;
                                    _selectedBuildingPoint.Stop();
                                    _selectedBuildingPoint.Start(building.Point);
                                    _selectedBuilding = building;

                                    GProperty property = building.GetProperty();
                                    if (property != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)property.Point.Buffer(10);
                                        _parentRadius.Stop();
                                        _parentRadius.Start(property.Point);
                                    }
                                    else
                                    {
                                        // added by noraini ali - apr 2020 - search in Property AND
                                        GPropertyAND propertyAND = building.GetPropertyAND();
                                        if (propertyAND != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)propertyAND.Point.Buffer(10);
                                            _parentRadius.Stop();
                                            _parentRadius.Start(propertyAND.Point);
                                        }
                                        else
                                        {
                                            _parentRadius.Stop();
                                        }                                         
                                    }
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            IPoint newPoint = _selectedBuildingPoint.Stop() as IPoint;
                            _parentRadius.Stop();
                            _progress = Progress.Select;

                            GProperty property = _selectedBuilding.GetProperty();
                            if (property != null)
                            {
                                double distance = newPoint.DistanceTo(property.Shape);
                                List<GProperty> properties = repo.SpatialSearch<GProperty>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                if (properties.Count > 0)
                                {
                                    GProperty nearestProperty = GeometryUtils.GetNearestFeature(properties, newPoint);
                                    if (!nearestProperty.Equals(property))
                                    {
                                        throw new QualityControlException("Another property nearby.");
                                    }
                                }
                            }
                          
                            repo.StartTransaction(() =>
                            {
                                _selectedBuilding.Shape = newPoint;
                                repo.Update(_selectedBuilding);
                            });
                            _selectedBuilding = null;
                        }
                        OnReported("Select a building (AND) to move.");
                        OnStepReported("Select a building (AND) to move.");
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected List<GBuildingAND> SelectBuildingAND(RepositoryFactory repo, IPoint point)
        {
            Query<GBuildingAND> query = new Query<GBuildingAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GBuildingAND>(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Stop();
                _selectedBuildingPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedBuildingPoint != null)
            {
                _selectedBuildingPoint.Refresh(hDC);
            }
        }
    }
}


﻿// created by asyrul on the 7th February 2019
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Earthworm.AO;
using Geomatic.Core.Exceptions;
using Geomatic.MapTool.PropertyTools;

namespace Geomatic.MapTool.BuildingTools
{
    public partial class ReassociateBuildingTool : ReassociateTool
    {
        #region Fields

        protected List<GBuilding> _selectedBuildings;
        protected List<GBuildingAND> _selectedBuildingsAND;
        protected List<MovePoint> _selectedBuildingPoints;
        int _workAreaId = 0;

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        #endregion

        public ReassociateBuildingTool()
        {
            _name = "Reassociate Building to Property";
            InitMenu();
            _selectedBuildingPoints = new List<MovePoint>();

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a building to reassociate.");
            OnStepReported("Select a building to reassociate.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                                List<GBuilding> buildings;
                                List<GBuildingAND> buildingsAND;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    buildings = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    buildingsAND = repo.SpatialSearch<GBuildingAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (buildings.Count == 0 && buildingsAND.Count == 0)
                                    {
                                        return;
                                    }

                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id
                                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                                        andBuilding.CheckWithinWorkArea(SegmentName, point);
                                        andBuilding.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andBuilding.GetWorkAreaId(SegmentName, point);

                                        if (buildings.Count > 0)
                                        {
                                            buildings = andBuilding.FilterBuildingInWorkArea(buildings, _workAreaId);
                                        }

                                        if (buildingsAND.Count > 0)
                                        {
                                            buildingsAND = andBuilding.FilterBuildingInWorkArea(buildingsAND, _workAreaId);
                                        }

                                        if (buildings.Count == 0 && buildingsAND.Count == 0)
                                        {
                                            throw new QualityControlException("Selected building not in this work area");
                                        }

                                        _selectedBuildings = buildings;
                                        _selectedBuildingsAND = buildingsAND;

                                        foreach (GBuilding building in _selectedBuildings)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = building.Point;
                                            movePoint.Start(building.Point);
                                            _selectedBuildingPoints.Add(movePoint);

                                            // noraini ali - OKT 2020
                                            // highlight feature

                                            if (building.PropertyId.HasValue)
                                            {
                                                highlightProperty(repo, building.PropertyId.Value, false, _selectedFeaturePoints);
                                            }
                                            if (building.GroupId.HasValue)
                                            {
                                                highlightBuildingGroup(repo, building.GroupId.Value, false, _selectedFeaturePoints);
                                            }
                                        }

                                        foreach (GBuildingAND building in _selectedBuildingsAND)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = building.Point;
                                            movePoint.Start(building.Point);
                                            _selectedBuildingPoints.Add(movePoint);

                                            // noraini ali - OKT 2020
                                            // highlight feature
                                            if (building.PropertyId.HasValue)
                                            {
                                                highlightProperty(repo, building.PropertyId.Value, false, _selectedFeaturePoints);
                                            }
                                            if (building.GroupId.HasValue)
                                            {
                                                highlightBuildingGroup(repo, building.GroupId.Value, false, _selectedFeaturePoints);
                                            }
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a property for building association or right-click for reselect menu.");
                                        OnStepReported("Select a property for building association or right-click for reselect menu.");

                                    }
                                    else  // OTHERS USER
                                    {
                                        // checking user non-AND cannot touch feature in Working Area
                                        ANDBuildingTools andBuilding = new ANDBuildingTools();
                                        //if (!andBuilding.checkingPointInWorkArea(SegmentName, point))
                                        //{
                                        //    return;
                                        //}

                                        _selectedBuildings = buildings;

                                        foreach (GBuilding building in _selectedBuildings)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = building.Point;
                                            movePoint.Start(building.Point);
                                            _selectedBuildingPoints.Add(movePoint);

                                            // noraini ali - OKT 2020
                                            // highlight feature
                                            if (building.PropertyId.HasValue)
                                            {
                                                highlightProperty(repo, building.PropertyId.Value, false, _selectedFeaturePoints);
                                            }
                                            if (building.GroupId.HasValue)
                                            {
                                                highlightBuildingGroup(repo, building.GroupId.Value, false, _selectedFeaturePoints);
                                            }

                                            // noraini ali - Mei 2021 - check feature lock by AND
                                            andBuilding.CheckFeatureLock(building.AreaId,SegmentName);

                                            // noraini ali - Mei 2021 - check feature on verification
                                            andBuilding.CheckFeatureOnVerification(building);
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a property for building association or right-click for reselect menu.");
                                        OnStepReported("Select a property for building association or right-click for reselect menu.");
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GProperty> properties = SelectProperty(repo, point);
                                List<GPropertyAND> propertiesAND = SelectPropertyAND(repo, point);

                                if (properties.Count == 0 && propertiesAND.Count == 0)
                                {
                                    return;
                                }

                                // noraini ali - 
                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id
                                    ANDPropertyTools andProperty = new ANDPropertyTools();
                                    andProperty.CheckWithinWorkArea(SegmentName, point);
                                    andProperty.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaIdProp = andProperty.GetWorkAreaId(SegmentName, point);

                                    if (_workAreaIdProp != _workAreaId)
                                    {
                                        throw new Exception("Unable Reassociate to Property different Work Area.");
                                    }

                                    // filter feature
                                    if (properties.Count > 0)
                                    {
                                        properties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                                    }

                                    if (propertiesAND.Count > 0)
                                    {
                                        propertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                                    }

                                    if (properties.Count == 0 && propertiesAND.Count ==0)
                                    {
                                        throw new QualityControlException("Properties is not in this work area.");
                                    }

                                    if (properties.Count > 0)
                                    {
                                        GProperty property = properties[0];
                                        // noraini ali - OKT 2020
                                        highlightProperty(repo, property.OID, true, _selectedFeaturePoints);
                                        ValidationOnProperty(repo, property);
                                    }
                                    else
                                    {
                                        if (propertiesAND.Count > 0)
                                        {
                                            GPropertyAND propertyAND = propertiesAND[0];
                                            // noraini ali - OKT 2020
                                            highlightProperty(repo, propertyAND.OID, true, _selectedFeaturePoints);
                                            ValidationOnProperty(repo, propertyAND);
                                        }
                                    }
                                }
                                else  // OTHERS USER
                                {
                                    if (properties.Count > 0)
                                    {
                                        GProperty property = properties[0];

                                        // noraini added
                                        if (property.HasBuilding())
                                        {
                                            throw new Exception("Unable Reassociate to Property has building.");
                                        }
                                        if (property.CanHasBuilding())
                                        {
                                            // noraini ali - OKT 2020 to highlight New Property
                                            highlightProperty(repo, property.OID, true, _selectedFeaturePoints);

                                            QuestionMessageBox confirmBox = new QuestionMessageBox();
                                            confirmBox.SetCaption("Reassociate Building")
                                                      .SetText("Are you sure you want to reassociate to {0}?", StringUtils.TrimSpaces(property.TypeValue));

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                using (new WaitCursor())
                                                {
                                                    repo.StartTransaction(() =>
                                                    {
                                                        foreach (GBuilding building in _selectedBuildings)
                                                        {
                                                            building.PropertyId = property.OID;
                                                            repo.Update(building);
                                                        }
                                                    });

                                                    OnReported("Select a building to reassociate.");
                                                    OnStepReported("Select a building to reassociate.");
                                                    Reset();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string msgtext = $"Unable Reassociate to Property type '{property.TypeValue}'";
                                            MessageBox.Show(msgtext, "Reassociate Building", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        // end added
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported("Select a building to reassociate.");
            OnStepReported("Select a building to reassociate.");
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedBuildings = null;
            _selectedBuildingsAND = null;
            foreach (MovePoint movePoint in _selectedBuildingPoints)
            {
                movePoint.Stop();
            }
            _selectedBuildingPoints.Clear();

            // noraini ali - OKT 2020 Clear Highlight feature
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            foreach (MovePoint movePoint in _selectedBuildingPoints)
            {
                movePoint.Refresh(hDC);
            }
        }

        // noraini ali - OKT 2020
        private void ValidationOnProperty(RepositoryFactory repo, GProperty property)
        {
            if (property.HasBuildingAND())
            {
                throw new Exception("Unable reassociate to property has building.");
            }
            else
            {
                if (property.HasBuilding())
                {
                    GBuilding building = property.Getbuilding();
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        throw new Exception("Unable to reassociate property. Property has building.");
                    }
                }
            }
            
            if (property.CanHasBuilding())
            {
                QuestionMessageBox confirmBox = new QuestionMessageBox();
                confirmBox.SetCaption("Reassociate Building")
                .SetText("Are you sure you want to reassociate to {0}?", StringUtils.TrimSpaces(property.TypeValue));

                if (confirmBox.Show() == DialogResult.Yes)
                {
                    using (new WaitCursor())
                    {
                        repo.StartTransaction(() =>
                        {
                            ANDBuildingTools andBuilding = new ANDBuildingTools();
                            foreach (GBuilding building in _selectedBuildings)
                            {
                                andBuilding.Reassociate(repo, building, property.OID);
                            }
                            foreach (GBuildingAND building in _selectedBuildingsAND)
                            {
                                andBuilding.Reassociate(repo, building, property.OID);
                            }
                        });

                        OnReported("Select a building to reassociate.");
                        OnStepReported("Select a building to reassociate.");
                        Reset();
                    }
                }
            }
            else
            {
                throw new Exception("Property cannot have building.");
            }
        }

        private void ValidationOnProperty(RepositoryFactory repo, GPropertyAND property)
        {
            if (property.HasBuildingAND())
            {
                throw new Exception("Unable reassociate to property has building.");
            }
            else
            {
                if (property.HasBuilding())
                {
                    GBuilding building = property.Getbuilding();
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        throw new Exception("Unable to reassociate property. Property has building.");
                    }
                }
            }
            if (property.CanHasBuilding())
            {
                QuestionMessageBox confirmBox = new QuestionMessageBox();
                confirmBox.SetCaption("Reassociate Building")
                .SetText("Are you sure you want to reassociate to {0}?", StringUtils.TrimSpaces(property.TypeValue));

                if (confirmBox.Show() == DialogResult.Yes)
                {
                    using (new WaitCursor())
                    {
                        repo.StartTransaction(() =>
                        {
                            ANDBuildingTools andBuilding = new ANDBuildingTools();
                            foreach (GBuilding building in _selectedBuildings)
                            {
                                andBuilding.Reassociate(repo, building, property.OriId);
                            }
                            foreach (GBuildingAND building in _selectedBuildingsAND)
                            {
                                andBuilding.Reassociate(repo, building, property.OriId);
                            }
                        });

                        OnReported("Select a building to reassociate.");
                        OnStepReported("Select a building to reassociate.");
                        Reset();
                    }
                }
            }
            else
            {
                throw new Exception("Property cannot have building.");
            }
        }
    }
}


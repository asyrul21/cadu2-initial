﻿using Earthworm.AO;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.MapTool.PropertyTools;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;

namespace Geomatic.MapTool.BuildingTools
{
    public class AddBuildingTool : AddTool
    {
        #region Fields

        protected MovePolygon _area;
        private IRgbColor _color = ColorUtils.Get(Color.Green);
        private const double RADIUS = 10;
        private const string Message1 = "Click on map location to add building.";
        private const string Message2 = "Select a property from the list for building association.";

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        #endregion

        #region Properties

        #endregion

        public AddBuildingTool()
        {
            _name = "Add Building";
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_area == null)
            {
                _area = new MovePolygon(ScreenDisplay, _color, _color, 1);
            }

            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (!_area.IsStarted)
            {
                _area.Polygon = (IPolygon)point.Buffer(RADIUS);
                _area.Start(point);
            }
            _area.MoveTo(point);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                InProgress = true;
                try
                {
                    CreateBuilding(point);
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }

                InProgress = false;
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        protected virtual void CreateBuilding(IPoint point)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GProperty> properties = repo.SpatialSearch<GProperty>(point.Buffer(RADIUS), esriSpatialRelEnum.esriSpatialRelContains).ToList();
            List<GPropertyAND> propertiesAND = repo.SpatialSearch<GPropertyAND>(point.Buffer(RADIUS), esriSpatialRelEnum.esriSpatialRelContains).ToList();

            GProperty property = null;
            GPropertyAND propertyAND = null;

            // added by noraini ali -  Mei 2020 
            // USER AND
            if (Session.User.GetGroup().Name == "AND")
            {
                // get Work Area Id
                ANDBuildingTools andBuilding = new ANDBuildingTools();
                andBuilding.CheckWithinWorkArea(SegmentName, point);
                andBuilding.CheckUserOwnWorkArea(SegmentName, point);
                int _workAreaId = andBuilding.GetWorkAreaId(SegmentName, point);

                // filter street ADM & AND in this working area only
                List<GProperty> _NewListProperties = new List<GProperty>();
                List<GPropertyAND> _NewListPropertiesAND = new List<GPropertyAND>();

                ANDPropertyTools andProperty = new ANDPropertyTools();

                if (properties.Count > 0)
                {
                    _NewListProperties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                }

                if (propertiesAND.Count > 0)
                {
                    _NewListPropertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                }

                // create new list to store all ADM feature to use in ChooseForm
                List<GProperty> _properties = new List<GProperty>();
                foreach (GProperty propertyTemp in _NewListProperties)
                {
                    if (propertyTemp.CanHasBuilding())
                    {
                        _properties.Add(propertyTemp);
                    }
                }

                foreach (GPropertyAND propertyTempAND in _NewListPropertiesAND)
                {
                    if (propertyTempAND.CanHasBuilding())
                    {
                        GProperty Property = repo.GetById<GProperty>(propertyTempAND.OriId);
                        if (Property != null)
                        {
                            _properties.Add(Property);
                        }
                    }
                }

                propertyAND = null;
                if (_properties.Count == 0)
                {
                    throw new QualityControlException("No property found or Property cannot have building.");
                }
                else if (_properties.Count == 1)
                {
                    property = _properties[0];
                    propertyAND = null;

                    GPropertyAND PropertyTempAND = property.GetPropertyANDId();
                    if (PropertyTempAND != null)
                    {
                        propertyAND = PropertyTempAND;
                        property = null;
                    }
                }
                else
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    property = ChooseFeature(_properties, null);
                    propertyAND = null;

                    // if OID exist in AND feature
                    GPropertyAND PropertyTempAND = repo.GetById<GPropertyAND>(property.OID);
                    if (PropertyTempAND != null)
                    {
                        propertyAND = PropertyTempAND;
                        property = null;
                    }
                }

                // validation On Property
                if (propertyAND != null)
                {
                    // noraini ali - OKT 2020 - hightlight feature
                    highlightProperty(repo, propertyAND.OriId, true, _selectedFeaturePoints);
                    ValidationOnProperty(repo, propertyAND);
                }
                else
                {
                    // noraini ali - OKT 2020 - hightlight feature
                    highlightProperty(repo, property.OID, true, _selectedFeaturePoints);
                    ValidationOnProperty(repo, property);
                }
            }
            else  //OTHER GROUP
            {
                // checking user non-AND cannot touch feature in Working Area
                ANDBuildingTools andBuilding = new ANDBuildingTools();
                andBuilding.checkingPointInWorkArea(SegmentName, point);
                //if (!andBuilding.checkingPointInWorkArea(SegmentName, point))
                //{
                //    return;
                //}

                if (properties.Count == 0)
                {
                    throw new QualityControlException("No property found.");
                }

                else if (properties.Count == 1)
                {
                    property = properties[0];
                }
                else if (properties.Count > 1)
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    property = ChooseFeature(properties, null);
                }

                // noraini ali - OKT 2020 - hightlight feature
                highlightProperty(repo, property.OID, true, _selectedFeaturePoints);


                if (!property.CanHasBuilding())
                {
                    throw new QualityControlException("Property cannot have building.");
                }

                if (property.HasBuilding())
                {
                    throw new QualityControlException("Property already have one building.");
                }

                if (property.HasFloor())
                {
                    throw new QualityControlException("Property already have floor.");
                }
            }

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }

                GBuilding newBuilding = repo.NewObj<GBuilding>();
                newBuilding.Init();

                if (propertyAND != null)
                {
                    newBuilding.PropertyId = propertyAND.OriId;
                }
                else
                {
                    newBuilding.PropertyId = property.OID;
                }
                newBuilding.Shape = point;

                //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                if (Session.User.GetGroup().Name == "AND")
                {
                    ANDBuildingTools andBuilding = new ANDBuildingTools();
                    andBuilding.UpdateCreatedByUserAND(newBuilding);
                    andBuilding.UpdateModifiedByUserAND(newBuilding);
                }

                newBuilding = repo.Insert(newBuilding, false);

                using (AddBuildingForm form = new AddBuildingForm(newBuilding))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        //repo.Update(newBuilding);
                        repo.UpdateRemainStatus(newBuilding, true);

                        //noraini ali- April 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDBuildingTools andBuilding = new ANDBuildingTools();
                            andBuilding.AddBuildingAND(repo, newBuilding);
                        }
                        else
                        {
                            foreach (GPoi poi in property.GetPois())
                            {
                                poi.ParentType = (int)ParentClass.Building;
                                poi.ParentId = newBuilding.OID;
                                poi.Shape = newBuilding.Shape;
                                repo.Update(poi);
                            }
                        }

                        // noraini ali - OKT 2020 - hightlight feature
                        highlightBuilding(repo, newBuilding.OID, true, _selectedFeaturePoints);

                        OnReported("New building created. {0}", newBuilding.OID);
                        OnReported(Message1);
                        OnStepReported(Message1);
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        public override bool Deactivate()
        {
            if (_area.IsStarted)
            {
                _area.Stop();
            }
            _area = null;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            _area.Refresh(hDC);
        }

        //added by noraini ali - Mei 2020
        public void ValidationOnProperty(RepositoryFactory repo, GProperty property)
        {
            if (!property.CanHasBuilding())
            {
                throw new QualityControlException("Property cannot have building.");
            }

            if (property.HasBuildingAND())
            {
                GBuildingAND buildingAnd = property.GetbuildingAnd();
                if (buildingAnd.AndStatus != 2)
                {
                    throw new QualityControlException("Property already have building.");
                }
            }
            else
            {
                if (property.HasBuilding())
                {
                    GBuilding building = property.Getbuilding();
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        throw new QualityControlException("Property already have building.");
                    }
                }
            }

            if (property.HasFloor())
            {
                throw new QualityControlException("Property already have floor.");
            }
        }

        public void ValidationOnProperty(RepositoryFactory repo, GPropertyAND propertyAND)
        {
            if (!propertyAND.CanHasBuilding())
            {
                throw new QualityControlException("Property cannot have building.");
            }

            if (propertyAND.HasBuildingAND())
            {
                GBuildingAND building = propertyAND.GetbuildingAND();
                if (building.AndStatus != 2)
                {
                    throw new QualityControlException("Property already have building.");
                }
            }
            else
            {
                if (propertyAND.HasBuilding())
                {
                    GBuilding building = propertyAND.Getbuilding();
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        throw new QualityControlException("Property already have building.");
                    }
                }
            }

            if (propertyAND.HasFloor())
            {
                throw new QualityControlException("Property already have floor.");
            }
        }
        // end
    }
}

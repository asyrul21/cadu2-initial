﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class RotateClockwise : Tool
	{
		public RotateClockwise()
		{
			_name = "Rotate Clockwise Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.RotateRasterClockwise();
		}
	}
}

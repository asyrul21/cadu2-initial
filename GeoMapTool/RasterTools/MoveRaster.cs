﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;

namespace Geomatic.MapTool.RasterTools
{
	public class MoveRaster : Tool
	{
		private const string Message1 = "Select the center point where you want the raster to be moved to.";

		public MoveRaster()
		{
			_name = "Move Raster";
		}

		public override void OnClick()
		{
			base.OnClick();

			OnReported(Message1);
			OnStepReported(Message1);
		}

		public override void OnMouseDown(int button, int shift, int x, int y)
		{
			if (button == MouseKey.Left)
			{
				MapDocument.MoveRaster(x, y);
			}

			if (button == MouseKey.Right)
			{
				MapDocument.ShowContextMenu(x, y);

			}
		}
	}
}

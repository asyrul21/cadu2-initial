﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class RotateAntiClockwise : Tool
	{
		public RotateAntiClockwise()
		{
			_name = "Rotate Anti-clockwise Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.RotateRasterAntiClockwise();
		}
	}
}

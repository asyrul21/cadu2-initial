﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;

namespace Geomatic.MapTool.RasterTools
{
	public class DeletePoint : Tool
	{
		private const string Message1 = "Select the point source and target point you want to delete. It will delete on pair.";

		public DeletePoint()
		{
			_name = "Delete Point";
		}

		public override void OnClick()
		{
			base.OnClick();

			OnReported(Message1);
			OnStepReported(Message1);
		}

		public override void OnMouseDown(int button, int shift, int x, int y)
		{
			if (button == MouseKey.Left)
			{
				MapDocument.DeletePoint(x, y);				
			}

			if (button == MouseKey.Right)
			{
				MapDocument.ShowContextMenu(x, y);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;

namespace Geomatic.MapTool.RasterTools
{
	public class AddPoint : Tool
	{
		bool isSource = true;
		private const string Message1 = "Select the point where you want to place the source and target point. Source is red marker and target is green marker.";

		public AddPoint()
		{
			_name = "Add Point";
		}

		public override void OnClick()
		{
			base.OnClick();

			OnReported(Message1);
			OnStepReported(Message1);
		}

		public override void OnMouseDown(int button, int shift, int x, int y)
		{
			if (button == MouseKey.Left)
			{
				if (isSource)
				{
					MapDocument.AddSourcePoint(x, y);
					isSource = false;
				}
				else
				{
					MapDocument.AddTargetPoint(x, y);
					isSource = true;
				}

			}

			if (button == MouseKey.Right)
			{
				MapDocument.ShowContextMenu(x, y);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class ScaleUp : Tool
	{
		public ScaleUp()
		{
			_name = "Scale Up Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.ScaleUpRaster();
		}
	}
}

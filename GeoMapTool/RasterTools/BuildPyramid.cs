﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class BuildPyramid : Tool
	{
		public BuildPyramid()
		{
			_name = "Build Raster Pyramid";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.BuildPyramid();
		}
	}
}

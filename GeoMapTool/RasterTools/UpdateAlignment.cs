﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class UpdateAlignment : Tool
	{
		public UpdateAlignment()
		{
			_name = "Update Raster Alignment";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.UpdateAlignment();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class FitToDisplay : Tool
	{
		public FitToDisplay()
		{
			_name = "Fit To Display";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.FitToDisplay();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class ScaleDown : Tool
	{
		public ScaleDown()
		{
			_name = "Scale Down Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.ScaleDownRaster();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class RegisterRaster : Tool
	{
		public RegisterRaster()
		{
			_name = "Register Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.RegisterRaster();
		}
	}
}

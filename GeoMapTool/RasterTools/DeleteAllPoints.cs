﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class DeleteAllPoints : Tool
	{
		public DeleteAllPoints()
		{
			_name = "Delete All Points";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.DeleteAllPoints();
		}
	}
}

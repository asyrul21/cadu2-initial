﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.RasterTools
{
	public class ExportRaster : Tool
	{
		public ExportRaster()
		{
			_name = "Export Raster";
		}

		public override void OnClick()
		{
			base.OnClick();
			MapDocument.ExportRaster();
		}
	}
}

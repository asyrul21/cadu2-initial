﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Search;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Utilities;
using Earthworm.AO;
using Geomatic.UI.Forms.Choose;

namespace Geomatic.MapTool
{
    // Implementation - noraini ali - OKT 2020
    // purpose to highlight feature
    // selected feature - highlight in Red Color
    // related feature - default color (ColorUtils.Select)

    public abstract class HighlightTool : ChooseTool
    {
        protected IRgbColor _colorRed = ColorUtils.Get(Color.Red);
        protected IRgbColor _colorBlue = ColorUtils.Get(Color.Blue);
        protected IRgbColor _colorBlack = ColorUtils.Get(Color.Black);

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        public List<MoveLine> highlightStreet(RepositoryFactory repo, int StreetId, bool SelectedInRed, List<MoveLine> _selectedFeatureLines)
        {
            MoveLine _selectedStreetLine;

            _selectedStreetLine = new MoveLine(ScreenDisplay, _colorBlack, LINE_WIDTH);

            if (SelectedInRed)
            {
                _selectedStreetLine = new MoveLine(ScreenDisplay, _colorRed, LINE_WIDTH);
            }

            GStreet street = repo.GetById<GStreet>(StreetId);
            if (street != null)
            {
                GStreetAND streetAND = street.GetStreetANDId();
                if (streetAND != null && streetAND.AndStatus != 2)
                {
                    _selectedStreetLine.Line = streetAND.Polyline;
                    _selectedStreetLine.Start(streetAND.FromPoint);
                    _selectedFeatureLines.Add(_selectedStreetLine);
                }
                else
                {
                    if (string.IsNullOrEmpty(street.AndStatus.ToString()) || (street.AndStatus == 0))
                    {
                        _selectedStreetLine.Line = street.Polyline;
                        _selectedStreetLine.Start(street.FromPoint);
                        _selectedFeatureLines.Add(_selectedStreetLine);
                    }
                }
            }
            return _selectedFeatureLines;
        }

        public List<MovePoint> highlightProperty(RepositoryFactory repo, int PropertyId, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            GProperty property = repo.GetById<GProperty>(PropertyId);
            if (property != null)
            {
                GPropertyAND propertyAND = property.GetPropertyANDId();
                if (propertyAND != null && propertyAND.AndStatus != 2)
                {
                    _selectedFeaturePoint.Point = propertyAND.Point;
                    _selectedFeaturePoint.Start(propertyAND.Point);
                    _featurePoints.Add(_selectedFeaturePoint);
                }
                else
                {
                    if (string.IsNullOrEmpty(property.AndStatus.ToString()) || (property.AndStatus == 0))
                    {
                        _selectedFeaturePoint.Point = property.Point;
                        _selectedFeaturePoint.Start(property.Point);
                        _featurePoints.Add(_selectedFeaturePoint);
                    }
                }
            }
            return _featurePoints;
        }

        public List<MovePoint> highlightBuildingGroup(RepositoryFactory repo, int BuildingGrpId, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            GBuildingGroup buildingGrp = repo.GetById<GBuildingGroup>(BuildingGrpId);
            if (buildingGrp != null)
            {
                GBuildingGroupAND buildingGrpAND = buildingGrp.GetBuildingGroupANDId();
                if (buildingGrpAND != null && buildingGrpAND.AndStatus != 2)
                {
                    _selectedFeaturePoint.Point = buildingGrpAND.Point;
                    _selectedFeaturePoint.Start(buildingGrpAND.Point);
                    _featurePoints.Add(_selectedFeaturePoint);
                }
                else
                {
                    if (string.IsNullOrEmpty(buildingGrp.AndStatus.ToString()) || (buildingGrp.AndStatus == 0))
                    {
                        _selectedFeaturePoint.Point = buildingGrp.Point;
                        _selectedFeaturePoint.Start(buildingGrp.Point);
                        _featurePoints.Add(_selectedFeaturePoint);
                    }
                }
            }
            return _featurePoints;
        }

        public List<MovePoint> highlightBuilding(RepositoryFactory repo, int BuildingId, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            GBuilding building = repo.GetById<GBuilding>(BuildingId);
            if (building != null)
            {
                GBuildingAND buildingAND = building.GetBuildingANDId();
                if (buildingAND != null && buildingAND.AndStatus != 2)
                {
                    _selectedFeaturePoint.Point = buildingAND.Point;
                    _selectedFeaturePoint.Start(buildingAND.Point);
                    _featurePoints.Add(_selectedFeaturePoint);
                }
                else
                {
                    if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                    {
                        _selectedFeaturePoint.Point = building.Point;
                        _selectedFeaturePoint.Start(building.Point);
                        _featurePoints.Add(_selectedFeaturePoint);
                    }
                }
            }
            return _featurePoints;
        }

        public List<MovePoint> highlightLandmark(RepositoryFactory repo, int LandamrkId, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            GLandmark landmark = repo.GetById<GLandmark>(LandamrkId);
            if (landmark != null)
            {
                GLandmarkAND landamrkAND = landmark.GetLandmarkANDId();
                if (landamrkAND != null && landamrkAND.AndStatus != 2)
                {
                    _selectedFeaturePoint.Point = landamrkAND.Point;
                    _selectedFeaturePoint.Start(landamrkAND.Point);
                    _featurePoints.Add(_selectedFeaturePoint);
                }
                else
                {
                    if (string.IsNullOrEmpty(landmark.AndStatus.ToString()) || (landmark.AndStatus == 0))
                    {
                        _selectedFeaturePoint.Point = landmark.Point;
                        _selectedFeaturePoint.Start(landmark.Point);
                        _featurePoints.Add(_selectedFeaturePoint);
                    }
                }
            }
            return _featurePoints;
        }

        public List<MovePoint> highlightJunction(RepositoryFactory repo, int JunctionId, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            GJunction junction = repo.GetById<GJunction>(JunctionId);
            if (junction != null)
            {
                GJunctionAND junctionAND = junction.GetJunctionANDId();
                if (junctionAND != null && junctionAND.AndStatus != 2)
                {
                    _selectedFeaturePoint.Point = junctionAND.Point;
                    _selectedFeaturePoint.Start(junctionAND.Point);
                    _featurePoints.Add(_selectedFeaturePoint);
                }
                else
                {
                    if (string.IsNullOrEmpty(junction.AndStatus.ToString()) || (junction.AndStatus == 0))
                    {
                        _selectedFeaturePoint.Point = junction.Point;
                        _selectedFeaturePoint.Start(junction.Point);
                        _featurePoints.Add(_selectedFeaturePoint);
                    }
                }
            }
            return _featurePoints;
        }

        public List<MovePoint> highlightPOI(RepositoryFactory repo, GPoi poi, bool SelectedInRed, List<MovePoint> _featurePoints)
        {
            MovePoint _selectedFeaturePoint;
            _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorBlue, POINT_SIZE);

            if (SelectedInRed)
            {
                _selectedFeaturePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
            }

            _selectedFeaturePoint.Point = poi.Point;
            _selectedFeaturePoint.Start(poi.Point);
            _featurePoints.Add(_selectedFeaturePoint);

            return _featurePoints;
        }

        public void HighlightNoDuplicateStreet(RepositoryFactory repo, List<int> _ListStreetId, List<MoveLine>_selectedFeatureLines)
        {
            var uniqueItemsList = _ListStreetId.Distinct().ToList();
            foreach (var item in uniqueItemsList)
            {
                highlightStreet(repo, item, false, _selectedFeatureLines);
            }
        }

        public List<MovePoint> highlightBuildGroup_Building(RepositoryFactory repo, GBuildingGroup buildingGroup, GBuildingGroupAND buildingGroupAND, List<MovePoint> _selectedFeaturePoints)
        {
            if (buildingGroupAND != null)
            {
                highlightBuildingGroup(repo, buildingGroupAND.OriId, true, _selectedFeaturePoints);
                foreach (GBuilding building in buildingGroupAND.GetBuildings())
                {
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND == null)
                        highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                }
                foreach (GBuildingAND building in buildingGroupAND.GetBuildingsAND())
                {
                    highlightBuilding(repo, building.OriId, false, _selectedFeaturePoints);
                }
            }
            else
            {
                highlightBuildingGroup(repo, buildingGroup.OID, true, _selectedFeaturePoints);
                foreach (GBuilding building in buildingGroup.GetBuildings())
                {
                    GBuildingAND buildingAND = building.GetBuildingANDId();
                    if (buildingAND == null)
                        highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                }
                foreach (GBuildingAND building in buildingGroup.GetBuildingsAND())
                {
                    highlightBuilding(repo, building.OriId, false, _selectedFeaturePoints);
                }
            }
            return _selectedFeaturePoints;
        }

        public List<MoveLine> highlightBuildGroup_StreetId(RepositoryFactory repo, GBuildingGroup buildingGroup, GBuildingGroupAND buildingGroupAND, List<MoveLine> _selectedFeatureLines)
        {
            if (buildingGroupAND != null)
            {
                if (buildingGroupAND.StreetId.HasValue)
                {
                    highlightStreet(repo, buildingGroupAND.StreetId.Value, false, _selectedFeatureLines);
                }
            }
            else
            {
                if (buildingGroup.StreetId.HasValue)
                {
                    highlightStreet(repo, buildingGroup.StreetId.Value, false, _selectedFeatureLines);
                }
            }
            return _selectedFeatureLines;
        }

        public List<MovePoint> highlightBuilding_PropertyNBuildGrp(RepositoryFactory repo, GBuilding building, GBuildingAND buildingAND, List<MovePoint> _selectedFeaturePoints, bool status)
        {
            // noraini ali - OKT 2020 - hightlight feature
            if (buildingAND != null)
            {
                if (status)
                    highlightBuilding(repo, buildingAND.OriId, true, _selectedFeaturePoints);

                if (buildingAND.PropertyId.HasValue)
                {
                    highlightProperty(repo, buildingAND.PropertyId.Value, false, _selectedFeaturePoints);
                }
                if (buildingAND.GroupId.HasValue && buildingAND.GroupId != 0)
                {
                    highlightBuildingGroup(repo, buildingAND.GroupId.Value, false, _selectedFeaturePoints);
                }
            }
            else
            {
                if (status)
                    highlightBuilding(repo, building.OID, true, _selectedFeaturePoints);

                if (building.PropertyId.HasValue)
                {
                    highlightProperty(repo, building.PropertyId.Value, false, _selectedFeaturePoints);
                }
                if (building.GroupId.HasValue && building.GroupId != 0)
                {
                    highlightBuildingGroup(repo, building.GroupId.Value, false, _selectedFeaturePoints);
                }
            }
            return _selectedFeaturePoints;
        }

        public List<MovePoint> highlightProperty_hasBuilding(RepositoryFactory repo, GProperty property, GPropertyAND propertyAND,
              List<MovePoint> _selectedFeaturePoints)
        {
            if (propertyAND != null)
            {
                highlightProperty(repo, propertyAND.OriId, true, _selectedFeaturePoints);
                if (propertyAND.HasBuildingAND())
                {
                    foreach (GBuildingAND buildingAnd in propertyAND.GetbuildingsAND())
                    {
                        if (buildingAnd.AndStatus != 2)
                        {
                            highlightBuilding(repo, buildingAnd.OriId, false, _selectedFeaturePoints);
                        }
                    }
                }
                else
                {
                    if (propertyAND.HasBuilding())
                    {
                        foreach (GBuilding building in propertyAND.GetBuildings())
                        {
                            if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                            {
                                highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                            }
                        }
                    }
                }
            }
            else
            {
                highlightProperty(repo, property.OID, true, _selectedFeaturePoints);
                if (property.HasBuildingAND())
                {
                    foreach (GBuildingAND buildingAnd in property.GetbuildingsAND())
                    {
                        if (buildingAnd.AndStatus != 2)
                        {
                            highlightBuilding(repo, buildingAnd.OriId, false, _selectedFeaturePoints);
                        }
                    }
                }
                else
                {
                    if (property.HasBuilding())
                    {
                        foreach (GBuilding building in property.GetBuildings())
                        {
                            if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                            {
                                highlightBuilding(repo, building.OID, false, _selectedFeaturePoints);
                            }
                        }
                    }
                }
            }
            return _selectedFeaturePoints;
        }

        public List<MoveLine> highlightProperty_Streetid(RepositoryFactory repo, GProperty property, GPropertyAND propertyAND, List<MoveLine> _selectedFeatureLines)
        {
            if (propertyAND != null)
            {
                if (propertyAND.StreetId.HasValue)
                {
                    highlightStreet(repo, propertyAND.StreetId.Value, false, _selectedFeatureLines);
                }
            }
            else
            {
                if (property.StreetId.HasValue)
                {
                    highlightStreet(repo, property.StreetId.Value, false, _selectedFeatureLines);
                }
            }
            return _selectedFeatureLines;
        }

        public List<MovePolygon> highlightWorkArea(GWorkArea workArea, IPoint point, List<MovePolygon> _SelectFeaturePolygon)
        {
            MovePolygon _selectedWorkAreaPolygon;

            // declare object
            _selectedWorkAreaPolygon = new MovePolygon(ScreenDisplay, _colorRed);

            // set work area polygon
            _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
            _selectedWorkAreaPolygon.Start(workArea.FromPoint);
            _SelectFeaturePolygon.Add(_selectedWorkAreaPolygon);
            return _SelectFeaturePolygon;
        }

        public void highlightPolygonClear(List<MovePolygon> _SelectFeaturePolygon)
        {
            foreach (MovePolygon movePolygon in _SelectFeaturePolygon)
            {
                movePolygon.Stop();
            }
            _SelectFeaturePolygon.Clear();

        }

        public void highlightClear(List<MovePoint> _selectedFeaturePoints, List<MoveLine> _selectedFeatureLines)
        {
            foreach (MovePoint movePoint in _selectedFeaturePoints)
            {
                movePoint.Stop();
            }
            _selectedFeaturePoints.Clear();

            foreach (MoveLine moveLine in _selectedFeatureLines)
            {
                moveLine.Stop();
            }
            _selectedFeatureLines.Clear();
        }
    }
    // end
}

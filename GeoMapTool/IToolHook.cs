﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool
{
    public interface IToolHook
    {
        void SetHook(object hook);
    }
}

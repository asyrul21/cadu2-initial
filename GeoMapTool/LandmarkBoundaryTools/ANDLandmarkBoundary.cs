﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using ESRI.ArcGIS.Geometry;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    // Implementation - noraini ali - Apr 2020
    public class ANDLandmarkBoundary : ValidateWorkAreaAndStatus
    { 
        public void Insert(RepositoryFactory repo, GLandmarkBoundary LandmarkBoundary, int andStatus)
        {
            // Update AND_STATUS for ADM Feature
            LandmarkBoundary.AndStatus = andStatus;
            repo.Update(LandmarkBoundary);

            //Copy data into AND Table
            List<GWorkArea> area;

            //Set other required data for AND LandmarkBoundary Table (AreaId, OriId, AndStatus, FromNodeId, ToNodeId)
            area = repo.SpatialSearch<GWorkArea>(LandmarkBoundary.Polygon, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            GLandmarkBoundaryAND LandmarkBoundaryAND = repo.NewObj<GLandmarkBoundaryAND>();
            //LandmarkBoundaryAND.CopyFrom(LandmarkBoundary);
            area.ForEach(thearea => LandmarkBoundaryAND.AreaId = thearea.OID);
            LandmarkBoundaryAND.OriId = LandmarkBoundary.OID;
            LandmarkBoundaryAND.AndStatus = LandmarkBoundary.AndStatus;
            LandmarkBoundaryAND.Shape = LandmarkBoundary.Polygon;

            //Insert into the AND Table
            repo.Insert(LandmarkBoundaryAND, false);

            // noraini ali - Jun 2020 - Set Completed Flag = 0 on AND Touch (ReOpen Work Area)
            GWorkArea workarea = repo.GetById<GWorkArea>(LandmarkBoundaryAND.AreaId.Value);
            UpdateWorkAreaCompletedFlag(repo, workarea);
        }

 
        public void Delete(RepositoryFactory repo, GLandmarkBoundaryAND deleteLandmarkBoundaryAND, int andStatus)
        {
            //if (ValidateAndStatus(deleteLandmarkBoundaryAND.AndStatus.Value, andStatus))
            //{
            //    deleteLandmarkBoundaryAND.AndStatus = andStatus;    
            //}
            repo.Update(deleteLandmarkBoundaryAND);

            // noraini ali - Jun 2020 - Set Completed Flag = 0 on AND Touch (ReOpen Work Area)
            GWorkArea workarea = repo.GetById<GWorkArea>(deleteLandmarkBoundaryAND.AreaId.Value);
            UpdateWorkAreaCompletedFlag(repo, workarea);
        }

        public void Move(RepositoryFactory repo, GLandmarkBoundaryAND moveLandmarkBoundaryAND, IPolygon newPolygon, int andStatus)
        {
            //if (ValidateAndStatus(moveLandmarkBoundaryAND.AndStatus.Value, andStatus))
            //{
            //    moveLandmarkBoundaryAND.AndStatus = andStatus;
            //}

            moveLandmarkBoundaryAND.Shape = newPolygon;
            repo.Update(moveLandmarkBoundaryAND);

            // noraini ali - Jun 2020 - Set Completed Flag = 0 on AND Touch (ReOpen Work Area)
            GWorkArea workarea = repo.GetById<GWorkArea>(moveLandmarkBoundaryAND.AreaId.Value);
            UpdateWorkAreaCompletedFlag(repo, workarea);
        }
    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Repositories;
using Earthworm.AO;
using Geomatic.UI.Utilities;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    public class AddLandmarkBoundaryTool : SingleLayerTool
    {
        #region Fields

        protected NewPolygon _newPolygon;

        #endregion

        public AddLandmarkBoundaryTool()
        {
            _name = "Add Landmark Boundary";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_newPolygon == null)
            {
                _newPolygon = new NewPolygon(ScreenDisplay, ColorUtils.Select, 1);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                if (button == MouseKey.Left)
                {
                    if (!InProgress)
                    {
                        InProgress = true;
                        _newPolygon.Start(point);
                    }
                    else
                    {
                        _newPolygon.AddPoint(point);
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                _newPolygon.MoveTo(point);
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        public override void OnDblClick()
        {
            if (!InProgress)
                return;

            _newPolygon.Stop();

            try
            {
                //CreateStreet();
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }

            InProgress = false;
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_newPolygon != null)
            {
                _newPolygon.Refresh(hDC);
            }
        }
    }
}

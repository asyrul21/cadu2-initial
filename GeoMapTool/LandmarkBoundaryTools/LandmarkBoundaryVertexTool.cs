﻿using Earthworm.AO;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    public abstract class LandmarkBoundaryVertexTool : Tool
    {
        #region Fields

        protected GLandmarkBoundary _selectedLandmarkBoundary;
        protected MovePolygon _selectedLandmarkBoundaryPolygon;
        protected List<MovePoint> _landmarkBoundaryVertices;

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        protected const string Message1 = "Select a landmark boundary.";

        protected GLandmarkBoundaryAND _selectedLandmarkBoundaryAND;
        #endregion

        public LandmarkBoundaryVertexTool()
        {
            _landmarkBoundaryVertices = new List<MovePoint>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedLandmarkBoundaryPolygon == null)
            {
                _selectedLandmarkBoundaryPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        protected List<GLandmarkBoundary> SelectLandmarkBoundary(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkBoundary> query = new Query<GLandmarkBoundary>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        // added by noraini ali - Mei 20202 CADU2 AND
        protected List<GLandmarkBoundaryAND> SelectLandmarkBoundaryAND(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkBoundaryAND> query = new Query<GLandmarkBoundaryAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }
        // end

        protected override void Reset()
        {
            _selectedLandmarkBoundary = null;
            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Stop();
            }
            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
            {
                movePoint.Stop();
            }
            _landmarkBoundaryVertices.Clear();
        }

        public override bool Deactivate()
        {
            _selectedLandmarkBoundary = null;
            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Stop();
                _selectedLandmarkBoundaryPolygon = null;
            }
            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
            {
                movePoint.Stop();
            }
            _landmarkBoundaryVertices.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Refresh(hDC);
            }
            if (_landmarkBoundaryVertices != null)
            {
                foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }
    }
}

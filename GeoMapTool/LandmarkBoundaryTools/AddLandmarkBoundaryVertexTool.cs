﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    public class AddLandmarkBoundaryVertexTool : LandmarkBoundaryVertexTool
    {
        private enum Progress
        {
            Select = 0,
            AddVertex
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;
        private const string Message2 = "Click a point to add vertex.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _pencilHandle;
                }
            }
        }

        #endregion

        public AddLandmarkBoundaryVertexTool()
        {
            _name = "Add Landmark Boundary Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                                    List<GLandmarkBoundaryAND> landmarkBoundariesAND = SelectLandmarkBoundaryAND(repo, point);

                                    // added by noraini ali - Mei 2020
                                    if (landmarkBoundaries.Count == 0 && landmarkBoundariesAND.Count == 0)
                                    {
                                        _selectedLandmarkBoundary = null;
                                        _selectedLandmarkBoundaryAND = null;
                                        _selectedLandmarkBoundaryPolygon.Stop();

                                        foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _landmarkBoundaryVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }

                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        ANDLandmarkBoundary andLandmarkBoundary = new ANDLandmarkBoundary();
                                        if (landmarkBoundaries.Count > 0 && landmarkBoundariesAND.Count == 0)
                                        {
                                            GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];

                                            // Check for User Own Work Area & Activity Within Work Area 
                                            andLandmarkBoundary.CheckWithinWorkArea(SegmentName, landmarkBoundary);
                                            andLandmarkBoundary.CheckUserOwnWorkArea(SegmentName, landmarkBoundary);

                                            repo.StartTransaction(() =>
                                            {
                                                if (string.IsNullOrEmpty(landmarkBoundary.AndStatus.ToString()) || (landmarkBoundary.AndStatus == 0))
                                                {
                                                    andLandmarkBoundary.Insert(repo, landmarkBoundary, 3);
                                                }
                                            });

                                            GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundary.GetLandmarkBoundayANDId();

                                            _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                            _selectedLandmarkBoundaryPolygon.Start(point);
                                            List<IPoint> vertices = landmarkBoundaryAND.Vertices;
                                            for (int count = 0; count < landmarkBoundaryAND.PointCount - 1; count++)
                                            {
                                                IPoint vertex = vertices[count];
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _landmarkBoundaryVertices.Add(movePoint);
                                            }
                                            _selectedLandmarkBoundaryAND = landmarkBoundaryAND;
                                            _progress = Progress.AddVertex;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundariesAND[0];

                                            _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                            _selectedLandmarkBoundaryPolygon.Start(point);
                                            List<IPoint> vertices = landmarkBoundaryAND.Vertices;
                                            for (int count = 0; count < landmarkBoundaryAND.PointCount - 1; count++)
                                            {
                                                IPoint vertex = vertices[count];
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _landmarkBoundaryVertices.Add(movePoint);
                                            }
                                            _selectedLandmarkBoundaryAND = landmarkBoundaryAND;
                                            _progress = Progress.AddVertex;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                    }
                                    else  // others User
                                    {
                                        if (landmarkBoundaries.Count > 0)
                                        {
                                            GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];

                                            _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                            _selectedLandmarkBoundaryPolygon.Start(point);
                                            List<IPoint> vertices = landmarkBoundary.Vertices;
                                            for (int count = 0; count < landmarkBoundary.PointCount - 1; count++)
                                            {
                                                IPoint vertex = vertices[count];
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _landmarkBoundaryVertices.Add(movePoint);
                                            }
                                            _selectedLandmarkBoundary = landmarkBoundary;
                                            _progress = Progress.AddVertex;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        else
                                        {
                                            _selectedLandmarkBoundary = null;
                                            _selectedLandmarkBoundaryPolygon.Stop();
                                            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _landmarkBoundaryVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.AddVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 4, 1);
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        IPolygon polygon = _selectedLandmarkBoundaryAND.Polygon;
                                        if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                        {
                                            if (_selectedLandmarkBoundaryAND.TryAddVertex(point))
                                            {
                                                repo.StartTransaction(() =>
                                                {
                                                    repo.Update(_selectedLandmarkBoundaryAND);

                                                    _selectedLandmarkBoundaryPolygon.Stop();
                                                    _selectedLandmarkBoundaryPolygon.Polygon = _selectedLandmarkBoundaryAND.Polygon;
                                                    _selectedLandmarkBoundaryPolygon.Start(point);
                                                    foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                                    {
                                                        movePoint.Stop();
                                                    }
                                                    _landmarkBoundaryVertices.Clear();
                                                    List<IPoint> vertices = _selectedLandmarkBoundaryAND.Vertices;
                                                    for (int count = 0; count < _selectedLandmarkBoundaryAND.PointCount - 1; count++)
                                                    {
                                                        IPoint vertex = vertices[count];
                                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                        movePoint.Point = vertex;
                                                        movePoint.Start(point);
                                                        _landmarkBoundaryVertices.Add(movePoint);
                                                    }
                                                });
                                                break;
                                            }
                                        }

                                        List<GLandmarkBoundaryAND> landmarkBoundariesAND = SelectLandmarkBoundaryAND(repo, point);
                                        if (landmarkBoundariesAND.Count == 0)
                                        {
                                            _progress = Progress.Select;
                                            _selectedLandmarkBoundaryAND = null;
                                            _selectedLandmarkBoundaryPolygon.Stop();
                                            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _landmarkBoundaryVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }

                                        GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundariesAND[0];
                                        if (!_selectedLandmarkBoundaryAND.Equals(landmarkBoundaryAND))
                                        {
                                            _selectedLandmarkBoundaryPolygon.Stop();
                                            _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                            _selectedLandmarkBoundaryPolygon.Start(point);
                                            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _landmarkBoundaryVertices.Clear();
                                            List<IPoint> vertices = landmarkBoundaryAND.Vertices;
                                            for (int count = 0; count < landmarkBoundaryAND.PointCount - 1; count++)
                                            {
                                                IPoint vertex = vertices[count];
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _landmarkBoundaryVertices.Add(movePoint);
                                            }
                                            _selectedLandmarkBoundaryAND = landmarkBoundaryAND;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }                                   
                                    }
                                    else  // others Group User
                                    {
                                        IPolygon polygon = _selectedLandmarkBoundary.Polygon;
                                        if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                        {
                                            if (_selectedLandmarkBoundary.TryAddVertex(point))
                                            {
                                                repo.StartTransaction(() =>
                                                {
                                                    repo.Update(_selectedLandmarkBoundary);

                                                    _selectedLandmarkBoundaryPolygon.Stop();
                                                    _selectedLandmarkBoundaryPolygon.Polygon = _selectedLandmarkBoundary.Polygon;
                                                    _selectedLandmarkBoundaryPolygon.Start(point);
                                                    foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                                    {
                                                        movePoint.Stop();
                                                    }
                                                    _landmarkBoundaryVertices.Clear();
                                                    List<IPoint> vertices = _selectedLandmarkBoundary.Vertices;
                                                    for (int count = 0; count < _selectedLandmarkBoundary.PointCount - 1; count++)
                                                    {
                                                        IPoint vertex = vertices[count];
                                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                        movePoint.Point = vertex;
                                                        movePoint.Start(point);
                                                        _landmarkBoundaryVertices.Add(movePoint);
                                                    }
                                                });
                                                break;
                                            }
                                        
                                        }

                                        List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                                        if (landmarkBoundaries.Count == 0)
                                        {
                                            _progress = Progress.Select;
                                            _selectedLandmarkBoundary = null;
                                            _selectedLandmarkBoundaryPolygon.Stop();
                                            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _landmarkBoundaryVertices.Clear();
                                            OnReported(Message1);
                                            OnStepReported(Message1);
                                            break;
                                        }

                                        GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];
                                        if (!_selectedLandmarkBoundary.Equals(landmarkBoundary))
                                        {
                                            _selectedLandmarkBoundaryPolygon.Stop();
                                            _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                            _selectedLandmarkBoundaryPolygon.Start(point);
                                            foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                            {
                                                movePoint.Stop();
                                            }
                                            _landmarkBoundaryVertices.Clear();
                                            List<IPoint> vertices = landmarkBoundary.Vertices;
                                            for (int count = 0; count < landmarkBoundary.PointCount - 1; count++)
                                            {
                                                IPoint vertex = vertices[count];
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                movePoint.Point = vertex;
                                                movePoint.Start(point);
                                                _landmarkBoundaryVertices.Add(movePoint);
                                            }
                                            _selectedLandmarkBoundary = landmarkBoundary;
                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

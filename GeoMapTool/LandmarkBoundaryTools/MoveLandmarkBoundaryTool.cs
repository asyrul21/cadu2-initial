﻿using Earthworm.AO;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    public class MoveLandmarkBoundaryTool : MoveTool
    {
        #region Fields

        protected GLandmarkBoundary _selectedLandmarkBoundary;
        protected MovePolygon _selectedLandmarkBoundaryPolygon;

        private const string Message1 = "Select a landmark boundary to move.";
        private const string Message2 = "Drag selected landmark boundary to move.";

        protected GLandmarkBoundaryAND _selectedLandmarkBoundaryAND;  // added by noraini ali - Mei 2020

        #endregion

        public MoveLandmarkBoundaryTool()
        {
            _name = "Move Landmark Boundary";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedLandmarkBoundaryPolygon == null)
            {
                _selectedLandmarkBoundaryPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Intersects(_selectedLandmarkBoundaryAND.Polygon))
                                {
                                    _selectedLandmarkBoundaryPolygon.Restart(point);
                                    _progress = Progress.Moving;
                                }
                            }
                            else
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Intersects(_selectedLandmarkBoundary.Polygon))
                                {
                                    _selectedLandmarkBoundaryPolygon.Restart(point);
                                    _progress = Progress.Moving;
                                }
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedLandmarkBoundaryPolygon.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                                List<GLandmarkBoundaryAND> landmarkBoundariesAND = SelectLandmarkBoundaryAND(repo, point);

                                if (landmarkBoundaries.Count == 0 && landmarkBoundariesAND.Count ==0)
                                {
                                    break;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    ANDLandmarkBoundary andLandmarkBoundary = new ANDLandmarkBoundary();

                                    if (landmarkBoundaries.Count > 0 && landmarkBoundariesAND.Count == 0) 
                                    {
                                        GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];

                                        // Check for User Own Work Area & Activity Within Work Area 
                                        andLandmarkBoundary.CheckWithinWorkArea(SegmentName, landmarkBoundary);
                                        andLandmarkBoundary.CheckUserOwnWorkArea(SegmentName, landmarkBoundary);

                                        repo.StartTransaction(() =>
                                        {
                                            if (string.IsNullOrEmpty(landmarkBoundary.AndStatus.ToString()) || (landmarkBoundary.AndStatus == 0))
                                            {
                                                andLandmarkBoundary.Insert(repo, landmarkBoundary, 3);
                                            }
                                        });

                                        GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundary.GetLandmarkBoundayANDId();

                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        _selectedLandmarkBoundaryAND = landmarkBoundaryAND;

                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundariesAND[0];

                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        _selectedLandmarkBoundaryAND = landmarkBoundaryAND;

                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                }
                                else  // others User
                                {
                                    if (landmarkBoundaries.Count > 0)
                                    {
                                        GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];
                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        _selectedLandmarkBoundary = landmarkBoundary;

                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                                List<GLandmarkBoundaryAND> landmarkBoundariesAND = SelectLandmarkBoundaryAND(repo, point);

                                // no landmark boundary found
                                if (landmarkBoundaries.Count == 0 && landmarkBoundariesAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedLandmarkBoundaryPolygon.Stop();
                                    _selectedLandmarkBoundary = null;
                                    break;
                                }

                                // added by noraini ali - Mei 2020
                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // take 1st landmark boundary
                                    GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];
                                    if (!_selectedLandmarkBoundary.Equals(landmarkBoundary))
                                    {
                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Stop();
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        _selectedLandmarkBoundary = landmarkBoundary;
                                        break;
                                    }
                                }
                                else
                                {
                                    // take 1st landmark boundaryAND
                                    GLandmarkBoundaryAND landmarkBoundaryAND = landmarkBoundariesAND[0];
                                    if (!_selectedLandmarkBoundaryAND.Equals(landmarkBoundaryAND))
                                    {
                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundaryAND.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Stop();
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        _selectedLandmarkBoundaryAND = landmarkBoundaryAND;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            IPolygon newPolygon = _selectedLandmarkBoundaryPolygon.Stop() as IPolygon;
                            _progress = Progress.Select;

                            // added by noraini ali - Mei 2020
                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                            {
                                ANDLandmarkBoundary andLandmarkBoundary = new ANDLandmarkBoundary();

                                repo.StartTransaction(() =>
                                {
                                    andLandmarkBoundary.Move(repo, _selectedLandmarkBoundaryAND, newPolygon, 3);
                                });
                                _selectedLandmarkBoundaryAND = null;
                            }
                            else
                            {
                                repo.StartTransaction(() =>
                                {
                                    _selectedLandmarkBoundary.Shape = newPolygon;
                                    repo.Update(_selectedLandmarkBoundary);
                                });
                                _selectedLandmarkBoundary = null;
                            }
                        }
                        OnReported(Message1);
                        OnStepReported(Message1);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected List<GLandmarkBoundary> SelectLandmarkBoundary(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkBoundary> query = new Query<GLandmarkBoundary>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        // added by noraini ali - 
        protected List<GLandmarkBoundaryAND> SelectLandmarkBoundaryAND(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkBoundaryAND> query = new Query<GLandmarkBoundaryAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }
        // end

        protected override void Reset()
        {
            _progress = Progress.Select;

            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Stop();
                _selectedLandmarkBoundaryPolygon = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedLandmarkBoundaryPolygon != null)
            {
                _selectedLandmarkBoundaryPolygon.Refresh(hDC);
            }
        }
    }
}

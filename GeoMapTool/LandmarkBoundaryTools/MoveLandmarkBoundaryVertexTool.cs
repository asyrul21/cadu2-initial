﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.LandmarkBoundaryTools
{
    public class MoveLandmarkBoundaryVertexTool : LandmarkBoundaryVertexTool
    {
        private enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        private int _crosshairHandle;
        protected MovePoint _selectedVertex;
        protected MovePolygonPoint _movePolygonPoint;
        private const string Message2 = "Drag a vertex to move.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _crosshairHandle;
                }
            }
        }

        #endregion

        public MoveLandmarkBoundaryVertexTool()
        {
            _name = "Move Landmark Boundary Vertex";
            _crosshairHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_movePolygonPoint == null)
            {
                _movePolygonPoint = new MovePolygonPoint(ScreenDisplay, _color2);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();
                            IPolygon polygon = _selectedLandmarkBoundary.Polygon;

                            if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                            {
                                if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedLandmarkBoundary.PointCount - 1)
                                {
                                    hitSegmentIndex = 0;
                                }
                                _movePolygonPoint.Index = hitSegmentIndex;
                                _selectedVertex = _landmarkBoundaryVertices[hitSegmentIndex];
                                _movePolygonPoint.Start(point);
                                _progress = Progress.Moving;
                                break;
                            }

                            List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                            if (landmarkBoundaries.Count == 0)
                            {
                                _progress = Progress.Select;
                                _selectedLandmarkBoundary = null;
                                _selectedLandmarkBoundaryPolygon.Stop();
                                foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _landmarkBoundaryVertices.Clear();
                                OnReported(Message1);
                                OnStepReported(Message1);
                                break;
                            }

                            GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];
                            if (!_selectedLandmarkBoundary.Equals(landmarkBoundary))
                            {
                                _selectedLandmarkBoundaryPolygon.Stop();
                                _movePolygonPoint.Polygon = landmarkBoundary.Polygon;
                                _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                _selectedLandmarkBoundaryPolygon.Start(point);
                                foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _landmarkBoundaryVertices.Clear();

                                List<IPoint> vertices = landmarkBoundary.Vertices;
                                for (int count = 0; count < landmarkBoundary.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _landmarkBoundaryVertices.Add(movePoint);
                                }
                                _selectedLandmarkBoundary = landmarkBoundary;
                                OnReported(Message2);
                                OnStepReported(Message2);
                                break;
                            }
                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _movePolygonPoint.MoveTo(point);
                        _selectedVertex.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GLandmarkBoundary> landmarkBoundaries = SelectLandmarkBoundary(repo, point);
                                    if (landmarkBoundaries.Count > 0)
                                    {
                                        GLandmarkBoundary landmarkBoundary = landmarkBoundaries[0];

                                        _movePolygonPoint.Polygon = landmarkBoundary.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Polygon = landmarkBoundary.Polygon;
                                        _selectedLandmarkBoundaryPolygon.Start(point);
                                        List<IPoint> vertices = landmarkBoundary.Vertices;
                                        for (int count = 0; count < landmarkBoundary.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _landmarkBoundaryVertices.Add(movePoint);
                                        }
                                        _selectedLandmarkBoundary = landmarkBoundary;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedLandmarkBoundary = null;
                                        _selectedLandmarkBoundaryPolygon.Stop();
                                        foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _landmarkBoundaryVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            using (new WaitCursor())
                            {
                                IPolygon polygon = (IPolygon)_movePolygonPoint.Stop();
                                _selectedVertex.Stop();

                                repo.StartTransaction(() =>
                                {
                                    _selectedLandmarkBoundary.Shape = polygon;
                                    repo.Update(_selectedLandmarkBoundary);
                                });

                                _selectedLandmarkBoundaryPolygon.Stop();
                                _movePolygonPoint.Polygon = polygon;
                                _selectedLandmarkBoundaryPolygon.Polygon = polygon;
                                _selectedLandmarkBoundaryPolygon.Start(point);
                                foreach (MovePoint movePoint in _landmarkBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _landmarkBoundaryVertices.Clear();
                                List<IPoint> vertices = _selectedLandmarkBoundary.Vertices;
                                for (int count = 0; count < _selectedLandmarkBoundary.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _landmarkBoundaryVertices.Add(movePoint);
                                }
                                _progress = Progress.TryMove;
                                break;
                            }
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_movePolygonPoint != null)
            {
                _movePolygonPoint.Refresh(hDC);
            }
        }
    }
}

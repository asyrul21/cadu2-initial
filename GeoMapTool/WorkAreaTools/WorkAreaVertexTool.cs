﻿using Earthworm.AO;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.WorkAreaTools
{
    public abstract class WorkAreaVertexTool : Tool
    {
        #region Fields

        protected GWorkArea _selectedWorkArea;
        protected MovePolygon _selectedWorkAreaPolygon;
        protected List<MovePoint> _workAreaVertices;

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        protected const string Message1 = "Select a work area.";

        #endregion

        public WorkAreaVertexTool()
        {
            _workAreaVertices = new List<MovePoint>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedWorkAreaPolygon == null)
            {
                _selectedWorkAreaPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        protected List<GWorkArea> SelectWorkArea(RepositoryFactory repo, IPoint point)
        {
            Query<GWorkArea> query = new Query<GWorkArea>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _selectedWorkArea = null;
            if (_selectedWorkAreaPolygon != null)
            {
                _selectedWorkAreaPolygon.Stop();
            }
            foreach (MovePoint movePoint in _workAreaVertices)
            {
                movePoint.Stop();
            }
            _workAreaVertices.Clear();
        }

        public override bool Deactivate()
        {
            _selectedWorkAreaPolygon = null;
            if (_selectedWorkAreaPolygon != null)
            {
                _selectedWorkAreaPolygon.Stop();
                _selectedWorkAreaPolygon = null;
            }
            foreach (MovePoint movePoint in _workAreaVertices)
            {
                movePoint.Stop();
            }
            _workAreaVertices.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedWorkAreaPolygon != null)
            {
                _selectedWorkAreaPolygon.Refresh(hDC);
            }
            if (_workAreaVertices != null)
            {
                foreach (MovePoint movePoint in _workAreaVertices)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }

        protected List<GStreet> GetDiffList(List<GStreet> beforeReshape, List<GStreet> afterReshape)
        {
            List<GStreet> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GProperty> GetDiffList(List<GProperty> beforeReshape, List<GProperty> afterReshape)
        {
            List<GProperty> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GJunction> GetDiffList(List<GJunction> beforeReshape, List<GJunction> afterReshape)
        {
            List<GJunction> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GBuilding> GetDiffList(List<GBuilding> beforeReshape, List<GBuilding> afterReshape)
        {
            List<GBuilding> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GLandmark> GetDiffList(List<GLandmark> beforeReshape, List<GLandmark> afterReshape)
        {
            List<GLandmark> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GBuildingGroup> GetDiffList(List<GBuildingGroup> beforeReshape, List<GBuildingGroup> afterReshape)
        {
            List<GBuildingGroup> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GStreetAND> GetDiffList(List<GStreetAND> beforeReshape, List<GStreetAND> afterReshape)
        {
            List<GStreetAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GPropertyAND> GetDiffList(List<GPropertyAND> beforeReshape, List<GPropertyAND> afterReshape)
        {
            List<GPropertyAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GJunctionAND> GetDiffList(List<GJunctionAND> beforeReshape, List<GJunctionAND> afterReshape)
        {
            List<GJunctionAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GBuildingAND> GetDiffList(List<GBuildingAND> beforeReshape, List<GBuildingAND> afterReshape)
        {
            List<GBuildingAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GLandmarkAND> GetDiffList(List<GLandmarkAND> beforeReshape, List<GLandmarkAND> afterReshape)
        {
            List<GLandmarkAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }

        protected List<GBuildingGroupAND> GetDiffList(List<GBuildingGroupAND> beforeReshape, List<GBuildingGroupAND> afterReshape)
        {
            List<GBuildingGroupAND> diffList, unionList, intersectList;
            if (afterReshape.Count() > beforeReshape.Count())
            {
                diffList = afterReshape.Except(beforeReshape).ToList();
            }
            else if (beforeReshape.Count() > afterReshape.Count())
            {
                diffList = beforeReshape.Except(afterReshape).ToList();
            }
            else
            {
                unionList = beforeReshape.Union(afterReshape).ToList();
                intersectList = beforeReshape.Intersect(afterReshape).ToList();
                diffList = unionList.Except(intersectList).ToList();
            }

            return diffList;
        }
    }
}

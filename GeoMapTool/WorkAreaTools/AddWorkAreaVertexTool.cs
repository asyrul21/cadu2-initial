﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class AddWorkAreaVertexTool : WorkAreaVertexTool
    {
        private enum Progress
        {
            Select = 0,
            AddVertex
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;
        private const string Message2 = "Click a point to add vertex.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _pencilHandle;
                }
            }
        }

        #endregion

        public AddWorkAreaVertexTool()
        {
            _name = "Add Work Area Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                                    if (workAreas.Count > 0)
                                    {
                                        GWorkArea workArea = workAreas[0];

                                        ValidateWorkAreaAndStatus validateUser = new ValidateWorkAreaAndStatus();
                                        validateUser.CheckUserOwnWorkArea(SegmentName, workArea);

                                        _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Start(point);
                                        List<IPoint> vertices = workArea.Vertices;
                                        for (int count = 0; count < workArea.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _workAreaVertices.Add(movePoint);
                                        }
                                        _selectedWorkArea = workArea;
                                        _progress = Progress.AddVertex;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedWorkArea = null;
                                        _selectedWorkAreaPolygon.Stop();
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.AddVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 4, 1);
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    IPolygon polygon = _selectedWorkArea.Polygon;
                                    if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                    {
                                        if (_selectedWorkArea.TryAddVertex(point))
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                _selectedWorkArea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");
                                                repo.Update(_selectedWorkArea);

                                                _selectedWorkAreaPolygon.Stop();
                                                _selectedWorkAreaPolygon.Polygon = _selectedWorkArea.Polygon;
                                                _selectedWorkAreaPolygon.Start(point);
                                                foreach (MovePoint movePoint in _workAreaVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _workAreaVertices.Clear();
                                                List<IPoint> vertices = _selectedWorkArea.Vertices;
                                                for (int count = 0; count < _selectedWorkArea.PointCount - 1; count++)
                                                {
                                                    IPoint vertex = vertices[count];
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                    movePoint.Point = vertex;
                                                    movePoint.Start(point);
                                                    _workAreaVertices.Add(movePoint);
                                                }
                                            });
                                            break;
                                        }
                                    }

                                    List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                                    if (workAreas.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _selectedWorkArea = null;
                                        _selectedWorkAreaPolygon.Stop();
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GWorkArea workArea = workAreas[0];
                                    if (!_selectedWorkArea.Equals(workArea))
                                    {
                                        _selectedWorkAreaPolygon.Stop();
                                        _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Start(point);
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        List<IPoint> vertices = workArea.Vertices;
                                        for (int count = 0; count < workArea.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _workAreaVertices.Add(movePoint);
                                        }
                                        _selectedWorkArea = workArea;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                        break;
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }


        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

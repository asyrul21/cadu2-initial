﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class CompleteWorkAreaTool : DeleteTool
    {
        private const string Message1 = "Select a Work Area to complete.";
        protected List<MovePolygon> _selectedWorkAreaPolygon;

        public CompleteWorkAreaTool()
        {
            _name = "Complete on Work Area";
            _selectedWorkAreaPolygon = new List<MovePolygon>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GWorkArea> workAreas;

                    GWorkArea workArea = null;

                    List<GBuilding> building;
                    List<GLandmark> landmark;
                    List<GBuildingGroup> buildingGroup;
                    List<GProperty> property;
                    List<GJunction> junction;
                    List<GStreet> street;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        workAreas = repo.SpatialSearch<GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (workAreas.Count > 0)
                    { 
                        workArea = workAreas[0];
                    }

                    if (workArea == null)
                    {
                        return;
                    }

                    // noraini ali - Dec 2020 - to highlight selected WorkArea 
                    highlightWorkArea(workArea, point, _selectedWorkAreaPolygon);
                    // end

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Complete Work Area")
                                  .SetText("Are you sure you want to Complete this Work Area?\nId: {0}", workArea.OID);

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        // checking if any feature assign this Work Area
                        if (!workArea.CanDelete())
                        {
                            throw new Exception("Unable to complete Work Area");
                        }

                        repo.StartTransaction(() =>
                        {
                            // feature with area ID, need to remove the area ID from feature
                            building = repo.SpatialSearch<GBuilding>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            landmark= repo.SpatialSearch<GLandmark>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            buildingGroup = repo.SpatialSearch<GBuildingGroup>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            property = repo.SpatialSearch<GProperty>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            junction = repo.SpatialSearch<GJunction>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            street = repo.SpatialSearch<GStreet>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                            int featureCount = street.Count() + property.Count() + junction.Count() + building.Count() + landmark.Count() + buildingGroup.Count();
                            Console.WriteLine(featureCount);

                            int currentCount = 0;

                            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
                            {
                                form.setMinimun(0);
                                form.setMaximum(featureCount);
                                form.Show();

                                //Remove the area ID from feature
                                building.ForEach(build =>
                                {
                                    if (string.IsNullOrEmpty(build.AndStatus.ToString()) || build.AndStatus == 0)
                                    {
                                        build.AreaId = null;
                                        //repo.UpdateRemainStatus(build,true);
                                        repo.UpdateExt(build, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                                landmark.ForEach(lnd =>
                                {
                                    if (string.IsNullOrEmpty(lnd.AndStatus.ToString()) || lnd.AndStatus == 0)
                                    {
                                        lnd.AreaId = null;
                                        //repo.UpdateRemainStatus(lnd,true);
                                        repo.UpdateExt(lnd, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                                buildingGroup.ForEach(BuildGrp =>
                                {
                                    if (string.IsNullOrEmpty(BuildGrp.AndStatus.ToString()) || BuildGrp.AndStatus == 0)
                                    {
                                        BuildGrp.AreaId = null;
                                        //repo.UpdateRemainStatus(BuildGrp,true);
                                        repo.UpdateExt(BuildGrp, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                                property.ForEach(prop =>
                                {
                                    if (string.IsNullOrEmpty(prop.AndStatus.ToString()) || prop.AndStatus == 0)
                                    {
                                        prop.AreaId = null;
                                        //repo.UpdateRemainStatus(prop,true);
                                        repo.UpdateExt(prop, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                                junction.ForEach(junc =>
                                {
                                    if (string.IsNullOrEmpty(junc.AndStatus.ToString()) || junc.AndStatus == 0)
                                    {
                                        junc.AreaId = null;
                                        //repo.UpdateRemainStatus(junc,true);
                                        repo.UpdateExt(junc, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                                street.ForEach(str =>
                                {
                                    if (string.IsNullOrEmpty(str.AndStatus.ToString()) || str.AndStatus == 0)
                                    {
                                        str.AreaId = null;
                                        //repo.UpdateRemainStatus(str,true);
                                        repo.UpdateExt(str, true);
                                        currentCount = currentCount + 1;
                                        form.setValue(currentCount);
                                    }
                                });
                            }

                            OnReported("Work Area Completed. {0}", workArea.OID);
                            repo.Delete(workArea);
                        });

                        // noraini ali - Mei 2021 - auto save 
                        using (new WaitCursor())
                        {
                            Session.Current.Cadu.Save(true);
                        }

                        OnReported(Message1);
                        OnStepReported(Message1);
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

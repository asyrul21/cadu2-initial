﻿using Earthworm.AO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
//using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI.Commands;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.Core.Oracle;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class CreateWorkAreaTool : AddTool
    {
        private IGeometry geometry;
        protected List<MovePolygon> _selectedWorkAreaPolygon;

        public CreateWorkAreaTool()
        {
            _name = "Create Work Area";
            _selectedWorkAreaPolygon = new List<MovePolygon>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnStepReported("Draw a working a area. Double click to end drawing.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);
                    geometry = new PolygonTracker().TrackNew(ActiveView, point);

                    List<GWorkArea> area;
                    List<GStreet> street;
                    List<GProperty> property;
                    List<GJunction> junction;
                    List<GBuilding> building;
                    List<GLandmark> landmark;
                    List<GBuildingGroup> buildingGroup;
                    List<GRegion> region;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        area = repo.SpatialSearch<GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();

                        //Checked for whether there is any overlap working area. if yes, then cannot create the working area.
                        if (area.Count > 0)
                        {
                            Reset();
                            throw new Exception("Area locked ! Current area is being used." + "\n" + area[0].ToString());
                        }

                        street = repo.SpatialSearch<GStreet>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        property = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        junction = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        building = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        landmark = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        buildingGroup = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        region = repo.SpatialSearch<GRegion>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();

                        int featureCount = street.Count() + property.Count() + junction.Count() + building.Count() + landmark.Count() + buildingGroup.Count();

                        //Checked if featureCOunt more than 500. if yes, then cannot create the working area.
                        if (featureCount > 500)
                        {
                            Reset();
                            throw new Exception("Please draw a smaller area. Feature count exceed 500." + "\nFeature Count = " + featureCount);
                        }

                        IList<string> regionIndex = new List<string>();
                        if (region.Count > 0)
                        {
                            region.ForEach(reg => regionIndex.Add(reg.index.Replace(" ", "_") + ".lnd"));
                        }

                        try
                        {
                            using (new WaitCursor())
                            {
                                repo.StartTransaction();
                            }
                            OnStepReported("Feature Included = " + featureCount);
                           
							// Create Work Area
                            GWorkArea newArea = repo.NewObj<GWorkArea>();
                            newArea.DateCreated1 = DateTime.Now.ToString("yyyyMMdd");
                            newArea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");
                            newArea.Shape = geometry;
                            newArea.user_id = Session.User.Name;
                            newArea.WorkAreaSegmentName = SegmentName.ToString();
                            newArea = repo.Insert(newArea, false);

                            street = repo.SpatialSearch<GStreet>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            property = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            junction = repo.SpatialSearch<GJunction>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            building = repo.SpatialSearch<GBuilding>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            landmark = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            buildingGroup = repo.SpatialSearch<GBuildingGroup>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                            int currentCount = 0;

							// Update feature to insert work area ID
                            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
                            {
                                form.setMinimun(0);
                                form.setMaximum(featureCount);
                                form.Show();

                                street.ForEach(str => {
                                    str.AreaId = newArea.OID;
                                    repo.UpdateRemainStatusByAND(str, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });

                                property.ForEach(prop => {
                                    prop.AreaId = newArea.OID;
                                    repo.UpdateRemainStatusByAND(prop, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });

                                junction.ForEach(junc => {
                                    junc.AreaId = newArea.OID;
                                    repo.UpdateRemainStatus(junc, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });

                                building.ForEach(build => {
                                    build.AreaId = newArea.OID;
                                    repo.UpdateRemainStatusByAND(build, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });
                                    
                                landmark.ForEach(lnd => {
                                    lnd.AreaId = newArea.OID;
                                    repo.UpdateRemainStatusByAND(lnd, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });

                                buildingGroup.ForEach(buildGrp => {
                                    buildGrp.AreaId = newArea.OID;
                                    repo.UpdateRemainStatusByAND(buildGrp, true);
                                    currentCount = currentCount + 1;
                                    form.setValue(currentCount);
                                });

                            }
							// Display form and submit to WOMS
							string wo_num, wo_description;
							using (UI.Forms.WorkOrderForm form = new UI.Forms.WorkOrderForm())
							{
								if (form.ShowDialog() != DialogResult.OK)
								{
									throw new ProcessCancelledException();
								}

								using (new WaitCursor())
								{
									form.SetIndexArray(regionIndex);
									wo_num = form.SubmitToWorms();
                                    wo_description = form.GetWorkOrderDescription();
                                    OnStepReported("Submitted to WOMS");
								}
							}
							// Update Work Area to insert WO Number
							newArea.Wo_no = wo_num;
                            newArea.Wo_desc = wo_description;
                            repo.Update(newArea, false);

							// Display message box
							MessageBox.Show("Work Order No : " + wo_num + "\n" + "Working Area No : " + newArea.OID);
                            repo.EndTransaction();
							// Save the current state
                            using (new WaitCursor())
                            {
                                Session.Current.Cadu.Save(true);
                            }

                            // noraini ali - Dec 2020 - to highlight selected WorkArea 
                            highlightWorkArea(newArea, point, _selectedWorkAreaPolygon);
                            // end

                            OnStepReported("Draw a working a area. Double click to end drawing.");
                        }
                        catch (Exception ex)
                        {
                            using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                            {
                                box.Show();
                            }
                            repo.AbortTransaction();
                            OnStepReported("Draw a working a area. Double click to end drawing.");
                        } 
                    }
                    InProgress = false;
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        protected override void Reset()
        {
            geometry = null;
            InProgress = false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Geomatic.Core.Features;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;

//GeoUI
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.UI.Utilities.GeometryTrackers;
using System.Drawing;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class VerifyWorkAreaTool : VerifyTool
    {
        public VerifyWorkAreaTool()
        {
            _name = "Verify Work Area";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select Work Area to Verity AND Feature.");
            OnStepReported("Select Work Area to Verity AND Feature.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            //IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;
                    IPoint point = DisplayTransformation.ToMapPoint(x, y);
    
                    RepositoryFactory repo = new RepositoryFactory(SegmentName);
                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GWorkArea> workareas;

                    GWorkArea workarea = null;

                    if (geometry == null)
                        return;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                            return;

                        workareas = repo.SpatialSearch<GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (workareas.Count == 1)
                        workarea = workareas[0];

                    // noraini ali - Jun 2020 Cadu2 AND - Only Allow work area Closed
                    if (workarea.flag != "1")
                    {
                        throw new Exception("Unable to Verify Work Area not Closed");
                    }

                    using (VerifyWorkAreaForm form = new VerifyWorkAreaForm(workarea))
                    {
                        if (form.ShowDialog() != DialogResult.OK)
                        {                           
                            return;
                        }

                        bool success = false;
                        try
                        {
                            using (new WaitCursor())
                            {
                                repo.StartTransaction();
                            }
                            success = true;
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            if (success)
                            {
                                repo.EndTransaction();
                            }
                            else
                            {
                                repo.AbortTransaction();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }

}

﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class DeleteWorkAreaVertexTool : WorkAreaVertexTool
    {
        private enum Progress
        {
            Select = 0,
            DeleteVertex
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;
        private const string Message2 = "Select a vertex to delete.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _pencilHandle;
                }
            }
        }

        #endregion

        public DeleteWorkAreaVertexTool()
        {
            _name = "Delete Work Area Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                                    if (workAreas.Count > 0)
                                    {
                                        GWorkArea workArea = workAreas[0];

                                        ValidateWorkAreaAndStatus validateUser = new ValidateWorkAreaAndStatus();
                                        validateUser.CheckUserOwnWorkArea(SegmentName, workArea);

                                        _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Start(point);
                                        List<IPoint> vertices = workArea.Vertices;
                                        for (int count = 0; count < workArea.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _workAreaVertices.Add(movePoint);
                                        }
                                        _selectedWorkArea = workArea;
                                        _progress = Progress.DeleteVertex;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedWorkArea = null;
                                        _selectedWorkAreaPolygon.Stop();
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.DeleteVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    IPolygon polygon = _selectedWorkArea.Polygon;

                                    List<GStreet> streetBeforeReshape = repo.SpatialSearch<GStreet>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GProperty> propertyBeforeReshape = repo.SpatialSearch<GProperty>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GJunction> junctionBeforeReshape = repo.SpatialSearch<GJunction>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GBuilding> buildingBeforeReshape = repo.SpatialSearch<GBuilding>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GLandmark> landmarkBeforeReshape = repo.SpatialSearch<GLandmark>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GBuildingGroup> buildingGroupBeforeReshape = repo.SpatialSearch<GBuildingGroup>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    int featureCountBeforeReshape = streetBeforeReshape.Count() + propertyBeforeReshape.Count() + junctionBeforeReshape.Count() + buildingBeforeReshape.Count() + landmarkBeforeReshape.Count() + buildingGroupBeforeReshape.Count();

                                    List<GStreetAND> streetANDBeforeReshape = repo.SpatialSearch<GStreetAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GPropertyAND> propertyANDBeforeReshape = repo.SpatialSearch<GPropertyAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GJunctionAND> junctionANDBeforeReshape = repo.SpatialSearch<GJunctionAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GBuildingAND> buildingANDBeforeReshape = repo.SpatialSearch<GBuildingAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GLandmarkAND> landmarkANDBeforeReshape = repo.SpatialSearch<GLandmarkAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    List<GBuildingGroupAND> buildingGroupANDBeforeReshape = repo.SpatialSearch<GBuildingGroupAND>(polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    int featureANDCountBeforeReshape = streetANDBeforeReshape.Count() + propertyANDBeforeReshape.Count() + junctionANDBeforeReshape.Count() + buildingANDBeforeReshape.Count() + landmarkANDBeforeReshape.Count() + buildingGroupANDBeforeReshape.Count();

                                    if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                    {
                                        if (_selectedWorkArea.PointCount <= 4)
                                        {
                                            throw new QualityControlException("Unable to remove point.");
                                        }

                                        if (_selectedWorkArea.TryDeleteVertex(hitSegmentIndex))
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                List<GStreetAND> streetANDAfterReshape = repo.SpatialSearch<GStreetAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                List<GPropertyAND> propertyANDAfterReshape = repo.SpatialSearch<GPropertyAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                List<GJunctionAND> junctionANDAfterReshape = repo.SpatialSearch<GJunctionAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                List<GBuildingAND> buildingANDAfterReshape = repo.SpatialSearch<GBuildingAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                List<GLandmarkAND> landmarkANDAfterReshape = repo.SpatialSearch<GLandmarkAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                List<GBuildingGroupAND> buildingGroupANDAfterReshape = repo.SpatialSearch<GBuildingGroupAND>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                                int featureANDCountAfterReshape = streetANDAfterReshape.Count() + propertyANDAfterReshape.Count() + junctionANDAfterReshape.Count() + buildingANDAfterReshape.Count() + landmarkANDAfterReshape.Count() + buildingGroupANDAfterReshape.Count();

                                                if (featureANDCountAfterReshape < featureANDCountBeforeReshape)
                                                {
                                                    Reset();
                                                    throw new QualityControlException("There is/are excluded feature on delete vertex! Cannot delete vertex.");
                                                }

                                                _selectedWorkArea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");
                                                repo.Update(_selectedWorkArea);
                                                _selectedWorkAreaPolygon.Stop();
                                                _selectedWorkAreaPolygon.Polygon = _selectedWorkArea.Polygon;
                                                _selectedWorkAreaPolygon.Start(point);
                                                foreach (MovePoint movePoint in _workAreaVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _workAreaVertices.Clear();
                                                List<IPoint> vertices = _selectedWorkArea.Vertices;
                                                for (int count = 0; count < _selectedWorkArea.PointCount - 1; count++)
                                                {
                                                    IPoint vertex = vertices[count];
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                    movePoint.Point = vertex;
                                                    movePoint.Start(point);
                                                    _workAreaVertices.Add(movePoint);
                                                }
                                            });

                                            List<GStreet> streetAfterReshape = repo.SpatialSearch<GStreet>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            List<GProperty> propertyAfterReshape = repo.SpatialSearch<GProperty>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            List<GJunction> junctionAfterReshape = repo.SpatialSearch<GJunction>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            List<GBuilding> buildingAfterReshape = repo.SpatialSearch<GBuilding>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            List<GLandmark> landmarkAfterReshape = repo.SpatialSearch<GLandmark>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            List<GBuildingGroup> buildingGroupAfterReshape = repo.SpatialSearch<GBuildingGroup>(_selectedWorkArea.Polygon, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                            int featureCountAfterReshape = streetAfterReshape.Count() + propertyAfterReshape.Count() + junctionAfterReshape.Count() + buildingAfterReshape.Count() + landmarkAfterReshape.Count() + buildingGroupAfterReshape.Count();

                                            //Checked if featureCOunt more than 500. if yes, then cannot create the working area.
                                            if (featureCountAfterReshape > 500)
                                            {
                                                Reset();
                                                throw new Exception("Cannot Delete Work Area Vertex! Feature count exceed 500." + "\nFeature Count = " + featureCountAfterReshape);
                                            }

                                            List<GStreet> streetDiff = GetDiffList(streetBeforeReshape, streetAfterReshape);
                                            List<GProperty> propertyDiff = GetDiffList(propertyBeforeReshape, propertyAfterReshape);
                                            List<GJunction> junctionDiff = GetDiffList(junctionBeforeReshape, junctionAfterReshape);
                                            List<GBuilding> buildingDiff = GetDiffList(buildingBeforeReshape, buildingAfterReshape);
                                            List<GLandmark> landmarkDiff = GetDiffList(landmarkBeforeReshape, landmarkAfterReshape);
                                            List<GBuildingGroup> buildingGroupDiff = GetDiffList(buildingGroupBeforeReshape, buildingGroupAfterReshape);
                                            int featureCountDiff = streetDiff.Count() + propertyDiff.Count() + junctionDiff.Count() + buildingDiff.Count() + landmarkDiff.Count() + buildingGroupDiff.Count();

                                            int currentCount = 0;

                                            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
                                            {
                                                form.setMinimun(0);
                                                form.setMaximum(featureCountDiff);
                                                form.Show();

                                                streetDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature,true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });

                                                propertyDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });

                                                junctionDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });

                                                buildingDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });

                                                landmarkDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });

                                                buildingGroupDiff.ForEach(feature =>
                                                {
                                                    if (feature.AreaId == null)
                                                    {
                                                        feature.AreaId = _selectedWorkArea.OID;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    else
                                                    {
                                                        feature.AreaId = null;
                                                        repo.UpdateRemainStatusByAND(feature, true);
                                                    }
                                                    currentCount = currentCount + 1;
                                                    form.setValue(currentCount);
                                                });
                                            }

                                            OnReported(Message2);
                                            OnStepReported(Message2);
                                        }
                                        break;
                                    }

                                    List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                                    if (workAreas.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _selectedWorkArea = null;
                                        _selectedWorkAreaPolygon.Stop();
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GWorkArea workArea = workAreas[0];
                                    if (!_selectedWorkArea.Equals(workArea))
                                    {
                                        _selectedWorkAreaPolygon.Stop();
                                        _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Start(point);
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        List<IPoint> vertices = workArea.Vertices;
                                        for (int count = 0; count < workArea.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _workAreaVertices.Add(movePoint);
                                        }
                                        _selectedWorkArea = workArea;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                        break;
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Utilities;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class DeleteWorkAreaTool : DeleteTool
    {
        private const string Message1 = "Select a Work Area to delete.";
        protected List<MovePolygon> _selectedWorkAreaPolygon;

        public DeleteWorkAreaTool()
        {
            _name = "Delete on Work Area";
            _selectedWorkAreaPolygon = new List<MovePolygon>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GWorkArea> workAreas;

                    GWorkArea workArea = null;

                    List<GBuilding> building;
                    List<GLandmark> landmark;
                    List<GBuildingGroup> buildingGroup;
                    List<GProperty> property;
                    List<GJunction> junction;
                    List<GStreet> street;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        workAreas = repo.SpatialSearch<GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (workAreas.Count > 0)
                    { 
                        workArea = workAreas[0];
                    }

                    if (workArea == null)
                    {
                        return;
                    }

                    // noraini ali - Dec 2020 - to highlight selected WorkArea 
                    highlightWorkArea(workArea, point, _selectedWorkAreaPolygon);
                    // end

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Delete Work Area")
                                  .SetText("Are you sure you want to delete this Work Area?\nId: {0}", workArea.OID);

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        // checking if any feature assign this Work Area
                        if (!workArea.CanDelete())
                        {
                            MessageBox.Show("Unable to delete Work Area.");
                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            return;
                        }

                        repo.StartTransaction(() =>
                        {
                            // feature with area ID, need to remove the area ID from feature
                            building = repo.SpatialSearch<GBuilding>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            landmark= repo.SpatialSearch<GLandmark>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            buildingGroup = repo.SpatialSearch<GBuildingGroup>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            property = repo.SpatialSearch<GProperty>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            junction = repo.SpatialSearch<GJunction>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                            street = repo.SpatialSearch<GStreet>(workArea.Shape, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                            //Remove the area ID from feature
                            building.ForEach(build => {
                                if (build.AndStatus == null)
                                {
                                    build.AreaId = null;
                                    repo.UpdateRemainStatusByAND(build, true);
                                }
                            });
                            landmark.ForEach(lnd => {
                                if (lnd.AndStatus == null)
                                {
                                    lnd.AreaId = null;
                                    repo.UpdateRemainStatusByAND(lnd, true);
                                }
                            });
                            buildingGroup.ForEach(BuildGrp => {
                                if (BuildGrp.AndStatus == null)
                                {
                                    BuildGrp.AreaId = null;
                                    repo.UpdateRemainStatusByAND(BuildGrp, true);
                                }
                            });
                            property.ForEach(prop => {
                                if (prop.AndStatus == null)
                                {
                                    prop.AreaId = null;
                                    repo.UpdateRemainStatusByAND(prop, true);
                                }
                            });
                            junction.ForEach(junc => {
                                if (junc.AndStatus == null)
                                {
                                    junc.AreaId = null;
                                    repo.UpdateRemainStatusByAND(junc, true);
                                }
                            });
                            street.ForEach(str => {
                                if (str.AndStatus == null)
                                {
                                    str.AreaId = null;
                                    repo.UpdateRemainStatusByAND(str, true);
                                }
                            });

                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            OnReported("Work Area deleted. {0}", workArea.OID);
                            repo.Delete(workArea);
                        });

                        OnReported(Message1);
                        OnStepReported(Message1);
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightPolygonClear(_selectedWorkAreaPolygon);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

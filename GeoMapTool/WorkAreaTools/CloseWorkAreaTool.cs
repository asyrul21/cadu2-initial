﻿using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Sessions.Workspaces;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class CloseWorkAreaTool : HighlightTool
    {
        private const string Message1 = "Select a Work Area to closed.";

        protected GWorkArea _selectedWorkArea;
        protected List<MovePolygon> _selectedWorkAreaPolygon;

        public CloseWorkAreaTool()
        {
            _name = "Close a Work Area";
            _selectedWorkAreaPolygon = new List<MovePolygon>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GWorkArea> workAreas;

                    GWorkArea workArea = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        workAreas = repo.SpatialSearch<GWorkArea>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (workAreas.Count > 0)
                    {
                        workArea = workAreas[0];
                    }

                    if (workArea == null)
                    {
                        return;
                    }

                    // noraini ali - Dec 2020 - to highlight selected WorkArea 
                    highlightWorkArea(workArea, point, _selectedWorkAreaPolygon);
                    // end

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        //if (workArea.flag == "1")
                        //{
                        //    MessageBox.Show("Area currently already closed.");
                        //    return;
                        //      
                        //}
                        //else
                        //{
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                      .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                      .SetCaption("Close Work Area")
                                      .SetText("Are you sure you want to Close this Work Area?\nId: {0}", workArea.OID);

                            if (confirmBox.Show() == DialogResult.No)
                            {
                                highlightPolygonClear(_selectedWorkAreaPolygon);
                                return;
                            }
                        //}
                    }

                    using (new WaitCursor())
                    {
                        if (workArea.flag == "1")
                        {
                            MessageBox.Show("Area currently already closed.");
                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            return;
                        }
                        if (workArea.flag == "2")
                        {
                            MessageBox.Show("Area locked. Currently used for verification.");
                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            return;
                        }

                        repo.StartTransaction(() =>
                        {
                            // noraini ali - Dec 2020 - to highlight selected WorkArea 
                            highlightPolygonClear(_selectedWorkAreaPolygon);
                            // end

                            workArea.flag = "1";
                            workArea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");
                            repo.Update(workArea);
                            OnReported("Work Area Closed. {0}", workArea.OID);
                        });

						// noraini - Feb 2022 - Save the current state
						using (new WaitCursor())
						{
							Session.Current.Cadu.Save(true);
						}
						OnReported(Message1);
                        OnStepReported(Message1);
                    }

                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightPolygonClear(_selectedWorkAreaPolygon);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

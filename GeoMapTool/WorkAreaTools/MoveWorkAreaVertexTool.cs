﻿using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class MoveWorkAreaVertexTool : WorkAreaVertexTool
    {
        private enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        private int _crosshairHandle;
        protected MovePoint _selectedVertex;
        protected MovePolygonPoint _movePolygonPoint;
        private const string Message2 = "Drag a vertex to move.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _crosshairHandle;
                }
            }
        }

        #endregion

        public MoveWorkAreaVertexTool()
        {
            _name = "Move Work Area Vertex";
            _crosshairHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_movePolygonPoint == null)
            {
                _movePolygonPoint = new MovePolygonPoint(ScreenDisplay, _color2);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();
                            IPolygon polygon = _selectedWorkArea.Polygon;

                            if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                            {
                                if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedWorkArea.PointCount - 1)
                                {
                                    hitSegmentIndex = 0;
                                }
                                _movePolygonPoint.Index = hitSegmentIndex;
                                _selectedVertex = _workAreaVertices[hitSegmentIndex];
                                _movePolygonPoint.Start(point);
                                _progress = Progress.Moving;
                                break;
                            }

                            List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                            if (workAreas.Count == 0)
                            {
                                _progress = Progress.Select;
                                _selectedWorkArea = null;
                                _selectedWorkAreaPolygon.Stop();
                                foreach (MovePoint movePoint in _workAreaVertices)
                                {
                                    movePoint.Stop();
                                }
                                _workAreaVertices.Clear();
                                OnReported(Message1);
                                OnStepReported(Message1);
                                break;
                            }

                            GWorkArea workArea = workAreas[0];
                            if (!_selectedWorkArea.Equals(workArea))
                            {
                                _selectedWorkAreaPolygon.Stop();
                                _movePolygonPoint.Polygon = workArea.Polygon;
                                _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                _selectedWorkAreaPolygon.Start(point);
                                foreach (MovePoint movePoint in _workAreaVertices)
                                {
                                    movePoint.Stop();
                                }
                                _workAreaVertices.Clear();

                                List<IPoint> vertices = workArea.Vertices;
                                for (int count = 0; count < workArea.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _workAreaVertices.Add(movePoint);
                                }
                                _selectedWorkArea = workArea;
                                OnReported(Message2);
                                OnStepReported(Message2);
                                break;
                            }
                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _movePolygonPoint.MoveTo(point);
                        _selectedVertex.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GWorkArea> workAreas = SelectWorkArea(repo, point);
                                    if (workAreas.Count > 0)
                                    {
                                        GWorkArea workArea = workAreas[0];

                                        ValidateWorkAreaAndStatus validateUser = new ValidateWorkAreaAndStatus();
                                        validateUser.CheckUserOwnWorkArea(SegmentName, workArea);

                                        _movePolygonPoint.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Polygon = workArea.Polygon;
                                        _selectedWorkAreaPolygon.Start(point);
                                        List<IPoint> vertices = workArea.Vertices;
                                        for (int count = 0; count < workArea.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _workAreaVertices.Add(movePoint);
                                        }
                                        _selectedWorkArea = workArea;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedWorkArea = null;
                                        _selectedWorkAreaPolygon.Stop();
                                        foreach (MovePoint movePoint in _workAreaVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _workAreaVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            using (new WaitCursor())
                            {
                                IPolygon polygon = (IPolygon)_movePolygonPoint.Stop();
                                _selectedVertex.Stop();

                                IGeometry shapeBefore = _selectedWorkArea.Shape;
                                IPolygon shapeAfter = polygon;

                                repo.StartTransaction(() =>
                                {
                                    checkForOverlapArea(repo, polygon);
                                    checkForExcludedANDFeature(repo, shapeBefore, shapeAfter);
                                    int featureCount = GetFeatureCount(shapeAfter);
                                    if (featureCount > 500)
                                    {
                                        Reset();
                                        throw new Exception("Unable to move Vertex. Feature count exceed 500." + "\nFeature Count = " + featureCount);
                                    }
                                    _selectedWorkArea.Shape = polygon;
                                    _selectedWorkArea.DateUpdated1 = DateTime.Now.ToString("yyyyMMdd");
                                    repo.Update(_selectedWorkArea);

                                    var FeaturesInWorkArea = GetFeatureDiff(shapeBefore, shapeAfter);
                                    int featureCountDiff = GetFeatureDiffCount(FeaturesInWorkArea);
                                    UpdateAreaIdForFeaturesInWorkArea(FeaturesInWorkArea, _selectedWorkArea.OID, featureCountDiff);
                                });

                                OnStepReported("Drag a vertex to move.");

                                _selectedWorkAreaPolygon.Stop();
                                _movePolygonPoint.Polygon = polygon;
                                _selectedWorkAreaPolygon.Polygon = polygon;
                                _selectedWorkAreaPolygon.Start(point);
                                foreach (MovePoint movePoint in _workAreaVertices)
                                {
                                    movePoint.Stop();
                                }
                                _workAreaVertices.Clear();
                                List<IPoint> vertices = _selectedWorkArea.Vertices;
                                for (int count = 0; count < _selectedWorkArea.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _workAreaVertices.Add(movePoint);
                                }
                                _progress = Progress.TryMove;
                                break;
                            }
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_movePolygonPoint != null)
            {
                _movePolygonPoint.Refresh(hDC);
            }
        }

        private void checkForOverlapArea(RepositoryFactory repo, IPolygon polygon)
        {
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(polygon, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
            if (area.Count > 1)
            {
                Reset();
                throw new QualityControlException("Area Overlap!");
            }
        }

        private void checkForExcludedANDFeature(RepositoryFactory repo, IGeometry shapeBefore, IPolygon shapeAfter)
        {

            List<GStreetAND> streetDiff = GetDiffList(repo.SpatialSearch<GStreetAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GStreetAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GPropertyAND> propertyDiff = GetDiffList(repo.SpatialSearch<GPropertyAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GPropertyAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GJunctionAND> junctionDiff = GetDiffList(repo.SpatialSearch<GJunctionAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GJunctionAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GBuildingAND> buildingDiff = GetDiffList(repo.SpatialSearch<GBuildingAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GBuildingAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GLandmarkAND> landmarkDiff = GetDiffList(repo.SpatialSearch<GLandmarkAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GLandmarkAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GBuildingGroupAND> buildingGroupDiff = GetDiffList(repo.SpatialSearch<GBuildingGroupAND>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(), repo.SpatialSearch<GBuildingGroupAND>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            int featureCountDiff = streetDiff.Count() + propertyDiff.Count() + junctionDiff.Count() + buildingDiff.Count() + landmarkDiff.Count() + buildingGroupDiff.Count();

            if (featureCountDiff > 0)
            {
                Reset();
                throw new QualityControlException("There is/are excluded feature on moving vertex! Cannot move vertex.");
            }
        }

        private int GetFeatureCount(IPolygon polygon)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            int street = repo.SpatialSearch<GStreet>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();
            int property = repo.SpatialSearch<GProperty>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();
            int junction = repo.SpatialSearch<GJunction>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();
            int building = repo.SpatialSearch<GBuilding>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();
            int landmark = repo.SpatialSearch<GLandmark>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();
            int buildingGroup = repo.SpatialSearch<GBuildingGroup>(polygon, esriSpatialRelEnum.esriSpatialRelContains).Count();

            return street + property + junction + building + landmark + buildingGroup;
        }

        private Tuple<List<GStreet>, List<GProperty>, List<GJunction>, List<GBuilding>, List<GLandmark>, List<GBuildingGroup>>
            GetFeatureDiff(IGeometry shapeBefore, IPolygon shapeAfter)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GStreet> street = GetDiffList(repo.SpatialSearch<GStreet>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                    repo.SpatialSearch<GStreet>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GProperty> property = GetDiffList(repo.SpatialSearch<GProperty>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                        repo.SpatialSearch<GProperty>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GJunction> junction = GetDiffList(repo.SpatialSearch<GJunction>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                        repo.SpatialSearch<GJunction>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GBuilding> building = GetDiffList(repo.SpatialSearch<GBuilding>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                        repo.SpatialSearch<GBuilding>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GLandmark> landmark = GetDiffList(repo.SpatialSearch<GLandmark>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                        repo.SpatialSearch<GLandmark>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());
            List<GBuildingGroup> buildingGroup = GetDiffList(repo.SpatialSearch<GBuildingGroup>(shapeBefore, esriSpatialRelEnum.esriSpatialRelContains).ToList(),
                                        repo.SpatialSearch<GBuildingGroup>(shapeAfter, esriSpatialRelEnum.esriSpatialRelContains).ToList());

            return new Tuple<List<GStreet>, List<GProperty>, List<GJunction>, List<GBuilding>, List<GLandmark>, List<GBuildingGroup>>
                (street, property, junction, building, landmark, buildingGroup);
        }

        private int GetFeatureDiffCount(Tuple<List<GStreet>, List<GProperty>, List<GJunction>,
            List<GBuilding>, List<GLandmark>, List<GBuildingGroup>> features)
        {
            return features.Item1.Count() + features.Item2.Count() +
                features.Item3.Count() + features.Item4.Count() +
                features.Item5.Count() + features.Item6.Count();
        }

        private void UpdateAreaIdForFeaturesInWorkArea(Tuple<List<GStreet>, List<GProperty>, List<GJunction>,
            List<GBuilding>, List<GLandmark>, List<GBuildingGroup>> features, int areaId, int featureCountDiff)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);
            int currentCount = 0;
            using (UI.Forms.ProgressForm form = new UI.Forms.ProgressForm())
            {
                form.setMinimun(0);
                form.setMaximum(featureCountDiff);
                form.Show();

                features.Item1.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });
                features.Item2.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });
                features.Item3.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });
                features.Item4.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });
                features.Item5.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });
                features.Item6.ForEach(feature =>
                {
                    if (feature.AreaId == null)
                    {
                        feature.AreaId = _selectedWorkArea.OID;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    else
                    {
                        feature.AreaId = null;
                        repo.UpdateRemainStatusByAND(feature, true);
                    }
                    currentCount = currentCount + 1;
                    form.setValue(currentCount);
                });

            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//GeoCore
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Utilities;
using Geomatic.Core.Exceptions;

//GeoUI
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;

//ESRI ArcGIS
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;

namespace Geomatic.MapTool.WorkAreaTools
{
    public class SearchWorkAreaTool : EditTool
    {
        public SearchWorkAreaTool()
        {
            _name = "Search Work Area";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a work area to view.");
            OnStepReported("Select a work area to view.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            //IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    MessageBox.Show("Searching for a work area!");
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

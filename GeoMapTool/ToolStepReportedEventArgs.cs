﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;

namespace Geomatic.MapTool
{
    public class ToolStepReportedEventArgs : EventArgs
    {
        public ITool Tool { private set; get; }
        public string Message { private set; get; }

        public ToolStepReportedEventArgs(ITool tool, string message)
        {
            Tool = tool;
            Message = message;
        }
    }
}

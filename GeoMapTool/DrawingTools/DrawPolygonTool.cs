﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI;

namespace Geomatic.MapTool.DrawingTools
{
    public class DrawPolygonTool : Tool
    {
        public DrawPolygonTool()
        {
            _name = "Draw Polygon";
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (MapDocument == null)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                if (!InProgress)
                {
                    InProgress = true;
                    try
                    {
                        IGeometry geometry = new PolygonTracker().TrackNew(ActiveView, point);

                        if (geometry == null)
                        {
                            InProgress = false;
                            return;
                        }
                        MapDocument.AddGraphic(geometry);
                        MapDocument.PartialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        InProgress = false;
                    }
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;

namespace Geomatic.MapTool.EditorTools
{
    public interface IEditorTool
    {
        void OnSelectedLayerChanged(ILayer selectedLayer);
    }
}

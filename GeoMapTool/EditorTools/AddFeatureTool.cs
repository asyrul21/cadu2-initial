﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using ESRI.ArcGIS.Display;
using Geomatic.UI.Forms.MessageBoxes;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Sessions.Workspaces;

namespace Geomatic.MapTool.EditorTools
{
    public partial class AddFeatureTool : Tool, IEditorTool
    {
        protected ILayer _selectedLayer;
        protected MovePoint _movePoint;
        protected NewLine _newLine;
        protected NewPolygon _newPolygon;

        protected bool _drawingPoint;
        protected bool _drawingLine;
        protected bool _drawingPolygon;

        public override void OnClick()
        {
            base.OnClick();

            if (_movePoint == null)
            {
                _movePoint = new MovePoint(ScreenDisplay, ColorUtils.Select, 8);
            }
            if (_newLine == null)
            {
                _newLine = new NewLine(ScreenDisplay, ColorUtils.Select, 2, esriSimpleLineStyle.esriSLSDash);
            }
            if (_newPolygon == null)
            {
                _newPolygon = new NewPolygon(ScreenDisplay, ColorUtils.Select, 2);
            }
            _drawingLine = false;
            _drawingPolygon = false;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (InProgress)
            {
                return;
            }
            if (_selectedLayer == null)
            {
                return;
            }
            IFeatureLayer featureLayer = _selectedLayer as IFeatureLayer;
            if (featureLayer == null)
            {
                return;
            }
            if (featureLayer.DataSourceType != "Shapefile Feature Class"
                && featureLayer.DataSourceType != "File Geodatabase Feature Class")
            {
                return;
            }
            if (button == MouseKey.Left)
            {
                InProgress = true;

                esriGeometryType geometryType = featureLayer.FeatureClass.ShapeType;

                switch (geometryType)
                {
                    case esriGeometryType.esriGeometryPoint:
                        try
                        {
                            IWorkspace workspace = featureLayer.FeatureClass.GetWorkspace();
                            workspace.StartTransaction(() =>
                            {
                                IFeature newFeature = featureLayer.FeatureClass.CreateFeature();
                                newFeature.Shape = point;
                                newFeature.Store();
                            });
                        }
                        catch
                        {
                            throw;
                        }
                        break;
                    case esriGeometryType.esriGeometryPolyline:
                        if (!_drawingLine)
                        {
                            _newLine.Start(point);
                            _drawingLine = true;
                        }
                        else
                        {
                            _newLine.AddPoint(point);
                            if (shift == KeyCode.Ctrl)
                            {
                                try
                                {
                                    _newLine.Stop();
                                    IWorkspace workspace = featureLayer.FeatureClass.GetWorkspace();
                                    workspace.StartTransaction(() =>
                                    {
                                        IFeature newFeature = featureLayer.FeatureClass.CreateFeature();
                                        newFeature.Shape = _newLine.Geometry;
                                        newFeature.Store();
                                    });
                                }
                                catch
                                {
                                    throw;
                                }
                                finally
                                {
                                    _drawingLine = false;
                                }
                            }
                        }
                        break;
                    case esriGeometryType.esriGeometryPolygon:
                        if (!_drawingPolygon)
                        {
                            _newPolygon.Start(point);
                            _drawingPolygon = true;
                        }
                        else
                        {
                            _newPolygon.AddPoint(point);
                            if (shift == KeyCode.Ctrl)
                            {
                                try
                                {
                                    _newPolygon.Stop();
                                    IWorkspace workspace = featureLayer.FeatureClass.GetWorkspace();
                                    workspace.StartTransaction(() =>
                                    {
                                        IFeature newFeature = featureLayer.FeatureClass.CreateFeature();
                                        newFeature.Shape = _newPolygon.Geometry;
                                        newFeature.Store();
                                    });
                                }
                                catch
                                {
                                    throw;
                                }
                                finally
                                {
                                    _drawingPolygon = false;
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
                InProgress = false;
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                _newLine.MoveTo(point);
                _newPolygon.MoveTo(point);
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        public virtual void OnSelectedLayerChanged(ILayer selectedLayer)
        {
            if (_selectedLayer != null && !_selectedLayer.Equals(selectedLayer))
            {
                if (IsActive)
                {
                    Reset();
                }
            }
            _selectedLayer = selectedLayer;
        }

        private void OnUndoLast(object sender, EventArgs e)
        {
            //if (_newLine.PointCount >= 2)
            //{
            //    _newLine.RemoveLastPoint();
            //}
        }

        private void OnRedraw(object sender, EventArgs e)
        {
            //if (!InProgress)
            //{
            //    return;
            //}
            //_newLine.Stop();
            //InProgress = false;
            //OnReported(Message1);
            //OnStepReported(Message1);
        }

        protected override void Reset()
        {
            if (_drawingLine)
            {
                if (_newLine != null)
                {
                    _newLine.Stop();
                }
                _drawingLine = false;
            }
            if (_drawingPolygon)
            {
                if (_newPolygon != null)
                {
                    _newPolygon.Stop();
                }
                _drawingPolygon = false;
            }
        }

        public override bool Deactivate()
        {
            if (_movePoint != null)
            {
                _movePoint.Stop();
                _movePoint = null;
            }
            if (_newLine != null)
            {
                _newLine.Stop();
                _newLine = null;
            }
            if (_newPolygon != null)
            {
                _newPolygon.Stop();
                _newPolygon = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_newLine != null)
            {
                _newLine.Refresh(hDC);
            }
            if (_newPolygon != null)
            {
                _newPolygon.Refresh(hDC);
            }
        }
    }
}

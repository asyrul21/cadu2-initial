﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;

namespace Geomatic.MapTool.EditorTools
{
    public class ReshapeFeatureTool : Tool, IEditorTool
    {
        protected ILayer _selectedLayer;

        public virtual void OnSelectedLayerChanged(ILayer selectedLayer)
        {
            if (_selectedLayer != null && !_selectedLayer.Equals(selectedLayer))
            {
                if (IsActive)
                {
                    Reset();
                }
            }
            _selectedLayer = selectedLayer;
        }
    }
}

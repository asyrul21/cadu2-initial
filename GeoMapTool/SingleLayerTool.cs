﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using GeoMapDocument = Geomatic.UI.Forms.Documents.MapDocument;
using Geomatic.UI.Utilities;
using System.Drawing;

namespace Geomatic.MapTool
{
    /// <summary>
    /// Tool with a layer
    /// </summary>
    public abstract class SingleLayerTool : Tool
    {
        protected ToolLayer _toolLayer;
        protected IRgbColor _layerColor = ColorUtils.Get(Color.Purple);
        protected IRgbColor _layerOutlineColor = ColorUtils.Get(Color.Purple);

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_toolLayer == null)
            {
                _toolLayer = new ToolLayer(_name, true, _layerColor, _layerOutlineColor);
            }
            _toolLayer.LayerSelectionChanged += new EventHandler(ToolLayer_SelectionChanged);
            MapDocument.AddLayer(_toolLayer);
        }

        protected virtual void ToolLayer_SelectionChanged(object sender, EventArgs e)
        {
            if (_toolLayer != null)
            {
                MapDocument.PartialRefresh(esriViewDrawPhase.esriViewGraphics, _toolLayer, ActiveView.Extent);
            }
        }
        public override void OnDocumentChanged(GeoMapDocument mapDocument)
        {
            if (MapDocument != null)
            {
                MapDocument.DeleteLayer(_toolLayer);
            }

            MapDocument = mapDocument;

            if (MapDocument != null)
            {
                MapDocument.AddLayer(_toolLayer);
            }
        }

        public override bool Deactivate()
        {
            if (_toolLayer != null)
            {
                _toolLayer.Clear();
                _toolLayer.LayerSelectionChanged -= ToolLayer_SelectionChanged;
                MapDocument.DeleteLayer(_toolLayer);
            }
            return base.Deactivate();
        }
    }
}

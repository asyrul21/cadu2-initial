﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class ReassociatePropertyTool
    {
        class Command
        {
            public const string SelectMethod = "SelectMethod";
            //public const string Polygon = "Polygon";
            //public const string Rectangle = "Rectangle";
            public const string Polygon = "Fence";
            public const string Rectangle = "Single";
        }
    }
}

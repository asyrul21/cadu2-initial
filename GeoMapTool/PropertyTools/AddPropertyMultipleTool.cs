using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Earthworm.AO;
using Geomatic.Core.Repositories.StatusUpdaters;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class AddPropertyMultipleTool : AddPropertyTool
    {
        
        private enum Progress
        {
            InitializeValues = 0,
            Add
        }

        #region Fields

        private int _arrowHandle;

        private Progress _progress;

        private AddPropertyMultipleForm.LotHouseType _lotHouseNoType;

        private int? _constructionStatus;
        private int? _type;
        private int _lotHouseNo;
        private int? _numberOfFloor;
        private int? _yearInstall;
        private string _source;
        private int _count;
        private int _amount;
        private int _increment;
        private string _prefix;
        private string _postfix;

        private GStreet maintainStreet = null;
        private GStreetAND maintainStreetAND = null;
        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                switch (_progress)
                {
                    case Progress.InitializeValues:
                        return _arrowHandle;
                    case Progress.Add:
                        return base.Cursor;
                    default:
                        throw new Exception("Invalid cursor");
                }
            }
        }

        // hides superclass values of RADIUS size
        protected new double RADIUS1 = 10;
        protected new double RADIUS2 = 50;
        protected new double RADIUS3 = 100;

        #endregion

        public AddPropertyMultipleTool()
        {
            _name = "Add Multiple Property";
            _arrowHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorArrow);
            InitMenu();
        }

        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.InitializeValues;

            InitializeValues(); 
        }

        private void InitializeValues()
        {
            using (AddPropertyMultipleForm addForm = new AddPropertyMultipleForm())
            {
                OnReported(MessageForm1);
                if (addForm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                OnReported(MessageButton1);
                _constructionStatus = addForm.SelectedConstructionStatus;
                _type = addForm.SelectedType.Value;
                _numberOfFloor = addForm.SelectedQuantity;
                _yearInstall = addForm.SelectedYearInstall;
                _source = addForm.SelectedSource;
                _lotHouseNoType = addForm.SelectedLotHouseType;
                _lotHouseNo = addForm.SelectedStartNo;
                //_amount = addForm.SelectedAmount;
                _prefix = addForm.SelectedPrefix;
                _postfix = addForm.SelectedPostfix;

                _count = 0;
                _increment = addForm.SelectedIncrement;

                _progress = Progress.Add;
            }
            maintainStreet = null;
            maintainStreetAND = null;
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.InitializeValues:
                    case Progress.Add:
                        if (!_areaCircle.IsStarted)
                        {
                            MovePolygon movePolygon1 = new MovePolygon(ScreenDisplay, _color1, _color1, 1);
                            movePolygon1.Polygon = (IPolygon)point.Buffer(RADIUS1);

                            _areaCircle.Add(movePolygon1);

                            MovePolygon movePolygon2 = new MovePolygon(ScreenDisplay, _color2, _color2, 1);
                            movePolygon2.Polygon = (IPolygon)point.Buffer(RADIUS2);

                            _areaCircle.Add(movePolygon2);

                            MovePolygon movePolygon3 = new MovePolygon(ScreenDisplay, _color3, _color3, 1);
                            movePolygon3.Polygon = (IPolygon)point.Buffer(RADIUS3);

                            _areaCircle.Add(movePolygon3);

                            _areaCircle.Start(point);
                        }

                        _areaCircle.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                OnReported(Message1);
                InProgress = true;

                switch (_progress)
                {
                    case Progress.InitializeValues:
                        if (button == MouseKey.Left)
                        {
                            InitializeValues();
                            break;
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                            break;
                        }
                        break;
                    case Progress.Add:
                        if (button == MouseKey.Left)
                        {
                            //if (_count >= _amount)
                            //{
                            //    break;
                            //}
                            //else
                            //{
                            // checking user non-AND cannot touch feature in Working Area
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                andProperty.CheckWithinWorkArea(SegmentName, point);
                                andProperty.CheckUserOwnWorkArea(SegmentName, point);
                            }
                            else
                            {
                                ANDPropertyTools andProperty = new ANDPropertyTools();
                                andProperty.checkingPointInWorkArea(SegmentName, point);
                                //if (!andProperty.checkingPointInWorkArea(SegmentName, point))
                                //{
                                //    return;
                                //}
                            }
                            // end

                            CreateProperty(point);
                            _lotHouseNo += _increment;
                            _count++;
                            if (shift == KeyCode.Ctrl)
                            {
                                _amount = _count;
                                _progress = Progress.InitializeValues;
                                break;
                            }
                            //}
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                            break;
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void CreateProperty(IPoint point)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GStreet> streets;
            GStreet street = null;

            List<GStreetAND> streetsAND;
            GStreetAND streetAND = null;

            bool isStreetinRadius = false;

            //noraini ali - search street within 50m distance from new property
            using (new WaitCursor())
            {
                streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
            }

            //noraini ali- OKT 2021
            // USER AND
            if (Session.User.GetGroup().Name == "AND")
            {
                // get Work Area Id & Checking point in Working Area & Owner
                ANDPropertyTools andProperty = new ANDPropertyTools();
                andProperty.CheckWithinWorkArea(SegmentName, point);
                andProperty.CheckUserOwnWorkArea(SegmentName, point);
                int _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                // filter street ADM & AND in this working area only
                List<GStreet> _NewListStreets = new List<GStreet>();
                List<GStreetAND> _NewListStreetsAND = new List<GStreetAND>();

                if (streets.Count > 0)
                {
                    _NewListStreets = andProperty.FilterStreetInWorkArea(streets, _workAreaId);
                }

                if (streetsAND.Count > 0)
                {
                    _NewListStreetsAND = andProperty.FilterStreetInWorkArea(streetsAND, _workAreaId);
                }

                // create new list to store all ADM feature to use in ChooseForm
                List<GStreet> _streets = new List<GStreet>();
                foreach (GStreet streetTemp in _NewListStreets)
                {
                    _streets.Add(streetTemp);
                }

                foreach (GStreetAND streetTempAND in _NewListStreetsAND)
                {
                    GStreet Street = repo.GetById<GStreet>(streetTempAND.OriId);
                    if (Street != null)
                        _streets.Add(Street);
                }

                streetAND = null;
                if (_streets.Count == 0)
                {
                    throw new Exception("No streets found within radius");
                }

                if (maintainStreet != null)
                {
                    foreach (GStreet Street in _streets)
                    {
                        if (Street.OID.ToString() == maintainStreet.OID.ToString())
                        {
                            // check if street AND exist
                            GStreetAND streetAnd = repo.GetById<GStreetAND>(maintainStreet.OID);
                            if (streetAnd != null)
                            {
                                street = maintainStreet;
                                streetAND = maintainStreetAND;
                            }
                            else
                            {
                                street = maintainStreet;
                            }
                            isStreetinRadius = true;
                            break;
                        }
                    }
                }

                if (!isStreetinRadius)
                {
                    GStreet MaintainStreet = null;
                    if (_streets.Count == 1)
                    {
                        MaintainStreet = _streets[0];
                    }
                    else //if (_streets.Count > 1)
                    {
                        OnReported(Message2);
                        OnStepReported(Message2);
                        MaintainStreet = ChooseFeature(_streets, GeometryUtils.GetNearestFeature(_streets, point));
                    }

                    // check if street AND exist
                    GStreetAND streetAnd = repo.GetById<GStreetAND>(MaintainStreet.OID);
                    if (streetAnd != null)
                    {
                        GStreet STREET = repo.GetById<GStreet>(streetAnd.OriId);
                        maintainStreet = STREET;
                        maintainStreetAND = streetAnd;
                    }
                    else
                    {
                        maintainStreet = MaintainStreet;
                        maintainStreetAND = null;
                    }
                    streetAND = maintainStreetAND;
                    street = maintainStreet;
                }

                if (street == null && streetAND == null)
                {
                    throw new Exception("No street.");
                }

                // noraini ali - OKT 2020 - hightlight feature
                if (streetAND != null)
                {
                    highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                }
                else
                {
                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                }
            }
            else // User non-AND
            {
                if (streets.Count == 0)
                {
                    throw new Exception("No streets found within radius");
                }

                if (maintainStreet != null)
                {
                    foreach (GStreet Street in streets)
                    {
                        if (Street.OID.ToString() == maintainStreet.OID.ToString())
                        {
                            isStreetinRadius = true;
                            street = maintainStreet;
                            break;
                        }
                    }
                }

                if (!isStreetinRadius)
                {
                    if (streets.Count == 1)
                    {
                        maintainStreet = streets[0];
                    }
                    else // if (streets.Count > 1)
                    {
                        OnReported(Message2);
                        OnStepReported(Message2);
                        maintainStreet = ChooseFeature(streets, GeometryUtils.GetNearestFeature(streets, point));
                    }
                    street = maintainStreet;
                }

                if (street == null)
                {
                    throw new Exception("No street.");
                }

                highlightStreet(repo, street.OID, true, _selectedFeatureLines);
            }

            using (new WaitCursor())
            {
                repo.StartTransaction(() =>
                {
                    GProperty newProperty = repo.NewObj<GProperty>();
                    newProperty.Init();
                    newProperty.ConstructionStatus = _constructionStatus;
                    newProperty.Type = _type;
                    newProperty.NumberOfFloor = _numberOfFloor;
                    newProperty.YearInstall = _yearInstall;
                    newProperty.Source = _source;
                    string lotHouse = string.Format("{0}{1}{2}", _prefix, _lotHouseNo, _postfix);

                    if (_lotHouseNoType == AddPropertyMultipleForm.LotHouseType.Lot)
                    {
                        newProperty.Lot = lotHouse;
                    }
                    else
                    {
                        newProperty.House = lotHouse;
                    }
                    if (streetAND != null)
                    {
                        newProperty.StreetId = streetAND.OriId;
                    }
                    else
                    {
                        newProperty.StreetId = street.OID;
                    }
                    newProperty.Shape = point;

                    //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        andProperty.UpdateCreatedByUserAND(newProperty);
                        andProperty.UpdateModifiedByUserAND(newProperty);
                    }

                    newProperty = repo.Insert(newProperty);
                    //new PropertyStatusUpdater().CreateLog(newProperty);

                    //noraini ali- April 2020
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        //andProperty.CheckWithinWorkArea(SegmentName, newProperty);
                        //andProperty.CheckUserOwnWorkArea(SegmentName, newProperty);

                        //andProperty.Insert(repo, newProperty);
                        andProperty.AddPropertyAND(repo, newProperty);
                    }
                    // end

                    OnReported("New property created. {0}", newProperty.OID);
                    OnReported(Message1 + " or right-click for end.");
                    OnStepReported(Message1 + " or right-click for end.");
                });
            }
        }

        //protected override void CreateProperty(IPoint point)
        //{
        //    RepositoryFactory repo = new RepositoryFactory(SegmentName);
        //
        //    List<GStreet> streets;
        //    GStreet street = null;
        //
        //    using (new WaitCursor())
        //    {
        //        streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
        //    }
        //
        //    if (streets.Count == 0)
        //    {
        //        using (new WaitCursor())
        //        {
        //            streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
        //        }
        //    }
        //
        //    if (streets.Count == 0)
        //    {
        //        using (new WaitCursor())
        //        {
        //            streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
        //        }
        //    }
        //
        //    if (streets.Count == 1)
        //    {
        //        street = streets[0];
        //    }
        //    else if (streets.Count > 1)
        //    {
        //        OnReported(Message2);
        //        OnStepReported(Message2);
        //        street = ChooseFeature(streets, GeometryUtils.GetNearestFeature(streets, point));
        //    }
        //    else if (streets.Count == 0)
        //    {
        //        throw new Exception("No streets found within radius");
        //    }
        //
        //    if (street == null)
        //    {
        //        throw new Exception("No street.");
        //    }
        //
        //    using (new WaitCursor())
        //    {
        //        repo.StartTransaction(() =>
        //        {
        //            GProperty newProperty = repo.NewObj<GProperty>();
        //            newProperty.Init();
        //            newProperty.ConstructionStatus = _constructionStatus;
        //            newProperty.Type = _type;
        //            newProperty.NumberOfFloor = _numberOfFloor;
        //            newProperty.YearInstall = _yearInstall;
        //            newProperty.Source = _source;
        //            string lotHouse = string.Format("{0}{1}{2}", _prefix, _lotHouseNo, _postfix);
        //
        //            if (_lotHouseNoType == AddPropertyMultipleForm.LotHouseType.Lot)
        //            {
        //                newProperty.Lot = lotHouse;
        //            }
        //            else
        //            {
        //                newProperty.House = lotHouse;
        //            }
        //
        //            newProperty.StreetId = street.OID;
        //            newProperty.Shape = point;
        //
        //            //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
        //            if (Session.User.GetGroup().Name == "AND")
        //            {
        //                ANDPropertyTools andProperty = new ANDPropertyTools();
        //                andProperty.UpdateCreatedByUserAND(newProperty);
        //                andProperty.UpdateModifiedByUserAND(newProperty);
        //            }
        //
        //            newProperty = repo.Insert(newProperty);
        //
        //            //noraini ali- April 2020
        //            if (Session.User.GetGroup().Name == "AND")
        //            {
        //                ANDPropertyTools andProperty = new ANDPropertyTools();
        //                andProperty.CheckWithinWorkArea(SegmentName, newProperty);
        //                andProperty.CheckUserOwnWorkArea(SegmentName, newProperty);
        //
        //                //andProperty.Insert(repo, newProperty);
        //                andProperty.AddPropertyAND(repo, newProperty);
        //            }
        //            // end
        //
        //            OnReported("New property created. {0}", newProperty.OID);
        //            OnReported(Message1 + " or right-click for end.");
        //            OnStepReported(Message1 + " or right-click for end.");
        //        });
        //    }
        //}

        private void OnEnd(object sender, EventArgs e)
        {
            _progress = Progress.InitializeValues;
        }

        public override bool Deactivate()
        {
            _progress = Progress.InitializeValues;

            return base.Deactivate();
        }
    }
}

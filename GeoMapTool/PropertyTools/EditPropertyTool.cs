﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.Core;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Earthworm.AO;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.PropertyTools
{
    public class EditPropertyTool : EditTool
    {
        #region Fields
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        protected const string Message1 = "Select a property to edit.";
        protected const string Message2 = "Clicked property location on map.";
        protected const string MessageForm1 = "Opening form completed";
        protected const string MessageButton1 = "Add button clicked";
        protected const string MessageButton2 = "Edit button clicked";
        protected const string MessageButton3 = "Delete button clicked";
        protected const string MessageButton4 = "Apply button edit property form clicked";
        protected const string MessageButton5 = "Apply button format address form clicked";
        protected const string MessageStreet = "Street selected";
        #endregion

        public EditPropertyTool()
        {
            _name = "Edit Property";

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    IPoint point = DisplayTransformation.ToMapPoint(x, y);
                    OnReported(Message2);

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GProperty> properties;
                    GProperty property = null;

                    // added by noraini ali - Mei 2020
                    GPropertyAND propertyAND = null;
                    List<GPropertyAND> propertiesAND;
                    propertiesAND = null;
                    // end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        properties = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        propertiesAND = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (properties.Count == 0 && propertiesAND.Count == 0)
                    {
                        return;
                    }

                    int _workAreaId = 0;
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id 
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                        if (_workAreaId > 0)
                        {
                            List<GProperty> _NewListproperties = new List<GProperty>();
                            List<GPropertyAND> _NewListpropertiesAND = new List<GPropertyAND>();

                            if (properties.Count > 0)
                            {
                                _NewListproperties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                            }

                            if (propertiesAND.Count > 0)
                            {
                                _NewListpropertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                            }

                            //Assign new list ADM 
                            List<GProperty> _properties = new List<GProperty>();
                            foreach (GProperty propertyTemp in _NewListproperties)
                            {
                                _properties.Add(propertyTemp);
                            }

                            foreach (GPropertyAND propertyTempAND in _NewListpropertiesAND)
                            {
                                GProperty Property = repo.GetById<GProperty>(propertyTempAND.OriId);
                                if (Property != null)
                                    _properties.Add(Property);
                            }

                            propertyAND = null;
                            if (_properties.Count == 0)
                            {
                                return;
                            }
                            else if (_properties.Count == 1)
                            {
                                property = _properties[0];
                                propertyAND = null;

                                // if AND feature exist
                                GPropertyAND PropertyTempAND = property.GetPropertyANDId();
                                if (PropertyTempAND != null)
                                {
                                    propertyAND = PropertyTempAND;
                                    property = null;
                                }
                                // noraini ali - OKT 2020 - hightlight feature
                                highlightProperty_hasBuilding(repo, property, propertyAND, _selectedFeaturePoints);
                                highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines);
                            }
                            else
                            {
                                property = ChooseFeature(_properties, null);
                                propertyAND = null;

                                // if OID exist in AND feature
                                GPropertyAND PropertyTempAND = repo.GetById<GPropertyAND>(property.OID);
                                if (PropertyTempAND != null)
                                {
                                    propertyAND = PropertyTempAND;
                                    property = null;
                                }
                                // noraini ali - OKT 2020 - hightlight feature
                                highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines);
                            }
                        } 
                        else // feature not in work area
                        {
                            property = null;
                            propertyAND = null;

                            if (properties.Count > 0)
                            {
                                if (propertiesAND.Count == 0)
                                {
                                    property = properties[0];
                                }
                            }
                            else if (properties.Count == 0)
                            {
                                if (propertiesAND.Count > 0)
                                {
                                    propertyAND = propertiesAND[0];
                                }
                            }

                            // noraini ali - OKT 2020 - hightlight feature
                            highlightProperty_hasBuilding(repo, property, propertyAND, _selectedFeaturePoints);
                            highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines);
                        }
                    }
                    // end
                    else  //OTHERS USER
                    {
                        // checking user non-AND cannot touch feature in Working Area
                        ANDPropertyTools andProperty = new ANDPropertyTools();

                        propertyAND = null; 
                        if (properties.Count > 0)
                        {
                            property = properties[0];
                            // noraini ali - OKT 2020 - hightlight feature
                            highlightProperty_hasBuilding(repo, property, propertyAND, _selectedFeaturePoints);
                            highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines);
                        }

                        //else if (properties.Count > 1)
                        //{
                        //    property = ChooseFeature(properties, null);
                        //    property = properties[0];
                        //    highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines);
                        //}

                        if (property == null)
                        {
                            return;
                        }
                    }

                    bool success = false;

                    Console.WriteLine("\nProperty: " + property);

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        //added by noraini ali -Mei 2020
                        // checking feature can edit or review only
                        bool _CanEdit = false;
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        if (_CanEdit = andProperty.ValidateUserCanEdit(SegmentName, _workAreaId))
                        {
                            if (property != null)
                            {
                                andProperty.Update(repo, property);
                                OnReported(andProperty.MessageButtonClick());
                                OnReported("Property updated. {0}", property.OID);
                                OnReported(Message1);
                                success = true;
                            }
                            else
                            {
                                andProperty.Update(repo, propertyAND);
                                OnReported(andProperty.MessageButtonClick());
                                OnReported("Property updated. {0}", propertyAND.OID);
                                OnReported(Message1);
                                success = true;
                            }
                        }
                        // end
                        else
                        {
                            // noraini ali - NOV 2020
                            if (property == null)
                            {
                                andProperty.Update(repo, propertyAND);
                                OnReported(andProperty.MessageButtonClick());
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }

                            using (EditPropertyForm form = new EditPropertyForm(property))
                            {
                                OnReported(MessageForm1);
                                if (form.ShowDialog() != DialogResult.OK)
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                OnReported(form.MessageButtonClick());
                                OnReported(MessageButton4);

                                // noraini ali - Apr 2021 - check feature lock by user AND
                                andProperty.CheckFeatureLock(property.AreaId, SegmentName);

                                // noraini ali - Mei 2021 - check feature on verification
                                andProperty.CheckFeatureOnVerification(property);

                                using (new WaitCursor())
                                {
                                    form.SetValues();
                                    repo.Update(property);
                                }
                            }

                            OnReported("Property updated. {0}", property.OID);
                            OnReported(Message1);
                            success = true;
                        }
                        //noraini ali - OKT 2020 - Clear Highlight feature
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                    catch
                    {
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                 
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class AddPropertyMultipleTool 
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuEnd;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuEnd = new ToolStripMenuItem();
            // 
            // menuEnd
            //
            this.menuEnd.Name = "menuEnd";
            this.menuEnd.Text = "End";
            this.menuEnd.Click += new EventHandler(this.OnEnd);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuEnd});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

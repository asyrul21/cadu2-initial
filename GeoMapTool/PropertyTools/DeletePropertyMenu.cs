﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class DeletePropertyTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuRectangle;
        private ToolStripMenuItem menuPolygon;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuRectangle = new ToolStripMenuItem();
            this.menuPolygon = new ToolStripMenuItem();
            // 
            // menuRectangle
            //
            this.menuRectangle.Name = "menuRectangle";
            //this.menuRectangle.Text = "Rectangle";
            this.menuRectangle.Text = "Single";
            this.menuRectangle.Click += new EventHandler(this.OnRectangle);
            // 
            // menuPolygon
            //
            this.menuPolygon.Name = "menuPolygon";
            //this.menuPolygon.Text = "Polygon";
            this.menuPolygon.Text = "Fence";
            this.menuPolygon.Click += new EventHandler(this.OnPolygon);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] {
            this.menuRectangle,
            this.menuPolygon});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

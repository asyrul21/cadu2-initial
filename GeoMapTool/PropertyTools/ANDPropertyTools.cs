﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using System.Text;
using System;

namespace Geomatic.MapTool.PropertyTools
{
    // Implementation - noraini ali - Apr 2021- Apply STATUS & AND_STATUS - Data Intergration to NEPS

    public class ANDPropertyTools : ValidateWorkAreaAndStatus
    {
        StringBuilder sb = new StringBuilder();

        public void CreatePropertyAND(RepositoryFactory repo, GProperty Property)
        {
            GProperty property = repo.GetById<GProperty>(Property.OID);
            GPropertyAND PropertyAND = repo.NewObj<GPropertyAND>();
            PropertyAND.CopyFrom(property);
            repo.Insert(PropertyAND, false);
        }

        public void AddPropertyAND(RepositoryFactory repo, GProperty Property)
        {
            // update property ADM - Area ID
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(Property.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => Property.AreaId = thearea.OID);
            repo.UpdateInsertByAND(Property, true);

            // create new property AND as copy from ADM property 
            CreatePropertyAND(repo, Property);

            // update property AND - CreatedBy, DateCreated, Updatedby & DateUpdated
            GPropertyAND PropertyAND = Property.GetPropertyANDId();
            UpdateCreatedByUserAND(PropertyAND);
            UpdateModifiedByUserAND(PropertyAND);
            repo.UpdateRemainStatus(PropertyAND, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GProperty Property)
        {
            GPropertyAND absObjAND = Property.GetPropertyANDId();
            if (absObjAND == null)
            {
                // Create Property AND if not exist
                CreatePropertyAND(repo, Property);
            }

            // Update property AND - User info,STATUS & AND_STATUS as Delete status
            GPropertyAND PropertyAND = Property.GetPropertyANDId();
            UpdateModifiedByUserAND(PropertyAND);
            repo.UpdateDeleteByAND(PropertyAND, true);

            // update property ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(Property, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GPropertyAND PropertyAND)
        {
            // Update property ADM - AND_STATUS & STATUS as Delete status
            GProperty Property = repo.GetById<GProperty>(PropertyAND.OriId);
            repo.UpdateDeleteByAND(Property, true);

            // Update property AND -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(PropertyAND);
            repo.UpdateDeleteByAND(PropertyAND, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GProperty Property)
        {
            GPropertyAND absObjAND = Property.GetPropertyANDId();
            if (absObjAND == null)
            {
                // Create Property AND
                CreatePropertyAND(repo, Property);
            }

            //Get property AND 
            GPropertyAND PropertyAND = Property.GetPropertyANDId();

            //Display form 
            using (EditPropertyANDForm form = new EditPropertyANDForm(PropertyAND))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append(form.MessageButtonClick());
                sb.Append("\n");
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - 
                    // AreadId, Updatedby & DateUpdated
                    List<GWorkArea> area;
                    area = repo.SpatialSearch<GWorkArea>(Property.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
                    area.ForEach(thearea => PropertyAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(PropertyAND);
                    repo.Update(PropertyAND);

                    // Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => Property.AreaId = thearea.OID);
                    repo.UpdateByAND(Property, true);

                    // update work area flag as open 
                    int AreaID = PropertyAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GPropertyAND PropertyAND)
        {
            GProperty Property = repo.GetById<GProperty>(PropertyAND.OriId);
            if (Property == null)
            {
                throw new QualityControlException("Cannot proceed, property ADM not found.");
            }

            //Display form 
            using (EditPropertyANDForm form = new EditPropertyANDForm(PropertyAND))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append(form.MessageButtonClick());
                sb.Append("\n");
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(PropertyAND);
                    repo.Update(PropertyAND, true);

                    // update ADM - STATUS & AND_STATUS to update status
                    repo.UpdateByAND(Property, true);

                    // update work area flag as open 
                    int AreaID = PropertyAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void UpdateMultiple(RepositoryFactory repo, List<GProperty> features)
        {
            int AreaID = 0;

            // create list AND Property
            List<GPropertyAND> propertiesAND = new List<GPropertyAND>();

            foreach (GProperty Property in features)
            {
                //create AND property if not exist
                GPropertyAND absObjAND = Property.GetPropertyANDId();
                if (absObjAND == null)
                {
                    CreatePropertyAND(repo, Property);
                }

                // Add AND Propery in the list
                GPropertyAND PropertyAND = Property.GetPropertyANDId();
                propertiesAND.Add(PropertyAND);
            }

            //Display form and update AND Table
            using (EditPropertyMultipleANDForm form = new EditPropertyMultipleANDForm(propertiesAND))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
                {
                    form.SetValues();
                    foreach (GPropertyAND propertyAND in propertiesAND)
                    {
                        GProperty Property = repo.GetById<GProperty>(propertyAND.OriId);

                        // Update AND Feature - Updatedby, DateUpdated, STATUS & AND_STATUS as Updated status
                        UpdateModifiedByUserAND(propertyAND);
                        repo.Update(propertyAND, true);

                        // Update ADM Feature - STATUS & AND_STATUS as Updated status
                        repo.UpdateByAND(Property, true);
                        AreaID = propertyAND.AreaId ?? 0;
                    }

                    // update work area flag as open 
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void UpdateMultiple(RepositoryFactory repo, List<GPropertyAND> features)
        {
            int AreaID = 0;

            //Display form and update AND Table
            using (EditPropertyMultipleANDForm form = new EditPropertyMultipleANDForm(features))
            {
                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Opening form completed");
                sb.Append("\n");

                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                sb.Append("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "] " + "Apply button form clicked.");
                sb.Append("\n");

                using (new WaitCursor())
                {
                    form.SetValues();
                    foreach (GPropertyAND PropertyAND in features)
                    {
                        GProperty Property = repo.GetById<GProperty>(PropertyAND.OriId);

                        // Update AND Feature - Updatedby & DateUpdated, STATUS & AND_STATUS as Updated status
                        UpdateModifiedByUserAND(PropertyAND);
                        repo.Update(PropertyAND, true);

                        // Update ADM Feature - STATUS & AND_STATUS as Updated status
                        repo.UpdateByAND(Property, true);

                        AreaID = PropertyAND.AreaId ?? 0;
                    }

                    // update work area flag as open 
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GProperty Property, IPoint newPoint)
        {
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Property Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != Property.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // if AND property not exist so create AND Property 
            GPropertyAND absObjAND = Property.GetPropertyANDId();
            if (absObjAND == null)
            {
                // Create Property AND
                CreatePropertyAND(repo, Property);
            }

            // set new location for AND Property
            GPropertyAND PropertyAND = Property.GetPropertyANDId();
            PropertyAND.Shape = newPoint;
            UpdateModifiedByUserAND(PropertyAND);
            repo.UpdateGraphic(PropertyAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(Property, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GPropertyAND PropertyAND, IPoint newPoint)
        {
            GProperty Property = repo.GetById<GProperty>(PropertyAND.OriId);
            if (Property == null)
            {
                throw new QualityControlException("Cannot proceed, property ADM not found.");
            }

            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Property Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != PropertyAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // set new location for AND Property & STATUS & AND_STATUS (Update Graphic)
            PropertyAND.Shape = newPoint;
            UpdateModifiedByUserAND(PropertyAND);
            repo.UpdateGraphic(PropertyAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Property, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GProperty Property, int streetId)
        {
            GPropertyAND absObjAND = Property.GetPropertyANDId();
            if (absObjAND == null)
            {
                // Create Property AND
                CreatePropertyAND(repo, Property);
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            GPropertyAND PropertyAND = Property.GetPropertyANDId();
            PropertyAND.StreetId = streetId;
            UpdateModifiedByUserAND(PropertyAND);
            repo.Update(PropertyAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Property, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GPropertyAND PropertyAND, int streetId)
        {
            GProperty Property = repo.GetById<GProperty>(PropertyAND.OriId);
            if (Property == null)
            {
                throw new QualityControlException("Cannot proceed, property ADM not found.");
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            PropertyAND.StreetId = streetId;
            UpdateModifiedByUserAND(PropertyAND);
            repo.Update(PropertyAND, true);

            //repo.UpdateByAND(PropertyAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Property, true);

            // update work area flag as open 
            int AreaID = PropertyAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        // noraini ali - OKT 2020
        public List<GProperty> FilterPropertyInWorkArea(List<GProperty> _propertys, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GProperty> properties = new List<GProperty>();
            foreach (GProperty _propertyTemp in _propertys)
            {
                if (_propertyTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_propertyTemp.AndStatus.ToString()) || (_propertyTemp.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_propertyTemp.AreaId.ToString()) || (_propertyTemp.AreaId > 0))
                        {
                            if (_propertyTemp.AreaId.Value == _workareaid)
                            {
                                properties.Add(_propertyTemp);
                            }
                        }
                    }
                }
            }
            return properties;
        }

        public List<GPropertyAND> FilterPropertyInWorkArea(List<GPropertyAND> _properties, int _workareaid)
        {
            // filter ADM Property - get only fresh ADM
            List<GPropertyAND> propertiesAND = new List<GPropertyAND>();
            foreach (GPropertyAND _propertyTemp in _properties)
            {
                if (_propertyTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_propertyTemp.AreaId.ToString()) || _propertyTemp.AreaId > 0)
                    {
                        if (_propertyTemp.AreaId.Value == _workareaid)
                        {
                            propertiesAND.Add(_propertyTemp);
                        }
                    }
                }
            }
            return propertiesAND;
        }

        public void CheckFeatureOnVerification(GProperty prop)
        {
            GPropertyAND PropAND = prop.GetPropertyANDId();
            if (PropAND != null)
            {
                throw new QualityControlException("Unable to edit feature without verification.");
            }
        }

        public List<GStreet> FilterStreetInWorkArea(List<GStreet> _streets, int _workareaid)
        {
            // filter ADM Street - get only fresh ADM
            List<GStreet> streets = new List<GStreet>();
            foreach (GStreet _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_streetTemp.AndStatus.ToString()) || (_streetTemp.AndStatus == 0))
                    {
                        if ((!string.IsNullOrEmpty(_streetTemp.AreaId.ToString())) || _streetTemp.AreaId > 0)
                        {
                            if (_streetTemp.AreaId.Value == _workareaid)
                            {
                                streets.Add(_streetTemp);
                            }
                        }
                    }
                }
            }
            return streets;
        }

        public List<GStreetAND> FilterStreetInWorkArea(List<GStreetAND> _streets, int _workareaid)
        {
            // filter AND Street only in this working area
            List<GStreetAND> streetsAND = new List<GStreetAND>();
            foreach (GStreetAND _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_streetTemp.AreaId.ToString()) || _streetTemp.AreaId > 0)
                    {
                        if (_streetTemp.AreaId.Value == _workareaid)
                        {
                            streetsAND.Add(_streetTemp);
                        }
                    }
                }
            }
            return streetsAND;
        }

        public string MessageButtonClick()
        {
            return "\n" + sb.ToString();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.Utilities;
using System.Drawing;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.PropertyTools
{
    public class EditPropertyMultipleTool : EditTool
    {
        #region Fields

        private const string Message1 = "Select multiple properties to edit.";
        protected List<MovePoint> _propertiesT;
        #endregion

        public EditPropertyMultipleTool()
        {
            _name = "Edit Multiple Property";
            _propertiesT = new List<MovePoint>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    IPoint point = DisplayTransformation.ToMapPoint(x, y);

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    // noraini ali - OKT 2020
                    // get Work Area Id & Checking point in Working Area & Owner
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        andProperty.CheckWithinWorkArea(SegmentName, point);
                        andProperty.CheckUserOwnWorkArea(SegmentName, point);
                    }

                    IGeometry geometry = new PolygonTracker().TrackNew(ActiveView, point);

                    List<GProperty> properties;
                    List<GPropertyAND> propertiesAND;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        properties = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        propertiesAND = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    if (properties.Count == 0 && propertiesAND.Count == 0)
                    {
                        return;
                    }

                    bool success = false;

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        // noraini ali - Apr 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            // get Work Area Id & Checking point in Working Area & Owner
                            ANDPropertyTools andProperty = new ANDPropertyTools();
                            //andProperty.CheckWithinWorkArea(SegmentName, point);
                            //andProperty.CheckUserOwnWorkArea(SegmentName, point);
                            int _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                            if (_workAreaId > 0)
                            {
                                List<GProperty> _NewListproperties = new List<GProperty>();
                                List<GPropertyAND> _NewListpropertiesAND = new List<GPropertyAND>();

                                if (properties.Count > 0)
                                {
                                    _NewListproperties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                                }

                                if (propertiesAND.Count > 0)
                                {
                                    _NewListpropertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                                }

                                //Assign new list ADM 
                                List<GProperty> _properties = new List<GProperty>();
                                foreach (GProperty property in _NewListproperties)
                                {
                                    _properties.Add(property);
                                }
                                foreach (GPropertyAND property in _NewListpropertiesAND)
                                {
                                    GProperty Property = repo.GetById<GProperty>(property.OriId);
                                    if (Property != null)
                                        _properties.Add(Property);
                                }

                                if (_properties.Count == 0)
                                {
                                    return;
                                }

                                // noraini ali - highlight all selected properties
                                foreach (MovePoint movePoint in _propertiesT)
                                {
                                    movePoint.Stop();
                                }
                                _propertiesT.Clear();

                                foreach (GProperty property in _properties)
                                {
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, POINT_SIZE);
                                    movePoint.Point = property.Point;
                                    movePoint.Start(point);
                                    _propertiesT.Add(movePoint);
                                }

                                andProperty.UpdateMultiple(repo, _properties);
                                OnReported(andProperty.MessageButtonClick());
                            }

                        }
                        // end
                        else  // OTHERS GROUP
                        {
                            // checking user non-AND cannot touch feature in Working Area
                            ANDPropertyTools andProperty = new ANDPropertyTools();

                            using (EditPropertyMultipleForm form = new EditPropertyMultipleForm(properties))
                            {
                                if (form.ShowDialog() != DialogResult.OK)
                                {
                                    return;
                                }

                                // noraini ali - Jun 2020 - not allow to edit feature in working area for others groups except AND & Supervisor.
                                // noraini ali - Mac 2021 - Add rule - not to allow MLI(Supervisor) to delete feature under verification
                                foreach (GProperty property in properties)
                                {
                                    // noraini ali - Apr 2021 - check feature lock by user AND
                                    andProperty.CheckFeatureLock(property.AreaId, SegmentName);

                                    // noraini ali - Mei 2021 - check feature on verification
                                    andProperty.CheckFeatureOnVerification(property);
                                }

                                using (new WaitCursor())
                                {
                                    form.SetValues();
                                    foreach (GProperty property in properties)
                                    {
                                        repo.Update(property);
                                        OnReported("Property updated. {0}", property.OID);
                                    }
                                }
                            }
                        }
                        OnReported(Message1);
                        OnStepReported(Message1);
                        success = true;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

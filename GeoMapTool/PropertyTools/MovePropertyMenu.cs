﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class MovePropertyTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuRectangle;
        private ToolStripMenuItem menuPolygon;
        private ToolStripMenuItem menuCircle;

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuRectangle = new ToolStripMenuItem();
            this.menuPolygon = new ToolStripMenuItem();
            this.menuCircle = new ToolStripMenuItem();
            // 
            // menuRectangle
            //
            this.menuRectangle.Name = "menuRectangle";
            //this.menuRectangle.Text = "Rectangle";
            this.menuRectangle.Text = "Single";
            this.menuRectangle.Click += new EventHandler(this.OnRectangle);
            // 
            // menuPolygon
            //
            this.menuPolygon.Name = "menuPolygon";
            //this.menuPolygon.Text = "Polygon";
            this.menuPolygon.Text = "Fence";
            this.menuPolygon.Click += new EventHandler(this.OnPolygon);
            // 
            // menuCircle
            //
            this.menuCircle.Name = "menuCircle";
            this.menuCircle.Text = "Circle";
            this.menuCircle.Click += new EventHandler(this.OnCircle);
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] {
            this.menuRectangle,
            this.menuPolygon,
            this.menuCircle});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Rows;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Earthworm.AO;
using Geomatic.Core.Sessions;

namespace Geomatic.MapTool.PropertyTools
{
    public class AddPropertyTool : AddTool
    {
        #region Fields

        protected FeedBackCollection _areaCircle;

        protected IRgbColor _color3 = ColorUtils.Get(Color.Red);
        protected const double RADIUS1 = 20;
        protected const double RADIUS2 = 100;
        protected const double RADIUS3 = 200;

        protected const string Message1 = "Click on map location to add property.";
        protected const string Message2 = "Select a street from the list for property association.";
        protected const string Message3 = "Clicked property location on map.";
        protected const string MessageForm1 = "Opening form completed";
        protected const string MessageButton1 = "Button accept clicked";
        protected const string MessageStreet = "Street selected";

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        #endregion

        public AddPropertyTool()
        {
            _name = "Add Property";

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_areaCircle == null)
            {
                _areaCircle = new FeedBackCollection();
            }

            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                if (!_areaCircle.IsStarted)
                {
                    MovePolygon movePolygon1 = new MovePolygon(ScreenDisplay, _color1, _color1, 1);
                    movePolygon1.Polygon = (IPolygon)point.Buffer(RADIUS1);

                    _areaCircle.Add(movePolygon1);

                    MovePolygon movePolygon2 = new MovePolygon(ScreenDisplay, _color2, _color2, 1);
                    movePolygon2.Polygon = (IPolygon)point.Buffer(RADIUS2);

                    _areaCircle.Add(movePolygon2);

                    MovePolygon movePolygon3 = new MovePolygon(ScreenDisplay, _color3, _color3, 1);
                    movePolygon3.Polygon = (IPolygon)point.Buffer(RADIUS3);

                    _areaCircle.Add(movePolygon3);

                    _areaCircle.Start(point);
                }

                _areaCircle.MoveTo(point);
            }

            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                OnReported(Message3);
                InProgress = true;

                if (button == MouseKey.Left)
                {
                    CreateProperty(point);
                }
                else if (button == MouseKey.Right)
                {
                    MapDocument.ShowContextMenu(x, y);
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        protected virtual void CreateProperty(IPoint point)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GStreet> streets;
            GStreet street = null;

            List<GStreetAND> streetsAND;
            GStreetAND streetAND = null;

            using (new WaitCursor())
            {
                streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
            }

            if (streets.Count == 0)
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }

            if (streets.Count == 0)
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }
            
            //noraini ali- OKT 2020 
            // USER AND
            if (Session.User.GetGroup().Name == "AND")
            {
                // get Work Area Id & Checking point in Working Area & Owner
                ANDPropertyTools andProperty = new ANDPropertyTools();
                andProperty.CheckWithinWorkArea(SegmentName, point);
                andProperty.CheckUserOwnWorkArea(SegmentName, point);
                int _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                // filter street ADM & AND in this working area only
                List<GStreet> _NewListStreets = new List<GStreet>();
                List<GStreetAND> _NewListStreetsAND = new List<GStreetAND>();

                if (streets.Count > 0)
                {
                    _NewListStreets = andProperty.FilterStreetInWorkArea(streets, _workAreaId);
                }

                if (streetsAND.Count > 0)
                {
                    _NewListStreetsAND = andProperty.FilterStreetInWorkArea(streetsAND, _workAreaId);
                }

                // create new list to store all ADM feature to use in ChooseForm
                List<GStreet> _streets = new List<GStreet>();
                foreach (GStreet streetTemp in _NewListStreets)
                {
                    _streets.Add(streetTemp);
                }

                foreach (GStreetAND streetTempAND in _NewListStreetsAND)
                {
                    GStreet Street = repo.GetById<GStreet>(streetTempAND.OriId);
                    if (Street != null)
                        _streets.Add(Street);
                }

                streetAND = null;
                if (_streets.Count == 0)
                {
                    throw new Exception("No streets found within radius in this Working  Area");
                }
                else if (_streets.Count == 1)
                {
                    street = _streets[0];
                }
                else // _streets.count > 1
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(_streets, GeometryUtils.GetNearestFeature(_streets, point));

                }

                // if OID exist in AND feature
                GStreetAND StreetTempAND = repo.GetById<GStreetAND>(street.OID);
                if (StreetTempAND != null)
                {
                    streetAND = StreetTempAND;
                    street = null;
                }

                // noraini ali - OKT 2020 - hightlight feature
                if (streetAND != null)
                {
                    highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                }
                else
                {
                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                }
            }
            else  // OTHERS GROUP
            {
                // checking user non-AND cannot touch feature in Working Area
                ANDPropertyTools andProperty = new ANDPropertyTools();
                andProperty.checkingPointInWorkArea(SegmentName, point);
                //if (!andProperty.checkingPointInWorkArea(SegmentName, point))
                //{
                //    return;
                //}

                if (streets.Count == 1)
                {
                    street = streets[0];
                }
                else if (streets.Count > 1)
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(streets, GeometryUtils.GetNearestFeature(streets, point));
                    OnReported(MessageStreet);
                }
                else if (streets.Count == 0)
                {
                    throw new Exception("No streets found within radius");
                }

                // noraini ali - OKT 2020 - hightlight feature
                highlightStreet(repo, street.OID, true, _selectedFeatureLines);
            }

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }

                GProperty newProperty = repo.NewObj<GProperty>();
                newProperty.Init();
                
                if (streetAND != null)
                {
                    newProperty.StreetId = streetAND.OriId;
                }
                else
                    newProperty.StreetId = street.OID;

                newProperty.Shape = point;

                //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated
                if (Session.User.GetGroup().Name == "AND")
                {
                    ANDPropertyTools andProperty = new ANDPropertyTools();
                    andProperty.UpdateCreatedByUserAND(newProperty);
                    andProperty.UpdateModifiedByUserAND(newProperty);
                }

                newProperty = repo.Insert(newProperty, false);

                using (AddPropertyForm form = new AddPropertyForm(newProperty))
                {
                    OnReported(MessageForm1);
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }
                    OnReported(MessageButton1);
                    using (new WaitCursor())
                    {
                        form.SetValues();
                        //repo.Update(newProperty);
                        repo.UpdateRemainStatus(newProperty, true);

                        //noraini ali- April 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDPropertyTools andProperty = new ANDPropertyTools();
                            andProperty.AddPropertyAND(repo, newProperty);
                        }
                        // end

                        // noraini ali - OKT 2020 - hightlight feature
                        highlightProperty(repo, newProperty.OID, true, _selectedFeaturePoints);

                        OnReported("New property created. {0}", newProperty.OID);
                        OnReported(Message1);
                        OnStepReported(Message1);
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        public override bool Deactivate()
        {
            if (_areaCircle != null)
            {
                _areaCircle.Stop(); ;
                _areaCircle = null;
            }

            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_areaCircle != null)
            {
                _areaCircle.Refresh(hDC);
            }
        }
    }
}

﻿using Earthworm.AO;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.Commands;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class MovePropertyTool : MoveTool
    {
        #region Fields

        private CommandPool _commandPool;
        private string _selectMethod;

        protected Dictionary<GProperty, MovePoint> _selectedProperties;

        // added by noraini ali - apr 2020
        protected Dictionary<GPropertyAND, MovePoint> _selectedPropertiesAND;
        // end

        // added by asyrul
        private const string messageDefault = "Select a property(s) to move. Right click to change tracker method";
        private const string messagePolygon = "Draw property(s) to move. Double click to end drawing.";
        private const string messageMove = "Drag property(s) to move them.";
        // added end

        #endregion

        public MovePropertyTool()
        {
            _name = "Move Property";
            InitMenu();
            InitCommand();
            OnRectangle(null, null);
        }

        private void InitCommand()
        {
            _commandPool = new CommandPool();

            _commandPool.Register(Command.Rectangle, menuRectangle);
            _commandPool.Register(Command.Polygon, menuPolygon);
            _commandPool.Register(Command.Circle, menuCircle);

            _commandPool.RegisterRadio(Command.SelectMethod,
                                                Command.Rectangle,
                                                Command.Polygon,
                                                Command.Circle);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            if (_selectedProperties == null)
            {
                _selectedProperties = new Dictionary<GProperty, MovePoint>();
            }

            // noraini ali - Apr 2020 
            if (_selectedPropertiesAND == null)
            {
                _selectedPropertiesAND = new Dictionary<GPropertyAND, MovePoint>();
            }
            // end
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            IGeometry geometry;

                            switch (_selectMethod)
                            {
                                case Command.Rectangle:
                                    {
                                        geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                        break;
                                    }
                                case Command.Polygon:
                                    {
                                        geometry = new PolygonTracker().TrackNew(ActiveView, point);
                                        break;
                                    }
                                case Command.Circle:
                                    {
                                        geometry = new CircleTracker().TrackNew(ActiveView, point);
                                        break;
                                    }
                                default:
                                    throw new Exception("Unknown draw method.");
                            }

                            using (new WaitCursor())
                            {
                                if (geometry == null)
                                {
                                    return;
                                }

                                List<GProperty> properties = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                List<GPropertyAND> propertiesAND = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                // added by noraini ali- Apr 2020
                                if (properties.Count == 0 && propertiesAND.Count == 0)
                                {
                                    return;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    ANDPropertyTools andProperty = new ANDPropertyTools();

                                    _selectedProperties.Clear();
                                    _selectedPropertiesAND.Clear();

                                    // Check list feature in SpatialSearch, filter only selected list property AND not exist
                                    foreach (GProperty property in properties)
                                    {
                                        if (string.IsNullOrEmpty(property.AndStatus.ToString()) || (property.AndStatus.ToString() == "0"))
                                        {
                                            // check area id exist or owner
                                            andProperty.CheckWithinWorkArea(SegmentName, property);
                                            andProperty.CheckUserOwnWorkArea(SegmentName, property);

                                            // add list property
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed);
                                            movePoint.Point = property.Point;
                                            movePoint.Start(point);
                                            _selectedProperties.Add(property, movePoint);
                                        }
                                    }

                                    //Check list feature in SpatialSearch, filter only selected list property AND with ANDSTATUS not in delete
                                    foreach (GPropertyAND propertyAND in propertiesAND)
                                    {
                                        if (propertyAND.AndStatus != 2)
                                        {
                                            // check area id exist or owner
                                            andProperty.CheckWithinWorkArea(SegmentName, propertyAND);
                                            andProperty.CheckUserOwnWorkArea(SegmentName, propertyAND);

                                            // add list property AND
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed);
                                            movePoint.Point = propertyAND.Point;
                                            movePoint.Start(point);
                                            _selectedPropertiesAND.Add(propertyAND, movePoint);
                                        }
                                    }
                                }
                                // end added 
                                else   // Case non AND
                                {
                                    // checking user non-AND cannot touch feature in Working Area
                                    ANDPropertyTools andProperty = new ANDPropertyTools();

                                    _selectedProperties.Clear();
                                    foreach (GProperty property in properties)
                                    {
                                        MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed);
                                        movePoint.Point = property.Point;
                                        movePoint.Start(point);
                                        _selectedProperties.Add(property, movePoint);
                                    }

                                    foreach (GProperty property in properties)
                                    {
                                        // noraini ali - Apr 2021 - check feature lock by user AND
                                        andProperty.CheckFeatureLock(property.AreaId, SegmentName);

                                        // noraini ali - Mei 2021 - check feature on verification
                                        andProperty.CheckFeatureOnVerification(property);
                                    }
                                }

                                OnReported(messageMove);
                                OnStepReported(messageMove);
                                _progress = Progress.TryMove;
                            }
                        }
                        //else if (button == MouseKey.Right)
                        //{
                        //    MapDocument.ShowContextMenu(x, y);
                        //}
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        using (new WaitCursor())
                        {
                            if (Core.Sessions.Session.User.GetGroup().Name != "AND")
                            {
                                bool noContain = true;
                                foreach (GProperty property in _selectedProperties.Keys)
                                {
                                    noContain &= !point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(property.Point);
                                }
                                if (noContain)
                                {
                                    foreach (MovePoint movePoint in _selectedProperties.Values)
                                    {
                                        movePoint.Stop();
                                    }
                                    _selectedProperties.Clear();
                                    _progress = Progress.Select;
                                }
                            }
                            else  // case AND User
                            {
                                // search contain AND Property
                                bool noContain = true;

                                foreach (GProperty property in _selectedProperties.Keys)
                                {
                                    noContain &= !point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(property.Point);
                                }
                                if (noContain)
                                {
                                    foreach (MovePoint movePoint in _selectedProperties.Values)
                                    {
                                        movePoint.Stop();
                                    }
                                    _selectedProperties.Clear();
                                    _progress = Progress.Select;
                                }

                               foreach (GPropertyAND propertyAND in _selectedPropertiesAND.Keys)
                               {
                                   noContain &= !point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(propertyAND.Point);
                               }
                               if (noContain)
                               {
                                   foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                                   {
                                       movePoint.Stop();
                                   }
                                   _selectedPropertiesAND.Clear();
                                   _progress = Progress.Select;
                               }
                            }
                            _progress = Progress.Moving;

                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        //  added by noraini ali - apr 2020
                        if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                        {
                            foreach (MovePoint movePoint in _selectedProperties.Values)
                            {
                                movePoint.MoveTo(point, true);
                            }

                            foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                            {
                                movePoint.MoveTo(point, true);
                            }
                        }
                        // end
                        else
                        {
                            foreach (MovePoint movePoint in _selectedProperties.Values)
                            {
                                movePoint.MoveTo(point, true);
                            }
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }

                            using (new WaitCursor())
                            {
                                try
                                {
                                    //  added by noraini ali - apr 2020
                                    if (Core.Sessions.Session.User.GetGroup().Name != "AND")
                                    {
                                        foreach (MovePoint movePoint in _selectedProperties.Values)
                                        {
                                            movePoint.Stop();
                                        }

                                        repo.StartTransaction(() =>
                                        {
                                            foreach (KeyValuePair<GProperty, MovePoint> pair in _selectedProperties)
                                            {
                                                IPoint newPoint = pair.Value.Geometry as IPoint;
                                                pair.Key.Shape = newPoint;

                                                //repo.Update(pair.Key);
                                                repo.UpdateGraphic(pair.Key, true);

                                                foreach (GPoi poi in pair.Key.GetPois())
                                                {
                                                    poi.Shape = newPoint;
                                                    repo.Update(poi);
                                                }
                                            }
                                        });
                                    }
                                    else  // case user AND
                                    {
                                        // noraini ali - Sept 2020-Confirmation to move feature
                                        // process move the original feature (ADM)
                                        // confirm to move so need to update ADM feature to previous location
                                        // and create AND at new location

                                        bool bool_confirmMove = false;
                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        confirmBox.SetCaption("Move Property")
                                            .SetButtons(MessageBoxButtons.OKCancel)
                                            .SetText("Confirm Move Property to New Location as AND Property?");

                                        if (confirmBox.Show() == DialogResult.OK)
                                        {
                                            bool_confirmMove = true;
                                        }
                                        
                                        foreach (MovePoint movePoint in _selectedProperties.Values)
                                        {
                                            movePoint.Stop();
                                        }

                                        foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                                        {
                                            movePoint.Stop();
                                        }


                                        repo.StartTransaction(() =>
                                        {
                                            foreach (KeyValuePair<GProperty, MovePoint> pair in _selectedProperties)
                                            {
                                                IPoint newPoint = pair.Value.Geometry as IPoint;

                                                if (bool_confirmMove)
                                                {
                                                    // create AND Property with new Location
                                                    ANDPropertyTools andProperty = new ANDPropertyTools();
                                                    andProperty.Move(repo, pair.Key, newPoint);
                                                }
                                            }

                                            foreach (KeyValuePair<GPropertyAND, MovePoint> pair in _selectedPropertiesAND)
                                            {
                                                IPoint newPoint = pair.Value.Geometry as IPoint;

                                                if (bool_confirmMove)
                                                {
                                                    GProperty Property = repo.GetById<GProperty>(pair.Key.OriId);
                                                    ANDPropertyTools andProperty = new ANDPropertyTools();
                                                    andProperty.Move(repo, Property, newPoint);
                                                }
                                            }
                                        });
                                    }
                                }
                                finally
                                {
                                    _selectedProperties.Clear();
                                    _selectedPropertiesAND.Clear();
                                    _progress = Progress.Select;

                                    // added by asyrul
                                    switch (_selectMethod)
                                    {
                                        case Command.Rectangle:
                                            OnReported(messageDefault);
                                            OnStepReported(messageDefault);
                                            break;

                                        case Command.Polygon:
                                            OnReported(messagePolygon);
                                            OnStepReported(messagePolygon);
                                            break;

                                        case Command.Circle:
                                            OnReported(messageDefault);
                                            OnStepReported(messageDefault);
                                            break;
                                    }
                                    // added end
                                }
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_selectedProperties != null)
            {
                foreach (MovePoint movePoint in _selectedProperties.Values)
                {
                    movePoint.Stop();
                }
                _selectedProperties.Clear();
            }

            // noraini ali - Apr 2020
            if (_selectedPropertiesAND != null)
            {
                foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                {
                    movePoint.Stop();
                }
                _selectedPropertiesAND.Clear();
            }
            // end
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_selectedProperties != null)
            {
                foreach (MovePoint movePoint in _selectedProperties.Values)
                {
                    movePoint.Stop();
                }
                _selectedProperties.Clear();
            }
            _selectedProperties = null;

            //noraini ali 
            if (_selectedPropertiesAND != null)
            {
                foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                {
                    movePoint.Stop();
                }
                _selectedPropertiesAND.Clear();
            }
            _selectedPropertiesAND = null;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedProperties != null)
            {
                foreach (MovePoint movePoint in _selectedProperties.Values)
                {
                    movePoint.Refresh(hDC);
                }
            }
            // noriani ali - Apr 2020
            if (_selectedPropertiesAND != null)
            {
                foreach (MovePoint movePoint in _selectedPropertiesAND.Values)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }

        private void OnRectangle(object sender, EventArgs e)
        {
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            _selectMethod = Command.Rectangle;
            _commandPool.Check(Command.Rectangle);
        }

        private void OnPolygon(object sender, EventArgs e)
        {
            OnReported(messagePolygon);
            OnStepReported(messagePolygon);

            _selectMethod = Command.Polygon;
            _commandPool.Check(Command.Polygon);
        }

        private void OnCircle(object sender, EventArgs e)
        {
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            _selectMethod = Command.Circle;
            _commandPool.Check(Command.Circle);
        }
    }
}

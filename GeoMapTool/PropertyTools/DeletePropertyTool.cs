﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI.Commands;
using Geomatic.Core.Rows;
using Geomatic.Core.Sessions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class DeletePropertyTool : DeleteTool
    {
        // added by asyrul
        #region Fields

        private CommandPool _commandPool;
        private string _selectMethod;

        private const string messageDefault = "Double click to select a property(s) to delete. Right click to change tracker method";
        private const string messagePolygon = "Draw property(s) to delete. Double click to end drawing.";
        private const string messageTransfer = "Double click to select a property to transfer the phone number.";

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        protected List<GProperty> _propertyPhone;
        protected List<GPropertyAND> _propertyPhoneAND;

        #endregion
        // added end

        private enum Progress
        {
            SelectSourceProperty = 0,
            SelectDestinationProperty
        }

        private Progress _progress;

        public DeletePropertyTool()
        {
            _name = "Delete Property";

            // added by asyrul
            InitMenu();
            InitCommand();
            OnRectangle(null, null);
            // added end

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
            _propertyPhone = new List<GProperty>();
        }

        // added by asyrul
        private void InitCommand()
        {
            _commandPool = new CommandPool();

            _commandPool.Register(Command.Rectangle, menuRectangle);
            _commandPool.Register(Command.Polygon, menuPolygon);

            _commandPool.RegisterRadio(Command.SelectMethod,
                                                Command.Rectangle,
                                                Command.Polygon);
        }
        // added end

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(messageDefault);
            OnStepReported(messageDefault);
            _progress = Progress.SelectSourceProperty;
        }


        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    IPoint point = DisplayTransformation.ToMapPoint(x, y);
                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    InProgress = true;

                    IGeometry geometry;
                    switch (_selectMethod)
                    {
                        case Command.Rectangle:
                            {
                                geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                break;
                            }
                        case Command.Polygon:
                            {
                                geometry = new PolygonTracker().TrackNew(ActiveView, point);
                                break;
                            }
                        
                        default:
                            throw new Exception("Unknown draw method.");
                    }

                    switch (_progress)
                    {
                        case Progress.SelectSourceProperty:
                            {
                                List<GProperty> properties;
                                GProperty property = null;

                                List<GPropertyAND> propertiesAND; 
                                GPropertyAND propertyAND = null;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }
                                    properties = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    propertiesAND = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                }

                                if (properties.Count == 0 && propertiesAND.Count == 0)
                                {
                                    return;
                                }
                                // AND USER
                                if (Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id & Checking point in Working Area & Owner
                                    ANDPropertyTools andProperty = new ANDPropertyTools();
                                    andProperty.CheckWithinWorkArea(SegmentName, point);
                                    andProperty.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                                    List<GProperty> _NewListproperties = new List<GProperty>();
                                    List<GPropertyAND> _NewListpropertiesAND = new List<GPropertyAND>();

                                    if (properties.Count > 0)
                                    {
                                        _NewListproperties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                                    }

                                    if (propertiesAND.Count > 0)
                                    {
                                        _NewListpropertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                                    }
       
                                    List<GProperty> _properties = new List<GProperty>(); // create new list to store all ADM feature to use in ChooseForm
                                    foreach (GProperty propertyTemp in _NewListproperties)
                                    {
                                        _properties.Add(propertyTemp);
                                    }

                                    foreach (GPropertyAND propertyTempAND in _NewListpropertiesAND)
                                    {
                                        GProperty Property = repo.GetById<GProperty>(propertyTempAND.OriId);
                                        if (Property != null)
                                            _properties.Add(Property);
                                    }

                                    propertyAND = null;
                                    if (_properties.Count == 0)
                                    {
                                        return;
                                    }
                                    else if (_properties.Count == 1)
                                    {
                                        property = _properties[0];

                                        GPropertyAND PropertyTempAND = property.GetPropertyANDId();
                                        if (PropertyTempAND != null) // check if OID exist in AND feature
                                        {
                                            propertyAND = PropertyTempAND;
                                            property = null;
                                        }
                                    }
                                    else
                                    {
                                        property = ChooseFeature(_properties, null);

                                        GPropertyAND PropertyTempAND = repo.GetById<GPropertyAND>(property.OID);
                                        if (PropertyTempAND != null) // check if OID exist in AND feature
                                        {
                                            propertyAND = PropertyTempAND;
                                            property = null;
                                        }
                                    }
                                    
                                    highlightProperty_hasBuilding(repo, property, propertyAND, _selectedFeaturePoints); // noraini ali - OKT 2020 - hightlight feature
                                    highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines); // noraini ali - OKT 2020 - hightlight feature

                                }
                                // OTHER USER
                                else
                                {
                                    ANDPropertyTools andProperty = new ANDPropertyTools();

                                    if (properties.Count == 1)
                                    {
                                        property = properties[0];
                                    }
                                    else if (properties.Count > 1)
                                    {
                                        if (Session.User.GetGroup().Name != "SUPERVISOR") // noraini ali - insert supervisor user validation 
                                        {
                                            throw new Exception("Delete property by fence only allowed for Supervisor."); 
                                        }

                                        foreach (GProperty prop in properties)
                                        {  
                                            andProperty.CheckFeatureLock(prop.AreaId, SegmentName); // noraini ali - Apr 2021 - check feature lock by user AND   
                                            andProperty.CheckFeatureOnVerification(prop); // noraini ali - Mei 2021 - Check feature on verification
                                        }

                                        if (_selectMethod == Command.Polygon)
                                        {
                                            QuestionMessageBox confirmBox = new QuestionMessageBox();
                                            confirmBox.SetButtons(MessageBoxButtons.YesNo);
                                            confirmBox.SetCaption("Delete Property by Fence")
                                                      .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                                      .SetText("Are you sure you want to delete " + properties.Count + " selected Properties ?");

                                            if (confirmBox.Show() == DialogResult.Yes)
                                            {
                                                int deletedCounter = 0;
                                                using (new WaitCursor())
                                                {
                                                    foreach (GProperty prop in properties)
                                                    {
                                                        deleteProperty(prop, repo);
                                                        deletedCounter++;
                                                    }
                                                }
                                                if (_propertyPhone.Count > 0)
                                                {
                                                    _progress = Progress.SelectDestinationProperty;
                                                    OnStepReported(messageTransfer);
                                                }
                                                else
                                                {
                                                    MessageBox.Show(deletedCounter + " properties deleted successfully.");
                                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                                    return;
                                                }
                                                
                                            }
                                        }
                                        else
                                        {
                                            property = ChooseFeature(properties, null);
                                        }
                                    }

                                    if (property == null)
                                    {
                                        return;
                                    }
            
                                    propertyAND = null; // noraini ali - OKT 2020 - hightlight feature
                                    highlightProperty_hasBuilding(repo, property, propertyAND, _selectedFeaturePoints); // noraini ali - OKT 2020 - hightlight feature
                                    highlightProperty_Streetid(repo, property, propertyAND, _selectedFeatureLines); // noraini ali - OKT 2020 - hightlight feature

                                    andProperty.CheckFeatureLock(property.AreaId, SegmentName); // noraini ali - Jun 2020 - check feature lock by AND
                                    andProperty.CheckFeatureOnVerification(property); // noraini ali - Mac 2021 - check feature on verification
                                }

                                using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                                {
                                    // AND USER
                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        if (propertyAND != null)
                                        {
                                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                                .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                                .SetCaption("Delete Property")
                                                .SetText("Are you sure you want to delete this property?\nOriId: {0}\nLot:{1} House:{2}", propertyAND.OriId, propertyAND.Lot, propertyAND.House);
                                        }
                                        else
                                        {
                                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                            .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                            .SetCaption("Delete Property")
                                            .SetText("Are you sure you want to delete this property?\nId: {0}\nLot:{1} House:{2}", property.OID, property.Lot, property.House);

                                        }
                                    }
                                    // OTHER USER
                                    else
                                    {
                                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                          .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                          .SetCaption("Delete Property")
                                          .SetText("Are you sure you want to delete this property?\nId: {0}\nLot:{1} House:{2}", property.OID, property.Lot, property.House);
                                    }

                                    if (confirmBox.Show() == DialogResult.No)
                                    {
                                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                        return;
                                    }
                                }

                                using (new WaitCursor())
                                {
                                    // AND USER
                                    if (Session.User.GetGroup().Name == "AND")
                                    {
                                        if (property != null)
                                        {
                                            deleteProperty(property, repo);
                                        }
                                        else
                                        {
                                            deleteProperty(propertyAND, repo);
                                        }
                                    }
                                    // OTHER USER
                                    else
                                    {
                                        deleteProperty(property, repo);
                                    }
                                }

                                OnReported(messageDefault);
                                OnStepReported(messageDefault);
                                if (_propertyPhone.Count > 0)
                                {
                                    _progress = Progress.SelectDestinationProperty;
                                    OnStepReported(messageTransfer);
                                }
                                else
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                }
                                break;
                            }
                        case Progress.SelectDestinationProperty:
                            {
                                if (button == MouseKey.Left)
                                {
                                    List<GProperty> propertiesToBeTransfered = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    if (propertiesToBeTransfered.Count < 1)
                                    {
                                        throw new Exception("No Property selected!");
                                    }
                                    if (propertiesToBeTransfered.Count > 1)
                                    {
                                        throw new Exception("Cannot select more than ONE Property!");
                                    }
                                    foreach (GProperty prop in _propertyPhone)
                                    {
                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        confirmBox.SetButtons(MessageBoxButtons.YesNo);
                                        confirmBox.SetCaption("Property Phone Number Transfer")
                                                  .SetText(
                                                  "Do you sure you want to TRANSFER ALL phone numbers to this property? " +
                                                  "\nYou CANNOT undo this process.");

                                        if (confirmBox.Show() == DialogResult.Yes)
                                        {
                                            using (new WaitCursor())
                                            {                                               
                                                GProperty propertyToBeTransfered = propertiesToBeTransfered[0];
                                                Console.WriteLine(propertyToBeTransfered);
                                                prop.transferAllPhones(propertyToBeTransfered);
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("Unable to delete property. Property has phone.");
                                        }

                                        repo.StartTransaction(() =>
                                        {
                                            foreach (GPropertyText text in prop.GetTexts())
                                            {
                                                repo.Delete(text);
                                            }
                                            
                                            OnReported("Property deleted. {0}", prop.OID);
                                            repo.Delete(prop);
                                        });
                                    }
                                    MessageBox.Show("Done Transfer and Delete Success!");
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    _propertyPhone.Clear(); // Removing all elements from list
                                    _progress = Progress.SelectSourceProperty;
                                    OnStepReported(messageTransfer);
                                }
                                else if (button == MouseKey.Right)
                                {
                                    MapDocument.ShowContextMenu(x, y);
                                }
                                break;
                            }
                        default:
                            throw new Exception("Unknown progress.");
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    _propertyPhone.Clear();
                    OnReported(messageDefault);
                    OnStepReported(messageDefault);
                    _progress = Progress.SelectSourceProperty;
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(contextMenu, x, y);
            }
        }

        private void OnRectangle(object sender, EventArgs e)
        {
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            _selectMethod = Command.Rectangle;
            _commandPool.Check(Command.Rectangle);
        }

        private void OnPolygon(object sender, EventArgs e)
        {
            OnReported(messagePolygon);
            OnStepReported(messagePolygon);

            _selectMethod = Command.Polygon;
            _commandPool.Check(Command.Polygon);
        }

        private void deleteProperty(GProperty prop, RepositoryFactory repo)
        {
            //AND
            if (Session.User.GetGroup().Name == "AND")
            {
                // get ADM Building for this property
                // if AND building exist so validation check on AND Building Only
                if (prop.HasBuildingAND())
                {
                    foreach (GBuildingAND buildingAnd in prop.GetbuildingsAND())
                    {
                        if (buildingAnd.AndStatus != 2)
                        {
                            throw new Exception("Unable to delete property. Property has building.");
                        }
                    }
                }
                else
                {
                    if (prop.HasBuilding())
                    {
                        foreach (GBuilding building in prop.GetBuildings())
                        {
                            if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                            {
                                throw new Exception("Unable to delete property. Property has building.");
                            }
                        }
                    }
                }
            }
            //OTHERS
            else
            {
                if (prop.HasPoi())
                {
                    throw new Exception("Unable to delete property. Property has poi.");
                }

                if (prop.HasBuilding())
                {
                    throw new Exception("Unable to delete property. Property has building.");
                }
            }

            if (prop.HasFloor())
            {
                throw new Exception("Unable to delete property. Property has MultiFloor.");
            }

            if (prop.HasPhone())
            {
                if (Session.User.GetGroup().Name == "AND")
                {
                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                    confirmBox.SetButtons(MessageBoxButtons.YesNo);
                    confirmBox.SetCaption("Delete Property")
                              .SetText(
                              "The Property you about to delete has Phone Number. " +
                              "\nProceed?");

                    if (confirmBox.Show() == DialogResult.No)
                    {
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        return;
                    }
                }
                else
                {
                    string phonesMessage = $"Property number {prop.House} with ID {prop.OID} \nhas phone number(s):\n";

                    foreach (GPhone phone in prop.GetPhones())
                    {
                        phonesMessage = phonesMessage + "\n - " + phone.Number;
                    }

                    phonesMessage = phonesMessage + "\n\n Please select ONE Property to transfer the Phone Number(s).";

                    MessageBox.Show(phonesMessage);
                    _propertyPhone.Add(prop);
                    return;
                }   
            }

            repo.StartTransaction(() =>
            {
                if (Session.User.GetGroup().Name == "AND")
                {
                    ANDPropertyTools andProperty = new ANDPropertyTools();
                    andProperty.Delete(repo, prop);
                }
                else
                {
                    foreach (GPropertyText text in prop.GetTexts())
                    {
                        repo.Delete(text);
                    }
                
                    OnReported("Property deleted. {0}", prop.OID);
                    repo.Delete(prop);
                }
            });
            OnReported("Select a property to delete.");
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        private void deleteProperty(GPropertyAND prop, RepositoryFactory repo)
        {
            GProperty propAdmin = repo.GetById<GProperty>(prop.OriId);

            if (prop.HasBuildingAND())
            {
                foreach (GBuildingAND buildingAnd in prop.GetbuildingsAND())
                {
                    if (buildingAnd.AndStatus != 2)
                    {
                        throw new Exception("Unable to delete property. Property has building.");
                    }
                }
            }
            else
            {
                if (prop.HasBuilding())
                {
                    foreach (GBuilding building in prop.GetBuildings())
                    {
                        if (string.IsNullOrEmpty(building.AndStatus.ToString()) || (building.AndStatus == 0))
                        {
                            throw new Exception("Unable to delete property. Property has building.");
                        }
                    }
                }
            }

            if (prop.HasFloor())
            {
                throw new Exception("Unable to delete property. Property has MultiFloor.");
            }

            if (propAdmin.HasPhone())
            {
                QuestionMessageBox confirmBox = new QuestionMessageBox();
                confirmBox.SetButtons(MessageBoxButtons.YesNo);
                confirmBox.SetCaption("Delete Property")
                          .SetText(
                          "The Property you about to delete has Phone Number. " +
                          "\nProceed?");

                if (confirmBox.Show() == DialogResult.No)
                {
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    return;
                }
            }

            repo.StartTransaction(() =>
            {
                ANDPropertyTools andProperty = new ANDPropertyTools();
                andProperty.Delete(repo, prop);
            });

            OnReported("Select a property to delete.");
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }
    }
}



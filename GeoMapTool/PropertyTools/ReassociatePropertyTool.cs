﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Earthworm.AO;
using Geomatic.UI.Commands;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.PropertyTools
{
    public partial class ReassociatePropertyTool : ReassociateTool
    {
        #region Fields

        protected List<GProperty> _selectedProperties;
        protected List<GPropertyAND> _selectedPropertiesAND;
        protected List<MovePoint> _selectedPropertyPoints;
        // added by asyrul
        private CommandPool _commandPool;
        private string _selectMethod;

        private const string messageDefault = "Select a property(s) to reassociate. Right click to change tracker method";
        private const string messagePolygon = "Draw property(s) to reassociate. Double click to end drawing.";
        int _workAreaId = 0;

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        // added end

        #endregion

        public ReassociatePropertyTool()
        {
            _name = "Reassociate Property";
            InitMenu();
            _selectedPropertyPoints = new List<MovePoint>();
            //added by asyrul
            InitCommand();
            OnRectangle(null, null);
            // added end

            // noraini ali - OKT 2020          
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        // added by asyrul
        private void InitCommand()
        {
            _commandPool = new CommandPool();

            _commandPool.Register(Command.Rectangle, menuRectangle);
            _commandPool.Register(Command.Polygon, menuPolygon);

            _commandPool.RegisterRadio(Command.SelectMethod,
                                                Command.Rectangle,
                                                Command.Polygon);
        }
        // added end

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported(messageDefault);
            OnStepReported(messageDefault);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                //IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                                // added by asyrul
                                IGeometry geometry;
                                switch (_selectMethod)
                                {
                                    case Command.Rectangle:
                                        {
                                            geometry = new RectangleTracker().TrackNew(ActiveView, point);
                                            break;
                                        }
                                    case Command.Polygon:
                                        {
                                            geometry = new PolygonTracker().TrackNew(ActiveView, point);
                                            break;
                                        }

                                    default:
                                        throw new Exception("Unknown draw method.");
                                }
                                // added end

                                List<GProperty> properties;
                                List<GPropertyAND> propertiesAND;
                                List<int> _ListStreetId = new List<int>();

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    properties = repo.SpatialSearch<GProperty>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    propertiesAND = repo.SpatialSearch<GPropertyAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (properties.Count == 0 && propertiesAND.Count == 0)
                                    {
                                        return;
                                    }

                                    if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id & Checking point in Working Area & Owner
                                        ANDPropertyTools andProperty = new ANDPropertyTools();
                                        andProperty.CheckWithinWorkArea(SegmentName, point);
                                        andProperty.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andProperty.GetWorkAreaId(SegmentName, point);

                                        if (_workAreaId > 0)
                                        {
                                            if (properties.Count > 0)
                                            {
                                                properties = andProperty.FilterPropertyInWorkArea(properties, _workAreaId);
                                            }

                                            if (propertiesAND.Count > 0)
                                            {
                                                propertiesAND = andProperty.FilterPropertyInWorkArea(propertiesAND, _workAreaId);
                                            }

                                            _selectedProperties = properties;
                                            _selectedPropertiesAND = propertiesAND;

                                            // noraini ali - OKT 2020
                                            // to highlight selected property & exisitng Associate Street
                                            foreach (GProperty property in _selectedProperties)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                                movePoint.Point = property.Point;
                                                movePoint.Start(property.Point);
                                                _selectedPropertyPoints.Add(movePoint);

                                                // noraini ali - OKT 2020 
                                                // store value Street for use highlight
                                                if (property.StreetId.HasValue)
                                                {
                                                    int Streetid = property.StreetId.Value;
                                                    _ListStreetId.Add(Streetid);
                                                }
                                            }

                                            foreach (GPropertyAND propertyAND in _selectedPropertiesAND)
                                            {
                                                MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                                movePoint.Point = propertyAND.Point;
                                                movePoint.Start(propertyAND.Point);
                                                _selectedPropertyPoints.Add(movePoint);

                                                // noraini ali - OKT 2020 
                                                // store value Street for use highlight
                                                if (propertyAND.StreetId.HasValue)
                                                {
                                                    int Streetid = propertyAND.StreetId.Value;
                                                    _ListStreetId.Add(Streetid);
                                                }
                                            }
                                            // end

                                            // noraini ali - OKT 2020 - Hightlight Street with not duplicate
                                            HighlightNoDuplicateStreet(repo, _ListStreetId, _selectedFeatureLines);

                                            _progress = Progress.SelectParent;

                                            OnReported("Select a street for property association or right-click for reselect menu.");
                                            OnStepReported("Select a street for property association or right-click for reselect menu.");
                                        }
                                    }
                                    else  //OTHERS USER
                                    {
                                        // checking user non-AND cannot touch feature in Working Area
                                        ANDPropertyTools andProperty = new ANDPropertyTools();

                                        _selectedProperties = properties;

                                        foreach (GProperty property in _selectedProperties)
                                        {
                                            // noraini ali - Mei 2021 - check feature lock by user AND
                                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                            andProperty.CheckFeatureLock(property.AreaId,SegmentName);

                                            // noraini ali - Mei 2021 - check feature on verification
                                            andProperty.CheckFeatureOnVerification(property);
                                        }

                                        foreach (GProperty property in _selectedProperties)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = property.Point;
                                            movePoint.Start(property.Point);
                                            _selectedPropertyPoints.Add(movePoint);

                                            if (property.StreetId.HasValue)
                                            {
                                                int Streetid = property.StreetId.Value;
                                                _ListStreetId.Add(Streetid);
                                            }
                                        }

                                        // noraini ali - OKT 2020 - Hightlight Street with not duplicate
                                        HighlightNoDuplicateStreet(repo, _ListStreetId, _selectedFeatureLines);

                                        _progress = Progress.SelectParent;

                                        OnReported("Select a street for property association or right-click for reselect menu.");
                                        OnStepReported("Select a street for property association or right-click for reselect menu.");
                                    }
                                }
                            }
                            // added by asyrul
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(contextMenu, x, y);
                            }
                            // added end
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                if (streets.Count == 0 && streetsAND.Count == 0)
                                {
                                    return;
                                }

                                // noraini ali - OKT 2020
                                if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id & Checking point in Working Area & Owner
                                    ANDPropertyTools andProperty = new ANDPropertyTools();
                                    andProperty.CheckWithinWorkArea(SegmentName, point);
                                    andProperty.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaIdStreet = andProperty.GetWorkAreaId(SegmentName, point);

                                    if (_workAreaIdStreet != _workAreaId)
                                    {
                                        throw new Exception("Unable Reassociate to Street different Work Area.");
                                    }

                                    // filter street ADM & AND in this working area only
                                    if (streets.Count > 0)
                                    {
                                        streets = andProperty.FilterStreetInWorkArea(streets, _workAreaId);
                                    }

                                    if (streetsAND.Count > 0)
                                    {
                                        streetsAND = andProperty.FilterStreetInWorkArea(streetsAND, _workAreaId);
                                    }

                                    if (streets.Count == 0 && streetsAND.Count == 0)
                                    {
                                        throw new Exception("Selected street is not in this work area.");
                                    }

                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        // noraini ali - OKT 2020
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                        ReassociateNewStreet(repo, street, point);
                                    }
                                    else
                                    {
                                        if (streetsAND.Count > 0)
                                        {
                                            GStreetAND street = streetsAND[0];
                                            // noraini ali - OKT 2020
                                            highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                            ReassociateNewStreet(repo, street, point);
                                        }
                                    }

                                    OnReported(messageDefault);
                                    OnStepReported(messageDefault);
                                    Reset();
                                }
                                else  // OTHERS USER
                                {
                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        // noraini ali - OKT 2020
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);

                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        confirmBox.SetCaption("Reassociate Property")
                                                  .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

                                        if (confirmBox.Show() == DialogResult.Yes)
                                        {
                                            using (new WaitCursor())
                                            {
                                                repo.StartTransaction(() =>
                                                {                                           
                                                    foreach (GProperty property in _selectedProperties)
                                                    {
                                                        property.StreetId = street.OID;
                                                        repo.Update(property);
                                                    }
                                                });
                                                OnReported(messageDefault);
                                                OnStepReported(messageDefault);
                                                Reset();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported(messageDefault);
            OnStepReported(messageDefault);
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedProperties = null;
            _selectedPropertiesAND = null;
            foreach (MovePoint movePoint in _selectedPropertyPoints)
            {
                movePoint.Stop();
            }
            _selectedPropertyPoints.Clear();
            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            foreach (MovePoint movePoint in _selectedPropertyPoints)
            {
                movePoint.Refresh(hDC);
            }
        }

        // added by asyrul
        private void OnRectangle(object sender, EventArgs e)
        {
            OnReported(messageDefault);
            OnStepReported(messageDefault);

            _selectMethod = Command.Rectangle;
            _commandPool.Check(Command.Rectangle);
        }

        private void OnPolygon(object sender, EventArgs e)
        {
            OnReported(messagePolygon);
            OnStepReported(messagePolygon);

            _selectMethod = Command.Polygon;
            _commandPool.Check(Command.Polygon);
        }
        // added end

        // noraini ali - OKT 2020
        private void ReassociateNewStreet(RepositoryFactory repo, GStreet street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Property")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        foreach (GProperty property in _selectedProperties)
                        {
                            // Update Feature ADM & AND
                            andProperty.Reassociate(repo, property, street.OID);
                        }
                        foreach (GPropertyAND property in _selectedPropertiesAND)
                        {
                            // Update Feature ADM & AND
                            andProperty.Reassociate(repo, property, street.OID);
                        }
                    });
                }
            }
        }

        private void ReassociateNewStreet(RepositoryFactory repo, GStreetAND street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Property")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDPropertyTools andProperty = new ANDPropertyTools();
                        foreach (GProperty property in _selectedProperties)
                        {
                            // Update Feature ADM & AND
                            andProperty.Reassociate(repo, property, street.OriId);
                        }

                        foreach (GPropertyAND property in _selectedPropertiesAND)
                        {
                            // Update Feature ADM & AND
                            andProperty.Reassociate(repo, property, street.OriId);
                        }
                    });
                }
            }
        }
    }
}

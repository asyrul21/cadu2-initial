﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Geomatic.MapTool.PropertyTools
{
    partial class ReassociatePropertyTool
    {
        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem menuReselect;
        // added by asyrul
        private ToolStripMenuItem menuRectangle;
        private ToolStripMenuItem menuPolygon;
        private ToolStripSeparator seperator1;

        // added end

        private void InitMenu()
        {
            this.contextMenu = new ContextMenuStrip();
            this.menuReselect = new ToolStripMenuItem();
            // 
            // menuReselect
            //
            this.menuReselect.Name = "menuReselect";
            this.menuReselect.Text = "Reselect";
            this.menuReselect.Click += new EventHandler(this.OnReselect);

            // added by asyrul
            //
            this.menuRectangle = new ToolStripMenuItem();
            this.menuPolygon = new ToolStripMenuItem();
            this.seperator1 = new System.Windows.Forms.ToolStripSeparator();


            this.seperator1.Name = "separator";
            this.seperator1.Size = new System.Drawing.Size(178, 6);
            // menuRectangle
            //
            this.menuRectangle.Name = "menuRectangle";
            //this.menuRectangle.Text = "Rectangle";
            this.menuRectangle.Text = "Single";
            this.menuRectangle.Click += new EventHandler(this.OnRectangle);
            // 
            // menuPolygon
            //
            this.menuPolygon.Name = "menuPolygon";
            //this.menuPolygon.Text = "Polygon";
            this.menuPolygon.Text = "Fence";
            this.menuPolygon.Click += new EventHandler(this.OnPolygon);

            // added end
            //
            // contextMenu
            //
            this.contextMenu.Items.AddRange(new ToolStripItem[] { 
            this.menuReselect,
            this.seperator1,
            this.menuRectangle,
            this.menuPolygon});
            this.contextMenu.Name = "contextMenu";
        }
    }
}

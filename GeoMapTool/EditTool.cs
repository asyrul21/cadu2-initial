﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geomatic.UI;
using ESRI.ArcGIS.SystemUI;

namespace Geomatic.MapTool
{
    public abstract class EditTool : HighlightTool
    {
        #region Fields

        private int _pencilHandle;

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                return _pencilHandle;
            }
        }

        #endregion

        public EditTool()
        {
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (button == MouseKey.Right)
            {
                if (InProgress)
                {
                    return;
                }

                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Carto;
using Geomatic.Core.Features;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Utilities;
using Geomatic.UI.Utilities.Elements;

namespace Geomatic.MapTool
{
    public class ToolLayer : CompositeGraphicsLayerClass
    {
        public event EventHandler LayerSelectionChanged;

        protected List<IGFeature> _selections;
        protected bool _isUpdating;

        public IRgbColor Color { set; get; }

        public IRgbColor OutlineColor { set; get; }

        public int SelectionCount { get { return _selections.Count; } }

        public IEnumerable<IGFeature> Items
        {
            get
            {
                foreach (IGFeature feature in _selections)
                {
                    yield return feature;
                }
            }
        }

        public ToolLayer()
            : this(string.Empty, true, null, null)
        {
        }

        public ToolLayer(string name, bool cached, IRgbColor color, IRgbColor outlineColor)
        {
            _selections = new List<IGFeature>();
            _isUpdating = false;
            Name = name;
            Cached = cached;
            Color = color;
            OutlineColor = outlineColor;
        }

        /// <summary>
        /// Use begin and end update if using multiple Add function
        /// </summary>
        public void BeginUpdate()
        {
            _isUpdating = true;
        }

        /// <summary>
        /// Use begin and end update if using multiple Add function
        /// </summary>
        public void EndUpdate()
        {
            _isUpdating = false;
            OnSelectionChanged();
        }

        public virtual void Add(IGFeature feature)
        {
            _selections.Add(feature);
            if (!_isUpdating)
            {
                OnSelectionChanged();
            }
        }

        public virtual void Add(IEnumerable<IGFeature> features)
        {
            _selections.AddRange(features);
            if (!_isUpdating)
            {
                OnSelectionChanged();
            }
        }

        public virtual void Remove(IGFeature feature)
        {
            if (!_selections.Contains(feature))
            {
                return;
            }
            _selections.Remove(feature);
            if (!_isUpdating)
            {
                OnSelectionChanged();
            }
        }

        public virtual void Clear()
        {
            _selections.Clear();
            if (!_isUpdating)
            {
                OnSelectionChanged();
            }
        }

        protected virtual void OnSelectionChanged()
        {
            IGraphicsContainer graphicsContainer = this;
            graphicsContainer.DeleteAllElements();

            foreach (IGFeature feature in _selections)
            {
                IGeometry geometry = feature.Shape;
                IElement element = ElementFactory.CreateElement(geometry, Color, OutlineColor);
                if (element != null)
                {
                    element.Geometry = geometry;
                    graphicsContainer.AddElement(element, 0);
                }
            }

            if (LayerSelectionChanged != null)
            {
                LayerSelectionChanged(this, EventArgs.Empty);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Utilities;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public class AddSectionBoundaryVertexTool : SectionBoundaryVertexTool
    {
        private enum Progress
        {
            Select = 0,
            AddVertex
        }

        #region Fields

        private Progress _progress;
        private int _pencilHandle;
        private const string Message2 = "Click a point to add vertex.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _pencilHandle;
                }
            }
        }

        #endregion

        public AddSectionBoundaryVertexTool()
        {
            _name = "Add Section Boundary Vertex";
            _pencilHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPencil);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                                    if (sectionBoundaries.Count > 0)
                                    {
                                        GSectionBoundary sectionBoundary = sectionBoundaries[0];

                                        _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                        _selectedSectionBoundaryPolygon.Start(point);
                                        List<IPoint> vertices = sectionBoundary.Vertices;
                                        for (int count = 0; count < sectionBoundary.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _sectionBoundaryVertices.Add(movePoint);
                                        }
                                        _selectedSectionBoundary = sectionBoundary;
                                        _progress = Progress.AddVertex;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedSectionBoundary = null;
                                        _selectedSectionBoundaryPolygon.Stop();
                                        foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _sectionBoundaryVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.AddVertex:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 4, 1);
                                    double hitDistance = 0;
                                    int hitPartIndex = 0;
                                    int hitSegmentIndex = 0;
                                    bool isRightSide = false;

                                    IPoint hitPoint = new PointClass();
                                    IPolygon polygon = _selectedSectionBoundary.Polygon;
                                    if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartBoundary, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                                    {
                                        if (_selectedSectionBoundary.TryAddVertex(point))
                                        {
                                            repo.StartTransaction(() =>
                                            {
                                                repo.Update(_selectedSectionBoundary);

                                                _selectedSectionBoundaryPolygon.Stop();
                                                _selectedSectionBoundaryPolygon.Polygon = _selectedSectionBoundary.Polygon;
                                                _selectedSectionBoundaryPolygon.Start(point);
                                                foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                                {
                                                    movePoint.Stop();
                                                }
                                                _sectionBoundaryVertices.Clear();
                                                List<IPoint> vertices = _selectedSectionBoundary.Vertices;
                                                for (int count = 0; count < _selectedSectionBoundary.PointCount - 1; count++)
                                                {
                                                    IPoint vertex = vertices[count];
                                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                                    movePoint.Point = vertex;
                                                    movePoint.Start(point);
                                                    _sectionBoundaryVertices.Add(movePoint);
                                                }
                                            });
                                            break;
                                        }
                                    }

                                    List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                                    if (sectionBoundaries.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _selectedSectionBoundary = null;
                                        _selectedSectionBoundaryPolygon.Stop();
                                        foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _sectionBoundaryVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GSectionBoundary sectionBoundary = sectionBoundaries[0];
                                    if (!_selectedSectionBoundary.Equals(sectionBoundary))
                                    {
                                        _selectedSectionBoundaryPolygon.Stop();
                                        _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                        _selectedSectionBoundaryPolygon.Start(point);
                                        foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _sectionBoundaryVertices.Clear();
                                        List<IPoint> vertices = sectionBoundary.Vertices;
                                        for (int count = 0; count < sectionBoundary.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _sectionBoundaryVertices.Add(movePoint);
                                        }
                                        _selectedSectionBoundary = sectionBoundary;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                        break;
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.UI.Forms.MessageBoxes;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public class DeleteSectionBoundaryTool : DeleteTool
    {
        private const string Message1 = "Select a section boundary to delete.";

        public DeleteSectionBoundaryTool()
        {
            _name = "Delete Section Boundary";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GSectionBoundary> sectionBoundaries;

                    GSectionBoundary sectionBoundary = null;

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        sectionBoundaries = repo.SpatialSearch<GSectionBoundary>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (sectionBoundaries.Count == 1)
                    {
                        sectionBoundary = sectionBoundaries[0];
                    }
                    else if (sectionBoundaries.Count > 1)
                    {
                        sectionBoundary = ChooseFeature(sectionBoundaries, null);
                    }

                    if (sectionBoundary == null)
                    {
                        return;
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                  .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                  .SetCaption("Delete Section Boundary")
                                  .SetText("Are you sure you want to delete this section boundary?\nId: {0}\n{1}", sectionBoundary.OID, sectionBoundary.Name);

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        repo.StartTransaction(() =>
                        {
                            OnReported("Section boundary deleted. {0}", sectionBoundary.OID);
                            repo.Delete(sectionBoundary);
                        });

                        OnReported(Message1);
                        OnStepReported(Message1);
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

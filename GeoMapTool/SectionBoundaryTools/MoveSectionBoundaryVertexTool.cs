﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Repositories;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public class MoveSectionBoundaryVertexTool : SectionBoundaryVertexTool
    {
        private enum Progress
        {
            Select = 0,
            TryMove,
            Moving
        }

        #region Fields

        private Progress _progress;
        private int _crosshairHandle;
        protected MovePoint _selectedVertex;
        protected MovePolygonPoint _movePolygonPoint;
        private const string Message2 = "Drag a vertex to move.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_progress == Progress.Select)
                {
                    return base.Cursor;
                }
                else
                {
                    return _crosshairHandle;
                }
            }
        }

        #endregion

        public MoveSectionBoundaryVertexTool()
        {
            _name = "Move Section Boundary Vertex";
            _crosshairHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorCrosshair);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            _progress = Progress.Select;
            if (_movePolygonPoint == null)
            {
                _movePolygonPoint = new MovePolygonPoint(ScreenDisplay, _color2);
            }
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            double searchRadius = MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1);
                            double hitDistance = 0;
                            int hitPartIndex = 0;
                            int hitSegmentIndex = 0;
                            bool isRightSide = false;
                            IPoint hitPoint = new PointClass();
                            IPolygon polygon = _selectedSectionBoundary.Polygon;

                            if (polygon.HitTest(point, searchRadius, esriGeometryHitPartType.esriGeometryPartVertex, hitPoint, ref hitDistance, ref hitPartIndex, ref hitSegmentIndex, ref isRightSide))
                            {
                                if (hitSegmentIndex == 0 || hitSegmentIndex == _selectedSectionBoundary.PointCount - 1)
                                {
                                    hitSegmentIndex = 0;
                                }
                                _movePolygonPoint.Index = hitSegmentIndex;
                                _selectedVertex = _sectionBoundaryVertices[hitSegmentIndex];
                                _movePolygonPoint.Start(point);
                                _progress = Progress.Moving;
                                break;
                            }

                            List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                            if (sectionBoundaries.Count == 0)
                            {
                                _progress = Progress.Select;
                                _selectedSectionBoundary = null;
                                _selectedSectionBoundaryPolygon.Stop();
                                foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _sectionBoundaryVertices.Clear();
                                OnReported(Message1);
                                OnStepReported(Message1);
                                break;
                            }

                            GSectionBoundary sectionBoundary = sectionBoundaries[0];
                            if (!_selectedSectionBoundary.Equals(sectionBoundary))
                            {
                                _selectedSectionBoundaryPolygon.Stop();
                                _movePolygonPoint.Polygon = sectionBoundary.Polygon;
                                _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                _selectedSectionBoundaryPolygon.Start(point);
                                foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _sectionBoundaryVertices.Clear();

                                List<IPoint> vertices = sectionBoundary.Vertices;
                                for (int count = 0; count < sectionBoundary.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _sectionBoundaryVertices.Add(movePoint);
                                }
                                _selectedSectionBoundary = sectionBoundary;
                                OnReported(Message2);
                                OnStepReported(Message2);
                                break;
                            }
                        }
                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _movePolygonPoint.MoveTo(point);
                        _selectedVertex.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                using (new WaitCursor())
                                {
                                    List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                                    if (sectionBoundaries.Count > 0)
                                    {
                                        GSectionBoundary sectionBoundary = sectionBoundaries[0];

                                        _movePolygonPoint.Polygon = sectionBoundary.Polygon;
                                        _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                        _selectedSectionBoundaryPolygon.Start(point);
                                        List<IPoint> vertices = sectionBoundary.Vertices;
                                        for (int count = 0; count < sectionBoundary.PointCount - 1; count++)
                                        {
                                            IPoint vertex = vertices[count];
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                            movePoint.Point = vertex;
                                            movePoint.Start(point);
                                            _sectionBoundaryVertices.Add(movePoint);
                                        }
                                        _selectedSectionBoundary = sectionBoundary;
                                        _progress = Progress.TryMove;
                                        OnReported(Message2);
                                        OnStepReported(Message2);
                                    }
                                    else
                                    {
                                        _selectedSectionBoundary = null;
                                        _selectedSectionBoundaryPolygon.Stop();
                                        foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                        {
                                            movePoint.Stop();
                                        }
                                        _sectionBoundaryVertices.Clear();
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.TryMove:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            using (new WaitCursor())
                            {
                                IPolygon polygon = (IPolygon)_movePolygonPoint.Stop();
                                _selectedVertex.Stop();

                                repo.StartTransaction(() =>
                                {
                                    _selectedSectionBoundary.Shape = polygon;
                                    repo.Update(_selectedSectionBoundary);
                                });

                                _selectedSectionBoundaryPolygon.Stop();
                                _movePolygonPoint.Polygon = polygon;
                                _selectedSectionBoundaryPolygon.Polygon = polygon;
                                _selectedSectionBoundaryPolygon.Start(point);
                                foreach (MovePoint movePoint in _sectionBoundaryVertices)
                                {
                                    movePoint.Stop();
                                }
                                _sectionBoundaryVertices.Clear();
                                List<IPoint> vertices = _selectedSectionBoundary.Vertices;
                                for (int count = 0; count < _selectedSectionBoundary.PointCount - 1; count++)
                                {
                                    IPoint vertex = vertices[count];
                                    MovePoint movePoint = new MovePoint(ScreenDisplay, _color1, _color1, 8, esriSimpleMarkerStyle.esriSMSSquare);
                                    movePoint.Point = vertex;
                                    movePoint.Start(point);
                                    _sectionBoundaryVertices.Add(movePoint);
                                }
                                _progress = Progress.TryMove;
                                break;
                            }
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            base.Reset();
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_movePolygonPoint != null)
            {
                _movePolygonPoint.Refresh(hDC);
            }
        }
    }
}

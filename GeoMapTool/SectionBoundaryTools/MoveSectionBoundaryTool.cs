﻿using Earthworm.AO;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public class MoveSectionBoundaryTool : MoveTool
    {
        #region Fields

        protected GSectionBoundary _selectedSectionBoundary;
        protected MovePolygon _selectedSectionBoundaryPolygon;

        private const string Message1 = "Select a section boundary to move.";
        private const string Message2 = "Drag selected section boundary to move.";

        #endregion

        public MoveSectionBoundaryTool()
        {
            _name = "Move Section Boundary";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedSectionBoundaryPolygon == null)
            {
                _selectedSectionBoundaryPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Intersects(_selectedSectionBoundary.Polygon))
                            {
                                _selectedSectionBoundaryPolygon.Restart(point);
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedSectionBoundaryPolygon.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                                if (sectionBoundaries.Count > 0)
                                {
                                    GSectionBoundary sectionBoundary = sectionBoundaries[0];
                                    _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                    _selectedSectionBoundaryPolygon.Start(point);
                                    _selectedSectionBoundary = sectionBoundary;

                                    _progress = Progress.TryMove;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GSectionBoundary> sectionBoundaries = SelectSectionBoundary(repo, point);
                                // no section boundary found
                                if (sectionBoundaries.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _selectedSectionBoundaryPolygon.Stop();
                                    _selectedSectionBoundary = null;
                                    break;
                                }

                                // take 1st section boundary
                                GSectionBoundary sectionBoundary = sectionBoundaries[0];
                                if (!_selectedSectionBoundary.Equals(sectionBoundary))
                                {
                                    _selectedSectionBoundaryPolygon.Polygon = sectionBoundary.Polygon;
                                    _selectedSectionBoundaryPolygon.Stop();
                                    _selectedSectionBoundaryPolygon.Start(point);
                                    _selectedSectionBoundary = sectionBoundary;
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        if (button != 1)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            IPolygon newPolygon = _selectedSectionBoundaryPolygon.Stop() as IPolygon;
                            _progress = Progress.Select;

                            repo.StartTransaction(() =>
                            {
                                _selectedSectionBoundary.Shape = newPolygon;
                                repo.Update(_selectedSectionBoundary);
                            });
                            _selectedSectionBoundary = null;
                        }
                        OnReported(Message1);
                        OnStepReported(Message1);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        protected List<GSectionBoundary> SelectSectionBoundary(RepositoryFactory repo, IPoint point)
        {
            Query<GSectionBoundary> query = new Query<GSectionBoundary>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;

            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Stop();
                _selectedSectionBoundaryPolygon = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Earthworm.AO;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public abstract class SectionBoundaryVertexTool : Tool
    {
        #region Fields

        protected GSectionBoundary _selectedSectionBoundary;
        protected MovePolygon _selectedSectionBoundaryPolygon;
        protected List<MovePoint> _sectionBoundaryVertices;

        protected IRgbColor _color1 = ColorUtils.Get(Color.Green);
        protected IRgbColor _color2 = ColorUtils.Get(Color.Blue);

        protected const string Message1 = "Select a section boundary.";

        #endregion

        public SectionBoundaryVertexTool()
        {
            _sectionBoundaryVertices = new List<MovePoint>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_selectedSectionBoundaryPolygon == null)
            {
                _selectedSectionBoundaryPolygon = new MovePolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        protected List<GSectionBoundary> SelectSectionBoundary(RepositoryFactory repo, IPoint point)
        {
            Query<GSectionBoundary> query = new Query<GSectionBoundary>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch<GSectionBoundary>(query).ToList();
        }

        protected override void Reset()
        {
            _selectedSectionBoundary = null;
            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Stop();
            }
            foreach (MovePoint movePoint in _sectionBoundaryVertices)
            {
                movePoint.Stop();
            }
            _sectionBoundaryVertices.Clear();
        }

        public override bool Deactivate()
        {
            _selectedSectionBoundary = null;
            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Stop();
                _selectedSectionBoundaryPolygon = null;
            }
            foreach (MovePoint movePoint in _sectionBoundaryVertices)
            {
                movePoint.Stop();
            }
            _sectionBoundaryVertices.Clear();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_selectedSectionBoundaryPolygon != null)
            {
                _selectedSectionBoundaryPolygon.Refresh(hDC);
            }
            if (_sectionBoundaryVertices != null)
            {
                foreach (MovePoint movePoint in _sectionBoundaryVertices)
                {
                    movePoint.Refresh(hDC);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.SystemUI;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public partial class AddSectionBoundaryTool : Tool
    {
        #region Fields

        protected NewPolygon _newPolygon;
        private bool _panStarted;
        private int _panningHandle;
        private const string Message1 = "Start draw section boundary. Middle click to pan. Control + click to end draw.";
        private const string Message2 = "Middle click to pan. Control + click to end draw. Right click for menu.";

        #endregion

        #region Properties

        public override int Cursor
        {
            get
            {
                if (_panStarted)
                {
                    return _panningHandle;
                }
                else
                {
                    return base.Cursor;
                }
            }
        }

        #endregion

        public AddSectionBoundaryTool()
        {
            _name = "Add Section Boundary";
            InitMenu();
            _panningHandle = EsriCursor.GetHandle(esriSystemMouseCursor.esriSystemMouseCursorPanning);
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_newPolygon == null)
            {
                _newPolygon = new NewPolygon(ScreenDisplay, ColorUtils.Select);
            }
            OnReported(Message1);
            OnStepReported(Message1);
            _panStarted = false;
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Middle)
            {
                if (_panStarted)
                {
                    return;
                }
                ScreenDisplay.PanStart(point);
                _panStarted = true;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                _newPolygon.MoveTo(point);

                if (button == MouseKey.Middle)
                {
                    if (_panStarted)
                    {
                        ScreenDisplay.PanMoveTo(point);
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                if (button == MouseKey.Left)
                {
                    if (!InProgress)
                    {
                        _newPolygon.Start(point);
                        InProgress = true;
                        OnReported(Message2);
                        OnStepReported(Message2);
                    }
                    else
                    {
                        _newPolygon.AddPoint(point);
                        if (shift == KeyCode.Ctrl)
                        {
                            try
                            {
                                _newPolygon.Stop();
                                CreateSectionBoundary();
                            }
                            catch
                            {
                                throw;
                            }
                            finally
                            {
                                InProgress = false;
                            }
                        }
                    }
                }
                else if (button == MouseKey.Right)
                {
                    if (!InProgress)
                    {
                        MapDocument.ShowContextMenu(x, y);
                    }
                    else
                    {
                        MapDocument.ShowContextMenu(contextMenu, x, y);
                    }
                }
                else if (button == MouseKey.Middle)
                {
                    if (_panStarted)
                    {
                        ScreenDisplay.PanStop();
                        _panStarted = false;
                    }
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
        }

        private void OnUndoLast(object sender, EventArgs e)
        {
            if (_newPolygon.PointCount >= 3)
            {
                _newPolygon.RemoveLastPoint();
            }
        }

        private void OnRedraw(object sender, EventArgs e)
        {
            if (!InProgress)
            {
                return;
            }
            _newPolygon.Stop();
            InProgress = false;
            OnReported(Message1);
            OnStepReported(Message1);
        }

        private void CreateSectionBoundary()
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }
                GSectionBoundary newSectionBoundary = repo.NewObj<GSectionBoundary>();
                newSectionBoundary.Init();
                newSectionBoundary.Shape = _newPolygon.Geometry;
                newSectionBoundary = repo.Insert(newSectionBoundary, false);

                using (AddSectionBoundaryForm form = new AddSectionBoundaryForm(newSectionBoundary))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        repo.Update(newSectionBoundary);

                        OnReported("New section boundary created. {0}", newSectionBoundary.OID);
                        OnReported(Message1);
                        OnStepReported(Message1);
                    }
                }
                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        protected override void Reset()
        {
            if (InProgress)
            {
                if (_newPolygon != null)
                {
                    _newPolygon.Stop();
                }
                InProgress = false;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
        }

        public override bool Deactivate()
        {
            if (_newPolygon != null)
            {
                _newPolygon.Stop();
                _newPolygon = null;
            }
            if (_panStarted)
            {
                ScreenDisplay.PanStop();
                _panStarted = false;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_newPolygon != null)
            {
                _newPolygon.Refresh(hDC);
            }
        }
    }
}

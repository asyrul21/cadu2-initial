﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI.Forms.Edit;

namespace Geomatic.MapTool.SectionBoundaryTools
{
    public class EditSectionBoundaryTool : EditTool
    {
        #region Fields

        private const string Message1 = "Select a section boundary to edit.";

        #endregion

        public EditSectionBoundaryTool()
        {
            _name = "Edit Section Boundary";
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GSectionBoundary> sectionBoundaries;

                    GSectionBoundary sectionBoundary = null;

                    if (geometry == null)
                    {
                        return;
                    }

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        sectionBoundaries = repo.SpatialSearch<GSectionBoundary>(geometry, esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    }

                    if (sectionBoundaries.Count == 1)
                    {
                        sectionBoundary = sectionBoundaries[0];
                    }
                    else if (sectionBoundaries.Count > 1)
                    {
                        sectionBoundary = ChooseFeature(sectionBoundaries, null);
                    }

                    if (sectionBoundary == null)
                    {
                        return;
                    }

                    using (EditSectionBoundaryForm form = new EditSectionBoundaryForm(sectionBoundary))
                    {
                        if (form.ShowDialog() != DialogResult.OK)
                        {
                            return;
                        }

                        using (new WaitCursor())
                        {
                            repo.StartTransaction(() =>
                            {
                                form.SetValues();
                                repo.Update(sectionBoundary);
                            });
                        }
                    }

                    OnReported("Section boundary updated. {0}", sectionBoundary.OID);
                    OnReported(Message1);
                    OnStepReported(Message1);
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                }
                finally
                {
                    InProgress = false;
                }
            }
        }
    }
}

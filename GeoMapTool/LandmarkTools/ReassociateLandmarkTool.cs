﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.FeedBacks;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Earthworm.AO;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.LandmarkTools
{
    public partial class ReassociateLandmarkTool : ReassociateTool
    {
        #region Fields

        protected List<GLandmark> _selectedLandmarks;
        protected List<GLandmarkAND> _selectedLandmarksAND;
        protected List<MovePoint> _selectedLandmarkPoints;
        int _workAreaId = 0;

        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        #endregion

        public ReassociateLandmarkTool()
        {
            _name = "Reassociate Landmark";
            InitMenu();
            _selectedLandmarkPoints = new List<MovePoint>();
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a landmark to reassociate.");
            OnStepReported("Select a landmark to reassociate.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                InProgress = true;

                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                switch (_progress)
                {
                    case Progress.Select:
                        {
                            if (button == MouseKey.Left)
                            {
                                IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                                List<GLandmark> landmarks;
                                List<GLandmarkAND> landmarksAND;

                                using (new WaitCursor())
                                {
                                    if (geometry == null)
                                    {
                                        return;
                                    }

                                    landmarks = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                                    landmarksAND = repo.SpatialSearch<GLandmarkAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();

                                    if (landmarks.Count == 0 && landmarksAND.Count == 0)
                                    {
                                        return;
                                    }

                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        // get Work Area Id & Checking point in Working Area & Owner
                                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                        andLandmark.CheckWithinWorkArea(SegmentName, point);
                                        andLandmark.CheckUserOwnWorkArea(SegmentName, point);
                                        _workAreaId = andLandmark.GetWorkAreaId(SegmentName, point);

                                        if (landmarks.Count > 0)
                                        {
                                            landmarks = andLandmark.FilterLandmarkInWorkArea(landmarks, _workAreaId);
                                        }

                                        if (landmarksAND.Count > 0)
                                        {
                                            landmarksAND = andLandmark.FilterLandmarkInWorkArea(landmarksAND, _workAreaId);
                                        }

                                        if (landmarks.Count == 0 && landmarksAND.Count == 0)
                                        {
                                            throw new QualityControlException("Landmark is not in this work area.");
                                        }

                                        _selectedLandmarks = landmarks;
                                        _selectedLandmarksAND = landmarksAND;

                                        foreach (GLandmark landmark in _selectedLandmarks)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = landmark.Point;
                                            movePoint.Start(landmark.Point);
                                            _selectedLandmarkPoints.Add(movePoint);

                                            highlightStreet(repo, landmark.StreetId.Value, false, _selectedFeatureLines);
                                        }

                                        foreach (GLandmarkAND landmark in _selectedLandmarksAND)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = landmark.Point;
                                            movePoint.Start(landmark.Point);
                                            _selectedLandmarkPoints.Add(movePoint);

                                            highlightStreet(repo, landmark.StreetId.Value, false, _selectedFeatureLines);
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a street for landmark association or right-click for reselect menu.");
                                        OnStepReported("Select a street for landmark association or right-click for reselect menu.");

                                    }
                                    else  // other user
                                    {
                                        // checking user non-AND cannot touch feature in Working Area
                                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                        //if (!andLandmark.checkingPointInWorkArea(SegmentName, point))
                                        //{
                                        //    return;
                                        //}

                                        _selectedLandmarks = landmarks;


                                        foreach (GLandmark landmark in _selectedLandmarks)
                                        {
                                            // noraini ali - Mei 2021 - check feature lock by AND
                                            andLandmark.CheckFeatureLock(landmark.AreaId, SegmentName);

                                            // noraini ali - Mei 2021 - check feature on verification
                                            andLandmark.CheckFeatureOnVerification(landmark);
                                        }

                                        foreach (GLandmark landmark in _selectedLandmarks)
                                        {
                                            MovePoint movePoint = new MovePoint(ScreenDisplay, _colorRed, POINT_SIZE);
                                            movePoint.Point = landmark.Point;
                                            movePoint.Start(landmark.Point);
                                            _selectedLandmarkPoints.Add(movePoint);

                                            // noraini ali - OKT 2020 
                                            // highlight landmark
                                            highlightStreet(repo, landmark.StreetId.Value, false, _selectedFeatureLines);
                                        }

                                        _progress = Progress.SelectParent;
                                        OnReported("Select a street for landmark association or right-click for reselect menu.");
                                        OnStepReported("Select a street for landmark association or right-click for reselect menu.");
                                    }
                                }
                            }
                            else if (button == MouseKey.Right)
                            {
                                MapDocument.ShowContextMenu(x, y);
                            }
                            break;
                        }
                    case Progress.SelectParent:
                        {
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.SelectParent:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GStreet> streets = SelectStreet(repo, point);
                                List<GStreetAND> streetsAND = SelectStreetAND(repo, point);

                                if (streets.Count == 0 && streetsAND.Count == 0)
                                {
                                    return;
                                }

                                // noraini ali - OKT 2020
                                if (Geomatic.Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    // get Work Area Id & Checking point in Working Area & Owner
                                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                    andLandmark.CheckWithinWorkArea(SegmentName, point);
                                    andLandmark.CheckUserOwnWorkArea(SegmentName, point);
                                    int _workAreaIdStreet = andLandmark.GetWorkAreaId(SegmentName, point);

                                    if (_workAreaIdStreet != _workAreaId)
                                    {
                                        throw new Exception("Unable Reassociate to Street in different Work Area.");
                                    }

                                    // filter street ADM & AND in this working area only
                                    if (streets.Count > 0)
                                    {
                                        streets = andLandmark.FilterStreetInWorkArea(streets, _workAreaId);
                                    }

                                    if (streetsAND.Count > 0)
                                    {
                                        streetsAND = andLandmark.FilterStreetInWorkArea(streetsAND, _workAreaId);
                                    }

                                    if (streets.Count == 0 && streetsAND.Count == 0)
                                    {
                                        throw new Exception("Selected street is not in this work area.");
                                    }

                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];
                                        // noraini ali - OKT 2020 - highlight new street
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                                        ReassociateNewStreet(repo, street, point);
                                    }
                                    else
                                    {
                                        if (streetsAND.Count > 0)
                                        {
                                            GStreetAND street = streetsAND[0];
                                            // noraini ali - OKT 2020 - highlight new street
                                            highlightStreet(repo, street.OID, true,_selectedFeatureLines);
                                            ReassociateNewStreet(repo, street, point);
                                        }
                                    }

                                    OnReported("Select a landmark to reassociate.");
                                    OnStepReported("Select a landmark to reassociate.");
                                    Reset();
                                }
                                else  // Others User
                                {
                                    if (streets.Count > 0)
                                    {
                                        GStreet street = streets[0];

                                        // noraini ali - OKT 2020 - highlight new street
                                        highlightStreet(repo, street.OID, true, _selectedFeatureLines);

                                        QuestionMessageBox confirmBox = new QuestionMessageBox();
                                        confirmBox.SetCaption("Reassociate Landmark")
                                                  .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

                                        if (confirmBox.Show() == DialogResult.Yes)
                                        {
                                            using (new WaitCursor())
                                            {
                                                repo.StartTransaction(() =>
                                                {
                                                    foreach (GLandmark landmark in _selectedLandmarks)
                                                    {
                                                        landmark.StreetId = street.OID;
                                                        repo.Update(landmark);
                                                    }
                                                });

                                                OnReported("Select a landmark to reassociate.");
                                                OnStepReported("Select a landmark to reassociate.");
                                                Reset();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(contextMenu, x, y);
                        }
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
            }
            finally
            {
                InProgress = false;
            }
        }

        private void OnReselect(object sender, EventArgs e)
        {
            Reset();
            OnReported("Select a landmark to reassociate.");
            OnStepReported("Select a landmark to reassociate.");
        }

        protected override void Reset()
        {
            base.Reset();
            _progress = Progress.Select;
            _selectedLandmarks = null;
            _selectedLandmarksAND = null;
            foreach (MovePoint movePoint in _selectedLandmarkPoints)
            {
                movePoint.Stop();
            }
            _selectedLandmarkPoints.Clear();

            foreach (MoveLine moveLine in _selectedFeatureLines)
            {
                moveLine.Stop();
            }
            _selectedFeatureLines.Clear();
        }

        public override bool Deactivate()
        {
            Reset();
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            foreach (MovePoint movePoint in _selectedLandmarkPoints)
            {
                movePoint.Refresh(hDC);
            }
        }

        // noraini ali - OKT 2020
        private void ReassociateNewStreet(RepositoryFactory repo, GStreet street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Landmark")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        foreach (GLandmark landmark in _selectedLandmarks)
                        {
                            andLandmark.Reassociate(repo, landmark, street.OID);
                        }
                        foreach (GLandmarkAND landmark in _selectedLandmarksAND)
                        {
                            andLandmark.Reassociate(repo, landmark, street.OID);
                        }
                    });
                }
            }
        }

        private void ReassociateNewStreet(RepositoryFactory repo, GStreetAND street, IPoint point)
        {
            QuestionMessageBox confirmBox = new QuestionMessageBox();
            confirmBox.SetCaption("Reassociate Landmark")
                      .SetText("Are you sure you want to reassociate to {0} {1} ?", StringUtils.TrimSpaces(street.TypeValue), street.Name);

            if (confirmBox.Show() == DialogResult.Yes)
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction(() =>
                    {
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        foreach (GLandmark landmark in _selectedLandmarks)
                        {
                            andLandmark.Reassociate(repo, landmark, street.OriId);
                        }
                        foreach (GLandmarkAND landmark in _selectedLandmarksAND)
                        {
                            andLandmark.Reassociate(repo, landmark, street.OriId);
                        }
                    });
                }
            }
        }
    }
}

﻿using ESRI.ArcGIS.Geodatabase;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.UI;
using Geomatic.UI.Forms.Edit;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ESRI.ArcGIS.Geometry;
using Geomatic.Core.Exceptions;

namespace Geomatic.MapTool.LandmarkTools
{
    // Implementation - noraini ali - Apr 2021- Apply STATUS & AND_STATUS - Data Intergration to NEPS

    public class ANDLandmarkTools : ValidateWorkAreaAndStatus
    {
        public void CreateLandmarkAND(RepositoryFactory repo, GLandmark Landmark)
        {
            GLandmark landmark = repo.GetById<GLandmark>(Landmark.OID);
            GLandmarkAND LandmarkAND = repo.NewObj<GLandmarkAND>();
            LandmarkAND.CopyFrom(landmark);
            repo.Insert(LandmarkAND, false);
        }

        public void AddLandmarkAND(RepositoryFactory repo, GLandmark Landmark)
        {
            // update ADM - AreaId
            List<GWorkArea> area;
            area = repo.SpatialSearch<GWorkArea>(Landmark.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();
            area.ForEach(thearea => Landmark.AreaId = thearea.OID);
            repo.UpdateInsertByAND(Landmark, true);

            // create new AND as copy from ADM landmark 
            CreateLandmarkAND(repo, Landmark);

            // update AND - 
            // CreatedBy, DateCreated, Updatedby & DateUpdated
            GLandmarkAND LandmarkAND = Landmark.GetLandmarkANDId();
            UpdateCreatedByUserAND(LandmarkAND);
            UpdateModifiedByUserAND(LandmarkAND);
            repo.UpdateRemainStatus(LandmarkAND, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GLandmark Landmark)
        {
            GLandmarkAND absObjAND = Landmark.GetLandmarkANDId();
            if (absObjAND == null)
            {
                // Create Landmark AND
                CreateLandmarkAND(repo, Landmark);
            }

            // Update AND - User info,STATUS & AND_STATUS as Delete status
            GLandmarkAND LandmarkAND = Landmark.GetLandmarkANDId();
            UpdateModifiedByUserAND(LandmarkAND);
            repo.UpdateDeleteByAND(LandmarkAND, true);

            // update ADM - STATUS & AND_STATUS as Delete status
            repo.UpdateDeleteByAND(Landmark, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Delete(RepositoryFactory repo, GLandmarkAND LandmarkAND)
        {
            // Update ADM feature - AND_STATUS & STATUS as Delete status
            GLandmark Landmark = repo.GetById<GLandmark>(LandmarkAND.OriId);
            repo.UpdateDeleteByAND(Landmark, true);

            // Update AND table -  User info, STATUS & AND_STATUS as Delete status
            UpdateModifiedByUserAND(LandmarkAND);
            repo.UpdateDeleteByAND(LandmarkAND, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Update(RepositoryFactory repo, GLandmark Landmark)
        {
            GLandmarkAND absObjAND = Landmark.GetLandmarkANDId();
            if (absObjAND == null)
            {
                // Create Landmark AND
                CreateLandmarkAND(repo, Landmark);
            }

            //Get data from the AND Table
            GLandmarkAND LandmarkAND = Landmark.GetLandmarkANDId();

            //Display form and update AND Table
            using (EditLandmarkANDForm form = new EditLandmarkANDForm(LandmarkAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // to update Area Id for ADM & AND Landmark
                    List<GWorkArea> area;

                    //Set other required data for AND Landmark Table (AreaId, OriId, status n andstatus)
                    area = repo.SpatialSearch<GWorkArea>(Landmark.Point, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

                    // Update AND Feature - AreadId, Updatedby & DateUpdated
                    area.ForEach(thearea => LandmarkAND.AreaId = thearea.OID);
                    UpdateModifiedByUserAND(LandmarkAND);
                    repo.Update(LandmarkAND);

                    // noraini ali - Apr 2021 - Update ADM Feature, attribute from form, STATUS, AND_STATUS as Updated status
                    area.ForEach(thearea => Landmark.AreaId = thearea.OID);
                    repo.UpdateByAND(Landmark, true);

                    // update work area flag as open 
                    int AreaID = LandmarkAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Update(RepositoryFactory repo, GLandmarkAND LandmarkAND)
        {
            GLandmark Landmark = repo.GetById<GLandmark>(LandmarkAND.OriId);
            if (Landmark == null)
            {
                throw new QualityControlException("Cannot proceed, landmark ADM not found.");
            }

            //Display form and update AND Table
            using (EditLandmarkANDForm form = new EditLandmarkANDForm(LandmarkAND))
            {
                if (form.ShowDialog() != DialogResult.OK)
                {
                    throw new ProcessCancelledException();
                }

                using (new WaitCursor())
                {
                    form.SetValues();

                    // Update AND Feature - AndStatus, Updatedby & DateUpdated
                    UpdateModifiedByUserAND(LandmarkAND);
                    repo.Update(LandmarkAND, true);

                    // update ADM - STATUS & AND_STATUS to update status
                    repo.UpdateByAND(Landmark, true);

                    // update work area flag as open 
                    int AreaID = LandmarkAND.AreaId ?? 0;
                    ReOpenWorkArea(repo, AreaID);
                }
            }
        }

        public void Move(RepositoryFactory repo, GLandmark Landmark, IPoint newPoint)
        {
            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Landmark Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != Landmark.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // if AND landmark not exist so create AND Landmark 
            GLandmarkAND absObjAND = Landmark.GetLandmarkANDId();
            if (absObjAND == null)
            {
                // Create Landmark AND
                CreateLandmarkAND(repo, Landmark);
            }

            // set new location for AND Landmark
            GLandmarkAND LandmarkAND = Landmark.GetLandmarkANDId();
            LandmarkAND.Shape = newPoint;
            UpdateModifiedByUserAND(LandmarkAND);
            repo.UpdateGraphic(LandmarkAND, true);

            // Update ADM feature - AND_STATUS & STATUS as updated Graphic status
            repo.UpdateGraphicByAND(Landmark, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Move(RepositoryFactory repo, GLandmarkAND LandmarkAND, IPoint newPoint)
        {
            GLandmark Landmark = repo.GetById<GLandmark>(LandmarkAND.OriId);
            if (Landmark == null)
            {
                throw new QualityControlException("Cannot proceed, landmark ADM not found.");
            }

            // noraini -Sept 2020
            //Restrict from moving out NOT Owner of work area or Work Area not exist
            List<GWorkArea> area = repo.SpatialSearch<GWorkArea>(newPoint, esriSpatialRelEnum.esriSpatialRelWithin).ToList();

            if (area.Count < 1)
            {
                //System.Diagnostics.Debug.WriteLine("Area Count " + area.Count); //use
                throw new QualityControlException("Landmark Out of Area Bound.");
            }
            area.ForEach(thearea =>
            {
                if (thearea.OID != LandmarkAND.AreaId)
                {
                    throw new QualityControlException("Cannot move out of different work area.");
                }
            });

            // set new location for AND Landmark & STATUS & AND_STATUS (Update Graphic)
            LandmarkAND.Shape = newPoint;
            UpdateModifiedByUserAND(LandmarkAND);
            repo.UpdateGraphic(LandmarkAND, true);

            // Update ADM - STATUS & AND_STATUS - update Graphic status
            repo.UpdateGraphicByAND(Landmark, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GLandmark Landmark, int streetId)
        {
            GLandmarkAND absObjAND = Landmark.GetLandmarkANDId();
            if (absObjAND == null)
            {
                // Create Landmark AND
                CreateLandmarkAND(repo, Landmark);
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            GLandmarkAND LandmarkAND = Landmark.GetLandmarkANDId();
            LandmarkAND.StreetId = streetId;
            UpdateModifiedByUserAND(LandmarkAND);
            repo.Update(LandmarkAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Landmark, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void Reassociate(RepositoryFactory repo, GLandmarkAND LandmarkAND, int streetId)
        {
            GLandmark Landmark = repo.GetById<GLandmark>(LandmarkAND.OriId);
            if (Landmark == null)
            {
                throw new QualityControlException("Cannot proceed, landmark ADM not found.");
            }

            // Update table AND - User info,STATUS & AND_STATUS as Updated status
            LandmarkAND.StreetId = streetId;
            UpdateModifiedByUserAND(LandmarkAND);
            repo.Update(LandmarkAND, true);

            // Update table ADM - STATUS & AND_STATUS as Updated status
            repo.UpdateByAND(Landmark, true);

            // update work area flag as open 
            int AreaID = LandmarkAND.AreaId ?? 0;
            ReOpenWorkArea(repo, AreaID);
        }

        public void ReOpenWorkArea(RepositoryFactory repo, int AreaId)
        {
            if (AreaId > 0)
            {
                GWorkArea workarea = repo.GetById<GWorkArea>(AreaId);
                UpdateWorkAreaCompletedFlag(repo, workarea);
            }
        }

        // noraini ali - OKT 2020
        public List<GLandmark> FilterLandmarkInWorkArea(List<GLandmark> _landmarks, int _workareaid)
        {
            // filter ADM Landmark - get only fresh ADM
            List<GLandmark> landmarks = new List<GLandmark>();
            foreach (GLandmark _landmarkTemp in _landmarks)
            {
                if (_landmarkTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_landmarkTemp.AndStatus.ToString()) || (_landmarkTemp.AndStatus == 0))
                    {
                        if (!string.IsNullOrEmpty(_landmarkTemp.AreaId.ToString()) || (_landmarkTemp.AreaId > 0))
                        {
                            if (_landmarkTemp.AreaId.Value == _workareaid)
                            {
                                landmarks.Add(_landmarkTemp);
                            }
                        }
                    }
                }
            }
            return landmarks;
        }

        public List<GLandmarkAND> FilterLandmarkInWorkArea(List<GLandmarkAND> _landmarks, int _workareaid)
        {
            // filter ADM Landmark - get only fresh ADM
            List<GLandmarkAND> landmarksAND = new List<GLandmarkAND>();
            foreach (GLandmarkAND _landmarkTemp in _landmarks)
            {
                if (_landmarkTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_landmarkTemp.AreaId.ToString()) || _landmarkTemp.AreaId > 0)
                    {
                        if (_landmarkTemp.AreaId.Value == _workareaid)
                        {
                            landmarksAND.Add(_landmarkTemp);
                        }
                    }
                }
            }
            return landmarksAND;
        }

        public void CheckFeatureOnVerification(GLandmark landmark)
        {
            GLandmarkAND landmarkAND = landmark.GetLandmarkANDId();
            if (landmarkAND != null)
            {
                throw new QualityControlException("Unable to edit feature without verification.");
            }
        }

        // noraini ali - OKT 2020
        public List<GStreet> FilterStreetInWorkArea(List<GStreet> _streets, int _workareaid)
        {
            // filter ADM Street - get only fresh ADM
            List<GStreet> streets = new List<GStreet>();
            foreach (GStreet _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (string.IsNullOrEmpty(_streetTemp.AndStatus.ToString()) || (_streetTemp.AndStatus == 0))
                    {
                        if ((!string.IsNullOrEmpty(_streetTemp.AreaId.ToString())) || _streetTemp.AreaId > 0)
                        {
                            if (_streetTemp.AreaId.Value == _workareaid)
                            {
                                streets.Add(_streetTemp);
                            }
                        }
                    }
                }
            }
            return streets;
        }

        public List<GStreetAND> FilterStreetInWorkArea(List<GStreetAND> _streets, int _workareaid)
        {
            // filter AND Street only in this working area
            List<GStreetAND> streetsAND = new List<GStreetAND>();
            foreach (GStreetAND _streetTemp in _streets)
            {
                if (_streetTemp.AndStatus != 2)
                {
                    if (!string.IsNullOrEmpty(_streetTemp.AreaId.ToString()) || _streetTemp.AreaId > 0)
                    {
                        if (_streetTemp.AreaId.Value == _workareaid)
                        {
                            streetsAND.Add(_streetTemp);
                        }
                    }
                }
            }
            return streetsAND;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Carto;
using Geomatic.Core;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Rows;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Utilities.GeometryTrackers;
using ParentClass = Geomatic.Core.Features.GPoi.ParentClass;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.LandmarkTools
{
    public class EditLandmarkTool : EditTool
    {
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public EditLandmarkTool()
        {
            _name = "Edit Landmark";

            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a landmark to edit.");
            OnStepReported("Select a landmark to edit.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GLandmark> landmarks;
                    GLandmark landmark = null;

                    // added by noraini
                    List<GLandmarkAND> landmarksAND;
                    GLandmarkAND landmarkAND = null;
                    //end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        landmarks = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        landmarksAND = repo.SpatialSearch<GLandmarkAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (landmarks.Count == 0 && landmarksAND.Count == 0)
                    {
                        return;
                    }

                    int _workAreaId = 0;
                    if (Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id 
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        _workAreaId = andLandmark.GetWorkAreaId(SegmentName, point);

                        if (_workAreaId > 0)
                        {
                            List<GLandmark> _NewListlandmarks = new List<GLandmark>();
                            List<GLandmarkAND> _NewListlandmarksAND = new List<GLandmarkAND>();

                            if (landmarks.Count > 0)
                            {
                                _NewListlandmarks = andLandmark.FilterLandmarkInWorkArea(landmarks, _workAreaId);
                            }

                            if (landmarksAND.Count > 0)
                            {
                                _NewListlandmarksAND = andLandmark.FilterLandmarkInWorkArea(landmarksAND, _workAreaId);
                            }

                            // to add all feature ADM in new list to use in ChooseFeature Form
                            List<GLandmark> _landmarks = new List<GLandmark>();
                            foreach (GLandmark landmarkTemp in _NewListlandmarks)
                            {
                                _landmarks.Add(landmarkTemp);
                            }

                            foreach (GLandmarkAND landmarkTempAND in _NewListlandmarksAND)
                            {
                                GLandmark Landmark = repo.GetById<GLandmark>(landmarkTempAND.OriId);
                                if (Landmark != null)
                                    _landmarks.Add(Landmark);
                            }

                            landmarkAND = null;
                            if (_landmarks.Count == 0)
                            {
                                return;
                            }
                            else if (_landmarks.Count == 1)
                            {
                                landmark = _landmarks[0];
                                landmarkAND = null;

                                // if AND feature exist
                                GLandmarkAND LandmarkTempAND = landmark.GetLandmarkANDId();
                                if (LandmarkTempAND != null)
                                {
                                    landmarkAND = LandmarkTempAND;
                                    landmark = null;
                                }

                                // noraini ali - OKT 2020 - hightlight feature
                                HighlightLandmarkNStreet(repo, landmark, landmarkAND, true);
                            }
                            else
                            {
                                landmark = ChooseFeature(_landmarks, null);
                                landmarkAND = null;

                                // if OID exist in AND feature
                                GLandmarkAND LandmarkTempAND = repo.GetById<GLandmarkAND>(landmark.OID);
                                if (LandmarkTempAND != null)
                                {
                                    landmarkAND = LandmarkTempAND;
                                    landmark = null;
                                }
                                HighlightLandmarkNStreet(repo, landmark, landmarkAND, true);
                            }
                        }
                        else // feature not in work area
                        {
                            landmark = null;
                            landmarkAND = null;
                            if (landmarks.Count > 0)
                            {
                                landmark = landmarks[0];
                            }
                            else if (landmarks.Count == 0)
                            {
                                if (landmarksAND.Count == 0)
                                {
                                    return;
                                }
                                else if (landmarksAND.Count > 0)
                                {
                                    landmarkAND = landmarksAND[0];
                                }
                            }
                            HighlightLandmarkNStreet(repo, landmark, landmarkAND, true);
                        }
                    }
                    // end
                    else  // other group
                    {
                        // checking user non-AND cannot touch feature in Working Area
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        //if (!andLandmark.checkingPointInWorkArea(SegmentName, point))
                        //{
                        //    return;
                        //}

                        landmarkAND = null;
                        if (landmarks.Count == 1)
                        {
                            landmark = landmarks[0];
                            HighlightLandmarkNStreet(repo, landmark, landmarkAND, true);
                        }
                        else if (landmarks.Count > 1)
                        {
                            landmark = ChooseFeature(landmarks, null);
                            HighlightLandmarkNStreet(repo, landmark, landmarkAND, true);
                        }

                        if (landmark == null)
                        {
                            return;
                        }
                    }

                    bool success = false;

                    try
                    {
                        using (new WaitCursor())
                        {
                            repo.StartTransaction();
                        }

                        //added by noraini ali -Mei 2020
                        // checking feature can edit or review only
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        bool _CanEdit = andLandmark.ValidateUserCanEdit(SegmentName, _workAreaId);
                        if (_CanEdit)
                        {
                            if (landmark != null)
                            {                               
                                andLandmark.Update(repo, landmark);
                                OnReported("Landmark updated. {0}", landmark.OID);
                            }
                            else
                            {
                                andLandmark.Update(repo, landmarkAND);
                                OnReported("Landmark updated. {0}", landmarkAND.OID);
                            }

                            OnReported("Select a landmark in work area to edit or landmark to review.");
                            OnStepReported("Select a landmark in work area to edit or landmark to review.");
                        }
                        // end
                        else
                        {
                            // noraini ali - NOV 2020
                            if (landmark == null)
                            {
                                andLandmark.Update(repo, landmarkAND);
                                highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                return;
                            }

                            // noraini ali - Mei 2021 - check feature lock by AND
                            highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                            andLandmark.CheckFeatureLock(landmark.AreaId, SegmentName);

                            // noraini ali - Mei 2021 - check feature on verification
                            andLandmark.CheckFeatureOnVerification(landmark);

                            using (EditLandmarkForm form = new EditLandmarkForm(landmark))
                            {
                                if (form.ShowDialog() != DialogResult.OK)
                                {
                                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                                    return;
                                }

                                // end add
                                using (new WaitCursor())
                                {
                                    form.SetValues();
                                    repo.Update(landmark);
                                }
                            }
                            OnReported("Landmark updated. {0}", landmark.OID);
                            OnReported("Select a landmark to edit.");
                            OnStepReported("Select a landmark to edit.");
                        }
                        success = true;
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                    catch
                    {
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                        throw;
                    }
                    finally
                    {
                        if (success)
                        {
                            repo.EndTransaction();
                        }
                        else
                        {
                            repo.AbortTransaction();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        // noraini ali - OKT 2020 - hightlight feature
        private void HighlightLandmarkNStreet(RepositoryFactory repo, GLandmark landmark, GLandmarkAND landmarkAND, bool status)
        {
            if (status)
            {
                if (landmarkAND != null)
                {
                    highlightLandmark(repo, landmarkAND.OriId, true, _selectedFeaturePoints);
                    if (landmarkAND.StreetId.HasValue)
                    {
                        highlightStreet(repo, landmarkAND.StreetId.Value, true, _selectedFeatureLines);
                    }
                }
                else
                {
                    highlightLandmark(repo, landmark.OID, true, _selectedFeaturePoints);
                    if (landmark.StreetId.HasValue)
                    {
                        highlightStreet(repo, landmark.StreetId.Value, true, _selectedFeatureLines);
                    }
                }
            }
            else
            {
                if (landmarkAND != null)
                {
                    if (landmarkAND.StreetId.HasValue)
                    {
                        highlightStreet(repo, landmarkAND.StreetId.Value, true, _selectedFeatureLines);
                    }
                }
                else
                {
                    if (landmark.StreetId.HasValue)
                    {
                        highlightStreet(repo, landmark.StreetId.Value, true, _selectedFeatureLines);
                    }
                }
            }
        }
    }
}

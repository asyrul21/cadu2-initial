﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Utilities.GeometryTrackers;
using Geomatic.Core.Features;
using Geomatic.Core.Repositories;
using Geomatic.Core.Sessions;
using Geomatic.Core.Exceptions;
using Geomatic.UI.FeedBacks;

namespace Geomatic.MapTool.LandmarkTools
{
    public class DeleteLandmarkTool : DeleteTool
    {
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;

        public DeleteLandmarkTool()
        {
            _name = "Delete Landmark";
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        public override void OnClick()
        {
            base.OnClick();
            OnReported("Select a landmark to delete.");
            OnStepReported("Select a landmark to delete.");
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);

            if (InProgress)
            {
                return;
            }

            if (button == MouseKey.Left)
            {
                try
                {
                    InProgress = true;

                    RepositoryFactory repo = new RepositoryFactory(SegmentName);

                    IGeometry geometry = new RectangleTracker().TrackNew(ActiveView, point);

                    List<GLandmark> landmarks;
                    GLandmark landmark = null;

                    // noraini ali - Mei 2020
                    List<GLandmarkAND> landmarksAND;
                    GLandmarkAND landmarkAND = null;
                    // end

                    using (new WaitCursor())
                    {
                        if (geometry == null)
                        {
                            return;
                        }

                        landmarks = repo.SpatialSearch<GLandmark>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                        landmarksAND = repo.SpatialSearch<GLandmarkAND>(geometry, esriSpatialRelEnum.esriSpatialRelContains).ToList();
                    }

                    // added by noraini ali - Mei 2020
                    if (landmarks.Count == 0 && landmarksAND.Count == 0)
                    {
                        return;
                    }

                    if (Session.User.GetGroup().Name == "AND")
                    {
                        // get Work Area Id & Checking point in Working Area & Owner
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        andLandmark.CheckWithinWorkArea(SegmentName, point);
                        andLandmark.CheckUserOwnWorkArea(SegmentName, point);
                        int _workAreaId = andLandmark.GetWorkAreaId(SegmentName, point);

                        List<GLandmark> _NewListLandmarks = new List<GLandmark>();
                        List<GLandmarkAND> _NewListLandmarksAND = new List<GLandmarkAND>();

                        if (landmarks.Count > 0)
                        {
                            _NewListLandmarks = andLandmark.FilterLandmarkInWorkArea(landmarks, _workAreaId);
                        }

                        if (landmarksAND.Count > 0)
                        {
                            _NewListLandmarksAND = andLandmark.FilterLandmarkInWorkArea(landmarksAND, _workAreaId);
                        }

                        // create new list to store all ADM feature to use in ChooseForm
                        List<GLandmark> _landmarks = new List<GLandmark>();
                        foreach (GLandmark landmarkTemp in _NewListLandmarks)
                        {
                            _landmarks.Add(landmarkTemp);
                        }

                        foreach (GLandmarkAND landmarkTempAND in _NewListLandmarksAND)
                        {
                            GLandmark Landmark = repo.GetById<GLandmark>(landmarkTempAND.OriId);
                            if (Landmark != null)
                                _landmarks.Add(Landmark);
                        }

                        landmarkAND = null;
                        if (_landmarks.Count == 0)
                        {
                            return;
                        }
                        else if (_landmarks.Count == 1)
                        {
                            landmark = _landmarks[0];

                            // AND feature exist
                            GLandmarkAND LandmarkTempAND = landmark.GetLandmarkANDId();
                            if (LandmarkTempAND != null)
                            {
                                landmarkAND = LandmarkTempAND;
                                landmark = null;
                            }
                        }
                        else
                        {
                            landmark = ChooseFeature(_landmarks, null);

                            // if OID exist in AND feature
                            GLandmarkAND LandmarkTempAND = repo.GetById<GLandmarkAND>(landmark.OID);
                            if (LandmarkTempAND != null)
                            {
                                landmarkAND = LandmarkTempAND;
                                landmark = null;
                            }
                        }

                        // noraini ali - OKT 2020 - hightlight feature
                        if (landmarkAND != null)
                        {
                            highlightLandmark(repo, landmarkAND.OriId, true, _selectedFeaturePoints);
                            if (landmarkAND.StreetId.HasValue)
                            {
                                highlightStreet(repo, landmarkAND.StreetId.Value, true, _selectedFeatureLines);
                            }
                        }
                        else
                        {
                            highlightLandmark(repo, landmark.OID, true, _selectedFeaturePoints);
                            if (landmark.StreetId.HasValue)
                            {
                                highlightStreet(repo, landmark.StreetId.Value, true, _selectedFeatureLines);
                            }
                        }
                    }
                    else  //OTHERS GROUP USER
                    {
                        // checking user non-AND cannot touch feature in Working Area
                        ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                        //if (!andLandmark.checkingPointInWorkArea(SegmentName, point))
                        //{
                        //    return;
                        //}

                        if (landmarks.Count == 1)
                        {
                            landmark = landmarks[0];
                        }
                        else if (landmarks.Count > 1)
                        {
                            landmark = ChooseFeature(landmarks, null);
                        }

                        if (landmark == null)
                        {
                            return;
                        }

                        // noraini ali - Mei 2021 - check feature lock by AND
                        andLandmark.CheckFeatureLock(landmark.AreaId, SegmentName);

                        // noraini ali - Mei 2021 - check feature on verification
                        andLandmark.CheckFeatureOnVerification(landmark);


                        // noraini ali - OKT 2020 - hightlight feature 
                        highlightLandmark(repo, landmark.OID, true, _selectedFeaturePoints);
                        if (landmark.StreetId.HasValue)
                        {
                            highlightStreet(repo, landmark.StreetId.Value, true, _selectedFeatureLines);
                        }
                    }

                    using (QuestionMessageBox confirmBox = new QuestionMessageBox())
                    {
                        // noraini ali - Apr 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            if (landmarkAND != null)
                            {
                               // GLandmarkAND deletelandmarkAND = landmark.GetLandmarkANDId();
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                        .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                        .SetCaption("Delete Landmark")
                                        .SetText("Are you sure you want to delete this landmark?\nId: {0}\n{1} {2}", landmarkAND.OID, landmarkAND.Name, landmarkAND.Name2);
                            }
                            else // 
                            {
                                confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                    .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                    .SetCaption("Delete Landmark")
                                    .SetText("Are you sure you want to delete this landmark?\nId: {0}\n{1} {2}", landmark.OID, landmark.Name, landmark.Name2);
                            }
                        }
                        else
                        {
                            confirmBox.SetButtons(MessageBoxButtons.YesNo)
                                      .SetDefaultButton(MessageBoxDefaultButton.Button2)
                                      .SetCaption("Delete Landmark")
                                      .SetText("Are you sure you want to delete this landmark?\nId: {0}\n{1} {2}", landmark.OID, landmark.Name, landmark.Name2);
                        }

                        if (confirmBox.Show() == DialogResult.No)
                        {
                            return;
                        }
                    }

                    using (new WaitCursor())
                    {
                        if (Session.User.GetGroup().Name != "AND")
                        {
                            if (landmark.HasPoi())
                            {
                                throw new Exception("Unable to delete landmark. Landmark has poi.");
                            }
                        }
                        repo.StartTransaction(() =>
                        {
                            // noraini ali - Apr 2020
                            if (Session.User.GetGroup().Name == "AND")
                            {
                                if (landmark != null && landmarkAND == null)
                                {
                                    ANDLandmarkTools andLandmark2 = new ANDLandmarkTools();
                                    andLandmark2.Delete(repo, landmark);
                                }
                                else
                                {
                                    ANDLandmarkTools andLandmark2 = new ANDLandmarkTools();
                                    andLandmark2.Delete(repo, landmarkAND);
                                }
                            }
                            // end
                            else
                            { 
                                foreach (GLandmarkText text in landmark.GetTexts())
                                {
                                    repo.Delete(text);
                                }

                                OnReported("Landmark deleted. {0}", landmark.OID);
                                repo.Delete(landmark);
                            }
                        });

                        OnReported("Select a landmark to delete.");
                        OnStepReported("Select a landmark to delete.");
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }
    }
}

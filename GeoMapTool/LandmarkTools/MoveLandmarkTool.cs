﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Exceptions;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.UI.Utilities;
using Geomatic.UI.Forms.Choose;

namespace Geomatic.MapTool.LandmarkTools
{
    public class MoveLandmarkTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GLandmark _selectedLandmark;
        protected MovePoint _selectedLandmarkPoint;

        protected GLandmarkAND _selectedLandmarkAND;
        protected bool bool_confirmMove = true;

        private const string Message1 = "Select a landmark to move.";
        private const string Message2 = "Drag selected landmark to move.";

        #endregion

        public MoveLandmarkTool()
        {
            _name = "Move Landmark";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedLandmarkPoint == null)
            {
                _selectedLandmarkPoint = new MovePoint(ScreenDisplay, _colorRed);
            }
            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            // added by noraini ali - Mei 2020
                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                            {
                                if (_selectedLandmark != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedLandmark.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                                if (_selectedLandmarkAND != null)
                                {
                                    if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedLandmarkAND.Point))
                                    {
                                        _progress = Progress.Moving;
                                    }
                                }
                            }
                            // end
                            else
                            {
                                if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedLandmark.Point))
                                {
                                    _progress = Progress.Moving;
                                }
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedLandmarkPoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmark> landmarks = SelectLandmark(repo, point);
                                List<GLandmarkAND> landmarksAND = SelectLandmarkAND(repo, point);

                                // added by noraini ali - Mei 2020
                                if (landmarks.Count == 0 && landmarksAND.Count == 0)
                                {
                                    break;
                                }

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    _selectedLandmark = null;
                                    _selectedLandmarkAND = null;

                                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();

                                    if (landmarks.Count > 0 && landmarksAND.Count == 0)
                                    {
                                        GLandmark landmark = landmarks[0];

                                        andLandmark.CheckWithinWorkArea(SegmentName, landmark);
                                        andLandmark.CheckUserOwnWorkArea(SegmentName, landmark);

                                        _selectedLandmarkPoint.Point = landmark.Point;
                                        _selectedLandmarkPoint.Start(landmark.Point);
                                        _selectedLandmark = landmark;

                                        GStreet street = landmark.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                    else if (landmarks.Count == 0 && landmarksAND.Count > 0)
                                    {
                                        GLandmarkAND landmarkAND = landmarksAND[0];

                                        andLandmark.CheckWithinWorkArea(SegmentName, landmarkAND);
                                        andLandmark.CheckUserOwnWorkArea(SegmentName, landmarkAND);

                                        _selectedLandmarkPoint.Point = landmarkAND.Point;
                                        _selectedLandmarkPoint.Start(landmarkAND.Point);
                                        _selectedLandmarkAND = landmarkAND;

                                        GStreet street = landmarkAND.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                    else if (landmarks.Count > 0 && landmarksAND.Count > 0)
                                    {
                                        GLandmark landmark = landmarks[0];
                                        GLandmarkAND landmarkAND = landmarksAND[0];

                                        andLandmark.CheckWithinWorkArea(SegmentName, landmarkAND);
                                        andLandmark.CheckUserOwnWorkArea(SegmentName, landmarkAND);

                                        // selected as same building ADM & AND overlapping location
                                        // So active AND feature only
                                        if (landmark.OID == landmarkAND.OriId)
                                        {
                                            _selectedLandmarkPoint.Point = landmarkAND.Point;
                                            _selectedLandmarkPoint.Start(landmarkAND.Point);
                                            _selectedLandmarkAND = landmarkAND;

                                            GStreet street = landmarkAND.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Start(point);
                                            }
                                        }
                                    }
                                }
                                // end added
                                else // non AND User
                                {
                                    // checking user non-AND cannot touch feature in Working Area
                                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                    //if (!andLandmark.checkingPointInWorkArea(SegmentName, point))
                                    //{
                                    //    return;
                                    //}

                                    if (landmarks.Count > 0)
                                    {
                                        GLandmark landmark = landmarks[0];
                                        _selectedLandmarkPoint.Point = landmark.Point;
                                        _selectedLandmarkPoint.Start(landmark.Point);
                                        _selectedLandmark = landmark;

                                        // noraini ali - Mei 2021 - check feature lock by AND
                                        andLandmark.CheckFeatureLock(landmark.AreaId, SegmentName);

                                        // noraini ali - Mei 2021 - check feature on verification
                                        andLandmark.CheckFeatureOnVerification(landmark);

                                        GStreet street = landmark.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Start(point);
                                        }
                                    }
                                }

                                _progress = Progress.TryMove;
                                OnReported(Message2);
                                OnStepReported(Message2);
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmark> landmarks = SelectLandmark(repo, point);
                                List<GLandmarkAND> landmarksAND = SelectLandmarkAND(repo, point);

                                if (landmarks.Count == 0 && landmarksAND.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedLandmarkPoint.Stop();
                                    _selectedLandmark = null;
                                    _selectedLandmarkAND = null;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                // added by noraini ali - Mei 2020
                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();

                                    if (landmarks.Count > 0 && landmarksAND.Count == 0)
                                    {
                                        GLandmark landmark = landmarks[0];

                                        if (!_selectedLandmark.Equals(landmark))
                                        {
                                            _selectedLandmarkPoint.Point = landmark.Point;
                                            _selectedLandmarkPoint.Stop();
                                            _selectedLandmarkPoint.Start(landmark.Point);
                                            _selectedLandmark = landmark;

                                            GStreet street = landmark.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (landmarks.Count == 0 && landmarksAND.Count > 0)
                                    {
                                        GLandmarkAND landmarkAND = landmarksAND[0];

                                        if (!_selectedLandmarkAND.Equals(landmarkAND))
                                        {
                                            _selectedLandmarkPoint.Point = landmarkAND.Point;
                                            _selectedLandmarkPoint.Stop();
                                            _selectedLandmarkPoint.Start(landmarkAND.Point);
                                            _selectedLandmarkAND = landmarkAND;

                                            GStreet street = landmarkAND.GetStreet();
                                            if (street != null)
                                            {
                                                _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                _parentRadius.Stop();
                                                _parentRadius.Start(point);
                                            }
                                            else
                                            {
                                                _parentRadius.Stop();
                                            }
                                            break;
                                        }
                                    }
                                    else if (landmarks.Count > 0 && landmarksAND.Count > 0)
                                    {
                                        GLandmark landmark = landmarks[0];
                                        GLandmarkAND landmarkAND = landmarksAND[0];

                                        andLandmark.CheckWithinWorkArea(SegmentName, landmarkAND);
                                        andLandmark.CheckUserOwnWorkArea(SegmentName, landmarkAND);

                                        // selected as same building ADM & AND overlapping location
                                        // So active AND feature only
                                        if (landmark.OID == landmarkAND.OriId)
                                        {
                                            if (!_selectedLandmarkAND.Equals(landmarkAND))
                                            {
                                                _selectedLandmarkPoint.Point = landmarkAND.Point;
                                                _selectedLandmarkPoint.Stop();
                                                _selectedLandmarkPoint.Start(landmarkAND.Point);
                                                _selectedLandmarkAND = landmarkAND;

                                                GStreet street = landmarkAND.GetStreet();
                                                if (street != null)
                                                {
                                                    _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                                    _parentRadius.Stop();
                                                    _parentRadius.Start(point);
                                                }
                                                else
                                                {
                                                    _parentRadius.Stop();
                                                }
                                                break;
                                            }
                                        }
                                    }
                                } 
                                // end added
                                else  // non AND User
                                {
                                    if (landmarks.Count == 0)
                                    {
                                        _progress = Progress.Select;
                                        _parentRadius.Stop();
                                        _selectedLandmarkPoint.Stop();
                                        _selectedLandmark = null;
                                        OnReported(Message1);
                                        OnStepReported(Message1);
                                        break;
                                    }

                                    GLandmark landmark = landmarks[0];
                                    if (!_selectedLandmark.Equals(landmark))
                                    {
                                        _selectedLandmarkPoint.Point = landmark.Point;
                                        _selectedLandmarkPoint.Stop();
                                        _selectedLandmarkPoint.Start(landmark.Point);
                                        _selectedLandmark = landmark;

                                        GStreet street = landmark.GetStreet();
                                        if (street != null)
                                        {
                                            _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                            _parentRadius.Stop();
                                            _parentRadius.Start(point);
                                        }
                                        else
                                        {
                                            _parentRadius.Stop();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }

                            GStreet street;
                            IPoint newPoint;
                            List<GStreet> streets;
                            try
                            {
                                using (new WaitCursor())
                                {
                                    //newPoint = (IPoint)_selectedLandmarkPoint.Stop();
                                    _parentRadius.Stop();

                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                    {
                                        if (_selectedLandmark != null)
                                        {
                                            street = _selectedLandmark.GetStreet();
                                        }
                                        else
                                        {
                                            street = _selectedLandmarkAND.GetStreet();
                                        }
                                        
                                    }
                                    else
                                    {
                                        street = _selectedLandmark.GetStreet();
                                    }
                                }

                                // noraini ali - Sept 2020-Confirmation to move feature
                                // process move the original feature (ADM)
                                // confirm to move so need to update ADM feature to previous location
                                // and create AND at new location

                                if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                {
                                    bool_confirmMove = true;

                                    QuestionMessageBox confirmBox = new QuestionMessageBox();
                                    confirmBox.SetCaption("Move Landmark")
                                        .SetButtons(MessageBoxButtons.OKCancel)
                                        .SetText("Confirm Move Landmark to New Location ?");

                                    if (confirmBox.Show() == DialogResult.Cancel)
                                    {
                                        bool_confirmMove = false;
                                    }
                                }

                                newPoint = (IPoint)_selectedLandmarkPoint.Stop();

                                if (bool_confirmMove)
                                {
                                    if (street != null)
                                    {
                                        using (new WaitCursor())
                                        {
                                            double distance = newPoint.DistanceTo(street.Shape);
                                            streets = repo.SpatialSearch<GStreet>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                                        }
                                        if (streets.Count > 0)
                                        {
                                            GStreet nearestStreet = GeometryUtils.GetNearestFeature(streets, newPoint);
                                            if (!nearestStreet.Equals(street))
                                            {
                                                QuestionMessageBox chooseNewBox = new QuestionMessageBox();
                                                chooseNewBox.SetCaption("Move Landmark")
                                                            .SetText("Another street nearby. Choose new street?");

                                                if (chooseNewBox.Show() == DialogResult.Yes)
                                                {
                                                    GStreet newStreet = ChooseFeature(streets, nearestStreet);
                                                    if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                                    {
                                                        // noraini ali - OKT 2020 - cater if choose AND Street
                                                        GStreetAND newStreetAND = repo.GetById<GStreetAND>(newStreet.OID);
                                                        if (newStreetAND != null)
                                                        {
                                                            if (_selectedLandmarkAND != null)
                                                            {
                                                                _selectedLandmarkAND.StreetId = newStreetAND.OriId;
                                                                repo.UpdateGraphic(_selectedLandmarkAND, true);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (_selectedLandmarkAND != null)
                                                            {
                                                                _selectedLandmarkAND.StreetId = newStreet.OID;
                                                                repo.UpdateGraphic(_selectedLandmarkAND, true);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _selectedLandmark.StreetId = newStreet.OID;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    using (new WaitCursor())
                                    {
                                        repo.StartTransaction(() =>
                                        {
                                            if (Core.Sessions.Session.User.GetGroup().Name == "AND")
                                            {
                                                //if (bool_confirmMove)
                                                //{
                                                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                                                    if (_selectedLandmark != null)
                                                    {
                                                        andLandmark.Move(repo, _selectedLandmark, newPoint);
                                                    }
                                                    else
                                                    {
                                                        GLandmark Landmark = repo.GetById<GLandmark>(_selectedLandmarkAND.OriId);
                                                        andLandmark.Move(repo, Landmark, newPoint);
                                                    }
                                                //}
                                            }
                                            else
                                            {
                                                _selectedLandmark.Shape = newPoint;
                                                //repo.Update(_selectedLandmark);
                                                repo.UpdateGraphic(_selectedLandmark, true);

                                                foreach (GPoi poi in _selectedLandmark.GetPois())
                                                {
                                                    poi.Shape = newPoint;
                                                    repo.Update(poi);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedLandmark = null;
                                _selectedLandmarkAND = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GLandmark> SelectLandmark(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmark> query = new Query<GLandmark>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        // added by noraini ali - Mei 2020
        private List<GLandmarkAND> SelectLandmarkAND(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }
        // end

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedLandmarkPoint != null)
            {
                _selectedLandmarkPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedLandmarkPoint != null)
            {
                _selectedLandmarkPoint.Stop();
                _selectedLandmarkPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedLandmarkPoint != null)
            {
                _selectedLandmarkPoint.Refresh(hDC);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Geomatic.UI;
using Geomatic.UI.Forms;
using Geomatic.UI.Forms.Choose;
using Geomatic.UI.Forms.Add;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.UI.Forms.Edit;
using Geomatic.UI.Utilities;
using Geomatic.UI.FeedBacks;
using Geomatic.Core.Utilities;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Exceptions;
using Earthworm.AO;
using Geomatic.Core.Sessions;

namespace Geomatic.MapTool.LandmarkTools
{
    public class AddLandmarkTool : AddTool
    {
        #region Fields

        protected FeedBackCollection _area;

        protected IRgbColor _color3 = ColorUtils.Get(Color.Red);
        protected const double RADIUS1 = 10;
        protected const double RADIUS2 = 30;
        protected const double RADIUS3 = 100;
        private const string Message1 = "Click on map location to add landmark.";
        private const string Message2 = "Select a street from the list for landmark association.";
        // noraini ali - OKT 2020
        // variable to highlight
        protected List<MovePoint> _selectedFeaturePoints;
        protected List<MoveLine> _selectedFeatureLines;
        #endregion

        public AddLandmarkTool()
        {
            _name = "Add Landmark";
            // noraini ali - OKT 2020 - highlight feature
            _selectedFeaturePoints = new List<MovePoint>();
            _selectedFeatureLines = new List<MoveLine>();
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();
            if (_area == null)
            {
                _area = new FeedBackCollection();
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            IPoint point = DisplayTransformation.ToMapPoint(x, y);
            if (!_area.IsStarted)
            {
                MovePolygon movePolygon1 = new MovePolygon(ScreenDisplay, _color1, _color1, 1);
                movePolygon1.Polygon = (IPolygon)point.Buffer(RADIUS1);

                _area.Add(movePolygon1);

                MovePolygon movePolygon2 = new MovePolygon(ScreenDisplay, _color2, _color2, 1);
                movePolygon2.Polygon = (IPolygon)point.Buffer(RADIUS2);

                _area.Add(movePolygon2);

                MovePolygon movePolygon3 = new MovePolygon(ScreenDisplay, _color3, _color3, 1);
                movePolygon3.Polygon = (IPolygon)point.Buffer(RADIUS3);

                _area.Add(movePolygon3);

                _area.Start(point);
            }

            _area.MoveTo(point);
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (button == MouseKey.Left)
            {
                if (InProgress)
                {
                    return;
                }
                try
                {
                    InProgress = true;

                    IPoint point = DisplayTransformation.ToMapPoint(x, y);

                    CreateLandmark(point);
                }
                catch (Exception ex)
                {
                    using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                    {
                        box.Show();
                    }
                    highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                }
                finally
                {
                    InProgress = false;
                }
            }
            else if (button == MouseKey.Right)
            {
                MapDocument.ShowContextMenu(x, y);
            }
        }

        protected virtual void CreateLandmark(IPoint point)
        {
            RepositoryFactory repo = new RepositoryFactory(SegmentName);

            List<GStreet> streets;
            GStreet street = null;

            List<GStreetAND> streetsAND;
            GStreetAND streetAND = null;

            using (new WaitCursor())
            {
                streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS1), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
            }

            if (streets.Count == 0 )
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS2), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }

            if (streets.Count == 0)
            {
                using (new WaitCursor())
                {
                    streets = repo.SpatialSearch<GStreet>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                    streetsAND = repo.SpatialSearch<GStreetAND>(point.Buffer(RADIUS3), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                }
            }

            //noraini ali- April 2020
            if (Session.User.GetGroup().Name == "AND")
            {
                // get Work Area Id
                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                andLandmark.CheckWithinWorkArea(SegmentName, point);
                andLandmark.CheckUserOwnWorkArea(SegmentName, point);
                int _workAreaId = andLandmark.GetWorkAreaId(SegmentName, point);

                // filter street ADM & AND in this working area only
                List<GStreet> _NewListStreets = new List<GStreet>();
                List<GStreetAND> _NewListStreetsAND = new List<GStreetAND>();

                if (streets.Count > 0)
                {
                    _NewListStreets = andLandmark.FilterStreetInWorkArea(streets, _workAreaId);
                }

                if (streetsAND.Count > 0)
                {
                    _NewListStreetsAND = andLandmark.FilterStreetInWorkArea(streetsAND, _workAreaId);
                }

                // create new list to store all ADM feature to use in ChooseForm
                List<GStreet> _streets = new List<GStreet>();
                foreach (GStreet streetTemp in _NewListStreets)
                {
                    _streets.Add(streetTemp);
                }

                foreach (GStreetAND streetTempAND in _NewListStreetsAND)
                {
                    GStreet Street = repo.GetById<GStreet>(streetTempAND.OriId);
                    if (Street != null)
                        _streets.Add(Street);
                }

                streetAND = null;
                if (_streets.Count == 0)
                {
                    throw new Exception("No streets found within radius in this working area");
                }
                else if (_streets.Count == 1)
                {
                    street = _streets[0];
                    streetAND = null;

                    // if OID exist in AND feature
                    GStreetAND StreetTempAnd = street.GetStreetANDId();
                    if (StreetTempAnd != null)
                    {
                        streetAND = StreetTempAnd;
                        street = null;
                    }
                }
                else // _streets.count > 1
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(_streets, GeometryUtils.GetNearestFeature(_streets, point));
                    streetAND = null;

                    // if OID exist in AND feature
                    GStreetAND StreetTempAND = repo.GetById<GStreetAND>(street.OID);
                    if (StreetTempAND != null)
                    {
                        streetAND = StreetTempAND;
                        street = null;
                    }
                }

                // noraini ali - OKT 2020 - hightlight feature
                if (streetAND != null)
                {
                    highlightStreet(repo, streetAND.OriId, true, _selectedFeatureLines);
                }
                else
                {
                    highlightStreet(repo, street.OID, true, _selectedFeatureLines);
                }

            }
            //end
            else  //OTHERS USER
            {
                // checking user non-AND cannot touch feature in Working Area
                ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                andLandmark.checkingPointInWorkArea(SegmentName, point);
                //if (!andLandmark.checkingPointInWorkArea(SegmentName, point))
                //{
                //    return;
                //}

                if (streets.Count == 1)
                {
                    street = streets[0];
                }
                else if (streets.Count > 1)
                {
                    OnReported(Message2);
                    OnStepReported(Message2);
                    street = ChooseFeature(streets, GeometryUtils.GetNearestFeature(streets, point));
                }
                else if (streets.Count == 0)
                {
                    throw new Exception("No streets found within radius");
                }

                // noraini ali - OKT 2020 - hightlight feature
                highlightStreet(repo, street.OID, true, _selectedFeatureLines);
            }

            bool success = false;

            try
            {
                using (new WaitCursor())
                {
                    repo.StartTransaction();
                }

                GLandmark newLandmark = repo.NewObj<GLandmark>();
                newLandmark.Init();
                if (streetAND != null)
                {
                    newLandmark.StreetId = streetAND.OriId;
                }
                else
                {
                    newLandmark.StreetId = street.OID;
                }
                newLandmark.Shape = point;

                //noraini ali- Jul 2020 - Set CreatedBy, DateCreated, Updatedby & DateUpdated to display in form
                if (Session.User.GetGroup().Name == "AND")
                {
                    ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                    andLandmark.UpdateCreatedByUserAND(newLandmark);
                    andLandmark.UpdateModifiedByUserAND(newLandmark);
                }

                newLandmark = repo.Insert(newLandmark, false);

                using (AddLandmarkForm form = new AddLandmarkForm(newLandmark))
                {
                    if (form.ShowDialog() != DialogResult.OK)
                    {
                        throw new ProcessCancelledException();
                    }

                    using (new WaitCursor())
                    {
                        form.SetValues();
                        //repo.Update(newLandmark);
                        repo.UpdateRemainStatus(newLandmark, true);

                        //noraini ali- April 2020
                        if (Session.User.GetGroup().Name == "AND")
                        {
                            ANDLandmarkTools andLandmark = new ANDLandmarkTools();
                            andLandmark.AddLandmarkAND(repo, newLandmark);
                        }
                        // noraini ali - OKT 2020 - hightlight feature
                        highlightLandmark(repo, newLandmark.OID, true, _selectedFeaturePoints);

                        // end
                        OnReported("New landmark created. {0}", newLandmark.OID);
                        OnReported(Message1);
                        OnStepReported(Message1);
                        highlightClear(_selectedFeaturePoints, _selectedFeatureLines);
                    }
                }
                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (success)
                {
                    repo.EndTransaction();
                }
                else
                {
                    repo.AbortTransaction();
                }
            }
        }

        public override bool Deactivate()
        {
            if (_area != null)
            {
                _area.Stop();
                _area = null;
            }

            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_area != null)
            {
                _area.Refresh(hDC);
            }
        }
    }
}

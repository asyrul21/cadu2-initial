﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Geodatabase;
using Geomatic.UI;
using Geomatic.UI.Forms.MessageBoxes;
using Geomatic.Core.Repositories;
using Geomatic.Core.Features;
using Geomatic.Core.Search;
using Geomatic.Core.Utilities;
using Geomatic.UI.FeedBacks;
using Earthworm.AO;
using Geomatic.UI.Utilities;

namespace Geomatic.MapTool.LandmarkTools
{
    public class MoveLandmarkANDTool : MoveTool
    {
        #region Fields

        protected MovePolygon _parentRadius;
        protected GLandmarkAND _selectedLandmarkAND;
        protected MovePoint _selectedLandmarkANDPoint;

        private const string Message1 = "Select a landmark (AND) to move.";
        private const string Message2 = "Drag selected landmark (AND) to move.";

        #endregion

        public MoveLandmarkANDTool()
        {
            _name = "Move Landmark (AND)";
        }

        /// <summary>
        /// Occurs when this tool is clicked
        /// </summary>
        public override void OnClick()
        {
            base.OnClick();

            if (_selectedLandmarkANDPoint == null)
            {
                _selectedLandmarkANDPoint = new MovePoint(ScreenDisplay, ColorUtils.Select);
            }
            if (_parentRadius == null)
            {
                _parentRadius = new MovePolygon(ScreenDisplay, _color);
            }
            OnReported(Message1);
            OnStepReported(Message1);
        }

        public override void OnMouseDown(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }

                        using (new WaitCursor())
                        {
                            if (point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1)).Contains(_selectedLandmarkAND.Point))
                            {
                                _progress = Progress.Moving;
                            }
                        }

                        break;
                    case Progress.Moving:
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseMove(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        break;
                    case Progress.TryMove:
                        break;
                    case Progress.Moving:
                        if (button != MouseKey.Left)
                        {
                            break;
                        }
                        _selectedLandmarkANDPoint.MoveTo(point);
                        break;
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        public override void OnMouseUp(int button, int shift, int x, int y)
        {
            if (InProgress)
            {
                return;
            }

            try
            {
                IPoint point = DisplayTransformation.ToMapPoint(x, y);
                RepositoryFactory repo = new RepositoryFactory(SegmentName);

                InProgress = true;

                switch (_progress)
                {
                    case Progress.Select:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmarkAND> landmarks = SelectLandmarkAND(repo, point);
                                if (landmarks.Count > 0)
                                {
                                    GLandmarkAND landmark = landmarks[0];
                                    _selectedLandmarkANDPoint.Point = landmark.Point;
                                    _selectedLandmarkANDPoint.Start(landmark.Point);
                                    _selectedLandmarkAND = landmark;

                                    GStreet street = landmark.GetStreet();
                                    if (street != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                        _parentRadius.Start(point);
                                    }

                                    _progress = Progress.TryMove;
                                    OnReported(Message2);
                                    OnStepReported(Message2);
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.TryMove:
                        if (button == MouseKey.Left)
                        {
                            using (new WaitCursor())
                            {
                                List<GLandmarkAND> landmarks = SelectLandmarkAND(repo, point);
                                if (landmarks.Count == 0)
                                {
                                    _progress = Progress.Select;
                                    _parentRadius.Stop();
                                    _selectedLandmarkANDPoint.Stop();
                                    _selectedLandmarkAND = null;
                                    OnReported(Message1);
                                    OnStepReported(Message1);
                                    break;
                                }

                                GLandmarkAND landmark = landmarks[0];
                                if (!_selectedLandmarkAND.Equals(landmark))
                                {
                                    _selectedLandmarkANDPoint.Point = landmark.Point;
                                    _selectedLandmarkANDPoint.Stop();
                                    _selectedLandmarkANDPoint.Start(landmark.Point);
                                    _selectedLandmarkAND = landmark;

                                    GStreet street = landmark.GetStreet();
                                    if (street != null)
                                    {
                                        _parentRadius.Polygon = (IPolygon)street.Polyline.Buffer(20);
                                        _parentRadius.Stop();
                                        _parentRadius.Start(point);
                                    }
                                    else
                                    {
                                        _parentRadius.Stop();
                                    }
                                    break;
                                }
                            }
                        }
                        else if (button == MouseKey.Right)
                        {
                            MapDocument.ShowContextMenu(x, y);
                        }
                        break;
                    case Progress.Moving:
                        {
                            if (button != MouseKey.Left)
                            {
                                break;
                            }

                            GStreet street;
                            IPoint newPoint;
                            List<GStreet> streets;
                            try
                            {
                                using (new WaitCursor())
                                {
                                    newPoint = (IPoint)_selectedLandmarkANDPoint.Stop();
                                    _parentRadius.Stop();

                                    street = _selectedLandmarkAND.GetStreet();

                                }
                                if (street != null)
                                {
                                    using (new WaitCursor())
                                    {
                                        double distance = newPoint.DistanceTo(street.Shape);
                                        streets = repo.SpatialSearch<GStreet>(newPoint.Buffer(distance), esriSpatialRelEnum.esriSpatialRelIntersects).ToList();
                                    }
                                    if (streets.Count > 0)
                                    {
                                        GStreet nearestStreet = GeometryUtils.GetNearestFeature(streets, newPoint);
                                        if (!nearestStreet.Equals(street))
                                        {
                                            QuestionMessageBox chooseNewBox = new QuestionMessageBox();
                                            chooseNewBox.SetCaption("Move Landmark (AND)")
                                                        .SetText("Another street nearby. Choose new street?");

                                            if (chooseNewBox.Show() == DialogResult.Yes)
                                            {
                                                GStreet newStreet = ChooseFeature(streets, nearestStreet);
                                                _selectedLandmarkAND.StreetId = newStreet.OID;
                                            }
                                        }
                                    }
                                }
                                using (new WaitCursor())
                                {
                                    repo.StartTransaction(() =>
                                    {
                                        _selectedLandmarkAND.Shape = newPoint;
                                        repo.Update(_selectedLandmarkAND);

                                    });
                                }
                            }
                            finally
                            {
                                _progress = Progress.Select;
                                _selectedLandmarkAND = null;
                                OnReported(Message1);
                                OnStepReported(Message1);
                            }
                            break;
                        }
                    default:
                        throw new Exception("Unknown progress.");
                }
            }
            catch (Exception ex)
            {
                using (MessageBoxBuilder box = MessageBoxFactory.Create(ex))
                {
                    box.Show();
                }
            }
            finally
            {
                InProgress = false;
            }
        }

        private List<GLandmarkAND> SelectLandmarkAND(RepositoryFactory repo, IPoint point)
        {
            Query<GLandmarkAND> query = new Query<GLandmarkAND>(SegmentName);
            query.SpatialRelation = esriSpatialRelEnum.esriSpatialRelIntersects;
            query.Geometry = point.Buffer(MapUtils.ConvertPixelsToMapUnits(ActiveView, 8, 1));
            return repo.SpatialSearch(query).ToList();
        }

        protected override void Reset()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
            }
            if (_selectedLandmarkANDPoint != null)
            {
                _selectedLandmarkANDPoint.Stop();
            }
        }

        public override bool Deactivate()
        {
            _progress = Progress.Select;
            if (_parentRadius != null)
            {
                _parentRadius.Stop();
                _parentRadius = null;
            }
            if (_selectedLandmarkANDPoint != null)
            {
                _selectedLandmarkANDPoint.Stop();
                _selectedLandmarkANDPoint = null;
            }
            return base.Deactivate();
        }

        public override void Refresh(int hDC)
        {
            base.Refresh(hDC);
            if (_parentRadius != null)
            {
                _parentRadius.Refresh(hDC);
            }
            if (_selectedLandmarkANDPoint != null)
            {
                _selectedLandmarkANDPoint.Refresh(hDC);
            }
        }
    }
}
